﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Drawing;
using NodeView.Utils;
using NodeViewHotKey.Model;

namespace NodeViewHotKey
{
    public partial class NodeViewHotKeyForm : Form
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private NotifyIcon _notify = new NotifyIcon();

        public NodeViewHotKeyForm()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Minimized;
            this.Hide();
        }

        private void NodeViewHotKeyForm_Load(object sender, EventArgs e)
        {
            InitNotify();
            LoadHotKeyCommands("hotkeyCommands.json");
        }

        private void InitNotify()
        {
            var menu = new ContextMenuStrip();
            ToolStripMenuItem menuItem = new ToolStripMenuItem
            {
                Text = "Open"
            };
            menuItem.Click += delegate
            {
                this.WindowState = FormWindowState.Normal;
                this.Show();
            };
            menu.Items.Add(menuItem);

            menuItem = new ToolStripMenuItem
            {
                Text = "Exit"
            };
            menuItem.Click += delegate
            {
                _notify.Visible = false;
                Close();
            };
            menu.Items.Add(menuItem);

            //_notify.Icon = new System.Drawing.Icon(new Uri("path"));
            _notify.Icon = Properties.Resources.hotkey;
            _notify.Visible = true;
            _notify.DoubleClick += delegate
            {
                //DoubleMethod();
            };
            _notify.ContextMenuStrip = menu;
            _notify.Text = "NodeView HotKey";
        }

        private void LoadHotKeyCommands(string hotkeyCommandsJsonFile)
        {
            string jsonFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, hotkeyCommandsJsonFile);
            if (!File.Exists(jsonFilename))
            {
                listBoxLogs.Items.Insert(0, "[hotkeyCommands.json]파일을 찾을 수 없습니다.");
                return;
            }

            try
            {
                var converter = new KeyGestureConverter();
                var jsonArray = JArray.Parse(File.ReadAllText(jsonFilename));
                int no = 0;
                foreach (JToken jToken in jsonArray)
                {
                    no++;
                    JObject commandJson = jToken.Value<JObject>();
                    string keyGestureString = JsonUtils.GetStringValue(commandJson, "keyGesture");
                    string commandName = JsonUtils.GetStringValue(commandJson, "command");
                    string description = JsonUtils.GetStringValue(commandJson, "description", commandName);

                    if (string.IsNullOrWhiteSpace(keyGestureString) || string.IsNullOrWhiteSpace(commandName))
                    {
                        listBoxLogs.Items.Insert(0, $"[{no}]keyGesture 또는 command 항목이 없습니다.");
                        continue;
                    }

                    try
                    {
                        KeyGesture keyGesture = (KeyGesture)converter.ConvertFrom(keyGestureString);
                        keyGestureString = converter.ConvertToString(keyGesture);
                    }
                    catch
                    {
                        listBoxLogs.Items.Insert(0, $"[{no}] '{keyGestureString}'은 잘못된 keyGesture 입니다.");
                        continue;
                    }
                    if (commandName == "mouseCursor")
                    {
                        Point position = JsonUtils.GetValue(commandJson, "position", new Point(0, 0));
                        var command = new MouseCursorHotKeyCommand(description, position);
                        listBoxHotKey.Items.Add($"[{keyGestureString}] {description}");
                        GlobalHotKey.RegisterHotKey(keyGestureString, () =>
                        {
                            string timeStamp = StringUtils.GetCurrentDateTimeString();
                            listBoxLogs.Items.Insert(0, $"[{timeStamp}]:[{keyGestureString}] {description}");
                            command.Run();
                        });
                    }
                    else if(commandName == "restApi")
                    {
                        string url = JsonUtils.GetStringValue(commandJson, "url");
                        string method = JsonUtils.GetStringValue(commandJson, "method").ToUpper();
                        JObject bodyJson = JsonUtils.GetValue(commandJson, "body", new JObject());
                        if (string.IsNullOrWhiteSpace(url))
                        {
                            listBoxLogs.Items.Insert(0, $"[{no}]url 항목이 없습니다.");
                            continue;
                        }

                        var command = new RestApiHotKeyCommand(description, url, bodyJson, method);
                        listBoxHotKey.Items.Add($"[{keyGestureString}] {description}");
                        GlobalHotKey.RegisterHotKey(keyGestureString, () =>
                        {
                            string timeStamp = StringUtils.GetCurrentDateTimeString();
                            listBoxLogs.Items.Insert(0, $"[{timeStamp}]:[{keyGestureString}] {description}");
                            command.Run();
                        });
                    }
                    else
                    {
                        listBoxLogs.Items.Insert(0, $"[{no}] '{commandName}'은 지원하지 않는 커멘드 입니다.");
                        continue;
                    }
                }

            }
            catch (Exception e)
            {
                listBoxLogs.Items.Insert(0, e.Message);
            }
        }

        private void NodeViewHotKeyForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _notify.Visible = false;
        }

        private void NodeViewHotKeyForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }
    }
}
