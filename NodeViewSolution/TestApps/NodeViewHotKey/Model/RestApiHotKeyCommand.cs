﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Net;

namespace NodeViewHotKey.Model
{
    public class RestApiHotKeyCommand : IHotKeyCommand
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public string Description { get; }
        public string Url { get; }
        public string Method { get; }
        public JObject Body { get; }

        public RestApiHotKeyCommand(string description, string url, JObject body, string method = "GET")
        {
            Description = description;
            Url = url;
            Method = string.IsNullOrWhiteSpace(method) ? "GET" : method.ToUpper();
            Body = body ?? new JObject();
        }

        public void Run()
        {
            try
            {
                RestApiResponse response = null;
                RestApiClient apiClient = new RestApiClient("");
                switch (Method)
                {
                    case "GET":
                        response = apiClient.Get(Url);
                        break;
                    case "POST":
                        response = apiClient.Post(Url, Body);
                        break;
                    case "PUT":
                        response = apiClient.Put(Url, Body);
                        break;
                    case "DELETE":
                        response = apiClient.Delete(Url, Body);
                        break;
                }

                if (response == null)
                {
                    Logger.Warn($"[{Method}] is not supported method.");
                }
                else if (!response.IsSuccess)
                {
                    Logger.Warn($"failed :[{response.ResponseCode}].");
                }
            }
            catch
            {
                //skip
            }

        }
    }
}
