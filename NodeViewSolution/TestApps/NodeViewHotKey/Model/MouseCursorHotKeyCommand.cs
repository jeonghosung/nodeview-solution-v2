﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using NLog;

namespace NodeViewHotKey.Model
{
    public class MouseCursorHotKeyCommand : IHotKeyCommand
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public string Description { get; }
        public Point CursorPosition { get; }

        public MouseCursorHotKeyCommand(string description, System.Drawing.Point cursorPosition)
        {
            Description = description;
            CursorPosition = cursorPosition;
        }

        public void Run()
        {
            System.Windows.Forms.Cursor.Position = CursorPosition;
        }
    }
}
