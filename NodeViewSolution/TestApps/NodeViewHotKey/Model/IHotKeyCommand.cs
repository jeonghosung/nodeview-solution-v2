﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Windows.Input;

namespace NodeViewHotKey.Model
{
    public interface IHotKeyCommand
    {
        string Description { get; }
        void Run();
    }
}
