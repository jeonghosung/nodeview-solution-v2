﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using Prism.Commands;
using Prism.Ioc;
using Prism.Services.Dialogs;

namespace DesignTestApp.ViewModels
{
    public class MainWindowViewModel
    {
        private readonly IContainerProvider _containerProvider;
        private readonly IDialogService _dialogService;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ObservableCollection<string> TemplateItems { get; set; } = new ObservableCollection<string>();
        public ObservableCollection<string> XamlItems { get; set; } = new ObservableCollection<string>();

        public DelegateCommand<string> RunTemplateCommand { get; set; }
        public DelegateCommand<string> RunXamlCommand { get; set; }
        public MainWindowViewModel(IContainerProvider containerProvider, IDialogService dialogService)
        {
            _containerProvider = containerProvider;
            _dialogService = dialogService;
            TemplateItems.Add("RoundIconPopup");
            TemplateItems.Add(":confirm");
            TemplateItems.Add(":warningConfirm");
            TemplateItems.Add(":errorConfirm");

            XamlItems.Add("Popup1Window");
            XamlItems.Add("Popup2Window");
            XamlItems.Add("Popup3Window");
            XamlItems.Add("Popup4Window");
            XamlItems.Add("Popup5Window");
            XamlItems.Add("Popup6Window");
            XamlItems.Add("Popup7Window");
            XamlItems.Add("Popup8Window");
            XamlItems.Add("PowerControl");

            RunTemplateCommand = new DelegateCommand<string>(ExecuteRunTemplateCommand);
            RunXamlCommand = new DelegateCommand<string>(ExecuteRunXamlCommand);
        }

        private void ExecuteRunXamlCommand(string xamlName)
        {
            Type viewType = Type.GetType($"DesignTestApp.Views.{xamlName}");
            var view = _containerProvider.Resolve(viewType);
            if (view is Window window)
            {
                window.ShowDialog();
            }
        }

        private void ExecuteRunTemplateCommand(string windowName)
        {
            OpenWindow(windowName);
        }

        public void OpenWindow(string name)
        {
            try
            {
                if (name.StartsWith(":"))
                {
                    name = name.Trim(':');
                    _dialogService.ShowDialog(name, null, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                        }
                    });

                }
                else
                {
                    _dialogService.ShowDialog("CellSplitOptionDialog", null, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                        }
                    }, name);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSplitCellCommand:");
            }
        }
    }
}
