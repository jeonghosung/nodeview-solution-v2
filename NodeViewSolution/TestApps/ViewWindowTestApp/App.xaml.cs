﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using NodeView.Apps;
using NodeView.DataWallContentControls;
using NodeView.Dialogs;
using NodeView.DialogWindows;
using NodeView.Frameworks.Widgets;
using NodeView.Ioc;
using NodeView.Widgets;
using Prism.DryIoc;
using ViewWindowTestApp.Views;
using Prism.Ioc;
using Prism.Regions;
using ViewWindowTestApp.Dialogs.ViewModels;
using ViewWindowTestApp.Dialogs.Views;
using ViewWindowTestApp.Frameworks.ViewWindows;
using ViewWindowTestApp.TestDependencyInjectionModels;
using ViewWindowTestApp.ViewModels;
using ViewWindowTestApp.ViewModelTests.Controls;
using ViewWindowTestApp.ViewModelTests.Interface;
using ViewWindowTestApp.ViewModelTests.Views;

namespace ViewWindowTestApp
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : PrismApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private Window _mainWindow = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            string appName = ResourceAssembly.GetName().Name;
            if (AppUtils.IsDuplicateExecution())
            {
                MessageBox.Show($"'{appName}' 프로그램이 이미 실행 중입니다.", "알림", MessageBoxButton.OK,
                    MessageBoxImage.Information);
                Current.Shutdown();

            }
            else
            {
                SetupUnhandledExceptionHandling();
                base.OnStartup(e);

            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            if (containerRegistry is IContainerExtension containerExtension)
            {
                NodeViewContainerExtension.CreateInstance(containerExtension);
                var viewWindowService = new ViewWindowService(containerExtension);
                containerRegistry.RegisterInstance(typeof(ViewWindowService), viewWindowService);
            }

            //TestModels
            RegisterInstanceTestModel instance = new RegisterInstanceTestModel();
            instance.Value = "0000";
            containerRegistry.RegisterInstance(typeof(RegisterInstanceTestModel), instance);

            //IViewModelTest
            containerRegistry.Register(typeof(IViewModelTest), typeof(ViewModelTestControl), "test_control");
            containerRegistry.Register(typeof(IViewModelTest), typeof(ViewModelTestView), "test_view");
            containerRegistry.Register(typeof(IViewModelTest), typeof(ViewModelTestView2), "test_View2");

            //DataWallContentControls
            containerRegistry.RegisterDataWallContentControl(typeof(DummyDataWallContentControl), "");
            containerRegistry.RegisterDataWallContentControl(typeof(DummyDataWallContentControl), "dummy");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "media");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "camera");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "ptz-camera");
            containerRegistry.RegisterDataWallContentControl(typeof(WebDataWallContentControl), "web");

            //Dialogs WidgetContentControls
            //containerRegistry.RegisterWidgetContentControl(typeof(DummyWidget), "");
            containerRegistry.Register(typeof(IWidgetControl), typeof(DummyWidget), "dummy");
            //containerRegistry.RegisterWidgetContentControl(typeof(UserControl), typeof(DataWallControlWidgetOld), "widget_datawall");

            //Dialogs
            containerRegistry.RegisterDialogWindow<CustomDialogWindow>("custom");
            containerRegistry.RegisterDialogWindow<NoFrameMovableDialogWindow>("content");


            containerRegistry.RegisterDialog<CellSplitOptionDialog, CellSplitOptionDialogModelEx>();
            containerRegistry.RegisterDialog<TestDialog, TestDialogModel>("test");
            containerRegistry.RegisterDialog<DataWallWindowViewDialog, DataWallWindowViewDialogModel>("content");

        }

        protected override Window CreateShell()
        {
            var viewWindowService = Container.Resolve<ViewWindowService>();
            _mainWindow = Container.Resolve<MainWindow>();
            //_mainWindow.DataContext = Container.Resolve<DataWallManagerWindowViewModel>();
            return _mainWindow;
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();

            IRegionManager regionManager = Container.Resolve<IRegionManager>();
            //regionManager.RegisterViewWithRegion("WidgetSelector", typeof(WidgetSelectorOnTopControl));

            if (_mainWindow != null)
            {
                _mainWindow.Closed += (sender, args) => { Application.Current.Shutdown(); };
            }
        }

        private void SetupUnhandledExceptionHandling()
        {
            // Catch exceptions from all threads in the AppDomain.
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException",
                    false);

            // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
            TaskScheduler.UnobservedTaskException += (sender, args) =>
                ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", false);

            // Catch exceptions from a single specific UI dispatcher thread.
            Dispatcher.UnhandledException += (sender, args) =>
            {
                // If we are debugging, let Visual Studio handle the exception and take us to the code that threw it.
                if (!Debugger.IsAttached)
                {
                    args.Handled = true;
                    ShowUnhandledException(args.Exception, "Dispatcher.UnhandledException", true);
                }
            };
        }

        void ShowUnhandledException(Exception e, string unhandledExceptionType, bool promptUserForShutdown)
        {
            Logger.Error(e, $"ShowUnhandledException ['{unhandledExceptionType}',{promptUserForShutdown}]");
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{e}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
