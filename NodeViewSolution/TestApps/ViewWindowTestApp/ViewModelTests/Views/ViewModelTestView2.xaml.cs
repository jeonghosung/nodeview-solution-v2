﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewWindowTestApp.ViewModelTests.Interface;

namespace ViewWindowTestApp.ViewModelTests.Views
{
    /// <summary>
    /// ViewModelTestView2.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ViewModelTestView2 : UserControl, IViewModelTest
    {
        public IViewModelTestSource Source { get; }

        public ViewModelTestView2(IViewModelTestSource source)
        {
            Source = source;
            InitializeComponent();
        }
    }
}
