﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewWindowTestApp.ViewModelTests.Interface;

namespace ViewWindowTestApp.ViewModelTests.Controls
{
    public class ViewModelTestControlViewModel
    {
        public IViewModelTestSource Source { get; }

        public ViewModelTestControlViewModel(IViewModelTestSource source)
        {
            Source = source;
        }
    }
}
