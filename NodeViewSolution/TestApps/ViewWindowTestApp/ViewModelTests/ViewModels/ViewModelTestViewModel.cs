﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewWindowTestApp.ViewModelTests.Interface;

namespace ViewWindowTestApp.ViewModelTests.ViewModels
{
    public class ViewModelTestViewModel
    {
        public IViewModelTestSource Source { get; }

        public ViewModelTestViewModel(IViewModelTestSource source)
        {
            Source = source;
        }
    }
}
