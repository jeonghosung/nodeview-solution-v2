﻿using System.Windows;
using NodeView.DataWalls;

namespace ViewWindowTestApp.ViewWindows
{
    public class DialogContentViewWindowViewModel : DialogViewWindowViewModel, IContentWindowModel
    {
        public void SetContentWindow(IDataWallWindow nodeViewWindow)
        {
            this.SizeToContent = SizeToContent.Manual;
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Topmost = nodeViewWindow.IsPopup;
            this.Top = nodeViewWindow.WindowRect.Top;
            this.Left = nodeViewWindow.WindowRect.Left;
            this.Width = nodeViewWindow.WindowRect.Width;
            this.Height = nodeViewWindow.WindowRect.Height;
            this.Title = nodeViewWindow.Name;
        }
    }

    public interface IContentWindowModel
    {
        void SetContentWindow(IDataWallWindow nodeViewWindow);
    }
}
