﻿using System;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;

namespace ViewWindowTestApp.Frameworks.ViewWindows
{
    public class NodeViewWindow : IDataWallWindow
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string Id { get; }
        public string Name { get; set; }
        public bool IsPopup { get; set; }
        public IntRect WindowRect { get; set; }
        public string ContentId { get; set; }
        public IDataWallContent Content { get; private set; }

        public NodeViewWindow(IDataWallWindow window)
        {
            Id = window.Id;
            Name = window.Name;
            IsPopup = window.IsPopup;
            WindowRect = new IntRect(window.WindowRect);
            ContentId = window.ContentId;
            Content = window.Content == null ? null : new DataWallContent(window.Content);
        }
        public NodeViewWindow(string id, string name = "", bool isPopup = false, IntRect screenRect=null, string contentId = "", DataWallContent content = null)
        {
            Id = string.IsNullOrWhiteSpace(id) ? Uuid.NewUuid : id;
            IsPopup = isPopup;
            WindowRect = screenRect ?? IntRect.Empty;
            ContentId = contentId;
            Content = content == null ? null : new DataWallContent(content);
            Name = content != null ? content.Name : name;
        }

        public void SetContent(IDataWallContent content)
        {
            if (content != null)
            {
                Content = new DataWallContent(content);
                Name = Content.Name;
            }
        }

        public static NodeViewWindow CreateFrom(JObject json)
        {
            try
            {
                string id = JsonUtils.GetStringValue(json, "id", Uuid.NewUuid);
                if (json == null || string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }

                var window = new NodeViewWindow(id);
                if (window.LoadFrom(json))
                {
                    return window;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                Name = JsonUtils.GetValue(json, "name", Name);
                IsPopup = JsonUtils.GetValue(json, "isPopup", IsPopup);
                WindowRect = JsonUtils.GetValue(json, "windowRect", new IntRect(WindowRect));
                ContentId = JsonUtils.GetValue(json, "contentId", ContentId);
                DataWallContent content = null;
                JObject contentObject = JsonUtils.GetValue<JObject>(json, "content", null);
                if (contentObject != null)
                {
                    content = DataWallContent.CreateFrom(contentObject);
                }

                Content = content;

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public JToken ToJson()
        {
            var jObject = new JObject
            {
                ["id"] = Id,
                ["name"] = Name,
                ["isPopup"] = IsPopup,
                ["windowRect"] = WindowRect.ToString()
            };
            if (!string.IsNullOrWhiteSpace(ContentId))
            {
                jObject["contentId"] = ContentId;
            }
            if (Content != null)
            {
                jObject["content"] = Content.ToJson();
            }
            return jObject;
        }

        public static NodeViewWindow CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetAttrValue(xmlNode, "id", Uuid.NewUuid);
                var window = new NodeViewWindow(id);
                if (window.LoadFrom(xmlNode))
                {
                    return window;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
                IsPopup = XmlUtils.GetAttrValue(xmlNode, "isPopup", IsPopup);
                WindowRect = XmlUtils.GetAttrValue(xmlNode, "windowRect", new IntRect(WindowRect));
                ContentId = XmlUtils.GetAttrValue(xmlNode, "contentId", ContentId);
                DataWallContent content = null;
                XmlNode contentNode = xmlNode.SelectSingleNode("Content");
                if (contentNode != null)
                {
                    content = DataWallContent.CreateFrom(contentNode);
                }

                Content = content;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Window";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("isPopup", $"{IsPopup}");
            xmlWriter.WriteAttributeString("windowRect", $"{WindowRect}");
            if (!string.IsNullOrWhiteSpace(ContentId))
            {
                xmlWriter.WriteAttributeString("contentId", $"{ContentId}");
            }

            Content?.WriteXml(xmlWriter, "Content");

            xmlWriter.WriteEndElement();
        }
    }
}
