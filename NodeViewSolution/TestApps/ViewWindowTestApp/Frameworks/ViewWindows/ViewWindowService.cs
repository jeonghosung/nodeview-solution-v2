﻿using NodeView.DataWalls;
using Prism.Ioc;
using ViewWindowTestApp.Controls;
using ViewWindowTestApp.ViewWindows;

namespace ViewWindowTestApp.Frameworks.ViewWindows
{
    public class ViewWindowService
    {
        private readonly IContainerProvider _containerProvider;

        public ViewWindowService(IContainerExtension containerExtension)
        {
            _containerProvider = containerExtension;
        }
        public string Show()
        {
            IViewWindow window = _containerProvider.Resolve<DialogViewWindow>();
            var model = _containerProvider.Resolve<DialogViewWindowViewModel>();
            window.DataContext = model;
            window.ClientControlRegion.Content = _containerProvider.Resolve<TestControl>();
            window.Show();

            return "";
        }
        public string ShowContentWindow(IDataWallWindow nodeViewWindow)
        {
            /*
            IViewWindow window = _containerProvider.Resolve<DialogViewWindow>();
            var model = _containerProvider.Resolve<DialogContentViewWindowViewModel>();

            window.ClientControlRegion.Content = _containerProvider.Resolve<DummyViewControl>();
            DummyViewControlViewModel contentWindowModel = _containerProvider.Resolve<DummyViewControlViewModel>();
            contentWindowModel.SetNodeViewContent(nodeViewWindow.Content);
            window.ClientControlRegion.DataContext = contentWindowModel;

            window.DataContext = model;
            window.Show();
            */
            return "";
        }
    }
}
