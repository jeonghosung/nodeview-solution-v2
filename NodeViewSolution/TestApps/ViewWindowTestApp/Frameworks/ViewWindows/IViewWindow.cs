﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace ViewWindowTestApp.Frameworks.ViewWindows
{
    public interface IViewWindow
    {
        event RoutedEventHandler Loaded;
        event EventHandler Closed;
        event CancelEventHandler Closing;

        string Id { get; set; }

        ContentControl ClientControlRegion { get; }
        void Show();
        object DataContext { get; set; }
        void Close();
    }
}
