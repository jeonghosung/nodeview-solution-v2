﻿using System.Windows;
using NodeView.CommonWindows.ViewModels;

namespace ViewWindowTestApp.Frameworks.ViewWindows
{
    public class ViewWindowViewModelBase : WindowChromeViewModelBase
    {
        private bool _topmost = false;
        private SizeToContent _sizeToContent = SizeToContent.Manual;
        private WindowStartupLocation _windowStartupLocation = WindowStartupLocation.Manual;
        private int _top = 0;
        private int _left = 0;
        private int _width = 1280;
        private int _height = 720;
        private string _title = "View";

        public bool Topmost
        {
            get => _topmost;
            set => SetProperty(ref _topmost, value);
        }

        public SizeToContent SizeToContent
        {
            get => _sizeToContent;
            set => SetProperty(ref _sizeToContent, value);
        }

        public WindowStartupLocation WindowStartupLocation
        {
            get => _windowStartupLocation;
            set => SetProperty(ref _windowStartupLocation, value);
        }

        public int Top
        {
            get => _top;
            set => SetProperty(ref _top, value);
        }

        public int Left
        {
            get => _left;
            set => SetProperty(ref _left, value);
        }

        public int Width
        {
            get => _width;
            set => SetProperty(ref _width, value);
        }

        public int Height
        {
            get => _height;
            set => SetProperty(ref _height, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
    }
}
