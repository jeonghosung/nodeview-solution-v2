﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ViewWindowTestApp.NodeViewControls
{
    /// <summary>
    /// TestControlUsingSource.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class TestControlUsingSource : UserControl
    {
        public TestControlUsingSource()
        {
            InitializeComponent();
            NameLable.Content = "Initialize";
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("ContentSource", typeof(Object), typeof(TestControlUsingSource), new PropertyMetadata(OnSourcePropertyChanged));

        public Object Source
        {
            get => (Object)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }
        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is TestControlUsingSource owner)
            {
                owner.NameLable.Content = e.NewValue ?? "Null";
            }
        }
    }
}
