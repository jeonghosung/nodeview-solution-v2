﻿using System;
using System.Windows;
using System.Windows.Controls;
using NodeView.DataWalls;

namespace ViewWindowTestApp.NodeViewControls
{
    /// <summary>
    /// DataWallContentControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataWallContentControl : UserControl
    {
        //private readonly IContainerProvider _containerProvider;

        public DataWallContentControl()
        {
            //_containerProvider = containerProvider;

            InitializeComponent();
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("ContentSource", typeof(Object), typeof(DataWallContentControl), new PropertyMetadata(OnSourcePropertyChanged));

        public Object Source
        {
            get => (Object)GetValue(SourceProperty);
            set
            {
                if (value is IDataWallContent)
                {
                    SetValue(SourceProperty, value);
                }
            } 
        }
        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DataWallContentControl owner)
            {
                if (e.NewValue is IDataWallContent nodeViewContent)
                {
                    Control control = null;
                    if (nodeViewContent.ContentType == "web")
                    {
                        control = new DummyControl1(); //owner._containerProvider.Resolve<DummyControl1>();
                    }
                    else
                    {
                        control = new DummyControl2(); //owner._containerProvider.Resolve<DummyControl2>();
                    }
                    owner.ContentRegion.Content = control;
                }
            }
        }

    }
}
