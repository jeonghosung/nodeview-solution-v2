﻿using NodeView.Utils;
using Prism.Ioc;
using Prism.Mvvm;

namespace ViewWindowTestApp.TestDependencyInjectionModels
{
    public class RegisterInstanceTestModel : BindableBase
    {
        private string _value;
        public string Id { get; }

        public string Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public RegisterInstanceTestModel()
        {
            Id = Uuid.NewUuid;
            Value = "Value";
        }
    }
}
