﻿using NodeView.Utils;
using Prism.Ioc;
using Prism.Mvvm;

namespace ViewWindowTestApp.TestDependencyInjectionModels
{
    public class TestModel : BindableBase
    {
        private string _value;
        public string Id { get; }

        public string Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public TestModel(IContainerProvider containerProvider)
        {
            Id = Uuid.NewUuid;
            Value = "Value";
        }
    }
}
