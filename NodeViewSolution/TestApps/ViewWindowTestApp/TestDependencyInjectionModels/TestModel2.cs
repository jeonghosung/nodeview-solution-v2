﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;
using Prism.Ioc;

namespace ViewWindowTestApp.TestDependencyInjectionModels
{
    public class TestModel2
    {
        public TestModel BaseModel { get; set; }
        public RegisterInstanceTestModel Instance { get; set; }

        public TestModel2(IContainerProvider containerProvider, TestModel baseModel, RegisterInstanceTestModel instance)
        {
            BaseModel = baseModel;
            Instance = instance;
        }
    }
}
