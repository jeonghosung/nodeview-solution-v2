﻿using System;
using NodeView.CommonWindows.ViewModels;
using NodeView.Dialogs;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace ViewWindowTestApp.ViewModels
{
    public class CellSplitOptionDialogModelEx : WindowChromeViewModelBase, IDialogAware
    {
        private string _iconSource;
        public string IconSource
        {
            get => _iconSource;
            set => SetProperty(ref _iconSource, value);
        }

        private string _title;
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public event Action<IDialogResult> RequestClose;
        public virtual void RaiseRequestClose(IDialogResult dialogResult)
        {
            RequestClose?.Invoke(dialogResult);
        }

        private int _rows = 2;
        public int Rows
        {
            get => _rows;
            set => SetProperty(ref _rows, value);
        }

        private int _columns = 2;
        public int Columns
        {
            get => _columns;
            set => SetProperty(ref _columns, value);
        }
        public DelegateCommand CancelDialogCommand { get; set; }
        public DelegateCommand ApplyDialogCommand { get; set; }

        public CellSplitOptionDialogModelEx()
        {
            ApplyDialogCommand = new DelegateCommand(ExecuteApplyDialogCommand);
            CancelDialogCommand = new DelegateCommand(ExecuteCancelDialogCommand);
        }

        private void ExecuteApplyDialogCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("rows", Rows);
            parameters.Add("columns", Columns);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }

        private void ExecuteCancelDialogCommand()
        {
            RaiseRequestClose(new DialogResult(ButtonResult.Cancel));
        }

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
        }

        public void OnDialogOpened(IDialogParameters parameters)
        {
            Title = "셀 분할 옵션";
            if (parameters.TryGetValue("rows", out int rows))
            {
                Rows = rows;
            }
            if (parameters.TryGetValue("columns", out int columns))
            {
                Columns = columns;
            }
        }
    }
}
