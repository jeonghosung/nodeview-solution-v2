﻿using System.Collections.ObjectModel;
using NodeView.DataWalls;
using NodeView.Frameworks.ContentControls;
using NodeView.Frameworks.Widgets;
using NodeView.Utils;
using Prism.Mvvm;

namespace ViewWindowTestApp.ViewModels
{
    public class ControlSourceTestWindowViewModel: BindableBase
    {
        private IDataWallContent _dummyContent;
        private IDataWallContent _webContent;
        private IDataWallContent _cameraContent;
        private ObservableCollection<IContentSource> _nodeViewContents = new ObservableCollection<IContentSource>();

        public ObservableCollection<IContentSource> NodeViewContents
        {
            get => _nodeViewContents;
            set => SetProperty(ref _nodeViewContents, value);
        }

        public IDataWallContent DummyContent
        {
            get => _dummyContent;
            set => SetProperty(ref _dummyContent, value);
        }

        public IDataWallContent WebContent
        {
            get => _webContent;
            set => SetProperty(ref _webContent, value);
        }

        public IDataWallContent CameraContent
        {
            get => _cameraContent;
            set => SetProperty(ref _cameraContent, value);
        }

        public ControlSourceTestWindowViewModel()
        {
            var dummyContent = new DataWallContentBindable(){
                Id = Uuid.NewUuid,
                ContentType = "dummy",
                Name ="dummy datawall content"
            };
            DummyContent = dummyContent;

            var webContent = new DataWallContentBindable
            {
                Id = Uuid.NewUuid,
                ContentType = "web",
                Uri = "https://beta.map.naver.com/search?c=14125134.1925352,4507003.0954080,15,0,0,2,dh"
            };
            WebContent = webContent;
            
            var cameraContent = new DataWallContentBindable
            {
                Id = Uuid.NewUuid,
                ContentType = "camera",
                Uri = "rtsp://192.168.31.33/video1"
            };
            CameraContent = cameraContent;

            _nodeViewContents.Add(dummyContent);
            _nodeViewContents.Add(webContent);
            _nodeViewContents.Add(cameraContent);

            var dummyWidgetContent = new WidgetSource(Uuid.NewUuid, "dummy", "test Widget");
            _nodeViewContents.Add(dummyWidgetContent);

            var testContent = new WidgetSource(Uuid.NewUuid, "test", "test");
            _nodeViewContents.Add(dummyWidgetContent);

        }
    }
}
