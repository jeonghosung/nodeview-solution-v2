﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Ioc;
using NodeView.Utils;
using ViewWindowTestApp.TestDependencyInjectionModels;
using Prism.Commands;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using ViewWindowTestApp.ViewModelTests.Controls;
using ViewWindowTestApp.ViewModelTests.Interface;
using ViewWindowTestApp.ViewModelTests.Model;
using ViewWindowTestApp.ViewModelTests.Views;
using ViewWindowTestApp.Views;

namespace ViewWindowTestApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private readonly IContainerProvider _containerProvider;
        private readonly IDialogService _dialogService;

        public DelegateCommand CreateViewModeCommand { get; set; }
        public DelegateCommand OpenControlSourceTestWindowCommand { get; set; }
        public DelegateCommand<string> ShowWindowCommand { get; set; }
        public DelegateCommand<string> ShowContentWindowCommand { get; set; }
        public MainWindowViewModel(IContainerProvider containerProvider, IDialogService dialogService)
        {
            _containerProvider = containerProvider;
            _dialogService = dialogService;
            CreateViewModeCommand = new DelegateCommand(ExecuteCreateViewModeCommand);
            OpenControlSourceTestWindowCommand = new DelegateCommand(ExecuteOpenControlSourceTestWindowCommand);
            ShowWindowCommand = new DelegateCommand<string>(ExecuteShowWindowCommand);
            ShowContentWindowCommand = new DelegateCommand<string>(ExecuteShowContentWindowCommand);
        }

        private void ExecuteShowContentWindowCommand(string obj)
        {
            var webContent = new DataWallContentBindable();
            webContent.Id = Uuid.NewUuid;
            webContent.ContentType = "web";
            webContent.Uri = "https://beta.map.naver.com/search?c=14125134.1925352,4507003.0954080,15,0,0,2,dh";
            var dataWallWindow = new DataWallWindow(webContent.Id, webContent.Name);
            dataWallWindow.WindowRect = new IntRect(0, 540, 400, 540);
            dataWallWindow.SetContent(webContent);

            var parameters = new DialogParameters();
            parameters.Add("dataWallWindow", dataWallWindow);
            _dialogService.Show("content", parameters, null, "content");
        }

        private void ExecuteOpenControlSourceTestWindowCommand()
        {
            var window = _containerProvider.Resolve<ControlSourceTestWindow>();
            window.Show();
        }

        private void ExecuteShowWindowCommand(string windowType)
        {
            _dialogService.Show("test", null, null, "custom");
            /*
            if (windowType == "content")
            {
                NodeViewWindow window = new NodeViewWindow("testId", "testName", false, new IntRect(540, 480, 540, 480));
                DataWallContent content = new DataWallContent("testContentId", "testContentName");
                window.SetContent(content);

                var viewWindowService = _containerProvider.Resolve<ViewWindowService>();
                string windowId = viewWindowService.ShowContentWindow(window);
            }
            else
            {
                var viewWindowService = _containerProvider.Resolve<ViewWindowService>();
                string windowId = viewWindowService.Show();
            }
            */
        }

        private void ExecuteCreateViewModeCommand()
        {
            TestDiParam();
            TestViewModel();
        }

        private void TestViewModel()
        {
            try
            {
                ViewModelTestSource source = new ViewModelTestSource();
                List<(Type Type, object Instance)> paramList = new List<(Type Type, object Instance)>();
                paramList.Add((typeof(IViewModelTestSource), source));

                //var controlViewModel = _containerProvider.Resolve<ViewModelTestControlViewModel>(paramList.ToArray());
                
                var interfaceView1 = (Control)_containerProvider.ResolveView<IViewModelTest>("test_view", paramList.ToArray());
                var interfaceView1DataContext = interfaceView1.DataContext;

                var interfaceView = (Control)_containerProvider.Resolve<IViewModelTest>("test_view", paramList.ToArray());
                var interfaceViewDataContext = interfaceView.DataContext;
                var interfaceView2 = (Control)_containerProvider.Resolve<IViewModelTest>("test_View2", paramList.ToArray());
                var interfaceView2DataContext = interfaceView2.DataContext;
                var interfaceControl = (Control)_containerProvider.Resolve<IViewModelTest>("test_control", paramList.ToArray());
                var interfaceControlDataContext = interfaceControl.DataContext;


                var control = (Control)_containerProvider.Resolve<ViewModelTestControl>(paramList.ToArray());
                var controlDataContext = control.DataContext;
                var view = (Control)_containerProvider.Resolve<ViewModelTestView>(paramList.ToArray());
                var viewDataContext = view.DataContext;
                var view2 = (Control)_containerProvider.Resolve<ViewModelTestView2>(paramList.ToArray());
                var view2DataContext = view2.DataContext;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private void TestDiParam()
        {
            try
            {
                var testViewModel = _containerProvider.Resolve<TestModel>();
                testViewModel.Value = "4321";
                string id1 = testViewModel.Id;

                var testViewModelFirst = _containerProvider.Resolve<TestModel2>();
                string testViewModelFirstd1 = testViewModelFirst.BaseModel.Value;

                var testViewModel2 = _containerProvider.Resolve<TestModel2>();
                string modelId2 = testViewModel2.BaseModel.Id;
                string modelValue2 = testViewModel2.BaseModel.Value;
                string instanceId2 = testViewModel2.Instance.Id;
                string instanceValue2 = testViewModel2.Instance.Value;

                List<(Type Type, object Instance)> paramList = new List<(Type Type, object Instance)>();
                paramList.Add((typeof(TestModel), testViewModel));

                RegisterInstanceTestModel instance = new RegisterInstanceTestModel();
                instance.Value = "9999";

                paramList.Add((typeof(RegisterInstanceTestModel), instance));


                var testViewModel3 = _containerProvider.Resolve<TestModel2>(paramList.ToArray());
                string modelId3 = testViewModel3.BaseModel.Id;
                string modelValue3 = testViewModel3.BaseModel.Value;
                string instanceId3 = testViewModel3.Instance.Id;
                string instanceValue3 = testViewModel3.Instance.Value;


                var testViewModel4 = _containerProvider.Resolve<TestModel2>();
                string modelId4 = testViewModel4.BaseModel.Id;
                string modelValue4 = testViewModel4.BaseModel.Value;
                string instanceId4 = testViewModel4.Instance.Id;
                string instanceValue4 = testViewModel4.Instance.Value;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
