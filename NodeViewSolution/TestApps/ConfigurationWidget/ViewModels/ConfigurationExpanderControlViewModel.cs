﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Xml;
using NodeView.DataModels;
using NodeView.ViewModels;

namespace ConfigurationWidget.ViewModels
{
    public class ConfigurationExpanderControlViewModel : BindableBase
    {
        private ConfigurationEditSectionModel _configuration;
        #region Property
        /*
        private Section _selectedSection;
        public Section SelectedSection
        {
            get => _selectedSection;
            set => SetProperty(ref _selectedSection, value);
        }
        private ConfigProperty _selectedProperty;
        public ConfigProperty SelectedProperty
        {
            get => _selectedProperty;
            set => SetProperty(ref _selectedProperty, value);
        }
        private ObservableCollection<Section> _sections = new ObservableCollection<Section>();
        public ObservableCollection<Section> Sections
        {
            get => _sections;
            set => SetProperty(ref _sections, value);
        }
        */
        public ConfigurationEditSectionModel Configuration
        {
            get => _configuration;
            set => SetProperty(ref _configuration, value);
        }

        private object _selectedValuePath;
        public object SelectedValuePath
        {
            get 
            {
                return _selectedValuePath;
            }
            set
            {
                _selectedValuePath = value;
            }
        }

        public ConfigurationEditSectionModel[] Configurations => new ConfigurationEditSectionModel[] { _configuration };

        private ObservableCollection<ConfigurationEditSectionModel> _configurationProperties = new ObservableCollection<ConfigurationEditSectionModel>();
        public ObservableCollection<ConfigurationEditSectionModel> ConfigurationProperties
        {
            get => _configurationProperties;
            set => SetProperty(ref _configurationProperties, value);
        }
        #endregion

        #region Constructor
        public ConfigurationExpanderControlViewModel()
        {
            Init();
        } 
        #endregion

        #region Commanmd
        public DelegateCommand<ConfigurationEditSectionModel> SelectedConfigTreeItemCommand
        {
            get
            {
                return new DelegateCommand<ConfigurationEditSectionModel>(args =>
                {
                    CurrentEditingConfigurationSection = args;
                });
            }
        }
        private DelegateCommand _deletePropertyCommand;
        public DelegateCommand DeletePropertyCommand =>
            _deletePropertyCommand ?? (_deletePropertyCommand = new DelegateCommand(ExecuteDeletePropertyCommand));

        void ExecuteDeletePropertyCommand()
        {
            //var configuration = FindConfigurationBindable(SelectedSection);
            //configuration.RemoveProperty(SelectedPropertyBindable.Id);
            //string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewDataWall.config.xml");
            //ConfigurationBindable.Save(settingFile, _configuration);
            //ConfigurationPropertyBindables.Remove(SelectedPropertyBindable);
        }
        private DelegateCommand<object> _saveCommand;
        private ConfigurationEditSectionModel _currentEditingConfigurationSection = null;
        public ConfigurationEditSectionModel CurrentEditingConfigurationSection
        { get
            {
                return _currentEditingConfigurationSection;
            }
            set 
            {
                SetProperty(ref _currentEditingConfigurationSection, value);
            }
        }
        public DelegateCommand<object> SaveCommand =>
            _saveCommand ?? (_saveCommand = new DelegateCommand<object>(ExecuteSaveCommand));

        void ExecuteSaveCommand(object obj)
        {
            //var configuration = FindConfigurationBindable(SelectedSection);
            //configuration.SetProperty(SelectedPropertyBindable);
            //string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewDataWall.config.xml");
            //ConfigurationBindable.Save(settingFile, _configuration);
        }
        #endregion

        #region Private Method
        private void Init()
        {
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewDataWall.config.xml");
            //_configuration = ConfigurationBindable.Load(settingFile);
            XmlDocument doc = new XmlDocument();
            doc.Load(settingFile);
            _configuration = ConfigurationEditSectionModel.CreateFrom(doc.DocumentElement);
            MakeSection(_configuration);
        }

        private void MakeSection(ConfigurationEditSectionModel configuration)
        {
            var properties = configuration.Properties.Where(list => list is IProperty);
            List<PropertyViewModel> propertyViewModels = new List<PropertyViewModel>();
            foreach (var property in properties)
            {
                propertyViewModels.Add(PropertyViewModel.CreateFrom(property));
            }

            configuration.ClearProperties();
            configuration.SetProperties(propertyViewModels);
            var sections = configuration.ChildNodes.Where(list => list is ConfigurationBindable);
            foreach (var section in sections)
            {
                MakeSection(section as ConfigurationEditSectionModel);
            }

        }
        /*
        private ConfigurationEditSectionModel FindConfigurationBindable(Section section)
        {
            ConfigurationEditSectionModel configuration = null;
            //string path = section.Header.Path;
            string path = "";
            if (string.IsNullOrWhiteSpace(path))
            {
                return null;
            }
            string[] pathList = path.Split('/');
            configuration = _configuration;
            if (pathList.Count() > 1)
            {
                for (int i = 1; i < pathList.Count(); i++)
                {
                    configuration = configuration.ChildNodes.First(list => list.Id.Equals(pathList[i], StringComparison.OrdinalIgnoreCase)) as ConfigurationEditSectionModel;
                }
            }
            else
            {

            }
            return configuration;
        }
        */
        #endregion
    }

    //public class PropertyViewModel : PropertyBindableBase
    //{
    //    public string Description => Attributes["description"]?.ToString();

    //    public bool IsReadOnly => Attributes["isReadOnly"] != null &&
    //                              bool.TryParse(Attributes["isReadOnly"].ToString(), out bool IsReadOnly);

    //    public object Items { get; set; }

    //    public string ControlType
    //    {
    //        get
    //        {
    //            var controlType = "";
    //            switch (Attributes["type"].ToString())
    //            {
    //                case "bool":
    //                    List<string> items = new List<string>() { "true", "false" };
    //                    Items = items;
    //                    controlType = "combobox";
    //                    break;
    //                case "int":
    //                    controlType = "textbox";
    //                    break;
    //                case "enum":
    //                    controlType = "combobox";
    //                    break;
    //                case "intPoint":
    //                    controlType = "textbox";
    //                    break;
    //                case "intSize":
    //                    controlType = "password";
    //                    break;
    //                case "password":
    //                    break;
    //                default:
    //                    controlType = "textbox";
    //                    break;
    //            }
    //            return controlType;

    //        }
    //    }

    //    public static PropertyViewModel CreateFrom(IProperty property)
    //    {
    //        PropertyViewModel propertyViewModel = new PropertyViewModel {Id = property.Id, Value = property.Value};
    //        propertyViewModel.SetAttributes(property.Attributes);
    //        return propertyViewModel;
    //    }

    //    public override IProperty Clone()
    //    {
    //        PropertyViewModel property = new PropertyViewModel();
    //        Clone(property);
    //        return property;
    //    }
    //}
    /*
public class Section : BindableBase
{
    private ObservableCollection<ConfigProperty> _configurationProperties = new ObservableCollection<ConfigProperty>();
    public ObservableCollection<ConfigProperty> ConfigurationProperties
    {
        get => _configurationProperties;
        set => SetProperty(ref _configurationProperties, value);
    }
}

public class ConfigProperty : BindableBase
{
    private ControlItemViewModel _controlItemViewModel;
    public ControlItemViewModel ControlItemViewModel
    {
        get => _controlItemViewModel;
        set => SetProperty(ref _controlItemViewModel, value);
    }
    private string _id;
    public string Id
    {
        get => _id;
        set => SetProperty(ref _id, value);
    }
    private string _description;
    public string Description
    {
        get => _description;
        set => SetProperty(ref _description, value);
    }
}
*/
}
