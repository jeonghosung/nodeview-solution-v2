﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ConfigurationWidget.ViewModels
{
    public class TextboxTestViewModel : BindableBase
    {
        private string _value = "2231.14231233";
        public string Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }
        public TextboxTestViewModel()
        {

        }
    }
}
