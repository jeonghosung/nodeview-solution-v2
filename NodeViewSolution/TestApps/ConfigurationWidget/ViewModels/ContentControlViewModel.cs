﻿using NodeView.DataWalls;
using Prism.Mvvm;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;
using NodeView.ViewModels;
using Prism.Commands;

namespace ConfigurationWidget.ViewModels
{
    public class ContentControlViewModel : BindableBase
    {
        private ObservableCollection<ContentItem> _dataWallContents = new ObservableCollection<ContentItem>();
        public ObservableCollection<ContentItem> DataWallContents
        {
            get => _dataWallContents;
            set => SetProperty(ref _dataWallContents, value);
        }
        private ObservableCollection<ContentItem> _currentContents = new ObservableCollection<ContentItem>();
        public ObservableCollection<ContentItem> CurrentContent
        {
            get => _currentContents;
            set => SetProperty(ref _currentContents, value);
        }
        public ContentControlViewModel()
        {
            Init();
        }
        private DelegateCommand _saveCommand;
        public DelegateCommand SaveCommand =>
            _saveCommand ?? (_saveCommand = new DelegateCommand(ExecuteSaveCommand));

        void ExecuteSaveCommand()
        {
            string json = string.Empty;
            foreach (var dataWallContentViewModel in DataWallContents)
            {
                json += dataWallContentViewModel.ToJson();
            }
        }
        public DelegateCommand<object> CotentSelectionChangedCommand
        {
            get
            {
                return new DelegateCommand<object>(args =>
                {
                    if (args is ContentItem dataWallContent)
                    {
                        CurrentContent.Clear();
                        CurrentContent.Add(dataWallContent);
                    }
                });
            }
        }
        private void Init()
        {
            string contentFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "contents.xml");
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.Load(contentFile);
            XmlNodeList contentNodeList = xmlDocument.SelectNodes("/Contents/Content");
            if (contentNodeList != null)
            {
                foreach (XmlNode contentNode in contentNodeList)
                {
                    ContentItem nodeViewContent = ContentItem.CreateFrom(contentNode);
                    if (nodeViewContent != null)
                    {
                        DataWallContents.Add(nodeViewContent);
                    }
                }
            }
        }
    }
}
