﻿using System.Windows.Controls;

namespace ConfigurationWidget.Views
{
    /// <summary>
    /// Interaction logic for ControlMain
    /// </summary>
    public partial class ControlMain : UserControl
    {
        public ControlMain()
        {
            InitializeComponent();
            ConfigControl.Content = new ConfigurationControl();
            ContentControl.Content = new ContentControl();
        }
    }
}
