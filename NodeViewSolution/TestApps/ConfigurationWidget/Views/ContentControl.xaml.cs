﻿using System.Windows.Controls;

namespace ConfigurationWidget.Views
{
    /// <summary>
    /// Interaction logic for ContentControl
    /// </summary>
    public partial class ContentControl : UserControl
    {
        public ContentControl()
        {
            InitializeComponent();
        }
    }
}
