﻿using ConfigurationWidget.Views;
using Prism.Ioc;
using System.Windows;
using Prism.Regions;

namespace ConfigurationWidget
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterForNavigation<ConfigurationExpanderControl>();
            containerRegistry.RegisterForNavigation<ConfigurationControl>();
            containerRegistry.RegisterForNavigation<ContentControl>();
            containerRegistry.RegisterForNavigation<ControlMain>();
            containerRegistry.RegisterForNavigation<TextboxTest>();
        }
        protected override void OnInitialized()
        {
            base.OnInitialized();
            IRegionManager regionManager = Container.Resolve<IRegionManager>();
            regionManager.RequestNavigate("ContentRegion", "TextboxTest");
        }
    }
}
