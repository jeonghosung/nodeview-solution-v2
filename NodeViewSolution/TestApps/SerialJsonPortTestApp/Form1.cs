using NodeView.Net;
using System.IO;
using System.IO.Ports;
using System.Text;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;

namespace SerialJsonPortTestApp
{
    public partial class Form1 : Form
    {
        private const int SelectionCharOffset = 1;

        private SerialJsonPort _serialJsonPort = new SerialJsonPort();
        private Color _normalTextColor = Color.Black;
        private Color _rxTextColor = Color.DarkSlateBlue;
        private Color _txTextColor = Color.DarkRed;
        private Color _errTextColor = Color.Red;

        public Form1()
        {
            InitializeComponent();
            _serialJsonPort.JsonReceived += SerialJsonPortOnJsonReceived;
            _serialJsonPort.ErrorReceived += SerialPortOnErrorReceived;
        }
        private void OpenSerialPort(string comPort, int baudRate)
        {
            bool res = false;
            try
            {
                if (_serialJsonPort.IsOpen)
                {
                    WriteError($"-Error| [{_serialJsonPort.PortName}][{_serialJsonPort.BaudRate}]로 이미 열려 있습니다.");
                    return;
                }

                _serialJsonPort.PortName = comPort;
                _serialJsonPort.BaudRate = baudRate;
                _serialJsonPort.Open();

                OpenCloseButton.Text = "Close";
                SendButton.Enabled = true;
                res = true;
            }
            catch (Exception e)
            {
                WriteError("-Error| " + e.ToString());
            }

            if (res)
            {
                WriteText($"-Open| [{comPort}][{baudRate}] 시리얼포트를 열었습니다.");
            }
            else
            {
                WriteError($"-Error| [{comPort}][{baudRate}]를 열 수 없습니다.");
            }
        }

        private void CloseSerialPort()
        {
            try
            {
                if (_serialJsonPort.IsOpen)
                {
                    string comPort = _serialJsonPort.PortName;
                    int baudRate = _serialJsonPort.BaudRate;
                    _serialJsonPort.Close();
                    WriteText($"-Close| [{comPort}][{baudRate}] 시리얼포트를 닫았습니다.");
                    OpenCloseButton.Text = "Open";
                    SendButton.Enabled = false;
                }
                else
                {
                    WriteError("-Error| 시리얼포트가 열려 있지 않습니다. ");
                }
            }
            catch (Exception e)
            {
                WriteError("-Error| " + e.ToString());
            }
        }
        private void SendJson(string input)
        {
            _serialJsonPort.Write(input);
            WriteSendData(DateTime.Now, $">| " + input);
        }

        private void SerialJsonPortOnJsonReceived(object sender, JsonReceivedEventArgs args)
        {
            DateTime timestamp = DateTime.Now;
            var dataType = args.DataType;
            var payload = args.Payload;
            this.Invoke(new Action(() => WriteReceivedData(timestamp, $"<{dataType}| " + payload)));
        }

        private void SerialPortOnErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            SerialError err = e.EventType;
            string strErr = "";

            switch (err)
            {
                case SerialError.Frame:
                    strErr = "HardWare Framing Error";
                    break;
                case SerialError.Overrun:
                    strErr = "Charaters Buffer Over Run";
                    break;
                case SerialError.RXOver:
                    strErr = "Input Buffer OverFlow";
                    break;
                case SerialError.RXParity:
                    strErr = "Founded Parity Error";
                    break;
                case SerialError.TXFull:
                    strErr = "Write Buffer was Fulled";
                    break;
                default:
                    strErr = e.EventType.ToString();
                    break;
            }
            this.Invoke(new Action(() => WriteError("[Err] " + strErr)));
        }

        private void OpenCloseButton_Click(object sender, EventArgs e)
        {
            if (_serialJsonPort.IsOpen)
            {
                CloseSerialPort();
            }
            else
            {
                string comPort = ComPortComboBox.Text.Trim();
                int baudRate = 9600;
                if (!int.TryParse(BaudrateComboBox.Text.Trim(), out baudRate))
                {
                    baudRate = 9600;
                }
                OpenSerialPort(comPort, baudRate);
            }
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            string input = InputTextBox.Text.Trim();
            SendJson(input);
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            ClearOut();
        }

        private void WriteText(string message)
        {
            WriteLine(DateTime.Now, _normalTextColor, message);
        }
        private void WriteError(string message)
        {
            WriteLine(DateTime.Now, _errTextColor, message);
        }
        private void WriteSendData(DateTime timestamp, string message)
        {
            WriteLine(timestamp, _txTextColor, message);
        }
        private void WriteReceivedData(DateTime timestamp, string message)
        {
            WriteLine(timestamp, _rxTextColor, message);
        }

        private void WriteLine(DateTime timestamp, Color color, string message)
        {
            OutputTextBox.ScrollToCaret();
            OutputTextBox.SelectionCharOffset = SelectionCharOffset;
            OutputTextBox.SelectionColor = color;
            OutputTextBox.AppendText($"[{timestamp:HH:mm:ss.fff}]{message}\r\n");
            OutputTextBox.ScrollToCaret();
        }


        private void ClearOut()
        {
            OutputTextBox.SelectionCharOffset = SelectionCharOffset;
            OutputTextBox.Text = "";
            OutputTextBox.ScrollToCaret();
        }
    }
}