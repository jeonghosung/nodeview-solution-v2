﻿using NodeView.Systems;

namespace PrintMachineKeyConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string key = SystemInfo.GetSimpleMachineKey();
            File.WriteAllText("MachineKey.txt", key);
            Console.WriteLine(key);
        }
    }
}
