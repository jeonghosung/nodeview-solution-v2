﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeViewEventTestApp
{
    public class EventTestItem
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Id { get; set; }
        public bool IsOn { get; set; } = true;
        public string Message { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
