﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Net;
using NodeView.Utils;

namespace NodeViewEventTestApp
{
    public partial class Form1 : Form
    {
        private StringBuilder _sbOutput = new StringBuilder();
        private EventTestItem[] _eventTestItems = new EventTestItem[0];

        public Form1()
        {
            InitializeComponent();
            LoadEventTestSet();
            LoadConfig();
            comboBoxTestSet.Items.AddRange(_eventTestItems);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void buttonRaiseEvent_Click(object sender, EventArgs e)
        {
            SaveConfig();
            string resultMessage = "";
            try
            {
                RestApiClient apiClient = new RestApiClient("");
                JObject requestJson = new JObject()
                {
                    ["id"] = textBoxEventId.Text.Trim(),
                    ["isOn"] = radioButtonOn.Checked,
                    ["message"] = textBoxMessage.Text.Trim(),
                    ["param"] = ""
                };
                var response = apiClient.Post(textBoxUrl.Text, requestJson);
                resultMessage = response.IsSuccess ? "[성공]" :"[실패]";
            }
            catch (Exception ex)
            {
                AppendOutput(ex.Message);
                resultMessage = "[오류]";
            }
            AppendOutput($"RaiseEvent {resultMessage} Done!");
        }

        private void ClearOutput()
        {
            _sbOutput.Clear();
            UpdateOutputToFrom();
        }
        private void AppendOutput(string line)
        {
            _sbOutput.AppendLine(line);
            UpdateOutputToFrom();
        }

        private void UpdateOutputToFrom()
        {
            textBoxOutput.Text = _sbOutput.ToString();
        }

        private void LoadEventTestSet()
        {
            List<EventTestItem> eventTestItems = new List<EventTestItem>();

            try
            {
                string eventTestSetFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eventTestSet.xml"); ;
                if (!File.Exists(eventTestSetFilename))
                {
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(eventTestSetFilename);

                XmlNodeList testItemNodeList = doc.SelectNodes("/EventTestSet/EventTest");
                if (testItemNodeList == null || testItemNodeList.Count == 0)
                {
                    return;
                }

                int no = 0;
                foreach (XmlNode testItemNode in testItemNodeList)
                {
                    no ++;
                    string url = XmlUtils.GetStringAttrValue(testItemNode, "url");
                    string id = XmlUtils.GetStringAttrValue(testItemNode, "id");
                    string name = XmlUtils.GetStringAttrValue(testItemNode, "name", id);
                    bool isOn = XmlUtils.GetAttrValue(testItemNode, "isOn", true);
                    string message = XmlUtils.GetStringAttrValue(testItemNode, "message");

                    if (string.IsNullOrWhiteSpace(url) || string.IsNullOrWhiteSpace(id))
                    {
                        AppendOutput($"eventTestSet[{no}] url or id is empty!");
                        continue;
                    }

                    eventTestItems.Add(new EventTestItem()
                    {
                        Name = name,
                        Url = url,
                        Id = id,
                        IsOn = isOn,
                        Message = message
                    });
                }

                _eventTestItems = eventTestItems.ToArray();
            }
            catch (Exception ex)
            {
                AppendOutput(ex.Message);
            }
        }
        private void LoadConfig()
        {
            string configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cfg.json");
            if (File.Exists(configFile))
            {
                string jsonText = File.ReadAllText(configFile, Encoding.UTF8);
                JObject configJson = JObject.Parse(jsonText);
                textBoxUrl.Text = configJson["url"]?.Value<string>() ?? "";
                textBoxEventId.Text = configJson["id"]?.Value<string>() ?? "";
                textBoxMessage.Text = configJson["message"]?.Value<string>() ?? "";
                string isOnString= configJson["isOn"]?.Value<string>() ?? "true";
                if (isOnString.ToLower() == "true")
                {
                    radioButtonOn.Checked = true;
                }
                else
                {
                    radioButtonOff.Checked = true;
                }
            }
        }

        private void SaveConfig()
        {
            JObject configJson = new JObject()
            {
                ["url"] = textBoxUrl.Text,
                ["id"] = textBoxEventId.Text,
                ["isOn"] = $"{radioButtonOn.Checked}",
                ["message"] = textBoxMessage.Text,
            };
            File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "cfg.json"), configJson.ToString(), Encoding.UTF8);
        }

        private void comboBoxTestSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = comboBoxTestSet.SelectedIndex;
            if (selectedIndex >= 0 && selectedIndex < _eventTestItems.Length)
            {
                SetTestItem(_eventTestItems[selectedIndex]);
            }
        }

        private void SetTestItem(EventTestItem testItem)
        {
            if (testItem != null)
            {
                textBoxUrl.Text = testItem.Url;
                textBoxEventId.Text = testItem.Id;
                textBoxMessage.Text = testItem.Message;
                if (testItem.IsOn)
                {
                    radioButtonOn.Checked = true;
                }
                else
                {
                    radioButtonOff.Checked = true;
                }
            }
        }
    }
}
