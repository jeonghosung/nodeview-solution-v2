﻿using NodeView.DataModels;

class Program
{
    static void Main(string[] args)
    {
        if (args.Length > 2)
        {
            try
            {
                string configFilePath = args[0];
                string propertyKey = args[1];
                string[] propertyKeys = propertyKey.Split('.');
                string value = args[2];
                string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilePath);
                Console.WriteLine($"{settingFile}");
                var configuration = Configuration.Load(settingFile);
                if (propertyKeys.Length > 1)
                {
                    if (configuration.TryGetConfig(propertyKeys[0], out Configuration centerConfig))
                    {
                        if (centerConfig.TryGetObject(propertyKeys[1], out object centerPortConfig))
                        {
                            ((Property)centerPortConfig).Value = value;
                        }
                    }
                }
                else
                {
                    configuration.SetProperty(propertyKeys[0], value);
                }
                Configuration.Save(settingFile, configuration);
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }
        }
        else
        {
            Console.WriteLine("Invalid parameter.");
        }
    }
}