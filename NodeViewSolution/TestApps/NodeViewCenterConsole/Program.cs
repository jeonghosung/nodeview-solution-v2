﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using NodeView.Apps;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Extension.Stations;
using NodeView.Frameworks.Modularity;
using NodeView.Frameworks.Stations;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.Systems;
using NodeViewCenter;
using Prism.DryIoc;
using Prism.Events;
using Prism.Ioc;

namespace NodeViewCenterConsole
{
    class Program
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        private static ModuleManager _nodeViewModuleManager = new ModuleManager();
        
        static void Main(string[] args)
        {
            CenterService service = CenterService.Instance;
            string configFilename = "NodeViewCenter.config.xml";

            OnStartup();
            var container = new DryIocContainerExtension();
            NodeViewContainerExtension.CreateInstance(container);
            container.RegisterInstance<IAuthentication>(new NodeViewAppAdminAuthentication());

            //StationService
            container.RegisterSingleton(typeof(IStationRepository), typeof(SqliteStationRepository));
            container.RegisterInstance<StationServiceManager>(service);
            container.RegisterInstance(typeof(ModuleManager), _nodeViewModuleManager);

            Logger.Info("Start ====================== ");

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var config = Configuration.Load(settingFile);
            if (config == null)
            {
                Logger.Error("Cannot load config!");
                Shutdown();
                return;
            }

            _nodeViewModuleManager.AddAssembly("NodeView.Extension.dll");
            if (config.TryGetNode("extensions", out var extensionsSection))
            {
                foreach (var extenstionProperty in extensionsSection.Properties)
                {
                    if (!_nodeViewModuleManager.AddAssembly(extenstionProperty.Value))
                    {
                        Logger.Error($"{extenstionProperty.Value}를 로드하는중에 오류가 발생했습니다.");
                        Shutdown();
                        return;
                    }
                }
            }

            RegisterTypes(container);
            OnCreatedShell(container, args);

            if (!service.Initialize(container, configFilename))
            {
                Logger.Error("service Initialize error!");
                Shutdown();
                return;
            }
            Thread.Sleep(100);
            OnInitialized(container);

            try
            {
                if (!service.Start())
                {
                    Logger.Error("service start error!");
                    Shutdown();
                    return;
                }
                while (true)
                {
                    var codeKey = Console.ReadKey().Key;
                    if (codeKey == ConsoleKey.Q)
                    {
                        service.Stop();
                        break;
                    }
                }
                Logger.Info("========================= End");
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
                service.Stop();
            }
        }

        private static void RegisterTypes(IContainerExtension containerExtension)
        {
            IContainerRegistry containerRegistry = containerExtension;

            _nodeViewModuleManager.RegisterTypes(containerRegistry);
        }

        private static void OnCreatedShell(IContainerProvider container, string[] args)
        {
            _nodeViewModuleManager.OnCreatedShell(container, args);
        }

        private static void OnInitialized(IContainerProvider container)
        {
            _nodeViewModuleManager.OnInitialized(container);
        }

        private static void Shutdown()
        {
            Console.WriteLine("종료......");
            Console.ReadKey();
            AppUtils.Exit();
        }

        static void OnStartup()
        {
            string appName = AppUtils.GetAppFileName();
            if (AppUtils.IsDuplicateExecution())
            {
                Logger.Error($"'{appName}' 프로그램이 이미 실행 중입니다.");
                AppUtils.Exit();
            }
            else
            {
                SetupUnhandledExceptionHandling();

            }
        }
        static void SetupUnhandledExceptionHandling()
        {
            // Catch exceptions from all threads in the AppDomain.
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException",
                    false);

            // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
            TaskScheduler.UnobservedTaskException += (sender, args) =>
                ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", false);
        }

        static void ShowUnhandledException(Exception e, string unhandledExceptionType, bool promptUserForShutdown)
        {
            Logger.Error(e, $"ShowUnhandledException ['{unhandledExceptionType}',{promptUserForShutdown}]");
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{e}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
