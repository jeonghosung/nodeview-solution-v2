﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DatawallTestApp.ViewModels;

namespace DatawallTestApp.Models
{
    public class PresetTestInfo
    {
        public string Ip { get; set; } = "127.0.0.1:20105";
        public int Interval { get; set; } = 10;
        public List<Preset> DataWallPresets { get; set; } = new List<Preset>();
        public List<Preset> TestPresets { get; set; } = new List<Preset>();
    }
}
