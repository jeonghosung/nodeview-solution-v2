﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;
using DatawallTestApp.Models;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Net;
using NodeView.Tasks;
using NodeView.Utils;
using Prism.Commands;
using Prism.Mvvm;

namespace DatawallTestApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private NodeViewApiClient _nodeViewApiClient = null;
        private Timer _setPresetTimer = null;
        private int _presetIndex = 0;
        private const string PresetListFileName = "PresetTestInfo.xml";

        private string _title = "Datawall Test";
        private string _ip = "127.0.0.1:20205";
        private ObservableCollection<Preset> _wallPresets = new ObservableCollection<Preset>();
        private ObservableCollection<Preset> _locationTestPresets = new ObservableCollection<Preset>();
        private Preset _selectedPreset;
        private Preset _selectedTestPreset;

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }
        private int _interval = 10;
        public int Interval
        {
            get { return _interval; }
            set { SetProperty(ref _interval, value); }
        }
        public string IP
        {
            get => _ip;
            set => SetProperty(ref _ip, value);
        }
        public ObservableCollection<Preset> WallPresets
        {
            get => _wallPresets;
            set => SetProperty(ref _wallPresets, value);
        }
        public ObservableCollection<Preset> LocationTestPresets
        {
            get => _locationTestPresets;
            set => SetProperty(ref _locationTestPresets, value);
        }

        public Preset SelectedPreset
        {
            get => _selectedPreset;
            set => SetProperty(ref _selectedPreset, value);
        }
        public Preset SelectedTestPreset
        {
            get => _selectedTestPreset;
            set => SetProperty(ref _selectedTestPreset, value);
        }
        private DelegateCommand _getPresetCommand;
        public DelegateCommand GetPresetCommand =>
            _getPresetCommand ?? (_getPresetCommand = new DelegateCommand(ExecuteGetPresetCommand));

        void ExecuteGetPresetCommand()
        {
            GetDataWallPresets();
        }
        private DelegateCommand _addPresetCommand;
        public DelegateCommand AddPresetCommand =>
            _addPresetCommand ?? (_addPresetCommand = new DelegateCommand(ExecuteAddPresetCommand));

        void ExecuteAddPresetCommand()
        {
            if (_selectedPreset != null)
            {
                LocationTestPresets.Add(_selectedPreset);
            }
        }

        private DelegateCommand _deleteSelectedTestCommand;
        public DelegateCommand DeleteSelectedTestCommand =>
            _deleteSelectedTestCommand ?? (_deleteSelectedTestCommand = new DelegateCommand(ExecuteDeleteSelectedTestCommand));

        void ExecuteDeleteSelectedTestCommand()
        {
            if (_selectedTestPreset != null)
            {
                LocationTestPresets.Remove(_selectedTestPreset);
            }
        }
        private DelegateCommand _startLocationTestCommand;
        public DelegateCommand StartLocationTestCommand =>
            _startLocationTestCommand ?? (_startLocationTestCommand = new DelegateCommand(ExecuteStartLocationTestCommand));

        void ExecuteStartLocationTestCommand()
        {
            _setPresetTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
            var currentDirectoryPath = AppDomain.CurrentDomain.BaseDirectory;
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(PresetTestInfo));
            using (StreamWriter streamWriter = new StreamWriter($"{currentDirectoryPath}/{PresetListFileName}"))
            {
                var presetlist = new PresetTestInfo() { Ip = IP, Interval = Interval,DataWallPresets = WallPresets.ToList(), TestPresets = LocationTestPresets.ToList() };
                xmlSerializer.Serialize(streamWriter, presetlist);
            }
        }
        private DelegateCommand _stopLocationTestCommand;
        public DelegateCommand StopLocationTestCommand =>
            _stopLocationTestCommand ?? (_stopLocationTestCommand = new DelegateCommand(ExecuteStopLocationTestCommand));

        void ExecuteStopLocationTestCommand()
        {
            _setPresetTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }
        private void OnSetPresetTimer(object state)
        {
            _setPresetTimer.Change(Timeout.Infinite, Timeout.Infinite);
            JObject requestJson = new JObject
            {
                ["action"] = "applyPreset",
                ["param"] = new JObject()
                {
                    ["id"] = LocationTestPresets[_presetIndex].Id
                }
            };
            _presetIndex++;
            if (_presetIndex == LocationTestPresets.Count)
            {
                _presetIndex = 0;
            }
            var response = _nodeViewApiClient.Put("/windows", requestJson);
            _setPresetTimer.Change(TimeSpan.FromSeconds(Interval), TimeSpan.FromSeconds(Interval));
        }
        private DelegateCommand _setSeledtedPresetCommand;
        public DelegateCommand SetSelectedPresetCommand =>
            _setSeledtedPresetCommand ?? (_setSeledtedPresetCommand = new DelegateCommand(ExecuteSetSelectedPresetCommand));

        void ExecuteSetSelectedPresetCommand()
        {
            JObject requestJson = new JObject
            {
                ["action"] = "applyPreset",
                ["param"] = new JObject()
                {
                    ["id"] = SelectedPreset.Id
                }
            };
            var response = _nodeViewApiClient.Put("/windows", requestJson);
        }
        public MainWindowViewModel()
        {
            Init();
        }

        private void Init()
        {
            var currentDirectoryPath = AppDomain.CurrentDomain.BaseDirectory;
            var presetListFilePath = $"{currentDirectoryPath}/{PresetListFileName}";
            _setPresetTimer = new Timer(OnSetPresetTimer);
            if (File.Exists(presetListFilePath))
            {
                using (var streamReader = new StreamReader(presetListFilePath))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(PresetTestInfo));
                    PresetTestInfo presetTestInfo = (PresetTestInfo)xmlSerializer.Deserialize(streamReader);
                    IP = presetTestInfo.Ip;
                    Interval = presetTestInfo.Interval;
                    foreach (var testPreset in presetTestInfo.TestPresets)
                    {
                        LocationTestPresets.Add(testPreset);
                    }

                    foreach (var dataWallPreset in presetTestInfo.DataWallPresets)
                    {
                        WallPresets.Add(dataWallPreset);
                    }
                    string appUri = $"http://{IP}/api/v1";
                    _nodeViewApiClient = new NodeViewApiClient(appUri);
                }
                
            }
           
        }
        private void GetDataWallPresets()
        {
            try
            {
                string appUri = $"http://{IP}/api/v1";
                _nodeViewApiClient = new NodeViewApiClient(appUri);
                var response = _nodeViewApiClient.Get("/presets");
                if (response.IsSuccess)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        List<Preset> presetList = new List<Preset>();
                        var responseArray = response.ResponseJsonArray;
                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                string id = JsonUtils.GetStringValue(json, "id");
                                string name = JsonUtils.GetStringValue(json, "name");
                                presetList.Add(new Preset(){Id = id, Name = name});
                            }
                        }
                        ThreadAction.PostOnUiThread(() =>
                        {
                            LocationTestPresets.Clear();
                            WallPresets.Clear();
                            WallPresets.AddRange(presetList);
                        });
                    }
                    else
                    {
                        Logger.Error("reponse of presets is not an array.");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetDataWallPresets:");
            }
        }
    }
    public class Preset
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
