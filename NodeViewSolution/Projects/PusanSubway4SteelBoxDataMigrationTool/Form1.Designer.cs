﻿
namespace PusanSubway4SteelBoxDataMigrationTool
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonLoadInputData = new System.Windows.Forms.Button();
            this.buttonPrintXml = new System.Windows.Forms.Button();
            this.buttonPrintDigestXml = new System.Windows.Forms.Button();
            this.buttonPrintCameraAndMonitor = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.buttonCreateMcsCameraXml = new System.Windows.Forms.Button();
            this.buttonCreateNodeViewEventAction = new System.Windows.Forms.Button();
            this.buttonPrintEventActionData = new System.Windows.Forms.Button();
            this.buttonTransMcsConfig = new System.Windows.Forms.Button();
            this.buttonPrintMigrationXml = new System.Windows.Forms.Button();
            this.buttonCreateNodeViewContents = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Location = new System.Drawing.Point(10, 189);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxOutput.Size = new System.Drawing.Size(1135, 417);
            this.textBoxOutput.TabIndex = 0;
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(1070, 160);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 1;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonLoadInputData
            // 
            this.buttonLoadInputData.Location = new System.Drawing.Point(25, 20);
            this.buttonLoadInputData.Name = "buttonLoadInputData";
            this.buttonLoadInputData.Size = new System.Drawing.Size(124, 29);
            this.buttonLoadInputData.TabIndex = 2;
            this.buttonLoadInputData.Text = "입력자료 로드";
            this.buttonLoadInputData.UseVisualStyleBackColor = true;
            this.buttonLoadInputData.Click += new System.EventHandler(this.buttonLoadInputData_Click);
            // 
            // buttonPrintXml
            // 
            this.buttonPrintXml.Location = new System.Drawing.Point(165, 20);
            this.buttonPrintXml.Name = "buttonPrintXml";
            this.buttonPrintXml.Size = new System.Drawing.Size(111, 29);
            this.buttonPrintXml.TabIndex = 3;
            this.buttonPrintXml.Text = "XML 출력";
            this.buttonPrintXml.UseVisualStyleBackColor = true;
            this.buttonPrintXml.Click += new System.EventHandler(this.buttonPrintXml_Click);
            // 
            // buttonPrintDigestXml
            // 
            this.buttonPrintDigestXml.Location = new System.Drawing.Point(291, 20);
            this.buttonPrintDigestXml.Name = "buttonPrintDigestXml";
            this.buttonPrintDigestXml.Size = new System.Drawing.Size(111, 29);
            this.buttonPrintDigestXml.TabIndex = 4;
            this.buttonPrintDigestXml.Text = "요약 XML 출력";
            this.buttonPrintDigestXml.UseVisualStyleBackColor = true;
            this.buttonPrintDigestXml.Click += new System.EventHandler(this.buttonPrintDigestXml_Click);
            // 
            // buttonPrintCameraAndMonitor
            // 
            this.buttonPrintCameraAndMonitor.Location = new System.Drawing.Point(25, 18);
            this.buttonPrintCameraAndMonitor.Name = "buttonPrintCameraAndMonitor";
            this.buttonPrintCameraAndMonitor.Size = new System.Drawing.Size(139, 29);
            this.buttonPrintCameraAndMonitor.TabIndex = 6;
            this.buttonPrintCameraAndMonitor.Text = "카메라/모니터 출력";
            this.buttonPrintCameraAndMonitor.UseVisualStyleBackColor = true;
            this.buttonPrintCameraAndMonitor.Click += new System.EventHandler(this.buttonPrintCameraAndMonitor_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonLoadInputData);
            this.groupBox1.Controls.Add(this.buttonPrintXml);
            this.groupBox1.Controls.Add(this.buttonPrintDigestXml);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(439, 59);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "From CSV";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonPrintCameraAndMonitor);
            this.groupBox2.Location = new System.Drawing.Point(12, 83);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(439, 56);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "From XML";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.buttonCreateNodeViewContents);
            this.groupBox3.Controls.Add(this.buttonCreateMcsCameraXml);
            this.groupBox3.Controls.Add(this.buttonCreateNodeViewEventAction);
            this.groupBox3.Controls.Add(this.buttonPrintEventActionData);
            this.groupBox3.Controls.Add(this.buttonTransMcsConfig);
            this.groupBox3.Controls.Add(this.buttonPrintMigrationXml);
            this.groupBox3.Location = new System.Drawing.Point(466, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(678, 126);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Migration";
            // 
            // buttonCreateMcsCameraXml
            // 
            this.buttonCreateMcsCameraXml.Location = new System.Drawing.Point(516, 20);
            this.buttonCreateMcsCameraXml.Name = "buttonCreateMcsCameraXml";
            this.buttonCreateMcsCameraXml.Size = new System.Drawing.Size(129, 29);
            this.buttonCreateMcsCameraXml.TabIndex = 9;
            this.buttonCreateMcsCameraXml.Text = "MCS camera XML";
            this.buttonCreateMcsCameraXml.UseVisualStyleBackColor = true;
            this.buttonCreateMcsCameraXml.Click += new System.EventHandler(this.buttonCreateMcsCameraXml_Click);
            // 
            // buttonCreateNodeViewEventAction
            // 
            this.buttonCreateNodeViewEventAction.Location = new System.Drawing.Point(334, 20);
            this.buttonCreateNodeViewEventAction.Name = "buttonCreateNodeViewEventAction";
            this.buttonCreateNodeViewEventAction.Size = new System.Drawing.Size(176, 29);
            this.buttonCreateNodeViewEventAction.TabIndex = 8;
            this.buttonCreateNodeViewEventAction.Text = "NodeView EventAction 생성";
            this.buttonCreateNodeViewEventAction.UseVisualStyleBackColor = true;
            this.buttonCreateNodeViewEventAction.Click += new System.EventHandler(this.buttonCreateNodeViewEventAction_Click);
            // 
            // buttonPrintEventActionData
            // 
            this.buttonPrintEventActionData.Location = new System.Drawing.Point(152, 20);
            this.buttonPrintEventActionData.Name = "buttonPrintEventActionData";
            this.buttonPrintEventActionData.Size = new System.Drawing.Size(176, 29);
            this.buttonPrintEventActionData.TabIndex = 7;
            this.buttonPrintEventActionData.Text = "EventActionData CSV 출력";
            this.buttonPrintEventActionData.UseVisualStyleBackColor = true;
            this.buttonPrintEventActionData.Click += new System.EventHandler(this.buttonPrintEventActionData_Click);
            // 
            // buttonTransMcsConfig
            // 
            this.buttonTransMcsConfig.Location = new System.Drawing.Point(532, 89);
            this.buttonTransMcsConfig.Name = "buttonTransMcsConfig";
            this.buttonTransMcsConfig.Size = new System.Drawing.Size(129, 29);
            this.buttonTransMcsConfig.TabIndex = 6;
            this.buttonTransMcsConfig.Text = "MCS Config 변환";
            this.buttonTransMcsConfig.UseVisualStyleBackColor = true;
            this.buttonTransMcsConfig.Click += new System.EventHandler(this.buttonTransMcsConfig_Click);
            // 
            // buttonPrintMigrationXml
            // 
            this.buttonPrintMigrationXml.Location = new System.Drawing.Point(17, 20);
            this.buttonPrintMigrationXml.Name = "buttonPrintMigrationXml";
            this.buttonPrintMigrationXml.Size = new System.Drawing.Size(129, 29);
            this.buttonPrintMigrationXml.TabIndex = 5;
            this.buttonPrintMigrationXml.Text = "Migration XML 출력";
            this.buttonPrintMigrationXml.UseVisualStyleBackColor = true;
            this.buttonPrintMigrationXml.Click += new System.EventHandler(this.buttonPrintMigrationXml_Click);
            // 
            // buttonCreateNodeViewContents
            // 
            this.buttonCreateNodeViewContents.Location = new System.Drawing.Point(17, 89);
            this.buttonCreateNodeViewContents.Name = "buttonCreateNodeViewContents";
            this.buttonCreateNodeViewContents.Size = new System.Drawing.Size(153, 29);
            this.buttonCreateNodeViewContents.TabIndex = 10;
            this.buttonCreateNodeViewContents.Text = "NodeViewContents 생성";
            this.buttonCreateNodeViewContents.UseVisualStyleBackColor = true;
            this.buttonCreateNodeViewContents.Click += new System.EventHandler(this.buttonCreateNodeViewContents_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1157, 618);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.textBoxOutput);
            this.Name = "Form1";
            this.Text = "SteelBoxDataMigrationTool";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonLoadInputData;
        private System.Windows.Forms.Button buttonPrintXml;
        private System.Windows.Forms.Button buttonPrintDigestXml;
        private System.Windows.Forms.Button buttonPrintCameraAndMonitor;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonPrintMigrationXml;
        private System.Windows.Forms.Button buttonTransMcsConfig;
        private System.Windows.Forms.Button buttonPrintEventActionData;
        private System.Windows.Forms.Button buttonCreateNodeViewEventAction;
        private System.Windows.Forms.Button buttonCreateMcsCameraXml;
        private System.Windows.Forms.Button buttonCreateNodeViewContents;
    }
}

