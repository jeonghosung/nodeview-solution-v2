﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.IO;
using NodeView.Utils;
using PusanSubway4SteelBoxDataMigrationTool.Models;

namespace PusanSubway4SteelBoxDataMigrationTool
{
    public partial class Form1 : Form
    {
        private TextFieldConverter _textFieldConverter = new TextFieldConverter();
        private StringBuilder _outputStringBuilder = new StringBuilder();

        private SteelBoxDataPortEx[] _dataPorts = new SteelBoxDataPortEx[0];
        private SteelBoxActionEx[] _actions = new SteelBoxActionEx[0];
        private SteelBoxEventEx[] _events = new SteelBoxEventEx[0];
        private Dictionary<string, SteelBoxActionGroup> _actionGroupDictionary = new Dictionary<string, SteelBoxActionGroup>();
        private Dictionary<string, SteelBoxActionGroup> _actionGroupNameDictionary = new Dictionary<string, SteelBoxActionGroup>();
        private Dictionary<string, string> _stationDictionary = new Dictionary<string, string>();

        private Dictionary<string, NvAction> _nvActionMapBySid = new Dictionary<string, NvAction>();
        
        Dictionary<string, CameraMappingInfo> _cameraMappingDictionaryByName = new Dictionary<string, CameraMappingInfo>();
        private Dictionary<string, string> _steelBoxCameraMap = new Dictionary<string, string>();
        public string InputFolder { get; }
        public string OutputFolder { get; }
        public Form1()
        {
            InitializeComponent();
            InputFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Inputs");
            OutputFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Outputs");
            Directory.CreateDirectory(OutputFolder);

            InitMapping();
            LoadInputs();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            _outputStringBuilder.Clear();
            UpdateOutputString();
        }

        private void AppendOutput(string message)
        {
            _outputStringBuilder.Append(message);
            UpdateOutputString();
        }
        private void AppendOutputLine(string message)
        {
            _outputStringBuilder.AppendLine(message);
            UpdateOutputString();
        }

        private void UpdateOutputString()
        {
            textBoxOutput.Text = _outputStringBuilder.ToString();
        }

        private void InitMapping()
        {
            _textFieldConverter.Register(new TextFieldMapping<SteelBoxDataPortEx>(string.Join(",", SteelBoxDataPortEx.GetHeaders())));
            _textFieldConverter.Register(new TextFieldMapping<SteelBoxActionEx>(string.Join(",", SteelBoxActionEx.GetHeaders())));
            _textFieldConverter.Register(new TextFieldMapping<SteelBoxEventEx>(string.Join(",", SteelBoxEventEx.GetHeaders())));

            _textFieldConverter.Register(new TextFieldMapping<CameraMappingInfo>("StationCode,CameraCode,Name,Connection,OsdName,WallContentName,NodeViewContentName,NodeViewContentPath,Ip,Channel,UserId,UserPassword,UsingPtz"));
            _textFieldConverter.Register(new TextFieldMapping<LeadtechCamera>("Id,Name,Osd,Url,UserId,UserPassword"));
            _textFieldConverter.Register(new TextFieldMapping<Subway4EventActionData>(string.Join(",", Subway4EventActionData.GetHeaders())));
        }
        private void LoadInputs()
        {
            try
            {
                _actionGroupDictionary.Clear();
                _actionGroupNameDictionary.Clear();

                _dataPorts = LoadSteelBoxDataArray<SteelBoxDataPortEx>(Path.Combine(InputFolder, "ResDataPort.csv"));
                _actions = LoadSteelBoxDataArray<SteelBoxActionEx>(Path.Combine(InputFolder, "ResAction.csv"));
                //var actions = LoadSteelBoxDataArray<SteelBoxActionEx>(Path.Combine(InputFolder, "ResAction.csv"));
                //List<SteelBoxActionEx> actionList = new List<SteelBoxActionEx>();
                //actionList.AddRange(actions);
                //actionList.Sort((x, y) => string.Compare(x.GroupId, y.GroupId, StringComparison.OrdinalIgnoreCase));
                //_actions = actionList.ToArray();
                
                var events = LoadSteelBoxDataArray<SteelBoxEventEx>(Path.Combine(InputFolder, "ResEvent.csv"));
                List<SteelBoxEventEx> eventList = new List<SteelBoxEventEx>();
                eventList.AddRange(events);
                eventList.Sort((x, y) => string.Compare(x.TriggerData, y.TriggerData, StringComparison.OrdinalIgnoreCase));
                _events = eventList.ToArray();

                foreach (var action in _actions)
                {
                    if (action.ActionType == "RunExternalApplication")
                    {
                        // 외부 프로그램 실행 하는듯 함 일단 Skip
                        //3420	RunExternalApplication	ARARM ON		3420	NULL	0
                        //3421    RunExternalApplication ALARM OFF       3421    NULL    0
                    }
                    else if (action.ActionType == "List" || action.ActionType == "Group")
                    {
                        if (_actionGroupDictionary.TryGetValue(action.Id, out var foundGroup))
                        {
                            AppendOutputLine($"actionGroup[{action.Id}] is already exist.");
                            continue;
                        }

                        var actionGroup = new SteelBoxActionGroup(action);
                        _actionGroupDictionary[actionGroup.Id] = actionGroup;
                        _actionGroupNameDictionary[actionGroup.Name] = actionGroup;
                    }
                    else
                    {
                        SteelBoxActionGroup actionGroup;
                        if (!_actionGroupDictionary.TryGetValue(action.ParentActionId, out actionGroup))
                        {
                            AppendOutputLine($"Cannot find actionGroup[{action.ParentActionId}] for [{action.Id}].");
                            continue;
                        }

                        if (action.ActionType == "CallAction")
                        {
                            if (action.Name.StartsWith("Call Action "))
                            {
                                string actionGroupName = action.Name.Substring(12).Trim().Trim('"');
                                if (_actionGroupNameDictionary.TryGetValue(actionGroupName, out var foundCallActionGroup))
                                {
                                    actionGroup.AddActions(action.Sequence, foundCallActionGroup);
                                }
                                else
                                {
                                    AppendOutputLine($"Cannot find call Action name [{actionGroupName}] for [{action.Id}].");
                                }
                            }
                            else
                            {
                                AppendOutputLine($"CallAction name is not start [Call Action ] for [{action.Id}].");
                            }
                        }
                        else
                        {
                            actionGroup.AddAction(action);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            AppendOutputLine($"Done LoadInputs====");
        }
        private T[] LoadSteelBoxDataArray<T>(string sourceCsvFile) where T : ISteelBoxData
        {
            Dictionary<string, T> dict = new Dictionary<string, T>();
            var rows = TextFieldReader.Convert<T>(sourceCsvFile, _textFieldConverter, 0);
            foreach (var row in rows)
            {
                if (row.Id != "NULL")
                {
                    dict[row.Id] = row;
                }
            }

            return dict.Values.ToArray();
        }

        private void buttonLoadInputData_Click(object sender, EventArgs e)
        {
            LoadInputs();
        }

        private void buttonPrintXml_Click(object sender, EventArgs args)
        {
            try
            {
                string filePath = Path.Combine(OutputFolder, "steelBoxData.xml");
                using (var xmlWriter = XmlUtils.CreateXmlWriter(filePath))
                {
                    xmlWriter.WriteStartElement("SteelBoxData");
                    foreach (var @event in _events)
                    {
                        xmlWriter.WriteStartElement("Event");
                        xmlWriter.WriteAttributeString("sid", @event.Id);
                        xmlWriter.WriteAttributeString("name", @event.Name);
                        xmlWriter.WriteAttributeString("folder", @event.Folder);
                        xmlWriter.WriteAttributeString("trigger", @event.TriggerData);

                        if (_actionGroupDictionary.TryGetValue(@event.ActionIdAutomation, out var actionGroup))
                        {
                            xmlWriter.WriteStartElement("Actions");
                            xmlWriter.WriteAttributeString("sid", actionGroup.Id);
                            xmlWriter.WriteAttributeString("name", actionGroup.Name);
                            xmlWriter.WriteAttributeString("folder", actionGroup.Folder);
                            xmlWriter.WriteAttributeString("parameter", actionGroup.Parameter);
                            foreach (var action in actionGroup.Actions)
                            {
                                xmlWriter.WriteStartElement("Action");
                                xmlWriter.WriteAttributeString("sid", action.Id);
                                xmlWriter.WriteAttributeString("sequence", action.Sequence);
                                xmlWriter.WriteAttributeString("actionType", action.ActionType);
                                xmlWriter.WriteAttributeString("name", action.Name);
                                xmlWriter.WriteAttributeString("itemType1", action.ItemType1);
                                xmlWriter.WriteAttributeString("itemType2", action.ItemType2);

                                if (!string.IsNullOrWhiteSpace(action.SendData))
                                {
                                    xmlWriter.WriteAttributeString("sendData", action.SendData);
                                    xmlWriter.WriteAttributeString("dataPortName", action.DataPortName);
                                    xmlWriter.WriteAttributeString("profileName", action.ProfileName);
                                    xmlWriter.WriteAttributeString("profileType", action.ProfileType);
                                }
                                xmlWriter.WriteAttributeString("parameter", action.Parameter);

                                xmlWriter.WriteAttributeString("description", action.Description);

                                xmlWriter.WriteEndElement();
                            }
                            xmlWriter.WriteEndElement();
                        }
                        else
                        {
                            AppendOutputLine($"Cannot find actionGroup[{@event.ActionIdAutomation}] for event [{@event.Name}].");
                        }

                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            AppendOutputLine($"Done PrintXml====");
        }

        private void buttonPrintDigestXml_Click(object sender, EventArgs args)
        {
            try
            {
                string filePath = Path.Combine(OutputFolder, "steelBoxDataDigest.xml");
                using (var xmlWriter = XmlUtils.CreateXmlWriter(filePath))
                {
                    xmlWriter.WriteStartElement("SteelBoxData");
                    foreach (var @event in _events)
                    {
                        xmlWriter.WriteStartElement("Event");
                        xmlWriter.WriteAttributeString("sid", @event.Id);
                        xmlWriter.WriteAttributeString("name", @event.Name);
                        xmlWriter.WriteAttributeString("folder", @event.Folder);
                        xmlWriter.WriteAttributeString("trigger", @event.TriggerData);

                        if (_actionGroupDictionary.TryGetValue(@event.ActionIdAutomation, out var actionGroup))
                        {
                            xmlWriter.WriteStartElement("Actions");
                            xmlWriter.WriteAttributeString("sid", actionGroup.Id);
                            xmlWriter.WriteAttributeString("name", actionGroup.Name);
                            xmlWriter.WriteAttributeString("folder", actionGroup.Folder);

                            foreach (var action in actionGroup.Actions)
                            {
                                if (action.ActionType == "DelayInMs")
                                {
                                    continue;
                                }

                                xmlWriter.WriteStartElement("Action");
                                xmlWriter.WriteAttributeString("sid", action.Id);
                                xmlWriter.WriteAttributeString("sequence", action.Sequence);
                                xmlWriter.WriteAttributeString("actionType", action.ActionType);
                                string name = action.Name;
                                if (name.StartsWith("Data Port Send415 OCC Centauri"))
                                {
                                    name = name.Substring(30);
                                }

                                if (name.EndsWith(" - Data"))
                                {
                                    name = name.Substring(0, name.Length - 7);
                                }

                                xmlWriter.WriteAttributeString("name", name);

                                string itemType1 = action.ItemType1;
                                if ((action.ActionType == "DataPortSend" && itemType1 == "Data Port") ||
                                    (action.ActionType == "VideoConnect"))
                                {
                                    itemType1 = "";
                                }

                                if (!string.IsNullOrWhiteSpace(itemType1))
                                {
                                    xmlWriter.WriteAttributeString("itemType1", itemType1);
                                }

                                string itemType2 = action.ItemType2;
                                if ((action.ActionType == "VideoConnect"))
                                {
                                    itemType2 = "";
                                }

                                if (!string.IsNullOrWhiteSpace(itemType2))
                                {
                                    xmlWriter.WriteAttributeString("itemType2", itemType2);
                                }

                                if (action.Parameter.StartsWith("ActionTelemetryPreset:"))
                                {
                                    var splitParam = StringUtils.Split(action.Parameter, ',');
                                    foreach (var nameValue in splitParam)
                                    {
                                        var splitNameValue = StringUtils.Split(nameValue, '=', 2);
                                        if (splitNameValue[0] == "Preset")
                                        {
                                            action.SendData = splitNameValue[1];
                                        }
                                    }
                                }

                                if (!string.IsNullOrWhiteSpace(action.SendData))
                                {
                                    xmlWriter.WriteAttributeString("sendData", action.SendData);
                                }

                                if (!string.IsNullOrWhiteSpace(action.Description))
                                {
                                    xmlWriter.WriteAttributeString("description", action.Description);
                                }

                                if (!string.IsNullOrWhiteSpace(action.SendData))
                                {
                                    xmlWriter.WriteAttributeString("profileName", action.ProfileName);
                                }

                                xmlWriter.WriteEndElement();
                            }

                            xmlWriter.WriteEndElement();
                        }
                        else
                        {
                            AppendOutputLine(
                                $"Cannot find actionGroup[{@event.ActionIdAutomation}] for event [{@event.Name}].");
                        }

                        xmlWriter.WriteEndElement();
                    }

                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            AppendOutputLine($"Done PrintDigestXml====");
        }
        
        private void buttonPrintCameraAndMonitor_Click(object sender, EventArgs args)
        {
            try
            {
                LoadStationDictionary();
                Dictionary<string, CameraMappingInfo> cameraMappingInfoDictionary = GetCameraMappingInfoDictionary();


                Dictionary<string, string> cameraDictionary = new Dictionary<string, string>();
                Dictionary<string, string> monitorDictionary = new Dictionary<string, string>();

                string inputXmlPath = Path.Combine(InputFolder, "steelBoxDataDigest.xml");
                if (!File.Exists(inputXmlPath))
                {
                    if (!File.Exists(inputXmlPath))
                    {
                        AppendOutputLine($"Cannot find '{inputXmlPath}' file.");
                        return;
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(inputXmlPath);
                XmlNodeList actionNodeList = doc.SelectNodes("//Action[@actionType='VideoConnect']");
                if (actionNodeList != null)
                {
                    foreach (XmlNode actionNode in actionNodeList)
                    {
                        string actionId = XmlUtils.GetStringAttrValue(actionNode, "sid");

                        string[] splitName = StringUtils.Split(XmlUtils.GetStringAttrValue(actionNode, "name"), ';', 2);
                        if (!string.IsNullOrWhiteSpace(splitName[0]))
                        {
                            string camera = splitName[0].Substring(2);
                            cameraDictionary[camera] = camera;
                        }
                        else
                        {
                            AppendOutputLine($"camera is empty on [{actionId}]");
                        }

                        if (!string.IsNullOrWhiteSpace(splitName[1]))
                        {
                            string monitor = splitName[1].Substring(2);
                            monitorDictionary[monitor] = monitor;
                        }
                        else
                        {
                            AppendOutputLine($"monitor is empty on [{actionId}]");
                        }
                    }

                    string cameraFilePath = Path.Combine(OutputFolder, "steelBoxCamera.xml");
                    var cameraList = cameraDictionary.Values.ToList();
                    cameraList.Sort((x, y) => string.Compare(x, y, StringComparison.OrdinalIgnoreCase));

                    using (var xmlWriter = XmlUtils.CreateXmlWriter(cameraFilePath))
                    {
                        xmlWriter.WriteStartElement("Cameras");
                        foreach (var name in cameraList)
                        {
                            xmlWriter.WriteStartElement("Camera");
                            xmlWriter.WriteAttributeString("steelBoxName", name);
                            string code = name.Substring(1, 2) + name.Substring(4, 2);
                            string cameraName = "";
                            if (cameraMappingInfoDictionary.TryGetValue(code, out var cameraMapping))
                            {
                                cameraName = cameraMapping.Name;
                            }
                            xmlWriter.WriteAttributeString("name", cameraName);
                            string stationId = name.Substring(1, 2);
                            xmlWriter.WriteAttributeString("stationId", stationId);
                            if (_stationDictionary.TryGetValue(stationId, out var station))
                            {
                                xmlWriter.WriteAttributeString("stationName", station);
                            }
                            //xmlWriter.WriteAttributeString("cameraCode", name.Substring(1, 2) + name.Substring(4, 2));
                            //xmlWriter.WriteAttributeString("cameraNamePart", name.Substring(7));
                            xmlWriter.WriteEndElement();
                        }

                        xmlWriter.WriteEndElement();
                        xmlWriter.Close();
                    }


                    string monitorFilePath = Path.Combine(OutputFolder, "steelBoxMonitor.xml");
                    var monitorList = monitorDictionary.Values.ToList();
                    monitorList.Sort((x, y) => string.Compare(x, y, StringComparison.OrdinalIgnoreCase));

                    using (var xmlWriter = XmlUtils.CreateXmlWriter(monitorFilePath))
                    {
                        xmlWriter.WriteStartElement("Monitors");
                        foreach (var name in monitorList)
                        {
                            xmlWriter.WriteStartElement("Monitor");
                            xmlWriter.WriteAttributeString("steelBoxName", name);
                            string dataWallId = "";
                            if (name.StartsWith("BTC4"))
                            {
                                string[] splitName = StringUtils.Split(name, ':');
                                string stationCode = splitName[0].Substring(6, 2);
                                string monitorNo = splitName[splitName.Length - 2];
                                dataWallId = $"monitor{stationCode}{monitorNo}";
                            }
                            else
                            {
                                switch (name)
                                {
                                    case "설비사령 LED 4-1":
                                        dataWallId = $"monitor1504";
                                        break;
                                    case "운전사령#1 LED 1-1":
                                        dataWallId = $"monitor1501";
                                        break;
                                    case "운전사령#2 LED 2-1":
                                        dataWallId = $"monitor1502";
                                        break;
                                    case "전력사령 LED 3-1":
                                        dataWallId = $"monitor1503";
                                        break;
                                }
                            }
                            xmlWriter.WriteAttributeString("dataWallId", dataWallId);
                            xmlWriter.WriteEndElement();
                        }

                        xmlWriter.WriteEndElement();
                        xmlWriter.Close();
                    }
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
                return;
            }

            AppendOutputLine($"Done PrintCameraAndMonitor====");
        }

        private void LoadCameraMap()
        {
            try
            {
                _cameraMappingDictionaryByName.Clear();
                string sourceCsvFile = Path.Combine(InputFolder, "cameraMappingBaseInfo.csv");
                var cameraMappings = TextFieldReader.Convert<CameraMappingInfo>(sourceCsvFile, _textFieldConverter, 0);
                foreach (var cameraMapping in cameraMappings)
                {
                    if (string.IsNullOrWhiteSpace(cameraMapping.Code) || string.IsNullOrWhiteSpace(cameraMapping.Name))
                    {
                        continue;
                    }
                    _cameraMappingDictionaryByName[cameraMapping.Name] = cameraMapping;
                }

                _steelBoxCameraMap.Clear();
                XmlDocument doc = new XmlDocument();
                doc.Load(Path.Combine(InputFolder, "steelBoxCameraMap.xml"));
                XmlNodeList nodeList = doc.SelectNodes("//Door");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        string code = XmlUtils.GetStringAttrValue(node, "steelBoxName");
                        string name = XmlUtils.GetStringAttrValue(node, "name");
                        if (!string.IsNullOrWhiteSpace(code) && !string.IsNullOrWhiteSpace(name))
                        {
                            _steelBoxCameraMap[code] = name;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
        }

        private Dictionary<string, CameraMappingInfo> GetCameraMappingInfoDictionary()
        {
            Dictionary<string, CameraMappingInfo> cameraMappingInfoDictionary = new Dictionary<string, CameraMappingInfo>();

            try
            {
                string sourceCsvFile = Path.Combine(InputFolder, "cameraMappingBaseInfo.csv");
                var cameraMappings = TextFieldReader.Convert<CameraMappingInfo>(sourceCsvFile, _textFieldConverter, 0);
                foreach (var cameraMapping in cameraMappings)
                {
                    if (string.IsNullOrWhiteSpace(cameraMapping.Code) || string.IsNullOrWhiteSpace(cameraMapping.Name))
                    {
                        continue;
                    }

                    cameraMappingInfoDictionary[cameraMapping.Code] = cameraMapping;
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            return cameraMappingInfoDictionary;
        }

        private Dictionary<string, string> GetDoorDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            try
            {
                string mappingFile = Path.Combine(InputFolder, "doorMapping.xml");
                if (!File.Exists(mappingFile))
                {
                    if (!File.Exists(mappingFile))
                    {
                        AppendOutputLine($"Cannot find '{mappingFile}' file.");
                        return dictionary;
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(mappingFile);
                XmlNodeList nodeList = doc.SelectNodes("//Door");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        string code = XmlUtils.GetStringAttrValue(node, "code");
                        string name = XmlUtils.GetStringAttrValue(node, "name");
                        if (!string.IsNullOrWhiteSpace(code) && !string.IsNullOrWhiteSpace(name))
                        {
                            dictionary[code] = name;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            return dictionary;
        }
        
        private Dictionary<string, string> GetInterphoneDictionary()
        {
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            try
            {
                string mappingFile = Path.Combine(InputFolder, "interphoneMapping.xml");
                if (!File.Exists(mappingFile))
                {
                    if (!File.Exists(mappingFile))
                    {
                        AppendOutputLine($"Cannot find '{mappingFile}' file.");
                        return dictionary;
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(mappingFile);
                XmlNodeList nodeList = doc.SelectNodes("//Interphone");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        string code = XmlUtils.GetStringAttrValue(node, "code");
                        string name = XmlUtils.GetStringAttrValue(node, "name");
                        if (!string.IsNullOrWhiteSpace(code) && !string.IsNullOrWhiteSpace(name))
                        {
                            dictionary[code] = name;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            return dictionary;
        }

        private void LoadStationDictionary()
        {
            _stationDictionary.Clear();

            try
            {
                string stationXmlPath = Path.Combine(InputFolder, "stations.xml");
                if (!File.Exists(stationXmlPath))
                {
                    if (!File.Exists(stationXmlPath))
                    {
                        AppendOutputLine($"Cannot find '{stationXmlPath}' file.");
                        return;
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(stationXmlPath);
                XmlNodeList nodeList = doc.SelectNodes("//Station");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        string id = XmlUtils.GetStringAttrValue(node, "id");
                        string name = XmlUtils.GetStringAttrValue(node, "name");
                        if (!string.IsNullOrWhiteSpace(id))
                        {
                            _stationDictionary[id] = name;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
        }

        private void buttonPrintMigrationXml_Click(object sender, EventArgs e)
        {
            LoadStationDictionary();
            LoadCameraMap();
            Dictionary<string, CameraMappingInfo> cameraMappingInfoDictionary = GetCameraMappingInfoDictionary();
            Dictionary<string, string> doorDictionary = GetDoorDictionary();
            Dictionary<string, string> interphoneDictionary = GetInterphoneDictionary();

            string inputXmlPath = Path.Combine(InputFolder, "steelBoxDataDigest.xml");
            if (!File.Exists(inputXmlPath))
            {
                if (!File.Exists(inputXmlPath))
                {
                    AppendOutputLine($"Cannot find '{inputXmlPath}' file.");
                    return;
                }
            }

            Dictionary<string, NvEvent> eventDictionary = new Dictionary<string, NvEvent>();

            XmlDocument doc = new XmlDocument();
            doc.Load(inputXmlPath);
            XmlNodeList eventNodeList = doc.SelectNodes("/SteelBoxData/Event");
            if (eventNodeList != null)
            {
                foreach (XmlNode eventNode in eventNodeList)
                {
                    string eventSid = XmlUtils.GetStringAttrValue(eventNode, "sid");
                    string trigger = XmlUtils.GetStringAttrValue(eventNode, "trigger", "").Trim('\'');
                    if (trigger.Contains("FIRE") && trigger.Length == 11)
                    {
                        //역사화재
                        string stationId = trigger.Substring(1, 2);
                        string point = trigger.Substring(7, 3);
                        bool isAlarmOn = trigger.EndsWith("1");

                        //stationFireAlarm402 + (수신기번호:001)
                        string eventId = $"stationFireAlarm4{stationId}{point}";

                        NvEvent nvEvent = null;
                        if (!eventDictionary.TryGetValue(eventId, out nvEvent))
                        {
                            string stationName = GetStationName(stationId);
                            string description = $"{stationName}({stationId}) {point}지점 화재";
                            if (point == "000")
                            {
                                description = $"{stationName}({stationId}) 화재 종료";
                            }
                            nvEvent = new NvEvent(eventId, description);
                            eventDictionary[nvEvent.Id] = nvEvent;
                        }
                        nvEvent.TriggerList.Add(trigger);
                        nvEvent.SidList.Add(eventSid);

                        string onOffString = isAlarmOn ? "On" : "Off";
                        string actionId = $"stationFire{onOffString}Action4{stationId}{point}";
                        SetAction(nvEvent, actionId, eventNode, isAlarmOn);
                    }
                    else if (trigger.StartsWith("S") && trigger.EndsWith("E") && trigger.Length == 8)
                    {
                        //출통 이벤트
                        string code = trigger.Substring(1, 5);
                        bool isOpen = trigger.EndsWith("1E");

                        //doorSensorEvent4 + (출입문코드:01001)
                        string eventId = $"doorSensorEvent4{code}";

                        NvEvent nvEvent = null;
                        if (!eventDictionary.TryGetValue(eventId, out nvEvent))
                        {
                            string description = $"출입문 ({code}) 이벤트";
                            if (doorDictionary.TryGetValue(code, out var name))
                            {
                                description = $"{name} 이벤트";
                            }
                            nvEvent = new NvEvent(eventId, description);
                            nvEvent.Level = AlarmEventLevel.Warn;
                            eventDictionary[nvEvent.Id] = nvEvent;
                        }
                        nvEvent.TriggerList.Add(trigger);
                        nvEvent.SidList.Add(eventSid);

                        string onOffString = isOpen ? "Open" : "Close";
                        string actionId = $"doorSensor{onOffString}Action4{code}";
                        SetAction(nvEvent, actionId, eventNode, isOpen);
                    }
                    else if (trigger.StartsWith("{8") && trigger.EndsWith("}") && trigger.Length == 20)
                    {
                        if (!(trigger.StartsWith("{81") || trigger.StartsWith("{82")))
                        {
                            AppendOutputLine($"trigger [{trigger}] is not supported.");
                            continue;
                        }
                        //인터폰 이벤트
                        bool isOn = trigger.StartsWith("{81");
                        string code = trigger.Substring(trigger.Length-5, 4);

                        //interphoneEvent4 + (인터폰코드:01001)
                        string eventId = $"interphoneEvent4{code}";

                        NvEvent nvEvent = null;
                        if (!eventDictionary.TryGetValue(eventId, out nvEvent))
                        {
                            string description = $"인터폰 ({code}) 이벤트";
                            if (interphoneDictionary.TryGetValue(code, out var name))
                            {
                                description = $"{name} 이벤트";
                            }
                            nvEvent = new NvEvent(eventId, description);
                            nvEvent.Level = AlarmEventLevel.Warn;
                            eventDictionary[nvEvent.Id] = nvEvent;
                        }
                        nvEvent.TriggerList.Add(trigger);
                        nvEvent.SidList.Add(eventSid);

                        string onOffString = isOn ? "On" : "Off";
                        string actionId = $"doorSensor{onOffString}Action4{code}";
                        SetAction(nvEvent, actionId, eventNode, isOn);
                    }
                    else
                    {
                        AppendOutputLine($"trigger [{trigger}] is not supported.");
                    }
                }
            }

            string migrationXmlFilePath = Path.Combine(OutputFolder, "migration.xml");
            var eventList = eventDictionary.Values.ToList();
            eventList.Sort((x, y) => string.Compare(x.Id, y.Id, StringComparison.OrdinalIgnoreCase));

            using (var xmlWriter = XmlUtils.CreateXmlWriter(migrationXmlFilePath))
            {
                xmlWriter.WriteStartElement("Events");
                foreach (var @event in eventList)
                {
                    @event.WriteXml(xmlWriter, "Event");
                }

                xmlWriter.WriteEndElement();

                xmlWriter.Close();
            }
            
            AppendOutputLine($"Done PrintMigrationXml====");
        }

        private void SetAction(NvEvent nvEvent, string actionId, XmlNode eventNode, bool isAlarmOn)
        {
            var action = CreateNodeViewAction(actionId, eventNode.FirstChild);
            if (action != null)
            {
                if (isAlarmOn)
                {
                    if (nvEvent.OnAction != null)
                    {
                        if (!nvEvent.OnAction.SidList.Contains(action.SidList.First()))
                        {
                            nvEvent.OnAction.SidList.AddRange(action.SidList);
                            AppendOutputLine($"Already exist OnAction of {nvEvent.Id}.");
                        }
                    }
                    else
                    {
                        nvEvent.OnAction = action;
                    }
                }
                else
                {
                    if (nvEvent.OffAction != null)
                    {
                        if (!nvEvent.OffAction.SidList.Contains(action.SidList.First()))
                        {
                            nvEvent.OffAction.SidList.AddRange(action.SidList);
                            AppendOutputLine($"Already exist OffAction of {nvEvent.Id}.");
                        }
                    }
                    else
                    {
                        nvEvent.OffAction = action;
                    }
                }
            }
        }

        private NvAction CreateNodeViewAction(string actionId, XmlNode actionsNode)
        {
            if (actionsNode == null || !actionsNode.HasChildNodes || actionsNode.ChildNodes.Count == 0) return null;

            string sid = XmlUtils.GetStringAttrValue(actionsNode, "sid");
            if (_nvActionMapBySid.TryGetValue(sid, out var foundAction))
            {
                return foundAction;
            }

            NvAction action = new NvAction(actionId, XmlUtils.GetStringAttrValue(actionsNode, "name"));
            action.SidList.Add(sid);
            //powerController, encoderController, mcsPopupController

            XmlNodeList commandNodeList = actionsNode.SelectNodes("Action");
            if(commandNodeList != null)
            {
                foreach (XmlNode commandNode in commandNodeList)
                {
                    string appid = "";
                    string deviceId = "";
                    string commandId = "";


                    string steelboxActionType = XmlUtils.GetStringAttrValue(commandNode, "actionType");
                    var parameters = new KeyValueCollection();
                    switch (steelboxActionType)
                    {
                        case "DataPortSend":
                            deviceId = "steelbox";
                            commandId = steelboxActionType;

                            parameters["sendData"] = XmlUtils.GetStringAttrValue(commandNode, "sendData");
                            parameters["profile"] = XmlUtils.GetStringAttrValue(commandNode, "profileName");
                            parameters["desc"] = XmlUtils.GetStringAttrValue(commandNode, "description");
                            break;

                        case "TelemetryPreset":
                            string steelboxName = XmlUtils.GetStringAttrValue(commandNode, "name");

                            if (steelboxName.StartsWith("Preset "))
                            {
                                appid = "center";
                                deviceId = "encoderController";
                                commandId = "applyPreset";

                                string steelboxCameraName = steelboxName.Substring(7).Trim();
                                if(!_steelBoxCameraMap.TryGetValue(steelboxCameraName, out var cameraName))
                                {
                                    cameraName = steelboxCameraName;
                                }

                                if (_cameraMappingDictionaryByName.TryGetValue(cameraName, out var cameraMapping))
                                {
                                    parameters["ip"] = cameraMapping.Ip;
                                    parameters["channel"] = cameraMapping.Channel;
                                    parameters["id"] = XmlUtils.GetStringAttrValue(commandNode, "sendData");
                                }
                                else
                                {
                                    parameters["steelboxCameraName"] = steelboxCameraName;
                                    parameters["ip"] = XmlUtils.GetStringAttrValue(commandNode, "sendData");
                                    parameters["channel"] = XmlUtils.GetStringAttrValue(commandNode, "profileName");
                                    parameters["id"] = XmlUtils.GetStringAttrValue(commandNode, "sendData");
                                }
                            }
                            else
                            {
                                deviceId = "steelbox";
                                commandId = steelboxActionType;
                                parameters["name"] = XmlUtils.GetStringAttrValue(commandNode, "name");
                                parameters["sendData"] = XmlUtils.GetStringAttrValue(commandNode, "sendData");
                            }
                            break;

                        default:
                            deviceId = "steelbox";
                            commandId = steelboxActionType;
                            foreach (XmlAttribute attribute in commandNode.Attributes)
                            {
                                switch (attribute.Name)
                                {
                                    case "sid":
                                    case "sequence":
                                    case "actionType":
                                        break;
                                    default:
                                        parameters[attribute.Name] = attribute.Value;
                                        break;
                                }
                            }
                            break;
                    }

                    NvCommand command = new NvCommand(appid, deviceId, commandId, parameters);
                    action.Commands.Add(command);
                }
            }

            _nvActionMapBySid[sid] = action;
            return action;
        }

        private string GetStationName(string stationId)
        {
            if (_stationDictionary.TryGetValue(stationId, out string name))
            {
                return name;
            }

            return stationId;
        }


        private void buttonPrintEventActionData_Click(object sender, EventArgs e)
        {
            LoadStationDictionary();
            LoadCameraMap();
            Dictionary<string, CameraMappingInfo> cameraMappingInfoDictionary = GetCameraMappingInfoDictionary();
            Dictionary<string, string> doorDictionary = GetDoorDictionary();
            Dictionary<string, string> interphoneDictionary = GetInterphoneDictionary();

            string inputXmlPath = Path.Combine(InputFolder, "steelBoxDataDigest.xml");
            if (!File.Exists(inputXmlPath))
            {
                if (!File.Exists(inputXmlPath))
                {
                    AppendOutputLine($"Cannot find '{inputXmlPath}' file.");
                    return;
                }
            }

            Dictionary<string, Subway4EventActionData> eventDictionary = new Dictionary<string, Subway4EventActionData>();
            Dictionary<string, Subway4EventActionData> eventByTriggerDictionary = new Dictionary<string, Subway4EventActionData>();

            XmlDocument doc = new XmlDocument();
            doc.Load(inputXmlPath);
            XmlNodeList eventNodeList = doc.SelectNodes("/SteelBoxData/Event");
            if (eventNodeList != null)
            {
                foreach (XmlNode eventNode in eventNodeList)
                {
                    string eventName = XmlUtils.GetStringAttrValue(eventNode, "name");
                    string eventSid = XmlUtils.GetStringAttrValue(eventNode, "sid");
                    string trigger = XmlUtils.GetStringAttrValue(eventNode, "trigger", "").Trim('\'');
                    if (trigger.Contains("FIRE") && trigger.Length == 11)
                    {
                        //역사화재
                        string stationId = trigger.Substring(1, 2);
                        string point = trigger.Substring(7, 3);
                        bool isAlarmOn = trigger.EndsWith("1");
                        var eventActionData = new Subway4EventActionData()
                        {
                            Sid = eventSid,
                            Trigger = trigger,
                            StationId = stationId,
                            Point = point,
                            Name = eventName,
                            IsOn = isAlarmOn ? "On" : "Off",
                            AlarmType = "fire"
                        };

                        SetActionData(eventActionData, eventNode.FirstChild);
                        eventDictionary[eventSid] = eventActionData;
                        if (eventByTriggerDictionary.TryGetValue(trigger, out var foundEvent))
                        {
                            foundEvent.Note += "[중복]";
                            eventActionData.Note += "[중복]";
                        }
                        else
                        {
                            eventByTriggerDictionary[trigger] = eventActionData;
                        }
                    }
                    else if (trigger.StartsWith("S") && trigger.EndsWith("E") && trigger.Length == 8)
                    {
                        //출통 이벤트
                        string code = trigger.Substring(1, 5);
                        string stationId = code.Substring(0, 2);
                        string point = code.Substring(2);
                        bool isOpen = trigger.EndsWith("1E");

                        var eventActionData = new Subway4EventActionData()
                        {
                            Sid = eventSid,
                            Trigger = trigger,
                            StationId = stationId,
                            Point = point,
                            Name = eventName,
                            IsOn = isOpen ? "On" : "Off",
                            AlarmType = "fire"
                        };

                        SetActionData(eventActionData, eventNode.FirstChild);
                        eventDictionary[eventSid] = eventActionData;
                        if (eventByTriggerDictionary.TryGetValue(trigger, out var foundEvent))
                        {
                            foundEvent.Note += "[중복]";
                            eventActionData.Note += "[중복]";
                        }
                        else
                        {
                            eventByTriggerDictionary[trigger] = eventActionData;
                        }
                    }
                    else if (trigger.StartsWith("{8") && trigger.EndsWith("}") && trigger.Length == 20)
                    {
                        if (!(trigger.StartsWith("{81") || trigger.StartsWith("{82")))
                        {
                            AppendOutputLine($"trigger [{trigger}] is not supported.");
                            continue;
                        }
                        //인터폰 이벤트
                        bool isOn = trigger.StartsWith("{81");
                        string code = trigger.Substring(trigger.Length - 5, 4);
                        string stationId = code.Substring(0, 2);
                        string point = code.Substring(2);

                        var eventActionData = new Subway4EventActionData()
                        {
                            Sid = eventSid,
                            Trigger = trigger,
                            StationId = stationId,
                            Point = point,
                            Name = eventName,
                            IsOn = isOn ? "On" : "Off",
                            AlarmType = "fire"
                        };

                        SetActionData(eventActionData, eventNode.FirstChild);
                        eventDictionary[eventSid] = eventActionData;
                        if (eventByTriggerDictionary.TryGetValue(trigger, out var foundEvent))
                        {
                            foundEvent.Note += "[중복]";
                            eventActionData.Note += "[중복]";
                        }
                        else
                        {
                            eventByTriggerDictionary[trigger] = eventActionData;
                        }
                    }
                    else
                    {
                        AppendOutputLine($"trigger [{trigger}] is not supported.");
                    }
                }
            }

            string eventActionDataFilePath = Path.Combine(OutputFolder, "eventActionData.csv");
            var eventList = eventDictionary.Values.ToList();
            eventList.Sort((x,y)=>string.Compare(x.Trigger, y.Trigger, StringComparison.OrdinalIgnoreCase));
            SaveSteelBoxDto(eventList.ToArray(), eventActionDataFilePath);
            AppendOutputLine($"Done PrintEventActionData====");
        }

        private void SetActionData(Subway4EventActionData eventActionData, XmlNode actionsNode)
        {
            if (actionsNode == null || !actionsNode.HasChildNodes || actionsNode.ChildNodes.Count == 0) return;

            eventActionData.Description = XmlUtils.GetStringAttrValue(actionsNode, "name");

            XmlNodeList commandNodeList = actionsNode.SelectNodes("Action");
            if (commandNodeList != null)
            {
                Dictionary<string, string> usingCameraDictionary = new Dictionary<string, string>();
                foreach (XmlNode commandNode in commandNodeList)
                {
                    string steelboxActionType = XmlUtils.GetStringAttrValue(commandNode, "actionType");
                    string steelboxName = XmlUtils.GetStringAttrValue(commandNode, "name");
                    string sendData = XmlUtils.GetStringAttrValue(commandNode, "sendData");

                    switch (steelboxActionType)
                    {
                        case "DataPortSend":
                            /*
                            parameters["sendData"] = XmlUtils.GetStringAttrValue(commandNode, "sendData");
                            parameters["profile"] = XmlUtils.GetStringAttrValue(commandNode, "profileName");
                            parameters["desc"] = XmlUtils.GetStringAttrValue(commandNode, "description");
                            */
                            break;

                        case "TelemetryPreset":
                            if (steelboxName.StartsWith("Preset "))
                            {
                                string steelboxCameraName = steelboxName.Substring(7).Trim();
                                if (!_steelBoxCameraMap.TryGetValue(steelboxCameraName, out var cameraName))
                                {
                                    cameraName = steelboxCameraName;
                                }

                                usingCameraDictionary[cameraName] = cameraName;
                                eventActionData.Preset = sendData;
                            }
                            else
                            {
                                eventActionData.Note = $"{steelboxName}:{sendData}";
                            }
                            break;
                        case "VideoConnect":
                            string[] items = StringUtils.Split(steelboxName, ';');
                            foreach (var item in items)
                            {
                                if (item.StartsWith("C:")) //카메라
                                {
                                    string steelboxCameraName = item.Substring(2).Trim();
                                    if (!_steelBoxCameraMap.TryGetValue(steelboxCameraName, out var cameraName))
                                    {
                                        cameraName = steelboxCameraName;
                                    }

                                    usingCameraDictionary[cameraName] = cameraName;
                                }
                                else if (item.StartsWith("M:Cube"))
                                {
                                    eventActionData.McsPopup = "Y";
                                }
                                else if (item.StartsWith("M:운전사령#1"))
                                {
                                    eventActionData.TrainService1Popup = "Y";
                                }
                                else if (item.StartsWith("M:운전사령#2"))
                                {
                                    eventActionData.TrainService2Popup = "Y";
                                }
                                else if (item.StartsWith("M:설비사령"))
                                {
                                    eventActionData.FacilityService1Popup = "Y";
                                }
                                else if (item.StartsWith("M:전력사령"))
                                {
                                    eventActionData.PowerServicePopup = "Y";
                                }
                                else if (item.StartsWith("M:BTC4"))
                                {
                                    //skip 각 역사 모니터
                                }
                                else
                                {
                                    eventActionData.Name += $",{item}";
                                }
                            }
                            break;
                        case "VideoDisconnect":
                            break;
                        default:
                            eventActionData.Note = steelboxActionType;
                            break;
                    }

                }

                eventActionData.Camera = string.Join(",", usingCameraDictionary.Values);
            }
        }

        private void SaveSteelBoxDto<T>(T[] items, string targetCsvFile) where T : ISteelBoxDto
        {
            TextFieldWriter writer = null;
            bool isFirst = true;
            foreach (T item in items)
            {
                if (isFirst)
                {
                    writer = new TextFieldWriter(targetCsvFile);
                    writer.WriteFields(item.Headers);
                    isFirst = false;
                }

                string[] row = _textFieldConverter.ToFields<T>(item);
                writer.WriteFields(row);
            }
            writer?.Close();
        }


        private void buttonCreateNodeViewEventAction_Click(object sender, EventArgs e)
        {
            Dictionary<string, EventAction> nodeViewEventActionDictionary = new Dictionary<string, EventAction>();
            Dictionary<string, NodeViewAction> nodeViewActionDictionary = new Dictionary<string, NodeViewAction>();

            Dictionary<string, List<EventAction>> stationEventActionDictionary = new Dictionary<string, List<EventAction>>();
            Dictionary<string, List<NodeViewAction>> stationActionDictionary = new Dictionary<string, List<NodeViewAction>>();

            List<Subway4EventActionData> subway4EventActionList = new List<Subway4EventActionData>();
            Dictionary<string, CameraMappingInfo> cameraMappingByNameDictionary = new Dictionary<string, CameraMappingInfo>();

            string eventActionDataCsv = Path.Combine(InputFolder, "eventActionData.csv");
            var subway4EventActions = TextFieldReader.Convert<Subway4EventActionData>(eventActionDataCsv, _textFieldConverter, 1);
            subway4EventActionList.AddRange(subway4EventActions);
            subway4EventActionList.Sort((x,y)=>string.Compare(x.Trigger, y.Trigger, StringComparison.OrdinalIgnoreCase));

            string cameraMappingCsv = Path.Combine(InputFolder, "cameraMappingBaseInfo.csv");
            var cameraMappings = TextFieldReader.Convert<CameraMappingInfo>(cameraMappingCsv, _textFieldConverter, 1);
            foreach (var cameraMapping in cameraMappings)
            {
                cameraMappingByNameDictionary[cameraMapping.Name] = cameraMapping;
            }

            for (int stationNo = 1; stationNo < 15; stationNo++)
            {
                NodeViewAction offAction = new NodeViewAction($"fireOff4{stationNo:D2}000", $"4{stationNo:D2} 화재 종료");
                
                KeyValueCollection firePopupParameters = new KeyValueCollection();
                firePopupParameters.Set("id", "firePopup");
                offAction.AddCommand(new NodeViewCommand($"stationMonitor4{stationNo:D2}", "dataWall", "closePopup", firePopupParameters));

                KeyValueCollection mscPopupParameters = new KeyValueCollection();
                mscPopupParameters.Set("isAlarm", "True");
                mscPopupParameters.Set("stationId", $"{stationNo:D2}");
                mscPopupParameters.Set("message", $"4{stationNo:D2} 화재 종료");
                offAction.AddCommand(new NodeViewCommand("center", "mcsPopupController", "closePopup", mscPopupParameters));
                nodeViewActionDictionary[offAction.Id] = offAction;

                EventAction nodeViewEventAction = new EventAction()
                {
                    Id = $"stationFireAlarm4{stationNo:D2}000",
                    Description = $"4{stationNo:D2} 화재 종료",
                    Level = AlarmEventLevel.Alarm,
                    EventOffActionId = offAction.Id
                };
                nodeViewEventActionDictionary[nodeViewEventAction.Id] = nodeViewEventAction;

                //역사별 알람 이벤트 생성
                {
                    if (!stationActionDictionary.TryGetValue($"{stationNo:D2}", out var stationActions))
                    {
                        stationActions = new List<NodeViewAction>();
                        stationActionDictionary[$"{stationNo:D2}"] = stationActions;
                    }
                    if (!stationEventActionDictionary.TryGetValue($"{stationNo:D2}", out var stationEventActions))
                    {
                        stationEventActions = new List<EventAction>();
                        stationEventActionDictionary[$"{stationNo:D2}"] = stationEventActions;
                    }

                    NodeViewAction stationOffAction = new NodeViewAction(offAction.Id, offAction.Description);

                    KeyValueCollection eventOffParameters = new KeyValueCollection();
                    eventOffParameters.Set("id", nodeViewEventAction.Id);
                    stationOffAction.AddCommand(new NodeViewCommand("", "eventSender", "eventOff", eventOffParameters));
                    stationActions.Add(stationOffAction);

                    EventAction stationEventAction = new EventAction()
                    {
                        Id = nodeViewEventAction.Id,
                        Description = nodeViewEventAction.Description,
                        Level = nodeViewEventAction.Level,
                        EventOffActionId = stationOffAction.Id
                    };
                    stationEventActions.Add(stationEventAction);
                }
            }

            foreach (var subway4EventActionData in subway4EventActionList)
            {
                EventAction nodeViewEventAction = null;
                NodeViewAction nodeViewOnAction = null;
                NodeViewAction nodeViewOffAction = null;
                CameraMappingInfo camera = null;
                if (!cameraMappingByNameDictionary.TryGetValue(subway4EventActionData.Camera, out camera))
                {
                    AppendOutputLine($"Cannot find camera ID for [{subway4EventActionData.Trigger}].");
                    continue;
                }

                if (subway4EventActionData.AlarmType == "fire")
                {
                    (nodeViewEventAction, nodeViewOnAction) = CreateFireEventAndAction(subway4EventActionData, camera);
                    nodeViewEventActionDictionary[nodeViewEventAction.Id] = nodeViewEventAction;
                    nodeViewActionDictionary[nodeViewOnAction.Id] = nodeViewOnAction;

                    //역사별 알람 이벤트 생성
                    {
                        int stationNo = StringUtils.GetIntValue(subway4EventActionData.StationId);
                        if (!stationActionDictionary.TryGetValue($"{stationNo:D2}", out var stationActions))
                        {
                            stationActions = new List<NodeViewAction>();
                            stationActionDictionary[$"{stationNo:D2}"] = stationActions;
                        }
                        if (!stationEventActionDictionary.TryGetValue($"{stationNo:D2}", out var stationEventActions))
                        {
                            stationEventActions = new List<EventAction>();
                            stationEventActionDictionary[$"{stationNo:D2}"] = stationEventActions;
                        }

                        NodeViewAction stationOnAction = new NodeViewAction(nodeViewOnAction.Id, nodeViewOnAction.Description);

                        KeyValueCollection eventOnParameters = new KeyValueCollection();
                        eventOnParameters.Set("id", nodeViewEventAction.Id);
                        eventOnParameters.Set("message", nodeViewOnAction.Description);

                        stationOnAction.AddCommand(new NodeViewCommand("", "eventSender", "eventOn", eventOnParameters));
                        stationActions.Add(stationOnAction);

                        EventAction stationEventAction = new EventAction()
                        {
                            Id = nodeViewEventAction.Id,
                            Description = nodeViewEventAction.Description,
                            Level = nodeViewEventAction.Level,
                            EventOnActionId = stationOnAction.Id
                        };
                        stationEventActions.Add(stationEventAction);
                    }
                }
                else if (subway4EventActionData.AlarmType == "interphone")
                {
                    (nodeViewEventAction, nodeViewOnAction, nodeViewOffAction) = CreateOnOffEventAndAction(subway4EventActionData, camera);
                    nodeViewEventActionDictionary[nodeViewEventAction.Id] = nodeViewEventAction;
                    nodeViewActionDictionary[nodeViewOnAction.Id] = nodeViewOnAction;
                    nodeViewActionDictionary[nodeViewOffAction.Id] = nodeViewOffAction;

                    //centerItoStation 알람 이벤트 생성
                    {
                        int stationNo = 15;
                        if (!stationActionDictionary.TryGetValue($"{stationNo:D2}", out var stationActions))
                        {
                            stationActions = new List<NodeViewAction>();
                            stationActionDictionary[$"{stationNo:D2}"] = stationActions;
                        }
                        if (!stationEventActionDictionary.TryGetValue($"{stationNo:D2}", out var stationEventActions))
                        {
                            stationEventActions = new List<EventAction>();
                            stationEventActionDictionary[$"{stationNo:D2}"] = stationEventActions;
                        }

                        NodeViewAction stationOnAction = new NodeViewAction(nodeViewOnAction.Id, nodeViewOnAction.Description);

                        KeyValueCollection eventOnParameters = new KeyValueCollection();
                        eventOnParameters.Set("id", nodeViewEventAction.Id);
                        eventOnParameters.Set("message", nodeViewOnAction.Description);

                        stationOnAction.AddCommand(new NodeViewCommand("", "eventSender", "eventOn", eventOnParameters));
                        stationActions.Add(stationOnAction);

                        NodeViewAction stationOffAction = new NodeViewAction(nodeViewOffAction.Id, nodeViewOffAction.Description);

                        KeyValueCollection eventOffParameters = new KeyValueCollection();
                        eventOffParameters.Set("id", nodeViewEventAction.Id);
                        eventOffParameters.Set("message", nodeViewOffAction.Description);

                        stationOffAction.AddCommand(new NodeViewCommand("", "eventSender", "eventOff", eventOffParameters));
                        stationActions.Add(stationOffAction);
                        EventAction stationEventAction = new EventAction()
                        {
                            Id = nodeViewEventAction.Id,
                            Description = nodeViewEventAction.Description,
                            Level = nodeViewEventAction.Level,
                            EventOnActionId = stationOnAction.Id,
                            EventOffActionId = stationOffAction.Id
                        };
                        stationEventActions.Add(stationEventAction);
                    }
                }
                else if (subway4EventActionData.AlarmType == "door")
                {
                    (nodeViewEventAction, nodeViewOnAction, nodeViewOffAction) = CreateOnOffEventAndAction(subway4EventActionData, camera);
                    nodeViewEventActionDictionary[nodeViewEventAction.Id] = nodeViewEventAction;
                    nodeViewActionDictionary[nodeViewOnAction.Id] = nodeViewOnAction;
                    nodeViewActionDictionary[nodeViewOffAction.Id] = nodeViewOffAction;

                    //centerItoStation 알람 이벤트 생성
                    {
                        int stationNo = 15;
                        if (!stationActionDictionary.TryGetValue($"{stationNo:D2}", out var stationActions))
                        {
                            stationActions = new List<NodeViewAction>();
                            stationActionDictionary[$"{stationNo:D2}"] = stationActions;
                        }
                        if (!stationEventActionDictionary.TryGetValue($"{stationNo:D2}", out var stationEventActions))
                        {
                            stationEventActions = new List<EventAction>();
                            stationEventActionDictionary[$"{stationNo:D2}"] = stationEventActions;
                        }

                        NodeViewAction stationOnAction = new NodeViewAction(nodeViewOnAction.Id, nodeViewOnAction.Description);

                        KeyValueCollection eventOnParameters = new KeyValueCollection();
                        eventOnParameters.Set("id", nodeViewEventAction.Id);
                        eventOnParameters.Set("message", nodeViewOnAction.Description);

                        stationOnAction.AddCommand(new NodeViewCommand("", "eventSender", "eventOn", eventOnParameters));
                        stationActions.Add(stationOnAction);

                        NodeViewAction stationOffAction = new NodeViewAction(nodeViewOffAction.Id, nodeViewOffAction.Description);

                        KeyValueCollection eventOffParameters = new KeyValueCollection();
                        eventOffParameters.Set("id", nodeViewEventAction.Id);
                        eventOffParameters.Set("message", nodeViewOffAction.Description);

                        stationOffAction.AddCommand(new NodeViewCommand("", "eventSender", "eventOff", eventOffParameters));
                        stationActions.Add(stationOffAction);
                        EventAction stationEventAction = new EventAction()
                        {
                            Id = nodeViewEventAction.Id,
                            Description = nodeViewEventAction.Description,
                            Level = nodeViewEventAction.Level,
                            EventOnActionId = stationOnAction.Id,
                            EventOffActionId = stationOffAction.Id
                        };
                        stationEventActions.Add(stationEventAction);
                    }
                }
                else
                {
                    AppendOutputLine($"[{subway4EventActionData.AlarmType}] is not supported AlarmType.");
                    break;
                }
            }

            //actions.xml
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            using (XmlWriter xmlWriter = XmlWriter.Create(Path.Combine(OutputFolder, "actions.xml"), settings))
            {
                xmlWriter.WriteStartElement("Actions");
                foreach (var action in nodeViewActionDictionary.Values)
                {
                    action.WriteXml(xmlWriter, "Action");
                }
                xmlWriter.WriteEndElement();
                xmlWriter.Close();
            }

            //eventActions.xml
            using (XmlWriter xmlWriter = XmlWriter.Create(Path.Combine(OutputFolder, "eventActions.xml"), settings))
            {
                xmlWriter.WriteStartElement("EventActions");
                foreach (var eventActon in nodeViewEventActionDictionary.Values)
                {
                    eventActon.WriteXml(xmlWriter, "EventAction");
                }
                xmlWriter.WriteEndElement();
                xmlWriter.Close();
            }

            //역사별 이벤트 액션 생성
            {
                foreach (var stationActionPair in stationActionDictionary)
                {
                    string stationCode = stationActionPair.Key;
                    Directory.CreateDirectory(Path.Combine(OutputFolder, stationCode));
                    using (XmlWriter xmlWriter = XmlWriter.Create(Path.Combine(OutputFolder, stationCode, "actions.xml"), settings))
                    {
                        xmlWriter.WriteStartElement("Actions");
                        foreach (var action in stationActionPair.Value)
                        {
                            action.WriteXml(xmlWriter, "Action");
                        }
                        xmlWriter.WriteEndElement();
                        xmlWriter.Close();
                    }
                }

                foreach (var stationEventActionPair in stationEventActionDictionary)
                {
                    string stationCode = stationEventActionPair.Key;
                    Directory.CreateDirectory(Path.Combine(OutputFolder, stationCode));
                    using (XmlWriter xmlWriter = XmlWriter.Create(Path.Combine(OutputFolder, stationCode, "eventActions.xml"), settings))
                    {
                        xmlWriter.WriteStartElement("EventActions");
                        foreach (var eventAction in stationEventActionPair.Value)
                        {
                            eventAction.WriteXml(xmlWriter, "EventAction");
                        }
                        xmlWriter.WriteEndElement();
                        xmlWriter.Close();
                    }
                }
            }
            AppendOutputLine($"Done CreateNodeViewEventAction====");
        }

        private (EventAction, NodeViewAction, NodeViewAction) CreateOnOffEventAndAction(Subway4EventActionData subway4EventActionData, CameraMappingInfo camera)
        {
            EventAction nodeViewEventAction = null;
            NodeViewAction nodeViewOnAction = null;
            NodeViewAction nodeViewOffAction = null;

            int stationNum = StringUtils.GetIntValue(subway4EventActionData.StationId);
            int pointNum = StringUtils.GetIntValue(subway4EventActionData.Point);

            string windowId = $"{subway4EventActionData.AlarmType}Popup";
            string eventId = $"{subway4EventActionData.AlarmType}Event4{stationNum:D2}{pointNum:D3}";
            string actionOnId = $"{subway4EventActionData.AlarmType}On4{stationNum:D2}{pointNum:D3}";
            string actionOffId = $"{subway4EventActionData.AlarmType}Off4{stationNum:D2}{pointNum:D3}";
            string onMessage = (subway4EventActionData.AlarmType == "door") ? "열림" : "호출";
            string offMessage = (subway4EventActionData.AlarmType == "door") ? "닫힘" : "종료";
            nodeViewOnAction = new NodeViewAction(actionOnId, $"4{subway4EventActionData.Name} {onMessage}");
            nodeViewOffAction = new NodeViewAction(actionOffId, $"4{subway4EventActionData.Name} {offMessage}");

            if (!string.IsNullOrWhiteSpace(subway4EventActionData.Preset))
            {
                KeyValueCollection parameters = new KeyValueCollection();
                parameters.Set("ip", camera.Ip);
                parameters.Set("channel", camera.Channel);
                parameters.Set("id", subway4EventActionData.Preset);
                nodeViewOnAction.AddCommand(new NodeViewCommand("center", "encoderController", "applyPreset", parameters));
            }

            KeyValueCollection dataWallPopupCloseParameters = new KeyValueCollection();
            dataWallPopupCloseParameters.Set("id", windowId);

            if (subway4EventActionData.StationPopup == "Y")
            {
                KeyValueCollection dataWallPopupOpenParameters = new KeyValueCollection();
                dataWallPopupOpenParameters.Set("id", windowId);
                dataWallPopupOpenParameters.Set("contentId", camera.Id);
                dataWallPopupOpenParameters.Set("windowRect", "0,0,1920,1080");
                nodeViewOnAction.AddCommand(new NodeViewCommand($"stationMonitor4{stationNum:D2}", "dataWall", "createPopup", dataWallPopupOpenParameters));

                nodeViewOffAction.AddCommand(new NodeViewCommand($"stationMonitor4{stationNum:D2}", "dataWall", "closePopup", dataWallPopupCloseParameters));
            }

            if (subway4EventActionData.FacilityService1Popup == "Y")
            {
                KeyValueCollection dataWallPopupOpenParameters = new KeyValueCollection();
                dataWallPopupOpenParameters.Set("id", windowId);
                dataWallPopupOpenParameters.Set("contentId", camera.Id);
                dataWallPopupOpenParameters.Set("windowRect", "0,0,1920,1080");
                nodeViewOnAction.AddCommand(new NodeViewCommand($"facilityMonitor", "dataWall", "createPopup", dataWallPopupOpenParameters));

                // 설비모니터에서 수동으로 팝업 삭제(NodeViewHotKey 사용) 요청으로 closdPopup 삭제 : 2021.08.19 현장 적용
                //nodeViewOffAction.AddCommand(new NodeViewCommand($"facilityMonitor", "dataWall", "closePopup", dataWallPopupCloseParameters));
            }
            if (subway4EventActionData.TrainService1Popup == "Y")
            {
                KeyValueCollection dataWallPopupOpenParameters = new KeyValueCollection();
                dataWallPopupOpenParameters.Set("id", windowId);
                dataWallPopupOpenParameters.Set("contentId", camera.Id);
                dataWallPopupOpenParameters.Set("windowRect", "0,0,1920,1080");
                nodeViewOnAction.AddCommand(new NodeViewCommand($"train1Monitor", "dataWall", "createPopup", dataWallPopupOpenParameters));

                nodeViewOffAction.AddCommand(new NodeViewCommand($"train1Monitor", "dataWall", "closePopup", dataWallPopupCloseParameters));
            }
            if (subway4EventActionData.TrainService2Popup == "Y")
            {
                KeyValueCollection dataWallPopupOpenParameters = new KeyValueCollection();
                dataWallPopupOpenParameters.Set("id", windowId);
                dataWallPopupOpenParameters.Set("contentId", camera.Id);
                dataWallPopupOpenParameters.Set("windowRect", "0,0,1920,1080");
                nodeViewOnAction.AddCommand(new NodeViewCommand($"train2Monitor", "dataWall", "createPopup", dataWallPopupOpenParameters));

                nodeViewOffAction.AddCommand(new NodeViewCommand($"train2Monitor", "dataWall", "closePopup", dataWallPopupCloseParameters));
            }
            if (subway4EventActionData.PowerServicePopup == "Y")
            {
                KeyValueCollection dataWallPopupOpenParameters = new KeyValueCollection();
                dataWallPopupOpenParameters.Set("id", windowId);
                dataWallPopupOpenParameters.Set("contentId", camera.Id);
                dataWallPopupOpenParameters.Set("windowRect", "0,0,1920,1080");
                nodeViewOnAction.AddCommand(new NodeViewCommand($"powerMonitor", "dataWall", "createPopup", dataWallPopupOpenParameters));

                nodeViewOffAction.AddCommand(new NodeViewCommand($"powerMonitor", "dataWall", "closePopup", dataWallPopupCloseParameters));
            }


            nodeViewEventAction = new EventAction
            {
                Id = eventId,
                Description = subway4EventActionData.Name,
                Level = AlarmEventLevel.Normal,
                EventOnActionId = nodeViewOnAction.Id,
                EventOffActionId = nodeViewOffAction.Id
            };
            return (nodeViewEventAction, nodeViewOnAction, nodeViewOffAction);
        }

        private (EventAction, NodeViewAction) CreateFireEventAndAction(Subway4EventActionData subway4EventActionData, CameraMappingInfo camera)
        {
            EventAction nodeViewEventAction = null;
            NodeViewAction nodeViewOnAction = null;

            int stationNum = StringUtils.GetIntValue(subway4EventActionData.StationId);
            int pointNum = StringUtils.GetIntValue(subway4EventActionData.Point);

            string windowId = $"firePopup";
            string eventId = $"stationFireAlarm4{stationNum:D2}{pointNum:D3}";
            string actionOnId = $"{subway4EventActionData.AlarmType}On4{stationNum:D2}{pointNum:D3}";
            //string mcsMessage = $"4{stationNum:D2}-{pointNum:D3} 화재 발생";
            string mcsMessage = subway4EventActionData.Description;
            nodeViewOnAction = new NodeViewAction(actionOnId, $"4{stationNum:D2}-{pointNum:D3} 화재 발생");
            if (!string.IsNullOrWhiteSpace(subway4EventActionData.Preset))
            {
                KeyValueCollection parameters = new KeyValueCollection();
                parameters.Set("ip", camera.Ip);
                parameters.Set("channel", camera.Channel);
                parameters.Set("id", subway4EventActionData.Preset);
                nodeViewOnAction.AddCommand(new NodeViewCommand("center", "encoderController", "applyPreset", parameters));
            }

            KeyValueCollection mscPopupParameters = new KeyValueCollection();
            mscPopupParameters.Set("isAlarm", "True");
            mscPopupParameters.Set("stationId", $"{stationNum:D2}");
            mscPopupParameters.Set("contentId", camera.Id);
            mscPopupParameters.Set("message", mcsMessage);
            nodeViewOnAction.AddCommand(new NodeViewCommand("center", "mcsPopupController", "openPopup", mscPopupParameters));

            KeyValueCollection dataWallParameters = new KeyValueCollection();
            dataWallParameters.Set("id", windowId);
            dataWallParameters.Set("contentId", camera.Id);
            dataWallParameters.Set("windowRect", "0,0,1920,1080");
            nodeViewOnAction.AddCommand(new NodeViewCommand($"stationMonitor4{stationNum:D2}", "dataWall", "createPopup", dataWallParameters));

            // 화재 발생시 설비모니터에 팝업 요청 : 2021.08.19 현장 적용
            dataWallParameters = new KeyValueCollection();
            dataWallParameters.Set("id", windowId);
            dataWallParameters.Set("contentId", camera.Id);
            dataWallParameters.Set("windowRect", "0,0,1920,1080");
            nodeViewOnAction.AddCommand(new NodeViewCommand($"facilityMonitor", "dataWall", "createPopup", dataWallParameters));

            nodeViewEventAction = new EventAction
            {
                Id = eventId,
                Description = $"4{stationNum:D2}-{pointNum:D3} 화재 수신기",
                Level = AlarmEventLevel.Alarm,
                EventOnActionId = nodeViewOnAction.Id
            };
            return (nodeViewEventAction, nodeViewOnAction);
        }
        
        private void buttonCreateMcsCameraXml_Click(object sender, EventArgs e)
        {
            List<CameraMappingInfo> cameraMappingList = new List<CameraMappingInfo>();

            string cameraMappingCsv = Path.Combine(InputFolder, "cameraMappingBaseInfo.csv");
            var cameraMappings = TextFieldReader.Convert<CameraMappingInfo>(cameraMappingCsv, _textFieldConverter, 1);
            cameraMappingList.AddRange(cameraMappings);
            cameraMappingList.Sort((x, y) => string.Compare(x.Id, y.Id, StringComparison.OrdinalIgnoreCase));

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            using (XmlWriter xmlWriter = XmlWriter.Create(Path.Combine(OutputFolder, "mcsCameras.xml"), settings))
            {
                xmlWriter.WriteStartElement("WallContents");
                foreach (var cameraMapping in cameraMappingList)
                {
                    if (!string.IsNullOrWhiteSpace(cameraMapping.WallContentName))
                    {
                        xmlWriter.WriteStartElement("WallContent");
                        xmlWriter.WriteAttributeString("id", cameraMapping.Id);
                        xmlWriter.WriteAttributeString("name", cameraMapping.WallContentName);
                        xmlWriter.WriteEndElement();
                    }
                }
                xmlWriter.WriteEndElement();
                xmlWriter.Close();
            }

            string leadtechCameraFilePath = Path.Combine(OutputFolder, "leadtechCamera.csv");
            List<LeadtechCamera> leadtechCameraList = new List<LeadtechCamera>();
            foreach (var cameraMapping in cameraMappingList)
            {
                if (!string.IsNullOrWhiteSpace(cameraMapping.WallContentName))
                {
                    leadtechCameraList.Add(new LeadtechCamera()
                    {
                        Id = cameraMapping.Id,
                        Name = cameraMapping.Name,
                        Osd = cameraMapping.OsdName,
                        Url = $"rtsp://{cameraMapping.Ip}/{cameraMapping.Channel}/profile2/media.smp",
                        UserId = cameraMapping.UserId,
                        UserPassword = cameraMapping.UserPassword,
                    });
                }
            }
            SaveSteelBoxDto(leadtechCameraList.ToArray(), leadtechCameraFilePath);

            AppendOutputLine($"Done CreateMcsCameraXml====");
        }

        private void buttonTransMcsConfig_Click(object sender, EventArgs args)
        {
            try
            {
                string inoutFile = Path.Combine(InputFolder, "mcs.config.xml");
                string outputFile = Path.Combine(OutputFolder, "mcs.config.xml");

                XmlDocument doc = new XmlDocument();
                doc.Load(inoutFile);

                string cameraMappingCsv = Path.Combine(InputFolder, "cameraMappingBaseInfo.csv");
                var cameraMappings = TextFieldReader.Convert<CameraMappingInfo>(cameraMappingCsv, _textFieldConverter, 1);
                Dictionary<string, CameraMappingInfo> cameraMappingDictionary = new Dictionary<string, CameraMappingInfo>();
                foreach (var cameraMapping in cameraMappings)
                {
                    if (!string.IsNullOrWhiteSpace(cameraMapping.WallContentName))
                    {
                        cameraMappingDictionary[cameraMapping.WallContentName] = cameraMapping;
                    }
                }

                //wallContents Id 변경
                Dictionary<string, string> cameraIdDictionary = new Dictionary<string, string>();
                XmlNodeList wallContentNodeList = doc.SelectNodes("/Config/WallContents/WallContent");
                foreach (XmlNode wallContentNode in wallContentNodeList)
                {
                    string cameraName = XmlUtils.GetStringAttrValue(wallContentNode, "name");
                    if (cameraMappingDictionary.TryGetValue(cameraName, out var mapping))
                    {
                        string oldCameraId = XmlUtils.GetStringAttrValue(wallContentNode, "id");
                        string newCameraId = mapping.Id;

                        if(wallContentNode is XmlElement wallContentElement)
                        {
                            wallContentElement.SetAttribute("id", newCameraId);
                        }

                        XmlNodeList hasContentIdNodeList = doc.SelectNodes($"//*[@contentId='{oldCameraId}']");
                        if (hasContentIdNodeList != null)
                        {
                            foreach (XmlElement xmlElement in hasContentIdNodeList)
                            {
                                xmlElement.SetAttribute("contentId", newCameraId);
                            }
                        }
                    }
                    else
                    {
                        AppendOutputLine($"camera name [{cameraName}] is not found.");
                    }
                }
                //station Id 변경
                Dictionary<string, string> mcsStationMap = new Dictionary<string, string>();
                mcsStationMap["01"] = "15";
                mcsStationMap["02"] = "14";
                mcsStationMap["03"] = "13";
                mcsStationMap["04"] = "12";
                mcsStationMap["05"] = "11";
                mcsStationMap["06"] = "10";
                mcsStationMap["07"] = "09";
                mcsStationMap["08"] = "08";
                mcsStationMap["09"] = "07";
                mcsStationMap["10"] = "06";
                mcsStationMap["11"] = "05";
                mcsStationMap["12"] = "04";
                mcsStationMap["13"] = "03";
                mcsStationMap["14"] = "02";
                mcsStationMap["15"] = "01";
                XmlNodeList stationNodeList = doc.SelectNodes("/Config/Stations/Station");
                if (stationNodeList != null)
                {
                    foreach (XmlElement stationNode in stationNodeList)
                    {
                        string oldStationId = stationNode.GetAttribute("id");
                        if (mcsStationMap.TryGetValue(oldStationId, out string newStationId))
                        {
                            stationNode.SetAttribute("id", newStationId);
                        }
                        else
                        {
                            AppendOutputLine($"station id[{oldStationId}] is not found.");
                        }
                    }
                }
                XmlNodeList hasStationIdNodeList = doc.SelectNodes("//*[@stationId]");
                if (hasStationIdNodeList != null)
                {
                    foreach (XmlElement xmlElement in hasStationIdNodeList)
                    {
                        string oldStationId = xmlElement.GetAttribute("stationId");
                        if (mcsStationMap.TryGetValue(oldStationId, out string newStationId))
                        {
                            xmlElement.SetAttribute("stationId", newStationId);
                        }
                        else
                        {
                            AppendOutputLine($"stationid[{oldStationId}] is not found.");
                        }
                    }
                }

                using (var xmlWriter = XmlUtils.CreateXmlWriter(outputFile))
                {
                    doc.Save(xmlWriter);
                    xmlWriter.Close();
                }
                /*
                SemsolConfiguration config = SemsolConfigurationManager.Load(inoutFile);

                SemsolConfigurationManager.Save(outputFile, config);
                */

            }
            catch (Exception e)
            {
                AppendOutputLine(e.ToString());
            }
            AppendOutputLine($"Done TransMcsConfig====");
        }

        private void buttonCreateNodeViewContents_Click(object sender, EventArgs e)
        {
            List<DataWallContent> dataWallContents = new List<DataWallContent>();
            
            string cameraMappingCsv = Path.Combine(InputFolder, "cameraMappingBaseInfo.csv");
            var cameraMappings = TextFieldReader.Convert<CameraMappingInfo>(cameraMappingCsv, _textFieldConverter, 1);


            foreach (var cameraMapping in cameraMappings)
            {
                DataWallContent content = new DataWallContent();
                content.ContentType = "camera";
                content.Id = cameraMapping.Id;
                content.Name = cameraMapping.OsdName;
                content.Path = $"s4{cameraMapping.StationCode}";
                content.Uri = $"rtsp://{cameraMapping.Ip}/{cameraMapping.Channel}/profile2/media.smp";
                content.UserId = cameraMapping.UserId;
                content.UserPassword = cameraMapping.UserPassword;
                /*
                content.Uri = $"rtsp://192.168.31.30/0/Mainstream/media.smp";
                content.UserId = "admin";
                content.UserPassword = "semsol1";
                */
                dataWallContents.Add(content);
            }
            string contentsFile = Path.Combine(OutputFolder, "contents.xml");
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(contentsFile))
                {
                    xmlWriter.WriteStartElement("Contents");
                    foreach (var content in dataWallContents)
                    {
                        content.WriteXml(xmlWriter, "Content");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception ex)
            {
                AppendOutputLine(ex.ToString());
            }
            AppendOutputLine($"Done CreateNodeViewContents====");
        }
    }
}
