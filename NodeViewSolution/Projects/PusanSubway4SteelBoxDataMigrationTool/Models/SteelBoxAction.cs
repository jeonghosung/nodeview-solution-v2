﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxAction : ISteelBoxData
    {
        public string Id { get; set; } = "";
        public string ActionTypeId { get; set; } = "";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string Sequence { get; set; } = "";
        public string ParentActionId { get; set; } = "";
        public string FolderId { get; set; } = "";
        public string ItemId { get; set; } = "";
        public string ItemId1 { get; set; } = "";
        public string ItemId2 { get; set; } = "";
        public string Path { get; set; } = "";
        public string SerializedData { get; set; } = "";
    }
}
