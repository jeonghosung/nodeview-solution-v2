﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxActionEx: SteelBoxAction
    {
        public string ActionType { get; set; } = "";
        public string GroupId { get; set; } = "";
        public string Folder { get; set; } = "";
        public string ItemType { get; set; } = "";
        public string ItemType1 { get; set; } = "";
        public string ItemType2 { get; set; } = "";
        public string DataPortName { get; set; } = "";
        public string ProfileName { get; set; } = "";
        public string ProfileType { get; set; } = "";
        public SteelBoxDataPortEx DataPort { get; set; } = null;
        public string SendData { get; set; } = "";
        public string Parameter { get; set; } = "";
        public int RefEventCount { get; set; } = 0;
        public bool IsUsing => (RefEventCount > 0);

        //DataMigration 용
        public SteelBoxActionEx Clone()
        {
            var action = new SteelBoxActionEx();
            action.Id = Id;
            action.ActionType = ActionType;
            action.Name = Name;
            action.Description = Description;
            action.GroupId = GroupId;
            action.ParentActionId = ParentActionId;
            action.Sequence = Sequence;
            action.Folder = Folder;
            action.ItemType = ItemType;
            action.ItemType1 = ItemType1;
            action.ItemType2 = ItemType2;
            action.DataPortName = DataPortName;
            action.ProfileName = ProfileName;
            action.ProfileType = ProfileType;
            action.SendData = SendData;
            action.Parameter = Parameter;
            action.RefEventCount = RefEventCount;
            return action;
        }
        public string[] Headers => GetHeaders();

        public static string[] GetHeaders()
        {
            return new string[]
            {
                "Id",
                "ActionType",
                "Name",
                "Description",
                "GroupId",
                "ParentActionId",
                "Sequence",
                "Folder",
                "ItemType",
                "ItemType1",
                "ItemType2",
                "DataPortName",
                "ProfileName",
                "ProfileType",
                "SendData",
                "Parameter",
                "RefEventCount",
                "IsUsing"
            };
        }
    }
}
