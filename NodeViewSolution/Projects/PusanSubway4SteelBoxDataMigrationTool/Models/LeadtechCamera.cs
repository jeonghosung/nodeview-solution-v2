﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class LeadtechCamera : ISteelBoxDto
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Osd { get; set; } = "";
        public string Url { get; set; } = "";
        public string UserId { get; set; } = "";
        public string UserPassword { get; set; } = "";

        public string[] Headers => GetHeaders();

        public static string[] GetHeaders()
        {
            return new string[]
            {
                "Id",
                "Name",
                "Osd",
                "Url",
                "UserId",
                "UserPassword"
            };
        }
    }
}
