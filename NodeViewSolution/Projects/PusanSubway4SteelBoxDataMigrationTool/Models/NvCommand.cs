﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NodeView.DataModels;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class NvCommand
    {
        public string AppId { get; set; } = "";
        public string DeviceId { get; set; } = "";
        public string Command { get; set; } = "";
        public KeyValueCollection Parameters { get; set; } = new KeyValueCollection();

        public string Sid { get; set; } = "";

        public NvCommand(string appId = "", string deviceId = "", string command = "", KeyValueCollection parameters = null)
        {
            AppId = appId;
            DeviceId = deviceId;
            Command = command;
            if (parameters != null)
            {
                Parameters.Sets(parameters);
            }
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Command" : tagName;
            xmlWriter.WriteStartElement(tagName);
            if (!string.IsNullOrWhiteSpace(AppId))
            {
                xmlWriter.WriteAttributeString("appId", AppId);
            }
            xmlWriter.WriteAttributeString("deviceId", DeviceId);
            xmlWriter.WriteAttributeString("command", Command);
            xmlWriter.WriteAttributeString("sid", Sid);

            xmlWriter.WriteStartElement("Param");
            foreach (var parameter in Parameters)
            {
                xmlWriter.WriteAttributeString(parameter.Key, $"{parameter.Value}");
            }
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
        }
    }
}
