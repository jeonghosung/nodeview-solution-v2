﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NodeView.DataModels;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class NvAction
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public List<NvCommand> Commands { get; } = new List<NvCommand>();
        public List<string> SidList { get; } = new List<string>();

        public NvAction(string id = "", string description = "", IEnumerable<NvCommand> commands = null)
        {
            Id = id;
            Description = description;
            if (commands != null) Commands.AddRange(commands);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Action" : tagName;

            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("description", Description);
            xmlWriter.WriteAttributeString("sids", string.Join(",", SidList));

            foreach (var command in Commands.ToArray())
            {
                command.WriteXml(xmlWriter, "Command");
            }

            xmlWriter.WriteEndElement();
        }
    }
}
