﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NodeView.DataModels;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class NvEvent
    {
        public string Id { get; set; } = "";
        public AlarmEventLevel Level { get; set; } = AlarmEventLevel.Alarm;
        public string Description { get; set; } = "";
        public string EventOnActionId { get; set; } = "";
        public string EventOffActionId { get; set; } = "";
        public NvAction OnAction { get; set; }
        public NvAction OffAction { get; set; }
        public List<string> TriggerList { get; set; } = new List<string>();
        public List<string> SidList { get; } = new List<string>();

        public NvEvent(string id, string description)
        {
            Id = id;
            Description = description;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Event" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("level", $"{Level}");
            xmlWriter.WriteAttributeString("description", Description);
            xmlWriter.WriteAttributeString("triggers", string.Join(",", TriggerList));
            xmlWriter.WriteAttributeString("sids", string.Join(",", SidList));

            if (!string.IsNullOrWhiteSpace(EventOnActionId))
            {
                xmlWriter.WriteElementString("EventOnActionId", EventOnActionId);
            }
            else
            {
                OnAction?.WriteXml(xmlWriter, "OnAction");
            }
            if (!string.IsNullOrWhiteSpace(EventOffActionId))
            {
                xmlWriter.WriteElementString("EventOffActionId", EventOffActionId);
            }
            else
            {
                OffAction?.WriteXml(xmlWriter, "OffAction");
            }

            xmlWriter.WriteEndElement();
        }
    }
}
