﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxEvent: ISteelBoxData
    {
        public string Id { get; set; } = "";
        public string EventType { get; set; } = "";
        public string ItemId { get; set; } = "";
        public string FolderId { get; set; } = "";
        public string ItemRaisedBy { get; set; } = "";
        public string DenverNodeId { get; set; } = "";
        public string ActionIdAutomation { get; set; } = "";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string XmlConfiguration { get; set; } = "";
        public string ActionIdOperator { get; set; } = "";
        public string ActionIdCleanup { get; set; } = "";
    }
}
