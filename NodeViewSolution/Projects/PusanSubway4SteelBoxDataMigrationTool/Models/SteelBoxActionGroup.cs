﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxActionGroup : SteelBoxActionEx
    {
        private List<SteelBoxActionEx> _actions = new List<SteelBoxActionEx>();
        public SteelBoxActionEx[] Actions => _actions.ToArray();

        public SteelBoxActionGroup(SteelBoxActionEx steelBoxAction)
        {
            Id = steelBoxAction.Id;
            ActionType = steelBoxAction.ActionType;
            Name = steelBoxAction.Name;
            Description = steelBoxAction.Description;
            GroupId = steelBoxAction.GroupId;
            ParentActionId = steelBoxAction.ParentActionId;
            Sequence = steelBoxAction.Sequence;
            Folder = steelBoxAction.Folder;
            ItemType = steelBoxAction.ItemType;
            ItemType1 = steelBoxAction.ItemType1;
            ItemType2 = steelBoxAction.ItemType2;
            DataPortName = steelBoxAction.DataPortName;
            ProfileName = steelBoxAction.ProfileName;
            ProfileType = steelBoxAction.ProfileType;
            SendData = steelBoxAction.SendData;
            Parameter = steelBoxAction.Parameter;
            RefEventCount = steelBoxAction.RefEventCount;
        }

        public bool AddAction(SteelBoxActionEx steelBoxAction)
        {
            if (steelBoxAction == null) return false;
            steelBoxAction = steelBoxAction.Clone();
            if (_actions.Count == 0)
            {
                _actions.Add(steelBoxAction);
                return true;
            }

            for (int i = 0; i < _actions.Count; i++)
            {
                if (string.CompareOrdinal(_actions[i].Sequence, steelBoxAction.Sequence) > 0)
                {
                    _actions.IndexOf(steelBoxAction, i);
                    return false;
                }
            }
            _actions.Add(steelBoxAction);
            return true;
        }

        public bool AddActions(string sequence, SteelBoxActionGroup steelBoxGroup)
        {
            if (steelBoxGroup == null || steelBoxGroup._actions.Count == 0) return false;

            if (_actions.Count == 0)
            {
                foreach (var steelBoxActionEx in steelBoxGroup.Actions)
                {
                    var steelBoxAction = steelBoxActionEx.Clone();
                    steelBoxAction.Sequence = $"{sequence}-{steelBoxAction.Sequence}";
                    steelBoxAction.Description = steelBoxGroup.Name;
                    _actions.Add(steelBoxAction);
                }
                
                return true;
            }

            for (int i = 0; i < _actions.Count; i++)
            {
                if (string.CompareOrdinal(_actions[i].Sequence, sequence) > 0)
                {
                    int offset = i;
                    foreach (var steelBoxActionEx in steelBoxGroup.Actions)
                    {
                        var steelBoxAction = steelBoxActionEx.Clone();
                        steelBoxAction.Sequence = $"{sequence}-{steelBoxAction.Sequence}";
                        steelBoxAction.Description = steelBoxGroup.Name;
                        _actions.IndexOf(steelBoxAction, offset);
                    }
                    return false;
                }
            }
            foreach (var steelBoxActionEx in steelBoxGroup.Actions)
            {
                var steelBoxAction = steelBoxActionEx.Clone();
                steelBoxAction.Sequence = $"{sequence}-{steelBoxAction.Sequence}";
                steelBoxAction.Description = steelBoxGroup.Name;
                _actions.Add(steelBoxAction);
            }
            return true;
        }

    }
}
