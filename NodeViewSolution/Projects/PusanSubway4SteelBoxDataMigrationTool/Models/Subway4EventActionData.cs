﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class Subway4EventActionData : ISteelBoxDto
    {
        //알람타입 역사번호    지점번호 알람 이름 카메라Id   프리셋Id

        public string Sid { get; set; } = "";
        public string Trigger { get; set; } = "";
        public string AlarmType { get; set; } = "";
        public string IsOn { get; set; } = "";
        public string StationId { get; set; } = "";
        public string Point { get; set; } = "";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string Camera { get; set; } = "";
        public string Preset { get; set; } = "";
        // MCS팝업   설비사령팝업 운전사령1팝업 운전사령2팝업 전력사령 팝업
        public string McsPopup { get; set; } = "";
        public string StationPopup { get; set; } = "";
        public string FacilityService1Popup { get; set; } = "";
        public string TrainService1Popup { get; set; } = "";
        public string TrainService2Popup { get; set; } = "";
        public string PowerServicePopup { get; set; } = "";
        public string Note { get; set; } = "";
        public string[] Headers => GetHeaders();

        public static string[] GetHeaders()
        {
            return new string[]
            {
                "Sid",
                "Trigger",
                "AlarmType",
                "IsOn",
                "StationId",
                "Point",
                "Name",
                "Description",
                "Camera",
                "Preset",
                "McsPopup",
                "StationPopup",
                "FacilityService1Popup",
                "TrainService1Popup",
                "TrainService2Popup",
                "PowerServicePopup",
                "Note"
            };
        }
    }
}
