﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxDataPort: ISteelBoxData
    {
        public string Id { get; set; } = "";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string EntityValue { get; set; } = "";
        public string EndpointId { get; set; } = "";
        public string ItemId { get; set; } = "";
        public string FolderId { get; set; } = "";
        public string ProfileId { get; set; } = "";
    }
}
