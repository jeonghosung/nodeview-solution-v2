﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxEventEx: SteelBoxEvent
    {
        public string Folder { get; set; } = "";
        public string ItemType { get; set; } = "";
        public string DenverNodeName { get; set; } = "";
        public string DataPortName { get; set; } = "";
        public string ProfileName { get; set; } = "";
        public string ProfileType { get; set; } = "";
        public SteelBoxDataPortEx DataPort { get; set; } = null;
        public string ActionName { get; set; } = "";
        public string TriggerData { get; set; } = "";

        public string[] Headers => GetHeaders();

        public static string[] GetHeaders()
        {
            return new string[]
            {
                "Id",
                "EventType",
                "Name",
                "Description",
                "Folder",
                "ItemType",
                "ItemType1",
                "DenverNodeName",
                "DataPortName",
                "ProfileName",
                "ProfileType",
                "TriggerData",
                "ActionIdAutomation",
                "ActionName"
            };
        }
    }
}
