﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class Interphone
    {
        public string Code { get; }
        public string Name { get; }
        public bool UsingEvent { get; }

        public Interphone(string code, string name, bool usingEvent = true)
        {
            Code = code;
            Name = name;
            UsingEvent = usingEvent;
        }
    }
}
