﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class Door
    {
        public string Code { get; }
        public string Name { get; }
        public bool UsingEvent { get; }

        public Door(string code, string name, bool usingEvent)
        {
            Code = code;
            Name = name;
            UsingEvent = usingEvent;
        }
    }
}
