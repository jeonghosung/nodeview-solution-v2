﻿namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class SteelBoxDataPortEx: SteelBoxDataPort
    {
        public string Folder { get; set; } = "";
        public string ItemType { get; set; } = "";
        public string ProfileName { get; set; } = "";
        public string ProfileType { get; set; } = "";
        public int RefEventCount { get; set; } = 0;
        public int RefActionCount { get; set; } = 0;
        public int RefActionInEventCount { get; set; } = 0;

        public bool IsUsing => (RefEventCount > 0 || RefActionCount > 0 || RefActionInEventCount > 0);

        public string[] Headers => GetHeaders();

        public static string[] GetHeaders()
        {
            return new string[]
            {
                "Id",
                "Name",
                "Description",
                "Folder",
                "ItemType",
                "ProfileName",
                "ProfileType",
                "RefEventCount",
                "RefActionCount",
                "RefActionInEventCount",
                "IsUsing"
            };
        }
    }
}
