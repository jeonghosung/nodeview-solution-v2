﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PusanSubway4SteelBoxDataMigrationTool.Models
{
    public class CameraMappingInfo
    {
        public string Id => $"cctv4{StationCode}0{CameraCode}";
        public string Code => $"{StationCode}{CameraCode}";
        public string StationCode { get; set; } = "";
        public string CameraCode { get; set; } = "";
        public string Name { get; set; } = "";
        public string Connection { get; set; } = "";
        public string OsdName { get; set; } = "";
        public string WallContentName { get; set; } = "";
        public string NodeViewContentName { get; set; } = "";
        public string NodeViewContentPath { get; set; } = "";
        public string Ip { get; set; } = "";
        public string Channel { get; set; } = "";
        public string UserId { get; set; } = "";
        public string UserPassword { get; set; } = "";
        public string UsingPtz { get; set; } = "";
    }
}
