﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.ViewModels;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdContentViewModel : SelectableViewModel
    {
        private string _name;
        private string _inputSource;

        public HdContentViewModel(string id, string inputSource, string channel, string name)
        {
            Id = id;
            InputSource = inputSource;
            Channel = channel;
            Name = name;
        }
        public string Id { get; set; }

        public string InputSource
        {
            get => _inputSource;
            set
            {
                SetProperty(ref _inputSource, value);
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public string Channel { get; set; }

        public string Name
        {
            get => _name;
            set
            {
                SetProperty(ref _name, value);
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public string BackgroundColor => (string.IsNullOrWhiteSpace(Name)) ? "#55565E" : (InputSource=="wall") ? "#38974c" : "#cc8b28";

    }
}
