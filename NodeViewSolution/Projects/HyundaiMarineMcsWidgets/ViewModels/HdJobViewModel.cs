﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdJobViewModel
    {
        public string Id { get; set; } = Uuid.NewUuid;
        public int Hour { get; set; } = 0;
        public int Min { get; set; } = 0;
        public string PresetId { get; set; } = "";
        public string PresetName { get; set; } = "";
        public int TotalMin => Hour * 60 + Min;

        public HdJobViewModel(int hour, int min, string presetId, string presetName)
        {
            Hour = hour;
            Min = min;
            PresetId = presetId;
            PresetName = presetName;
        }

        public HdJobViewModel Clone()
        {
            var clone = new HdJobViewModel(Hour, Min, PresetId, PresetName);
            clone.Id = Id;
            return clone;
        }
    }
}
