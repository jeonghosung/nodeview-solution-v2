﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Documents;
using System.Xml;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Mvvm;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdWindowCellViewModel : HdCellViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _windowScale;
        private string _inputSource = "";
        private string _channelId = "";
        private string _channelName = "";

        public string Id { get; set; } = "";

        public string WindowScale
        {
            get => _windowScale;
            set
            {
                SetProperty(ref _windowScale, value);
                RaisePropertyChanged("WindowScaleLabel");
            } 
        }

        public string WindowScaleLabel => _windowScale == "1x1" ? "" : "2x2";

        public string InputSource
        {
            get => _inputSource;
            set
            {
                SetProperty(ref _inputSource, value);
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public string ChannelId
        {
            get => _channelId;
            set => SetProperty(ref _channelId, value);
        }

        public string ChannelName
        {
            get => _channelName;
            set
            {
                SetProperty(ref _channelName, value);
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public int[] MonitorNumbers { get; set; } = new int[0];

        public string BackgroundColor => (string.IsNullOrWhiteSpace(ChannelName)) ? "#55565E" : (InputSource == "wall") ? "#38974c" : "#cc8b28";

        public HdWindowCellViewModel(string id, IntRect baseRect)
        {
            Id = id;
            BaseRect = baseRect.Clone();
            Rect = baseRect.Clone();
        }
        public static HdWindowCellViewModel CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                var id = XmlUtils.GetAttrValue(xmlNode, "id", Uuid.NewUuid);
                var baseRect = XmlUtils.GetAttrValue(xmlNode, "baseRect", IntRect.Empty);
                var window = new HdWindowCellViewModel(id, baseRect);
                if (window.LoadFrom(xmlNode))
                {
                    return window;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public HdWindowCellViewModel Clone()
        {
            var clone = new HdWindowCellViewModel(Id, BaseRect);
            clone.Rect = Rect;
            clone._windowScale = _windowScale;
            clone._inputSource = _inputSource;
            clone._channelId = _channelId;
            clone._channelName = _channelName;
            clone.MonitorNumbers = MonitorNumbers.ToArray();
            return clone;
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Id = XmlUtils.GetAttrValue(xmlNode, "id", Uuid.NewUuid);
                BaseRect = XmlUtils.GetAttrValue(xmlNode, "baseRect", new IntRect(BaseRect));
                Rect = XmlUtils.GetAttrValue(xmlNode, "rect", new IntRect(Rect));
                WindowScale = XmlUtils.GetAttrValue(xmlNode, "windowScale", "");
                InputSource = XmlUtils.GetAttrValue(xmlNode, "inputSource", "");
                ChannelId = XmlUtils.GetAttrValue(xmlNode, "channelId", "");
                ChannelName = XmlUtils.GetAttrValue(xmlNode, "channelName", "");
                List<int> monitorNumbers = new List<int>();
                string[] monitorNumbersSplit = StringUtils.Split(XmlUtils.GetAttrValue(xmlNode, "monitorNumbers", ""), ',');
                foreach (var number in monitorNumbersSplit)
                {
                    if (int.TryParse(number, out int no))
                    {
                        monitorNumbers.Add(no);
                    }
                }
                //int[] monitorNumbers = XmlUtils.GetAttrValue(xmlNode, "monitorNumbers", new int[0]);
                MonitorNumbers = monitorNumbers.ToArray();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "WindowCell";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("baseRect", $"{BaseRect}");
            xmlWriter.WriteAttributeString("rect", $"{Rect}");
            xmlWriter.WriteAttributeString("windowScale", WindowScale);
            xmlWriter.WriteAttributeString("inputSource", InputSource);
            xmlWriter.WriteAttributeString("channelId", ChannelId);
            xmlWriter.WriteAttributeString("channelName", ChannelName);
            xmlWriter.WriteAttributeString("monitorNumbers", $"{string.Join(",", MonitorNumbers)}");

            xmlWriter.WriteEndElement();
        }
    }
}
