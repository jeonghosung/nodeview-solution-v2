﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NLog;
using NodeView.DataWalls;
using NodeView.Utils;
using NodeView.ViewModels;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdMcsPresetViewModel : SelectableViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _id;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        private string _name;

        public HdMcsPresetViewModel(string id = "", string name = "", HdWindowCellViewModel[] windowCells= null)
        {
            Id = id;
            Name = name;
            WindowCells = windowCells;
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public HdWindowCellViewModel[] WindowCells { get; set; }

        public static HdMcsPresetViewModel CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {  
                var preset = new HdMcsPresetViewModel();
                if (preset.LoadFrom(xmlNode))
                {
                    return preset;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
                Id = XmlUtils.GetAttrValue(xmlNode, "id", Id);

                List<HdWindowCellViewModel> windowCells = new List<HdWindowCellViewModel>();
                XmlNodeList windowNodeList = xmlNode.SelectNodes("Window");
                if (windowNodeList != null)
                {
                    foreach (XmlNode windowNode in windowNodeList)
                    {
                        var windowCell = HdWindowCellViewModel.CreateFrom(windowNode);
                        if (windowCell != null)
                        {
                            windowCells.Add(windowCell);
                        }
                    }
                }

                WindowCells = windowCells.ToArray();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Preset";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);

            if (WindowCells != null)
            {
                foreach (var windowCell in WindowCells)
                {
                    windowCell.WriteXml(xmlWriter, "Window");
                }
            }

            xmlWriter.WriteEndElement();
        }
    }
}
