﻿using NLog;
using Prism.Commands;
using System;
using System.IO.Ports;

namespace HyundaiMarineMcsWidgets.Models
{
    public class SpeakerControlViewmodel 
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IVolumeControl volumeControl;
        private readonly string _channel = "ch1";

        private DelegateCommand<bool?> _volumeUpDownCommand;
        public DelegateCommand<bool?> VolumeUpDownCommand => _volumeUpDownCommand ?? (_volumeUpDownCommand = new DelegateCommand<bool?>(ExecuteVolumeUpDown));

        public SpeakerControlViewmodel(string channel, SerialPort serialPort, bool isTestMode)
        {
            _channel = channel;
            volumeControl = new HdmVolumeControl(serialPort, isTestMode);
        }

        private void ExecuteVolumeUpDown(bool? isUp)
        {
            if (isUp == null)
            {
                return;
            }
            volumeControl.VolumeUpDown(_channel, (bool)isUp);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                volumeControl.Dispose();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
       
    }
}
