﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NodeView.DeviceControllers;
using NodeView.Extension.Plugins;
using Prism.Commands;
using Prism.Mvvm;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class TvControlViewModel : BindableBase, IDisposable
    {
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string VolumeSectionName = "Volume";
        private const string VolumePreFix = "Volume";
        private const string IniFileName = "Setting.ini";
        private const string MutePrefix = "Mute";
        private readonly string _muteKey;
        private readonly string _volumeKey;
        private readonly string _iniFilePath;
        
        private readonly string _id;
        private readonly int _index;

        private readonly LgDidControlClient _lgDidControl;

        private bool _isMute;

        private DelegateCommand<bool?> _volumeUpDownCommand;
        private DelegateCommand _muteOnOffCommand;
        private DelegateCommand<bool?> _monitorPowerOnOffCommand;

        public bool IsMute
        {
            get => _isMute;
            set => SetProperty(ref _isMute, value);
        }
        public DelegateCommand<bool?> VolumeUpDownCommand => _volumeUpDownCommand ?? (_volumeUpDownCommand = new DelegateCommand<bool?>(ExecuteVolumeUpDown));
        public DelegateCommand MuteOnOffCommand => _muteOnOffCommand ?? (_muteOnOffCommand = new DelegateCommand(ExecuteMuteOnOff));
        public DelegateCommand<bool?> MonitorPowerOnOffCommand => _monitorPowerOnOffCommand ?? (_monitorPowerOnOffCommand = new DelegateCommand<bool?>(ExecuteMonitorPowerOnOffCommand));

        public TvControlViewModel(string id, int index, LgDidControlClient lgDidControl)
        {
            _id = id;
            _muteKey = $"{MutePrefix}_{id}";
            _volumeKey = $"{VolumePreFix}_{id}";
            _index = index;
            _lgDidControl = lgDidControl;
            _iniFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, IniFileName);
            IsMute = GetMuteStatusFromIni();
        }

        private void ExecuteVolumeUpDown(bool? isUp)
        {
            if (_lgDidControl == null || isUp == null) return;
            var currentVolume = GetVolumeFromIni();
            int value;
            if ((bool)isUp)
            {
                value = currentVolume + 1;
                if (value > 100)
                {
                    value = 100;
                }
            }
            else
            {
                value = currentVolume - 1;
                if (value < 0)
                {
                    value = 0;
                }
            }
            _lgDidControl.ControlVolume(_index, value);
            SaveVolumeToIni(value);
        }
        private void ExecuteMuteOnOff()
        {
            if (_lgDidControl == null) return;
            _lgDidControl.ControlMute(_index, !IsMute);
            IsMute = !IsMute;
            SaveMuteStatusToIni((bool)IsMute);
        }
        private void ExecuteMonitorPowerOnOffCommand(bool? powerOnOff)
        {
            if (_lgDidControl == null || powerOnOff == null) return;

            _lgDidControl.ControlPower(_index, (bool)powerOnOff);
        }

        private void SaveVolumeToIni(int volume)
        {
            WritePrivateProfileString(VolumeSectionName, _volumeKey, volume.ToString(), _iniFilePath);
        }
        private int GetVolumeFromIni()
        {
            StringBuilder volumeStringBuilder = new StringBuilder();
            GetPrivateProfileString(VolumeSectionName, _volumeKey, "20", volumeStringBuilder, volumeStringBuilder.Capacity, _iniFilePath);
            var volumeString = volumeStringBuilder.ToString();
            int.TryParse(volumeString, out var volume);
            return volume;
        }
        private void SaveMuteStatusToIni(bool isMute)
        {
            WritePrivateProfileString(VolumeSectionName, _muteKey, isMute.ToString(), _iniFilePath);
        }
        private bool GetMuteStatusFromIni()
        {
            StringBuilder muteStringBuilder = new StringBuilder();
            GetPrivateProfileString(VolumeSectionName, _muteKey, "False", muteStringBuilder, muteStringBuilder.Capacity, _iniFilePath);
            var muteString = muteStringBuilder.ToString();
            bool.TryParse(muteString, out var isMute);
            return isMute;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _lgDidControl.Dispose();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
