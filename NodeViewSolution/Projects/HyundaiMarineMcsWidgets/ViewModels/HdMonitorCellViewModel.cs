﻿using System;
using System.Xml;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Mvvm;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdMonitorCellViewModel : HdCellViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private int _monitorNo = 1;
        private string _windowScale;
        private string _inputSource = "";
        private string _channelId = "";
        private string _channelName = "";
        private int _tileNumber = 0;
        private string _windowId = "";
        private IntRect _windowRect = new IntRect(0, 0, 1920, 1080);

        public string Id => $"{_monitorNo}";
        public int MonitorNo
        {
            get => _monitorNo;
            set => SetProperty(ref _monitorNo, value);
        }

        public string WindowScale
        {
            get => _windowScale;
            set
            {
                SetProperty(ref _windowScale, value);
                RaisePropertyChanged("WindowScaleLabel");
            } 
        }

        public string WindowScaleLabel => _windowScale == "1x1" ? "" : "2x2";

        public string InputSource
        {
            get => _inputSource;
            set
            {
                SetProperty(ref _inputSource, value);
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public string ChannelId
        {
            get => _channelId;
            set => SetProperty(ref _channelId, value);
        }

        public string ChannelName
        {
            get => _channelName;
            set
            {
                SetProperty(ref _channelName, value);
                RaisePropertyChanged("BackgroundColor");
            }
        }

        public int TileNumber
        {
            get => _tileNumber;
            set => SetProperty(ref _tileNumber, value);
        }

        public string WindowId
        {
            get => _windowId;
            set => SetProperty(ref _windowId, value);
        }

        public IntRect WindowRect
        {
            get => _windowRect;
            set => SetProperty(ref _windowRect, value);
        }
        public string BackgroundColor => (string.IsNullOrWhiteSpace(ChannelName)) ? "#55565E" : (InputSource == "wall") ? "#38974c" : "#cc8b28";

        public HdMonitorCellViewModel(int monitorNo, IntRect baseRect)
        {
            MonitorNo = monitorNo;
            BaseRect = baseRect.Clone();
            Rect = baseRect.Clone();
        }
        public static HdMonitorCellViewModel CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                var id = XmlUtils.GetAttrValue(xmlNode, "monitorNo", 1);
                var baseRect = XmlUtils.GetAttrValue(xmlNode, "baseRect", IntRect.Empty);
                var window = new HdMonitorCellViewModel(id, baseRect);
                if (window.LoadFrom(xmlNode))
                {
                    return window;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public HdMonitorCellViewModel Clone()
        {
            var clone = new HdMonitorCellViewModel(MonitorNo, BaseRect);
            clone.Rect = Rect;
            clone._windowScale = _windowScale;
            clone._inputSource = _inputSource;
            clone._channelId = _channelId;
            clone._channelName = _channelName;
            clone._tileNumber = _tileNumber;
            clone._windowId = _windowId;
            clone._windowRect = _windowRect;
            return clone;
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                MonitorNo = XmlUtils.GetAttrValue(xmlNode, "monitorNo", 1);
                BaseRect = XmlUtils.GetAttrValue(xmlNode, "baseRect", new IntRect(BaseRect));
                Rect = XmlUtils.GetAttrValue(xmlNode, "rect", new IntRect(Rect));
                WindowScale = XmlUtils.GetAttrValue(xmlNode, "windowScale", "");
                InputSource = XmlUtils.GetAttrValue(xmlNode, "inputSource", "");
                ChannelId = XmlUtils.GetAttrValue(xmlNode, "channelId", "");
                ChannelName = XmlUtils.GetAttrValue(xmlNode, "channelName", "");
                TileNumber = XmlUtils.GetAttrValue(xmlNode, "tileNumber", 0);
                WindowId = XmlUtils.GetAttrValue(xmlNode, "windowId", "");
                WindowRect = XmlUtils.GetAttrValue(xmlNode, "windowRect", new IntRect(WindowRect));
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "MonitorCell";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("monitorNo", MonitorNo.ToString());
            xmlWriter.WriteAttributeString("baseRect", $"{BaseRect}");
            xmlWriter.WriteAttributeString("rect", $"{Rect}");
            xmlWriter.WriteAttributeString("windowScale", WindowScale);
            xmlWriter.WriteAttributeString("inputSource", InputSource);
            xmlWriter.WriteAttributeString("channelId", ChannelId);
            xmlWriter.WriteAttributeString("channelName", ChannelName);
            xmlWriter.WriteAttributeString("tileNumber", TileNumber.ToString());
            xmlWriter.WriteAttributeString("windowId", WindowId);
            xmlWriter.WriteAttributeString("windowRect", $"{WindowRect}");

            xmlWriter.WriteEndElement();
        }
    }
}
