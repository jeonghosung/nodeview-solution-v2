﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using NLog;
using NodeView.Drawing;
using NodeView.Utils;
using NodeView.ViewModels;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdCellViewModel : SelectableViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IntRect _rect = new IntRect(0, 0, 1920, 1080);
        public IntRect BaseRect { get; set; }

        public IntRect Rect
        {
            get => _rect;
            set => SetProperty(ref _rect, value);
        }

        public void UpdateRect(IntPoint offset, double scale)
        {
            Rect = new IntRect(offset.X + (int)(scale * BaseRect.X), offset.Y + (int)(scale * BaseRect.Y), (int)(scale * BaseRect.Width), (int)(scale * BaseRect.Height));
        }
    }
}