﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.ViewModels;

namespace HyundaiMarineMcsWidgets.ViewModels
{
    public class HdWindowScaleViewModel : SelectableViewModel
    {
        public string Name { get; set; }

        public HdWindowScaleViewModel(string name)
        {
            Name = name;
        }
    }
}
