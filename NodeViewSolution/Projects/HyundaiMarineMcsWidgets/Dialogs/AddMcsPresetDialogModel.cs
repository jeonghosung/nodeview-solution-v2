﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HyundaiMarineMcsWidgets.ViewModels;
using NLog;
using NodeView.DataWalls;
using NodeView.Dialogs;
using NodeView.Utils;
using Prism.Services.Dialogs;

namespace HyundaiMarineMcsWidgets.Dialogs
{
    public class AddMcsPresetDialogModel : DialogModelBase
    {
        private string _errorMessage;
        private string _presetName;
        private string _presetId;
        private string _configFolder = "";
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private List<HdMcsPresetViewModel> _mcsPresets = new List<HdMcsPresetViewModel>();
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public string PresetName
        {
            get => _presetName;
            set => SetProperty(ref _presetName, value);
        }
        public string PresetId
        {
            get => _presetId;
            set => SetProperty(ref _presetId, value);
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "즐겨찾기 추가";
            _configFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            if (!parameters.TryGetValue("mcsPresets", out HdMcsPresetViewModel[] mcsPresets))
            {
                return;
            }
            if (mcsPresets != null && mcsPresets.Length > 0)
            {
                _mcsPresets = mcsPresets.ToList();
            }
        }
        protected override void ExecuteApplyDialogCommand()
        {
            if (_mcsPresets != null && _mcsPresets.Any(preset => preset.Id.Equals(PresetId, StringComparison.OrdinalIgnoreCase)))
            {
                ErrorMessage = "동일한 ID가 존재합니다.";
                PresetId = "";
                return;
            }

            if (string.IsNullOrWhiteSpace(PresetId) || string.IsNullOrWhiteSpace(PresetName))
            {
                ErrorMessage = "ID 또는 이름은 비워놓을 수 없습니다.";
                return;
            }

            var parameters = new DialogParameters();
            parameters.Add("presetId", PresetId);
            parameters.Add("presetName", PresetName);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
            /*
            var newPreset = new HdMcsPresetViewModel(Id, Name, _monitorCells);
            _mcsPresets?.Add(newPreset);
            SavePresets();
            base.ExecuteApplyDialogCommand();
            */
        }
    }
}
