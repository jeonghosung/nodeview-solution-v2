﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using HyundaiMarineMcsWidgets.Models;
using HyundaiMarineMcsWidgets.ViewModels;
using NodeView.DataWalls;
using NodeView.Dialogs;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace HyundaiMarineMcsWidgets.Dialogs
{
    public class ScheduleManagerDialogModel : DialogModelBase
    {
        private int _hour;
        private int _minute;
        private HdMcsPresetViewModel _currentPreset;
        private HdJobViewModel _currentHdJob;
        private ObservableCollection<HdMcsPresetViewModel> _presets = new ObservableCollection<HdMcsPresetViewModel>();
        private ObservableCollection<HdJobViewModel> _hdjobList = new ObservableCollection<HdJobViewModel>();

        private DelegateCommand _addJobCommand;
        private DelegateCommand<string> _deleteJobCommand;

        public int Hour
        {
            get => _hour;
            set => SetProperty(ref _hour, value);
        }
        public int Minute
        {
            get => _minute;
            set => SetProperty(ref _minute, value);
        }
        public HdMcsPresetViewModel CurrentPreset
        {
            get => _currentPreset;
            set => SetProperty(ref _currentPreset, value);
        }

        public HdJobViewModel CurrentHdJob
        {
            get => _currentHdJob;
            set => SetProperty(ref _currentHdJob, value);
        }
        public ObservableCollection<HdMcsPresetViewModel> Presets
        {
            get => _presets;
            set => SetProperty(ref _presets, value);
        }
        public ObservableCollection<HdJobViewModel> HdJobList
        {
            get => _hdjobList;
            set => SetProperty(ref _hdjobList, value);
        }
        public List<string> HourList { get; set; } = new List<string>()
        {
            "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17",
            "18", "19", "20", "21", "22","23"
        };
        public List<string> MinuteList { get; set; } = new List<string>()
        {
            "00", "10", "20", "30", "40", "50"
        };

        public DelegateCommand AddJobCommand => _addJobCommand ??
                                                     (_addJobCommand =
                                                         new DelegateCommand(ExecuteAddJobCommand));
        public DelegateCommand<string> DeleteJobCommand => _deleteJobCommand ??
                                                        (_deleteJobCommand =
                                                            new DelegateCommand<string>(ExecuteDeleteJobCommand));


        protected override void ExecuteCancelDialogCommand()
        {
            RaiseRequestClose(new DialogResult(ButtonResult.Cancel));
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("title", out string title))
            {
                if (!string.IsNullOrWhiteSpace(title))
                {
                    Title = title;
                }
            }

            if (parameters.TryGetValue("presets", out HdMcsPresetViewModel[] presets))
            {
                foreach (var preset in presets)
                {
                    Presets.Add(preset);
                }
            }

            if (parameters.TryGetValue("schedule", out HdJobViewModel[] schedule))
            {
                HdJobList.Clear();
                foreach (var job in schedule)
                {
                    HdJobList.Add(job.Clone());
                }
            }
        }
        
        protected override void ExecuteApplyDialogCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("schedule", HdJobList.ToArray());
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }
        private void ExecuteAddJobCommand()
        {
            AddJob();
        }

        private void AddJob()
        {
            var newJob = new HdJobViewModel(Hour, Minute, CurrentPreset.Id, CurrentPreset.Name);
            var newJobTotalMin = newJob.TotalMin;
            bool isAdded = false;
            for (int i = 0; i < HdJobList.Count; i++)
            {
                var currentJob = HdJobList[i];
                if (currentJob.TotalMin > newJobTotalMin)
                {
                    HdJobList.Insert(i, newJob);
                    isAdded = true;
                    break;
                }
            }

            if (!isAdded)
            {
                HdJobList.Add(newJob);
            }
        }

        private void ExecuteDeleteJobCommand(string id)
        {
            var schedule = HdJobList.ToArray();
            foreach (var job in schedule)
            {
                if (job.Id == id)
                {
                    HdJobList.Remove(job);
                    break;
                }
            }
        }
    }
}
