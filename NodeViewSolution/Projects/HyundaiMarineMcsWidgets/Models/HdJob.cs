﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HyundaiMarineMcsWidgets.Models
{
    public class HdJob 
    {
        public int Hour { get; set; }
        public int Min { get; set; }
        public string PresetId { get; set; }
    }
}
