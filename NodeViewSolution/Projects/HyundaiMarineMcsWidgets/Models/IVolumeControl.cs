﻿namespace HyundaiMarineMcsWidgets.Models
{
    public interface IVolumeControl : IControlBase
    {
        void VolumeUpDown(string index, bool isUp);
    }
}
