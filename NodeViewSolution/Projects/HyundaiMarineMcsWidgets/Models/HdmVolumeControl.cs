﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HyundaiMarineMcsWidgets.Models;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Utils;
using Prism.Commands;

namespace HyundaiMarineMcsWidgets.Models
{
    public class HdmVolumeControl : IVolumeControl
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly bool _isTestMode;
        private readonly SerialPort _speakerVolumeSerialPort = null;

        public HdmVolumeControl(SerialPort serialPort, bool isTestMode)
        {
            _speakerVolumeSerialPort = serialPort;
            _isTestMode = isTestMode;
        }
        public void VolumeUpDown(string channel, bool isUp)
        {
            try
            {
                if (_isTestMode)
                {
                    Logger.Info($"Port : {_speakerVolumeSerialPort.PortName} BaudRate : {_speakerVolumeSerialPort.BaudRate}");
                    Logger.Info($"Channel : {channel} IsUp : {isUp}");
                    return;
                }
                using (new SerialConnection(_speakerVolumeSerialPort))
                {
                    string command = isUp ? $"{channel} up\rr\r" : $"{channel} dn\rr\r";
                    Logger.Info($"Volume Command : {command}");
                    _speakerVolumeSerialPort.ReadExisting();
                    _speakerVolumeSerialPort.WriteLine(command);
                    Thread.Sleep(100);
                    _speakerVolumeSerialPort.ReadExisting();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlPower:");
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
