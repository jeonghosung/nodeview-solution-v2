﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NodeView.DataModels;
using NodeView.DeviceControllers;
using NodeView.Drawing;
using NodeView.Extension.Plugins;
using NodeView.Frameworks.Widgets;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace HyundaiMarineMcsWidgets.Widgets
{
    public class MatrixControlWidgetModelOld : BindableBase, IScreenAreaViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IntSize _monitorMatrix;
        private const int BaseWidth = 1920;
        private const int BaseHeight = 1080;
        private IntSize _screenSize = new IntSize(1920, 1080);
        private LgDidControlClient _didController;
        private string _curnetMatrix = "1X1";

        protected ObservableCollection<MonitorInputSource> _inputSources = new ObservableCollection<MonitorInputSource>();
        private ObservableCollection<MonitorCellViewModel> _monitorCells = new ObservableCollection<MonitorCellViewModel>();

        private DelegateCommand<string> _selectCellCommand;
        private DelegateCommand<string> _setMatrixCommand;

        public ObservableCollection<MonitorInputSource> InputSources
        {
            get => _inputSources;
            set => SetProperty(ref _inputSources, value);
        }
        public ObservableCollection<MonitorCellViewModel> MonitorCells
        {
            get => _monitorCells;
            set => SetProperty(ref _monitorCells, value);
        }

        public DelegateCommand<string> SelectCellCommand => _selectCellCommand ?? (_selectCellCommand = new DelegateCommand<string>(ExecuteSelectCellCommand));

        public DelegateCommand<string> SetMatrixCommand => _setMatrixCommand ?? (_setMatrixCommand = new DelegateCommand<string>(ExecuteSetMatrixCommand));

        public MatrixControlWidgetModelOld(IDialogService dialogService, IWidgetSource widgetSource)
        {
            LoadConfig((IConfiguration)widgetSource.Config);
            CreateMonitorCells();
        }

        private void LoadConfig(IConfiguration config)
        {
            if (config.TryGetNode("boardInfo", out var boardControlNode))
            {
                var boardControlConfig = (Configuration)boardControlNode;

                _monitorMatrix = boardControlConfig.GetValue("matrix", new IntSize(1, 1));
                var connectionString = boardControlConfig.GetValue("connectionString", "");
                var isTestMode = boardControlConfig.GetValue("isTestMode", false); ;
                var isVerbose = isTestMode || boardControlConfig.GetValue("isVerbose", false);
                _didController = new LgDidControlClient(connectionString, isTestMode, isVerbose);
                _didController.Init();
                if (boardControlConfig.TryGetNode("inputSources", out var inputsSection))
                {
                    foreach (var property in inputsSection.Properties)
                    {
                        var source = MonitorInputSource.CreateFrom(property);
                        if (source != null)
                        {
                            InputSources.Add(source);
                        }
                    }
                }
            }
        }
        private void CreateMonitorCells()
        {
            MonitorCells.Clear();
            if (!_monitorMatrix.IsEmpty)
            {
                int seq = 1;
                for (int i = 0; i < _monitorMatrix.Height; i++)
                {
                    for (int j = 0; j < _monitorMatrix.Width; j++)
                    {
                        MonitorCells.Add(new MonitorCellViewModel(seq++, new IntRect(j * BaseWidth, i * BaseHeight, BaseWidth, BaseHeight)));
                    }
                }
            }

            UpdateRectMonitorCells();
        }
        private void UpdateRectMonitorCells()
        {
            double baseTotalWidth = BaseWidth * _monitorMatrix.Width;
            double baseTotalHeight = BaseHeight * _monitorMatrix.Height;
            double scale = Math.Min(_screenSize.Width / baseTotalWidth, _screenSize.Height / baseTotalHeight);
            int offsetX = (int)((_screenSize.Width - (baseTotalWidth * scale)) / 2);
            int offsetY = (int)((_screenSize.Height - (baseTotalHeight * scale)) / 2);
            var offset = new IntPoint(offsetX, offsetY);

            foreach (var monitorCell in MonitorCells.ToArray())
            {
                monitorCell.UpdateRectFromBaseRect(offset, scale);
            }
        }

        private void ExecuteSelectCellCommand(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    return;
                }

                var foundMonitorCell = FindScreenCell(id);
                if (foundMonitorCell == null)
                {
                    return;
                }

                string[] currentMatrix = _curnetMatrix.Split('X');
                int[] widthHeight = Array.ConvertAll(currentMatrix, s => int.Parse(s));
                var startIndex = foundMonitorCell.Seq;

                for (int height = 1; height <= widthHeight[1]; height++)
                {
                    for (int width = 1; width <= widthHeight[0]; width++)
                    {
                        var index = width + (height - 1) * widthHeight[0];
                        _didController.ControlTile(_monitorMatrix.Width * (height - 1) + startIndex + width - 1, widthHeight[0], widthHeight[1], index);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteSelectCellCommand:");
            }
        }
        private void ExecuteSetMatrixCommand(string matrix)
        {
            _curnetMatrix = matrix;
        }
        private MonitorCellViewModel FindScreenCell(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            MonitorCellViewModel foundScreenCell = null;
            foreach (var cell in MonitorCells.ToArray())
            {
                if (cell.Id == id)
                {
                    foundScreenCell = cell;
                    break;
                }
            }

            return foundScreenCell;
        }


        public void UpdatedScreenAreaSize(IntSize screenAreaSize)
        {
            if (_screenSize != screenAreaSize)
            {
                _screenSize = screenAreaSize;
                UpdateRectMonitorCells();
            }
        }
    }
}
