﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using HyundaiMarineMcsWidgets.Dialogs;
using HyundaiMarineMcsWidgets.ViewModels;
using Newtonsoft.Json.Linq;
using NLog;
using NLog.Fluent;
using NodeView.DataModels;
using NodeView.DeviceControllers;
using NodeView.Drawing;
using NodeView.Extension.Plugins;
using NodeView.Extension.Protocols;
using NodeView.Frameworks.Widgets;
using NodeView.Ioc;
using NodeView.Net;
using NodeView.Tasks;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Ioc;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace HyundaiMarineMcsWidgets.Widgets
{
    public class HdMcsWidgetModel : BindableBase, IScreenAreaViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const int MonitorMatrixX = 8;
        private const int MonitorMatrixY = 2;
        private const int BaseWidth = 1920;
        private const int BaseHeight = 1080;

        private string _configFolder = "";
        private string _cacheFolder = "";

        private readonly IDialogService _dialogService = null;

        private IntSize _screenSize = new IntSize(1920, 1080);
        private double _screenScale = 1;
        private IntPoint _screenOffset = new IntPoint(0, 0);

        private LgDidControlClient _didController = null;
        private NodeViewDataWallClient _dataWallClient = null;
        private MatrixGm2400Client _matrixClient = null;

        private HdMonitorCellViewModel[] _monitorCells = new HdMonitorCellViewModel[0];
        private HdWindowScaleViewModel[] _windowScales = new HdWindowScaleViewModel[0];
        private HdContentViewModel[] _contents = new HdContentViewModel[0];
        private HdJobViewModel[] _jobSchedule = new HdJobViewModel[0];

        private string _contentsFile = "hdContents.xml";
        private int _wallInputSourceValue;
        private int _matrixInputSourceValue;

        private DelegateCommand<string> _toggleContentSelectionCommand;
        private HdContentViewModel _selectedContent;
        private DelegateCommand<string> _windowScaleSelectionCommand;
        private HdWindowScaleViewModel _selectedWindowScale;
        private DelegateCommand<string> _monitorCellSelectionCommand;
        private DelegateCommand _setIpWallModeCommand;
        private DelegateCommand _setMatrixModeCommand;
        private DelegateCommand _openAddPresetDialogCommand;
        private DelegateCommand<string> _runPresetCommand;
        private DelegateCommand<string> _deletePresetCommand;
        private DelegateCommand _openScheduleManagerDialogCommand;
        private DelegateCommand _toggleScheduleRunCommand;

        private volatile bool _isScheduleRun = false;
        private readonly Timer _checkingScheduleTimer = null;
        private int _lastCheckingScheduleTime = -1;

        public IntSize ScreenSize
        {
            get => _screenSize;
            set => SetProperty(ref _screenSize, value);
        }
        public HdMonitorCellViewModel[] MonitorCells
        {
            get => _monitorCells;
            set => SetProperty(ref _monitorCells, value);
        }
        public ObservableCollection<HdWindowCellViewModel> WindowCells { get; } = new ObservableCollection<HdWindowCellViewModel>();

        public HdWindowScaleViewModel[] WindowScales
        {
            get => _windowScales;
            set => SetProperty(ref _windowScales, value);
        }
        public HdContentViewModel[] Contents
        {
            get => _contents;
            set => SetProperty(ref _contents, value);
        }

        public ObservableCollection<HdMcsPresetViewModel> McsPresets { get; } = new ObservableCollection<HdMcsPresetViewModel>();
        public SelectableViewModel ToggleScheduleRun { get; } = new SelectableViewModel();
        public HdMcsWidgetModel(IDialogService dialogService, IWidgetSource widgetSource)
        {
            _dialogService = dialogService;
            _configFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            _cacheFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");

            IContainerRegistry containerProvider = NodeViewContainerExtension.GetContainerRegistry();
            containerProvider.RegisterDialog<ScheduleManagerDialog, ScheduleManagerDialogModel>();
            containerProvider.RegisterDialog<AddMcsPresetDialog, AddMcsPresetDialogModel>();

            LoadConfig((IConfiguration)widgetSource.Config);
            Init();

            _checkingScheduleTimer = new Timer(CheckingScheduleTimerCallBack);
            _checkingScheduleTimer.Change(TimeSpan.FromSeconds(60), TimeSpan.FromSeconds(60));
        }

        private void LoadConfig(IConfiguration config)
        {
            _contentsFile = config.GetValue("contents", _contentsFile);
            bool isScheduleRunOnStartup = config.GetValue("isScheduleRunOnStartup", false);
            if (isScheduleRunOnStartup)
            {
                _isScheduleRun = true;
                ToggleScheduleRun.IsSelected = _isScheduleRun;
            }

            if (config.TryGetConfiguration("dataWall", out var dataWallConfig))
            {
                var isTestMode = dataWallConfig.GetValue("isTestMode", false);
                var isVerbose = isTestMode || dataWallConfig.GetValue("isVerbose", false);
                var connectionString = dataWallConfig.GetValue("connectionString", "");
                if (connectionString.StartsWith("http://"))
                {
                    _dataWallClient = new NodeViewDataWallClient(connectionString.TrimEnd('/'));
                    _dataWallClient.IsTestMode = isTestMode;
                }
                else
                {
                    Logger.Error("dataWall connectionString is empty or mismatched!");
                }
            }

            if (config.TryGetConfiguration("matrix", out var matrixConfig))
            {
                var isTestMode = matrixConfig.GetValue("isTestMode", false);
                var isVerbose = isTestMode || matrixConfig.GetValue("isVerbose", false);
                var connectionString = matrixConfig.GetValue("connectionString", "");
                if (connectionString.StartsWith("tcp://"))
                {
                    int port = 9096;
                    try
                    {
                        string[] connectionParams = StringUtils.Split(connectionString.Substring(6), ':');
                        string ip = connectionParams[0];
                        if (connectionParams.Length > 1)
                        {
                            port = StringUtils.GetIntValue(connectionParams[1], port);
                        }

                        _matrixClient = new MatrixGm2400Client(ip, port);
                        _matrixClient.IsTestMode = isTestMode;
                    }
                    catch (Exception ex)
                    {
                        Logger.Error(ex, "matrix:");
                    }
                }
                else
                {
                    Logger.Error("matrix connectionString is or mismatched!");
                }
            }

            if (config.TryGetNode("did", out var didConfig))
            {
                var isTestMode = didConfig.GetValue("isTestMode", false);
                var isVerbose = isTestMode || didConfig.GetValue("isVerbose", false);
                var connectionString = didConfig.GetValue("connectionString", "");
                if (connectionString.StartsWith("serial://"))
                {
                    _didController = new LgDidControlClient(connectionString, isTestMode, isVerbose);
                    _didController.Init();
                }
                else
                {
                    Logger.Error("did connectionString is or mismatched!");
                }

                _wallInputSourceValue = didConfig.GetValue("inputSources.wall", 80);
                _matrixInputSourceValue = didConfig.GetValue("inputSources.matrix", 81);
            }
        }
        private void Init()
        {
            WindowScales = new HdWindowScaleViewModel[2];
            WindowScales[0] = new HdWindowScaleViewModel("1x1");
            WindowScales[0].IsSelected = true;
            _selectedWindowScale = WindowScales[0];
            WindowScales[1] = new HdWindowScaleViewModel("2x2");
            LoadContents();
            LoadScreen();
            LoadPresets();
            LoadSchedule();
        }

        private void LoadContents()
        {
            List<HdContentViewModel> contents = new List<HdContentViewModel>();
            try
            {
                string filePath = System.IO.Path.Combine(_configFolder, _contentsFile);
                if (!File.Exists(filePath))
                {
                    Logger.Error($"Cannot find Contents file.");
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNodeList contentNodeList = doc.SelectNodes("/Contents/Content");
                if (contentNodeList == null || contentNodeList.Count == 0)
                {
                    return;
                }

                int index = 1;
                foreach (XmlNode contentNode in contentNodeList)
                {
                    string inputSource = XmlUtils.GetStringAttrValue(contentNode, "inputSource");
                    string channel = XmlUtils.GetStringAttrValue(contentNode, "channel");
                    string name = XmlUtils.GetStringAttrValue(contentNode, "name");

                    if (string.IsNullOrWhiteSpace(inputSource) || string.IsNullOrWhiteSpace(channel) || string.IsNullOrWhiteSpace(name))
                    {
                        continue;
                    }
                    contents.Add(new HdContentViewModel($"content{index}", inputSource, channel, name));
                    index++;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"LoadContents:");
            }

            Contents = contents.ToArray();
        }

        private void LoadScreen()
        {
            MonitorCells = new HdMonitorCellViewModel[MonitorMatrixX * MonitorMatrixY];

            int index = 0;
            for (int y = 0; y < MonitorMatrixY; y++)
            {
                for (int x = 0; x < MonitorMatrixX; x++)
                {
                    MonitorCells[index] = new HdMonitorCellViewModel(index + 1, new IntRect(x* BaseWidth, y*BaseHeight, BaseWidth, BaseHeight));
                    index++;
                }
            }

            try
            {
                string filePath = System.IO.Path.Combine(_cacheFolder, "hdScreen.xml");
                if (!File.Exists(filePath))
                {
                    SetIpWallMode();
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNodeList cellNodeList = doc.SelectNodes("/Screen/Cell");
                if (cellNodeList == null || cellNodeList.Count == 0)
                {
                    return;
                }

                List<HdWindowCellViewModel> windowCells = new List<HdWindowCellViewModel>();
                foreach (XmlNode cellNode in cellNodeList)
                {
                    HdWindowCellViewModel windowCell = HdWindowCellViewModel.CreateFrom(cellNode);
                    if (windowCell.MonitorNumbers.Length > 0)
                    {
                        windowCells.Add(windowCell);
                    }
                }
                WindowCells.AddRange(windowCells);
                UpdateMonitorCells(WindowCells);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"LoadContents:");
            }

            UpdateCellsRect();
        }

        private void UpdateMonitorCells(IEnumerable<HdWindowCellViewModel> windowCells)
        {
            foreach (var monitorCell in MonitorCells)
            {
                monitorCell.WindowScale = "";
                monitorCell.ChannelId = "";
                monitorCell.ChannelName = "";
                monitorCell.TileNumber = 0;
                monitorCell.WindowId = "";
                monitorCell.WindowRect = monitorCell.BaseRect.Clone();
            }

            foreach (var windowCell in windowCells)
            {
                int tileNumber = 1;
                foreach (int monitorNumber in windowCell.MonitorNumbers)
                {
                    if (monitorNumber > 0 && monitorNumber <= MonitorCells.Length)
                    {
                        var monitorCell = MonitorCells[monitorNumber - 1];
                        monitorCell.WindowScale = windowCell.WindowScale;
                        monitorCell.InputSource = windowCell.InputSource;
                        monitorCell.ChannelId = windowCell.ChannelId;
                        monitorCell.ChannelName = windowCell.ChannelName;
                        monitorCell.TileNumber = tileNumber;
                        monitorCell.WindowId = windowCell.Id;
                        monitorCell.WindowRect = windowCell.BaseRect.Clone();
                    }

                    tileNumber++;
                }
            }
        }

        private void CheckingScheduleTimerCallBack(object state)
        {
            var now = DateTime.Now;
            int totalMin = now.Hour * 60 + now.Minute;

            if (_lastCheckingScheduleTime == totalMin)
            {
                return;
            }

            _lastCheckingScheduleTime = totalMin;
            if (!_isScheduleRun)
            {
                return;
            }

            var schedule = _jobSchedule.ToArray();
            foreach (var job in schedule)
            {
                if (job.TotalMin == totalMin)
                {
                    ThreadAction.PostOnUiThread(() =>
                    {
                        RunPreset(job.PresetId);
                    });
                }
            }
        }

        private void SetIpWallMode()
        {
            ClearSelection();
            WindowCells.Clear();
            _dataWallClient?.CloseAllWindows();

            foreach (var monitorCell in _monitorCells)
            {
                _didController?.ControlInputSource(monitorCell.MonitorNo, _wallInputSourceValue);
                _didController?.ControlTile(monitorCell.MonitorNo, 1, 1);

                monitorCell.WindowScale = "1x1";
                monitorCell.InputSource = "";
                monitorCell.ChannelId = "";
                monitorCell.ChannelName = "";
                monitorCell.TileNumber = 0;
                monitorCell.WindowId = "";
                monitorCell.WindowRect = new IntRect(0, 0, 0, 0);
            }
            SaveScreen();
        }
        private void SetMatrixMode()
        {
            ClearSelection();
            _dataWallClient?.CloseAllWindows();

            foreach (var monitorCell in _monitorCells)
            {
                _didController?.ControlInputSource(monitorCell.MonitorNo, _matrixInputSourceValue);
                _didController?.ControlTile(monitorCell.MonitorNo, 1, 1);

                monitorCell.WindowScale = "1x1";
                monitorCell.InputSource = "matrix";
                monitorCell.ChannelId = "";
                monitorCell.ChannelName = "";
                monitorCell.TileNumber = 0;
                monitorCell.WindowId = "";
                monitorCell.WindowRect = new IntRect(0, 0, 0, 0);
            }
            SaveScreen();
        }

        private void SaveScreen()
        {
            try
            {
                string filePath = System.IO.Path.Combine(_cacheFolder, "hdScreen.xml");
                using (var xmlWriter = XmlUtils.CreateXmlWriter(filePath))
                {
                    xmlWriter.WriteStartElement("Screen");
                    foreach (var windowCell in WindowCells)
                    {
                        if (windowCell.MonitorNumbers.Length > 0)
                        {
                            windowCell.WriteXml(xmlWriter, "Cell");
                        }
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot SaveScreen:");
            }
        }

        public DelegateCommand<string> ToggleContentSelectionCommand =>
            _toggleContentSelectionCommand ?? (_toggleContentSelectionCommand = new DelegateCommand<string>(ExecuteToggleContentSelectionCommand));

        void ExecuteToggleContentSelectionCommand(string id)
        {
            if (_selectedContent != null)
            {
                string lastSelectedId = _selectedContent.Id;
                _selectedContent.IsSelected = false;
                _selectedContent = null;
                if (lastSelectedId == id)
                {
                    return;
                }
            }

            foreach (var hdContent in Contents)
            {
                if (hdContent.Id == id)
                {
                    _selectedContent = hdContent;
                    hdContent.IsSelected = true;
                }
            }
        }

        public DelegateCommand<string> WindowScaleSelectionCommand =>
            _windowScaleSelectionCommand ?? (_windowScaleSelectionCommand = new DelegateCommand<string>(ExecuteWindowScaleSelectionCommand));

        void ExecuteWindowScaleSelectionCommand(string windowScaleName)
        {
            if (_selectedWindowScale != null)
            {
                if (_selectedWindowScale.Name == windowScaleName)
                {
                    return;
                }
            }

            foreach (var windowScale in WindowScales)
            {
                if (windowScale.Name == windowScaleName)
                {
                    _selectedWindowScale = windowScale;
                    windowScale.IsSelected = true;
                }
                else
                {
                    windowScale.IsSelected = false;
                }
            }
        }

        public DelegateCommand<string> MonitorCellSelectionCommand =>
            _monitorCellSelectionCommand ?? (_monitorCellSelectionCommand = new DelegateCommand<string>(ExecuteMonitorCellSelectionCommand));

        void ExecuteMonitorCellSelectionCommand(string monitorNoString)
        {
            if (_selectedContent != null && _selectedWindowScale != null)
            {
                int monitorNo = StringUtils.GetIntValue(monitorNoString, 0);
                if (monitorNo > 0 && monitorNo <= MonitorCells.Length)
                {
                    SetContent(monitorNo, _selectedWindowScale.Name, _selectedContent);
                }
            }
        }

        public DelegateCommand SetIpWallModeCommand => (_setIpWallModeCommand ?? (_setIpWallModeCommand = new DelegateCommand(ExecuteSetIpWallModeCommand)));

        private void ExecuteSetIpWallModeCommand()
        {
            if (MessageBox.Show("상황판 화면을 초기화 하시겠습니까?", "상황판 초기화", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                SetIpWallMode();
            }
        }

        public DelegateCommand SetMatrixModeCommand => (_setMatrixModeCommand ?? (_setMatrixModeCommand = new DelegateCommand(ExecuteSetMatrixModeCommand)));

        private void ExecuteSetMatrixModeCommand()
        {
            SetMatrixMode();
        }

        public DelegateCommand OpenAddPresetDialogCommand =>
            (_openAddPresetDialogCommand ?? (_openAddPresetDialogCommand = new DelegateCommand(ExecuteOpenAddPresetDialogCommand)));

        private void ExecuteOpenAddPresetDialogCommand()
        {
            var parameters = new DialogParameters
            {
                {"mcsPresets", McsPresets.ToList()}
            };
            _dialogService.ShowDialog("AddMcsPresetDialog", parameters, response =>
            {
                if (response.Result == ButtonResult.OK)
                {
                    if (response.Parameters.TryGetValue("presetId", out string presetId))
                    {
                        if (response.Parameters.TryGetValue("presetName", out string presetName))
                        {
                            if (!string.IsNullOrWhiteSpace(presetId) && !string.IsNullOrWhiteSpace(presetId))
                            {
                                List<HdWindowCellViewModel> windowCells = new List<HdWindowCellViewModel>();
                                foreach (var windowCell in WindowCells)
                                {
                                    windowCells.Add(windowCell.Clone());
                                }
                                McsPresets.Add(new HdMcsPresetViewModel(presetId, presetName, windowCells.ToArray()));
                                SavePresets();
                            }
                        }
                    }
                }
            });
        }

        public DelegateCommand<string> RunPresetCommand =>
            (_runPresetCommand ?? (_runPresetCommand = new DelegateCommand<string>(ExecuteRunPresetCommand)));

        private void ExecuteRunPresetCommand(string id)
        {
            var presets = McsPresets.ToArray();
            foreach (var preset in presets)
            {
                if (preset.Id == id)
                {
                    if (MessageBox.Show($"프리셋 '{preset.Name}'을 적용하시겠습니까?", "프리셋 적용", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                    {
                        RunPreset(preset);
                        break;
                    }
                }
            }
        }

        public DelegateCommand<string> DeletePresetCommand =>
            (_deletePresetCommand ?? (_deletePresetCommand = new DelegateCommand<string>(ExecuteDeletePresetCommand)));

        private void ExecuteDeletePresetCommand(string id)
        {
            var mcsPresets = McsPresets.ToArray();
            foreach (var preset in mcsPresets)
            {
                if (preset.Id == id)
                {
                    if (MessageBox.Show($"프리셋 '{preset.Name}'을 삭제하시겠습니까?", "프리셋 삭제", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    { 
                        DeletePreset(id);
                        break;
                    }
                }
            }
        }

        public DelegateCommand OpenScheduleManagerDialogCommand => _openScheduleManagerDialogCommand ?? (_openScheduleManagerDialogCommand = new DelegateCommand(ExecuteScheduleManagerDialogCommand));
        private void ExecuteScheduleManagerDialogCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "스케쥴 관리");

            List<HdJobViewModel> currentSchedule = new List<HdJobViewModel>();
            foreach (var job in _jobSchedule)
            {
                currentSchedule.Add(job.Clone());
            }
            parameters.Add("presets", McsPresets.ToArray());
            parameters.Add("schedule", currentSchedule.ToArray());
            _dialogService.ShowDialog("ScheduleManagerDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                    if (r.Parameters.TryGetValue("schedule", out HdJobViewModel[] newSchedule))
                    {
                        _jobSchedule = newSchedule;
                        SaveSchedule();
                    }
                }
            });
        }

        public DelegateCommand ToggleScheduleRunCommand => _toggleScheduleRunCommand ?? (_toggleScheduleRunCommand = new DelegateCommand(ExecuteToggleScheduleRunCommand));
        private void ExecuteToggleScheduleRunCommand()
        {
            if (_isScheduleRun)
            {
                if (MessageBox.Show("스케줄 자동실행을 종료하시겠습니까?", "스케줄 종료", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    _isScheduleRun = false;
                    ToggleScheduleRun.IsSelected = _isScheduleRun;
                }
            }
            else
            {
                if (MessageBox.Show("스케줄 자동실행을 시작하시겠습니까?", "스케줄 시작", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    _isScheduleRun = true;
                    ToggleScheduleRun.IsSelected = _isScheduleRun;
                }
            }
        }

        private void SetContent(int monitorNo, string windowScale, HdContentViewModel content)
        {
            if (monitorNo <= 0 || monitorNo > MonitorCells.Length)
            {
                return;
            }

            try
            {
                Dictionary<string, string> deletedWindowDictionary = new Dictionary<string, string>();
                if (windowScale == "1x1")
                {
                    var monitorCell = MonitorCells[monitorNo - 1];

                    string windowId = Uuid.NewUuid;
                    IntRect windowRect = monitorCell.BaseRect.Clone();

                    if (!string.IsNullOrWhiteSpace(monitorCell.WindowId))
                    {
                        deletedWindowDictionary[monitorCell.WindowId] = monitorCell.WindowId;
                    }

                    if (monitorCell.InputSource != content.InputSource)
                    {
                        if (content.InputSource == "wall")
                        {
                            _didController?.ControlInputSource(monitorCell.MonitorNo, _wallInputSourceValue);
                        }
                        else
                        {
                            _didController?.ControlInputSource(monitorCell.MonitorNo, _matrixInputSourceValue);
                        }
                    }
                    if (monitorCell.WindowScale != windowScale)
                    {
                        _didController?.ControlTile(monitorCell.MonitorNo, 1, 1);
                    }


                    if (content.InputSource == "wall")
                    {
                        //Create Wall window.
                        bool res = _dataWallClient?.CreateWindow(windowRect, content.Channel, windowId) ?? false;
                        
                        monitorCell.WindowScale = windowScale;
                        monitorCell.InputSource = content.InputSource;
                        monitorCell.ChannelId = content.Channel;
                        monitorCell.ChannelName = content.Name;
                        monitorCell.TileNumber = 0;
                        monitorCell.WindowId = windowId;
                        monitorCell.WindowRect = windowRect;
                    }
                    else
                    {
                        //메트리스 제어
                        if (byte.TryParse(content.Channel, out byte inputNo))
                        {
                            try
                            {
                                if (_matrixClient != null)
                                {
                                    _matrixClient.ConnectVideo(inputNo, (byte)monitorNo);
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Error(e, "control matrix:");
                            }
                        }
                        else
                        {
                            Logger.Error("content.Channel is not byte value.");
                        }
                        
                        monitorCell.WindowScale = windowScale;
                        monitorCell.InputSource = content.InputSource;
                        monitorCell.ChannelId = content.Channel;
                        monitorCell.ChannelName = content.Name;
                        monitorCell.TileNumber = 0;
                        monitorCell.WindowId = windowId;
                        monitorCell.WindowRect = windowRect;
                    }

                    var windowCell = new HdWindowCellViewModel(windowId, windowRect);
                    windowCell.WindowScale = windowScale;
                    windowCell.InputSource = content.InputSource;
                    windowCell.ChannelId = content.Channel;
                    windowCell.ChannelName = content.Name;
                    windowCell.MonitorNumbers = new[] {monitorCell.MonitorNo};
                    WindowCells.Add(windowCell);
                    UpdateCellRect(windowCell);
                }
                else //2x2
                {
                    int startIndex = Math.Min(MonitorMatrixX-2, (monitorNo - 1) % MonitorMatrixX);
                    var monitorCells = new HdMonitorCellViewModel[4];
                    monitorCells[0] = MonitorCells[startIndex];
                    monitorCells[1] = MonitorCells[startIndex+1];
                    monitorCells[2] = MonitorCells[startIndex+MonitorMatrixX];
                    monitorCells[3] = MonitorCells[startIndex + MonitorMatrixX + 1];

                    string windowId = Uuid.NewUuid;
                    IntRect windowRect = new IntRect(monitorCells[0].BaseRect.X, monitorCells[0].BaseRect.Y,
                        monitorCells[0].BaseRect.Width * 2, monitorCells[0].BaseRect.Height * 2);

                    foreach (var monitorCell in monitorCells)
                    {
                        if (!string.IsNullOrWhiteSpace(monitorCell.WindowId))
                        {
                            deletedWindowDictionary[monitorCell.WindowId] = monitorCell.WindowId;
                        }
                    }

                    for (int i = 0; i < 4; i++)
                    {
                        if ((monitorCells[i].WindowScale != windowScale) || (monitorCells[i].InputSource != content.InputSource))
                        {
                            if (content.InputSource == "wall")
                            {
                                _didController?.ControlInputSource(monitorCells[i].MonitorNo, _wallInputSourceValue);
                                _didController?.ControlTile(monitorCells[i].MonitorNo, 1, 1);
                            }
                            else
                            {
                                _didController?.ControlInputSource(monitorCells[i].MonitorNo, _matrixInputSourceValue);
                                _didController?.ControlTile(monitorCells[i].MonitorNo, 2, 2, i + 1);
                            }
                        }
                    }


                    if (content.InputSource == "wall")
                    {
                        //Wall popup

                        bool res = _dataWallClient?.CreateWindow(windowRect, content.Channel, windowId) ?? false;

                        for (int i = 0; i < 4; i++)
                        {
                            monitorCells[i].WindowScale = windowScale;
                            monitorCells[i].InputSource = content.InputSource;
                            monitorCells[i].ChannelId = content.Channel;
                            monitorCells[i].ChannelName = content.Name;
                            monitorCells[i].TileNumber = 0;
                            monitorCells[i].WindowId = windowId;
                            monitorCells[i].WindowRect = windowRect;
                        }
                    }
                    else
                    {
                        //메트리스 제어
                        for (int i = 0; i < 4; i++)
                        {
                            if (byte.TryParse(content.Channel, out byte inputNo))
                            {
                                try
                                {
                                    if (_matrixClient != null)
                                    {
                                        _matrixClient.ConnectVideo(inputNo, (byte)monitorCells[i].MonitorNo);
                                    }
                                }
                                catch (Exception e)
                                {
                                    Logger.Error(e, "control matrix:");
                                }
                            }
                            else
                            {
                                Logger.Error("content.Channel is not byte value.");
                            }

                            monitorCells[i].WindowScale = windowScale;
                            monitorCells[i].InputSource = content.InputSource;
                            monitorCells[i].ChannelId = content.Channel;
                            monitorCells[i].ChannelName = content.Name;
                            monitorCells[i].TileNumber = i+1;
                            monitorCells[i].WindowId = windowId;
                            monitorCells[i].WindowRect = windowRect;
                        }
                    }
                    var windowCell = new HdWindowCellViewModel(windowId, windowRect);
                    windowCell.WindowScale = windowScale;
                    windowCell.InputSource = content.InputSource;
                    windowCell.ChannelId = content.Channel;
                    windowCell.ChannelName = content.Name;
                    windowCell.MonitorNumbers = monitorCells.Select((x)=>x.MonitorNo).ToArray();
                    WindowCells.Add(windowCell);
                    UpdateCellRect(windowCell);

                }

                // window close
                foreach (string deleteWindowId in deletedWindowDictionary.Keys)
                {
                    bool matched = false;
                    foreach (var monitorCell in _monitorCells)
                    {
                        if (monitorCell.WindowId == deleteWindowId)
                        {
                            matched = true;
                            break;
                        }
                    }

                    if (!matched)
                    {
                        var windowCells = WindowCells.ToArray();
                        foreach (var windowCell in windowCells)
                        {
                            if (windowCell.Id == deleteWindowId)
                            {
                                if (windowCell.InputSource == "wall")
                                {
                                    _dataWallClient.CloseWindow(deleteWindowId);
                                }

                                WindowCells.Remove(windowCell);
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetContent:");
            }

            ClearSelection();
            SaveScreen();
        }

        private void RunPreset(string id)
        {
            var presets = McsPresets.ToArray();
            foreach (var preset in presets)
            {
                if (preset.Id == id)
                {
                    RunPreset(preset);
                    break;
                }
            }
        }

        private void RunPreset(HdMcsPresetViewModel preset)
        {
            if (preset == null || preset.WindowCells == null)
            {
                return;
            }

            Logger.Info($"RunPreset [{preset.Id}/{preset.Name}]");

            WindowCells.Clear();
            foreach (var windowCell in preset.WindowCells)
            {
                WindowCells.Add(windowCell);
            }
            UpdateMonitorCells(WindowCells.ToArray());

            _dataWallClient?.CloseAllWindows();


            Dictionary<string, string> windowIdDictionary = new Dictionary<string, string>();
            foreach (var monitorCell in MonitorCells)
            {
                if (monitorCell.InputSource == "wall")
                {
                    _didController?.ControlInputSource(monitorCell.MonitorNo, _wallInputSourceValue);
                    _didController?.ControlTile(monitorCell.MonitorNo, 1, 1);
                    if (!string.IsNullOrWhiteSpace(monitorCell.WindowId))
                    {
                        if (!windowIdDictionary.ContainsKey(monitorCell.WindowId))
                        {
                            _dataWallClient?.CreateWindow(monitorCell.WindowRect, monitorCell.ChannelId, monitorCell.WindowId);
                            windowIdDictionary[monitorCell.WindowId] = monitorCell.WindowId;
                        }
                    }
                }
                else
                {
                    _didController?.ControlInputSource(monitorCell.MonitorNo, _matrixInputSourceValue);
                    if (monitorCell.WindowScale == "2x2")
                    {
                        _didController?.ControlTile(monitorCell.MonitorNo, 2, 2, monitorCell.TileNumber);
                    }
                    else
                    {
                        _didController?.ControlTile(monitorCell.MonitorNo, 1, 1);
                    }

                    if (!string.IsNullOrWhiteSpace(monitorCell.ChannelId))
                    {
                        if (byte.TryParse(monitorCell.ChannelId, out byte inputNo))
                        {
                            try
                            {
                                if (_matrixClient != null)
                                {
                                    _matrixClient.ConnectVideo(inputNo, (byte)monitorCell.MonitorNo);
                                }
                            }
                            catch (Exception e)
                            {
                                Logger.Error(e, "control matrix:");
                            }
                        }
                        else
                        {
                            Logger.Error("content.Channel is not byte value.");
                        }
                    }
                }
            }

            ClearSelection();
            SaveScreen();
        }

        public void DeletePreset(string id)
        {
            var mcsPresets = McsPresets.ToArray();
            foreach (var preset in mcsPresets)
            {
                if (preset.Id == id)
                {
                    McsPresets.Remove(preset);
                }
            }
            ClearSelection();
            SavePresets();
        }

        private void ClearSelection()
        {
            if (_selectedContent != null)
            {
                _selectedContent.IsSelected = false;
                _selectedContent = null;
            }


            _selectedWindowScale = WindowScales[0];
            WindowScales[0].IsSelected = true;
            WindowScales[1].IsSelected = false;
        }
        /*
        private void ExecuteSelectCellCommand(string id)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    return;
                }

                var foundMonitorCell = FindScreenCell(id);
                if (foundMonitorCell == null)
                {
                    return;
                }

                string[] currentMatrix = _curnetMatrix.Split('X');
                int[] widthHeight = Array.ConvertAll(currentMatrix, s => int.Parse(s));
                var startIndex = foundMonitorCell.Seq;

                for (int height = 1; height <= widthHeight[1]; height++)
                {
                    for (int width = 1; width <= widthHeight[0]; width++)
                    {
                        var index = width + (height - 1) * widthHeight[0];
                        _didController.ControlTile(_monitorMatrix.Width * (height - 1) + startIndex + width - 1, widthHeight[0], widthHeight[1], index);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteSelectCellCommand:");
            }
        }
        private void ExecuteSetMatrixCommand(string matrix)
        {
            _curnetMatrix = matrix;
        }
        private MonitorCellViewModel FindScreenCell(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            MonitorCellViewModel foundScreenCell = null;
            foreach (var cell in MonitorCells.ToArray())
            {
                if (cell.Id == id)
                {
                    foundScreenCell = cell;
                    break;
                }
            }

            return foundScreenCell;
        }

        */

        public void UpdatedScreenAreaSize(IntSize screenAreaSize)
        {
            if (ScreenSize != screenAreaSize)
            {
                ScreenSize = screenAreaSize;
                double baseTotalWidth = BaseWidth * MonitorMatrixX;
                double baseTotalHeight = BaseHeight * MonitorMatrixY;
                _screenScale = Math.Min(ScreenSize.Width / baseTotalWidth, ScreenSize.Height / baseTotalHeight);
                int offsetX = (int)((ScreenSize.Width - (baseTotalWidth * _screenScale)) / 2);
                int offsetY = (int)((ScreenSize.Height - (baseTotalHeight * _screenScale)) / 2);
                _screenOffset = new IntPoint(offsetX, offsetY);

                UpdateCellsRect();
            }
        }
        private void UpdateCellsRect()
        {
            foreach (var cell in WindowCells)
            {
                UpdateCellRect(cell);
            }

            foreach (var cell in MonitorCells.ToArray())
            {
                UpdateCellRect(cell);
            }
        }
        private void UpdateCellRect(HdCellViewModel cell)
        {
            cell.UpdateRect(_screenOffset, _screenScale);
        }

        private void LoadPresets()
        {
            string presetsFile = Path.Combine(_cacheFolder, "hdPresets.xml");
            if (!File.Exists(presetsFile))
            {
                return;
            }

            try
            {
                McsPresets.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(presetsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Presets/Preset");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        var nodeViewPreset = HdMcsPresetViewModel.CreateFrom(node);
                        if (nodeViewPreset != null)
                        {
                            McsPresets.Add(nodeViewPreset);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load presets file.");
            }
        }

        private void SavePresets()
        {
            string presetsFile = Path.Combine(_cacheFolder, "hdPresets.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(presetsFile))
                {
                    xmlWriter.WriteStartElement("Presets");
                    foreach (var preset in McsPresets)
                    {
                        preset.WriteXml(xmlWriter, "Preset");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SavePresets:");
            }
        }
        private void LoadSchedule()
        {
            List<HdJobViewModel> jobList = new List<HdJobViewModel>();
            try
            {
                string filePath = System.IO.Path.Combine(_cacheFolder, "hdSchedule.xml");
                if (!File.Exists(filePath))
                {
                    Logger.Error($"Cannot find Schedule file.");
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNodeList jobNodeList = doc.SelectNodes("/Schedule/Job");
                if (jobNodeList == null || jobNodeList.Count == 0)
                {
                    return;
                }

                foreach (XmlNode jobNode in jobNodeList)
                {
                    int hour = XmlUtils.GetAttrValue(jobNode, "hour", 0);
                    int min = XmlUtils.GetAttrValue(jobNode, "min", 0);
                    string presetId = XmlUtils.GetStringAttrValue(jobNode, "presetId");
                    string presetName = XmlUtils.GetStringAttrValue(jobNode, "presetName");

                    if (string.IsNullOrWhiteSpace(presetId) || string.IsNullOrWhiteSpace(presetName))
                    {
                        continue;
                    }
                    jobList.Add(new HdJobViewModel(hour, min, presetId, presetName));
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"LoadContents:");
            }

            _jobSchedule = jobList.ToArray();
        }

        private void SaveSchedule()
        {
            string filePath = Path.Combine(_cacheFolder, "hdSchedule.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(filePath))
                {
                    xmlWriter.WriteStartElement("Schedule");
                    foreach (var job in _jobSchedule)
                    {
                        xmlWriter.WriteStartElement("Job");
                        xmlWriter.WriteAttributeString("hour", $"{job.Hour}");
                        xmlWriter.WriteAttributeString("min", $"{job.Min}");
                        xmlWriter.WriteAttributeString("presetId", job.PresetId);
                        xmlWriter.WriteAttributeString("presetName", job.PresetName);
                        xmlWriter.WriteEndElement();
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SavePresets:");
            }

        }
    }
}
