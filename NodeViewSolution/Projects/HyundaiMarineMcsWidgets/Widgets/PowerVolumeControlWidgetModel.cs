﻿using HyundaiMarineMcsWidgets.Models;
using NLog;
using NodeView.DataModels;
using NodeView.DeviceControllers;
using NodeView.Drawing;
using NodeView.Extension.Plugins;
using NodeView.Frameworks.Widgets;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO.Ports;
using System.Linq;
using HyundaiMarineMcsWidgets.ViewModels;
using Configuration = NodeView.DataModels.Configuration;
using MonitorCellViewModel = NodeView.ViewModels.MonitorCellViewModel;

namespace HyundaiMarineMcsWidgets.Widgets
{
    public class PowerVolumeControlWidgetModel : BindableBase, IScreenAreaViewModel, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string ConnectionStartString = "serial://";

        private IntSize _monitorMatrix;
        private const int BaseWidth = 1920;
        private const int BaseHeight = 1200;
        private IntSize _screenSize = new IntSize(1920, 1080);

        private LgDidControlClient _boardController;
        private LgDidControlClient _sideTvController;   

        protected ObservableCollection<MonitorInputSource> _inputSources = new ObservableCollection<MonitorInputSource>();
        private ObservableCollection<MonitorCellViewModel> _monitorCells = new ObservableCollection<MonitorCellViewModel>();
        private SpeakerControlViewmodel _speakerControlFrontFrontViewmodel;
        private SpeakerControlViewmodel _speakerControlCeilingViewmodel;
        private TvControlViewModel _leftTvControlViewModel;
        private TvControlViewModel _rightTvControlViewModel;

        private DelegateCommand<string> _toggleSelectionMonitorCommand;
        private DelegateCommand _toggleSelectionAllMonitorCommand;
        private DelegateCommand<string> _monitorPowerOnOffCommand;
        private DelegateCommand<string> _setMonitorInputCommand;

        public SpeakerControlViewmodel SpeakerControlFrontViewModel
        {
            get => _speakerControlFrontFrontViewmodel;
            set => SetProperty(ref _speakerControlFrontFrontViewmodel, value);
        }
        public SpeakerControlViewmodel SpeakerControlCeilingViewModel
        {
            get => _speakerControlCeilingViewmodel;
            set => SetProperty(ref _speakerControlCeilingViewmodel, value);
        }
        public TvControlViewModel LeftTvControlViewModel
        {
            get => _leftTvControlViewModel;
            set => SetProperty(ref _leftTvControlViewModel, value);
        }
        public TvControlViewModel RightTvControlViewModel
        {
            get => _rightTvControlViewModel;
            set => SetProperty(ref _rightTvControlViewModel, value);
        }

        public ObservableCollection<MonitorInputSource> InputSources
        {
            get => _inputSources;
            set => SetProperty(ref _inputSources, value);
        }
        public ObservableCollection<MonitorCellViewModel> MonitorCells
        {
            get => _monitorCells;
            set => SetProperty(ref _monitorCells, value);
        }
        public DelegateCommand<string> ToggleSelectionMonitorCommand => _toggleSelectionMonitorCommand ?? (_toggleSelectionMonitorCommand = new DelegateCommand<string>(ExecuteToggleSelectionMonitorCommand));
        public DelegateCommand ToggleSelectionAllMonitorCommand => _toggleSelectionAllMonitorCommand ?? (_toggleSelectionAllMonitorCommand = new DelegateCommand(ExecuteToggleSelectionAllMonitorCommand));
        public DelegateCommand<string> MonitorPowerOnOffCommand => _monitorPowerOnOffCommand ?? (_monitorPowerOnOffCommand = new DelegateCommand<string>(ExecuteMonitorPowerOnOffCommand));
        public DelegateCommand<string> SetMonitorInputCommand => _setMonitorInputCommand ?? (_setMonitorInputCommand = new DelegateCommand<string>(ExecuteSetMonitorInputCommand));

        public PowerVolumeControlWidgetModel(IDialogService dialogService, IWidgetSource widgetSource)
        {
            LoadConfig((IConfiguration)widgetSource.Config);
            CreateMonitorCells();
        }
        private void LoadConfig(IConfiguration config)
        {
            if (config.TryGetNode("screenControl", out var boardControlNode))
            {
                var boardControlConfig = (Configuration)boardControlNode;

                _monitorMatrix = boardControlConfig.GetValue("matrix", new IntSize(1, 1));
                var connectionString = boardControlConfig.GetValue("connectionString", "");
                var isTestMode = boardControlConfig.GetValue("isTestMode", false); ;
                var isVerbose = isTestMode || boardControlConfig.GetValue("isVerbose", false);
                _boardController = new LgDidControlClient(connectionString, isTestMode, isVerbose);
                _boardController.Init();
                if (boardControlConfig.TryGetNode("inputSources", out var inputsSection))
                {
                    foreach (var property in inputsSection.Properties)
                    {
                        var source = MonitorInputSource.CreateFrom(property);
                        if (source != null)
                        {
                            InputSources.Add(source);
                        }
                    }
                }
            }
            if (config.TryGetNode("tvControl", out var tvContolNode))
            {
                var tvControlConfig = (Configuration)tvContolNode;
                var connectionString = tvControlConfig.GetValue("connectionString", "");
                var isTestMode = tvControlConfig.GetValue("isTestMode", false); ;
                var isVerbose = isTestMode || tvControlConfig.GetValue("isVerbose", false);
                _sideTvController = new LgDidControlClient(connectionString, isTestMode, isVerbose);
                _sideTvController.Init();
                if (tvControlConfig.TryGetConfig("sources", out var sources))
                {
                    foreach (var sourcesProperty in sources.Properties)
                    {
                        var index = int.Parse(sourcesProperty.Value);
                        if (sourcesProperty.Id.Equals("left", StringComparison.OrdinalIgnoreCase))
                        {
                            LeftTvControlViewModel = new TvControlViewModel(sourcesProperty.Id, index, _sideTvController);
                        }
                        else if (sourcesProperty.Id.Equals("right", StringComparison.OrdinalIgnoreCase))
                        {
                            RightTvControlViewModel = new TvControlViewModel(sourcesProperty.Id, index, _sideTvController);
                        }
                    }
                }
            }
            if (config.TryGetNode("speakerControl", out var speakerContolCeilingNode))
            {
                var speakerConfig = (Configuration)speakerContolCeilingNode;
                var speakerSerialport = GetSerialPort(speakerConfig);
                if (speakerConfig.TryGetConfig("sources", out var sources))
                {
                    foreach (var sourcesProperty in sources.Properties)
                    {
                        var channel = sourcesProperty.Value;
                        var isTestMode = speakerConfig.GetValue("isTestMode", false);
                        if (sourcesProperty.Id.Equals("front", StringComparison.OrdinalIgnoreCase))
                        {
                            SpeakerControlFrontViewModel = new SpeakerControlViewmodel(channel, speakerSerialport, isTestMode);
                        }
                        else if (sourcesProperty.Id.Equals("ceiling", StringComparison.OrdinalIgnoreCase))
                        {
                            SpeakerControlCeilingViewModel = new SpeakerControlViewmodel(channel, speakerSerialport, isTestMode);
                        }
                    }
                }
            }
        }
        private void CreateMonitorCells()
        {
            if (_monitorMatrix == null)
            {
                return;
            }
            MonitorCells.Clear();
            if (!_monitorMatrix.IsEmpty)
            {
                int seq = 1;
                for (int i = 0; i < _monitorMatrix.Height; i++)
                {
                    for (int j = 0; j < _monitorMatrix.Width; j++)
                    {
                        MonitorCells.Add(new MonitorCellViewModel(seq++, new IntRect(j * BaseWidth, i * BaseHeight, BaseWidth, BaseHeight)));
                    }
                }
            }

            UpdateRectMonitorCells();
        }
        private void UpdateRectMonitorCells()
        {
            double baseTotalWidth = BaseWidth * _monitorMatrix.Width;
            double baseTotalHeight = BaseHeight * _monitorMatrix.Height;
            double scale = Math.Min(_screenSize.Width / baseTotalWidth, _screenSize.Height / baseTotalHeight);
            int offsetX = (int)((_screenSize.Width - (baseTotalWidth * scale)) / 2);
            int offsetY = (int)((_screenSize.Height - (baseTotalHeight * scale)) / 2);
            var offset = new IntPoint(offsetX, offsetY);

            foreach (var monitorCell in MonitorCells.ToArray())
            {
                monitorCell.UpdateRectFromBaseRect(offset, scale);
            }
        }
        public void UpdatedScreenAreaSize(IntSize screenAreaSize)
        {
            if (_screenSize != screenAreaSize)
            {
                _screenSize = screenAreaSize;
                UpdateRectMonitorCells();
            }
        }
        private void ExecuteToggleSelectionMonitorCommand(string id)
        {
            foreach (var monitorCell in MonitorCells.ToArray())
            {
                if (monitorCell.Id == id)
                {
                    monitorCell.IsSelected = !monitorCell.IsSelected;
                }
            }
        }
        private void ExecuteToggleSelectionAllMonitorCommand()
        {
            bool isSelected = !MonitorCells.Any(cell => cell.IsSelected);

            foreach (var screenCell in MonitorCells)
            {
                screenCell.IsSelected = isSelected;
            }
        }
        private void ExecuteMonitorPowerOnOffCommand(string powerOnOff)
        {
            if (_boardController == null) return;

            if (string.IsNullOrWhiteSpace(powerOnOff))
            {
                Logger.Warn("powerOnOff is null.");
                return;
            }

            bool isOn = powerOnOff.Equals("on", StringComparison.OrdinalIgnoreCase);

            if (MonitorCells.All(cell => cell.IsSelected))
            {
                _boardController.ControlPowerAll(isOn);
            }
            else
            {
                foreach (var monitor in MonitorCells.ToArray())
                {
                    if (monitor.IsSelected)
                    {
                        _boardController.ControlPower(monitor.Seq, isOn);
                    }
                }
            }
        }
        private void ExecuteSetMonitorInputCommand(string sourceType)
        {
            var value = 0;
            if (sourceType.Equals("ipwall", StringComparison.OrdinalIgnoreCase))
            {
                value = InputSources[0].Value;
            }
            else
            {
                value = InputSources[1].Value;
            }
            if (_boardController == null || value == 0) return;

            if (MonitorCells.All(cell => cell.IsSelected))
            {
                _boardController.ControlInputSourceNameAll(value);
            }
            else
            {
                foreach (var monitor in MonitorCells.ToArray())
                {
                    if (monitor.IsSelected)
                    {
                        _boardController.ControlInputSource(monitor.Seq, value);
                    }
                }
            }
        }
        private SerialPort GetSerialPort(Configuration config)
        {
            var connectionString = config.GetStringValue("connectionString", "serial://COM5/9600");
            var isTestMode = config.GetValue("isTestMode", false);
            SerialPort serialPort = null;
            try
            {
                string partName = "COM3";
                int baudRate = 9600;
                if (!connectionString.StartsWith(ConnectionStartString))
                {
                    Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                    return null;
                }

                string[] connectionParams = StringUtils.Split(connectionString.Substring(ConnectionStartString.Length), '/');
                partName = connectionParams[0];
                if (connectionParams.Length > 1)
                {
                    baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
                }

                Logger.Info($"Connect({partName}, {baudRate})");
                if (!isTestMode)
                {
                    serialPort = new SerialPort(partName) { BaudRate = baudRate };
                    serialPort.Open();
                    serialPort.Close();
                }

                //res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                return null;
            }

            return serialPort;
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                SpeakerControlFrontViewModel.Dispose();
                SpeakerControlCeilingViewModel.Dispose();
                LeftTvControlViewModel.Dispose();
                RightTvControlViewModel.Dispose();
                _boardController.Dispose();
                _sideTvController.Dispose();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
