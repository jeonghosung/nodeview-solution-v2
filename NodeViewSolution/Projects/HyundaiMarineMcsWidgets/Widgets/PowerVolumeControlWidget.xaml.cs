﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeView.Drawing;
using NodeView.Frameworks.Widgets;
using NodeView.ViewModels;

namespace HyundaiMarineMcsWidgets.Widgets
{
    /// <summary>
    /// PowerVolumeControlWidget.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PowerVolumeControlWidget : UserControl, IWidgetControl
    {
        public PowerVolumeControlWidget()
        {
            InitializeComponent();
        }

        public IWidgetSource Source
        {
            get => (IWidgetSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IWidgetSource), typeof(PowerVolumeControlWidgetModel));

        private void ScreenArea_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (DataContext is IScreenAreaViewModel screenAreaViewModel)
            {
                screenAreaViewModel.UpdatedScreenAreaSize(new IntSize((int)ScreenArea.ActualWidth, (int)ScreenArea.ActualHeight));
            }
        }
    }
}
