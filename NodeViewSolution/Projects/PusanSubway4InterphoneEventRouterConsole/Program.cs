﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;

namespace PusanSubway4InterphoneEventRouterConsole
{
    class Program
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            string configFilename = "PusanSubway4InterphoneEventRouter.config.xml";

            Logger.Info("Start ====================== ");

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var config = Configuration.Load(settingFile);
            if (config == null)
            {
                Logger.Error("Cannot load config!");
                Shutdown();
                return;
            }

            InterphoneEventServer server = new InterphoneEventServer(config);

            try
            {

                if (!server.Start())
                {
                    Logger.Error("service start error!");
                    Shutdown();
                    return;
                }
                while (true)
                {
                    var codeKey = Console.ReadKey().Key;
                    if (codeKey == ConsoleKey.Q)
                    {
                        server.Stop();
                        break;
                    }
                }
                Logger.Info("========================= End");
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
                server.Stop();
            }

            Console.ReadKey();

        }

        private static void Shutdown()
        {
            Console.WriteLine("종료......");
            Console.ReadKey();
            AppUtils.Exit();
        }
    }
}
