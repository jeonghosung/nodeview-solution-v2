﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.DataModels;

namespace PusanSubway4InterphoneEventRouterConsole
{
    public class InterphoneEventServer
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public const char StartTextCode = '{';
        public const char EndTextCode = '}';
        public const int CommandLength = 2;
        public const int PollingPacketLength = 16;
        public const int EventPacketLength = 20;

        private TcpListener _tcpListener = null;
        private List<InterphoneEventClient> _clientList = new List<InterphoneEventClient>();

        public bool IsVerbose { get; set; } = false;
        public int ServicePort { get; set; } = 9999;
        public bool IsStarted { get; protected set; } = false;

        public InterphoneEventServer(Configuration config)
        {
            if (!Init(config))
            {

            }
        }

        public bool Init(Configuration config)
        {
            if (config == null) return false;

            IsVerbose = config.GetValue("isVerbose", IsVerbose);
            ServicePort = config.GetValue("service.port", ServicePort);

            if (config.TryGetConfig("clients", out var clientsConfig))
            {
                foreach (var childNode in clientsConfig.ChildNodes)
                {
                    if (childNode is IConfiguration clientConfig)
                    {
                        string clientId = clientConfig.Id;
                        string clientIp = clientConfig.GetStringValue("ip");
                        int clientPort = clientConfig.GetValue("port", 0);
                        if (string.IsNullOrWhiteSpace(clientId) || string.IsNullOrWhiteSpace(clientIp) || clientPort <= 0)
                        {
                            Logger.Error($"client info error:[{clientId}][{clientIp}:{clientPort}]");
                            continue;
                        }

                        Logger.Info($"client:[{clientId}][{clientIp}:{clientPort}]");
                        _clientList.Add(new InterphoneEventClient(clientId, clientIp, clientPort));
                    }
                }
            }
            return true;
        }

        public bool Start()
        {
            if (IsStarted) return true;

            try
            {
                Logger.Info($"StartListener on [{ServicePort}]");
                _tcpListener = new TcpListener(IPAddress.Any, ServicePort);
                _tcpListener.Start();
                IsStarted = true;
                foreach (var interphoneEventClient in _clientList.ToArray())
                {
                    interphoneEventClient.Start();
                }
                Task.Factory.StartNew(StartListening);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Start:");
                IsStarted = false;
            }

            return IsStarted;
        }

        public void Stop()
        {
            IsStarted = false;
            _tcpListener?.Stop();
            _tcpListener = null;
            foreach (var interphoneEventClient in _clientList.ToArray())
            {
                interphoneEventClient.Stop();
            }
        }

        private void StartListening()
        {
            while (IsStarted)
            {
                try
                {
                    var client = _tcpListener.AcceptTcpClient();
                    Task.Factory.StartNew(() => HandleInterphoneClient(client));
                }
                catch// (Exception e)
                {
                    //skip
                    //Logger.Error(e, "AcceptTcpClientAsync:");
                }
            }
        }
        private void HandleInterphoneClient(TcpClient client)
        {
            if (client == null || !client.Connected)
            {
                return;
            }

            string clientKey = $"{client.Client.RemoteEndPoint}";
            Logger.Info($"InterphoneEvent source connented [{clientKey}]");
            try
            {
                client.ReceiveTimeout = 500;
                client.SendTimeout = 2000;
                using (var stream = client.GetStream())
                {
                    stream.ReadTimeout = client.ReceiveTimeout;
                    stream.WriteTimeout = client.SendTimeout;
                    StringBuilder stringPacketBuffer = new StringBuilder();
                    byte[] buffer = new byte[4096];
                    while (IsStarted && CheckConnectionAlive(client))
                    {
                        if (stream.CanRead)
                        {
                            try
                            {
                                var readCount = stream.Read(buffer, 0, buffer.Length);
                                if (readCount > 0)
                                {
                                    string data = Encoding.ASCII.GetString(buffer, 0, readCount);
                                    if (IsVerbose)
                                    {
                                        Logger.Info($"[{data}]");
                                    }

                                    stringPacketBuffer.Append(data);
                                    CheckPacket(stringPacketBuffer, stream);
                                }
                            }
                            catch
                            {
                                //skip
                            }
                        }
                        else
                        {
                            Thread.Sleep(500);
                        }
                    }
                }
            }
            catch// (Exception e)
            {
                //Logger.Error(e, "HandleInterphoneClient:");
            }
            finally
            {
                try
                {
                    client.Close();
                    client.Dispose();
                }
                catch
                {
                    //skip
                }
            }
            Logger.Info($"InterphoneEvent source disconnented [{clientKey}]");
        }

        public bool CheckConnectionAlive(TcpClient tcpClient)
        {
            if (tcpClient.Connected)
            {
                bool part1 = tcpClient.Client.Poll(1, SelectMode.SelectRead);
                bool part2 = (tcpClient.Client.Available == 0);
                if (part1 && part2)
                {
                    return false;
                }
                return true;
            }
            return false;
        }

        private void TestCheckPacket()
        {
            try
            {
                string testDataFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestData", "interphone.txt");
                //string testData = File.ReadAllText(testDataFilename).Trim().Replace(" ", "").Replace("\r", "").Replace("\n", "");
                StringBuilder stringPacketBuffer = new StringBuilder();
                stringPacketBuffer.Append(File.ReadAllText(testDataFilename).Trim());
                CheckPacket(stringPacketBuffer, null);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"TestCheckPacket:");
            }
        }

        private void CheckPacket(StringBuilder stringPacketBuffer, NetworkStream stream)
        {
            string packetBuffer = stringPacketBuffer.ToString();
            stringPacketBuffer.Clear();

            while (!string.IsNullOrWhiteSpace(packetBuffer))
            {
                int startCodeIndex = packetBuffer.IndexOf(StartTextCode, 0);
                if (startCodeIndex < 0)
                {
                    break;
                }
                if (startCodeIndex > 0)
                {
                    if (IsVerbose)
                    {
                        Logger.Info($"skip packet: [{packetBuffer.Substring(0, startCodeIndex)}]");
                    }
                }

                if (packetBuffer.Length < (startCodeIndex + CommandLength + 1))
                {
                    stringPacketBuffer.Append(packetBuffer.Substring(startCodeIndex));
                    break;
                }

                string command = packetBuffer.Substring(startCodeIndex + 1, CommandLength);

                if (command == "80") // polling
                {
                    if (packetBuffer.Length < (startCodeIndex + PollingPacketLength))
                    {
                        stringPacketBuffer.Append(packetBuffer.Substring(startCodeIndex));
                        break;
                    }

                    if (packetBuffer[startCodeIndex + PollingPacketLength - 1] == EndTextCode)
                    {

                        string packet = packetBuffer.Substring(startCodeIndex, PollingPacketLength);
                        packetBuffer = packetBuffer.Substring(startCodeIndex + PollingPacketLength);

                        RoutePacket(packet);
                    }
                    else
                    {
                        Logger.Error($"packet missmatched [{packetBuffer.Substring(startCodeIndex, PollingPacketLength)}] is not supported command.");
                        packetBuffer = packetBuffer.Substring(startCodeIndex + 1);
                    }
                }
                else if (command == "81" || command == "82") // 통화시작/종료
                {
                    if (packetBuffer.Length < (startCodeIndex + EventPacketLength))
                    {
                        stringPacketBuffer.Append(packetBuffer.Substring(startCodeIndex));
                        break;
                    }
                    if (packetBuffer[startCodeIndex + EventPacketLength - 1] == EndTextCode)
                    {

                        string packet = packetBuffer.Substring(startCodeIndex, EventPacketLength);
                        byte[] nakPacket = Encoding.ASCII.GetBytes(packetBuffer.Substring(startCodeIndex, EventPacketLength));
                        packetBuffer = packetBuffer.Substring(startCodeIndex + EventPacketLength);
                        RoutePacket(packet);
                        //Send NAK
                        try
                        {
                            stream?.Write(nakPacket, 0, nakPacket.Length);
                        }
                        catch (Exception e)
                        {
                            Logger.Warn(e, "Send NAK:");
                        }
                    }
                    else
                    {
                        Logger.Error($"packet missmatched [{packetBuffer.Substring(startCodeIndex, EventPacketLength)}] is not supported command.");
                        packetBuffer = packetBuffer.Substring(startCodeIndex + 1);
                    }
                }
                else
                {
                    Logger.Error($"[{command}] is not supported command.");
                    packetBuffer = packetBuffer.Substring(startCodeIndex + 1);
                }
            }
        }

        private void RoutePacket(string packet)
        {
            Logger.Info($"RoutePacket:[{packet}]");

            foreach (var interphoneEventClient in _clientList.ToArray())
            {
                interphoneEventClient.RoutePacket(packet);
            }
        }
    }
}
