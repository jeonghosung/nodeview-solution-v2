﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.DataModels;

namespace PusanSubway4InterphoneEventRouterConsole
{
    public class InterphoneEventClient
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private ConcurrentQueue<string> _packetQueue = new ConcurrentQueue<string>();
        private Thread _routingThread = null;
        private volatile bool _isStarted = false;
        public string Id { get; }
        public string Ip { get; }
        public int Port { get; }

        public InterphoneEventClient(string id, string ip, int port)
        {
            Id = id;
            Ip = ip;
            Port = port;
        }

        public void Start()
        {
            if (_isStarted) return;
            try
            {
                _isStarted = true;
                _routingThread = new Thread(Routing);
                _routingThread.IsBackground = true;
                _routingThread.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Start:");
            }
        }

        public void Stop()
        {
            _isStarted = false;
            try
            {
                _routingThread.Join(200);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Stop:");
            }

            _routingThread = null;
        }

        public void RoutePacket(string packet)
        {
            if (!_isStarted)
            {
                Logger.Warn($"_tcpClient [{Id}] is started");
                return;
            }
            _packetQueue.Enqueue(packet);
        }


        private void Routing()
        {
            var runningStatus = ProcessRunningStatus.None;
            var lastConnectedTime = DateTime.Now;
            byte[] readBuffer = new byte[1024];

            TcpClient tcpClient = null;
            NetworkStream stream = null;

            while (_isStarted)
            {
                try
                {
                    if (runningStatus == ProcessRunningStatus.None)
                    {
                        try
                        {
                            tcpClient = new TcpClient(Ip, Port);
                            stream = tcpClient.GetStream();
                        }
                        catch
                        {
                            tcpClient = null;
                            stream?.Dispose();
                            stream = null;
                        }
                        if (tcpClient?.Connected ?? false)
                        {
                            runningStatus = ProcessRunningStatus.Running;
                        }
                        else
                        {
                            Logger.Warn($"Cannot connect to [{Id}][{Ip}:{Port}].");
                            runningStatus = ProcessRunningStatus.Stopped;
                            lastConnectedTime = DateTime.Now;
                            try
                            {
                                stream?.Dispose();
                                tcpClient?.Dispose();
                            }
                            catch { /* skip */ }
                            tcpClient = null;
                        }
                    }

                    if (_packetQueue.TryDequeue(out string packet))
                    {
                        if (string.IsNullOrWhiteSpace(packet))
                        {
                            Logger.Error($"packet is empty.");
                            continue;
                        }

                        if (runningStatus == ProcessRunningStatus.Stopped)
                        {
                            // 패킷 들어올때마다 제연결 시도
                            try
                            {
                                tcpClient = new TcpClient(Ip, Port);
                                stream = tcpClient.GetStream();
                            }
                            catch
                            {
                                tcpClient = null;
                                stream?.Dispose();
                                stream = null;
                            }
                            if (tcpClient?.Connected ?? false)
                            {
                                runningStatus = ProcessRunningStatus.Running;
                                Logger.Info($"client connected [{Id}][{Ip}:{Port}].");
                            }
                            else
                            {
                                runningStatus = ProcessRunningStatus.Stopped;
                                lastConnectedTime = DateTime.Now;
                                try
                                {
                                    stream?.Dispose();
                                    tcpClient?.Dispose();
                                }
                                catch { /* skip */ }
                                tcpClient = null;
                            }
                        }

                        if (runningStatus == ProcessRunningStatus.Running)
                        {
                            if (tcpClient != null)
                            {
                                byte[] bytePacket = Encoding.ASCII.GetBytes(packet);
                                stream.Write(bytePacket, 0, bytePacket.Length);
                                if (packet.StartsWith("{81") || packet.StartsWith("{82"))
                                {
                                    // 굳이 체크 안하고 읽고 버림.
                                    stream.Read(readBuffer, 0, readBuffer.Length);
                                }
                                //이러기는 싫지만 기존 구현이 별루라 패킷 단위로 끊어서 보냄
                                Thread.Sleep(100);
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);

                        //15초 마다 연결 상태 확인, 10초 마다 패킷이 들어오니 아래코드는 신운영환경에서 실행 안된느것이 맞음
                        if ((DateTime.Now - lastConnectedTime).TotalSeconds > 15)
                        {
                            if (runningStatus == ProcessRunningStatus.Running)
                            {
                                if (tcpClient != null && !CheckConnectionAlive(tcpClient))
                                {
                                    Logger.Warn($"client disconnected [{Id}][{Ip}:{Port}].");
                                    runningStatus = ProcessRunningStatus.Stopped;
                                    lastConnectedTime = DateTime.Now;
                                    try
                                    {
                                        stream?.Dispose();
                                        tcpClient?.Dispose();
                                    }
                                    catch { /* skip */ }
                                    stream = null;
                                    tcpClient = null;
                                }
                            }
                            else if (runningStatus == ProcessRunningStatus.Stopped)
                            {
                                try
                                {
                                    tcpClient = new TcpClient(Ip, Port);
                                    stream = tcpClient.GetStream();
                                }
                                catch
                                {
                                    tcpClient = null;
                                    stream?.Dispose();
                                    stream = null;
                                }
                                if (tcpClient?.Connected ?? false)
                                {
                                    runningStatus = ProcessRunningStatus.Running;
                                    Logger.Info($"client connected [{Id}][{Ip}:{Port}].");
                                }
                                else
                                {
                                    runningStatus = ProcessRunningStatus.Stopped;
                                    lastConnectedTime = DateTime.Now;
                                    try
                                    {
                                        stream?.Dispose();
                                        tcpClient?.Dispose();
                                    }
                                    catch { /* skip */ }
                                    tcpClient = null;
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Routing:");
                    Logger.Warn($"client disconnected [{Id}][{Ip}:{Port}].");
                    runningStatus = ProcessRunningStatus.Stopped;
                    lastConnectedTime = DateTime.Now;
                    try
                    {
                        stream?.Dispose();
                        tcpClient?.Dispose();
                    }
                    catch { /* skip */ }
                    tcpClient = null;
                }
            }
        }
        public bool CheckConnectionAlive(TcpClient tcpClient)
        {
            if (tcpClient.Connected)
            {
                bool part1 = tcpClient.Client.Poll(1, SelectMode.SelectRead);
                bool part2 = (tcpClient.Client.Available == 0);
                if (part1 && part2)
                {
                    return false;
                }
                return true;
            }
            return false;
        }
    }
}
