﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NLog.Targets;

namespace PusanSubway4InterphoneEventReporterTestClient
{
    class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        static void Main(string[] args)
        {
            try
            {
                string testFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestData/interphone.txt");

                using (TcpClient client = new TcpClient("127.0.0.1", 3000))
                {
                    using (var stream = client.GetStream())
                    {
                        if (File.Exists(testFilename))
                        {
                            TextReader reader = new StringReader(File.ReadAllText(testFilename));
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                if (string.IsNullOrWhiteSpace(line))
                                {
                                    continue;
                                }
                                byte[] packet = Encoding.ASCII.GetBytes(line.Trim());
                                stream.Write(packet, 0, packet.Length);
                                Thread.Sleep(1000);
                            }
                        }
                        else
                        {
                            byte[] packet = Encoding.ASCII.GetBytes("{80000000000000}");
                            stream.Write(packet, 0, packet.Length);
                        }
                        Console.WriteLine("press any key to close socket.");
                        Console.ReadKey();
                    }

                    client.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
            }

            Console.ReadKey();
        }
    }
}
