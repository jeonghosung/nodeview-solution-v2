﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;
using System.IO.Ports;
using System.Text;
using Timer = System.Threading.Timer;

namespace Code0Detector
{
    public partial class Code0Detector : Form
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private NotifyIcon _notify = new NotifyIcon();
        
        private const string ConnectionStartString = "serial://";
        private SerialPort _eventSenderSerialPort = null;
        private bool _isEventSenderEnabled = false;
        private string _eventId = "alarmCode0";
        private Point _detectingPosition;
        private Timer _detectingTimer = null;
        private volatile bool _isEventOn = false;
        private DateTime _lastEventOnTime = DateTime.Now;

        public bool IsTestMode { get; protected set; } = false;
        public bool IsVerbose { get; protected set; } = true;

        public Code0Detector()
        {
            InitializeComponent();
            WindowState = FormWindowState.Minimized;
            Hide();
        }

        private void Init()
        {
            _detectingTimer = new Timer(DetectingTimerCallback);
            string configFilename = "Code0Detector.config.xml";
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var configuration = Configuration.Load(settingFile);
            Start(configuration);
            
        }

        private void InitNotify()
        {
            var menu = new ContextMenuStrip();
            ToolStripMenuItem menuItem;
            ToolStripSeparator sep;
            menuItem = new ToolStripMenuItem
            {
                Text = "Open"
            };
            menuItem.Click += (_, _) =>
            {
                WindowState = FormWindowState.Normal;
                Show();
            };
            menu.Items.Add(menuItem);

            menuItem = new ToolStripMenuItem
            {
                Text = "Exit"
            };
            menuItem.Click += (_, _) =>
            {
                _notify.Visible = false;
                Close();
            };
            menu.Items.Add(menuItem);

            //_notify.Icon = new System.Drawing.Icon(new Uri("path"));
            _notify.Icon = Properties.Resources.suncheon112;
            _notify.Visible = true;
            _notify.DoubleClick += (_, _) =>
            {
                //DoubleMethod();
            };
            _notify.ContextMenuStrip = menu;
            _notify.Text = "Code0Detector";
        }

        private void Code0Detector_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stop();
        }

        private void Code0Detector_Load(object sender, EventArgs e)
        {
            InitNotify();
            Init();
        }

        public void Start(Configuration config)
        {
            IsTestMode = config.GetValue("isTestMode", IsTestMode);
            IsVerbose = IsTestMode || config.GetValue("isVerbose", IsVerbose);

            _isEventSenderEnabled = config.GetValue("eventSender.isEnabled", _isEventSenderEnabled);
            if (_isEventSenderEnabled)
            {
                string connectionString = config.GetValue("eventSender.connectionString", "");
                string partName = "COM3";
                int baudRate = 9600;
                if (!connectionString.StartsWith(ConnectionStartString))
                {
                    Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                }
                else
                {
                    string[] connectionParams = StringUtils.Split(connectionString.Substring(ConnectionStartString.Length), '/');
                    partName = connectionParams[0];
                    if (connectionParams.Length > 1)
                    {
                        baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
                    }

                    Logger.Info($"Connect({partName}, {baudRate})");
                    if (!IsTestMode)
                    {
                        try
                        {
                            _eventSenderSerialPort = new SerialPort(partName) { BaudRate = baudRate };
                            _eventSenderSerialPort.Open();
                        }
                        catch (Exception e)
                        {
                            Logger.Error(e, "Connect open eventSenderSerialPort.");
                            _eventSenderSerialPort.Dispose();
                            _eventSenderSerialPort = null;
                        }
                    }
                }
            }

            _detectingPosition = config.GetValue("redDetector.position", new Point(0, 0));
            _eventId = config.GetValue("redDetector.eventId", _eventId);

            Logger.Info($"Start[IsTestMode={IsTestMode}][redDetector.position={_detectingPosition}]");

            _detectingTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        public void Stop()
        {
            _detectingTimer.Change(Timeout.Infinite, Timeout.Infinite);
            _eventSenderSerialPort?.Dispose();
            _eventSenderSerialPort = null;
            Logger.Info("Stop");
        }

        private void DetectingTimerCallback(object state)
        {
            _detectingTimer.Change(Timeout.Infinite, Timeout.Infinite);
            Color detectingColor = Color.Black;
            using (var bitmap = new Bitmap(1, 1))
            {
                using (var graphics = Graphics.FromImage(bitmap))
                {
                    graphics.CopyFromScreen(_detectingPosition, new Point(0, 0), new Size(1, 1));
                }
                detectingColor = bitmap.GetPixel(0, 0);
            }

            if (detectingColor.R > 128 &&
                (detectingColor.R - detectingColor.G) > 100 &&
                (detectingColor.R - detectingColor.B) > 100)

            {
                _lastEventOnTime = DateTime.Now;
                if (!_isEventOn)
                {
                    _isEventOn = true;
                    Task.Factory.StartNew(RaiseEventOn);
                }
            }
            else
            {
                if (_isEventOn)
                {
                    if((DateTime.Now - _lastEventOnTime).TotalSeconds > 4) 
                    {
                        _isEventOn = false;
                        Task.Factory.StartNew(RaiseEventOff);
                    }
                }
            }

            _detectingTimer.Change(TimeSpan.FromSeconds(0.4), TimeSpan.FromSeconds(0.4));
        }

        private void RaiseEventOn()
        {
            Logger.Warn("RaiseEventOn");
            listBoxLogs.BeginInvoke(new Action(() =>
            {
                listBoxLogs.Items.Insert(0, $"RaiseEventOn");
            }));
            if (_eventSenderSerialPort != null && _eventSenderSerialPort.IsOpen)
            {
                var payloadJson = new JObject()
                {
                    ["command"] = "riseEvent",
                    ["body"] = new JObject()
                    {
                        ["id"] = _eventId,
                        ["isOn"] = "True",
                        ["message"] = "CODE 0 발생",
                    }
                };

                var payloadBytes = Encoding.UTF8.GetBytes(payloadJson.ToString(Formatting.None));
                byte[] packetBytes = new byte[payloadBytes.Length + 2];
                packetBytes[0] = 0x02;
                Array.Copy(payloadBytes, 0, packetBytes, 1, payloadBytes.Length);
                packetBytes[packetBytes.Length - 1] = 0x03;
                _eventSenderSerialPort?.Write(packetBytes, 0, packetBytes.Length);
            }
        }

        private void RaiseEventOff()
        {
            Logger.Warn("RaiseEventOff");
            listBoxLogs.BeginInvoke(new Action(() =>
            {
                listBoxLogs.Items.Insert(0, $"RaiseEventOff");
            }));
            if (_eventSenderSerialPort != null && _eventSenderSerialPort.IsOpen)
            {
                var payloadJson = new JObject()
                {
                    ["command"] = "riseEvent",
                    ["body"] = new JObject()
                    {
                        ["id"] = _eventId,
                        ["isOn"] = "False",
                        ["message"] = "CODE 0 종료",
                    }
                };

                var payloadBytes = Encoding.UTF8.GetBytes(payloadJson.ToString(Formatting.None));
                byte[] packetBytes = new byte[payloadBytes.Length + 2];
                packetBytes[0] = 0x02;
                Array.Copy(payloadBytes, 0, packetBytes, 1, payloadBytes.Length);
                packetBytes[packetBytes.Length - 1] = 0x03;
                _eventSenderSerialPort?.Write(packetBytes, 0, packetBytes.Length);
            }
        }
    }
}
