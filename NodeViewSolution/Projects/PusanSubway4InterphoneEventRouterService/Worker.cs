using NLog;
using NodeView.DataModels;
using PusanSubway4InterphoneEventRouterConsole;

namespace PusanSubway4InterphoneEventRouterService
{
    public class Worker : BackgroundService
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        string configFilename = "PusanSubway4InterphoneEventRouter.config.xml";
        private InterphoneEventServer _server = null;

        public Worker(ILogger<Worker> logger)
        {
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            Logger.Info("Start ====================== ");
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var config = Configuration.Load(settingFile);
            if (config == null)
            {
                Logger.Error("Cannot load config!");
                return;
            }

            _server = new InterphoneEventServer(config);

            try
            {

                if (!_server.Start())
                {
                    Logger.Error("service start error!");
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
                _server.Stop();
                await StopAsync(stoppingToken);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            Logger.Info("========================= End");
            _server.Stop();
            return base.StopAsync(cancellationToken);
        }
    }
}