﻿using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PusanSubway4Plugins
{
    public class DoorSensorReporter : IDataReporter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public const char StartTextCode = 'S';
        public const char EndTextCode = 'E';
        public const int PayloadLength = 6;

        private IStationService _stationService = null;
        private const string ConnectionStartString = "serial://";

        private SerialPort _serialPort = null;
        private StringBuilder _stringPacketBuffer = new StringBuilder();

        private Dictionary<string, Door> _doorMapping = new Dictionary<string, Door>();
        
        public string Id { get; protected set; }
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string ConnectionString { get; set; } = "";
        public bool IsTestMode { get; set; } = false;
        public bool IsVerbose { get; set; } = false;
        public bool IsStarted { get; protected set; } = false;
        public string EventIdPrefix { get; set; } = "";

        public bool Init(Configuration config, IStationService stationService)
        {
            _stationService = stationService;
            if (!LoadConfig(config))
            {
                return false;
            }
            if (!RegisterOnStation())
            {
                return false;
            }

            return true;
        }
        protected virtual bool LoadConfig(Configuration config)
        {
            if (config == null)
            {
                if (string.IsNullOrWhiteSpace(Id)) return false;
                return true;
            }
            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    Id = StringUtils.GetStringValue(config.Id, Uuid.NewUuid);
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    Name = this.GetType().Name;
                }
                Description = config.GetValue("@description", "");
                IsTestMode = config.GetValue("isTestMode", false); ;
                IsVerbose = IsTestMode || config.GetValue("isVerbose", false);

                Name = config.GetValue("name", Name);
                EventIdPrefix = config.GetValue("eventIdPrefix", "doorSensor");

                ConnectionString = config.GetValue("connectionString", "");
                if (SetupConnection())
                {
                    string doorMappingXml = config.GetValue("doorMappingXml", "NodeViewConfigs/doorMapping.xml");
                    LoadDoorMapping(doorMappingXml);

                    res = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        private void LoadDoorMapping(string doorMappingXmlFilename)
        {
            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, doorMappingXmlFilename);
                if (!File.Exists(filePath))
                {
                    Logger.Error($"Cannot find file [{doorMappingXmlFilename}].");
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNodeList doorNodeList = doc.SelectNodes("/DoorMapping/Door");
                if (doorNodeList == null || doorNodeList.Count == 0)
                {
                    return;
                }

                foreach (XmlNode doorNode in doorNodeList)
                {
                    string code = XmlUtils.GetStringAttrValue(doorNode, "code");
                    string name = XmlUtils.GetStringAttrValue(doorNode, "name", code);
                    bool usingEvent = XmlUtils.GetAttrValue(doorNode, "usingEvent", false);

                    if (string.IsNullOrWhiteSpace(code))
                    {
                        continue;
                    }

                    _doorMapping[code] = new Door(code, name, usingEvent);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"LoadDoorMapping('{doorMappingXmlFilename}'):");
            }
        }

        private bool RegisterOnStation()
        {
            //Report 전용이라 모듈 등록 안해도 됨
            //Node도 등록 안함, Open만 있는것도 있음
            return true;
        }
        private void UnRegisterOnStation()
        {
        }


        private bool SetupConnection()
        {
            string partName = "COM3";
            int baudRate = 9600;
            if (!ConnectionString.StartsWith(ConnectionStartString))
            {
                Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                return false;
            }

            string[] connectionParams = StringUtils.Split(ConnectionString.Substring(ConnectionStartString.Length), '/');
            partName = connectionParams[0];
            if (connectionParams.Length > 1)
            {
                baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
            }

            Logger.Info($"Connect({partName}, {baudRate})");
            if (!IsTestMode)
            {
                _serialPort = new SerialPort(partName) { BaudRate = baudRate };
                _serialPort.Open();
                _serialPort.Close();
            }

            return true;
        }

        public bool Start()
        {
            if (IsStarted) return true;

            if (IsTestMode)
            {
                IsStarted = true;
                TestCheckPacket();
                return IsStarted;
            }

            try
            {
                _serialPort.DataReceived += SerialPortOnDataReceived;
                _serialPort.Open();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Start:");
            }

            IsStarted = _serialPort?.IsOpen ?? false;
            return IsStarted;
        }

        public void Stop()
        {
            if (_serialPort != null)
            {
                _serialPort.DataReceived -= SerialPortOnDataReceived;
            }
            UnRegisterOnStation();
            _stationService = null;
            IsStarted = false;
        }

        public void Dispose()
        {
            Stop();
            _serialPort?.Dispose();
            _serialPort = null;
        }

        private void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string receivedString = _serialPort.ReadExisting();
            if (string.IsNullOrWhiteSpace(receivedString)) return;

            _stringPacketBuffer.Append(receivedString);
            CheckPacket(_stringPacketBuffer);
        }

        private void TestCheckPacket()
        {
            try
            {
                string testDataFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestData", "door.txt");
                //string testData = File.ReadAllText(testDataFilename).Trim().Replace(" ", "").Replace("\r", "").Replace("\n", "");
                _stringPacketBuffer.Append(File.ReadAllText(testDataFilename).Trim());
                CheckPacket(_stringPacketBuffer);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"TestCheckPacket:");
            }
        }

        private void CheckPacket(StringBuilder stringPacketBuffer)
        {
            string packetString = stringPacketBuffer.ToString();
            stringPacketBuffer.Clear();

            while (!string.IsNullOrWhiteSpace(packetString))
            {
                int startCodeIndex = packetString.IndexOf(StartTextCode, 0);
                if (startCodeIndex < 0)
                {
                    break;
                }

                if (packetString.Length < (startCodeIndex + PayloadLength + 2))
                {
                    stringPacketBuffer.Append(packetString.Substring(startCodeIndex));
                    break;
                }

                if (packetString[startCodeIndex + PayloadLength + 1] == EndTextCode)
                {

                    string payload = packetString.Substring(startCodeIndex + 1, PayloadLength);
                    packetString = packetString.Substring(startCodeIndex + PayloadLength + 2);
                    OnReceivePayload(payload);
                }
                else
                {
                    Logger.Error($"packet missmatched [{packetString.Substring(startCodeIndex, PayloadLength)+1}] is not supported command.");
                    packetString = packetString.Substring(startCodeIndex + 1);
                }
            }
        }

        private void OnReceivePayload(string payload)
        {
            if (payload.Length < PayloadLength)
            {
                Logger.Error($"payload size is '{payload.Length}' != {PayloadLength}");
                return;
            }

            if (IsVerbose)
            {
                Logger.Info($"payload:[{payload}]");
            }

            int offset = 0;
            //code
            string code = payload.Substring(offset, 5);
            offset += 5;

            //status
            string status = payload.Substring(offset, 1);
            bool isDoorOpen = false;
            switch (status)
            {
                case "1":
                    isDoorOpen = true;
                    break;
                case "0":
                    isDoorOpen = false;
                    break;
                default:
                    Logger.Error($"[{payload}/{status}] is not supported status.");
                    return;
            }

            string doorOpenCloseMessage = isDoorOpen ? "열림" : "닫힘";
            Door door = null;
            if (!_doorMapping.TryGetValue(code, out door))
            {
                if (IsVerbose)
                {
                    Logger.Info($"[{code} / {doorOpenCloseMessage}], but not mapping.");
                }

                return;
            }

            Logger.Info($"{door.Name} {doorOpenCloseMessage}");

            if (door.UsingEvent)
            {
                Task.Factory.StartNew(() =>
                {
                    SendEvent(code, door.Name, isDoorOpen);
                });
            }
        }
        
        private void SendEvent(string doorCode, string doorName, bool isOn)
        {
            string onOffMessage = isOn ? "열림" : "닫힘";
            _stationService.OnEvent(new NodeViewEvent($"{EventIdPrefix}{doorCode}", isOn, $"[{doorName}] {onOffMessage}"));
        }
    }

    internal class Door
    {
        public string Code { get; }
        public string Name { get; }
        public bool UsingEvent { get; }

        public Door(string code, string name, bool usingEvent = true)
        {
            Code = code;
            Name = name;
            UsingEvent = usingEvent;
        }
    }
}
