﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.DataModels;
using NodeView.Drawing;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.Utils;

namespace PusanSubway4Plugins
{
    public class PowerControlUnitController : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string ConnectionStartString = "serial://";
        private SerialPort _serialPort = null;
        private int _channelCount = 10;
        public int ChannelCount
        {
            get => _channelCount;
            set
            {
                _channelCount = value;
                _attributes["channelCount"] = $"{value}";
            }
        }
        public PowerControlUnitController()
        {
            DeviceType = "powerControlUnit";
        }

        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("power", "전원 On/Off", new[]
            {
                new FieldDescription("seq", "1", "channel번호 1부터 시작", "int"),
                new FieldDescription("value", "on", "on/off", "enum",  "on,off")
            }));

            return commandDescriptions.ToArray();
        }

        public override bool Init(Configuration config, IStationService stationService)
        {
            if (!base.Init(config, stationService))
            {
                return false;
            }

            bool res = false;
            try
            {
                string partName = "COM3";
                int baudRate = 9600;
                if (!ConnectionString.StartsWith(ConnectionStartString))
                {
                    Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                    return false;
                }

                string[] connectionParams = StringUtils.Split(ConnectionString.Substring(ConnectionStartString.Length), '/');
                partName = connectionParams[0];
                if (connectionParams.Length > 1)
                {
                    baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
                }

                Logger.Info($"Connect({partName}, {baudRate})");
                if (!IsTestMode)
                {
                    _serialPort = new SerialPort(partName) { BaudRate = baudRate };
                    _serialPort.Open();
                    _serialPort.Close();
                }

                int channelCount = config.GetValue("channelCount", ChannelCount);
                if (channelCount > 0)
                {
                    ChannelCount = channelCount;
                }
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _serialPort?.Dispose();
                _serialPort = null;
            }
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "power", StringComparison.Ordinal) == 0)
                {
                    int seq = parameters.GetValue("seq", -1);
                    bool isOn = parameters.GetValue("value", "on") == "on";
                    if (seq >= 0)
                    {
                        ControlPower(seq, isOn);
                        isHandled = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }

        public void ControlPower(int channelSeq, bool isOn)
        {
            if (channelSeq <= 0 || channelSeq > ChannelCount ||  channelSeq > 15)
            {
                Logger.Warn($"[{channelSeq}] is out of channelSeq");
                return;
            }
            try
            {
                StringBuilder sbPacket = new StringBuilder();
                sbPacket.Append("02C201");
                sbPacket.Append(isOn ? "31": "32");
                sbPacket.Append("3");
                sbPacket.Append($"{channelSeq:X2}".Substring(1,1));
                sbPacket.Append("0380");
                string hexPacket = sbPacket.ToString();
                if (IsTestMode)
                {
                    Logger.Info($"ControlPower({channelSeq},{isOn}):{hexPacket}");
                    return;
                }
                using (new SerialConnection(_serialPort))
                {
                    _serialPort.ReadExisting();
                    byte[] packet = StringUtils.GetBytesFromHexString(hexPacket);
                    _serialPort.Write(packet, 0, packet.Length);
                    Thread.Sleep(50);
                    _serialPort.ReadExisting();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlPower:");
            }
        }

    }
}
