﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Utils;

namespace PusanSubway4Plugins
{
    public class InterphoneEventReporter : IDataReporter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public const char StartTextCode = '{';
        public const char EndTextCode = '}';
        public const int CommandLength = 2;
        public const int PollingPayloadLength = 14;
        public const int EventPayloadLength = 18;

        private IStationService _stationService = null;
        
        private TcpListener _tcpListener = null;
        private readonly Dictionary<string, Interphone> _interphoneMapping = new Dictionary<string, Interphone>();

        public string Id { get; protected set; }
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public bool IsTestMode { get; set; } = false;
        public bool IsVerbose { get; set; } = false;
        public bool IsStarted { get; protected set; } = false;
        public int ServicePort { get; set; } = 3000;
        public string EventIdPrefix { get; set; } = "";

        public bool Init(Configuration config, IStationService stationService)
        {
            _stationService = stationService;
            if (!LoadConfig(config))
            {
                return false;
            }
            if (!RegisterOnStation())
            {
                return false;
            }

            return true;
        }
        protected virtual bool LoadConfig(Configuration config)
        {
            if (config == null)
            {
                if (string.IsNullOrWhiteSpace(Id)) return false;
                return true;
            }
            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    Id = StringUtils.GetStringValue(config.Id, Uuid.NewUuid);
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    Name = this.GetType().Name;
                }
                Description = config.GetValue("@description", "");
                IsTestMode = config.GetValue("isTestMode", false); ;
                IsVerbose = IsTestMode || config.GetValue("isVerbose", false);

                Name = config.GetValue("name", Name);
                ServicePort = config.GetValue("servicePort", ServicePort);
                EventIdPrefix = config.GetValue("eventIdPrefix", "InterphoneEvent");

                if (SetupConnection())
                {
                    string interphoneMappingXml = config.GetValue("interphoneMapping", "NodeViewConfigs/interphoneMapping.xml");
                    LoadInterphoneMapping(interphoneMappingXml);

                    res = true;
                }

                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        private bool SetupConnection()
        {
            _tcpListener = new TcpListener(IPAddress.Any, ServicePort);
            return true;
        }

        private void LoadInterphoneMapping(string interphoneMappingXmlFilename)
        {
            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, interphoneMappingXmlFilename);
                if (!File.Exists(filePath))
                {
                    Logger.Error($"Cannot find file [{interphoneMappingXmlFilename}].");
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNodeList doorNodeList = doc.SelectNodes("/InterphoneMapping/Interphone");
                if (doorNodeList == null || doorNodeList.Count == 0)
                {
                    return;
                }

                foreach (XmlNode doorNode in doorNodeList)
                {
                    string code = XmlUtils.GetStringAttrValue(doorNode, "code");
                    string name = XmlUtils.GetStringAttrValue(doorNode, "name", code);
                    bool usingEvent = XmlUtils.GetAttrValue(doorNode, "usingEvent", false);

                    if (string.IsNullOrWhiteSpace(code))
                    {
                        continue;
                    }

                    _interphoneMapping[code] = new Interphone(code, name, usingEvent);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"LoadInterphoneMapping('{interphoneMappingXmlFilename}'):");
            }
        }

        private bool RegisterOnStation()
        {
            //Report 전용이라 모듈 등록 안해도 됨
            //Node도 등록 여부 고민 필요
            return true;
        }
        private void UnRegisterOnStation()
        {
        }

        public bool Start()
        {
            if (IsStarted) return true;

            if (IsTestMode)
            {
                IsStarted = true;
                TestCheckPacket();
                return IsStarted;
            }

            try
            {
                _tcpListener.Start();
                IsStarted = true;
                Task.Factory.StartNew(StartListening);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Start:");
                IsStarted = false;
            }

            return IsStarted;
        }

        private void StartListening()
        {
            while (IsStarted)
            {
                try
                {
                    var client = _tcpListener.AcceptTcpClient();
                    Task.Factory.StartNew(() => HandleInterphoneClient(client));
                }
                catch// (Exception e)
                {
                    //skip
                    //Logger.Error(e, "AcceptTcpClientAsync:");
                }
            }
        }

        public void Stop()
        {
            IsStarted = false;
            _tcpListener?.Stop();
        }
        public void Dispose()
        {
            Stop();
            _tcpListener = null;
        }

        private void HandleInterphoneClient(TcpClient client)
        {
            if (client == null || !client.Connected)
            {
                return;
            }

            string clientKey = $"{client.Client.RemoteEndPoint}";
            Logger.Info($"InterphoneClient start [{clientKey}]");
            try
            {
                client.ReceiveTimeout = 500;
                //혹시 인터폰쪽이 느려서  Write timeout 걸릴수 있어서 추가
                //일반 패킷을 에코 하는것이 프로토콜임
                client.SendTimeout = 2000;
                using (var stream = client.GetStream())
                {
                    stream.ReadTimeout = client.ReceiveTimeout;
                    stream.WriteTimeout = client.SendTimeout;
                    StringBuilder stringPacketBuffer = new StringBuilder();
                    byte[] buffer = new byte[4096];
                    while (IsStarted && CheckConnectionAlive(client))
                    {
                        if (stream.CanRead)
                        {
                            try
                            {
                                var readCount = stream.Read(buffer, 0, buffer.Length);
                                if (readCount > 0)
                                {
                                    string data = Encoding.ASCII.GetString(buffer, 0, readCount);
                                    if (IsVerbose)
                                    {
                                        Logger.Info($"[{data}]");
                                    }

                                    stringPacketBuffer.Append(data);
                                    CheckPacket(stringPacketBuffer, stream);
                                }
                            }
                            catch
                            {
                                //skip
                            }
                        }
                        else
                        {
                            Thread.Sleep(500);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "HandleInterphoneClient:");
            }
            finally
            {
                try
                {
                    client.Close();
                    client.Dispose();
                }
                catch
                {
                    //skip
                }
            }
            Logger.Info($"InterphoneClient end [{clientKey}]");
        }

        public bool CheckConnectionAlive(TcpClient tcpClient)
        {
            if (tcpClient.Connected)
            {
                bool part1 = tcpClient.Client.Poll(1, SelectMode.SelectRead);
                bool part2 = (tcpClient.Client.Available == 0);
                if (part1 && part2)
                {
                    return false;
                }
                return true;
            }
            return false;
        }
        
        private void TestCheckPacket()
        {
            try
            {
                string testDataFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestData", "interphone.txt");
                //string testData = File.ReadAllText(testDataFilename).Trim().Replace(" ", "").Replace("\r", "").Replace("\n", "");
                StringBuilder stringPacketBuffer = new StringBuilder();
                stringPacketBuffer.Append(File.ReadAllText(testDataFilename).Trim());
                CheckPacket(stringPacketBuffer, null);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"TestCheckPacket:");
            }
        }

        private void CheckPacket(StringBuilder stringPacketBuffer, NetworkStream stream)
        {
            string packetString = stringPacketBuffer.ToString();
            stringPacketBuffer.Clear();

            while (!string.IsNullOrWhiteSpace(packetString))
            {
                int startCodeIndex = packetString.IndexOf(StartTextCode, 0);
                if (startCodeIndex < 0)
                {
                    break;
                }
                if (startCodeIndex > 0)
                {
                    if (IsVerbose)
                    {
                        Logger.Info($"skip packet: [{packetString.Substring(0, startCodeIndex)}]");
                    }
                }

                if (packetString.Length < (startCodeIndex + CommandLength + 1))
                {
                    stringPacketBuffer.Append(packetString.Substring(startCodeIndex));
                    break;
                }

                string command = packetString.Substring(startCodeIndex + 1, CommandLength);

                if (command == "80") // polling
                {
                    if (packetString.Length < (startCodeIndex + PollingPayloadLength + 2))
                    {
                        stringPacketBuffer.Append(packetString.Substring(startCodeIndex));
                        break;
                    }

                    if (packetString[startCodeIndex + PollingPayloadLength + 1] == EndTextCode)
                    {

                        string payload = packetString.Substring(startCodeIndex + 1, PollingPayloadLength);
                        packetString = packetString.Substring(startCodeIndex + PollingPayloadLength + 2);

                        if (IsVerbose)
                        {
                            Logger.Info($"Receive: [{payload}]");
                        }
                    }
                    else
                    {
                        Logger.Error($"packet missmatched [{packetString.Substring(startCodeIndex, PollingPayloadLength + 1)}] is not supported command.");
                        packetString = packetString.Substring(startCodeIndex + 1);
                    }
                }
                else if (command == "81" || command == "82") // 통화시작/종료
                {
                    if (packetString.Length < (startCodeIndex + EventPayloadLength + 2))
                    {
                        stringPacketBuffer.Append(packetString.Substring(startCodeIndex));
                        break;
                    }
                    if (packetString[startCodeIndex + EventPayloadLength + 1] == EndTextCode)
                    {

                        string payload = packetString.Substring(startCodeIndex + 1, EventPayloadLength);
                        byte[] nakPacket = Encoding.ASCII.GetBytes(packetString.Substring(startCodeIndex, EventPayloadLength + 2));
                        packetString = packetString.Substring(startCodeIndex + EventPayloadLength + 2);
                        //순서 바꿈
                        OnReceivePayload(payload);
                        //Send NAK
                        try
                        {
                            stream?.Write(nakPacket, 0, nakPacket.Length);
                        }
                        catch (Exception e)
                        {
                            Logger.Warn(e, "Send NAK:");
                        }
                    }
                    else
                    {
                        Logger.Error($"packet missmatched [{packetString.Substring(startCodeIndex, EventPayloadLength + 1)}] is not supported command.");
                        packetString = packetString.Substring(startCodeIndex + 1);
                    }
                }
                else
                {
                    Logger.Error($"[{command}] is not supported command.");
                    packetString = packetString.Substring(startCodeIndex + 1);
                }
            }
        }

        private void OnReceivePayload(string payload)
        {
            if (payload.Length < EventPayloadLength)
            {
                Logger.Error($"payload size is '{payload.Length}' != {EventPayloadLength}");
                return;
            }

            if (IsVerbose)
            {
                Logger.Info($"payload:[{payload}]");
            }

            int offset = 0;
            //command
            string command = payload.Substring(offset, 2);


            //code
            offset = 14;
            string code = payload.Substring(offset, 4);

            bool isPhoneOn = false;
            switch (command)
            {
                case "81":
                    isPhoneOn = true;
                    break;
                case "82":
                    isPhoneOn = false;
                    break;
                default:
                    Logger.Error($"[{payload}/{command}] is not supported command.");
                    return;
            }
            
            string phoneOnOffMessage = isPhoneOn ? "통화시작" : "통화종료";

            Interphone interphone = null;
            if (!_interphoneMapping.TryGetValue(code, out interphone))
            {
                if (IsVerbose)
                {
                    Logger.Info($"[{code} / {phoneOnOffMessage}], but not mapping.");
                }

                return;
            }

            Logger.Info($"{interphone.Name} {phoneOnOffMessage}");

            if (interphone.UsingEvent)
            {
                Task.Factory.StartNew(() =>
                {
                    SendEvent(code, interphone.Name, isPhoneOn);
                });
            }
        }

        private void SendEvent(string interphoneCode, string interphoneName, bool isOn)
        {
            string onOffMessage = isOn ? "통화시작" : "통화종료";
            _stationService.OnEvent(new NodeViewEvent($"{EventIdPrefix}{interphoneCode.Substring(0,2)}0{interphoneCode.Substring(2, 2)}", isOn, $"[{interphoneName}] {onOffMessage}"));
        }
    }

    internal class Interphone
    {
        public string Code { get; }
        public string Name { get; }
        public bool UsingEvent { get; }

        public Interphone(string code, string name, bool usingEvent)
        {
            Code = code;
            Name = name;
            UsingEvent = usingEvent;
        }
    }
}
