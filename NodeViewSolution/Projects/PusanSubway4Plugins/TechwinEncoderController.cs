﻿using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace PusanSubway4Plugins
{
    public class TechwinEncoderController : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Dictionary<string, EncoderInfo> _encoderInfoDictionary = new Dictionary<string, EncoderInfo>();
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("applyPreset", "프리셋 적용", new[]
            {
                new FieldDescription("ip", "", "엔코더 IP", "string"),
                new FieldDescription("channel", "", "카메라 채널", "string"),
                new FieldDescription("id", "", "프리셋 ID", "string"),
            }));

            return commandDescriptions.ToArray();
        }
        public TechwinEncoderController()
        {
            DeviceType = "EncoderController";
        }
        public override bool Init(Configuration config, IStationService stationService)
        {
            if (!base.Init(config, stationService))
            {
                return false;
            }
            string cctvInfoXml = config.GetValue("endocerInfoXml", "NodeViewConfigs/cctvInfo.xml");
            LoadEncoderInfos(cctvInfoXml);
            return true;
        }
        private void LoadEncoderInfos(string encoderInfoXmlFilename)
        {
            try
            {
                string filePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, encoderInfoXmlFilename);
                if (!File.Exists(filePath))
                {
                    Logger.Error($"Cannot find file [{encoderInfoXmlFilename}].");
                    return;
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                XmlNodeList doorNodeList = doc.SelectNodes("/EncoderInfos/Encoder");
                if (doorNodeList == null || doorNodeList.Count == 0)
                {
                    return;
                }

                foreach (XmlNode doorNode in doorNodeList)
                {
                    string ip = XmlUtils.GetStringAttrValue(doorNode, "ip");
                    string id = XmlUtils.GetStringAttrValue(doorNode, "id");
                    string password = XmlUtils.GetStringAttrValue(doorNode, "password");
                    EncoderInfo encoderInfo = new EncoderInfo() { Id = id, Ip = ip, Password = password };
                    _encoderInfoDictionary[encoderInfo.Ip] = encoderInfo;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"LoadEncoderInfos('{encoderInfoXmlFilename}'):");
            }
        }
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "applyPreset", StringComparison.Ordinal) == 0)
                {
                    string ip = parameters.GetValue("ip", "127.0.0.1");
                    string channel = parameters.GetValue("channel", "0");
                    string name = parameters.GetValue("id", "");
                    if (string.IsNullOrWhiteSpace(ip) || string.IsNullOrWhiteSpace(channel) || string.IsNullOrWhiteSpace(name))
                    {
                        Logger.Warn($"RunCommand failed ip : {ip} chaanel : {channel} id : {name}");
                        return false;
                    }

                    Dictionary<string, object> queryStrings = new Dictionary<string, object>();
                    queryStrings.Add("msubmenu", "preset");
                    queryStrings.Add("action", "control");
                    queryStrings.Add("Channel", channel);
                    queryStrings.Add("Preset", name);
                    if (_encoderInfoDictionary.TryGetValue(ip, out var foundEncoder))
                    {
                        var apiClient = new RestApiClient($"http://{ip}")
                        {
                            Credentials = new NetworkCredential(foundEncoder.Id,foundEncoder.Password)
                        };
                        var response = apiClient.Get("/stw-cgi/ptzcontrol.cgi", queryStrings, "");
                        if (!response.IsSuccess)
                        {
                            Logger.Warn("RunCommand failed. response is not success.");
                        }
                    }
                    else
                    {
                        Logger.Warn($"Encoder info is not exist. Ip : {ip}");
                    }
                    isHandled = true;
                   
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }

            return isHandled;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
    }     
    public class EncoderInfo
    {
        public string Ip;
        public string Id;
        public string Password;
    }
}
