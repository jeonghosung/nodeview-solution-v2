﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NLog.Targets.Wrappers;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.Utils;

namespace PusanSubway4Plugins
{
    public class FireSensorReporter : IDataReporter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public const byte StartTextCode = 0x05;
        public const byte EndTextCode = 0x04;
        public const int CrcCodeLength = 2;

        private IStationService _stationService = null;
        private const string ConnectionStartString = "serial://";

        private SerialPort _serialPort = null;
        private ByteDataPool _byteDataPool = new ByteDataPool();
        private byte[] _bufferBytes = new byte[4096];
        private Node _stationFireSensor = null;
        private Dictionary<string, string> _firePointDictionary = new Dictionary<string, string>();

        public string Id { get; protected set; }
        public string Name { get; set; } = "";
        public string StationName { get; set; } = "";
        public string Description { get; set; } = "";
        public string ConnectionString { get; set; } = "";
        public bool IsTestMode { get; set; } = false;
        public bool IsVerbose { get; set; } = false;
        public bool IsStarted { get; protected set; } = false;
        public string NodePath { get; set; } = "stationFireSensors";
        public string NodeId { get; set; } = "";
        public string AlarmIdPrefix { get; set; } = "";

        public bool Init(Configuration config, IStationService stationService)
        {
            _stationService = stationService;
            if (!LoadConfig(config))
            {
                return false;
            }
            if (!RegisterOnStation())
            {
                return false;
            }

            return true;
        }
        protected virtual bool LoadConfig(Configuration config)
        {
            if (config == null)
            {
                if (string.IsNullOrWhiteSpace(Id)) return false;
                return true;
            }
            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    Id = StringUtils.GetStringValue(config.Id, Uuid.NewUuid);
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    Name = this.GetType().Name;
                }
                Description = config.GetValue("@description", "");
                IsTestMode = config.GetValue("isTestMode", false); ;
                IsVerbose = IsTestMode || config.GetValue("isVerbose", false);

                Name = config.GetValue("name", Name);
                StationName = config.GetValue("stationName", Name);
                NodePath = config.GetValue("nodePath", NodePath);
                NodeId = config.GetValue("nodeId", Id);
                AlarmIdPrefix = config.GetValue("alarmIdPrefix", NodeId);

                ConnectionString = config.GetValue("connectionString", "");
                if (SetupConnection())
                {

                    res = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        private bool RegisterOnStation()
        {
            //Report 전용이라 모듈 등록 안해도 됨
            //Node만 등록 하자.
            var node = _stationService.GetNode(NodePath, NodeId);
            //기존 등록된 노드가 없으면 등록한다.
            if (string.IsNullOrWhiteSpace(node?.Id))
            {
                _stationFireSensor = new Node()
                {
                    Id = NodeId,
                    Path = NodePath,
                    NodeType = "stationFireSensor",
                    Status = NodeStatus.Normal
                };
                _stationFireSensor.SetAttribute("name", Name);
                _stationFireSensor.SetProperty("firePoints", "");
                _stationService.CreateNode(NodePath, _stationFireSensor);
            }
            else
            {
                _stationFireSensor = Node.CreateFrom(node);
                _stationFireSensor.Status = NodeStatus.Normal;
                _stationFireSensor.SetProperty("firePoints", "");
                _stationService.UpdateNode(_stationFireSensor);
            }
            return true;
        }
        private void UnRegisterOnStation()
        {
        }


        private bool SetupConnection()
        {
            string partName = "COM3";
            int baudRate = 9600;
            if (!ConnectionString.StartsWith(ConnectionStartString))
            {
                Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                return false;
            }

            string[] connectionParams = StringUtils.Split(ConnectionString.Substring(ConnectionStartString.Length), '/');
            partName = connectionParams[0];
            if (connectionParams.Length > 1)
            {
                baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
            }

            Logger.Info($"Connect({partName}, {baudRate})");
            if (!IsTestMode)
            {
                _serialPort = new SerialPort(partName) { BaudRate = baudRate };
                _serialPort.Open();
                _serialPort.Close();
            }

            return true;
        }

        public bool Start()
        {
            if (IsStarted) return true;

            if (IsTestMode)
            {
                IsStarted = true;
                TestCheckPacket();
                return IsStarted;
            }

            try
            {
                _serialPort.DataReceived += SerialPortOnDataReceived;
                _serialPort.Open();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Start:");
            }

            IsStarted = _serialPort?.IsOpen ?? false;
            return IsStarted;
        }

        public void Stop()
        {
            if (_serialPort != null)
            {
                _serialPort.DataReceived -= SerialPortOnDataReceived;
            }
            UnRegisterOnStation();
            _stationService = null;
            IsStarted = false;
        }

        public void Dispose()
        {
            Stop();
            _serialPort?.Dispose();
            _serialPort = null;
        }

        private void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            int requestByteCount = Math.Min(_serialPort.BytesToRead, _bufferBytes.Length);
            if (requestByteCount <= 0)
            {
                return;
            }

            int readCount = _serialPort.Read(_bufferBytes, 0, requestByteCount);
            if (readCount > 0)
            {
                _byteDataPool.Append(_bufferBytes, 0, readCount);
                CheckPacket(_byteDataPool);
            }
            else if (readCount < 0)
            {
                Logger.Error($"[Id] readCount < 0!");
            }
        }

        private void TestCheckPacket()
        {
            try
            {
                string testDataFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "TestData", "stationFire.txt");
                string testData = File.ReadAllText(testDataFilename).Trim().Replace(" ", "").Replace("\r", "").Replace("\n", "");
                _byteDataPool.Append(StringUtils.GetBytesFromHexString(testData));
                CheckPacket(_byteDataPool);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"TestCheckPacket:");
            }
        }

        private void CheckPacket(ByteDataPool byteDataPool)
        {
            List<byte> packetByteList = new List<byte>();
            bool isFindingStartCode = true;
            while (byteDataPool.Count() > 0)
            {
                if (isFindingStartCode)
                {
                    byte readByte = byteDataPool.ReadByte();
                    if (readByte == StartTextCode)
                    {
                        isFindingStartCode = false;
                        packetByteList.Add(readByte);
                    }
                }
                else
                {
                    byte readByte = byteDataPool.ReadByte();
                    if (readByte == EndTextCode)
                    {
                        packetByteList.Add(readByte);
                        for (int i = 0; i < CrcCodeLength; i++)
                        {
                            byteDataPool.ReadByte();
                        }
                        OnReceivePayload(packetByteList.ToArray());
                        packetByteList.Clear();
                        isFindingStartCode = true;
                    }
                    else if (readByte == StartTextCode)
                    {
                        Logger.Error($"packet error!'{StringUtils.ToHexString(packetByteList.ToArray())}'");
                        packetByteList.Clear();
                        packetByteList.Add(readByte);
                    }
                    else
                    {
                        packetByteList.Add(readByte);
                    }
                }
            }

            if (packetByteList.Count > 0)
            {
                byteDataPool.Append(packetByteList.ToArray());
            }
            packetByteList.Clear();
        }

        private void OnReceivePayload(byte[] payloadBytes)
        {
            if (payloadBytes.Length < 17)
            {
                Logger.Error($"patload size is '{payloadBytes.Length}' < 15");
                return;
            }
            //01wSB06%MB0001000000000000000000000000000000000
            string payload = GetAsciiString(payloadBytes, 1, payloadBytes.Length -2);

            if (IsVerbose)
            {
                Logger.Info($"payload:[{payload}]");
            }

            int offset = 0;
            //header
            string header = payload.Substring(offset, 5);
            if (header != "01wSB")
            {
                Logger.Info($"payload:[{payload}]");
                Logger.Error($"header != '01wSB'!");
                return;
            }
            offset += 5;

            //keyNameLength
            int keyNameLength = int.Parse(payload.Substring(offset, 2), NumberStyles.AllowHexSpecifier);
            offset += 2;
            if (keyNameLength <= 0)
            {
                Logger.Info($"payload:[{payload}]");
                Logger.Error($"keyNameLength < 0!");
                return;
            }

            //keyName
            string keyName = payload.Substring(offset, keyNameLength);
            offset += keyNameLength;

            //dataLength
            int dataLength = int.Parse(payload.Substring(offset, 2), NumberStyles.AllowHexSpecifier);
            offset += 2;
            if (dataLength <= 0)
            {
                Logger.Info($"payload:[{payload}]");
                Logger.Error($"dataLength < 0!");
                return;
            }

            if (payload.Length < (offset + (dataLength*2)))
            {
                Logger.Info($"payload:[{payload}]");
                Logger.Error($"payload.Length < (offset + dataLength)");
                return;
            }
            
            if (payload.Length > (offset + (dataLength * 2)))
            {
                Logger.Info($"payload:[{payload}]");
                Logger.Warn($"payload.Length > (offset + dataLength)");
                return;
            }

            byte[] dataBytes = StringUtils.GetBytesFromHexString(payload.Substring(offset));
            UpdateAlarm(keyName, dataBytes);

        }

        private void UpdateAlarm(string keyName, byte[] dataBytes)
        {
            if (dataBytes == null || dataBytes.Length == 0)
            {
                return;
            }

            //마지막에 항상 ox80 오는것 필터링
            dataBytes[dataBytes.Length - 1] &= 0x7F;
            Dictionary<string, string> alarmPointDictionary = new Dictionary<string, string>();
            for (int byteIndex = 0; byteIndex < dataBytes.Length; byteIndex++)
            {
                byte dataByte = dataBytes[byteIndex];
                if (dataByte == 0x00)
                {
                    continue;
                }

                for (int bitIndex = 0; bitIndex < 8; bitIndex++)
                {
                    if ((dataByte & (0x01 << bitIndex)) != 0)
                    {
                        int point = (byteIndex * 8) + bitIndex + 1;
                        string pointCode = $"{point:D3}";
                        alarmPointDictionary[pointCode] = pointCode;
                    }
                }
            }

            if (alarmPointDictionary.Count == 0 && _firePointDictionary.Count == 0)
            {
                return;
            }

            int updatedCount = 0;
            List<string> alarmOffPointList = new List<string>();
            foreach (var alarmPoint in _firePointDictionary.Keys.ToArray())
            {
                if (alarmPointDictionary.ContainsKey(alarmPoint))
                {
                    alarmPointDictionary.Remove(alarmPoint);
                }
                else
                {
                    updatedCount++;
                    alarmOffPointList.Add(alarmPoint);
                    _firePointDictionary.Remove(alarmPoint);
                }
            }

            foreach (var alarmPoint in alarmPointDictionary.Values.ToArray())
            {
                updatedCount++;
                _firePointDictionary[alarmPoint] = alarmPoint;
            }

            if(updatedCount == 0) return;

            _stationFireSensor.SetProperty("firePoints", string.Join(",", _firePointDictionary.Values));

            int currentFirePointCount = _firePointDictionary.Count;
            _stationFireSensor.Status = currentFirePointCount > 0 ? NodeStatus.Alarm : NodeStatus.Normal;
            _stationService.UpdateNode(_stationFireSensor);

            foreach (var alarmOffPoint in alarmOffPointList.ToArray())
            {
                Logger.Info($"{alarmOffPoint}:화재 해제");
            }

            foreach (var alarmOnPoint in alarmPointDictionary.Values.ToArray())
            {
                Logger.Warn($"{alarmOnPoint}:화재 발생");
            }


            Task.Factory.StartNew(() =>
            {
                SendAlarms(alarmOffPointList.ToArray(), alarmPointDictionary.Values.ToArray());
                if (currentFirePointCount == 0)
                {
                    //화재가 전부 종료 되었을때 000포인트 해제 알람 발생 시킨다. (기존 스틸박스 액션 분석중 발견)
                    SendAlarm("000", false);
                }
            });
        }

        private string GetAsciiString(byte[] buffer, int offset, int length)
        {
            if (buffer.Length < (offset + length))
            {
                Logger.Error($"buffer.Length < (offset+ length).");
                return "";
            }
            if (length < 1)
            {
                return "";
            }

            var stringBytes = buffer.SubArray(offset, length);
            return Encoding.ASCII.GetString(stringBytes);
        }

        private byte GetDataByte(byte[] buffer, int offset)
        {
            if (buffer.Length < (offset + 2))
            {
                Logger.Error($"buffer.Length < (offset+ 2).");
                return 0x00;
            }
            byte realData = 0x00;

            //hi 4bit
            byte bufferData = buffer[offset];
            if ((bufferData & 0xF0) != 0x30)
            {
                Logger.Warn($"[{offset}, 0x{bufferData:X2}] is not start 0xC.");
            }
            realData = (byte)((bufferData & 0x0f) << 4);
            offset++;

            //low 4bit
            bufferData = buffer[offset];
            if ((bufferData & 0xF0) != 0x30)
            {
                Logger.Warn($"[{offset}, 0x{bufferData:X2}] is not start 0xC.");
            }
            realData = (byte)(bufferData & 0x0f);

            return realData;
        }

        private void SendAlarms(string[] alarmOffPoints, string[] alarmOnPoints)
        {
            foreach (string alarmOffPoint in alarmOffPoints)
            {
                SendAlarm(alarmOffPoint, false);
            }

            foreach (string alarmOnPoint in alarmOnPoints)
            {
                SendAlarm(alarmOnPoint, true);
            }
        }

        private void SendAlarm(string pointCode, bool isAlarmOn)
        {
            string onOffMessage = isAlarmOn ? "발생" : "해제";

            if (pointCode == "000")
            {
                _stationService.OnEvent(new NodeViewEvent($"{AlarmIdPrefix}{pointCode}", isAlarmOn, $"[{StationName}]역사 화재 {onOffMessage}"));
            }
            else
            {
                _stationService.OnEvent(new NodeViewEvent($"{AlarmIdPrefix}{pointCode}", isAlarmOn, $"[{StationName}]역사 [{pointCode}]지점 화재 {onOffMessage}"));
            }
        }
    }
}
