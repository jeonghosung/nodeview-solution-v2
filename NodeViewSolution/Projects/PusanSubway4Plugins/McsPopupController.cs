﻿using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace PusanSubway4Plugins
{
    public class McsPopupController : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string ConnectionStartString = "http://";
        private RestApiClient _apiClient = null;

        public McsPopupController()
        {
            DeviceType = "mcsPopupController";
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("openPopup", "팝업 창 생성", new[]
            {
                new FieldDescription("isAlarm", "True", "알람 여부", "bool"),
                new FieldDescription("stationId", "", "역사 ID (01~14)", "string"),
                new FieldDescription("contentId", "", "MCS에 등록된 contents ID", "string"),
                new FieldDescription("message", "", "이벤트/알람 메세지", "string"),
            }));

            commandDescriptions.Add(new CommandDescription("closePopup", "팝업 창 종료", new[]
            {
                new FieldDescription("isAlarm", "True", "알람 여부", "bool"),
                new FieldDescription("stationId", "", "역사 ID (01~14)", "string"),
                new FieldDescription("contentId", "", "팝업에 사용된 contents ID", "string"),
                new FieldDescription("message", "", "이벤트/알람 메세지", "string"),
            }));

            return commandDescriptions.ToArray();
        }

        public override bool Init(Configuration config, IStationService stationService)
        {
            if (!base.Init(config, stationService))
            {
                return false;
            }

            bool res = false;
            try
            {
                if (!ConnectionString.StartsWith(ConnectionStartString))
                {
                    Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                    return false;
                }

                _apiClient = new RestApiClient(ConnectionString);
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "openPopup", StringComparison.Ordinal) == 0)
                {
                    bool isAlarm = parameters.GetValue("isAlarm", true);
                    string stationId = parameters.GetValue("stationId", "");
                    string contentId = parameters.GetValue("contentId", "");
                    string message = parameters.GetValue("message", "");

                    if (string.IsNullOrWhiteSpace(stationId) || string.IsNullOrWhiteSpace(contentId))
                    {
                        Logger.Error($"stationId:[{stationId}] or contentId:[{contentId}] is empty.");
                    }
                    else
                    {
                        var requestJson = new JObject()
                        {
                            ["isAlarm"] = $"{isAlarm}",
                            ["stationId"] = stationId,
                            ["contentId"] = contentId,
                            ["message"] = message
                        };

                        var response = _apiClient.Post("/open", requestJson);
                        if (!response.IsSuccess)
                        {
                            Logger.Warn($"Failed popup open [{stationId}][{contentId}][{isAlarm}][{message}]");
                        }
                    }

                    isHandled = true;
                }
                else if (string.Compare(command, "closePopup", StringComparison.Ordinal) == 0)
                {
                    bool isAlarm = parameters.GetValue("isAlarm", true);
                    string stationId = parameters.GetValue("stationId", "");
                    string contentId = parameters.GetValue("contentId", "");
                    string message = parameters.GetValue("message", "");

                    if (string.IsNullOrWhiteSpace(stationId))
                    {
                        Logger.Error($"stationId:[{stationId}]");
                    }
                    else
                    {
                        var requestJson = new JObject()
                        {
                            ["isAlarm"] = $"{isAlarm}",
                            ["stationId"] = stationId,
                            ["contentId"] = contentId,
                            ["message"] = message
                        };

                        var response = _apiClient.Post("/close", requestJson);
                        if (!response.IsSuccess)
                        {
                            Logger.Warn($"Failed popup close [{stationId}][{contentId}][{isAlarm}][{message}]");
                        }
                    }

                    isHandled = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }

            return isHandled;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _apiClient = null;
            }
        }
    }     
}
