﻿using System.Windows;
using System.Windows.Controls;
using NodeView.Frameworks.Widgets;

namespace PusanSubway4Widgets.Widgets
{
    /// <summary>
    /// PusanSubway4PowerControlWidget.xaml에 대한 상호 작용 논리
    /// </summary>
    /// Todo: 부산 4호선 DLL로 옮겨야 한다.,
    public partial class PowerControlWidget : UserControl, IWidgetControl
    {
        public PowerControlWidget()
        {
            InitializeComponent();
        }

        public IWidgetSource Source
        {
            get => (IWidgetSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IWidgetSource), typeof(PowerControlWidget));

    }
}
