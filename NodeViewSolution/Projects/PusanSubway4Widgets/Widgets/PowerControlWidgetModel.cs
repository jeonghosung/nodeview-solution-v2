﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using NodeView.Utils;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace PusanSubway4Widgets.Widgets
{
    public class PowerControlWidgetModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IDialogService _dialogService;
        private int _pcuButtonCount = 12;
        private string _powerControlUrl = "http://127.0.0.1:20103/api/v1/device/powerControl";
        private RestApiClient _iotStationApiClient = new RestApiClient("");
        private List<PcuButton> _pcuButtons = new List<PcuButton>();
        private string[] _pcuNames;
        private string _name;
        private string _powerControllerId = "powerController";

        private DelegateCommand<string> _powerOnOffCommand;

        public List<PcuButton> PcuButtons
        {
            get => _pcuButtons;
            set => SetProperty(ref _pcuButtons, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public DelegateCommand<string> PowerOnOffCommand => _powerOnOffCommand ?? (_powerOnOffCommand = new DelegateCommand<string>(ExecutePowerOnOffCommand));

        private void ExecutePowerOnOffCommand(string command)
        {
            string[] spliCommand = StringUtils.Split(command,'|');
            if (spliCommand.Length < 2)
            {
                Logger.Warn($"Wrong command : {command}");
                return;
            }
            bool isOn = spliCommand[0].Equals("on", StringComparison.OrdinalIgnoreCase);
            string channel = spliCommand[1];

            var request = new JObject()
            {
                ["action"] = "power",
                ["param"] = new JObject()
                {
                    ["value"] = isOn? "on" : "off",
                    ["seq"] = channel
                }
            };
            var response = _iotStationApiClient.Put(_powerControllerId, request);
            if (!response.IsSuccess)
            {
                Logger.Warn($"response failed : {response.ResponseCode}");
            }
        }


        public PowerControlWidgetModel(IDialogService dialogService, IWidgetSource widgetSource)
        {
            _dialogService = dialogService;
            LoadConfig((IConfiguration)widgetSource.Config);
            Init();
        }

        private void Init()
        {            
            for (int i = 1; i <= _pcuButtonCount; i++)
            {
                PcuButtons.Add(new PcuButton() { ChannelSeq = i.ToString(), Name = string.IsNullOrWhiteSpace(_pcuNames[i - 1]) ? i.ToString() : _pcuNames[i - 1] });
            }
        }

        private void LoadConfig(IConfiguration config)
        {
            _powerControlUrl = config.GetValue("deviceUri", "http://127.0.0.1:20103/api/v1/devices");
            _pcuButtonCount = config.GetValue<int>("chaennelCount", 12);
            _pcuNames = new string[_pcuButtonCount];
            string[] pcuNames = config.GetValue("names", "").Split(',');
            if(pcuNames.Length > 0)
            {
                for (int i = 0; i < pcuNames.Length; i++)
                {
                    _pcuNames[i] = pcuNames[i];
                }
            }
            Name = config.GetValue("@name", "");
            string powerControllerId = config.GetValue("powerControllerId", "");
            if (!string.IsNullOrWhiteSpace(powerControllerId))
            {
                _powerControllerId = powerControllerId;
            }
            _iotStationApiClient = new RestApiClient(_powerControlUrl);
        }
    }

    public class PcuButton
    {
        public string Name { get; set; }
        public string ChannelSeq { get; set; }
    }
}
