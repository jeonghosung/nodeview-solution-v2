﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeiMyungComtekWidgets.Models
{
    public interface IEquipmentOperation
    {
        string DisplayName { get;   set; }
        string Name { get; set; }
    }
}
