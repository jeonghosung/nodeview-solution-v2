﻿using Newtonsoft.Json.Linq;

namespace KeiMyungComtekWidgets.Models
{
    public class EquipmentOperationCount : IEquipmentOperation
    {
        public string DisplayName { get; set; }
        public string Name { get; set; }
        public int Count { get; set; }

        public EquipmentOperationCount(string displayName, string name,  int count = 0)
        {
            DisplayName = displayName;
            Name = name;
            Count = count;
        }
        public JToken ToJson()
        {
            JObject json = new JObject();
            json["name"] = DisplayName;
            json["count"] = Count;
            return json;
        }

       
    }
}
