﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeiMyungComtekWidgets.Models
{
    public class EquipmentOperationStatus : IEquipmentOperation
    {

        public string DisplayName { get; set; }
        public string Name { get; set; }
        public bool Status { get; set; }
        public EquipmentOperationStatus(string diaplayName, string name, bool status = false)
        {
            DisplayName = diaplayName;
            Name = name;
            Status = status;
        }
        public JToken ToJson()
        {
            JObject json = new JObject();
            json["name"] = DisplayName;
            json["status"] = Status;
            return json;
        }
    }
}
