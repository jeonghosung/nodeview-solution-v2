﻿using KeiMyungComtekWidgets.Models;
using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeiMyungComtekWidgets.ViewModels
{
    public  class EquipmentOperationStatusViewModel : BindableBase, IEquipmentOperation
    {
        private string _displayName;
        private string _name;
        private bool _status;

        public string DisplayName
        {
            get => _displayName;
            set => SetProperty(ref _displayName, value);

        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public bool Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }
     
        public EquipmentOperationStatusViewModel(string displayName, string name, bool status = false)
        {
            DisplayName = displayName;
            Name = name;
            Status = status;
        }

    }
}
