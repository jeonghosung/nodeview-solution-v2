﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeiMyungComtekWidgets.Models;
using Prism.Commands;
using Prism.Mvvm;

namespace KeiMyungComtekWidgets.ViewModels
{
    public class EquipmentOperationCountViewModel :BindableBase, IEquipmentOperation
    {
        private string _displayName;
        private string _name;
        private int _count;

        public string DisplayName
        {
            get => _displayName;
            set => SetProperty(ref _displayName, value);

        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public int Count
        {
            get => _count;
            set => SetProperty(ref _count, value);
        }
        
       

        public EquipmentOperationCountViewModel(string displayName, string name, int count = 0)
        {
            DisplayName = displayName;
            Name = name;
            Count = count;
        }
    }
}
