﻿using KeiMyungComtekWidgets.Models;
using NLog;
using NodeView.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using KeiMyungComtekWidgets.ViewModels;
using Prism.Mvvm;
using NodeView.Frameworks.Widgets;
using NodeView.DataModels;
using NodeView.Net;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using Prism.Commands;
using System.Drawing;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace KeiMyungComtekWidgets.Widgets
{
    public class EquipmentOperationSettingWidgetModel :BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private RestApiClient _apiClient;
        private ObservableCollection<EquipmentOperationCountViewModel>  _equipmentOperationCountViewModels = new ObservableCollection<EquipmentOperationCountViewModel>();
        private ObservableCollection<EquipmentOperationStatusViewModel> _equipmentOperationStatusViewModels = new ObservableCollection<EquipmentOperationStatusViewModel>();

        private DelegateCommand<EquipmentOperationStatusViewModel> _equipmentStatusOnCommand;
        private DelegateCommand<EquipmentOperationStatusViewModel> _equipmentStatusOffCommand;
        private DelegateCommand<EquipmentOperationCountViewModel> _equipmentCountUpCommand;
        private DelegateCommand<EquipmentOperationCountViewModel> _equipmentCountDownCommand;
        public ObservableCollection<EquipmentOperationCountViewModel> EquipmentOperationCountViewModels
        {
            get => _equipmentOperationCountViewModels;
            set => SetProperty(ref _equipmentOperationCountViewModels, value);
        }
        public ObservableCollection<EquipmentOperationStatusViewModel> EquipmentOperationStatusViewModels
        {
            get => _equipmentOperationStatusViewModels;
            set => SetProperty(ref _equipmentOperationStatusViewModels, value);
        }
        public EquipmentOperationSettingWidgetModel(IWidgetSource widgetSource)
        {
            LoadConfig((IConfiguration)widgetSource.Config);
            Init();
        }

        public DelegateCommand<EquipmentOperationStatusViewModel> EquipmentStatusOnCommand => _equipmentStatusOnCommand ?? (_equipmentStatusOnCommand = new DelegateCommand<EquipmentOperationStatusViewModel>(ExecuteStatusOnCommand));
        public DelegateCommand<EquipmentOperationStatusViewModel> EquipmentStatusOffCommand => _equipmentStatusOffCommand ?? (_equipmentStatusOffCommand = new DelegateCommand<EquipmentOperationStatusViewModel>(ExecuteStatusOffCommand));
        public DelegateCommand<EquipmentOperationCountViewModel> EquipmentCountUpCommand => _equipmentCountUpCommand ?? (_equipmentCountUpCommand = new DelegateCommand<EquipmentOperationCountViewModel>(ExecuteCountUpCommand));
        public DelegateCommand<EquipmentOperationCountViewModel> EquipmentCountDownCommand => _equipmentCountDownCommand ?? (_equipmentCountDownCommand = new DelegateCommand<EquipmentOperationCountViewModel>(ExecuteCountDownCommand));
        private void LoadConfig(IConfiguration config)
        {
            if (config.TryGetConfiguration("dataServer", out var keiMyungConfig))
            {
                var dataserverUrl = keiMyungConfig.GetValue("dataServerUrl", "");
                if (!string.IsNullOrWhiteSpace(dataserverUrl))
                {
                    _apiClient = new RestApiClient(dataserverUrl);
                }
            }

            
        }
        private void Init()
        {
            LoadEquipmentDatabaseInfos();
            GetEqipmentOperationCountList();
            GetEqipmentOperationStatusList();
        }
        

       

        private void LoadEquipmentDatabaseInfos()
        {
            var configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "configs\\EquipmentDatabaseInfos.xml");
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(configFile);
                XmlNodeList nodeList = doc.SelectNodes("/EquipmentDatabaseInfos/EquipmentDatabaseInfo");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        var column = XmlUtils.GetAttrValue(node, "column", "");
                        var displayName = XmlUtils.GetAttrValue(node, "displayName", "");
                        EquipmentOperationCountViewModels.Add(new EquipmentOperationCountViewModel(displayName, column));
                        EquipmentOperationStatusViewModels.Add(new EquipmentOperationStatusViewModel(displayName, column));
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadLhvwMapping:");
            }

        }
        private void GetEqipmentOperationCountList()
        {
            if (_apiClient != null)
            {
                var response = _apiClient.Get("/equipmentOperationCountList");
                if (response.IsSuccess)
                {
                    foreach (var epuipmentOperationCount in response.ResponseJsonArray)
                    {
                        var jobject = epuipmentOperationCount.Value<JObject>();
                        var name = JsonUtils.GetStringValue(jobject, "name","");
                        int count = JsonUtils.GetIntValue(jobject, "count", 0);

                        if (_equipmentOperationCountViewModels.Any(countList => countList.Name.Equals(name)))
                        {
                            var equipmentOperationCount =
                                _equipmentOperationCountViewModels.First(countList =>
                                    countList.Name.Equals(name));
                            equipmentOperationCount.Count = count;
                        }
                        else
                        {
                            Logger.Warn($"GetEqipmentOperationCountList Not regist name : {name}");
                        }
                    }
                }
            }
        }
        private void GetEqipmentOperationStatusList()
        {
            if (_apiClient != null)
            {
                var response = _apiClient.Get("/equipmentOperationStatus");
                if (response.IsSuccess)
                {
                    foreach (var epuipmentOperationCount in response.ResponseJsonArray)
                    {
                        var jobject = epuipmentOperationCount.Value<JObject>();
                        var name = JsonUtils.GetStringValue(jobject, "name", "");
                        bool status = JsonUtils.GetBoolValue(jobject, "status", false);

                        if (_equipmentOperationStatusViewModels.Any(countList => countList.Name.Equals(name)))
                        {
                            var equipmentOperationStatus =
                                _equipmentOperationStatusViewModels.First(countList =>
                                    countList.Name.Equals(name));
                            equipmentOperationStatus.Status = status;
                        }
                        else
                        {
                            Logger.Warn($"GetEqipmentOperationStatusList Not regist name : {name}");
                        }
                    }
                }
            }
        }
        private void ExecuteCountDownCommand(EquipmentOperationCountViewModel equipmentOperationCountViewModel)
        {
            if(equipmentOperationCountViewModel == null)
            {
                return;
            }

            EquipmentCountUpDown(equipmentOperationCountViewModel.Name, false);
        }

       

        private void ExecuteCountUpCommand(EquipmentOperationCountViewModel equipmentOperationCountViewModel)
        {
            if (equipmentOperationCountViewModel == null)
            {
                return;
            }
            EquipmentCountUpDown(equipmentOperationCountViewModel.Name, true);
        }
        private void ExecuteStatusOffCommand(EquipmentOperationStatusViewModel equipmentOperationStatusViewModel)
        {
            if (equipmentOperationStatusViewModel == null)
            {
                return;
            }
            EquipmentStatusOnOff(equipmentOperationStatusViewModel.Name, false);
        }

        private void ExecuteStatusOnCommand(EquipmentOperationStatusViewModel equipmentOperationStatusViewModel)
        {
            if (equipmentOperationStatusViewModel == null)
            {
                return;
            }
            EquipmentStatusOnOff(equipmentOperationStatusViewModel.Name, true);
        }
        private void EquipmentStatusOnOff(string name, bool isOn)
        {
            if (_apiClient != null)
            {
                JObject request = new JObject()
                {
                    ["name"] = name,
                    ["isOn"] = isOn,
                };
                var response = _apiClient.Post("/setEquipmentOperationStatus", request);
                if (response.IsSuccess)
                {
                    if (_equipmentOperationStatusViewModels.Any(countList => countList.Name.Equals(name)))
                    {
                        var equipmentOperationStatus =
                            _equipmentOperationStatusViewModels.First(countList =>
                                countList.Name.Equals(name));
                        equipmentOperationStatus.Status = isOn;
                    }
                    else
                    {
                        Logger.Warn($"GetEqipmentOperationStatusList Not regist name : {name}");
                    }
                }
            }
        }
        private void EquipmentCountUpDown(string name, bool isUp)
        {
            if (_apiClient != null)
            {
                JObject request = new JObject()
                {
                    ["name"] = name,
                    ["isUp"] = isUp,
                };
                var response = _apiClient.Post("/setEquipmentOperationCount", request);
                if (response.IsSuccess)
                {
                    if (_equipmentOperationCountViewModels.Any(countList => countList.Name.Equals(name)))
                    {
                        var equipmentOperationCount =
                            _equipmentOperationCountViewModels.First(countList =>
                                countList.Name.Equals(name));
                        equipmentOperationCount.Count = isUp ? equipmentOperationCount.Count + 1 : equipmentOperationCount.Count - 1;
                    }
                    else
                    {
                        Logger.Warn($"GetEqipmentOperationCountList Not regist name : {name}");
                    }
                }
            }
        }
    }
}
