﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;
using RestApiResponse = NodeView.WebServers.RestApiResponse;

namespace NodeViewProcessManager.WebServices
{
    [RestResource(BasePath = "/api/v1")]
    public class ApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = "")]
        public IHttpContext Echo(IHttpContext context)
        {
            Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok, "echo!");
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }

        [RestRoute(HttpMethod = Semsol.Grapevine.Shared.HttpMethod.GET, PathInfo = "/")]
        public IHttpContext Index(IHttpContext context)
        {
            Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok,
                    TemplateManager.Render("PageLinks", GetServicePageList()), Encoding.UTF8);
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }
        private object GetServicePageList()
        {
            var linkInfos = new List<PageLinkInfo>();
            linkInfos.Add(new PageLinkInfo("Apps", "/api/v1/apps", ""));
            return linkInfos;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/apps")]
        public IHttpContext GetApps(IHttpContext context)
        {
            Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            try
            {
                ProcessManagerService service = ProcessManagerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }

                var apps = service.GetApps();
                if (apps != null)
                {
                    JArray resArray = new JArray();
                    foreach (var app in apps)
                    {
                        resArray.Add(app.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get apps.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetApps!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/apps/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext GetApp(IHttpContext context)
        {
            Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            try
            {
                ProcessManagerService service = ProcessManagerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }

                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/apps/(?<id>[a-zA-Z0-9_\-]+)$");
                if (match.Success)
                {
                    id = match.Groups["id"].Value;
                }

                var app = service.GetApp(id);
                if (app != null)
                {
                    RestApiResponse.SendResponseSuccess(context, app.ToJson());
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get app.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetApp!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/apps")]
        public IHttpContext PutActionApps(IHttpContext context)
        {
            Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            try
            {
                ProcessManagerService service = ProcessManagerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }

                ApiResponse result = ApiResponse.BadRequest;
                JToken responseJson = null;

                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "register", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    var requestApp = ProcessNode.CreateFrom(JsonUtils.GetValue(json, "param", new JObject()));
                    if (requestApp != null)
                    {
                        var app = service.RegisterApp(requestApp);
                        if (app != null)
                        {
                            result = ApiResponse.Success;
                            responseJson = app.ToJson();
                        }
                    }
                }
                else if (string.Compare(putAction, "start", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    var app = service.StartApp(id);
                    if (app != null)
                    {
                        result = ApiResponse.Success;
                        responseJson = app.ToJson();
                    }
                }
                else if (string.Compare(putAction, "stop", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    var app = service.StopApp(id);
                    if (app != null)
                    {
                        result = ApiResponse.Success;
                        responseJson = app.ToJson();
                    }
                }
                else if (string.Compare(putAction, "restart", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    var app = service.RestartApp(id);
                    if (app != null)
                    {
                        result = ApiResponse.Success;
                        responseJson = app.ToJson();
                    }
                }
                else if (string.Compare(putAction, "getConfig", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    var config = service.GetConfig(id);
                    if (config != null)
                    {
                        result = ApiResponse.Success;
                        responseJson = config.ToJson();
                    }
                }
                else if (string.Compare(putAction, "setConfig", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    JObject configJson = JsonUtils.GetValue<JObject>(json, "param.config", null);
                    if (configJson != null)
                    {
                        var config = Configuration.CreateFrom(configJson);
                        if (config != null)
                        {
                            if(service.SetConfig(id, config))
                            {
                                result = ApiResponse.Success;
                                responseJson = new JObject();
                            }
                        }
                    }
                }
                else if (string.Compare(putAction, "getLogFileNames", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    var fileNames = service.GetLogFileNames(id);
                    if (fileNames != null)
                    {
                        JArray array = new JArray();
                        foreach (string filename in fileNames)
                        {
                            array.Add(filename);
                        }
                        result = ApiResponse.Success;
                        responseJson = array;
                    }
                }
                else if (string.Compare(putAction, "getLog", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    string fileName = JsonUtils.GetValue(json, "param.fileName", "");
                    var base64EncodedText = service.GetLogFile(id, fileName);
                    if (base64EncodedText != null)
                    {
                        result = ApiResponse.Success;
                        responseJson = new JObject()
                        {
                            ["log"] = base64EncodedText ?? ""
                        };
                    }
                }

                if (result.IsSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, responseJson);
                }
                else
                {
                    RestApiResponse.SendResponse(context, result);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Widnows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
    }
}
