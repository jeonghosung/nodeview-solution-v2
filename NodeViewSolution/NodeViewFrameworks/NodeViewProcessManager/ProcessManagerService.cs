﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Protocols.NodeViews;
using NodeView.Systems;
using NodeView.Utils;
using NodeView.WebServers;
using Prism.Ioc;

namespace NodeViewProcessManager
{
    public class ProcessManagerService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region SingleTone
        private static readonly Lazy<ProcessManagerService> Lazy = new Lazy<ProcessManagerService>(() => new ProcessManagerService());
        public static ProcessManagerService Instance => Lazy.Value;
        #endregion

        protected WebServer _webServer = null;
        protected volatile bool _isStarted = false;

        private readonly Dictionary<string, ProcessNode> _processNodeDictionary = new Dictionary<string, ProcessNode>();

        public IContainerProvider ContainerProvider { get; protected set; }
        public Configuration Config { get; protected set; }
        public bool IsTestMode { get; protected set; } = false;
        public bool IsVerbose { get; protected set; } = false;
        public string ServiceIp { get; protected set; }
        public int ServicePort { get; protected set; }
        public string ServiceName { get; protected set; }
        public string ServiceBaseUrl { get; protected set; }
        public string CachesFolder { get; protected set; } = "";
        public string ConfigsFolder { get; protected set; } = "";
        public bool IsStarted
        {
            get => _isStarted;
            protected set => _isStarted = value;
        }

        protected ProcessManagerService()
        {
            CachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(CachesFolder);
            ConfigsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            Directory.CreateDirectory(ConfigsFolder);
        }

        public bool Initialize(IContainerProvider containerProvider, string configFilename = "")
        {
            ContainerProvider = containerProvider;
            if (string.IsNullOrWhiteSpace(configFilename))
            {
                configFilename = AppUtils.GetAppName() + ".config.xml";
            }

            if (!File.Exists(configFilename))
            {
                configFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
                if (!File.Exists(configFilename))
                {
                    Logger.Error("Cannot find config file.");
                    return false;
                }
            }
            Config = Configuration.Load(configFilename);
            if (Config == null)
            {
                Logger.Error("Cannot load config file.");
                return false;
            }

            IsTestMode = Config.GetValue("isTestMode", IsTestMode);
            IsVerbose = IsTestMode || Config.GetValue("isVerbose", IsVerbose);
            ServiceIp = Config.GetValue("service.ip", "127.0.0.1");
            ServicePort = Config.GetValue("service.port", 20201);
            ServiceName = Config.GetValue("service.name", AppUtils.GetAppName());
            ServiceBaseUrl = $"http://{ServiceIp}:{ServicePort}";
            _webServer = new WebServer(ServicePort);
            
            if (!OnInitializing(Config))
            {
                Logger.Error("Cannot initializing.");
                return false;
            }

            OnInitialized();
            return true;
        }

        public bool Start()
        {
            if (!IsStarted)
            {
                try
                {
                    if (!OnStart()) return false;
                    if (!_webServer?.Start() ?? false)
                    {
                        Logger.Error("Cannot start web server.");
                        return false;
                    }

                    OnStarted();
                    IsStarted = true;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Start:");
                }
            }
            return IsStarted;
        }

        public void Stop()
        {
            try
            {
                _webServer?.Stop();
                OnStop();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Stop:");
            }
            IsStarted = false;
        }
        protected virtual bool OnInitializing(Configuration config)
        {
            return true;
        }
        protected virtual void OnInitialized()
        {
            LoadCachedApps();
        }

        protected virtual bool OnStart()
        {
            return true;
        }

        protected virtual void OnStarted()
        {

        }

        protected virtual void OnStop()
        {
        }
        private void LoadCachedApps()
        {
            try
            {
                string cachedAppsFile = Path.Combine(CachesFolder, "processNodes.json");
                if (File.Exists(cachedAppsFile))
                {
                    string text = File.ReadAllText(cachedAppsFile);
                    JArray jsonArray = JArray.Parse(text);
                    foreach (var jsonToken in jsonArray)
                    {
                        var processNode = ProcessNode.CreateFrom(jsonToken.Value<JObject>());
                        if (processNode != null)
                        {
                            _processNodeDictionary[processNode.Id] = processNode;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadCachedApps:");
            }
        }
        private void SaveCachedApps()
        {
            try
            {
                string cachedAppsFile = Path.Combine(CachesFolder, "processNodes.json");
                JArray jsonArray = new JArray();
                foreach (var processNode in _processNodeDictionary.Values.ToArray())
                {
                    jsonArray.Add(processNode.ToJson());
                }
                File.WriteAllText(cachedAppsFile, jsonArray.ToString());
            }
            catch (Exception e)
            {
                Logger.Error(e, "SaveCachedApps:");
            }
        }

        public ProcessNode[] GetApps()
        {
            UpdateAllStatus();
            return _processNodeDictionary.Values.ToArray();
        }

        public ProcessNode GetApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var foundProcessNode))
                {
                    UpdateProcessStatus(foundProcessNode);
                    if (_processNodeDictionary.TryGetValue(id, out var process))
                    {
                        return process;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetApp:");
            }
            return null;
        }


        public ProcessNode RegisterApp(ProcessNode processNode)
        {
            if (string.IsNullOrWhiteSpace(processNode?.Id)) return null;

            try
            {
                if (_processNodeDictionary.TryGetValue(processNode.Id, out var foundProcess))
                {
                    foundProcess.Update(processNode);
                    SaveCachedApps();
                    return foundProcess;
                }
                _processNodeDictionary[processNode.Id] = processNode;
                SaveCachedApps();
            }
            catch (Exception e)
            {
                Logger.Error(e, "RegisterApp");
            }

            return processNode;
        }

        public ProcessNode StartApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var process))
                {
                    if (StartProcess(process))
                    {
                        return process;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StartApp:");
            }
            
            return null;
        }
        public ProcessNode StopApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var process))
                {
                    if (StopProcess(process))
                    {
                        return process;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopApp:");
            }
            return null;
        }
        public ProcessNode RestartApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var process))
                {
                    if (RestartProcess(process))
                    {
                        return process;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "RestartApp:");
            }

            return null;
        }

        private bool StartProcess(ProcessNode processNode)
        {
            if (processNode == null) return false;

            try
            {
                if (processNode.ProcessType == ProcessType.Application)
                {
                    return StartApplication(processNode);
                }

                if (processNode.ProcessType == ProcessType.Service)
                {
                    return StartService(processNode);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StartProcess:");
            }

            return false;
        }

        private bool StartApplication(ProcessNode processNode)
        {
            if (processNode == null || processNode.ProcessType != ProcessType.Application) return false;

            try
            {
                string path = Path.Combine(processNode.WorkingFolder, processNode.Execution);
                var currentProcess = GetCurrentProcess(processNode);
                if (currentProcess == null)
                {
                    processNode.Status = ProcessRunningStatus.Stopped;
                    if (!File.Exists(path))
                    {
                        _processNodeDictionary.Remove(processNode.Id);
                        SaveCachedApps();
                        return false;
                    }
                    processNode.Status = ProcessRunningStatus.Stopped;
                }

                using (currentProcess)
                {
                    var runningStatus = GetStatus(currentProcess);
                    if (runningStatus == ProcessRunningStatus.Running)
                    {
                        return true;
                    }
                }
                
                int processId = 0;
                if (Environment.UserInteractive)
                {
                    var process = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = path,
                            Arguments = processNode.Argument,
                            UseShellExecute = false,
                            CreateNoWindow = true,
                            WorkingDirectory = processNode.WorkingFolder,
                        }
                    };

                    if (process.Start())
                    {
                        processId = process.Id;
                    }
                }
                else
                {
                    processId = WindowsServiceUtils.CreateProcessAsCurrentUser(path, processNode.Argument, processNode.WorkingFolder);

                }

                if (processId != 0)
                {
                    processNode.Status = GetStatus(processId);
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "StartApplication:");
            }

            return false;
        }

        private bool StartService(ProcessNode processNode)
        {
            if (processNode == null || processNode.ProcessType != ProcessType.Service) return false;
            try
            {
                using (var serviceController = new ServiceController(processNode.Execution))
                {
                    var runningStatus = GetStatus(serviceController);
                    if (runningStatus == ProcessRunningStatus.None)
                    {
                        _processNodeDictionary.Remove(processNode.Id);
                        SaveCachedApps();
                        return false;
                    }


                    if (serviceController.Status == ServiceControllerStatus.Stopped)
                    {
                        serviceController.Start();
                    }
                    processNode.Status = GetStatus(serviceController);
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StartService:");
            }

            return false;
        }

        private bool StopProcess(ProcessNode processNode)
        {
            if (processNode == null) return false;

            try
            {
                if (processNode.ProcessType == ProcessType.Application)
                {
                    return StopApplication(processNode);
                }

                if (processNode.ProcessType == ProcessType.Service)
                {
                    return StopService(processNode);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopProcess:");
            }

            return false;
        }

        private bool StopApplication(ProcessNode processNode)
        {
            if (processNode == null || processNode.ProcessType != ProcessType.Application) return false;

            try
            {
                var currentProcess = GetCurrentProcess(processNode);
                if (currentProcess == null)
                {
                    string path = Path.Combine(processNode.WorkingFolder, processNode.Execution);
                    if (!File.Exists(path))
                    {
                        _processNodeDictionary.Remove(processNode.Id);
                        SaveCachedApps();
                        return false;
                    }

                    processNode.Status = ProcessRunningStatus.Stopped;
                    return true;
                }

                using (currentProcess)
                {
                    var runningStatus = GetStatus(currentProcess);
                    if (runningStatus == ProcessRunningStatus.Running)
                    {
                        if (!string.IsNullOrWhiteSpace(processNode.StopApiUrl))
                        {
                            try
                            {
                                WebRequest request = WebRequest.Create(processNode.StopApiUrl);
                                WebResponse response = request.GetResponse();
                                Thread.Sleep(1000);
                            }
                            catch
                            {
                                // skip
                            }
                        }

                        if (!currentProcess.HasExited)
                        {
                            currentProcess.Kill();
                        }
                    }
                }
                processNode.Status = ProcessRunningStatus.Stopped;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopApplication:");
            }

            return false;
        }

        private bool StopService(ProcessNode processNode)
        {
            if (processNode == null || processNode.ProcessType != ProcessType.Service) return false;
            try
            {
                using (var serviceController = new ServiceController(processNode.Execution))
                {
                    var runningStatus = GetStatus(serviceController);
                    if (runningStatus == ProcessRunningStatus.None)
                    {
                        _processNodeDictionary.Remove(processNode.Id);
                        SaveCachedApps();
                        return false;
                    }


                    if (serviceController.Status == ServiceControllerStatus.Running)
                    {
                        serviceController.Stop();
                    }
                    processNode.Status = ProcessRunningStatus.Stopped;
                    return true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopService:");
            }

            return false;
        }

        private bool RestartProcess(ProcessNode processNode)
        {
            try
            {
                if (StopProcess(processNode))
                {
                    Thread.Sleep(500);
                    return StartProcess(processNode);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "RestartProcess:");
            }
            return false;
        }

        private void UpdateAllStatus()
        {
            try
            {
                foreach (var processNode in _processNodeDictionary.Values.ToArray())
                {
                    UpdateProcessStatus(processNode);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateAllStatus:");
            }
        }

        private void UpdateProcessStatus(ProcessNode processNode)
        {
            if (processNode == null) return;

            try
            {
                if (processNode.ProcessType == ProcessType.Application)
                {
                    UpdateApplicationStatus(processNode);
                }
                else if (processNode.ProcessType == ProcessType.Service)
                {
                    UpdateServiceStatus(processNode);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateProcessStatus:");
            }
        }

        private void UpdateApplicationStatus(ProcessNode processNode)
        {
            if (processNode == null || processNode.ProcessType != ProcessType.Application) return;
            try
            {
                var currentProcess = GetCurrentProcess(processNode);
                if (currentProcess == null)
                {
                    string path = Path.Combine(processNode.WorkingFolder, processNode.Execution);
                    if (!File.Exists(path))
                    {
                        _processNodeDictionary.Remove(processNode.Id);
                        SaveCachedApps();
                    }

                    processNode.Status = ProcessRunningStatus.Stopped;
                }
                else
                {
                    processNode.Status = GetStatus(currentProcess);
                    currentProcess.Dispose();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateApplicationStatus:");
            }
        }
        private void UpdateServiceStatus(ProcessNode processNode)
        {
            if (processNode == null || processNode.ProcessType != ProcessType.Service) return;
            try
            {
                using (var serviceController = new ServiceController(processNode.Execution))
                {
                    var runningStatus = GetStatus(serviceController);
                    if (runningStatus == ProcessRunningStatus.None)
                    {
                        _processNodeDictionary.Remove(processNode.Id);
                        SaveCachedApps();
                    }
                    else
                    {
                        processNode.Status = runningStatus;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateServiceStatus:");
            }
        }


        private ProcessRunningStatus GetStatus(Process process)
        {
            if (process == null)
            {
                return ProcessRunningStatus.Stopped;

            }
            return process.HasExited ? ProcessRunningStatus.Stopped : ProcessRunningStatus.Running;
        }

        private ProcessRunningStatus GetStatus(int processId)
        {
            try
            {
                using (Process process = Process.GetProcessById(processId))
                {
                    return process.HasExited ? ProcessRunningStatus.Stopped : ProcessRunningStatus.Running;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetStatus:");
            }

            return ProcessRunningStatus.None;
        }
        private ProcessRunningStatus GetStatus(ServiceController serviceController)
        {
            ProcessRunningStatus runningStatus = ProcessRunningStatus.None;
            try
            {
                switch (serviceController.Status)
                {
                    case ServiceControllerStatus.ContinuePending:
                        runningStatus = ProcessRunningStatus.Running;
                        break;
                    case ServiceControllerStatus.Paused:
                        runningStatus = ProcessRunningStatus.Stopped;
                        break;
                    case ServiceControllerStatus.PausePending:
                        runningStatus = ProcessRunningStatus.Stopped;
                        break;
                    case ServiceControllerStatus.Running:
                        runningStatus = ProcessRunningStatus.Running;
                        break;
                    case ServiceControllerStatus.StartPending:
                        runningStatus = ProcessRunningStatus.Running;
                        break;
                    case ServiceControllerStatus.Stopped:
                        runningStatus = ProcessRunningStatus.Stopped;
                        break;
                    case ServiceControllerStatus.StopPending:
                        runningStatus = ProcessRunningStatus.Stopped;
                        break;
                    default:
                        Logger.Error($"'{serviceController.Status}' is not supported status.");
                        runningStatus = ProcessRunningStatus.None;
                        break;
                }
            }
            catch(Exception ex)
            {
                Logger.Error(ex, "GetStatus Error.");
                runningStatus = ProcessRunningStatus.None;
            }

            return runningStatus;
        }


        private Process GetCurrentProcess(ProcessNode processNode)
        {
            try
            {
                Process[] processes = Process.GetProcessesByName(processNode.Name);
                if (processes.Length > 0)
                {
                    return processes[0];
                }
            }
            catch
            {
                //skip
            }
            return null;
        }

        public Configuration GetConfig(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var processNode))
                {
                    if (string.IsNullOrWhiteSpace(processNode.ConfigFilename))
                    {
                        return null;
                    }
                    string path = Path.Combine(processNode.WorkingFolder, processNode.ConfigFilename);
                    if (!File.Exists(path))
                    {
                        return null;
                    }

                    var config = Configuration.Load(path);
                    return config;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetConfig:");
            }
            return null;
        }

        public bool SetConfig(string id, Configuration config)
        {
            if (string.IsNullOrWhiteSpace(id) || config == null) return false;

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var processNode))
                {
                    if (string.IsNullOrWhiteSpace(processNode.ConfigFilename))
                    {
                        return false;
                    }
                    string path = Path.Combine(processNode.WorkingFolder, processNode.ConfigFilename);
                    if (!File.Exists(path))
                    {
                        return false;
                    }

                    return Configuration.Save(path, config);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetConfig");
            }
            
            return false;
        }

        public string[] GetLogFileNames(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return new string[0];

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var processNode))
                {
                    if (string.IsNullOrWhiteSpace(processNode.ConfigFilename))
                    {
                        return null;
                    }
                    string path = Path.Combine(processNode.WorkingFolder, "logs");
                    if (Directory.Exists(path))
                    {
                        var files = Directory.GetFiles(path, "*.log");
                        List<string> fileNames = new List<string>();
                        foreach (var file in files)
                        {
                            fileNames.Add(Path.GetFileName(file));
                        }
                        return fileNames.ToArray();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetLogFileNames:");
            }
            
            return new string[0];
        }

        public string GetLogFile(string id, string logFilename)
        {
            if (string.IsNullOrWhiteSpace(id)) return "";

            try
            {
                if (_processNodeDictionary.TryGetValue(id, out var processNode))
                {
                    if (string.IsNullOrWhiteSpace(processNode.ConfigFilename))
                    {
                        return null;
                    }
                    string path = Path.Combine(processNode.WorkingFolder, "logs", logFilename);
                    if (File.Exists(path))
                    {
                        string plainText = File.ReadAllText(path, Encoding.Default);
                        return StringUtils.Base64Encode(plainText);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetLogFile:");
            }
            
            return "";
        }
    }
}
