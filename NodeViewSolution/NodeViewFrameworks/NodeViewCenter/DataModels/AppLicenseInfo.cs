﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;

namespace NodeViewCenter.DataModels
{
    public class AppLicenseInfo
    {
        public string Name { get; }
        public int LicenseCount { get; }
        public int RegisteredCount => RegisteredAppInfos.Count;
        public int BlockedCount => BlockedAppInfos.Count;
        public List<NodeViewAppNode> RegisteredAppInfos { get; } = new List<NodeViewAppNode>();
        public List<NodeViewAppNode> BlockedAppInfos { get; } = new List<NodeViewAppNode>();

        public AppLicenseInfo(string name, int licenseCount)
        {
            Name = name;
            LicenseCount = licenseCount;
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["name"] = Name;
            json["licenseCount"] = LicenseCount;
            json["registeredCount"] = RegisteredCount;
            json["blockedCount"] = BlockedCount;
            json["licenseCount"] = LicenseCount;

            JArray registeredAppInfoArray = new JArray();
            foreach (var appInfo in RegisteredAppInfos)
            {
                registeredAppInfoArray.Add(appInfo.ToJson());
            }
            json["registered"] = registeredAppInfoArray;

            JArray blockedAppInfoArray = new JArray();
            foreach (var appInfo in BlockedAppInfos)
            {
                blockedAppInfoArray.Add(appInfo.ToJson());
            }
            json["blocked"] = blockedAppInfoArray;

            return json;
        }
    }
}
