﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;

namespace NodeViewCenter.Plugins
{
    public class NodeViewEventSenderForCenter : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string ConnectionStartString = "http";
        private RestApiClient _apiClient = null;

        public NodeViewEventSenderForCenter(string id)
        {
            Id = id;
            DeviceType = "eventSender";
            Name = "EventSender";
            Description = "NodeView Event Sender";
            ConnectionString = "";
            IsTestMode = false;
            IsVerbose = false;
        }

        protected override bool LoadConfig(Configuration config)
        {
            return true;
        }

        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("eventOn", "이벤트 발생", new[]
            {
                new FieldDescription("id", "eventId", "이벤트 아이디"),
                new FieldDescription("targetIds", "", "AppId list','로 구분, 하나이상 필요", "stringList", isOptional:true),
                new FieldDescription("message", "", "이벤트 메세지", isOptional:true),
                new FieldDescription("param", "", "이벤트 전달값", isOptional:true)
            }));
            commandDescriptions.Add(new CommandDescription("eventOff", "이벤트 종료", new[]
            {
                new FieldDescription("id", "eventId", "이벤트 아이디")
            }));

            return commandDescriptions.ToArray();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _apiClient = null;
            }
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }
                if (string.Compare(command, "eventOn", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (!string.IsNullOrWhiteSpace(id))
                    {

                        isHandled = ControlEventOn(id, parameters);
                    }
                }
                else if (string.Compare(command, "eventOff", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (!string.IsNullOrWhiteSpace(id))
                    {
                        isHandled = ControlEventOff(id);
                    }
                }
               
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }

        //Todo:이건 구현 다시해야 한다.
        private bool ControlEventOn(string id, IKeyValueCollection parameters = null)
        {
            bool result = false;
            if (parameters == null)
            {
                return false;
            }
            if (parameters.TryGetValue("targetIds", out object targetIds))
            {
                string[] appIds = targetIds.ToString().Split(',');
                if (appIds.Length > 0)
                {
                    JObject request = new JObject()
                    {
                        ["id"] = id,
                        ["isOn"] = true,
                    };
                    foreach (var keyValuePair in parameters)
                    {
                        switch (keyValuePair.Key)
                        {
                            case "id":
                            case "isOn":
                                break;
                            default:
                                request[keyValuePair.Key] = $"{keyValuePair.Value}";
                                break;

                        }
                    }
                    var apps = CenterService.Instance.GetApps();
                    foreach (var appId in appIds)
                    {
                        if (apps.Any(app => app.Id.Equals(appId, StringComparison.OrdinalIgnoreCase)))
                        {
                            var currentApp = apps.First(app =>
                                app.Id.Equals(appId, StringComparison.OrdinalIgnoreCase));
                            _apiClient = new RestApiClient(currentApp.Uri);
                            var response = _apiClient.Post("/events", request);
                            if (!response.IsSuccess)
                            {
                                Logger.Warn($"ControlEventOn failed. Id : {currentApp.Id} Ip : {currentApp.Uri}");
                            }
                            result = response.IsSuccess;
                        }
                        else
                        {
                            Logger.Warn($"AppId is not regist. AppId : {appId}");
                            return false;
                        }
                    }
                }
            }
            else
            {
                Logger.Warn("targetId is null.");
            }

           
           
            return result;
        }

        private bool ControlEventOff(string id)
        {
            if (_apiClient == null)
            {
                Logger.Error("apiClient is null.");
                return false;
            }

            JObject request = new JObject()
            {
                ["id"] = id,
                ["isOn"] = false,
            };
            var response = _apiClient.Post("/events", request);
            return response.IsSuccess;
        }
    }
}
