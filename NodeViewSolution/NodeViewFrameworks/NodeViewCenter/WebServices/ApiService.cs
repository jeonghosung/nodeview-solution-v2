﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeView.Net;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;
using RestApiResponse = NodeView.WebServers.RestApiResponse;

namespace NodeViewCenter.WebServices
{
    [RestResource(BasePath = "/api/v1")]
    public class ApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IAuthentication _authentication = null;

        public ApiService()
        {
            try
            {
                _authentication = (IAuthentication)NodeViewContainerExtension.GetContainerProvider()?.Resolve(typeof(IAuthentication));
            }
            catch
            {
                //Logger.Info("Not supported IAuthentication.");
                _authentication = null;
            }
        }

        private bool ValidateAccessToken(IHttpContext context)
        {
            return _authentication?.ValidateAccessToken(context) ?? true;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = "")]
        public IHttpContext Echo(IHttpContext context)
        {
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok, "echo!");
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }

        [RestRoute(HttpMethod = Semsol.Grapevine.Shared.HttpMethod.GET, PathInfo = "/")]
        public IHttpContext Index(IHttpContext context)
        {
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok,
                    TemplateManager.Render("PageLinks", GetServicePageList()), Encoding.UTF8);
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }
        private object GetServicePageList()
        {
            var linkInfos = new List<PageLinkInfo>();
            linkInfos.Add(new PageLinkInfo("Apps", "/api/v1/apps", ""));
            return linkInfos;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/licenses")]
        public IHttpContext GetLicenses(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                CenterService centerService = CenterService.Instance;

                var appLicenses = centerService.GetLicenses();
                if (appLicenses != null)
                {
                    JArray resArray = new JArray();
                    foreach (var appLicense in appLicenses)
                    {
                        resArray.Add(appLicense.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get licenses.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetLicenses!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/licenses")]
        public IHttpContext PutActionLicenses(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ApiResponse result = ApiResponse.BadRequest;
                JObject responseJson = new JObject();

                CenterService centerService = CenterService.Instance;

                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "removeApp", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    string id = JsonUtils.GetStringValue(json, "param.id");
                    result = centerService.RequestRemoveApp(requester, id);
                }
                else if (string.Compare(putAction, "removeAllApps", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    result = centerService.RequestRemoveAllApps(requester);
                }
                else if (string.Compare(putAction, "blockApp", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    string id = JsonUtils.GetStringValue(json, "param.id");
                    result = centerService.RequestBlockApp(requester, id);
                }

                if (result.IsSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, responseJson);
                }
                else
                {
                    RestApiResponse.SendResponse(context, result);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Widnows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/apps")]
        public IHttpContext GetApps(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                CenterService centerService = CenterService.Instance;

                var apps = centerService.GetApps();
                if (apps != null)
                {
                    JArray resArray = new JArray();
                    foreach (var app in apps)
                    {
                        resArray.Add(app.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get apps.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetApps!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/apps")]
        public IHttpContext PutActionApps(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                CenterService centerService = CenterService.Instance;
                ApiResponse result = ApiResponse.BadRequest;
                JObject responseJson = new JObject();

                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "keep-alive", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    var requestApp = NodeViewAppNode.CreateFrom(JsonUtils.GetValue(json, "param", new JObject()));
                    if (requestApp != null)
                    {
                        string requestAppIp = requestApp.Ip?.ToLower() ?? "";
                        switch (requestAppIp)
                        {
                            case "":
                            case "127.0.0.1":
                            case "localhost":
                                requestApp.Ip = $"{context.Request.RemoteEndPoint.Address}";
                                requestApp.Uri = requestApp.Uri.Replace($"//{requestAppIp}", $"//{requestApp.Ip}");
                                break;
                        }
                        result = centerService.RequestKeepAlive(requester, requestApp);
                    }
                }
                else if (string.Compare(putAction, "updateInfo", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    var paramObject = JsonUtils.GetValue(json, "param", new JObject());
                    string appKey = "";
                    KeyValueCollection parameters = new KeyValueCollection();
                    foreach (var property in paramObject.Properties())
                    {
                        if (property.Name == "appKey")
                        {
                            appKey = $"{property.Value}";
                        }
                        else
                        {
                            parameters[property.Name] = $"{property.Value}";
                        }
                    }
                    result = centerService.RequestUpdateAppInfo(requester, appKey, parameters);
                }

                if (result.IsSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, responseJson);
                }
                else
                {
                    RestApiResponse.SendResponse(context, result);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Widnows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
    }
}
