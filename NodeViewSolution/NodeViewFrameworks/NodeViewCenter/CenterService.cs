﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.NodeViews;
using NodeView.Protocols.NodeViews;
using NodeView.Utils;
using NodeViewCenter.DataModels;
using NodeViewCenter.Plugins;
using NodeViewMics;
using System.Net;
using System.Text;

namespace NodeViewCenter
{
    public class CenterService : StationServiceManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private NodeViewProcessManagerClient _processManagerClient;

        #region SingleTone

        private static readonly Lazy<CenterService> Lazy = new Lazy<CenterService>(() => new CenterService());
        public static CenterService Instance => Lazy.Value;

        #endregion


        private List<AppLicenseInfo> _appLicenseInfos = new List<AppLicenseInfo>();
        private ILicenseCard _licenseCard;

        private CenterService()
        {
            IsNodeVewApp = false;
        }
        protected override bool OnInitializing(Configuration config)
        {
            var res = LicenseReader.TryGetLicenseCard("NodeView", out var licenseCard);
            int processManagerPort = Config.GetValue("processManager.port", 20102);
            _processManagerClient = new NodeViewProcessManagerClient("127.0.0.1", processManagerPort);
            if (res == LicenseResponseCode.OK)
            {
                _licenseCard = licenseCard;

                foreach (var appLicense in _licenseCard.AppLicenses)
                {
                    _appLicenseInfos.Add(new AppLicenseInfo(appLicense.Name, appLicense.Count));
                }
            }
            else
            {
                Logger.Error($"License Error : {res}");
                return false;
            }

            LoadApps();
            return true;
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            Task.Factory.StartNew(() =>
            {
                _processManagerClient.RegisterApp(ProcessType.Application, AppUtils.GetAppName(), ConfigFilename, "");
            });
        }

        protected override void InitDefaultPlugins()
        {
            base.InitDefaultPlugins();

            var eventSender = new NodeViewEventSenderForCenter("eventSender");
            eventSender.Init(null, this);
            eventSender.IsTestMode = IsTestMode;
            eventSender.IsVerbose = IsVerbose;
            _pluginManager.AddPlugin(eventSender);
        }

        
        protected override bool RunDeviceCommand(NodeViewCommand command)
        {
            if (string.IsNullOrWhiteSpace(command?.DeviceId) || string.IsNullOrWhiteSpace(command?.Command))
                return false;

            var appId = command.AppId;
            if (string.IsNullOrWhiteSpace(appId) || appId == "center")
            {
                command.AppId = "";
                return base.RunDeviceCommand(command);
            }

            var app = FindAppNode(appId);
            if (app == null)
            {
                Logger.Warn($"Cannnot find app by [{command.AppId}]");
                return false;
            }

            if (string.IsNullOrWhiteSpace(app.Uri))
            {
                Logger.Warn($"uri of [{command.AppId}] is empty.");
                return false;
            }
            var apiClient = new RestApiClient(app.Uri);
            var request = command.GetCommandRequestJson();

            var response = apiClient.Put($"/devices/{command.DeviceId}", request);
            if (!response.IsSuccess)
            {
                Logger.Warn($"Cannnot run command by [{command.AppId}/{command.DeviceId}/{command.Command}]");
                return false;
            }
            return true;
        }

        public AppLicenseInfo[] GetLicenses()
        {
            return _appLicenseInfos.ToArray();
        }

        public ApiResponse RequestRemoveApp(string requester, string id)
        {
            RemoveApp(id);
            return ApiResponse.Success;
        }

        public ApiResponse RequestRemoveAllApps(string requester)
        {
            RemoveAllApps();
            return ApiResponse.Success;
        }
        public ApiResponse RequestBlockApp(string requester, string id)
        {
            if (BlockApp(id))
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }

        private void RemoveApp(string id)
        {
            var app = FindAppNode(id);
            if (app != null)
            {
                var appLicense = FindAppLicenseInfo(app.AppType);
                bool res = appLicense.RegisteredAppInfos.Remove(app);
                if (!res)
                {
                    appLicense.BlockedAppInfos.Remove(app);
                }

                if (res)
                {
                    Logger.Info($"App[{app.AppType}:{app.AppKey}:{app.Name}]가 제거 되었습니다.");
                    SaveApps();
                }
            }
        }
        private void RemoveAllApps()
        {
            foreach (var appLicenseInfo in _appLicenseInfos)
            {
                appLicenseInfo.BlockedAppInfos.Clear();
                appLicenseInfo.RegisteredAppInfos.Clear();
            }
            Logger.Info($"Apps 등록정보가 초기화 되었습니다.");
            SaveApps();
        }
        private bool BlockApp(string id)
        {
            var app = FindAppNode(id);
            if (app != null)
            {
                if (!app.IsBlocked)
                {
                    var appLicense = FindAppLicenseInfo(app.AppType);
                    if (appLicense != null)
                    {
                        appLicense.RegisteredAppInfos.Remove(app);
                        app.Path = $"{app.Name}/blocked";
                        app.IsBlocked = true;
                        appLicense.BlockedAppInfos.Add(app);
                        SaveApps();
                        Logger.Info($"App[{app.AppType}:{app.AppKey}:{app.Name}]가 블럭 되었습니다.");
                    }
                }
                return true;
            }

            return false;
        }

        public NodeViewAppNode[] GetApps(bool includingBlocked = false)
        {
            List<NodeViewAppNode> appInfos = new List<NodeViewAppNode>();
            foreach (var appLicenseInfo in _appLicenseInfos)
            {
                appInfos.AddRange(appLicenseInfo.RegisteredAppInfos);
                if (includingBlocked)
                {
                    appInfos.AddRange(appLicenseInfo.BlockedAppInfos);
                }
            }

            ProcessNode[] processNodes = _processManagerClient.GetApps();
            ProcessNode centerNode = processNodes.FirstOrDefault(node => node.AppType.Equals("center", StringComparison.OrdinalIgnoreCase));
            if (centerNode != null)
                appInfos.Add(new NodeViewAppNode("Center", centerNode.Id, centerNode.AppType, centerNode.Name, ServiceIp, ServicePort, processManagerPort:ProcessManagerPort));
            return appInfos.ToArray();
        }

        public ApiResponse RequestKeepAlive(string requester, NodeViewAppNode requestApp)
        {
            return KeepAlive(requestApp);
        }

        public ApiResponse KeepAlive(NodeViewAppNode requestApp)
        {
            NodeViewAppNode appNode = FindAppNodeByAppKey(requestApp.AppKey);
            if (appNode != null)
            {
                if (appNode.IsBlocked)
                {
                    Logger.Warn($"블럭된 App[{appNode.AppType}:{appNode.AppKey}]이 접근했습니다.");
                    return new ApiResponse(HttpStatusCode.Forbidden);
                }
                appNode.Updated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
                return ApiResponse.Success;
            }

            return JoinNewApp(requestApp);
        }

        private ApiResponse JoinNewApp(NodeViewAppNode requestApp)
        {
            var appLicenseInfo = FindAppLicenseInfo(requestApp.AppType);
            if (appLicenseInfo == null)
            {
                return ApiResponse.BadRequest;
            }
            if (appLicenseInfo.LicenseCount <= appLicenseInfo.RegisteredCount)
            {
                Logger.Warn($"라이센스가 초과해서 App[{requestApp.AppType}:{requestApp.AppKey}:{requestApp.Name}]를 등록 할 수 없습니다.");

                return new ApiResponse(HttpStatusCode.Forbidden);
            }
            
            Logger.Info($"App[{requestApp.AppType}:{requestApp.AppKey}:{requestApp.Name}]가 신규 등록 되었습니다.");

            requestApp.Updated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            requestApp.Path = $"{appLicenseInfo.Name}/registered";
            appLicenseInfo.RegisteredAppInfos.Add(requestApp);
            SaveApps();
            return ApiResponse.Success;
        }

        public ApiResponse RequestUpdateAppInfo(string requester, string appKey, KeyValueCollection parameters)
        {
            if (UpdateUpdateInfo(appKey, parameters))
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }

        public bool UpdateUpdateInfo(string appKey, KeyValueCollection parameters)
        {
            if (string.IsNullOrWhiteSpace(appKey) || parameters.Count == 0)
            {
                return false;
            }
            var appInfo = FindAppNodeByAppKey(appKey);
            if (appInfo != null)
            {
                foreach (var parameter in parameters)
                {
                    string value = $"{parameter.Value}";
                    if (string.IsNullOrWhiteSpace(value))
                    {
                        continue;
                    }
                    switch (parameter.Key)
                    {
                        case "id":
                            appInfo.Id =$"{parameter.Value}";
                            break;
                        case "name":
                            appInfo.Name = $"{parameter.Value}";
                            break;
                        case "ip":
                            appInfo.Ip = $"{parameter.Value}";
                            break;
                        case "processmanagerport":
                            appInfo.ProcessManagerPort = StringUtils.GetIntValue(parameter.Value, appInfo.ProcessManagerPort);
                            break;
                        case "":
                            appInfo.Port = StringUtils.GetIntValue(parameter.Value, appInfo.Port);
                            break;
                    }
                }

                SaveApps();
                return true;
            }

            return false;
        }

        private AppLicenseInfo FindAppLicenseInfo(string appType)
        {
            foreach (var appLicenseInfo in _appLicenseInfos)
            {
                if (appLicenseInfo.Name == appType)
                {
                    return appLicenseInfo;
                }
            }

            return null;
        }

        private NodeViewAppNode FindAppNodeByAppKey(string appKey)
        {
            foreach (var appLicenseInfo in _appLicenseInfos)
            {
                foreach (var registeredAppInfo in appLicenseInfo.RegisteredAppInfos)
                {
                    if (registeredAppInfo.AppKey == appKey)
                    {
                        return registeredAppInfo;
                    }
                }
                foreach (var blockedAppInfo in appLicenseInfo.BlockedAppInfos)
                {
                    if (blockedAppInfo.AppKey == appKey)
                    {
                        return blockedAppInfo;
                    }
                }
            }

            return null;
        }

        private NodeViewAppNode FindAppNode(string appId)
        {
            foreach (var appLicenseInfo in _appLicenseInfos)
            {
                foreach (var registeredAppInfo in appLicenseInfo.RegisteredAppInfos)
                {
                    if (registeredAppInfo.Id == appId)
                    {
                        return registeredAppInfo;
                    }
                }
                foreach (var blockedAppInfo in appLicenseInfo.BlockedAppInfos)
                {
                    if (blockedAppInfo.Id == appId)
                    {
                        return blockedAppInfo;
                    }
                }
            }

            return null;
        }

        private NodeViewAppNode FindAppNode(string appId, string appName)
        {
            var appLicenseInfo = FindAppLicenseInfo(appName);
            if (appLicenseInfo != null)
            {
                foreach (var registeredAppInfo in appLicenseInfo.RegisteredAppInfos)
                {
                    if (registeredAppInfo.Id == appId)
                    {
                        return registeredAppInfo;
                    }
                }
                foreach (var blockedAppInfo in appLicenseInfo.BlockedAppInfos)
                {
                    if (blockedAppInfo.Id == appId)
                    {
                        return blockedAppInfo;
                    }
                }
            }
            return null;
        }

        private void LoadApps()
        {
            string cachedAppsFilename = Path.Combine(CachesFolder, "apps.json");
            if (File.Exists(cachedAppsFilename))
            {
                JArray appArray = JArray.Parse(File.ReadAllText(cachedAppsFilename, Encoding.UTF8));
                foreach (var apptoken in appArray)
                {
                    JObject json = apptoken.Value<JObject>();
                    var app = NodeViewAppNode.CreateFrom(json);
                    if (app != null)
                    {
                        string appType = app.AppType;
                        bool isBlocked = app.IsBlocked;
                        var appLicense = FindAppLicenseInfo(appType);
                        if (appLicense != null)
                        {
                            if (isBlocked)
                            {
                                app.Path = $"{app.AppType}/blocked";
                                appLicense.BlockedAppInfos.Add(app);
                            }
                            else
                            {
                                app.Path = $"{app.AppType}/registered";
                                appLicense.RegisteredAppInfos.Add(app);
                            }
                        }
                    }
                }
            }
        }

        private void SaveApps()
        {
            var apps = GetApps(true);
            JArray appArray = new JArray();
            foreach (var app in apps)
            {
                appArray.Add(app.ToJson());
            }
            string cachedAppsFilename = Path.Combine(CachesFolder, "apps.json");
            File.WriteAllText(cachedAppsFilename, appArray.ToString(), Encoding.UTF8);
        }

        protected override void WriteEventLog(NodeViewEvent nodeViewEvent, EventAction eventAction = null)
        {
            if (nodeViewEvent == null) return;

            string eventOnOffString = nodeViewEvent.IsOn ? "ON" : "OFF";
            if (eventAction != null)
            {
                AlarmEventLevel eventLevel = nodeViewEvent.IsOn ? eventAction.Level: AlarmEventLevel.Normal;
                EventLog eventLog = new EventLog(eventLevel, "NodeViewCenter", $"{eventAction.Description} [{eventOnOffString}]");
                AppendEventLog(eventLog);
            }
            else if (!string.IsNullOrWhiteSpace(nodeViewEvent.Message))
            {
                AlarmEventLevel eventLevel = AlarmEventLevel.Normal;
                EventLog eventLog = new EventLog(eventLevel, "NodeViewCenter", $"{nodeViewEvent.Message} [{eventOnOffString}]");
                AppendEventLog(eventLog);
            }
        }
    }
}
