using System;
using System.Collections.Generic;
using NLog;
using NodeView.DataModels;
using NodeView.Extension.Plugins;
using NodeView.Frameworks.Stations;

namespace NodeView.CameraController
{
    public class TruenCameraController : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private TruenCameraControlClient _truenCameraControlClient;
        public TruenCameraController()
        {
            DeviceType = "TruenCameraController";
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("applyPreset", "프리셋 적용", new[]
            {
                new FieldDescription("ip", "", "카메라 IP", "string"),
                new FieldDescription("channel", "", "카메라 채널", "string"),
                new FieldDescription("id", "", "프리셋 ID", "string"),
            }));

            return commandDescriptions.ToArray();
        }
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "applyPreset", StringComparison.Ordinal) == 0)
                {
                    string ip = parameters.GetValue("ip", "127.0.0.1");
                    string channel = parameters.GetValue("channel", "1");
                    string name = parameters.GetValue("id", "");
                    if (string.IsNullOrWhiteSpace(ip) || string.IsNullOrWhiteSpace(channel) || string.IsNullOrWhiteSpace(name))
                    {
                        Logger.Warn($"RunCommand failed ip : {ip} chaanel : {channel} id : {name}");
                        return false;
                    }

                    _truenCameraControlClient = new TruenCameraControlClient(ip, "admin", "semsol1!", "1", false, false);
                    _truenCameraControlClient.ApplyPreset(name);
                    isHandled = true;

                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }

            return isHandled;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
    }
}
