﻿using LibVLCSharp.Shared;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using LogLevel = LibVLCSharp.Shared.LogLevel;

namespace NodeViewScreenFramework
{
    public class VlcLlibraryManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private LibVLC _libVlc = null;
        //private string[] _vlcOptions = Array.Empty<string>();
        List<string> _vlcOptions = new List<string>()
            { "--clock-synchro=0", "--clock-jitter=0", "--network-caching=150"};
        #region SingleTone

        private static readonly Lazy<VlcLlibraryManager> Lazy = new(() => new VlcLlibraryManager());
        public static VlcLlibraryManager Instance => Lazy.Value;

        #endregion

        private List<string> _exceptMessagesList = new List<string>() { "Timestamp", "initialize", "SetThumbNailClip", "timestamp", "deadlock" };

        public void Init(string[] vlcOptions)
        {
            _vlcOptions.AddRange(vlcOptions);
        }
        //public LibVLC GetLibVlc()
        //{
        //    if (_libVlc == null)
        //    {
        //        //string[] options = new[] { "--network-caching=1000",
        //        //    "--directx-use-sysmem",
        //        //    "--avcodec-hw=none"}
        //        //string[] options = new[] { "--clock-synchro=1", "--clock-jitter=5000", "--directx-use-sysmem" };
        //        //string[] options = { "--rtsp-tcp" };
        //        foreach (var vlcOption in _vlcOptions)
        //        {
        //            Logger.Info($"{vlcOption}");
        //        }
        //        if (_vlcOptions.Length > 0)
        //        {
        //            _libVlc = new LibVLC(true, _vlcOptions);
        //            foreach (var vlcOption in _vlcOptions)
        //            {
        //                Logger.Info($"{vlcOption}");
        //            }
        //        }
        //        else
        //        {
        //            _libVlc = new LibVLC(true);
        //        }
        //        _libVlc.Log += LibVlc_Log;
        //    }

        //    return _libVlc;
        //}

        public LibVLC GetLibVlc()
        {
            LibVLC libvlc = null;
            //foreach (var vlcOption in _vlcOptions)
            //{
            //    Logger.Info($"{vlcOption}");
            //}
            if (_vlcOptions.Count > 0)
            {
                libvlc = new LibVLC(true, _vlcOptions.ToArray());
                foreach (var vlcOption in _vlcOptions)
                {
                    Logger.Info($"{vlcOption}");
                }
            }
            else
            {
                libvlc = new LibVLC(true);
            }

            return libvlc;
        }

        private void LibVlc_Log(object? sender, LogEventArgs e)
        {
            if (e.Level == LogLevel.Error)
            {
                //Logger.Error($"Level : {e.Level} Message : {e.Message}");
                if (!_exceptMessagesList.Any(message => e.Message.IndexOf(message) >= 0))
                {
                    Logger.Error($"Level : {e.Level} Message : {e.Message} Module : {e.Module} SourceFile : {e.SourceFile} SourceLine : {e.SourceLine} ");
                }
            }
            else if (e.Level == LogLevel.Debug)
            {
                //Logger.Debug($"Level : {e.Level} Message : {e.Message} Module : {e.Module} SourceFile : {e.SourceFile} SourceLine : {e.SourceLine} FormateedLog : {e.FormattedLog}");
            }
            else if (e.Level == LogLevel.Warning)
            {
                //Logger.Warn($"Level : {e.Level} Message : {e.Message} Module : {e.Module} SourceFile : {e.SourceFile} SourceLine : {e.SourceLine} ");
            }
        }

        public void Dispose()
        {
            _libVlc.Log -= LibVlc_Log;
            _libVlc?.Dispose();
            _libVlc = null;
        }
    }
}
