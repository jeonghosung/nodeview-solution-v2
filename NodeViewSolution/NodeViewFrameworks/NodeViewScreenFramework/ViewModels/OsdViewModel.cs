﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NodeView.Models;
using Prism.Mvvm;

namespace NodeViewScreenFramework.ViewModels
{
    public class OsdViewModel : BindableBase
    {
        public Visibility Visibility { get; set; } = Visibility.Collapsed;
        public double Opacity { get; set; } = 1;
        public double FontSize { get; set; } = 11;
        public Brush Foreground { get; set; } = new SolidBrush(Color.White);
        public VerticalAlignment VerticalAlignment { get; set; } = VerticalAlignment.Top;
        public HorizontalAlignment HorizontalAlignment { get; set; } = HorizontalAlignment.Left;
        public Margins Margins { get; set; } = new Margins(0, 0, 0, 0);
        public string Text { get; set; } = "";

        public OsdViewModel()
        {

        }

        public OsdViewModel(OsdConfig config)
        {
            Set(config);
        }

        public void Set(OsdConfig config)
        {
            if (config == null) return;

            //Todo:추후 OsdConfig 포함 해서 수정 해야 한다.
            Visibility = config.IsShow ? Visibility.Visible : Visibility.Collapsed;
            FontSize = config.FontSize;
            Foreground = new SolidBrush(Color.FromArgb(config.Color));
        }
    }
}
