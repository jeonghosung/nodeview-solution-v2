﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;

namespace NodeViewScreenFramework.DataWallContentControls
{
    /// <summary>
    /// ImageContentControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ImageContentControl : UserControl, IDataWallContentControl
    {
        public ImageContentControl()
        {
            InitializeComponent();
        }

        public IDataWallContent Source { get; set; }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            IDataWallContent content = Source;
            if (string.IsNullOrWhiteSpace(content?.Uri))
            {
                return;
            }

            Image.Height = ActualHeight;
            Image.Width = ActualWidth;
            BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new Uri(content.Uri);
            bitmap.EndInit();
            Image.Source = bitmap;
        }
    }
}
