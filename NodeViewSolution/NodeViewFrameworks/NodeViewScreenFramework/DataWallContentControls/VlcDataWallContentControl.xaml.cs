﻿using LibVLCSharp.Shared;
using NLog;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;
using NodeView.Models;
using NodeView.Tasks;
using NodeViewScreenFramework;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Encodings.Web;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using NodeView.DataModels;
using LogLevel = LibVLCSharp.Shared.LogLevel;
using MediaPlayer = LibVLCSharp.Shared.MediaPlayer;
using NodeView.Utils;
using System.Web;
using System.Windows.Media.Effects;

namespace NodeView.DataWallContentControls
{
    /// <summary>
    /// LibVlcDataWallContentControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class VlcDataWallContentControl : UserControl, IDataWallContentControl
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Configuration _configuration;

        private Timer _checkingTimer = null;
        public bool IsTestMode { get; protected set; } = false;
        public bool IsVerbose { get; protected set; } = false;

        private LibVLC _libVlc;
        //private readonly OsdConfig _osdConfig = new();
        private bool _isPlaying = false;

        private int _dueTime = 1;
        private int _lastCount = 1;
        private Timer _tryReconnectTimer = null!;
        private Timer _resetTimer = null!;
        private readonly Stopwatch _resetTimeCheckStopwatch = new();
        private MediaPlayer _mediaPlayer;

        private bool _isDisposed = false;
        private bool _isOsdShow = false;
        //private VideoView _videoView;
        private string _url = "";

        private readonly List<string> _exceptMessagesList = new() { "Timestamp", "initialize", "SetThumbNailClip", "timestamp", "deadlock" };
        private readonly string _reStartErrorMessage = "more than 5 seconds of late video";
        public VlcDataWallContentControl(IContainerProvider containerProvider)
        {
            _configuration = containerProvider.Resolve<Configuration>("appConfig");
            InitializeComponent();
            Init();
        }

        public IDataWallContent Source
        {
            get => (IDataWallContent)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }
        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IDataWallContent), typeof(VlcDataWallContentControl), new PropertyMetadata(OnSourcePropertyChanged));
        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is VlcDataWallContentControl owner)
            {
                if (e.NewValue is IDataWallContent dataWallContent)
                {
                    //Todo 이건 좀 고민해보자
                    //owner.UpdateContent(nodeViewContent);
                    /*
                    owner._osdConfig.IsShow = dataWallContent.GetValue("osdIsShow", owner._osdConfig.IsShow);
                    owner._osdConfig.Alignment = dataWallContent.GetValue("osdAlignment", owner._osdConfig.Alignment);
                    owner._osdConfig.Offset = dataWallContent.GetValue("osdOffset", owner._osdConfig.Offset);
                    owner._osdConfig.Opacity = dataWallContent.GetValue("osdOpacity", owner._osdConfig.Opacity);
                    owner._osdConfig.FontSize = dataWallContent.GetValue("osdFontSize", owner._osdConfig.FontSize);
                    owner._osdConfig.Color = dataWallContent.GetValue("osdColor", owner._osdConfig.Color);
                    */
                    OsdConfig osdConfig = new();
                    osdConfig.IsShow = dataWallContent.GetValue("osdIsShow", osdConfig.IsShow);
                    osdConfig.Alignment = dataWallContent.GetValue("osdAlignment", osdConfig.Alignment);
                    osdConfig.Offset = dataWallContent.GetValue("osdOffset", osdConfig.Offset);
                    osdConfig.Opacity = dataWallContent.GetValue("osdOpacity", osdConfig.Opacity);
                    osdConfig.UseDropShadowEffect = dataWallContent.GetValue("osdUseDropShadowEffect", osdConfig.UseDropShadowEffect);
                    osdConfig.FontSize = dataWallContent.GetValue("osdFontSize", osdConfig.FontSize);
                    osdConfig.Color = dataWallContent.GetValue("osdColor", osdConfig.Color);
                    owner.SetOsd(osdConfig);
                }
            }
        }

        private void SetOsd(OsdConfig config)
        {
            if (config == null || Source == null) return;
            var alignment = config.Alignment;
            var offset = config.Offset;
            var horizontalAlignment = HorizontalAlignment.Left;
            var verticalAlignment = VerticalAlignment.Top;
            var margin = new Thickness();
            double opacity = 1;
            switch (alignment)
            {
                case OsdAlignmentType.LeftTop:
                    horizontalAlignment = HorizontalAlignment.Left;
                    verticalAlignment = VerticalAlignment.Top;
                    margin.Left = offset.X;
                    margin.Top = offset.Y;
                    break;
                case OsdAlignmentType.RightTop:
                    horizontalAlignment = HorizontalAlignment.Right;
                    verticalAlignment = VerticalAlignment.Top;
                    margin.Right = offset.X;
                    margin.Top = offset.Y;
                    break;
                case OsdAlignmentType.RightBottom:
                    horizontalAlignment = HorizontalAlignment.Right;
                    verticalAlignment = VerticalAlignment.Bottom;
                    margin.Right = offset.X;
                    margin.Bottom = offset.Y;
                    break;
                case OsdAlignmentType.LeftBottom:
                    horizontalAlignment = HorizontalAlignment.Left;
                    verticalAlignment = VerticalAlignment.Bottom;
                    margin.Left = offset.X;
                    margin.Bottom = offset.Y;
                    break;
                case OsdAlignmentType.Center:
                    horizontalAlignment = HorizontalAlignment.Center;
                    verticalAlignment = VerticalAlignment.Center;
                    margin.Left = offset.X;
                    margin.Top = offset.Y;
                    break;
                default:
                    horizontalAlignment = HorizontalAlignment.Left;
                    verticalAlignment = VerticalAlignment.Top;
                    margin.Left = offset.X;
                    margin.Top = offset.Y;
                    break;
            }

            opacity = config.Opacity / 100;
            _osdLabel.HorizontalAlignment = horizontalAlignment;
            _osdLabel.VerticalAlignment = verticalAlignment;
            _osdLabel.Margin = margin;
            _osdLabel.Opacity = opacity;
            //Todo: 일단 기본만 하자.
            _isOsdShow = config.IsShow;
            _osdLabel.Content = Source.Name;
            _osdLabel.Visibility = config.IsShow ? Visibility.Visible : Visibility.Collapsed;
            _osdLabel.FontSize = config.FontSize;
            _osdLabel.Foreground = new SolidColorBrush(Color.FromRgb((byte)(config.Color >> 16 & 0xff), (byte)(config.Color >> 8 & 0xff), (byte)(config.Color & 0xff)));
            if (config.UseDropShadowEffect)
            {
                DropShadowEffect dropShadowEffect = new DropShadowEffect()
                {
                    ShadowDepth = 0.5,
                    Direction = 320,
                    Color = Colors.Black,
                    Opacity = 1,
                    BlurRadius = 7,
                };
                _osdLabel.Effect = dropShadowEffect;
            }
        }

        private void Init()
        {
            _libVlc = VlcLlibraryManager.Instance.GetLibVlc();
            _libVlc.Log += LibVlc_Log;
            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
            _videoView.Loaded += VideoView_Loaded;


            _tryReconnectTimer = new Timer(OnTryReconnectTimer);
            IsTestMode = _configuration.GetValue("isTestMode", IsTestMode);
            IsVerbose = IsTestMode || _configuration.GetValue("isVerbose", IsVerbose);
            //_resetTimeCheckStopwatch.Start();
            //_resetTimer = new Timer(ResetControl);
            //_resetTimer.Change(TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(10));
        }
        private void ResetControl(object? state)
        {
            _resetTimer.Change(Timeout.Infinite, Timeout.Infinite);
            if (_resetTimeCheckStopwatch.ElapsedMilliseconds > 1000 * 60 * 60)
            {
                Logger.Info($"Reset! Time : {_resetTimeCheckStopwatch.ElapsedMilliseconds}");
                _resetTimeCheckStopwatch.Restart();
                ThreadAction.PostOnUiThread(PlayContent);
            }
            _resetTimer.Change(TimeSpan.FromMinutes(10), TimeSpan.FromMinutes(10));
        }

        private void OnTryReconnectTimer(object? state)
        {
            _tryReconnectTimer.Change(Timeout.Infinite, Timeout.Infinite);
            if (_isPlaying)
            {
                return;
            }
            if (_dueTime * 2 >= 1800)
            {
                _dueTime = 1800;
            }
            else
            {
                _dueTime += _lastCount;
                _lastCount = _dueTime - _lastCount;
            }
            //Logger.Info($"LastCount : {_lastCount} DueTime : {_dueTime}");
            ThreadAction.PostOnUiThread(PlayContent);
            _tryReconnectTimer.Change(TimeSpan.FromSeconds(_dueTime), TimeSpan.FromSeconds(_dueTime));
        }
        private void VideoView_Loaded(object sender, RoutedEventArgs e)
        {
            SetAndPlayContent();
        }
        private void LibVlc_Log(object? sender, LogEventArgs e)
        {
            if (e.Level == LogLevel.Error)
            {
                //Logger.Error($"Level : {e.Level} Message : {e.Message}");
                if (!_exceptMessagesList.Any(message => e.Message.Contains(message)))
                {
                    Logger.Error($"Level : {e.Level} Message : {e.Message} Module : {e.Module} SourceFile : {e.SourceFile} SourceLine : {e.SourceLine} Url : {_url}");
                }

                if (e.Message.Contains(_reStartErrorMessage))
                {
                    Logger.Info("Restart!");
                    ThreadAction.PostOnUiThread(PlayContent);
                    _resetTimeCheckStopwatch.Restart();
                }
            }
            else if (e.Level == LogLevel.Debug)
            {
                //Logger.Debug($"Level : {e.Level} Message : {e.Message} Module : {e.Module} SourceFile : {e.SourceFile} SourceLine : {e.SourceLine} FormateedLog : {e.FormattedLog}");
            }
            else if (e.Level == LogLevel.Warning)
            {
                if (IsVerbose)
                {
                    Logger.Warn($"Level : {e.Level} Message : {e.Message} Module : {e.Module} SourceFile : {e.SourceFile} SourceLine : {e.SourceLine} ");
                }

            }
        }
        private void SetAndPlayContent()
        {
            _mediaPlayer = new MediaPlayer(_libVlc)
            { EnableHardwareDecoding = true, Mute = true };
            _mediaPlayer.ToggleTeletext();
            _mediaPlayer.Playing += MediaPlayer_Playing;
            _mediaPlayer.EndReached += MediaPlayer_EndReached;
            _mediaPlayer.EncounteredError += MediaPlayer_EncounteredError;
            _mediaPlayer.EnableMouseInput = false;
            _mediaPlayer.EnableKeyInput = false;
            _videoView.MediaPlayer = _mediaPlayer;
            PlayContent();
        }
        private void MediaPlayer_Playing(object? sender, EventArgs e)
        {
            _isPlaying = true;
            _dueTime = 1;
            _lastCount = 1;
        }
        private void OnLoaded(object sender, RoutedEventArgs e)
        {
        }
        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                Loaded -= OnLoaded;
                Unloaded -= OnUnloaded;
                _videoView.Loaded -= VideoView_Loaded;
                _mediaPlayer.Playing -= MediaPlayer_Playing;
                _mediaPlayer.EndReached -= MediaPlayer_EndReached;
                _mediaPlayer.EncounteredError -= MediaPlayer_EncounteredError;

                Dispose();
                //Logger.Info("Vlc contol unloaded.");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "OnUnloaded");
            }
        }
        private void Dispose()
        {
            _isDisposed = true;
            _mediaPlayer?.Stop();
            _mediaPlayer?.Dispose();
            _mediaPlayer = null;
            _videoView.MediaPlayer = null;
            _videoView?.Dispose();
            _videoView = null;

            _libVlc.Log -= LibVlc_Log;
            _libVlc?.Dispose();
            _libVlc = null;
            _tryReconnectTimer.Dispose();
            _resetTimer.Dispose();
            //_checkingTimer.Change(Timeout.Infinite, Timeout.Infinite);


        }
        private void PlayContent()
        {
            if (_isDisposed)
            {
                return;
            }
            IDataWallContent content = Source;
            if (string.IsNullOrWhiteSpace(content?.Uri))
            {
                return;
            }
            string userId = content.UserId;
            string userPassword = content.UserPassword;
            var encodingId = HttpUtility.UrlEncode(userId);;
            var encodingPassword = HttpUtility.UrlEncode(userPassword);
            if (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(userPassword))
            {
                _url = content.Uri.Replace("://", $"://{encodingId}:{encodingPassword}@");
            }
            else
            {
                _url = content.Uri;
            }

            _mediaPlayer.AspectRatio = $"{ActualWidth}:{ActualHeight}";
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Enable, 0);
            /*
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Enable, 1);
            _mediaPlayer.SetMarqueeString(VideoMarqueeOption.Text, content.Name);
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Position, (int)_osdConfig.Alignment);
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.X, _osdConfig.Offset.X);
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Y, _osdConfig.Offset.Y);
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Opacity, _osdConfig.Opacity);
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Size, _osdConfig.FontSize);
            _mediaPlayer.SetMarqueeInt(VideoMarqueeOption.Color, _osdConfig.Color);
            */
            //var currentDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            //var destination = Path.Combine(currentDirectory, "record.ts");

            //if (_mediaPlayer.IsPlaying)
            //{
            //    _mediaPlayer.Stop();
            //}
            using var media = new Media(_libVlc, new Uri(_url));
            //media.AddOption(":sout=#transcode{vcodec=h264,vb=4096,fps=30,scale=0,width=1920,height=1080,acodec=none,scodec=none}:file{dst=record1.mp4}");
            //media.AddOption(":sout-keep");
            _mediaPlayer.Play(media);
        }
        private void MediaPlayer_EndReached(object? sender, EventArgs e)
        {
            _isPlaying = false;
            Logger.Info($"EndReached : {_url}");
            _tryReconnectTimer.Change(TimeSpan.FromSeconds(_dueTime), TimeSpan.FromSeconds(_dueTime));
        }
        private void MediaPlayer_EncounteredError(object? sender, EventArgs e)
        {
            _isPlaying = false;
            Logger.Info($"EncounteredError : {_url}");
            _tryReconnectTimer.Change(TimeSpan.FromSeconds(_dueTime), TimeSpan.FromSeconds(_dueTime));
        }
        private void UserControl_IsVisibleChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if(e.NewValue is bool isVisible)
            {
                if (_isOsdShow)
                {
                    _osdLabel.Visibility = isVisible ? Visibility.Visible : Visibility.Collapsed;
                }
            }
        }
    }
}
