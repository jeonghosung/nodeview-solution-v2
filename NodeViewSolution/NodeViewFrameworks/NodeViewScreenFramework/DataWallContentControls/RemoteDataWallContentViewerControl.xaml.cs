﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GlavSoft.RemoteCoreSDK.Viewer;
using GlavSoft.RemoteCoreSDK.Viewer.Data;
using GlavSoft.RemoteCoreSDK.Viewer.WPF;
using NLog;
using NodeView.DataWallContentControls;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;
using NodeView.Models;
using NodeView.Tasks;
namespace NodeViewScreenFramework.DataWallContentControls
{
    /// <summary>
    /// RemoteDataWallContentControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RemoteDataWallContentViewerControl : UserControl, IDataWallContentControl
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private RemoteDesktopControl _remoteDesktopControl = null;
        private bool _isOsdShow = false;
        public RemoteDataWallContentViewerControl()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
            _remoteDesktopControl = new() { IsAutoScale = true };
            mainGrid.Children.Add(_remoteDesktopControl);
            _remoteDesktopControl.AuthenticationError += OnAuthError;
            _remoteDesktopControl.Error += OnError;
            _remoteDesktopControl.Connect += OnConnected;
        }

        private void OnConnected(object? sender, EventArgs e)
        {
            //Logger.Info($"OnConnected.");
        }

        private void OnError(object? sender, ErrorEventArgs e)
        {
            Logger.Error($"OnError. : {e.Message}");
        }

        private void OnAuthError(object? sender, ErrorEventArgs e)
        {
            Logger.Error($"OnAuthError. : {e.Message}");
        }

        public IDataWallContent Source
        {
            get => (IDataWallContent)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IDataWallContent), typeof(RemoteDataWallContentViewerControl), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is RemoteDataWallContentViewerControl owner)
            {
                if (e.NewValue is IDataWallContent dataWallContent)
                {
                    //Todo 이건 좀 고민해보자
                    //owner.UpdateContent(nodeViewContent);
                    /*
                    owner._osdConfig.IsShow = dataWallContent.GetValue("osdIsShow", owner._osdConfig.IsShow);
                    owner._osdConfig.Alignment = dataWallContent.GetValue("osdAlignment", owner._osdConfig.Alignment);
                    owner._osdConfig.Offset = dataWallContent.GetValue("osdOffset", owner._osdConfig.Offset);
                    owner._osdConfig.Opacity = dataWallContent.GetValue("osdOpacity", owner._osdConfig.Opacity);
                    owner._osdConfig.FontSize = dataWallContent.GetValue("osdFontSize", owner._osdConfig.FontSize);
                    owner._osdConfig.Color = dataWallContent.GetValue("osdColor", owner._osdConfig.Color);
                    */
                    OsdConfig osdConfig = new();
                    osdConfig.IsShow = dataWallContent.GetValue("osdIsShow", osdConfig.IsShow);
                    osdConfig.Alignment = dataWallContent.GetValue("osdAlignment", osdConfig.Alignment);
                    osdConfig.Offset = dataWallContent.GetValue("osdOffset", osdConfig.Offset);
                    osdConfig.Opacity = dataWallContent.GetValue("osdOpacity", osdConfig.Opacity);
                    osdConfig.UseDropShadowEffect = dataWallContent.GetValue("osdUseDropShadowEffect", osdConfig.UseDropShadowEffect);
                    osdConfig.FontSize = dataWallContent.GetValue("osdFontSize", osdConfig.FontSize);
                    osdConfig.Color = dataWallContent.GetValue("osdColor", osdConfig.Color);
                    owner.SetOsd(osdConfig);
                }
            }
        }
        private void SetOsd(OsdConfig config)
        {
            if (config == null || Source == null) return;
            var alignment = config.Alignment;
            var offset = config.Offset;
            var horizontalAlignment = HorizontalAlignment.Left;
            var verticalAlignment = VerticalAlignment.Top;
            var margin = new Thickness();
            double opacity = 1;
            switch (alignment)
            {
                case OsdAlignmentType.LeftTop:
                    horizontalAlignment = HorizontalAlignment.Left;
                    verticalAlignment = VerticalAlignment.Top;
                    margin.Left = offset.X;
                    margin.Top = offset.Y;
                    break;
                case OsdAlignmentType.RightTop:
                    horizontalAlignment = HorizontalAlignment.Right;
                    verticalAlignment = VerticalAlignment.Top;
                    margin.Right = offset.X;
                    margin.Top = offset.Y;
                    break;
                case OsdAlignmentType.RightBottom:
                    horizontalAlignment = HorizontalAlignment.Right;
                    verticalAlignment = VerticalAlignment.Bottom;
                    margin.Right = offset.X;
                    margin.Bottom = offset.Y;
                    break;
                case OsdAlignmentType.LeftBottom:
                    horizontalAlignment = HorizontalAlignment.Left;
                    verticalAlignment = VerticalAlignment.Bottom;
                    margin.Left = offset.X;
                    margin.Bottom = offset.Y;
                    break;
                case OsdAlignmentType.Center:
                    horizontalAlignment = HorizontalAlignment.Center;
                    verticalAlignment = VerticalAlignment.Center;
                    margin.Left = offset.X;
                    margin.Top = offset.Y;
                    break;
                default:
                    horizontalAlignment = HorizontalAlignment.Left;
                    verticalAlignment = VerticalAlignment.Top;
                    margin.Left = offset.X;
                    margin.Top = offset.Y;
                    break;
            }

            opacity = config.Opacity / 100;
            _osdLabel.HorizontalAlignment = horizontalAlignment;
            _osdLabel.VerticalAlignment = verticalAlignment;
            _osdLabel.Margin = margin;
            _osdLabel.Opacity = opacity;
            //Todo: 일단 기본만 하자.
            _isOsdShow = config.IsShow;
            _osdLabel.Content = Source.Name;
            _osdLabel.Visibility = config.IsShow ? Visibility.Visible : Visibility.Collapsed;
            _osdLabel.FontSize = config.FontSize;
            _osdLabel.Foreground = new SolidColorBrush(Color.FromRgb((byte)(config.Color >> 16 & 0xff), (byte)(config.Color >> 8 & 0xff), (byte)(config.Color & 0xff)));

            if (config.UseDropShadowEffect)
            {
                DropShadowEffect dropShadowEffect = new DropShadowEffect()
                {
                    ShadowDepth = 0.5,
                    Direction = 320,
                    Color = Colors.Black,
                    Opacity = 1,
                    BlurRadius = 7,
                };
                _osdLabel.Effect = dropShadowEffect;
            }
        }
        private void UpdateContent(IDataWallContent content)
        {
            if (content == null)
            {
                return;
            }
        }
        private void PlayContent()
        {
            IDataWallContent content = Source;
            if (string.IsNullOrWhiteSpace(content?.Uri))
            {
                return;
            }

            bool isPreview = content.GetValue("isPreview", false);
            string uri = content.Uri;
            string userId = content.UserId;
            string userPassword = content.UserPassword;
            if (!isPreview)
            {
                _remoteDesktopControl.IsHitTestVisible = false;
            }
            else
            {
                content.RemoveProperty("isPreview");
            }

            _remoteDesktopControl.Scale = 1;
            _remoteDesktopControl.AutoReconnect = AutoReconnectMode.Always;
            _remoteDesktopControl.ConnectionMode = ConnectionMode.NewConnection;
            _remoteDesktopControl.ForceFullUpdateRequests = false;
            _remoteDesktopControl.Port = 5900;
            _remoteDesktopControl.ConnectedViewMode = ConnectedViewMode.RemoteDesktop;
            _remoteDesktopControl.PreferredEncoding = ViewerEncodings.Tight;
            _remoteDesktopControl.AllowJPEG = true;
            _remoteDesktopControl.CustomCompressionLevel = CompressionLevel.CompressionLevel9;
            _remoteDesktopControl.JpegQualityLevel = JpegQualityLevel.JpegLevel2;
            _remoteDesktopControl.UpdateRequestsDelay = 70;
            _remoteDesktopControl.RemoteHostAddress = uri;
            _remoteDesktopControl.Password = userPassword;
            _remoteDesktopControl.StartCommand.Execute(null);
        }

        public void DisposeContent()
        {
            try
            {
                mainGrid.Children.Clear();
                _remoteDesktopControl.StopCommand.Execute(null);
                _remoteDesktopControl.AuthenticationError -= OnAuthError;
                _remoteDesktopControl.Error -= OnError;
                _remoteDesktopControl.Connect -= OnConnected;
                _remoteDesktopControl.Dispose();
                _remoteDesktopControl = null;
            }
            catch (Exception e)
            {
                Logger.Error(e, "DisposeContent:");
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            //ThreadAction.PostOnUiThread(PlayContent);
            PlayContent();
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DisposeContent();
                //ThreadAction.PostOnUiThread(DisposeContent);

                Loaded -= OnLoaded;
                Unloaded -= OnUnloaded;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "UserControl_Unloaded:");
            }
        }
    }
}
