﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml.Linq;
using CefSharp;
using CefSharp.Wpf;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.DataWallContentControls;
using NodeView.Tasks;

namespace NodeView.DataWallContentControls
{
    /// <summary>
    /// WebViewControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class WebDataWallContentControl : UserControl, IDataWallContentControl
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _startScript;
        public WebDataWallContentControl()
        {
            InitializeComponent();
        }
        public IDataWallContent Source
        {
            get => (IDataWallContent)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IDataWallContent), typeof(WebDataWallContentControl), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is WebDataWallContentControl owner)
            {
                if (e.NewValue is IDataWallContent dataWallContent)
                {
                    owner.UpdateContent(dataWallContent);
                }
            }
        }
        private void UserControl_Loaded(object sender, System.Windows.RoutedEventArgs args)
        {
            WebBrowserContent.FrameLoadEnd += OnBrowserFrameLoadEnd;
        }

        private void UserControl_Unloaded(object sender, System.Windows.RoutedEventArgs args)
        {
            try
            {
                DisposeContent();
            }
            catch (Exception e)
            {
                Logger.Error(e, "UserControl_Unloaded:");
            }
        }
        private void UpdateContent(IDataWallContent content)
        {
            if (content == null || WebBrowserContent == null || WebBrowserContent.IsDisposed)
            {
                return;
            }

            string background = content.Properties.GetStringValue("background");

            if (!string.IsNullOrWhiteSpace(background))
            {
                BrushConverter bc = new BrushConverter();
                try
                {
                    Brush brush = (Brush)bc.ConvertFrom(background);
                    if (brush != null)
                    {
                        brush.Freeze();
                        Background = brush;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "Cannot parse 'background'.");
                }
            }

            if (content.Properties.Contains("contentRect"))
            {
                DoubleRect contentRect = content.Properties.GetValue<DoubleRect>("contentRect", null);
                if (contentRect != null && !contentRect.IsEmpty)
                {
                    WebBrowserContent.Width = contentRect.Width;
                    WebBrowserContent.Height = contentRect.Height;
                }
            }

            string startupScript = content.Properties.GetStringValue("startupScript");
            if (!string.IsNullOrWhiteSpace(startupScript))
            {
                _startScript = startupScript;
            }
            WebBrowserContent.Address = content.Uri;
        }

        public void DisposeContent()
        {
            try
            {
                if (!WebBrowserContent.IsDisposed)
                {
                    WebBrowserContent.FrameLoadEnd -= OnBrowserFrameLoadEnd;
                    WebBrowserContent.Dispose();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "DisposeContent:");
            }
        }

        private void OnBrowserFrameLoadEnd(object sender, FrameLoadEndEventArgs args)
        {
            try
            {
                if (args.Frame.IsMain)
                {
                    var mainFrame = args.Browser.MainFrame;

                    mainFrame.ExecuteJavaScriptAsync("document.documentElement.style.overflow ='hidden'");
                    //string startUpScript = Source.Properties.GetStringValue("startupScript");
                    if (!string.IsNullOrWhiteSpace(_startScript))
                    {
                        mainFrame.ExecuteJavaScriptAsync(_startScript);
                    }

                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "OnBrowserFrameLoadEnd:");
            }
        }
    }
}
