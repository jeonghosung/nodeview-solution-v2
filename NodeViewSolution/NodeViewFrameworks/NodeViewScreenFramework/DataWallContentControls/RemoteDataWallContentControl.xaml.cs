﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GlavSoft.RemoteCoreSDK.Viewer;
using GlavSoft.RemoteCoreSDK.Viewer.Data;
using GlavSoft.RemoteCoreSDK.Viewer.WPF;
using NLog;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;
using NodeView.Tasks;
namespace NodeViewScreenFramework.DataWallContentControls
{
    /// <summary>
    /// RemoteDataWallContentControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RemoteDataWallContentControl : UserControl, IDataWallContentControl
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        //private RemoteDesktopControl _remoteDesktopControl = null;
        public RemoteDataWallContentControl()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            Loaded += OnLoaded;
            Unloaded += OnUnloaded;
            _remoteDesktopControl = new(){IsAutoScale = true};
            mainGrid.Children.Add(_remoteDesktopControl);
            _remoteDesktopControl.AuthenticationError += OnAuthError;
            _remoteDesktopControl.Error += OnError;
            _remoteDesktopControl.Connect += OnConnected;
        }

        private void OnConnected(object? sender, EventArgs e)
        {
            //Logger.Info($"OnConnected.");
        }

        private void OnError(object? sender, ErrorEventArgs e)
        {
            Logger.Error($"OnError. : {e.Message}");
        }

        private void OnAuthError(object? sender, ErrorEventArgs e)
        {
            Logger.Error($"OnAuthError. : {e.Message}");
        }

        public IDataWallContent Source
        {
            get => (IDataWallContent)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IDataWallContent), typeof(RemoteDataWallContentControl), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }
        
        private void UpdateContent(IDataWallContent content)
        {
            if (content == null)
            {
                return;
            }
        }
        private void PlayContent()
        {
            IDataWallContent content = Source;
            if (string.IsNullOrWhiteSpace(content?.Uri))
            {
                return;
            }

            bool isPreview = content.GetValue("isPreview", false);
            string uri = content.Uri;
            string userId = content.UserId;
            string userPassword = content.UserPassword;
            if (!isPreview)
            {
                _remoteDesktopControl.IsHitTestVisible = false;
            }
            else
            {
                content.RemoveProperty("isPreview");
            }

            _remoteDesktopControl.Scale = 1;
            _remoteDesktopControl.AutoReconnect = AutoReconnectMode.Always;
            _remoteDesktopControl.ConnectionMode = ConnectionMode.NewConnection;
            _remoteDesktopControl.ForceFullUpdateRequests = false;
            _remoteDesktopControl.Port = 5900;
            _remoteDesktopControl.ConnectedViewMode = ConnectedViewMode.RemoteDesktop;
            _remoteDesktopControl.PreferredEncoding = ViewerEncodings.Tight;
            _remoteDesktopControl.AllowJPEG = true;
            _remoteDesktopControl.CustomCompressionLevel = CompressionLevel.CompressionLevel9;
            _remoteDesktopControl.JpegQualityLevel = JpegQualityLevel.JpegLevel2;
            _remoteDesktopControl.UpdateRequestsDelay = 70;
            _remoteDesktopControl.RemoteHostAddress = uri;
            _remoteDesktopControl.Password = userPassword;
            _remoteDesktopControl.StartCommand.Execute(null);
        }

        public void DisposeContent()
        {
            try
            {
                mainGrid.Children.Clear();
                _remoteDesktopControl.StopCommand.Execute(null);
                _remoteDesktopControl.AuthenticationError -= OnAuthError;
                _remoteDesktopControl.Error -= OnError;
                _remoteDesktopControl.Connect -= OnConnected;
                _remoteDesktopControl.Dispose();
                _remoteDesktopControl = null;
            }
            catch (Exception e)
            {
                Logger.Error(e, "DisposeContent:");
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            //ThreadAction.PostOnUiThread(PlayContent);
            PlayContent();
        }

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                DisposeContent();
                //ThreadAction.PostOnUiThread(DisposeContent);

                Loaded -= OnLoaded;
                Unloaded -= OnUnloaded;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "UserControl_Unloaded:");
            }
        }
    }
}
