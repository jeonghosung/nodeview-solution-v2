﻿using NodeView.Drawing;

namespace NodeView.Models
{
    public class OsdConfig
    {
        private double _opacity = 100;
        public bool IsShow { get; set; } = false;
        public OsdAlignmentType Alignment { get; set; } = OsdAlignmentType.LeftTop;

        public IntPoint Offset { get; set; } = new IntPoint(0,0);

        public double Opacity
        {
            get => _opacity;
            set
            {
                if (value < 0)
                {
                    _opacity = 0;
                }
                else if (value > 100)
                {
                    _opacity = 100;
                }
                else
                {
                    _opacity = value;
                }
            }
        }

        public bool UseDropShadowEffect { get; set; } = false;
        public int FontSize { get; set; } = 25;
        public int Color { get; set; } = 0xFFFFFF;

        public OsdConfig Clone()
        {
            return new OsdConfig()
            {
                IsShow = IsShow,
                Alignment = Alignment,
                Offset = new IntPoint(Offset),
                Opacity = Opacity,
                UseDropShadowEffect = UseDropShadowEffect,
                FontSize = FontSize,
                Color = Color
            };
        }
    }

    public enum OsdAlignmentType: int
    {
        Center = 0,
        LeftTop = 5,
        RightTop = 6,
        LeftBottom = 9,
        RightBottom = 10
    }
}
