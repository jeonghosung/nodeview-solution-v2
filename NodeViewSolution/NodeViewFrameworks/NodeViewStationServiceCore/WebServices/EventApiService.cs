﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.Utils;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;
using RestApiResponse = NodeView.WebServers.RestApiResponse;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1/events")]
    public class EventApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IAuthentication _authentication = null;
        private StationServiceManager _serviceManager = null;

        public EventApiService()
        {
            try
            {
                _authentication = (IAuthentication)NodeViewContainerExtension.GetContainerProvider()?.Resolve(typeof(IAuthentication));
            }
            catch
            {
                //Logger.Info("Not supported IAuthentication.");
                _authentication = null;
            }
            try
            {
                _serviceManager = (StationServiceManager)NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(StationServiceManager));
            }
            catch
            {
                Logger.Info("Not supported IStationService.");
                _serviceManager = null;
            }
        }

        private bool IsSupported(IHttpContext context)
        {
            if (_serviceManager == null)
            {
                return false;
            }

            if (_serviceManager.IsVerbose)
            {
                Logger.Info(
                    $"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                //string requester = $"{context.Request.RemoteEndPoint.Address}";
            }

            return true;
        }

        private bool ValidateAccessToken(IHttpContext context)
        {
            return _authentication?.ValidateAccessToken(context) ?? true;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = "")]
        public IHttpContext OnEvent(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var json = JObject.Parse(context.Request.Payload);
                var nodeViewEvent = NodeViewEvent.CreateFrom(json);
                if (string.IsNullOrWhiteSpace(nodeViewEvent?.Id))
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload error");
                    return context;
                }

                if (string.IsNullOrWhiteSpace(nodeViewEvent.Requester))
                {
                    nodeViewEvent.Requester = $"{context.Request.RemoteEndPoint.Address}";
                }

                _serviceManager.OnEvent(nodeViewEvent);
                RestApiResponse.SendResponseSuccess(context, "");
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot OnEvent!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = "")]
        public IHttpContext GetEventActions(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var eventActions = _serviceManager.GetEventActions();
                if (eventActions != null)
                {
                    JArray resArray = new JArray();
                    foreach (var eventAction in eventActions)
                    {
                        resArray.Add(eventAction.ToJson());
                    }

                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get eventActions.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetEventAction!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        
        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"")]
        public IHttpContext PutActionEventAction(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                bool isSuccess = false;
                string resMessage = "";

                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "onEvent", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    if (paramJson != null)
                    {
                        var nodeViewEvent = NodeViewEvent.CreateFrom(paramJson);
                        if (!string.IsNullOrWhiteSpace(nodeViewEvent?.Id))
                        {
                            isSuccess = _serviceManager.OnEvent(nodeViewEvent);
                            resMessage = isSuccess ? $"Success [{nodeViewEvent.Id}]." : $"cannot run the event action.";
                        }
                    }
                }
                else if (string.Compare(putAction, "create", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    var eventAction = EventAction.CreateFrom(paramJson);
                    if (!string.IsNullOrWhiteSpace(eventAction?.Id))
                    {
                        isSuccess = _serviceManager.CreateEventActions(eventAction);
                        resMessage = isSuccess ? $"created [{eventAction.Id}]." : $"cannot created event action.";
                    }
                }
                else if (string.Compare(putAction, "update", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    var eventAction = EventAction.CreateFrom(paramJson);
                    if (!string.IsNullOrWhiteSpace(eventAction?.Id))
                    {
                        isSuccess = _serviceManager.UpdateEventAction(eventAction);
                        resMessage = isSuccess ? $"updated [{eventAction.Id}]." : $"cannot updated event action.";
                    }
                }
                else if (string.Compare(putAction, "deleteAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    _serviceManager.DeleteAllActions();
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "delete", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    string deleteId = JsonUtils.GetStringValue(json, "param.id");
                    if (!string.IsNullOrWhiteSpace(deleteId))
                    {
                        isSuccess = _serviceManager.DeleteEventAction(deleteId);
                        resMessage = $"[{deleteId}] is deleted.";
                    }
                    else
                    {
                        JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                        if (idArray != null && idArray.Count > 0)
                        {
                            List<string> idList = new List<string>();
                            foreach (JToken token in idArray)
                            {
                                string id = token.Value<string>();
                                if (!string.IsNullOrWhiteSpace(id))
                                {
                                    idList.Add(id);
                                }
                            }

                            if (idList.Count > 0)
                            {
                                int updateCount = _serviceManager.DeleteEventActions(idList.ToArray());
                                if (updateCount > 0)
                                {
                                    isSuccess = true;
                                    resMessage = $"[{updateCount}] event actions are deleted.";
                                }
                            }
                        }
                    }
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, resMessage);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, resMessage);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for NodeViewEventAction!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
