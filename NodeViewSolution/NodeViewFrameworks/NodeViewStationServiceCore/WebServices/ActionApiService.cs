﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.Utils;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using System.Text.RegularExpressions;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;
using RestApiResponse = NodeView.WebServers.RestApiResponse;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1/actions")]
    public class ActionApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IAuthentication _authentication = null;
        private StationServiceManager _serviceManager = null;
        public ActionApiService()
        {
            try
            {
                _authentication = (IAuthentication)NodeViewContainerExtension.GetContainerProvider()?.Resolve(typeof(IAuthentication));
            }
            catch
            {
                //Logger.Info("Not supported IAuthentication.");
                _authentication = null;
            }

            try
            {
                _serviceManager = (StationServiceManager)NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(StationServiceManager));
            }
            catch
            {
                Logger.Info("Not supported IStationService.");
                _serviceManager = null;
            }
        }

        private bool IsSupported(IHttpContext context)
        {
            if (_serviceManager == null)
            {
                return false;
            }

            if (_serviceManager.IsVerbose)
            {
                Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            }

            return true;
        }

        private bool ValidateAccessToken(IHttpContext context)
        {
            return _authentication?.ValidateAccessToken(context) ?? true;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = "")]
        public IHttpContext GetActions(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var actions = _serviceManager.GetActions();
                if (actions != null)
                {
                    JArray resArray = new JArray();
                    foreach (var action in actions)
                    {
                        resArray.Add(action.ToJson());
                    }

                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get actions.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetAction!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext RunAction(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var match = Regex.Match(context.Request.PathInfo, @"/(?<id>[a-zA-Z0-9_\-]+)$");
                if (!match.Success)
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot parse actionId.");
                    return context;
                }

                string actionId = match.Groups["id"].Value;

                if (_serviceManager.RunAction(actionId))
                {
                    RestApiResponse.SendResponseSuccess(context, $"Success [{actionId}].");
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "cannot run the action.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot RunAction!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"")]
        public IHttpContext PutActionNodeViewAction(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                bool isSuccess = false;
                string resMessage = "";

                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "run", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    var actionId = JsonUtils.GetValue(paramJson, "id", "");
                    if (!string.IsNullOrWhiteSpace(actionId))
                    {
                        isSuccess = _serviceManager.RunAction(actionId);
                        resMessage = isSuccess ? $"Success [{actionId}]." : $"cannot run the action.";
                    }
                }
                else if (string.Compare(putAction, "create", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }
                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    var action = NodeViewAction.CreateFrom(paramJson);
                    if (!string.IsNullOrWhiteSpace(action?.Id))
                    {
                        isSuccess = _serviceManager.CreateAction(action);
                        resMessage = isSuccess ? $"created [{action.Id}]." : $"cannot created action.";
                    }
                }
                else if (string.Compare(putAction, "update", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }
                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    var action = NodeViewAction.CreateFrom(paramJson);
                    if (!string.IsNullOrWhiteSpace(action?.Id))
                    {
                        isSuccess = _serviceManager.UpdateAction(action);
                        resMessage = isSuccess ? $"updated [{action.Id}]." : $"cannot updated action.";
                    }
                }
                else if (string.Compare(putAction, "deleteAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }
                    _serviceManager.DeleteAllActions();
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "delete", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    string deleteId = JsonUtils.GetStringValue(json, "param.id");
                    if (!string.IsNullOrWhiteSpace(deleteId))
                    {
                        isSuccess = _serviceManager.DeleteAction(deleteId);
                        resMessage = $"[{deleteId}] is deleted.";
                    }
                    else
                    {
                        JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                        if (idArray != null && idArray.Count > 0)
                        {
                            List<string> idList = new List<string>();
                            foreach (JToken token in idArray)
                            {
                                string id = token.Value<string>();
                                if (!string.IsNullOrWhiteSpace(id))
                                {
                                    idList.Add(id);
                                }
                            }

                            if (idList.Count > 0)
                            {
                                int updateCount = _serviceManager.DeleteActions(idList.ToArray());
                                if (updateCount > 0)
                                {
                                    isSuccess = true;
                                    resMessage = $"[{updateCount}] actions are deleted.";
                                }
                            }
                        }
                    }
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, resMessage);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, resMessage);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for NodeViewAction!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
