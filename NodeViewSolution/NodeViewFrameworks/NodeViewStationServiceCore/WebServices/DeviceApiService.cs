﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using System.Text.RegularExpressions;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1/devices")]
    public class DeviceApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IAuthentication _authentication = null;
        private StationServiceManager _serviceManager = null;

        public DeviceApiService()
        {
            try
            {
                _authentication = (IAuthentication)NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(IAuthentication));
            }
            catch
            {
                //Logger.Info("Not supported IAuthentication.");
                _authentication = null;
            }

            try
            {
                _serviceManager = (StationServiceManager) NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(StationServiceManager));
            }
            catch
            {
                Logger.Info("Not supported IStationService.");
                _serviceManager = null;
            }
        }

        private bool IsSupported(IHttpContext context)
        {
            if (_serviceManager == null)
            {
                return false;
            }

            if (_serviceManager.IsVerbose)
            {
                Logger.Info(
                    $"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                //string requester = $"{context.Request.RemoteEndPoint.Address}";
            }

            return true;
        }

        private bool ValidateAccessToken(IHttpContext context)
        {
            return _authentication?.ValidateAccessToken(context) ?? true;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"")]
        public IHttpContext GetDevices(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var devices = _serviceManager.GetDevices();

                var resArray = new JArray();
                foreach (var device in devices)
                {
                    resArray.Add(device.ToJson());
                }

                RestApiResponse.SendResponseSuccess(context, resArray);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetDevices!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext GetDevice(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/(?<id>[a-zA-Z0-9_\-]+)$");
                if (match.Success)
                {
                    id = match.Groups["id"].Value;
                }

                var device = _serviceManager.GetDevice(id);
                if (device == null)
                {
                    RestApiResponse.SendResponseBadRequest(context, $"Cannot find device by [{id}].");
                }
                else
                {
                    RestApiResponse.SendResponseSuccess(context, device.ToJson());
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetDevice!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext PutActionDevices(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/(?<id>[a-zA-Z0-9_\-]+)$");
                if (match.Success)
                {
                    id = match.Groups["id"].Value;
                }

                bool isSuccess = false;
                string resMessage = "";
                var json = JObject.Parse(context.Request.Payload);
                string command = JsonUtils.GetValue(json, "action", "");

                if (!string.IsNullOrWhiteSpace(command) && !string.IsNullOrWhiteSpace(id))
                {
                    KeyValueCollection parameters = new KeyValueCollection();
                    JObject paramJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    if (paramJson != null)
                    {
                        foreach (var property in paramJson.Properties())
                        {
                            parameters[property.Name] = $"{property.Value}";
                        }
                    }
                    isSuccess = _serviceManager.RunDeviceCommand(id, command, parameters);
                }
                if (!isSuccess)
                {
                    resMessage = $"Cannot run command [{id}/{command}].";
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, resMessage);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, resMessage);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot PutActionDevices!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
