﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using System.Web;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1")]
    public class EventLogApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static StationServiceManager _serviceManager = null;

        public EventLogApiService()
        {
            try
            {
                _serviceManager = (StationServiceManager) NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(StationServiceManager));
            }
            catch
            {
                Logger.Info("Not supported IStationService.");
                _serviceManager = null;
            }
        }

        private static bool IsSupported(IHttpContext context)
        {
            if (_serviceManager == null)
            {
                return false;
            }

            if (_serviceManager.IsVerbose)
            {
                Logger.Info(
                    $"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                //string requester = $"{context.Request.RemoteEndPoint.Address}";
            }

            return true;
        }
        
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/eventLogs")]
        public IHttpContext GetLatestEventLogs(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var webParameter = KeyValueCollection.CreateFrom(context.Request.QueryString);
                string fromDate = webParameter.GetValue("fromDate", "");
                string toDate = webParameter.GetValue("toDate", "");
                string searchKey = HttpUtility.UrlDecode(webParameter.GetValue("searchKey", ""));
                bool ascendingOrder = webParameter.GetValue("ascendingOrder", true);
                int maxLength = webParameter.GetValue("maxLength", 1000);

                List<IEventLog> eventLogList = new List<IEventLog>();
                if (string.IsNullOrWhiteSpace(fromDate) || string.IsNullOrWhiteSpace(toDate))
                {
                    eventLogList.AddRange(_serviceManager.GetLatestEventLogs());
                }
                else
                {
                    eventLogList.AddRange(_serviceManager.SearchEventLogs(fromDate, toDate, searchKey, ascendingOrder, maxLength));
                }

                var resArray = new JArray();
                foreach (var eventLog in eventLogList)
                {
                    resArray.Add(eventLog.ToJson());
                }

                RestApiResponse.SendResponseSuccess(context, resArray);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetLatestEventLogs!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/eventLogs")]
        public IHttpContext AppendEventLog(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var jsonToken = JToken.Parse(context.Request.Payload);
                if (jsonToken.Type == JTokenType.Object)
                {
                    var eventLog = EventLog.CreateFrom(jsonToken.Value<JObject>());
                    if (string.IsNullOrWhiteSpace(eventLog?.Message))
                    {
                        RestApiResponse.SendResponseBadRequest(context, "payload error");
                        return context;
                    }

                    if (_serviceManager.AppendEventLog(eventLog))
                    {
                        RestApiResponse.SendResponseSuccess(context, $"A eventLog appended.");
                    }
                    else
                    {
                        RestApiResponse.SendResponseFailure(context, "Cannot append eventLog.");
                    }
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload is not (a) eventLog json.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot AppendEventLog!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
