﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using System.Text.RegularExpressions;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1")]
    public class NodeApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static StationServiceManager _serviceManager = null;

        public NodeApiService()
        {
            try
            {
                _serviceManager = (StationServiceManager) NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(StationServiceManager));
            }
            catch
            {
                Logger.Info("Not supported IStationService.");
                _serviceManager = null;
            }
        }

        private static bool IsSupported(IHttpContext context)
        {
            if (_serviceManager == null)
            {
                return false;
            }

            if (_serviceManager.IsVerbose)
            {
                Logger.Info(
                    $"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                //string requester = $"{context.Request.RemoteEndPoint.Address}";
            }

            return true;
        }
        /*
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"")]
        public IHttpContext GetNodes1(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var webParameter = KeyValueCollection.CreateFrom(context.Request.QueryString);
                bool recursive = webParameter.GetValue("recursive", false);

                string path = "";
                var match = Regex.Match(context.Request.PathInfo, @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$");
                if (match.Success)
                {
                    path = match.Groups["path"].Value;
                }

                var nodes = _serviceManager.GetNodes(path, recursive);

                var resArray = new JArray();
                foreach (var node in nodes)
                {
                    resArray.Add(node.ToJson());
                }

                RestApiResponse.SendResponseSuccess(context, resArray);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetNodes!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
        */
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$")]
        public IHttpContext GetNodes(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var webParameter = KeyValueCollection.CreateFrom(context.Request.QueryString);
                bool recursive = webParameter.GetValue("recursive", false);

                string path = "";
                var match = Regex.Match(context.Request.PathInfo, @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$");
                if (match.Success)
                {
                    path = match.Groups["path"].Value;
                }
                path = path.TrimStart('/');

                var nodes = _serviceManager.GetNodes(path, recursive);

                var resArray = new JArray();
                foreach (var node in nodes)
                {
                    resArray.Add(node.ToJson());
                }

                RestApiResponse.SendResponseSuccess(context, resArray);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetNodes!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$")]
        public IHttpContext CreateNodes(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                string path = "";
                var match = Regex.Match(context.Request.PathInfo, @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$");
                if (match.Success)
                {
                    path = match.Groups["path"].Value;
                }
                path = path.TrimStart('/');

                var jsonToken = JToken.Parse(context.Request.Payload);
                if (jsonToken.Type == JTokenType.Object)
                {
                    var node = Node.CreateFrom(jsonToken.Value<JObject>());
                    if (string.IsNullOrWhiteSpace(node?.Id))
                    {
                        RestApiResponse.SendResponseBadRequest(context, "payload error");
                        return context;
                    }

                    if (_serviceManager.CreateNode(path, node))
                    {
                        RestApiResponse.SendResponseSuccess(context, $"A node created.");
                    }
                    else
                    {
                        RestApiResponse.SendResponseFailure(context, "Cannot create node.");
                    }
                }
                else if (jsonToken.Type == JTokenType.Array)
                {
                    List<INode> nodes = new List<INode>();
                    JArray array = jsonToken.Value<JArray>() ?? new JArray();
                    foreach (var token in array)
                    {
                        var node = Node.CreateFrom(token.Value<JObject>());
                        if (string.IsNullOrWhiteSpace(node?.Id))
                        {
                            RestApiResponse.SendResponseBadRequest(context, "payload error");
                            return context;
                        }

                        nodes.Add(node);
                    }

                    int updatedCount = _serviceManager.CreateNodes(path, nodes.ToArray());
                    if (updatedCount == nodes.Count)
                    {
                        RestApiResponse.SendResponseSuccess(context, $"{updatedCount} node(s) updated.");
                    }
                    else
                    {
                        RestApiResponse.SendResponseFailure(context,
                            $"{nodes.Count} node(s) request, but {updatedCount} node(s) updated.");
                    }
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload is not (a) content(s) json.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateNodes!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$")]
        public IHttpContext PutActionNodes(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                string path = "";
                var match = Regex.Match(context.Request.PathInfo, @"/nodes(?<path>(/[a-zA-Z][a-zA-Z0-9_\-]+)*)$");
                if (match.Success)
                {
                    path = match.Groups["path"].Value;
                }
                path = path.TrimStart('/');

                bool isSuccess = false;
                string resMessage = "";
                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();
                if (string.Compare(putAction, "createNode", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JObject param = JsonUtils.GetValue<JObject>(json, "param.node", null);
                    if (param != null)
                    {
                        var node = Node.CreateFrom(param);
                        if (!string.IsNullOrWhiteSpace(node?.Id))
                        {
                            isSuccess = _serviceManager.CreateNode(path, node);
                        }
                    }
                    if (!isSuccess)
                    {
                        resMessage = "Cannot create node.";
                    }
                }
                else if (string.Compare(putAction, "createNodes", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray nodeArray = JsonUtils.GetValue<JArray>(json, "param.nodes", null);
                    if (nodeArray != null)
                    {
                        List<INode> nodes = new List<INode>();
                        foreach (var token in nodeArray)
                        {
                            var node = Node.CreateFrom(token.Value<JObject>());
                            if (string.IsNullOrWhiteSpace(node?.Id))
                            {
                                continue;
                            }
                            nodes.Add(node);
                        }
                        if (nodes.Count > 0)
                        {
                            int updatedCount = _serviceManager.CreateNodes(path, nodes.ToArray());
                            isSuccess = updatedCount == nodes.Count;
                            resMessage = $"{updatedCount} node(s) updated.";
                        }
                    }
                    if (!isSuccess)
                    {
                        resMessage = string.IsNullOrWhiteSpace(resMessage) ? "Cannot create nodes." : resMessage;
                    }
                }
                else if (string.Compare(putAction, "updateNode", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JObject param = JsonUtils.GetValue<JObject>(json, "param.node", null);
                    if (param != null)
                    {
                        var node = Node.CreateFrom(param);
                        if (!string.IsNullOrWhiteSpace(node?.Id))
                        {
                            isSuccess = _serviceManager.UpdateNode(node);
                        }
                    }
                    if (!isSuccess)
                    {
                        resMessage = "Cannot update node.";
                    }
                }
                else if (string.Compare(putAction, "updateNodes", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray nodeArray = JsonUtils.GetValue<JArray>(json, "param.nodes", null);
                    if (nodeArray != null)
                    {
                        List<INode> nodes = new List<INode>();
                        foreach (var token in nodeArray)
                        {
                            var node = Node.CreateFrom(token.Value<JObject>());
                            if (string.IsNullOrWhiteSpace(node?.Id))
                            {
                                continue;
                            }
                            nodes.Add(node);
                        }
                        if (nodes.Count > 0)
                        {
                            int updatedCount = _serviceManager.UpdateNodes(nodes.ToArray());
                            isSuccess = updatedCount == nodes.Count;
                            resMessage = $"{updatedCount} node(s) updated.";
                        }
                    }
                    if (!isSuccess)
                    {
                        resMessage = string.IsNullOrWhiteSpace(resMessage) ? "Cannot update nodes." : resMessage;
                    }
                }
                else if (string.Compare(putAction, "patchNode", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JObject param = JsonUtils.GetValue<JObject>(json, "param.node", null);
                    if (param != null)
                    {
                        var node = Node.CreateFrom(param);
                        if (!string.IsNullOrWhiteSpace(node?.Id))
                        {
                            isSuccess = _serviceManager.PatchNode(node);
                        }
                    }
                    if (!isSuccess)
                    {
                        resMessage = "Cannot patch node.";
                    }
                }
                else if (string.Compare(putAction, "patchNodes", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray nodeArray = JsonUtils.GetValue<JArray>(json, "param.nodes", null);
                    if (nodeArray != null)
                    {
                        List<INode> nodes = new List<INode>();
                        foreach (var token in nodeArray)
                        {
                            var node = Node.CreateFrom(token.Value<JObject>());
                            if (string.IsNullOrWhiteSpace(node?.Id))
                            {
                                continue;
                            }
                            nodes.Add(node);
                        }
                        if (nodes.Count > 0)
                        {
                            int updatedCount = _serviceManager.PatchNodes(nodes.ToArray());
                            isSuccess = updatedCount == nodes.Count;
                            resMessage = $"{updatedCount} node(s) updated.";
                        }
                    }
                    if (!isSuccess)
                    {
                        resMessage = string.IsNullOrWhiteSpace(resMessage) ? "Cannot patch nodes." : resMessage;
                    }
                }
                else if (string.Compare(putAction, "deleteNode", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetValue(json, "param.id", "");
                    string uid = string.IsNullOrWhiteSpace(path) ? id : $"{path}/{id}";
                    if (!string.IsNullOrWhiteSpace(id))
                    {
                        isSuccess = _serviceManager.DeleteNode(uid);
                    }
                    if (!isSuccess)
                    {
                        resMessage = "Cannot delete node.";
                    }
                }
                else if (string.Compare(putAction, "deleteNodes", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                    if (idArray != null && idArray.Count > 0)
                    {
                        List<string> idList = new List<string>();
                        foreach (JToken token in idArray)
                        {
                            string id = token.Value<string>();
                            if (!string.IsNullOrWhiteSpace(id))
                            {
                                idList.Add(id);
                            }
                        }

                        if (idList.Count > 0)
                        {
                            isSuccess = _serviceManager.DeleteNodes(path, idList.ToArray()) > 0;
                        }
                        isSuccess = true;
                    }
                }
                else if (string.Compare(putAction, "deleteAllNodes", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    bool recursive = JsonUtils.GetValue(json, "param.recursive", false);
                    _serviceManager.DeleteAllNodes(path, recursive);
                    isSuccess = true;
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, resMessage);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, resMessage);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot PutActionNodes!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
