﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1/folders")]
    public class FolderApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static StationServiceManager _serviceManager = null;

        public FolderApiService()
        {
            try
            {
                _serviceManager = (StationServiceManager) NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(StationServiceManager));
            }
            catch
            {
                Logger.Info("Not supported IStationService.");
                _serviceManager = null;
            }
        }

        private static bool IsSupported(IHttpContext context)
        {
            if (_serviceManager == null)
            {
                return false;
            }

            if (_serviceManager.IsVerbose)
            {
                Logger.Info(
                    $"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                //string requester = $"{context.Request.RemoteEndPoint.Address}";
            }

            return true;
        }

        
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/node")]
        public IHttpContext GetNodeFolders(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var folders = _serviceManager.GetNodeFolders();

                var resArray = new JArray();
                foreach (var folder in folders)
                {
                    resArray.Add(folder);
                }

                RestApiResponse.SendResponseSuccess(context, resArray);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetNodeFolders!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/device")]
        public IHttpContext GetDeviceFolders(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                var folders = _serviceManager.GetDeviceFolders();

                var resArray = new JArray();
                foreach (var folder in folders)
                {
                    resArray.Add(folder);
                }

                RestApiResponse.SendResponseSuccess(context, resArray);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetDeviceFolders!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
