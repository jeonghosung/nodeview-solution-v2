﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.DeviceControllers;
using NodeView.Frameworks.Plugins;
using NodeView.Frameworks.Stations;
using NodeView.Plugins;
using NodeView.Protocols.NodeViews;
using NodeView.WebServers;
using Prism.Ioc;

namespace NodeView.NodeViews
{
    public class StationServiceManager : IStationService, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private Timer _actionQueueWorkerTimer = null;
        private readonly ConcurrentQueue<Action> _actionQueue = new ConcurrentQueue<Action>();

        protected WebServer _webServer = null;
        protected NodeViewCenterClient _centerClient = null;
        protected PluginManager _pluginManager = null;
        protected IStationRepository _repository;
        //
        private readonly Dictionary<string, NodeViewAction> _actionDictionary = new Dictionary<string, NodeViewAction>();
        private readonly Dictionary<string, EventAction> _eventActionDictionary = new Dictionary<string, EventAction>();
        protected readonly Dictionary<string, Node> _nodeDictionary = new Dictionary<string, Node>();
        protected readonly Dictionary<string, IDataListener> _dataListenerDictionary = new Dictionary<string, IDataListener>();
        protected readonly Dictionary<string, IDeviceController> _deviceControllerDictionary = new Dictionary<string, IDeviceController>();
        protected volatile bool _isStarted = false;

        public IContainerProvider ContainerProvider { get; protected set; }
        public string ConfigFilename { get; set; }
        public Configuration Config { get; protected set; }
        public bool IsNodeVewApp { get; protected set; } = true;
        public bool IsTestMode { get; protected set; } = false;
        public bool IsVerbose { get; protected set; } = false;
        public string ServiceIp { get; protected set; }
        public int ServicePort { get; protected set; }
        public int ProcessManagerPort { get; protected set; }
        public string ServiceName { get; protected set; }
        public string ServiceBaseUrl { get; protected set; }
        public string CachesFolder { get; protected set; } = "";
        public string ConfigsFolder { get; protected set; } = "";
        public bool IsStarted
        {
            get => _isStarted;
            protected set => _isStarted = value;
        }

        protected StationServiceManager()
        {
            CachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(CachesFolder);
            ConfigsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            Directory.CreateDirectory(ConfigsFolder);
        }
        public virtual bool Initialize(IContainerProvider containerProvider, string configFilename = "")
        {
            ContainerProvider = containerProvider;
            _pluginManager = new PluginManager(containerProvider, this);
            if (string.IsNullOrWhiteSpace(configFilename))
            {
                configFilename = AppUtils.GetAppName() + ".config.xml";
            }

            if (!File.Exists(configFilename))
            {
                configFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
                if (!File.Exists(configFilename))
                {
                    Logger.Error("Cannot find config file.");
                    return false;
                }
            }

            ConfigFilename = configFilename;
            Config = Configuration.Load(configFilename);
            if (Config == null)
            {
                Logger.Error("Cannot load config file.");
                return false;
            }

            IsTestMode = Config.GetValue("isTestMode", IsTestMode);
            IsVerbose = IsTestMode || Config.GetValue("isVerbose", IsVerbose);
            ServiceIp = Config.GetValue("service.ip", "127.0.0.1");
            ServicePort = Config.GetValue("service.port", 20201);
            ServiceName = Config.GetValue("service.name", AppUtils.GetAppName());
            ProcessManagerPort = Config.GetValue("processManager.port", 20102);
            ServiceBaseUrl = $"http://{ServiceIp}:{ServicePort}";
            _webServer = new WebServer(ServicePort);
            if (IsNodeVewApp)
            {
                string centerIp = Config.GetValue("nodeViewCenter.ip", "127.0.0.1");
                int centerPort = Config.GetValue("nodeViewCenter.port", 20101);
                _centerClient = new NodeViewCenterClient(centerIp, centerPort, ServiceIp, ServicePort, ProcessManagerPort, ServiceName);
            }

            _repository = ContainerProvider.Resolve<IStationRepository>();

            if (!OnInitializing(Config))
            {
                Logger.Error("Cannot initializing.");
                return false;
            }

            LoadDataFromRepository();

            InitDefaultPlugins();
            if (!InitPlugins(Config))
            {
                Logger.Error("Cannot init plugins.");
                return false;
            }

            OnInitialized();
            return true;
        }

        private void LoadDataFromRepository()
        {
            foreach (var action in _repository.GetActions())
            {
                _actionDictionary[action.Id] = action.Clone();
            }
            foreach (var eventAction in _repository.GetEventActions())
            {
                _eventActionDictionary[eventAction.Id] = eventAction.Clone();
            }
            foreach (var node in _repository.GetNodes())
            {
                _nodeDictionary[node.PathId] = Node.CreateFrom(node);
            }
        }

        private void ActionQueueWorkerTimerCallback(object state)
        {
            _actionQueueWorkerTimer.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                while (_actionQueue.TryDequeue(out var action))
                {
                    action();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ActionQueueWorkerTimerCallback:");
            }
            _actionQueueWorkerTimer.Change(TimeSpan.FromMilliseconds(200), TimeSpan.FromMilliseconds(200));
        }

        public bool Start()
        {
            if (!IsStarted)
            
            {
                try
                {
                    if (!OnStart()) return false;
                    if (!StartPlugins())
                    {
                        Logger.Error("Cannot start plugins.");
                        return false;
                    }

                    if (IsNodeVewApp)
                    {
                        if (!_centerClient?.Start() ?? true)
                        {
                            Logger.Error("Cannot start nodeview center client.");
                            return false;
                        }
                    }
                    if (!_webServer?.Start() ?? false)
                    {
                        Logger.Error("Cannot start web server.");
                        return false;
                    }

                    //startAction
                    {
                        if (_actionQueueWorkerTimer == null)
                        {
                            _actionQueueWorkerTimer = new Timer(ActionQueueWorkerTimerCallback);
                        }
                        _actionQueueWorkerTimer?.Change(TimeSpan.FromMilliseconds(200), TimeSpan.FromMilliseconds(200));
                    }

                    OnStarted();
                    IsStarted = true;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Start:");
                }
            }
            return IsStarted;
        }

        public virtual void Stop()
        {
            try
            {
                _actionQueueWorkerTimer?.Change(Timeout.Infinite, Timeout.Infinite);

                _webServer?.Stop();
                _centerClient?.Stop();
                StopPlugins();
                OnStop();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Stop:");
            }
            IsStarted = false;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stop();
                _pluginManager?.Dispose();
            }
        }
        protected virtual bool OnInitializing(Configuration config)
        {
            return true;
        }
        protected virtual void OnInitialized()
        {
        }


        protected virtual bool OnStart()
        {
            return true;
        }

        protected virtual void OnStarted()
        {

        }

        protected virtual void OnStop()
        {
        }

        protected virtual void InitDefaultPlugins()
        {
            if (_pluginManager == null) return;

            var systemController = new SystemController("system");
            systemController.Init(null, this);
            systemController.IsTestMode = IsTestMode;
            systemController.IsVerbose = IsVerbose;
            _pluginManager.AddPlugin(systemController);

            var actionRunner = new NodeViewActionRunner("actionRunner");
            actionRunner.Init(null, this);
            actionRunner.IsTestMode = IsTestMode;
            actionRunner.IsVerbose = IsVerbose;
            _pluginManager.AddPlugin(actionRunner);

            if (_centerClient != null)
            {
                var eventSender = new NodeViewEventSender("eventSender", _centerClient.CenterApiBaseUri);
                eventSender.Init(null, this);
                eventSender.IsTestMode = IsTestMode;
                eventSender.IsVerbose = IsVerbose;
                _pluginManager.AddPlugin(eventSender);
            }
        }

        protected virtual bool InitPlugins(Configuration config)
        {
            bool hasError = false;
            if (config.TryGetNode("plugins", out var pluginsNode))
            {
                foreach (var pluginNode in pluginsNode.ChildNodes)
                {
                    if (pluginNode is Configuration pluginConfig)
                    {
                        if (!pluginConfig.GetValue("isEnabled", true))
                        {
                            continue;
                        }
                        if (!_pluginManager.LoadPlugin(pluginConfig))
                        {
                            hasError = true;
                        }
                    }
                }
            }

            return !hasError;
        }
        protected virtual bool StartPlugins()
        {
            if (_pluginManager == null) return false;

            return _pluginManager.Start() == _pluginManager.Count;
        }
        protected virtual void StopPlugins()
        {
            try
            {
                _pluginManager?.Stop();
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopPlugins:");
            }
        }
        
        public virtual bool RegisterDataListener(IDataListener dataListener)
        {
            if (string.IsNullOrWhiteSpace(dataListener?.Id)) return false;

            if (_dataListenerDictionary.TryGetValue(dataListener.Id, out var found))
            {
                return false;
            }

            _dataListenerDictionary[dataListener.Id] = dataListener;
            return true;
        }

        public virtual void UnRegisterDataListener(IDataListener dataListener)
        {
            if (string.IsNullOrWhiteSpace(dataListener?.Id)) return;

            _dataListenerDictionary.Remove(dataListener.Id);
        }

        public virtual bool RegisterDeviceController(IDeviceController deviceController)
        {
            if (string.IsNullOrWhiteSpace(deviceController?.Id)) return false;

            if (_deviceControllerDictionary.TryGetValue(deviceController.Id, out var found))
            {
                return false;
            }

            _deviceControllerDictionary[deviceController.Id] = deviceController;
            return true;
        }

        public virtual void UnRegisterDeviceController(IDeviceController deviceController)
        {
            if (string.IsNullOrWhiteSpace(deviceController?.Id)) return;

            _deviceControllerDictionary.Remove(deviceController.Id);
        }

        //actions
        public NodeViewAction[] GetActions()
        {
            return _actionDictionary.Values.ToArray();
        }

        public bool CreateAction(NodeViewAction action)
        {
            if (string.IsNullOrWhiteSpace(action?.Id)) return false;

            if (_actionDictionary.TryGetValue(action.Id, out var foundAction))
            {
                Logger.Warn($"[{action.Id}] ids already exist.");
                return false;
            }
            _actionDictionary[action.Id] = action;

            var createActions = new[] { action.Clone() };
            _actionQueue.Enqueue(() =>
            {
                _repository.CreateActions(createActions);
            });
            
            return true;
        }

        public bool UpdateAction(NodeViewAction action)
        {
            if (string.IsNullOrWhiteSpace(action?.Id)) return false;

            if (_actionDictionary.TryGetValue(action.Id, out var foundAction))
            {
                foundAction.Update(action);

                var updateActions = new[] {action.Clone()};
                _actionQueue.Enqueue(() =>
                {
                    _repository.UpdateActions(updateActions);
                });
                return true;
            }
            return false;
        }

        public bool DeleteAction(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;
            return DeleteActions(new[] {id}) == 1;
        }

        public int DeleteActions(string[] ids)
        {
            if (ids == null || ids.Length == 0) return 0;

            List<string> deletedList = new List<string>();
            foreach (string id in ids)
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    continue;
                }

                if (_actionDictionary.Remove(id))
                {
                    deletedList.Add(id);
                }
            }
            if (deletedList.Count > 0)
            {
                var deleted = deletedList.ToArray();
                _actionQueue.Enqueue(() =>
                {
                    _repository.DeleteActions(deleted);
                });
            }
            return deletedList.Count;
        }

        public void DeleteAllActions()
        {
            if (_actionDictionary.Count > 0)
            {
                _actionDictionary.Clear();

                _actionQueue.Enqueue(() =>
                {
                    _repository.DeleteAllActions();
                });
            }
        }

        public bool RunAction(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (_actionDictionary.TryGetValue(id, out var action))
            {
                RunAction(action);
                return true;
            }
            else
            {
                Logger.Warn($"action [{id}] 미등록 액션");
            }
            return false;
        }

        protected virtual void RunAction(NodeViewAction action)
        {
            if (string.IsNullOrWhiteSpace(action?.Id)) return;

            Logger.Info($"RunAction [{action.Id}]");
            var commands = action.Commands;
            int runCount = 0;
            if (commands.Length > 0)
            {
                foreach (var command in commands)
                {
                    if(string.IsNullOrWhiteSpace(command?.DeviceId) || string.IsNullOrWhiteSpace(command?.Command)) continue;

                    if (RunDeviceCommand(command))
                    {
                        if (IsVerbose)
                        {
                            Logger.Info($"Success [{command.AppId}/{command.DeviceId}/{command.Command}]");
                        }
                        runCount++;
                    }
                    else
                    {
                        Logger.Error($"Cannnot run [{command.AppId}/{command.DeviceId}/{command.Command}]");
                    }
                }

                Logger.Info($"EndAction [{action.Id}][{commands.Length}/{runCount}]==================");
            }
        }

        protected virtual bool RunDeviceCommand(NodeViewCommand command)
        {
            if (string.IsNullOrWhiteSpace(command?.DeviceId) || string.IsNullOrWhiteSpace(command?.Command)) return false;

            //Center 만 appId를 가질 수 있다.
            if (!string.IsNullOrWhiteSpace(command.AppId)) return false;

            return RunDeviceCommand(command.DeviceId, command.Command, command.Parameters);
        }

        //eventActions
        public EventAction[] GetEventActions()
        {
            return _eventActionDictionary.Values.ToArray();
        }

        public bool CreateEventActions(EventAction eventAction)
        {
            if (string.IsNullOrWhiteSpace(eventAction?.Id)) return false;

            if (_eventActionDictionary.TryGetValue(eventAction.Id, out var found))
            {
                Logger.Warn($"[{eventAction.Id}] ids already exist.");
                return false;
            }
            _eventActionDictionary[eventAction.Id] = eventAction;

            var createEvenActions = new[] {eventAction};
            _actionQueue.Enqueue(() =>
            {
                _repository.CreateEventActions(createEvenActions);
            });
            return true;
        }

        public bool UpdateEventAction(EventAction eventAction)
        {
            if (string.IsNullOrWhiteSpace(eventAction?.Id)) return false;

            if (_eventActionDictionary.TryGetValue(eventAction.Id, out var foundEventAction))
            {
                foundEventAction.Update(eventAction);

                var updatedEvenActions = new[] { eventAction };
                _actionQueue.Enqueue(() =>
                {
                    _repository.UpdateEventActions(updatedEvenActions);
                });
                return true;
            }
            return false;
        }

        public bool DeleteEventAction(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;
            return DeleteEventActions(new[] { id }) == 1;
        }

        public int DeleteEventActions(string[] ids)
        {
            if (ids == null || ids.Length == 0) return 0;

            List<string> deletedList = new List<string>();

            foreach (string id in ids)
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    continue;
                }

                if (_eventActionDictionary.Remove(id))
                {
                    deletedList.Add(id);
                }
            }
            if (deletedList.Count > 0)
            {
                var deletedEvenActions = deletedList.ToArray();
                _actionQueue.Enqueue(() =>
                {
                    _repository.DeleteEventActions(deletedEvenActions);
                });
            }
            return deletedList.Count;
        }

        public void DeleteAllEventActions()
        {
            if (_eventActionDictionary.Count > 0)
            {
                _eventActionDictionary.Clear();

                _actionQueue.Enqueue(() =>
                {
                    _repository.DeleteAllEventActions();
                });
            }
        }

        public bool OnEvent(NodeViewEvent nodeViewEvent)
        {
            if (string.IsNullOrWhiteSpace(nodeViewEvent?.Id)) return false;

            try
            {
                if (_eventActionDictionary.TryGetValue(nodeViewEvent.Id, out var eventAction))
                {
                    WriteEventLog(nodeViewEvent, eventAction);
                    bool res = true;
                    if (nodeViewEvent.IsOn)
                    {
                        Logger.Info($"event [{nodeViewEvent.Id}] on");
                        if (!string.IsNullOrWhiteSpace(eventAction.EventOnActionId))
                        {
                            res = RunAction(eventAction.EventOnActionId);
                        }
                    }
                    else
                    {
                        Logger.Info($"event [{nodeViewEvent.Id}] off");
                        if (!string.IsNullOrWhiteSpace(eventAction.EventOffActionId))
                        {
                            res = RunAction(eventAction.EventOffActionId);
                        }
                    }
                    return res;
                }
                else
                {
                    WriteEventLog(nodeViewEvent);
                }

                if (nodeViewEvent.IsOn)
                {
                    Logger.Warn($"event [{nodeViewEvent.Id}] on, 미등록 이벤트");
                }
                else
                {
                    Logger.Warn($"event [{nodeViewEvent.Id}] off, 미등록 이벤트");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "OnEvent:");
            }
            return false;
        }

        protected virtual void WriteEventLog(NodeViewEvent nodeViewEvent, EventAction eventAction = null)
        {
            
        }

        public string[] GetNodeFolders()
        {
            Dictionary<string, string> pathDictionary = new Dictionary<string, string>();

            foreach (var node in _nodeDictionary.Values.ToArray())
            {
                if (!pathDictionary.TryGetValue(node.Path, out var foundPath))
                {
                    pathDictionary[node.Path] = "/" + node.Path;
                }
            }

            var pathList = pathDictionary.Values.ToList();
            pathList.Sort(String.Compare);
            return pathList.ToArray();
        }

        public INode GetNode(string path, string id)
        {
            string pathId = string.IsNullOrWhiteSpace(path) ? id : $"{path}/{id}";
            if (string.IsNullOrWhiteSpace(pathId)) return null;

            if (_nodeDictionary.TryGetValue(pathId, out var node))
            {
                return node;
            }
            return null;
        }

        public bool CreateNode(string path, INode node, bool keepPathInNode = false)
        {
            if (node == null) return false;
            return CreateNodes(path, new []{node}, keepPathInNode) == 1;
        }

        public bool UpdateNode(INode node)
        {
            if (node == null) return false;
            return UpdateNodes(new[] { node }) == 1;
        }

        public bool PatchNode(INode node)
        {
            if (node == null) return false;
            return PatchNodes(new[] { node }) == 1;
        }

        public bool DeleteNode(string uid)
        {
            if (string.IsNullOrWhiteSpace(uid)) return false;

            return DeleteNodes(new[] { uid }) == 1;
        }

        public INode[] GetNodes(string path, bool recursive = false)
        {
            if (string.IsNullOrWhiteSpace(path) && recursive)
            {
                return _nodeDictionary.Values.ToArray();
            }

            if (_nodeDictionary.Count == 0)
            {
                return new INode[0];
            }

            List<INode> nodes = new List<INode>();
            if (recursive)
            {
                foreach (var node in _nodeDictionary.Values.ToArray())
                {
                    if (node.Path.StartsWith(path))
                    {
                        nodes.Add(node);
                    }
                }
            }
            else
            {
                foreach (var node in _nodeDictionary.Values.ToArray())
                {
                    if (node.Path == path)
                    {
                        nodes.Add(node);
                    }
                }
            }

            return nodes.ToArray();
        }

        public int CreateNodes(string path, INode[] nodes, bool keepPathInNode = false)
        {
            if (nodes == null) return 0;

            List<INode> createdList = new List<INode>();
            foreach (var requestNode in nodes)
            {
                var node = Node.CreateFrom(requestNode);
                if (string.IsNullOrWhiteSpace(node.Id))
                {
                    continue;
                }

                if (keepPathInNode)
                {
                    if (!node.Path.StartsWith(path))
                    {
                        node.Path = $"{path}/{node.Path}";
                        node.Path = node.Path.Trim('/');
                    }
                }
                else
                {
                    node.Path = path;
                }

                if (_nodeDictionary.TryGetValue(node.PathId, out var foundNode))
                {
                    Logger.Error($"[{node.PathId}] is alreay exist.");
                    continue;
                }
                _nodeDictionary[node.PathId] = node;
                createdList.Add(node);
            }
            if (createdList.Count > 0)
            {
                var createdArray = createdList.ToArray();
                _actionQueue.Enqueue(() =>
                {
                    _repository.CreateNodes(createdArray);
                });

                List<UpdatedNodeArg> updatedNodeArgList = new List<UpdatedNodeArg>();
                foreach (var node in createdList)
                {
                    updatedNodeArgList.Add(new UpdatedNodeArg(node.Path, node, null));
                }

                var updatedNodeArgs = updatedNodeArgList.ToArray();
                foreach (var dataListener in _dataListenerDictionary.Values.ToArray())
                {
                    Task.Factory.StartNew(() =>
                    {
                        dataListener.UpdatedNodes(updatedNodeArgs);
                    });
                }
            }
            return createdList.Count;
        }

        public int UpdateNodes(INode[] nodes)
        {
            if (nodes == null) return 0;

            List<UpdatedNodeArg> updatedNodeArgList = new List<UpdatedNodeArg>();
            List<INode> updatedList = new List<INode>();
            foreach (var requestNode in nodes)
            {
                var node = Node.CreateFrom(requestNode);
                if (string.IsNullOrWhiteSpace(node.Id))
                {
                    continue;
                }

                if (_nodeDictionary.TryGetValue(node.PathId, out var foundNode))
                {
                    var oldNode = foundNode.Clone();
                    foundNode.Update(node);
                    updatedList.Add(foundNode);

                    updatedNodeArgList.Add(new UpdatedNodeArg(foundNode.Path, foundNode, oldNode));
                }
                //Logger.Error($"Canont find node by [{node.PathId}].");
            }
            if (updatedList.Count > 0)
            {
                _actionQueue.Enqueue(() =>
                {
                    _repository.UpdateNodes(updatedList.ToArray());
                });

                var updatedNodeArgs = updatedNodeArgList.ToArray();
                foreach (var dataListener in _dataListenerDictionary.Values.ToArray())
                {
                    Task.Factory.StartNew(() =>
                    {
                        dataListener.UpdatedNodes(updatedNodeArgs);
                    });
                }
            }
            return updatedList.Count;
        }

        public int PatchNodes(INode[] nodes)
        {
            if (nodes == null) return 0;

            List<UpdatedNodeArg> updatedNodeArgList = new List<UpdatedNodeArg>();
            List<INode> updatedList = new List<INode>();
            foreach (var requestNode in nodes)
            {
                var node = Node.CreateFrom(requestNode);
                if (string.IsNullOrWhiteSpace(node.Id))
                {
                    continue;
                }

                if (_nodeDictionary.TryGetValue(node.PathId, out var foundNode))
                {
                    var oldNode = foundNode.Clone();
                    foundNode.Patch(node);
                    updatedList.Add(foundNode);

                    updatedNodeArgList.Add(new UpdatedNodeArg(foundNode.Path, foundNode, oldNode));

                }
                //Logger.Error($"Canont find node by [{node.PathId}].");
            }
            if (updatedList.Count > 0)
            {
                _actionQueue.Enqueue(() =>
                {
                    _repository.UpdateNodes(updatedList.ToArray());
                });

                var updatedNodeArgs = updatedNodeArgList.ToArray();
                foreach (var dataListener in _dataListenerDictionary.Values.ToArray())
                {
                    Task.Factory.StartNew(() =>
                    {
                        dataListener.UpdatedNodes(updatedNodeArgs);
                    });
                }
            }
            return updatedList.Count;
        }

        public int DeleteNodes(string[] uIds)
        {
            if (uIds == null) return 0;

            List<UpdatedNodeArg> updatedNodeArgList = new List<UpdatedNodeArg>();
            List<string> deletedList = new List<string>();
            foreach (var uid in uIds)
            {
                if (string.IsNullOrWhiteSpace(uid))
                {
                    continue;
                }
                
                if (_nodeDictionary.TryGetValue(uid, out var foundNode))
                {
                    _nodeDictionary.Remove(uid);
                    deletedList.Add(uid);
                    updatedNodeArgList.Add(new UpdatedNodeArg(foundNode.Path, null, foundNode));
                }
            }
            if (deletedList.Count > 0)
            {
                _repository.DeleteNodes(deletedList.ToArray());
            }
            return deletedList.Count;
        }

        public int DeleteNodes(string path, string[] ids)
        {
            if (ids == null) return 0;

            List<UpdatedNodeArg> updatedNodeArgList = new List<UpdatedNodeArg>();
            List<string> deletedList = new List<string>();
            foreach (var id in ids)
            {
                if (string.IsNullOrWhiteSpace(id))
                {
                    continue;
                }
                string pathId = string.IsNullOrWhiteSpace(path) ? id : $"{path}/{id}";
                if (id.Contains("/"))
                {
                    pathId = id;
                }
                if (_nodeDictionary.TryGetValue(pathId, out var foundNode))
                {
                    _nodeDictionary.Remove(pathId);
                    deletedList.Add(pathId);
                    updatedNodeArgList.Add(new UpdatedNodeArg(foundNode.Path, null, foundNode));
                }
            }
            if (deletedList.Count > 0)
            {
                _repository.DeleteNodes(deletedList.ToArray());
            }
            return deletedList.Count;
        }

        public void DeleteAllNodes(string path, bool recursive = false)
        {
            bool isSomeDeleted = false;
            if (_nodeDictionary.Count == 0)
            {
                return;
            }
            if (recursive)
            {
                if (string.IsNullOrWhiteSpace(path))
                {
                    _nodeDictionary.Clear();
                    isSomeDeleted = true;
                }
                else
                {
                    foreach (var node in _nodeDictionary.Values.ToArray())
                    {
                        if (node.Path.StartsWith(path))
                        {
                            _nodeDictionary.Remove(node.PathId);
                            isSomeDeleted = true;
                        }
                    }
                }
            }
            else
            {
                foreach (var node in _nodeDictionary.Values.ToArray())
                {
                    if (node.Path == path)
                    {
                        _nodeDictionary.Remove(node.PathId);
                        isSomeDeleted = true;
                    }
                }
            }

            if (isSomeDeleted)
            {
                _repository.DeleteAllNodes(path, recursive);

                _actionQueue.Enqueue(() =>
                {
                    _repository.DeleteAllNodes(path, recursive);
                });

                foreach (var dataListener in _dataListenerDictionary.Values.ToArray())
                {
                    Task.Factory.StartNew(() =>
                    {
                        dataListener.DeleteAllNodes(path, recursive);
                    });
                }
            }
        }
        public string[] GetDeviceFolders()
        {
            Dictionary<string, string> pathDictionary = new Dictionary<string, string>();

            foreach (var device in _deviceControllerDictionary.Values.ToArray())
            {
                if (!pathDictionary.TryGetValue(device.Path, out var foundPath))
                {
                    pathDictionary[device.Path] = "/" + device.Path;
                }
            }

            var pathList = pathDictionary.Values.ToList();
            pathList.Sort(String.Compare);
            return pathList.ToArray();
        }

        public IDevice[] GetDevices()
        {
            return _deviceControllerDictionary.Values.ToArray();
        }

        public IDevice GetDevice(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            if (_deviceControllerDictionary.TryGetValue(id, out var device))
            {
                return device;
            }
            return null;
        }

        public bool RunDeviceCommand(string id, string command, KeyValueCollection parameters)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(command)) return false;
            if (_deviceControllerDictionary.TryGetValue(id, out var device))
            {
                return device.RunCommand(command, parameters);
            }
            return false;
        }

        public IEventLog[] GetLatestEventLogs()
        {
            IEventLog[] eventLogs;
            try
            {
                eventLogs = _repository.GetLatestEventLogs()?.ToArray() ?? new IEventLog[0];
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetLatestEventLogs:");
                eventLogs = new IEventLog[0];
            }

            return eventLogs;
        }

        public IEventLog[] SearchEventLogs(string fromDate, string toDate, string searchKey, bool ascendingOrder = true, int maxLength = 1000)
        {
            IEventLog[] eventLogs;
            try
            {
                eventLogs = _repository.SearchEventLogs(fromDate, toDate, searchKey, ascendingOrder, maxLength)?.ToArray() ?? new IEventLog[0];
            }
            catch (Exception e)
            {
                Logger.Error(e, "SearchEventLogs:");
                eventLogs = new IEventLog[0];
            }

            return eventLogs;
        }

        public bool AppendEventLog(IEventLog eventLog)
        {
            if (eventLog == null) return false;

            bool res = false;
            try
            {
                res = _repository.AppendEventLog(eventLog);
            }
            catch (Exception e)
            {
                Logger.Error(e, "AppendEventLog:");
                res = false;
            }

            return res;
        }
    }
}
