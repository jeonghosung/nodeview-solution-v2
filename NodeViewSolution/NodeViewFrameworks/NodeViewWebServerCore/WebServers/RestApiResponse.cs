﻿using System;
using System.Text;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Net;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server;
using Semsol.Grapevine.Shared;
using HttpStatusCode = Semsol.Grapevine.Shared.HttpStatusCode;

namespace NodeView.WebServers
{
    public class  RestApiResponse
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static bool SendResponse(IHttpContext context, HttpStatusCode responseCode, JToken response)
        {
            bool res = false;
            try
            {
                context.Response.Headers["Access-Control-Allow-Origin"] = "*";
                WebServiceUtils.AddNoCacheHeader(context);
                WebServiceUtils.SetJsonHeader(context);

                
                context.Response.SendResponse(responseCode, response?.ToString()??"", Encoding.UTF8, ContentType.JSON);
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "SendResponse!");
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }
            return res;
        }

        public static bool SendResponseSuccess(IHttpContext context, JToken response)
        {
            return SendResponse(context, HttpStatusCode.Ok, response);
        }

        public static bool SendResponseFailure(IHttpContext context, string error = "")
        {
            JObject errorObject = new JObject
            {
                ["error"] = string.IsNullOrWhiteSpace(error) ? "failed" : error
            };
            return SendResponse(context, HttpStatusCode.Forbidden, errorObject);
        }

        public static bool SendResponseInternalServerError(IHttpContext context, string error = "")
        {
            JObject errorObject = new JObject
            {
                ["error"] = string.IsNullOrWhiteSpace(error) ? "internal server error" : error
            };
            return SendResponse(context, HttpStatusCode.InternalServerError, errorObject);
        }

        public static bool SendResponseBadRequest(IHttpContext context, string error)
        {
            JObject errorObject = new JObject
            {
                ["error"] = string.IsNullOrWhiteSpace(error) ? "BadRequest" : error
            };

            return SendResponse(context, HttpStatusCode.BadRequest, errorObject);
        }

        public static void SendResponse(IHttpContext context, ApiResponse response)
        {
            if (!response.IsSuccess)
            {
                JObject errorObject = new JObject
                {
                    ["error"] = string.IsNullOrWhiteSpace(response.Message) ? GetErrorMessage(response.ResponseCode) : response.Message
                };
                SendResponse(context, (Semsol.Grapevine.Shared.HttpStatusCode)response.ResponseCode, errorObject);
                return;
            }

            JObject messageObject = new JObject
            {
                ["message"] = string.IsNullOrWhiteSpace(response.Message) ? GetErrorMessage(response.ResponseCode) : response.Message
            };
            SendResponse(context, (Semsol.Grapevine.Shared.HttpStatusCode)response.ResponseCode, null);
        }

        private static string GetErrorMessage(System.Net.HttpStatusCode responseCode)
        {
            //Todo: 메세지 확장해야 한다.
            string message = $"{responseCode}";
            switch (responseCode)
            {
                case System.Net.HttpStatusCode.Continue:
                    break;
                case System.Net.HttpStatusCode.SwitchingProtocols:
                    break;
                case System.Net.HttpStatusCode.OK:
                    break;
                case System.Net.HttpStatusCode.Created:
                    break;
                case System.Net.HttpStatusCode.Accepted:
                    break;
                case System.Net.HttpStatusCode.NonAuthoritativeInformation:
                    break;
                case System.Net.HttpStatusCode.NoContent:
                    break;
                case System.Net.HttpStatusCode.ResetContent:
                    break;
                case System.Net.HttpStatusCode.PartialContent:
                    break;
                case System.Net.HttpStatusCode.MultipleChoices:
                    break;
                case System.Net.HttpStatusCode.MovedPermanently:
                    break;
                case System.Net.HttpStatusCode.Found:
                    break;
                case System.Net.HttpStatusCode.SeeOther:
                    break;
                case System.Net.HttpStatusCode.NotModified:
                    break;
                case System.Net.HttpStatusCode.UseProxy:
                    break;
                case System.Net.HttpStatusCode.Unused:
                    break;
                case System.Net.HttpStatusCode.TemporaryRedirect:
                    break;
                case System.Net.HttpStatusCode.BadRequest:
                    message = "잘못된 요청입니다.";
                    break;
                case System.Net.HttpStatusCode.Unauthorized:
                    message = "로그인이 필요합니다.";
                    break;
                case System.Net.HttpStatusCode.PaymentRequired:
                    break;
                case System.Net.HttpStatusCode.Forbidden:
                    message = "접근 권한이 없습니다.";
                    break;
                case System.Net.HttpStatusCode.NotFound:
                    message = "관련정보를 찾을 수없습니다.";
                    break;
                case System.Net.HttpStatusCode.MethodNotAllowed:
                    break;
                case System.Net.HttpStatusCode.NotAcceptable:
                    break;
                case System.Net.HttpStatusCode.ProxyAuthenticationRequired:
                    break;
                case System.Net.HttpStatusCode.RequestTimeout:
                    break;
                case System.Net.HttpStatusCode.Conflict:
                    break;
                case System.Net.HttpStatusCode.Gone:
                    break;
                case System.Net.HttpStatusCode.LengthRequired:
                    break;
                case System.Net.HttpStatusCode.PreconditionFailed:
                    break;
                case System.Net.HttpStatusCode.RequestEntityTooLarge:
                    break;
                case System.Net.HttpStatusCode.RequestUriTooLong:
                    break;
                case System.Net.HttpStatusCode.UnsupportedMediaType:
                    break;
                case System.Net.HttpStatusCode.RequestedRangeNotSatisfiable:
                    break;
                case System.Net.HttpStatusCode.ExpectationFailed:
                    break;
                case System.Net.HttpStatusCode.UpgradeRequired:
                    break;
                case System.Net.HttpStatusCode.InternalServerError:
                    break;
                case System.Net.HttpStatusCode.NotImplemented:
                    break;
                case System.Net.HttpStatusCode.BadGateway:
                    break;
                case System.Net.HttpStatusCode.ServiceUnavailable:
                    break;
                case System.Net.HttpStatusCode.GatewayTimeout:
                    break;
                case System.Net.HttpStatusCode.HttpVersionNotSupported:
                    break;
            }
            return message;
        }
    }
}
