﻿using System;
using System.Collections.Generic;
using System.IO;
using NLog;
using NodeView.IO;
using RazorEngine;
using RazorEngine.Configuration;
using RazorEngine.Templating;

namespace NodeView.WebServers
{
    public static class TemplateManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        static IRazorEngineService Service { get; set; }
        private static TemplateServiceConfiguration Configuration { get; set; }
        private static readonly object LockObject = new object();
        public static string TemplatesFolder { get; private set; }
        private static readonly FolderWatcher TemplateFolderWatcher = null;
        private static readonly Dictionary<string, RazorSource> RazorSourceDictionary = new Dictionary<string, RazorSource>();
        private static readonly Dictionary<string, FileSource> FileSourceDictionary =  new Dictionary<string, FileSource>();

        static TemplateManager()
        {
            Cleanup();
            Configuration = new TemplateServiceConfiguration();
            Service = RazorEngineService.Create(Configuration);
            Engine.Razor = Service;
            TemplatesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Templates");
            TemplateFolderWatcher =  new FolderWatcher(TemplatesFolder, true);
            
        }


        /// <summary>
        /// Resets the cache.
        /// </summary>
        public static void ResetCache()
        {
            //이것도 동작 안함
            Configuration.CachingProvider = new DefaultCachingProvider();
            Cleanup();
        }
        public static void Cleanup()
        {
            string tempPath = Path.GetTempPath();
            DirectoryInfo di = new DirectoryInfo(Path.GetTempPath());

            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                if (dir.Name.StartsWith("RazorEngine_"))
                {
                    try
                    {
                        dir.Delete(true);
                    }
                    catch
                    {
                        //pass
                    }
                }
            }
        }

        public static string Render(string templateKey)
        {
            string dummyString = "Semsol";
            return Render(templateKey, dummyString);
        }

        public static string Render<T>(string templateKey, T anonymousType)
        {
            try
            {
                DateTime folderUpdatedTime = TemplateFolderWatcher.UpdatedTime;
                RazorSource source = null;
                lock (LockObject)
                {
                    string file = $"Templates/{templateKey}.cshtml";
                    string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file);

                    if (RazorSourceDictionary.TryGetValue(templateKey, out source))
                    {
                        source.CheckUpdate(folderUpdatedTime);
                        if (!source.Exist)
                        {
                            RazorSourceDictionary.Remove(templateKey);
                        }
                    }
                    else
                    {
                        source = new RazorSource(fullPath, folderUpdatedTime);
                        if (source.Exist)
                        {
                            RazorSourceDictionary[templateKey] = source;
                        }
                    }

                    if (source.Exist)
                    {
                        if (!Service.IsTemplateCached(source.Key, anonymousType.GetType()))
                        {
                            Engine.Razor.Compile(new LoadedTemplateSource(source.Template, source.FilePath), source.Key,
                                anonymousType.GetType());
                        }
                    }
                    else
                    {
                        return "";
                    }
                }

                return Engine.Razor.Run(source.Key, anonymousType.GetType(), anonymousType);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot load template!");
            }
            return "";
        }

        public static bool Exist(string templateKey)
        {
            string file = $"Templates/{templateKey}.cshtml";
            string fullPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file);
            return File.Exists(fullPath);
        }
    }
}
