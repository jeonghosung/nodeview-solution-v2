﻿using System.IO;
using NodeView.Utils;

namespace NodeView.WebServers
{
    internal class FileSource
    {
        public string FilePath { get; }
        public string Key { get; }
        public string Text { get; }
        public bool Exist => !string.IsNullOrWhiteSpace(Key);
        public FileSource(string filePath)
        {
            FilePath = filePath;
            if (!File.Exists(filePath))
            {
                Key = "";
                Text = "";
            }
            else
            {
                Key = StringUtils.GetIso8601ExString(File.GetLastWriteTime(filePath));
                Text = File.ReadAllText(filePath);
            }
        }
    }
}
