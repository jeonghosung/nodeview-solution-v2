﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using NLog;

namespace NodeView.WebServers
{
    internal class RazorSource
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly Regex UsingRegex = new Regex(@"^@using\s+[^\r\n]*\r?$", RegexOptions.Compiled|RegexOptions.Multiline);
        private static readonly Regex IncludeRegex = new Regex(@"@Include\s*\(\s*""(?<subContent>[a-zA-Z0-9._\-/]+)""\s*\)");

        private DateTime _folderUpdatedTime;
        public string FilePath { get; }
        public string Key { get; private set; }
        public string Template { get; private set; }
        public bool Exist => !string.IsNullOrWhiteSpace(Template);

        public RazorSource(string filePath, DateTime folderUpdatedTime)
        {
            FilePath = filePath;
            _folderUpdatedTime = folderUpdatedTime;
            Key = Guid.NewGuid().ToString();
            Template = BuildTemplate(filePath);
        }

        private string BuildTemplate(string filePath, int depth = 0)
        {
            if (!File.Exists(filePath))
            {
                return "";
            }
            StringBuilder templateBuilder = new StringBuilder(File.ReadAllText(filePath));
            string folder = Path.GetDirectoryName(filePath) ?? AppDomain.CurrentDomain.BaseDirectory;

            if (depth > 0)
            {
                foreach (Match match in UsingRegex.Matches(templateBuilder.ToString()))
                {
                    string matchString = match.Value;
                    //Logger.Info(matchString);
                    templateBuilder.Replace(matchString, "");
                }
            }

            foreach (Match match in IncludeRegex.Matches(templateBuilder.ToString()))
            {
                string templateName = match.Groups["subContent"].Value;
                string matchString = match.Value;
                string  subContent = BuildTemplate(Path.Combine(folder, $"{templateName}.cshtml"), depth + 1);
                templateBuilder.Replace(matchString, subContent);
            }

            return templateBuilder.ToString();
        }

        public void CheckUpdate(DateTime folderUpdatedTime)
        {
            if (_folderUpdatedTime != folderUpdatedTime)
            {
                _folderUpdatedTime = folderUpdatedTime;
                Key = Guid.NewGuid().ToString();
                Template = BuildTemplate(FilePath);
            }
        }
    }
}
