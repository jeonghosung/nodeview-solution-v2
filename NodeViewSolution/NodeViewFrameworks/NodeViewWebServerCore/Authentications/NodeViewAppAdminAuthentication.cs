﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;
using Semsol.Grapevine.Interfaces.Server;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;

namespace NodeView.Authentications
{
    public class NodeViewAppAdminAuthentication : IAuthentication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static readonly string _cachedPasswordFileName;
        private static string _cachedPassword = "";
        private static List<AccessInfo> _accessInfoList = new List<AccessInfo>();

        internal class AccessInfo
        {
            public DateTime LastAccessedTime { get; set; }
            public string AccessToken { get; set; }
            public string UserToken { get; set; }

            public AccessInfo(string userToken)
            {
                AccessToken = Uuid.NewUniqueKey;
                UserToken = userToken ?? "";
                LastAccessedTime = CachedDateTime.Now;
            }

            public bool HasExpired()
            {
                return ((CachedDateTime.Now - LastAccessedTime).TotalMinutes > 60);
            }
            public void UpdateLastAccessedTime()
            {
                LastAccessedTime = CachedDateTime.Now;
            }
        }

        static NodeViewAppAdminAuthentication()
        {
            string cachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(cachesFolder);
            _cachedPasswordFileName = Path.Combine(cachesFolder, "cachedPassword.pw");
            if (File.Exists(_cachedPasswordFileName))
            {
                _cachedPassword = File.ReadAllText(_cachedPasswordFileName).Trim();
            }
        }

        public bool Login(IHttpContext context)
        {
            try
            {
                KeyValueCollection keyValues = KeyValueCollection.CreateFrom(context.Request.Headers);
                string userToken = keyValues.GetStringValue("userToken");
                string password = "";
                if (context.Request.HttpMethod == HttpMethod.POST)
                {
                    JObject requestJson = JObject.Parse(context.Request.Payload);
                    password = JsonUtils.GetStringValue(requestJson, "password");
                }

                if (string.IsNullOrWhiteSpace(userToken) || string.IsNullOrWhiteSpace(password))
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(_cachedPassword))
                {
                    if (password != "1234")
                    {
                        return false;
                    }
                }
                else
                {
                    if (_cachedPassword != CryptUtils.CreateMd5HexString(password))
                    {
                        return false;
                    }
                }

                foreach (var registeredAccessInfo in _accessInfoList.ToArray())
                {
                    if (registeredAccessInfo.UserToken == userToken)
                    {
                        _accessInfoList.Remove(registeredAccessInfo);
                    }
                }

                var accessInfo = new AccessInfo(userToken);
                _accessInfoList.Add(accessInfo);
                context.Response.Headers.Add("accessToken", accessInfo.AccessToken);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Login:");
            }

            return false;
        }

        public void Logout(IHttpContext context)
        {
            try
            {
                context.Response.Headers.Remove("accessToken");
                KeyValueCollection keyValues = KeyValueCollection.CreateFrom(context.Request.Headers);
                string userToken = keyValues.GetStringValue("userToken");

                if (string.IsNullOrWhiteSpace(userToken)) return;

                foreach (var registeredAccessInfo in _accessInfoList.ToArray())
                {
                    if (registeredAccessInfo.UserToken == userToken)
                    {
                        _accessInfoList.Remove(registeredAccessInfo);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Logout:");
            }
        }
        public bool ValidateAccessToken(IHttpContext context)
        {
            //Todo: 한시적으로 무조건 Tree로 반환함
            return true;
            try
            {
                KeyValueCollection keyValues = KeyValueCollection.CreateFrom(context.Request.Headers);
                string userToken = keyValues.GetStringValue("userToken");
                string accessToken = keyValues.GetStringValue("accessToken");

                if (string.IsNullOrWhiteSpace(userToken) || string.IsNullOrWhiteSpace(accessToken))
                {
                    return false;
                }

                foreach (var registeredAccessInfo in _accessInfoList.ToArray())
                {
                    if (registeredAccessInfo.HasExpired())
                    {
                        _accessInfoList.Remove(registeredAccessInfo);
                        continue;
                    }
                    if (registeredAccessInfo.AccessToken == accessToken)
                    {
                        if (registeredAccessInfo.UserToken == userToken)
                        {
                            registeredAccessInfo.UpdateLastAccessedTime();
                            return true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ValidateAccessToken:");
            }

            return false;
        }

        public bool ResetPassword(IHttpContext context)
        {
            try
            {
                if (!ValidateAccessToken(context)) return false;
                if (string.IsNullOrWhiteSpace(_cachedPassword)) return true;

                File.Delete(_cachedPasswordFileName);
                _cachedPassword = "";
                _accessInfoList.Clear();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ResetPassword:");
            }

            return false;
        }

        public bool ChangePassword(IHttpContext context)
        {
            try
            {
                if (!ValidateAccessToken(context)) return false;

                string newPassword = "";
                string currentPassword = "";
                if (context.Request.HttpMethod == HttpMethod.POST)
                {
                    JObject requestJson = JObject.Parse(context.Request.Payload);
                    newPassword = JsonUtils.GetStringValue(requestJson, "newpassword");
                    currentPassword = JsonUtils.GetStringValue(requestJson, "currentpassword");
                }

                if (string.IsNullOrWhiteSpace(newPassword) || string.IsNullOrWhiteSpace(currentPassword))
                {
                    return false;
                }

                string encryptNewPassword = CryptUtils.CreateMd5HexString(newPassword);
                string encryptCurrentPassword = CryptUtils.CreateMd5HexString(currentPassword);
                if (string.IsNullOrWhiteSpace(_cachedPassword) && !currentPassword.Equals("1234"))
                {
                    return false;
                }
                if (!string.IsNullOrWhiteSpace(_cachedPassword) && !encryptCurrentPassword.Equals(_cachedPassword, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
                if (_cachedPassword == encryptNewPassword)
                {
                    return true;
                }

                _cachedPassword = encryptNewPassword;
                File.WriteAllText(_cachedPasswordFileName, _cachedPassword);
                _accessInfoList.Clear();
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ChangePassword:");
            }

            return false;
        }
    }
}
