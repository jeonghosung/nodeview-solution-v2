﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Semsol.Grapevine.Interfaces.Server;

namespace NodeView.Authentications
{
    public interface IAuthentication
    {
        bool Login(IHttpContext context);
        void Logout(IHttpContext context);
        bool ResetPassword(IHttpContext context);
        bool ChangePassword(IHttpContext context);
        bool ValidateAccessToken(IHttpContext context);
    }
}
