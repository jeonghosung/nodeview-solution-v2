﻿using NLog;
using NodeView.Authentications;
using NodeView.Ioc;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using HttpMethod = Semsol.Grapevine.Shared.HttpMethod;
using RestApiResponse = NodeView.WebServers.RestApiResponse;

namespace NodeView.WebServices
{
    [RestResource(BasePath = "/api/v1/auth")]
    public class AuthApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static IAuthentication _authentication = null;

        public AuthApiService()
        {
            try
            {
                _authentication = (IAuthentication)NodeViewContainerExtension.GetContainerProvider()
                    ?.Resolve(typeof(IAuthentication));
            }
            catch
            {
                //Logger.Info("Not supported IAuthentication.");
                _authentication = null;
            }
        }

        private static bool IsSupported(IHttpContext context)
        {
            if (_authentication == null)
            {
                return false;
            }
            return true;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/login")]
        public IHttpContext Login(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                if (_authentication.Login(context))
                {
                    RestApiResponse.SendResponseSuccess(context, $"Success");
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "cannot login.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot Login!");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.ALL, PathInfo = @"/logout")]
        public IHttpContext Logout(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                if (!_authentication.ValidateAccessToken(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                    return context;
                }

                _authentication.Logout(context);
                RestApiResponse.SendResponseSuccess(context, $"Success");
            }
            catch (Exception e)
            {
                Logger.Error(e, "Logout:");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.ALL, PathInfo = @"/resetPassword")]
        public IHttpContext ResetPassword(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                if (!_authentication.ValidateAccessToken(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                    return context;
                }

                if (_authentication.ResetPassword(context))
                {
                    RestApiResponse.SendResponseSuccess(context, $"Success");
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, $"cannot resetPassword.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ResetPassword:");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.ALL, PathInfo = @"/changePassword")]
        public IHttpContext ChangePassword(IHttpContext context)
        {
            try
            {
                if (!IsSupported(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.NotImplemented, "");
                    return context;
                }

                if (!_authentication.ValidateAccessToken(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                    return context;
                }

                if (_authentication.ChangePassword(context))
                {
                    RestApiResponse.SendResponseSuccess(context, $"Success");
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, $"cannot changePassword.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ChangePassword:");
                RestApiResponse.SendResponseInternalServerError(context);
            }

            return context;
        }
    }
}
