﻿using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.NodeViews;
using NodeView.Protocols.NodeViews;
using System;
using System.Threading.Tasks;

namespace NodeViewIotStation
{
    public class IotStationService : StationServiceManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private NodeViewProcessManagerClient _processManagerClient;

        #region SingleTone
        private static readonly Lazy<IotStationService> Lazy = new Lazy<IotStationService>(() => new IotStationService());
        public static IotStationService Instance => Lazy.Value;
        #endregion
        protected override bool OnInitializing(Configuration config)
        {
            int processManagerPort = Config.GetValue("processManager.port", 20102);
            _processManagerClient = new NodeViewProcessManagerClient("127.0.0.1", processManagerPort);
            return true;
        }
        protected override void OnInitialized()
        {
            base.OnInitialized();

            Task.Factory.StartNew(() =>
            {
                _processManagerClient.RegisterApp(ProcessType.Service, AppUtils.GetAppName(), ConfigFilename, "");
            });
        }
    }
}
