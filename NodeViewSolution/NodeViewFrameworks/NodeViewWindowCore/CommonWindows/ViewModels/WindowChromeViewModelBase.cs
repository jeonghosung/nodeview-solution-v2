﻿using System.Windows;
using Prism.Commands;
using Prism.Mvvm;

namespace NodeView.CommonWindows.ViewModels
{
    public class WindowChromeViewModelBase : BindableBase
    {
        private WindowState _windowState = WindowState.Normal;
        private string _captionButtonForeground = "White";
        private string _windowIconSource = "../app.png";
        private Visibility _windowIconVisibility = Visibility.Visible;

        public string WindowIconSource
        {
            get => _windowIconSource;
            set => SetProperty(ref _windowIconSource, value);
        }

        public Visibility WindowIconVisibility
        {
            get => _windowIconVisibility;
            set => SetProperty(ref _windowIconVisibility, value);
        }

        public WindowState WindowState
        {
            get => _windowState;
            set => SetProperty(ref _windowState, value);
        }

        public string CaptionButtonForeground3
        {
            get => _captionButtonForeground;
            set => SetProperty(ref _captionButtonForeground, value);
        }

        public DelegateCommand<Window> MinimizeCommand { get; set; }
        public DelegateCommand<Window> MaximizeCommand { get; set; }
        public DelegateCommand<Window> RestoreCommand { get; set; }
        public DelegateCommand<Window> CloseCommand { get; set; }

        public WindowChromeViewModelBase()
        {
            MinimizeCommand = new DelegateCommand<Window>(ExecuteMinimizeCommand);
            MaximizeCommand = new DelegateCommand<Window>(ExecuteMaximizeCommand);
            RestoreCommand = new DelegateCommand<Window>(ExecuteRestoreCommand);
            CloseCommand = new DelegateCommand<Window>(ExecuteCloseCommand);
        }

        private void ExecuteCloseCommand(Window window)
        {
            SystemCommands.CloseWindow(window);
        }

        private void ExecuteRestoreCommand(Window window)
        {
            SystemCommands.RestoreWindow(window);
            WindowState = WindowState.Normal;
        }

        private void ExecuteMaximizeCommand(Window window)
        {
            SystemCommands.MaximizeWindow(window);
            WindowState = WindowState.Maximized;
        }

        private void ExecuteMinimizeCommand(Window window)
        {
            WindowState = WindowState.Minimized;
            SystemCommands.MinimizeWindow(window);
        }
    }
}
