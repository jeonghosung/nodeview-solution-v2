﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NLog;
using NodeView.ViewModels;
using NodeViewWindowCore.ViewModels;
using Prism.Services.Dialogs;

namespace NodeViewWindowCore.Views
{
    /// <summary>
    /// PtzCameraControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PtzCameraControl : Window
    {
        public PtzCameraControl(ContentItem contentItem, bool isTestMode, bool isVerbose, IDialogService dialogService)
        {
            InitializeComponent();
            this.DataContext = new PtzCameraControlViewModel(contentItem, isTestMode, isVerbose, dialogService);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;

            if (listBox?.SelectedItem != null)
            {
                listBox.Dispatcher.Invoke(() =>
                {
                    listBox.UpdateLayout();
                    listBox.ScrollIntoView(listBox.SelectedItem);
                });
            }
        }
        //Test Code
        private void Ellipse_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            int xRange = 0;
            int yRange = 0;
            Point p = Mouse.GetPosition(canvas);
            xRange = (int)((p.X - canvas.ActualWidth / 2) * 0.0416);
            yRange = -(int)((canvas.ActualHeight / 2 - p.Y) * 0.0416);
            if (this.DataContext is PtzCameraControlViewModel datacontext)
            {
                datacontext.MoveCamera(xRange, yRange);
            }
        }
    }
}
