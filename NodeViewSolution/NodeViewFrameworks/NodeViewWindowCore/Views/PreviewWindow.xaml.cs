﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using NodeView.ViewModels;

namespace NodeViewWindowCore.Views
{
    /// <summary>
    /// PreviewWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PreviewWindow : Window
    {
        private readonly ContentItem _content;
        public PreviewWindow()
        {
            InitializeComponent();
        }

        public PreviewWindow(ContentItem content)
        {
            InitializeComponent();
            _content = content;
           
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ContentPresenter.Source = _content;
            Title = _content.Name;
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
