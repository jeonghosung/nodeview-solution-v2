﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using NLog;

namespace NodeView.Tasks
{
    public class ThreadAction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static bool IsOnUiThread()
        {
            Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread);
            if (dispatcher != null)
            {
                return true;
            }

            return false;
        }

        public static void RunOnUiThread(Action action)
        {
            try
            {
                Dispatcher dispatcher = Dispatcher.FromThread(Thread.CurrentThread) ?? Application.Current?.Dispatcher;

                if (dispatcher != null)
                {
                    dispatcher.Invoke(action);
                }
                else
                {
                    Logger.Error(" Cannot find any dispatcher.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "RunOnUiThread:");
            }
        }

        public static void PostOnUiThread(Action action, long delay = 0)
        {
            if (delay <= 0)
            {
                Task.Run(() =>
                {
                    RunOnUiThread(action);
                });
            }
            else
            {
                Task.Delay(1000).ContinueWith(t => RunOnUiThread(action));
            }
        }
    }
}
