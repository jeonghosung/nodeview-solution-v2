﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using NLog;

namespace NodeView.Controls
{
    public class ValidatedTextBox :TextBox
    {
        public static readonly DependencyProperty ValidationRuleProperty =
            DependencyProperty.Register("ValidationRule", typeof(string), typeof(ValidatedTextBox));
        public static readonly DependencyProperty DefaultValueProperty =
            DependencyProperty.Register("DefaultValue", typeof(string), typeof(ValidatedTextBox));

        public static readonly DependencyProperty IsValidProperty =
            DependencyProperty.Register("IsValid", typeof(bool), typeof(ValidatedTextBox));

        public static readonly DependencyProperty IsModifiedProperty =
            DependencyProperty.Register("IsModified", typeof(bool), typeof(ValidatedTextBox));
        
        public ValidatedTextBox()
        {

        }
        public string ValidationRule
        {
            get => (string)GetValue(ValidationRuleProperty);
            set => SetValue(ValidationRuleProperty, value);
        }
        public string DefaultValue
        {
            get => (string)GetValue(DefaultValueProperty);
            set => SetValue(DefaultValueProperty, value);
        }
        public bool IsValid
        {
            get => (bool)GetValue(IsValidProperty);
            set => SetValue(IsValidProperty, value);
        }

        public bool IsModified
        {
            get => (bool)GetValue(IsModifiedProperty);
            set => SetValue(IsModifiedProperty, value);
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            base.OnTextChanged(e);
            CheckValidationAndModified(Text);
        }
        private void CheckValidationAndModified(string input)
        {
            //IsModified = input != DefaultValue;
            string validationRule = ValidationRule;
            if (IsReadOnly || string.IsNullOrWhiteSpace(validationRule) || string.IsNullOrWhiteSpace(input))
            {
                return;
            }

            IsValid = Regex.IsMatch(input, validationRule);
         
        }
    }
}
