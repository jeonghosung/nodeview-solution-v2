﻿using System.Windows;
using System.Windows.Controls;
using NodeView.DataWalls;
using NodeView.Frameworks.ContentControls;
using NodeView.Frameworks.Widgets;
using NodeView.Ioc;

namespace NodeView.Controls
{
    /// <summary>
    /// ContentControlPresenter.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ContentControlPresenter : UserControl
    {
        public ContentControlPresenter()
        {
            InitializeComponent();
        }
        public IContentSource Source
        {
            get => (IContentSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IContentSource), typeof(ContentControlPresenter), new PropertyMetadata(OnContentSourcePropertyChanged));

        private static void OnContentSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is ContentControlPresenter owner)
            {
                if (e.NewValue is IDataWallContent dataWallContent)
                {

                    var control = NodeViewContainerExtension.GetContainerProvider()?.ResolveDataWallContentControl<Control>(dataWallContent);
                    if (control != null)
                    {
                        owner.ContentPresenter.Content = control;
                    }
                }
                else if (e.NewValue is IWidgetSource widgetSource)
                {

                    var control = WidgetControlManager.CreateWidgetControl(widgetSource);
                    if (control != null)
                    {
                        owner.ContentPresenter.Content = control;
                    }
                }
                else if (e.NewValue == null)
                {
                    owner.ContentPresenter.Content = null;
                }
                else
                {
                    owner.ContentPresenter.Content = e.NewValue.ToString();
                }
            }
        }
    }
}
