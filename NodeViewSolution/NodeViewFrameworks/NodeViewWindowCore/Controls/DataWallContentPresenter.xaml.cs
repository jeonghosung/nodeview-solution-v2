﻿using System.Windows;
using System.Windows.Controls;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;
using NodeView.Ioc;

namespace NodeView.Controls
{
    /// <summary>
    /// DataWallContentPresenter.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataWallContentPresenter : UserControl,  IDataWallContentControl
    {
        public DataWallContentPresenter()
        {
            InitializeComponent();
        }

        public IDataWallContent Source
        {
            get => (IDataWallContent)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IDataWallContent), typeof(DataWallContentPresenter), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is DataWallContentPresenter owner)
            {
                if (e.NewValue is IDataWallContent dataWallContent)
                {

                    var control = NodeViewContainerExtension.GetContainerProvider()?.ResolveDataWallContentControl<Control>(dataWallContent);
                    if (control != null)
                    {
                        owner.ContentPresenter.Content = control;
                    }
                }
                else
                {
                    if (e.NewValue == null)
                    {
                        owner.ContentPresenter.Content = null;
                    }
                    else
                    {
                        owner.ContentPresenter.Content = e.NewValue.ToString();
                    }
                }
            }
        }
    }
}
