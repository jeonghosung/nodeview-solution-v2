﻿using System.Windows;
using System.Windows.Controls;
using NodeView.Frameworks.Widgets;
using NodeView.Ioc;

namespace NodeView.Controls
{
    /// <summary>
    /// WidgetPresenter.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class WidgetPresenter : UserControl, IWidgetControl
    {
        public WidgetPresenter()
        {
            InitializeComponent();
        }

        public IWidgetSource Source
        {
            get => (IWidgetSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IWidgetSource), typeof(WidgetPresenter), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d is WidgetPresenter owner)
            {
                if (e.NewValue is IWidgetSource widgetSource)
                {

                    var control = WidgetControlManager.CreateWidgetControl(widgetSource);
                    if (control != null)
                    {
                        owner.ContentPresenter.Content = control;
                    }
                }
                else
                {
                    owner.ContentPresenter.Content = e.NewValue.ToString();
                }
            }
        }
    }
}
