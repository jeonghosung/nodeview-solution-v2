﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace NodeView.Controls
{
    public class NodeViewPasswordBox : TextBox
    {
        private string _maskingString;
        private Regex _passWordRegex = new Regex(@"^[a-zA-Z0-9#?!@$%^&*-]");

        public static readonly DependencyProperty PassWordCharProperty =
            DependencyProperty.Register("PassWordChar", typeof(string), typeof(NodeViewPasswordBox), new PropertyMetadata("*"));
        public static readonly DependencyProperty PasswordProperty =
            DependencyProperty.Register("Password", typeof(string), typeof(NodeViewPasswordBox), new PropertyMetadata("string.Empty"));
        public static readonly DependencyProperty UsePasswordProperty =
            DependencyProperty.Register("UsePassword", typeof(bool), typeof(NodeViewPasswordBox), new PropertyMetadata(true));

        public string PassWordChar
        {
            get => (string)GetValue(PassWordCharProperty);
            set => SetValue(PassWordCharProperty, value);
        }
        public string Password
        {
            get => (string)GetValue(PasswordProperty);
            set => SetValue(PasswordProperty, value);
        }
        public bool UsePassword
        {
            get => (bool)GetValue(UsePasswordProperty);
            set => SetValue(UsePasswordProperty, value);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            if (e.Property == UsePasswordProperty)
            {
                Text = (bool)e.NewValue ? _maskingString : Password;
            }
            else if (e.Property == PasswordProperty)
            {
                if (string.IsNullOrWhiteSpace(Password))
                {
                    Text = _maskingString = string.Empty;
                }
            }
            base.OnPropertyChanged(e);
        }
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
            base.OnPreviewKeyDown(e);
        }
        protected override void OnPreviewTextInput(TextCompositionEventArgs e)
        {
            if (_passWordRegex.IsMatch(e.Text ?? string.Empty))
            {
                var currentSelectionStart = SelectionStart;
                if (!string.IsNullOrWhiteSpace(Password))
                {
                    currentSelectionStart = Math.Min(Password.Length, currentSelectionStart);
                }
                if (SelectionLength > 0)
                {
                    Password = Password.Remove(SelectionStart, SelectionLength);
                    _maskingString = _maskingString.Remove(SelectionStart, SelectionLength);
                }
                Password = string.IsNullOrWhiteSpace(Password) ? e.Text : Password.Insert(currentSelectionStart, e.Text ?? string.Empty);
                _maskingString += PassWordChar;
                Text = (UsePassword ? _maskingString : Password) ?? string.Empty;
                Select(currentSelectionStart + 1, 0);
            }
            e.Handled = true;

        }
        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (e.Changes.Count > 0)
            {
                foreach (var textChange in e.Changes)
                {
                    if (textChange.AddedLength == 0 && textChange.RemovedLength > 0)
                    {
                        if (string.IsNullOrWhiteSpace(Password))
                        {
                            return;
                        }

                        if (string.IsNullOrWhiteSpace(Password)) continue;
                        if(textChange.Offset < 0) continue;
                        Password = Password.Remove(textChange.Offset, textChange.RemovedLength); 
                        //_maskingString = _maskingString.Remove(textChange.Offset, textChange.RemovedLength);
                    }
                }
            }
        }
    }
}
