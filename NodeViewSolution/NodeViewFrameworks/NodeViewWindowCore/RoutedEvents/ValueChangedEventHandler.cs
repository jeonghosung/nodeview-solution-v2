﻿using System.Windows;

namespace NodeView.RoutedEvents
{
    public delegate void ValueChangedEventHandler(object sender, ValueChangedEventArgs e);
    public class ValueChangedEventArgs : RoutedEventArgs
    {
        public ValueChangedEventArgs(RoutedEvent routedEvent, object source, int oldValue, int newValue)
            : base(routedEvent, source)
        {
            OldValue = oldValue;
            NewValue = newValue;
        }
        public int OldValue { get; private set; }
        public int NewValue { get; private set; }
    }
}
