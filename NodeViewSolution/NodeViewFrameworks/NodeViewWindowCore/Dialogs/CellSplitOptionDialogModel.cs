﻿using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class CellSplitOptionDialogModel : DialogModelBase
    {
        private int _rows = 2;
        public int Rows
        {
            get => _rows;
            set => SetProperty(ref _rows, value);
        }

        private int _columns = 2;
        public int Columns
        {
            get => _columns;
            set => SetProperty(ref _columns, value);
        }

        public CellSplitOptionDialogModel()
        {
            Title = "셀 분할 옵션";
        }
        protected override void ExecuteApplyDialogCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("rows", Rows);
            parameters.Add("columns", Columns);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("rows", out int rows))
            {
                Rows = rows;
            }
            if (parameters.TryGetValue("columns", out int columns))
            {
                Columns = columns;
            }
        }
    }
}
