﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.ViewModels;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class AddPropertyDialogViewModel : DialogModelBase
    {
        private EditingField _idField;
        private EditingField _valueField;
        private EditingField _descriptionField;
        public EditingField IdField
        {
            get => _idField;
            set => SetProperty(ref _idField, value);
        }
        public EditingField ValueField
        {
            get => _valueField;
            set => SetProperty(ref _valueField, value);
        }
        public EditingField DescriptionField
        {
            get => _descriptionField;
            set => SetProperty(ref _descriptionField, value);
        }
        protected override void ExecuteApplyDialogCommand()
        {
            var parameters = GetResultParameters();
            parameters.Add("id", IdField.Value);
            parameters.Add("value", ValueField.Value);
            parameters.Add("description", DescriptionField.Value);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            IdField = new EditingField("Id", "Id", "", "id");
            ValueField = new EditingField("Value", "Value", "", "string");
            DescriptionField = new EditingField("Description", "Description", "", "string");
            base.OnDialogOpened(parameters);
            Title = "프로퍼티 추가";
        }
    }
}
