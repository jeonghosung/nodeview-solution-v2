﻿using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NodeView.Drawing;
using NodeView.ViewModels;
using System.IO;
using NodeViewCore.Utils;

namespace NodeView.Dialogs
{
    public class GetPopupRectDialogModel : DialogModelBase
    {
        private CacheManager _cacheManager;
        private EditingField _xField;
        private EditingField _yField;
        private EditingField _widthField;
        private EditingField _heightField;
        private string _errorMessage;
        private Regex _regex = new Regex(@"^[0-9]");

        public EditingField XField
        {
            get => _xField;
            set => SetProperty(ref _xField, value);
        }
        public EditingField YField
        {
            get => _yField;
            set => SetProperty(ref _yField, value);
        }
        public EditingField WidthField
        {
            get => _widthField;
            set => SetProperty(ref _widthField, value);
        }
        public EditingField HeightField
        {
            get => _heightField;
            set => SetProperty(ref _heightField, value);
        }
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            _cacheManager = new CacheManager(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches", "cache.json"));
            var popupRect = _cacheManager.GetValue<IntRect>("popup", new IntRect(0,0,960,540));
            base.OnDialogOpened(parameters);
            XField = new EditingField("X", popupRect.X.ToString(), "", "int");
            YField = new EditingField("Y", popupRect.Y.ToString(), "", "int");
            WidthField = new EditingField("Width", popupRect.Width.ToString(), "", "int");
            HeightField = new EditingField("Height", popupRect.Height.ToString(), "", "int");
            Title = "팝업 생성";
        }
        protected override void ExecuteApplyDialogCommand()
        {
            if (string.IsNullOrWhiteSpace(XField.Value) || string.IsNullOrWhiteSpace(YField.Value) || string.IsNullOrWhiteSpace(WidthField.Value) ||
                string.IsNullOrWhiteSpace(HeightField.Value))
            {
                ErrorMessage = "데이터를 입력해주세요.";
                return;
            }
            if (!_regex.Match(XField.Value).Success || !_regex.Match(YField.Value).Success || !_regex.Match(WidthField.Value).Success || !_regex.Match(HeightField.Value).Success)
            {
                ErrorMessage = "숫자만 입력 가능합니다.";
                return;
            }
            var parameters = new DialogParameters();
            parameters.Add("popupRect", $"{XField.Value},{YField.Value},{WidthField.Value},{HeightField.Value}");
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }
    }
}
