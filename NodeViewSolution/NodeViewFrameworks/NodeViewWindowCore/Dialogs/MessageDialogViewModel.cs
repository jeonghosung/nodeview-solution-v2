﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class MessageDialogViewModel : DialogModelBase
    {
        private string _message;

        public string Message
        {
            get => _message;
            set => SetProperty(ref _message, value);

        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("title", out string title))
            {
                Title = title;
            }
            if (parameters.TryGetValue("message", out string message))
            {
                Message = message;
            }
            if (string.IsNullOrWhiteSpace(Title))
            {
                Title = "확인창";
            }
            if (string.IsNullOrWhiteSpace(Message))
            {
                Message = "message를 입력하세요.";
            }
        }
    }
}
