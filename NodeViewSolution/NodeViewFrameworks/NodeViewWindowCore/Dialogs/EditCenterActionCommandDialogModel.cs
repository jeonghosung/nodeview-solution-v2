﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Microsoft.Xaml.Behaviors.Media;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class EditCenterActionCommandDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private NodeViewCommandBindable _command;
        private string _errorMessage = "";
        private bool _isCreateNew = true;
        private string _selectedAppId;
        private string _selectedDeviceId;
        private string _selectedCommandId;

        private readonly List<INodeViewAppNode> _apps = new List<INodeViewAppNode>();
        private readonly List<Device> _devices = new List<Device>();
        private readonly List<CommandDescription> _commandDescriptions = new List<CommandDescription>();
        private EditingField[] _parameters;
        private INodeViewAppNode _selectedApp;
        private Device _selectedDevice;

        public string SelectedAppId
        {
            get => _selectedAppId;
            set
            {
                if (_selectedAppId != value)
                {
                    SetProperty(ref _selectedAppId, value);
                    OnChangedSelectedAppId();
                }
            }
        }
        public string SelectedDeviceId
        {
            get => _selectedDeviceId;
            set
            {
                if (_selectedDeviceId != value)
                {
                    SetProperty(ref _selectedDeviceId, value);
                    OnChangedSelectedDeviceId();
                }
            }
        }
        public string SelectedCommandId
        {
            get => _selectedCommandId;
            set
            {
                if (_selectedCommandId != value)
                {
                    SetProperty(ref _selectedCommandId, value);
                    OnChangedSelectedCommandId();
                }
            }
        }
        public EditingField[] Parameters
        {
            get => _parameters ?? new EditingField[0];
            set => SetProperty(ref _parameters, value);
        }

        public string[] AppIds => _apps.Select(x => x.Id).ToArray();
        public string[] DeviceIds => _devices.Select(x => x.Id).ToArray();
        public string[] CommandIds => _commandDescriptions.Select(x => x.Command).ToArray();

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("isCreateNew", out bool isCreateNew))
            {
                _isCreateNew = isCreateNew;
            }
            if (parameters.TryGetValue("apps", out INodeViewAppNode[] apps))
            {
                _apps.AddRange(apps); 
            }
            if (parameters.TryGetValue("command", out NodeViewCommandBindable command))
            {
                _command = NodeViewCommandBindable.CreateFrom(command);
            }
            else
            {
                _isCreateNew = true;
                _command = new NodeViewCommandBindable();
            }

            Title = isCreateNew ? "신규 Command 생성" : "Command 편집";

            SetUpFields();
        }

        private void SetUpFields()
        {
            if (string.IsNullOrWhiteSpace(_command.AppId))
            {
                if (_apps.Count > 0)
                {
                    SelectedAppId = _apps.First().Id;
                }
            }
            else
            {
                SelectedAppId = _command.AppId;
                if (!string.IsNullOrWhiteSpace(_command.DeviceId))
                {
                    SelectedDeviceId = _command.DeviceId;
                }
                if (!string.IsNullOrWhiteSpace(_command.Command))
                {
                    SelectedCommandId = _command.Command;
                }
            }
        }

        private void OnChangedSelectedAppId()
        {
            var app = FindApp(_selectedAppId);
            _selectedApp = app;
            UpdateDevicesFromSelectedApp();
        }
        private void UpdateDevicesFromSelectedApp()
        {
            _devices.Clear();
            _commandDescriptions.Clear();
            Parameters = null;
            var app = _selectedApp;
            if (!string.IsNullOrWhiteSpace(app?.Uri))
            {
                var apiClient = new RestApiClient(app.Uri);
                var response = apiClient.Get("/devices");
                if (response.IsSuccess)
                {
                    foreach (var deviceToken in response.ResponseJsonArray)
                    {
                        var device = Device.CreateFrom(deviceToken.Value<JObject>());
                        if (device != null)
                        {
                            _devices.Add(device);
                        }
                    }
                }
            }
            RaisePropertyChanged("DeviceIds");
            SelectedDeviceId = _devices.Count > 0 ? _devices[0].Id : "";
        }

        private INodeViewAppNode FindApp(string id)
        {
            foreach (var app in _apps)
            {
                if (app.Id == id)
                {
                    return app;
                }
            }

            return null;
        }

        private void OnChangedSelectedDeviceId()
        {
            var device = FindDevice(_selectedDeviceId);
            _selectedDevice = device;
            UpdateCommandsFromSelectedDevice();
        }

        private void UpdateCommandsFromSelectedDevice()
        {
            _commandDescriptions.Clear();
            Parameters = null;
            var device = _selectedDevice;
            if (device != null)
            {
                _commandDescriptions.AddRange(device.CommandDescriptions);
            }
            RaisePropertyChanged("CommandIds");
            SelectedCommandId = _commandDescriptions.Count > 0 ? _commandDescriptions[0].Command : "";
        }

        private Device FindDevice(string id)
        {
            foreach (var device in _devices)
            {
                if (device.Id == id)
                {
                    return device;
                }
            }
            return null;
        }

        private void OnChangedSelectedCommandId()
        {
            Parameters = null;
            var command = FindCommand(_selectedCommandId);
            if (command != null)
            {
                List<EditingField> fields = new List<EditingField>();
                foreach (var parameter in command.Parameters)
                {
                    var field = EditingField.CreateFrom(parameter);
                    field.ValueType = parameter.ValueType;
                    if (_command.Parameters.TryGetValue(field.Id, out var value))
                    {
                        field.Value = $"{value}";
                        field.DefaultValue = field.Value;
                    }
                    fields.Add(field);
                }

                Parameters = fields.ToArray();
            }
        }
        private CommandDescription FindCommand(string id)
        {
            foreach (var command in _commandDescriptions)
            {
                if (command.Command == id)
                {
                    return command;
                }
            }
            return null;
        }

        protected override void ExecuteApplyDialogCommand()
        {
            var command = new NodeViewCommandBindable(SelectedAppId, SelectedDeviceId, SelectedCommandId);
            foreach (var parameter in Parameters)
            {
                command.Parameters[parameter.Id] = parameter.Value;
            }
            var parameters = GetResultParameters();
            parameters.Add("command", command);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }

        private bool CheckParametersValidation()
        {
            List<string> errorParameters = new List<string>();
            foreach (var field in Parameters.ToArray())
            {
                if (!field.IsValid)
                {
                    errorParameters.Add(field.Id);
                }
            }

            if (errorParameters.Count == 0)
            {
                return true;
            }

            ErrorMessage = $"{string.Join(",", errorParameters)} 항목 입력값이 잘못되었습니다.";
            return false;
        }
    }
}
