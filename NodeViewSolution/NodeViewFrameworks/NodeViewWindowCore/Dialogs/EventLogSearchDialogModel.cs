﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Tasks;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class EventLogSearchDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private NodeViewApiClient _nodeViewCenterApiClient = null;
        private string _searchKeyword;
        private bool _isAscending = true;
        private DateTime _searchStartDateTime = DateTime.Today;
        private DateTime _searchEndDateTime = DateTime.Today;
        
        private ObservableCollection<EventLog> _searchList = new ObservableCollection<EventLog>();

        private DelegateCommand _searchCommand;
        private DelegateCommand<KeyEventArgs> _keyUpDelegateCommand;
        public string SearchKeyword
        {
            get => _searchKeyword;
            set => SetProperty(ref _searchKeyword, value);
        }

        public bool IsAscending
        {
            get => _isAscending;
            set
            {
                SetProperty(ref _isAscending, value);
                ChangeSearchListOrder(value);
            }
        }

        private void ChangeSearchListOrder(bool value)
        {
            if (SearchList.Count > 0)
            {
                if (value)
                {
                    SearchList = new ObservableCollection<EventLog>(SearchList.OrderBy(list => list.Timestamp));
                }
                else
                {
                    SearchList = new ObservableCollection<EventLog>(SearchList.OrderByDescending(list => list.Timestamp));
                }
            }
        }

        public DateTime SearchStartDateTime
        {
            get => _searchStartDateTime;
            set => SetProperty(ref _searchStartDateTime, value);
        }

        public DateTime SearchEndDateTime
        {
            get => _searchEndDateTime;
            set => SetProperty(ref _searchEndDateTime, value);
        }

        public ObservableCollection<EventLog> SearchList
        {
            get => _searchList;
            set => SetProperty(ref _searchList, value);
        }
        public DelegateCommand SearchCommand => _searchCommand ?? (_searchCommand = new DelegateCommand(Search));
        public DelegateCommand<KeyEventArgs> KeyUpEventCommand => _keyUpDelegateCommand ?? (_keyUpDelegateCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyUpEvent));
        private void Search()
        {
            ThreadAction.RunOnUiThread(SearchInternal);
        }

        private void SearchInternal()
        {
            var fromDatetime = _searchStartDateTime.ToString("yyyy.MM.dd");
            var toDateTime = _searchEndDateTime.ToString("yyyy.MM.dd");
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            var urlEncodeKey = HttpUtility.UrlEncode(_searchKeyword);
            queryStringDictionary.Add("ascendingOrder", IsAscending ? true : false);
            queryStringDictionary.Add("searchKey", urlEncodeKey);
            queryStringDictionary.Add("fromDate", fromDatetime);
            queryStringDictionary.Add("toDate", toDateTime);
            var response = _nodeViewCenterApiClient.Get("/eventLogs", queryStringDictionary);
            if (response.IsSuccess && response.IsJsonResponse)  
            {
                if (response.ResponseType == RestApiResponseType.JsonArray)
                {
                    SearchList.Clear();
                    var responseArray = response.ResponseJsonArray;
                    foreach (var token in responseArray)
                    {
                        JObject json = token.Value<JObject>();
                        if (json != null)
                        {
                            var eventLog = EventLog.CreateFrom(json);
                            if (eventLog != null)
                            {
                                SearchList.Add(eventLog);
                            }
                        }
                    }
                }
                else
                {
                    Logger.Error("reponse of contents is not an array.");
                }
            }
        }
        private void ExecuteKeyUpEvent(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.Enter:
                    Search();
                    break;

            }
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            
            if (parameters.TryGetValue("title", out string title))
            {
                Title = title;
            }

            if (parameters.TryGetValue("apiclient", out NodeViewApiClient apiclient))
            {
                _nodeViewCenterApiClient = apiclient;
            }
        }
    }
}
