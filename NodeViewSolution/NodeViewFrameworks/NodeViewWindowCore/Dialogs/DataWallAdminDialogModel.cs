﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class DataWallAdminDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public IWidgetSource Source { get; set; }

        public DataWallAdminDialogModel()
        {
            Title = "관리자 설정";
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("widgetSource", out IWidgetSource widgetSource))
            {
                Source = widgetSource;
            }
        }
    }
}
