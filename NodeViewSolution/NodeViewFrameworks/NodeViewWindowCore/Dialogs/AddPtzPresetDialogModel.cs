﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Dialogs;
using NodeView.Net;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NodeViewWindowCore.Dialogs
{
    public class AddPtzPresetDialogModel : DialogModelBase
    {
        private string _errorMessage;
        private string _presetName;
        private int _presetId;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private RestApiClient _dataWallApiClient = null;

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public string PresetName
        {
            get => _presetName;
            set => SetProperty(ref _presetName, value);
        }
        public int PresetId
        {
            get => _presetId;
            set => SetProperty(ref _presetId, value);
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "즐겨찾기 추가";
            if (parameters.TryGetValue("clientBaseUrl", out string clientBaseUrl) && parameters.TryGetValue("id", out string id) && parameters.TryGetValue("password", out string password))
            {
                if (!string.IsNullOrWhiteSpace(clientBaseUrl))
                {
                    SetDataWallApiClient(clientBaseUrl, id, password);
                }
            }
        }
        private bool SetDataWallApiClient(string appUrl, string id, string password)
        {
            if (string.IsNullOrWhiteSpace(appUrl))
            {
                return false;
            }
            try
            {
                _dataWallApiClient = new RestApiClient($"http://{appUrl}//stw-cgi")
                {
                    Credentials = new NetworkCredential(id, password)
                };
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetDataWallApiClient:");
                _dataWallApiClient = null;
            }
            return _dataWallApiClient != null;
        }
        protected override void ExecuteApplyDialogCommand()
        {

            if (PresetId <= 0 || string.IsNullOrWhiteSpace(PresetName))
            {
                ErrorMessage = "ID 또는 이름은 비워놓을 수 없습니다.";
                return;
            }

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "preset");
            queryStringDictionary.Add("action", "add");
            queryStringDictionary.Add("Channel", 0);
            queryStringDictionary.Add("Preset", _presetId);
            queryStringDictionary.Add("Name", _presetName);
            var response = _dataWallApiClient.Get("/ptzconfig.cgi", queryStringDictionary);
            
            if (response != null && !response.IsSuccess)
            {
                //todo : 다른 방법 생각중
                ErrorMessage = "프리셋 저장에 실패했습니다.";
                return;
            }
            base.ExecuteApplyDialogCommand();
        }
    }
}
