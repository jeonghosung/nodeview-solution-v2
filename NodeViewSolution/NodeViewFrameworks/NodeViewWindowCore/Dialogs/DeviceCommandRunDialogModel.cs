﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class DeviceCommandRunDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _appUri = "";
        private DeviceViewModel _device = null;
        private CommandDescription _commandDescription = null;
        private string _errorMessage = "";
        private DelegateCommand _runCommand;

        public string AppUri
        {
            get => _appUri;
            set => SetProperty(ref _appUri, value);
        }

        public DeviceViewModel Device
        {
            get => _device;
            set => SetProperty(ref _device, value);
        }
        public CommandDescription CommandDescription
        {
            get => _commandDescription;
            set => SetProperty(ref _commandDescription, value);
        }

        public string DeviceId => Device?.Id ?? "";
        public string Command => CommandDescription?.Command ?? "";
        public ObservableCollection<EditingField> Parameters { get; } = new ObservableCollection<EditingField>();
        public DelegateCommand RunCommand => _runCommand ?? (_runCommand = new DelegateCommand(ExecuteRunCommand));

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("appUri", out string appUri))
            {
                AppUri = appUri;
            }
            if (parameters.TryGetValue("device", out DeviceViewModel device))
            {
                Device = device;
            }
            if (parameters.TryGetValue("commandDescription", out CommandDescription commandDescription))
            {
                CommandDescription = commandDescription;
            }

            Title = $"{Device?.Id} {CommandDescription?.Command} 실행";

            SetUpParameters();
        }

        private void SetUpParameters()
        {
            Parameters.Clear();
            foreach (var fieldDescription in CommandDescription.Parameters)
            {
                var field = EditingField.CreateFrom(fieldDescription);
                field.ValueType = fieldDescription.ValueType;
                Parameters.Add(field);
            }
        }

        void ExecuteRunCommand()
        {
            try
            {
                if (!CheckParametersValidation())
                {
                    return;
                }
                var param = new JObject();
                foreach (var field in Parameters.ToArray())
                {
                    param[field.Id] = field.Value;
                }
                var request = new JObject();
                request["action"] = Command;
                request["param"] = param;
                RestApiClient apiClient = new RestApiClient(AppUri);
                var response = apiClient.Put($"/devices/{DeviceId}", request);
                if (response.IsSuccess)
                {
                    ErrorMessage = "";
                    MessageBox.Show("Success", "성공");
                }
                else
                {
                    ErrorMessage = string.IsNullOrWhiteSpace(response.Message) ? "실행 실패" : response.Message;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteRunCommand:");
                ErrorMessage = "내부 오류 발생";
            }
        }

        private bool CheckParametersValidation()
        {
            List<string> errorParameters = new List<string>();
            foreach (var field in Parameters.ToArray())
            {
                if (!field.IsValid)
                {
                    errorParameters.Add(field.Id);
                }
            }

            if (errorParameters.Count == 0)
            {
                return true;
            }

            ErrorMessage = $"{string.Join(",", errorParameters)} 항목 입력값이 잘못되었습니다.";
            return false;
        }
    }
}
