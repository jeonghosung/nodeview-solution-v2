﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class EditEventActionDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private EventActionViewModel _eventAction;
        private string _errorMessage = "";
        private bool _isCreateNew = true;
        private RestApiClient _apiClient;

        public string[] ActionIds { get; set; }
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public Dictionary<string, EditingField> FieldMap { get; } = new Dictionary<string, EditingField>();

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);

            if (parameters.TryGetValue("apiClient", out RestApiClient apiClient))
            {
                _apiClient = apiClient;
            }
            if (parameters.TryGetValue("isCreateNew", out bool isCreateNew))
            {
                _isCreateNew = isCreateNew;
            }
            if (parameters.TryGetValue("actions", out NodeViewActionViewModel[] actions))
            {
                List<string> actionIdList = new List<string>();
                actionIdList.Add("");
                actionIdList.AddRange(actions.Select(x => x.Id));
                ActionIds = actionIdList.ToArray();
            }

            if (parameters.TryGetValue("eventAction", out EventActionViewModel eventAction))
            {
                _eventAction = EventActionViewModel.CreateFrom(eventAction);
                if (_isCreateNew)
                {
                    _eventAction.Id = _eventAction.Id + "New";
                }
            }
            else
            {
                _isCreateNew = true;
                _eventAction = new EventActionViewModel();
                _eventAction.Id = "newEvent";
            }

            Title = isCreateNew ? "신규 EventAction 생성" : "EventAction 편집";

            SetUpFields();
        }

        private void SetUpFields()
        {
            var editingField = new EditingField("Id", _eventAction.Id, valueType: "id");
            editingField.IsReadOnly = !_isCreateNew;
            FieldMap[editingField.Id] = editingField;
            editingField = new EditingField("Level", $"{_eventAction.Level}", valueType: "enum", items: string.Join(",", System.Enum.GetNames(typeof(AlarmEventLevel))));
            FieldMap[editingField.Id] = editingField;
            editingField = new EditingField("EventOnActionId", $"{_eventAction.EventOnActionId}", valueType: "enum", items: string.Join(",", ActionIds));
            FieldMap[editingField.Id] = editingField;
            editingField = new EditingField("EventOffActionId", $"{_eventAction.EventOffActionId}", valueType: "enum", items: string.Join(",", ActionIds));
            FieldMap[editingField.Id] = editingField;
            editingField = new EditingField("Description", _eventAction.Description);
            FieldMap[editingField.Id] = editingField;
        }

        protected override void ExecuteApplyDialogCommand()
        {
            try
            {
                if (!CheckValidation())
                {
                    return;
                }

                EventActionViewModel eventAction = new EventActionViewModel();
                eventAction.Id = FieldMap["Id"].Value;
                eventAction.Level = StringUtils.GetValue(FieldMap["Level"].Value, AlarmEventLevel.Alarm);
                eventAction.Description = FieldMap["Description"].Value;
                eventAction.EventOnActionId = FieldMap["EventOnActionId"].Value;
                eventAction.EventOffActionId = FieldMap["EventOffActionId"].Value;

                var request = new JObject();
                request["action"] = _isCreateNew ? "create" : "update";
                request["param"] = eventAction.ToJson();

                var response = _apiClient.Put($"/events", request);
                if (response.IsSuccess)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("eventAction", eventAction);
                    RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
                }
                else
                {
                    string message = JsonUtils.GetStringValue(response.ResponseJsonObject, "error");
                    if (string.IsNullOrWhiteSpace(message))
                    {
                        message = _isCreateNew ? "생성 오류" : "수정 오류";
                    }

                    ErrorMessage = message;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteRunCommand:");
                ErrorMessage = "내부 오류 발생";
            }
        }

        private bool CheckValidation()
        {
            List<string> errorFields = new List<string>();
            foreach (var field in FieldMap.Values.ToArray())
            {
                if (!field.IsValid)
                {
                    errorFields.Add(field.Id);
                }
            }

            if (errorFields.Count == 0) return true;

            ErrorMessage = $"{string.Join(",", errorFields)} 항목 입력값이 잘못되었습니다.";
            return false;
        }
    }
}
