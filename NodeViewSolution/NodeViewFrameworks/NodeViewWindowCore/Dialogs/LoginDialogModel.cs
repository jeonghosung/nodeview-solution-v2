﻿using NLog;
using NodeView.Net;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class LoginDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private NodeViewApiClient _nodeViewApiClient = null;

        private string _errorMessage;
        private string _password;
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        protected override void ExecuteApplyDialogCommand()
        {
            if (!_nodeViewApiClient.Login(Password))
            {
                ErrorMessage = "로그인에 실패했습니다.";
                return;
            }

            RaiseRequestClose(new DialogResult(ButtonResult.OK));
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "로그인";
            if (parameters.TryGetValue("apiClient", out NodeViewApiClient apiClient))
            {
                _nodeViewApiClient = apiClient;
            }
        }
    }
}
