﻿using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Net;

namespace NodeView.Dialogs
{
    public class AddPresetDialogModel : DialogModelBase
    {
        private string _errorMessage;
        private string _presetName;
        private string _presetId;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private RestApiClient _dataWallApiClient = null;
        private IDataWallPreset[] _currentDataWallPresets = null;

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public string PresetName
        {
            get => _presetName;
            set => SetProperty(ref _presetName, value);
        }
        public string PresetId
        {
            get => _presetId;
            set => SetProperty(ref _presetId, value);
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "즐겨찾기 추가";
            if (parameters.TryGetValue("clientBaseUrl", out string clientBaseUrl))
            {
                if (!string.IsNullOrWhiteSpace(clientBaseUrl))
                {
                    SetDataWallApiClient(clientBaseUrl);
                }
            }

            if (parameters.TryGetValue("wallPresets", out IDataWallPreset[] dataWallPresets))
            {
                if (dataWallPresets != null && dataWallPresets.Length > 0)
                {
                    _currentDataWallPresets = dataWallPresets;
                }
            }
        }
        protected override void ExecuteApplyDialogCommand()
        {
            //if (_currentDataWallPresets != null && _currentDataWallPresets.Any(preset => preset.Name.Equals(PresetName, StringComparison.OrdinalIgnoreCase)))
            //{
            //    ErrorMessage = "동일한 이름이 존재합니다.";
            //    PresetName = "";
            //    return;
            //}
            if (_currentDataWallPresets != null && _currentDataWallPresets.Any(preset => preset.Id.Equals(PresetId, StringComparison.OrdinalIgnoreCase)))
            {
                ErrorMessage = "동일한 ID가 존재합니다.";
                PresetId = "";
                return;
            }

            if (string.IsNullOrWhiteSpace(PresetId) || string.IsNullOrWhiteSpace(PresetName))
            {
                ErrorMessage = "ID 또는 이름은 비워놓을 수 없습니다.";
                return;
            }
            JObject requestJson = new JObject()
            {
                ["id"] = PresetId,
                ["name"] = PresetName
            };
            var response = _dataWallApiClient?.Post("/presets", requestJson);
            if (response != null &&  !response.IsSuccess)
            {
                //todo : 다른 방법 생각중
                ErrorMessage = "프리셋 저장에 실패했습니다.";
                return;
            }
            base.ExecuteApplyDialogCommand();
        }
        private bool SetDataWallApiClient(string appUrl)
        {
            if (string.IsNullOrWhiteSpace(appUrl))
            {
                return false;
            }
            try
            {
                _dataWallApiClient = new NodeViewApiClient(appUrl);
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetDataWallApiClient:");
                _dataWallApiClient = null;
            }

            //ThreadAction.PostOnUiThread(CheckDataWallUpdate);
            return _dataWallApiClient != null;
        }
    }
}
