﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NodeView.Net;
using NodeView.ViewModels;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class NodeViewAppEditDialogModel : DialogModelBase
    {
        private NodeViewAppItem _app = null;
        private string _id;
        private string _name;
        private string _errorMessage = "";
        private RestApiClient _apiClient;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string AppKey { get; set; }
        public string AppType { get; set; }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Ip { get; set; }
        public string Port { get; set; }
        public string Uri { get; set; }
        public string Updated { get; set; }

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "App 정보 편집";
            if (parameters.TryGetValue("app", out NodeViewAppItem app))
            {
                _app = app;
            }
            if (parameters.TryGetValue("apiClient", out RestApiClient apiClient))
            {
                _apiClient = apiClient;
            }
            

            Reset();
        }

        private void Reset()
        {
            Id = _app.Id;
            AppKey = _app.AppKey;
            AppType = _app.AppType;
            Name = _app.Name;
            Ip = _app.Ip;
            Port = $"{_app.Port}";
            Uri = $"{_app.Uri}";
            Updated = $"{_app.Updated}";
        }

        protected override void ExecuteApplyDialogCommand()
        {

            var parameters = GetResultParameters();
            if (CheckModified())
            {
                var request = new JObject()
                {
                    ["action"] = "updateInfo",
                    ["param"] = new JObject()
                    {
                        ["appKey"] = AppKey,
                        ["id"] = Id,
                        ["name"] = Name,
                    }
                };
                var response = _apiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    parameters.Add("appKey", AppKey);
                    parameters.Add("id", Id);
                    parameters.Add("name", Name);
                    RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
                }
                else
                {
                    ErrorMessage = string.IsNullOrWhiteSpace(response.Message) ? "수정 정보 등록에 실패했습니다.": response.Message;
                }
            }
            else
            {
                RaiseRequestClose(new DialogResult(ButtonResult.Cancel, parameters));
            }
        }

        private bool CheckModified()
        {
            if (Id != _app.Id) return true;
            if (Name != _app.Name) return true;
            return false;
        }
    }
}
