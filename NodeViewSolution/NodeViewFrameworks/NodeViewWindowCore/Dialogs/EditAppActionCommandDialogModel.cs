﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Microsoft.Xaml.Behaviors.Media;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class EditAppActionCommandDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private NodeViewCommandBindable _command;
        private string _errorMessage = "";
        private bool _isCreateNew = true;
        private string _selectedDeviceId;
        private string _selectedCommandId;

        private readonly List<DeviceViewModel> _devices = new List<DeviceViewModel>();
        private readonly List<CommandDescription> _commandDescriptions = new List<CommandDescription>();
        private EditingField[] _parameters;
        private DeviceViewModel _selectedDevice;

        public string SelectedDeviceId
        {
            get => _selectedDeviceId;
            set
            {
                if (_selectedDeviceId != value)
                {
                    SetProperty(ref _selectedDeviceId, value);
                    OnChangedSelectedDeviceId();
                }
            }
        }
        public string SelectedCommandId
        {
            get => _selectedCommandId;
            set
            {
                if (_selectedCommandId != value)
                {
                    SetProperty(ref _selectedCommandId, value);
                    OnChangedSelectedCommandId();
                }
            }
        }
        public EditingField[] Parameters
        {
            get => _parameters ?? new EditingField[0];
            set => SetProperty(ref _parameters, value);
        }

        public string[] DeviceIds => _devices.Select(x => x.Id).ToArray();
        public string[] CommandIds => _commandDescriptions.Select(x => x.Command).ToArray();

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("isCreateNew", out bool isCreateNew))
            {
                _isCreateNew = isCreateNew;
            }
            if (parameters.TryGetValue("devices", out DeviceViewModel[] devices))
            {
                _devices.AddRange(devices);
            }
            if (parameters.TryGetValue("command", out NodeViewCommandBindable command))
            {
                _command = NodeViewCommandBindable.CreateFrom(command);
            }
            else
            {
                _isCreateNew = true;
                _command = new NodeViewCommandBindable();
            }

            Title = isCreateNew ? "신규 Command 생성" : "Command 편집";

            if (_devices.Count > 0)
            {
                SelectedDeviceId = _devices[0].Id;
            }
            SetUpFields();
        }

        private void SetUpFields()
        {
            if (!string.IsNullOrWhiteSpace(_command.DeviceId))
            {
                SelectedDeviceId = _command.DeviceId;
            }
            if (!string.IsNullOrWhiteSpace(_command.Command))
            {
                SelectedCommandId = _command.Command;
            }
        }

        private void OnChangedSelectedDeviceId()
        {
            var device = FindDevice(_selectedDeviceId);
            _selectedDevice = device;
            UpdateCommandsFromSelectedDevice();
        }

        private void UpdateCommandsFromSelectedDevice()
        {
            _commandDescriptions.Clear();
            Parameters = null;
            var device = _selectedDevice;
            if (device != null)
            {
                _commandDescriptions.AddRange(device.CommandDescriptions);
            }
            RaisePropertyChanged("CommandIds");
            SelectedCommandId = _commandDescriptions.Count > 0 ? _commandDescriptions[0].Command : "";
        }

        private DeviceViewModel FindDevice(string id)
        {
            foreach (var device in _devices)
            {
                if (device.Id == id)
                {
                    return device;
                }
            }
            return null;
        }

        private void OnChangedSelectedCommandId()
        {
            Parameters = null;
            var command = FindCommand(_selectedCommandId);
            if (command != null)
            {
                List<EditingField> fields = new List<EditingField>();
                foreach (var parameter in command.Parameters)
                {
                    var field = EditingField.CreateFrom(parameter);
                    field.ValueType = parameter.ValueType;
                    if (_command.Parameters.TryGetValue(field.Id, out var value))
                    {
                        field.Value = $"{value}";
                        field.DefaultValue = field.Value;
                    }
                    fields.Add(field);
                }

                Parameters = fields.ToArray();
            }
        }
        private CommandDescription FindCommand(string id)
        {
            foreach (var command in _commandDescriptions)
            {
                if (command.Command == id)
                {
                    return command;
                }
            }
            return null;
        }

        protected override void ExecuteApplyDialogCommand()
        {
            var command = new NodeViewCommandBindable("", SelectedDeviceId, SelectedCommandId);
            foreach (var parameter in Parameters)
            {
                command.Parameters[parameter.Id] = parameter.Value;
            }
            var parameters = GetResultParameters();
            parameters.Add("command", command);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }

        private bool CheckParametersValidation()
        {
            List<string> errorParameters = new List<string>();
            foreach (var field in Parameters.ToArray())
            {
                if (!field.IsValid)
                {
                    errorParameters.Add(field.Id);
                }
            }

            if (errorParameters.Count == 0)
            {
                return true;
            }

            ErrorMessage = $"{string.Join(",", errorParameters)} 항목 입력값이 잘못되었습니다.";
            return false;
        }
    }
}
