﻿using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Forms.VisualStyles;
using NLog;
using NodeView.DataModels;
using NodeView.Drawing;
using NodeView.Protocols.NodeViews;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;

namespace NodeView.Dialogs
{
    public class MonitorControlDialogModel : DialogModelBase, IScreenAreaViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private NodeViewMonitorControllerClient _monitorController = null;
        private const int BaseWidth = 1920;
        private const int BaseHeight = 1200;

        private Device _monitorDevice = null;
        private string[] _inputSources = new string[0];
        private BackLightComboBoxItem[] _backLightValues = {};
        private IntSize _monitorMatrix;
        private volatile bool _monitorMatrixIsChanged = true;
        private string _selectedInputSource;
        private BackLightComboBoxItem _selectedBackLightValue;
        private IntSize _screenSize = new IntSize(1920, 1080);

        private DelegateCommand<string> _toggleSelectionMonitorCommand;
        private DelegateCommand _toggleSelectionAllMonitorCommand;
        private DelegateCommand<string> _monitorPowerOnOffCommand;
        private DelegateCommand _setMonitorInputCommand;
        private DelegateCommand _setMonitorBackLightCommand;

        public Device MonitorDevice
        {
            get => _monitorDevice;
            set
            {
                SetProperty(ref _monitorDevice, value);
                if (value == null)
                {
                    _monitorMatrix = new IntSize(1, 1);
                    InputSources = new string[0];
                }
                else
                {
                    var matrix = StringUtils.GetValue(value.Attributes["matrix"], new IntSize(1, 1));
                    if (!matrix.Equals(_monitorMatrix))
                    {
                        _monitorMatrix = matrix;
                        _monitorMatrixIsChanged = true;
                    }
                    InputSources = StringUtils.Split($"{value.Attributes["inputSources"]}", ',');
                }
            }
        }

        public string[] InputSources 
        {
            get => _inputSources;
            set
            {
                SetProperty(ref _inputSources, value);
                if (value.Length > 0)
                {
                    SelectedInputSource = value[0];
                }
            }
        }

        public BackLightComboBoxItem[] BackLightValues
        {
            get => _backLightValues;
            set
            {
                SetProperty(ref _backLightValues, value);
                if (value.Length > 0)
                {
                    SelectedBackLightValue = value[0];
                }
            }
        }
        public string SelectedInputSource
        {
            get => _selectedInputSource;
            set => SetProperty(ref _selectedInputSource, value);
        }
        public BackLightComboBoxItem SelectedBackLightValue
        {
            get => _selectedBackLightValue;
            set => SetProperty(ref _selectedBackLightValue, value);
        }
        public ObservableCollection<MonitorCellViewModel> MonitorCells { get; set; } = new ObservableCollection<MonitorCellViewModel>();

        public DelegateCommand<string> ToggleSelectionMonitorCommand => _toggleSelectionMonitorCommand ??= new DelegateCommand<string>(ExecuteToggleSelectionMonitorCommand);
        public DelegateCommand ToggleSelectionAllMonitorCommand => _toggleSelectionAllMonitorCommand ??= new DelegateCommand(ExecuteToggleSelectionAllMonitorCommand);
        public DelegateCommand<string> MonitorPowerOnOffCommand => _monitorPowerOnOffCommand ??= new DelegateCommand<string>(ExecuteMonitorPowerOnOffCommand);
        public DelegateCommand SetMonitorInputCommand => _setMonitorInputCommand ??= new DelegateCommand(ExecuteSetMonitorInputCommand);

        public DelegateCommand SetMonitorBackLightCommand => _setMonitorBackLightCommand ??= new DelegateCommand(ExecuteSetMonitorBackLightCommand);

       
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "모니터 제어";
            if (parameters.TryGetValue("deviceBaseUri", out string deviceBaseUri))
            {
                _monitorController = new NodeViewMonitorControllerClient(deviceBaseUri);
            }

            if (_monitorController != null)
            {
                MonitorDevice = _monitorController.GetDevice();
            }

            InitBackLightValue();
            
            UpdateMonitorCells();
        }

        private void InitBackLightValue()
        {
            _backLightValues = new BackLightComboBoxItem[] {
                new("1") { Name = "1단계", BackGroundColor = "#ffffff", FontColor = "#000" },
                new("2") { Name = "2단계", BackGroundColor = "#ECEFF1", FontColor = "#000" },
                new("3") { Name = "3단계", BackGroundColor = "#B0BEC5", FontColor = "#000" },
                new("4") { Name = "4단계", BackGroundColor = "#546E7A", FontColor = "#fff" },
                new("5") { Name = "5단계", BackGroundColor = "#263238", FontColor = "#fff" }};
            SelectedBackLightValue = BackLightValues[0];
        }

        private void ExecuteToggleSelectionMonitorCommand(string id)
        {
            foreach (var monitorCell in MonitorCells.ToArray())
            {
                if (monitorCell.Id == id)
                {
                    monitorCell.IsSelected = !monitorCell.IsSelected;
                }
            }
        }
        private void ExecuteToggleSelectionAllMonitorCommand()
        {
            bool isSelected = !IsSelectedAll();

            foreach (var screenCell in MonitorCells)
            {
                screenCell.IsSelected = isSelected;
            }
        }
        private void ExecuteMonitorPowerOnOffCommand(string powerOnOff)
        {
            if(_monitorController == null) return;

            if (string.IsNullOrWhiteSpace(powerOnOff))
            {
                Logger.Warn("powerOnOff is null.");
                return;
            }

            bool isOn = powerOnOff.Equals("on", StringComparison.OrdinalIgnoreCase);

            if (IsSelectedAll())
            {
                _monitorController.ControlPowerAll(isOn);
            }
            else
            {
                foreach (var monitor in MonitorCells.ToArray())
                {
                    if (monitor.IsSelected)
                    {
                        _monitorController.ControlPower(monitor.Seq, isOn);
                    }
                }
            }
        }
        void ExecuteSetMonitorInputCommand()
        {
            if (_monitorController == null || string.IsNullOrWhiteSpace(SelectedInputSource)) return;

            if (IsSelectedAll())
            {
                _monitorController.ControlInputSourceNameAll(SelectedInputSource);
            }
            else
            {
                foreach (var monitor in MonitorCells.ToArray())
                {
                    if (monitor.IsSelected)
                    {
                        _monitorController.ControlInputSourceName(monitor.Seq, SelectedInputSource);
                    }
                }
            }
        }
        private void ExecuteSetMonitorBackLightCommand()
        {
            if (_monitorController == null || SelectedBackLightValue == null) return;

            if (IsSelectedAll())
            {
                _monitorController.ControlInputBackLightAll(SelectedBackLightValue.Value);
            }
            else
            {
                foreach (var monitor in MonitorCells.ToArray())
                {
                    if (monitor.IsSelected)
                    {
                        _monitorController.ControlInputBackLight(monitor.Seq, SelectedBackLightValue.Value);
                    }
                }
            }
        }

        private bool IsSelectedAll()
        {
            foreach (var monitor in MonitorCells.ToArray())
            {
                if (!monitor.IsSelected)
                {
                    return false;
                }
            }

            return true;
        }

        public void UpdatedScreenAreaSize(IntSize screenAreaSize)
        {
            if (_screenSize != screenAreaSize)
            {
                _screenSize = screenAreaSize;
                UpdateMonitorCells();
            }
        }

        private void UpdateMonitorCells()
        {
            if (_monitorMatrixIsChanged)
            {
                CreateMonitorCells();
            }
            else
            {
                UpdateRectMonitorCells();
            }
        }

        private void CreateMonitorCells()
        {
            MonitorCells.Clear();
            if (!_monitorMatrix.IsEmpty)
            {
                int seq = 1;
                for (int i = 0; i < _monitorMatrix.Height; i++)
                {
                    for (int j = 0; j < _monitorMatrix.Width; j++)
                    {
                        MonitorCells.Add(new MonitorCellViewModel(seq++, new IntRect(j * BaseWidth, i * BaseHeight, BaseWidth, BaseHeight)));
                    }
                }
            }

            UpdateRectMonitorCells();
            _monitorMatrixIsChanged = false;
        }
        private void UpdateRectMonitorCells()
        {
            double baseTotalWidth = BaseWidth * _monitorMatrix.Width;
            double baseTotalHeight = BaseHeight* _monitorMatrix.Height;
            double scale = Math.Min(_screenSize.Width / baseTotalWidth, _screenSize.Height / baseTotalHeight);
            int offsetX = (int)((_screenSize.Width - (baseTotalWidth*scale)) / 2);
            int offsetY = (int)((_screenSize.Height - (baseTotalHeight * scale)) / 2);
            var offset = new IntPoint(offsetX, offsetY);

            foreach (var monitorCell in MonitorCells.ToArray())
            {
                monitorCell.UpdateRectFromBaseRect(offset, scale);
            }
        }

    }

    public class BackLightComboBoxItem : BindableBase
    {
        private string _value;
        private string _fontColor;
        private string _name;

        public string Value
        {
            get => _value;
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        private string _backGroundColor;

        public BackLightComboBoxItem(string value)
        {
            _value = value;
        }

        public string BackGroundColor
        {
            get => _backGroundColor;
            set => SetProperty(ref _backGroundColor, value);
        }

        public string FontColor
        {
            get => _fontColor;
            set => SetProperty(ref _fontColor, value);
        }
    }
}
