﻿using System;
using System.Windows;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class DialogModelBase : BindableBase, IDialogAware
    {
        private string _title = "";
        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string ThemeColor { get; set; } = "#0A6BF0";
        public DelegateCommand<Window> CloseWindowCommand { get; set; }
        public DelegateCommand CancelDialogCommand { get; set; }
        public DelegateCommand ApplyDialogCommand { get; set; }
        public event Action<IDialogResult> RequestClose;

        public DialogModelBase()
        {
            CloseWindowCommand = new DelegateCommand<Window>(ExecuteCloseWindowCommand);
            ApplyDialogCommand = new DelegateCommand(ExecuteApplyDialogCommand);
            CancelDialogCommand = new DelegateCommand(ExecuteCancelDialogCommand);
            ThemeColor = $"{Application.Current.Resources["brushKeyColor"]}";
        }

        private void ExecuteCloseWindowCommand(Window obj)
        {
            RaiseRequestClose(new DialogResult(ButtonResult.Cancel));
        }
        protected virtual void ExecuteApplyDialogCommand()
        {
            var parameters = GetResultParameters();
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }

        protected virtual void ExecuteCancelDialogCommand()
        {
            RaiseRequestClose(new DialogResult(ButtonResult.Cancel));
        }

        protected virtual DialogParameters GetResultParameters()
        {
            return new DialogParameters();
        }

        public virtual void RaiseRequestClose(IDialogResult dialogResult)
        {
            RequestClose?.Invoke(dialogResult);
        }

        public virtual bool CanCloseDialog()
        {
            return true;
        }

        public virtual void OnDialogClosed()
        {
        }

        public virtual void OnDialogOpened(IDialogParameters parameters)
        {

        }
    }
}
