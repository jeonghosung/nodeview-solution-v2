﻿using System;
using System.Windows;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class ErrorDialogModelBase : DialogModelBase
    {
        public ErrorDialogModelBase()
        {
            ThemeColor = $"{Application.Current.Resources["brushErrorPopup"]}";
        }
    }
}
