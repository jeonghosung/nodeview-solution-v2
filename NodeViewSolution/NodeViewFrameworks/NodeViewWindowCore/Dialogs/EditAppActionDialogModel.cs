﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class EditAppActionDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IDialogService _dialogService = null;
        private readonly List<DeviceViewModel> _devices = new List<DeviceViewModel>();
        private NodeViewActionViewModel _action;
        private string _errorMessage = "";
        private bool _isCreateNew = true;
        private EditingField _idField;
        private EditingField _descriptionField;
        private DelegateCommand _addNewCommandCommand;
        private DelegateCommand<string> _addDuplicateCommandCommand;
        private DelegateCommand<string> _editCommandCommand;
        private DelegateCommand<string> _deleteCommandCommand;
        private DelegateCommand<string> _moveUpCommandCommand;
        private DelegateCommand<string> _moveDownCommandCommand;
        private RestApiClient _apiClient;

        public DelegateCommand AddNewCommandCommand => _addNewCommandCommand ?? (_addNewCommandCommand = new DelegateCommand(ExecuteAddNewCommandCommand));
        public DelegateCommand<string> AddDuplicateCommandCommand => _addDuplicateCommandCommand ?? (_addDuplicateCommandCommand = new DelegateCommand<string>(ExecuteAddDuplicateCommandCommand));
        public DelegateCommand<string> EditCommandCommand => _editCommandCommand ?? (_editCommandCommand = new DelegateCommand<string>(ExecuteEditCommandCommand));
        public DelegateCommand<string> DeleteCommandCommand => _deleteCommandCommand ?? (_deleteCommandCommand = new DelegateCommand<string>(ExecuteDeleteCommandCommand));
        public DelegateCommand<string> MoveUpCommandCommand => _moveUpCommandCommand ?? (_moveUpCommandCommand = new DelegateCommand<string>(ExecuteMoveUpCommandCommand));
        public DelegateCommand<string> MoveDownCommandCommand => _moveDownCommandCommand ?? (_moveDownCommandCommand = new DelegateCommand<string>(ExecuteMoveDownCommandCommand));

        public EditingField IdField
        {
            get => _idField;
            set => SetProperty(ref _idField, value);
        }

        public EditingField DescriptionField
        {
            get => _descriptionField;
            set => SetProperty(ref _descriptionField, value);
        }

        public ObservableCollection<NodeViewCommandBindable> Commands => _action.Commands;

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }

        public EditAppActionDialogModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            
            if (parameters.TryGetValue("apiClient", out RestApiClient apiClient))
            {
                _apiClient = apiClient;
            }
            if (parameters.TryGetValue("isCreateNew", out bool isCreateNew))
            {
                _isCreateNew = isCreateNew;
            }
            if (parameters.TryGetValue("devices", out DeviceViewModel[] devices))
            {
                _devices.AddRange(devices);
            }
            if (parameters.TryGetValue("action", out NodeViewActionViewModel action))
            {
                _action = NodeViewActionViewModel.CreateFrom(action);
                if (isCreateNew)
                {
                    _action.Id = _action.Id + "New";
                }
            }
            else
            {
                _isCreateNew = true;
                _action = new NodeViewActionViewModel("newAction");
            }

            Title = isCreateNew ? "신규 Action 생성" : "Action 편집";

            SetUpFields();
        }

        private void SetUpFields()
        {
            var idField = new EditingField("Id", _action.Id, "", "id");
            idField.IsReadOnly = !_isCreateNew;

            IdField = idField;
            DescriptionField = new EditingField("Description", _action.Description, "", "string");
        }
        void ExecuteAddNewCommandCommand()
        {
            try
            {
                var parameters = new DialogParameters();
                parameters.Add("isCreateNew", true);
                parameters.Add("devices", _devices.ToArray());
                _dialogService.ShowDialog("EditAppActionCommandDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var resCommand = r.Parameters.GetValue<NodeViewCommandBindable>("command");
                        Commands.Add(NodeViewCommandBindable.CreateFrom(resCommand));
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteAddNewCommandCommand:");
            }
        }

        void ExecuteAddDuplicateCommandCommand(string id)
        {
            try
            {
                var command = FindCommand(id);
                if (command == null) return;

                var parameters = new DialogParameters();
                parameters.Add("isCreateNew", true);
                parameters.Add("devices", _devices.ToArray());
                parameters.Add("command", NodeViewCommandBindable.CreateFrom(command));
                _dialogService.ShowDialog("EditAppActionCommandDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var resCommand = r.Parameters.GetValue<NodeViewCommandBindable>("command");
                        Commands.Add(NodeViewCommandBindable.CreateFrom(resCommand));
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteAddDuplicateCommandCommand:");
            }
        }

        private NodeViewCommandBindable FindCommand(string id)
        {
            foreach (var command in Commands.ToArray())
            {
                if (command.Id == id)
                {
                    return command;
                }
            }

            return null;
        }

        void ExecuteEditCommandCommand(string id)
        {
            try
            {
                var command = FindCommand(id);
                if (command == null) return;

                var parameters = new DialogParameters();
                parameters.Add("isCreateNew", false);
                parameters.Add("devices", _devices.ToArray());
                parameters.Add("command", NodeViewCommandBindable.CreateFrom(command));
                _dialogService.ShowDialog("EditAppActionCommandDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var resCommand = r.Parameters.GetValue<NodeViewCommandBindable>("command");
                        command.Update(resCommand);
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteEditCommandCommand:");
            }
        }
        void ExecuteDeleteCommandCommand(string id)
        {
            try
            {
                var command = FindCommand(id);
                if (command == null) return;

                var parameters = new DialogParameters();
                parameters.Add("title", "삭제 확인");
                parameters.Add("message", $"{command.DeviceId}/{command.Command}을 삭제 하시겠습니까?");
                _dialogService.ShowDialog("warningConfirm", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        Commands.Remove(command);
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDeleteCommandCommand:");
            }
        }
        void ExecuteMoveUpCommandCommand(string id)
        {
            try
            {
                int index = 0;
                var commands = Commands.ToArray();
                foreach (var command in commands)
                {
                    if (command.Id == id)
                    {
                        if (index > 0)
                        {
                            Commands.Move(index, index-1);
                            return;
                        }
                    }

                    index++;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteMoveUpCommandCommand:");
            }
        }
        void ExecuteMoveDownCommandCommand(string id)
        {
            try
            {
                int index = 0;
                var commands = Commands.ToArray();
                int count = commands.Length;
                foreach (var command in commands)
                {
                    if (command.Id == id)
                    {
                        if (index < count-1)
                        {
                            Commands.Move(index, index + 1);
                            return;
                        }
                    }

                    index++;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteMoveDownCommandCommand:");
            }
        }

        protected override void ExecuteApplyDialogCommand()
        {
            try
            {
                if (!CheckValidation())
                {
                    return;
                }

                NodeViewActionViewModel action = new NodeViewActionViewModel(IdField.Value, DescriptionField.Value);
                foreach (var command in Commands)
                {
                    action.Commands.Add(NodeViewCommandBindable.CreateFrom(command));
                }

                var request = new JObject();
                request["action"] = _isCreateNew? "create" : "update";
                request["param"] = action.ToJson();

                var response = _apiClient.Put($"/actions", request);
                if (response.IsSuccess)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("action", action);
                    RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
                }
                else
                {
                    string message = JsonUtils.GetStringValue(response.ResponseJsonObject, "error");
                    if (string.IsNullOrWhiteSpace(message))
                    {
                        message = _isCreateNew ? "생성 오류" : "수정 오류";
                    }
                        
                    ErrorMessage = message;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteRunCommand:");
                ErrorMessage = "내부 오류 발생";
            }
        }

        private bool CheckValidation()
        {
            List<string> errorFields = new List<string>();
            if (string.IsNullOrWhiteSpace(IdField.Value) || !IdField.IsValid)
            {
                errorFields.Add(IdField.Id);
            }

            if (errorFields.Count == 0) return true;

            ErrorMessage = $"{string.Join(",", errorFields)} 항목 입력값이 잘못되었습니다.";
            return false;
        }
    }
}
