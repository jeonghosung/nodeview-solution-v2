﻿using System;
using System.Windows;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class WarningDialogModelBase : DialogModelBase
    {
        public WarningDialogModelBase()
        {
            ThemeColor = $"{Application.Current.Resources["brushWarningPopup"]}";
        }
    }
}
