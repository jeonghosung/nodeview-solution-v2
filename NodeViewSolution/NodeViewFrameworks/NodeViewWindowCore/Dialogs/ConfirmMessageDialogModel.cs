﻿using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class ConfirmMessageDialogModel : DialogModelBase
    {
        public string Message { get; set; } = "";

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("title", out string title))
            {
                Title = title;
            }
            if (parameters.TryGetValue("message", out string message))
            {
                Message = message;
            }
            if (string.IsNullOrWhiteSpace(Title))
            {
                Title = "확인창";
            }
            if (string.IsNullOrWhiteSpace(Message))
            {
                Message = "message를 입력하세요.";
            }
        }
    }
}
