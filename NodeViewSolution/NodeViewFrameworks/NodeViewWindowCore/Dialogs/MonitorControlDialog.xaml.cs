﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeView.Drawing;
using NodeView.ViewModels;

namespace NodeView.Dialogs
{
    /// <summary>
    /// MonitorControlDialog.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MonitorControlDialog : UserControl
    {
        public MonitorControlDialog()
        {
            InitializeComponent();
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (DataContext is IScreenAreaViewModel screenAreaViewModel)
            {
                screenAreaViewModel.UpdatedScreenAreaSize(new IntSize((int)ScreenArea.Width, (int)ScreenArea.Height));
            }
        }
    }
}
