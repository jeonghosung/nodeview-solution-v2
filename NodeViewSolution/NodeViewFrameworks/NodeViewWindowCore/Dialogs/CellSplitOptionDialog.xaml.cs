﻿using System.Windows.Controls;
using System.Windows.Input;
using NodeView.Utils;

namespace NodeView.Dialogs
{
    /// <summary>
    /// CellSplitOptionDialog.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CellSplitOptionDialog : UserControl
    {
        public CellSplitOptionDialog()
        {
            InitializeComponent();
        }

        private void NumberTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !StringUtils.IsInteger(e.Text);
        }
    }
}
