﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NodeView.DataModels;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class AppSelectionDialogModel : DialogModelBase
    {
        public ObservableCollection<INodeViewAppNode> Apps { get; set; } = new ObservableCollection<INodeViewAppNode>();
        public DelegateCommand<string> SelectAppCommand { get; set; }

        public AppSelectionDialogModel()
        {
            SelectAppCommand = new DelegateCommand<string>(ExecuteSelectAppCommand);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "NodeView App 선택";
            if (parameters.TryGetValue("apps", out IEnumerable<INodeViewAppNode> appNodes))
            {
                Apps.AddRange(appNodes);
            }
        }

        private void ExecuteSelectAppCommand(string appId)
        {
            foreach (var appNode in Apps.ToArray())
            {
                if (appNode.Id == appId)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("selectedApp", appNode);
                    RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
                    break;
                }
            }
        }
    }
}
