﻿using System;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class NoFrameMovableDialogModelBase : DialogModelBase
    {
        protected bool _topmost;
        protected int _top;
        protected int _left;
        protected int _width;
        protected int _height;
        public bool Topmost
        {
            get => _topmost;
            set => SetProperty(ref _topmost, value);
        }
        public int Top
        {
            get => _top;
            set => SetProperty(ref _top, value);
        }
        public int Left
        {
            get => _left;
            set => SetProperty(ref _left, value);
        }
        public int Width
        {
            get => _width;
            set => SetProperty(ref _width, value);
        }
        public int Height
        {
            get => _height;
            set => SetProperty(ref _height, value);
        }
    }
}
