﻿using NodeView.DataWalls;
using Prism.Services.Dialogs;

namespace NodeView.Dialogs
{
    public class DataWallWindowViewDialogModel : NoFrameMovableDialogModelBase
    {
        private IDataWallWindow _dataWallWindow;
        private string _id = "";
        private IDataWallContent _content;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public IDataWallContent Content
        {
            get => _content;
            set => SetProperty(ref _content, value);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("dataWallWindow", out IDataWallWindow dataWallWindow))
            {
                _dataWallWindow = dataWallWindow;

                Id = _dataWallWindow.Id;
                Topmost = _dataWallWindow.IsPopup;
                Top = _dataWallWindow.WindowRect.Top;
                Left = _dataWallWindow.WindowRect.Left;
                Width = _dataWallWindow.WindowRect.Width;
                Height = _dataWallWindow.WindowRect.Height;
                Content = _dataWallWindow.Content;
            }
        }
    }
}
