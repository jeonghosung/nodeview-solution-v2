﻿using System.Windows;
using Prism.Services.Dialogs;

namespace NodeView.DialogWindows
{
    /// <summary>
    /// CustomWindowBarWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class CustomDialogWindow : Window, IDialogWindow
    {
        public IDialogResult Result { get; set; }
        public CustomDialogWindow()
        {
            InitializeComponent();
        }
    }
}
