﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Windows;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;
using NodeView.Frameworks.Widgets;
using Prism.Ioc;

namespace NodeView.Ioc
{
    public static class ContainerNodeViewWindowExtensions
    {
        public static readonly Type DataWallContentControlType = typeof(IDataWallContentControl);
        public const string DataWallContentControlNamePrefix = "dataWallContent:";
        private static readonly Dictionary<string, bool> AutoWireViewModelDictionary = new Dictionary<string, bool>();
        public static IContainerRegistry RegisterView(this IContainerRegistry containerRegistry, Type type, bool autoWireViewModel = true)
        {
            return containerRegistry.RegisterView(type, type, "", autoWireViewModel);
        }

        public static IContainerRegistry RegisterView(this IContainerRegistry containerRegistry, Type type, string name, bool autoWireViewModel = true)
        {
            return containerRegistry.RegisterView(type, type, name, autoWireViewModel);
        }

        public static IContainerRegistry RegisterView(this IContainerRegistry containerRegistry, Type @from, Type to, bool autoWireViewModel = true)
        {
            return containerRegistry.RegisterView(@from, to, "", autoWireViewModel);
        }

        public static IContainerRegistry RegisterView(this IContainerRegistry containerRegistry, Type @from, Type to, string name, bool autoWireViewModel = true)
        {
            string key = $"{to.FullName}:{name}";
            AutoWireViewModelDictionary[key] = autoWireViewModel;

            if (string.IsNullOrWhiteSpace(name))
            {
                return containerRegistry.Register(@from, to);
            }
            return containerRegistry.Register(@from, to, name);
        }

        //RegisterDataWallContentControl
        public static IContainerRegistry RegisterDataWallContentControl(this IContainerRegistry containerRegistry, Type type, string contentTypeName, bool autoWireViewModel = true)
        {
            return containerRegistry.RegisterView(DataWallContentControlType, type, $"{DataWallContentControlNamePrefix}{contentTypeName}", autoWireViewModel);
        }
        
        public static T ResolveView<T>(this IContainerProvider provider)
        {
            return (T)provider.ResolveView(typeof(T));
        }
        public static object ResolveView(this IContainerProvider containerProvider, Type type)
        {
            return containerProvider.ResolveView(type, "", null);
        }

        public static T ResolveView<T>(this IContainerProvider provider, params (Type Type, object Instance)[] parameters)
        {
            return (T)provider.ResolveView(typeof(T), parameters);
        }
        public static object ResolveView(this IContainerProvider containerProvider, Type type, params (Type Type, object Instance)[] parameters)
        {
            return containerProvider.ResolveView(type, "", parameters);
        }

        public static T ResolveView<T>(this IContainerProvider provider, string name)
        {
            return (T)provider.ResolveView(typeof(T), name);
        }
        public static object ResolveView(this IContainerProvider containerProvider, Type type, string name)
        {
            return containerProvider.ResolveView(type, name, null);
        }

        public static T ResolveView<T>(this IContainerProvider provider, string name, params (Type Type, object Instance)[] parameters)
        {
            return (T)provider.ResolveView(typeof(T), name, parameters);
        }
        public static object ResolveView(this IContainerProvider containerProvider, Type type, string name, params (Type Type, object Instance)[] parameters)
        {
            object resObject = null;
            parameters = parameters ?? Array.Empty<(Type Type, object Instance)>();

            bool autoWireViewModel = true;
            string key = $"{type.FullName}:{name}";
            if (AutoWireViewModelDictionary.TryGetValue(key, out bool foundAutoWireViewModel))
            {
                autoWireViewModel = foundAutoWireViewModel;
            }

            if (string.IsNullOrWhiteSpace(name))
            {
                resObject = containerProvider.Resolve(type, parameters);
            }
            else
            {
                try
                {
                    resObject = containerProvider.Resolve(type, name, parameters);
                }
                catch
                {
                    resObject = null;
                }
                if (resObject == null)
                {
                    resObject = containerProvider.Resolve(type, parameters);
                }
            }

            if (autoWireViewModel == true && resObject != null)
            {
                if (resObject is FrameworkElement element)
                {
                    if (element.DataContext == null)
                    {
                        Type viewModelType = FindViewModelType(resObject.GetType());
                        if (viewModelType != null)
                        {
                            var viewModel = containerProvider.Resolve(viewModelType, parameters);
                            if (viewModel != null)
                            {
                                element.DataContext = viewModel;
                            }
                        }
                    }
                }
            }

            return resObject;
        }

        //ResolveDataWallContentControl
        public static T ResolveDataWallContentControl<T>(this IContainerProvider provider, IDataWallContent content)
        {
            return (T)provider.ResolveDataWallContentControl(content);
        }

        public static object ResolveDataWallContentControl(this IContainerProvider containerProvider, IDataWallContent content)
        {
            return containerProvider.ResolveDataWallContentControl(content, null);
        }

        public static T ResolveDataWallContentControl<T>(this IContainerProvider provider, IDataWallContent content, params (Type Type, object Instance)[] parameters)
        {
            return (T)provider.ResolveDataWallContentControl(content, parameters);
        }
        public static object ResolveDataWallContentControl(this IContainerProvider containerProvider, IDataWallContent content, params (Type Type, object Instance)[] parameters)
        {
            if (content == null)
            {
                return null;
            }
            List<(Type Type, object Instance)> paramList = new List<(Type Type, object Instance)>();
            if (parameters != null)
            {
                paramList.AddRange(parameters);
            }
            paramList.Add((typeof(IDataWallContent), content));
            var control = containerProvider.ResolveView<IDataWallContentControl>($"{DataWallContentControlNamePrefix}{content.ContentType}", paramList.ToArray());
            if (control != null)
            {
                control.Source = content;
            }

            return control;
        }
        
        private static Type FindViewModelType(Type viewType)
        {
            if (viewType == null)
            {
                return null;
            }
            var viewName = viewType?.FullName ?? "";
            viewName = viewName.Replace(".Views.", ".ViewModels.");
            var viewAssemblyName = viewType.GetTypeInfo().Assembly.FullName;
            var suffix = "ViewModel";
            var viewModelName = String.Format(CultureInfo.InvariantCulture, "{0}{1}, {2}", viewName, suffix, viewAssemblyName);
            Type retType = Type.GetType(viewModelName);
            if (retType == null)
            {
                suffix = "Model";
                viewModelName = String.Format(CultureInfo.InvariantCulture, "{0}{1}, {2}", viewName, suffix, viewAssemblyName);
                retType = Type.GetType(viewModelName);
            }
            return retType;
        }
    }
}
