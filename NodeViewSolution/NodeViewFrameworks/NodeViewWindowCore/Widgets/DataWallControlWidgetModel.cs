﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using NodeView.Protocols.NodeViews;
using NodeView.Tasks;
using NodeView.Utils;
using NodeView.ViewModels;
using NodeViewCore.Utils;
using NodeViewWindowCore.Views;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.Widgets
{
    public class DataWallControlWidgetModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private NodeViewApiClient _nodeViewApiClient = null;
        private bool _isUsingContentsTree = true;
        private Visibility _contentsBoxVisibility = Visibility.Hidden;
        private Visibility _contentsTreeVisibility = Visibility.Visible;
        private string _contentsTreeWidth = "300";
        private string _contentsBoxHeight = "0";
        private IntSize _screenSize = new IntSize(1920, 1080);
        private IntPoint _screenMatrix = new IntPoint(1, 1);
        private string _lastScreenUpdated = "";
        private string _lastPresetsUpdated = "";
        private string _lastContentsUpdated = "";
        private IDialogService _dialogService = null;
        private Device _monitorDevice = null;
        private List<ScreenCellViewModel> _selectedScreenCells = new List<ScreenCellViewModel>();
        private IDialogParameters _lastCellSplitOptionDialogParameters = null;
        private bool _isLayoutCellSelected;
        private bool _isMultiLayoutCellSelected;
        private bool _isWindowCellSelected;
        private bool _isPopupCellSelected;
        private bool _isPopupMovingMode;
        private bool _existWindows;
        private CacheManager _cacheManager;

        private List<ContentItem> _contents = new List<ContentItem>();
        private ContentItem _selectedContent = null;
        private ContentTreeViewFolder _wallContentFolder = new ContentTreeViewFolder("");
        private List<DataWallPresetBindable> _presets = new List<DataWallPresetBindable>();
        private string _dataWllApiUri = "";
        private NodeViewMonitorControllerClient _monitorController = null;
        private string[] _monitorInputSources;
        private IntRect _popupRect;

        public Device MonitorDevice
        {
            get => _monitorDevice;
            set
            {
                SetProperty(ref _monitorDevice, value);
                MonitorInputSources = value == null ? new string[0] : StringUtils.Split($"{value.Attributes["inputSources"]}", ',');
                RaisePropertyChanged("MonitorControlVisibility");
            }
        }
        public Visibility MonitorControlVisibility => (_monitorDevice == null) ? Visibility.Collapsed : Visibility.Visible;

        public bool IsEnableAddPopup => _contents.Any(content => content.IsSelected);
        public string[] MonitorInputSources
        {
            get => _monitorInputSources;
            set => SetProperty(ref _monitorInputSources, value);
        }
        public bool IsLayoutCellSelected
        {
            get => _isLayoutCellSelected;
            set => SetProperty(ref _isLayoutCellSelected, value);
        }
        public bool IsMultiLayoutCellSelected
        {
            get => _isMultiLayoutCellSelected;
            set => SetProperty(ref _isMultiLayoutCellSelected, value);
        }
        public bool IsWindowCellSelected
        {
            get => _isWindowCellSelected;
            set => SetProperty(ref _isWindowCellSelected, value);
        }
        public bool IsPopupCellSelected
        {
            get => _isPopupCellSelected;
            set => SetProperty(ref _isPopupCellSelected, value);
        }

        public bool IsPopupMovingMode
        {
            get => _isPopupMovingMode;
            set => SetProperty(ref _isPopupMovingMode, value);
        }

        public bool ExistWindows
        {
            get => _existWindows;
            set => SetProperty(ref _existWindows, value);
        }
        public ContentTreeViewFolder WallContentFolder
        {
            get => _wallContentFolder;
            set => SetProperty(ref _wallContentFolder, value);
        }

        public ObservableCollection<ContentItem> WallContentList { get; } = new ObservableCollection<ContentItem>();
        public ObservableCollection<DataWallPresetBindable> WallPresetsV { get; } = new ObservableCollection<DataWallPresetBindable>();
        public ObservableCollection<DataWallPresetBindable> WallPresetsH { get; } = new ObservableCollection<DataWallPresetBindable>();
        public ObservableCollection<DataWallWindowBindable> WallWindows { get; } = new ObservableCollection<DataWallWindowBindable>();
        public ObservableCollection<DataWallWindowBindable> WallPopupWindows { get; } = new ObservableCollection<DataWallWindowBindable>();
        public ObservableCollection<ScreenCellViewModel> ScreenCells { get; set; } = new ObservableCollection<ScreenCellViewModel>();

        public DelegateCommand<string> SetUsingContentsTreeCommand { get; set; }
        public DelegateCommand<string> SelectCellCommand { get; set; }
        public DelegateCommand SplitCellCommand { get; set; }
        public DelegateCommand MergeCellCommand { get; set; }
        public DelegateCommand MonitorOnAllCommand { get; set; }
        public DelegateCommand MonitorOffAllCommand { get; set; }
        public DelegateCommand OpenMonitorControlCommand { get; set; }
        public DelegateCommand CloseCellCommand { get; set; }
        public DelegateCommand CloseAllWindowsPopupsCommand { get; set; }
        public DelegateCommand ReloadCommand { get; set; }
        public DelegateCommand<string> ToggleWallContentSelectionCommand { get; set; }
        public DelegateCommand<string> MouseDoubleClickWallContentCommand { get; set; }
        public DelegateCommand<ContentItem> OpenPreviewWindowCommand { get; set; }
        public DelegateCommand<string> RunWallPresetCommand { get; set; }
        public DelegateCommand OpenAddPresetDialogCommand { get; set; }
        public DelegateCommand CreatePopupCommand { get; set; }
        public DelegateCommand ClosePopupCommand { get; set; }
        public DelegateCommand TogglePopupMovingCommand { get; set; }
        public DelegateCommand<string> MouseDoubleClickWindowCommand { get; set; }
        public DelegateCommand<KeyEventArgs> KeyUpEventCommand { get; set; }

        public DataWallControlWidgetModel(IDialogService dialogService, IWidgetSource widgetSource)
        {
            _dialogService = dialogService;

            _cacheManager = new CacheManager(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches", "cache.json"));
            _popupRect = _cacheManager.GetValue<IntRect>("popup", new IntRect(0, 0, 960, 540));
            SetUsingContentsTreeCommand = new DelegateCommand<string>(ExecuteSetUsingContentsTreeCommand);
            SelectCellCommand = new DelegateCommand<string>(ExecuteSelectCellCommand);
            SplitCellCommand = new DelegateCommand(ExecuteSplitCellCommand);
            MergeCellCommand = new DelegateCommand(ExecuteMergeCellCommand);
            CloseCellCommand = new DelegateCommand(ExecuteCloseCellCommand);
            CloseAllWindowsPopupsCommand = new DelegateCommand(ExecuteCloseAllWindowsPopupsCommand);

            MonitorOnAllCommand = new DelegateCommand(ExecuteMonitorOnAllCommand);
            MonitorOffAllCommand = new DelegateCommand(ExecuteMonitorOffAllCommand);
            OpenMonitorControlCommand = new DelegateCommand(ExecuteOpenMonitorControlCommand);

            ReloadCommand = new DelegateCommand(ExecuteReloadCommand);

            OpenAddPresetDialogCommand = new DelegateCommand(ExecuteOpenAddPresetDialogCommand);
            ToggleWallContentSelectionCommand = new DelegateCommand<string>(ExecuteToggleWallContentSelectionCommand);
            MouseDoubleClickWallContentCommand = new DelegateCommand<string>(ExecuteMouseDoubleClickWallContentCommand);
            OpenPreviewWindowCommand = new DelegateCommand<ContentItem>(ExecuteOpenPreviewWindowCommand);
            RunWallPresetCommand = new DelegateCommand<string>(ExecuteRunWallPresetCommand);
            CreatePopupCommand = new DelegateCommand(ExecuteCreatePopupCommand);
            ClosePopupCommand = new DelegateCommand(ExecuteClosePopupCommand);
            TogglePopupMovingCommand = new DelegateCommand(ExecuteTogglePopupMovingCommand);
            MouseDoubleClickWindowCommand = new DelegateCommand<string>(ExecuteMouseDoubleClickWindowCommand);
            KeyUpEventCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyUpCommand);
            foreach (var config in widgetSource.Config.ChildNodes)
            {
                string appType = config.GetValue("@appType", "");
                if (string.Compare(appType, "dataWall", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string appIp = config.GetValue("@ip", "");
                    int appPort = config.GetValue("@port", 0);
                    string appUri = config.GetValue("@uri", "");
                    if (string.IsNullOrWhiteSpace(appUri))
                    {
                        appUri = $"http://{appIp}:{appPort}/api/v1";
                    }

                    _dataWllApiUri = appUri;
                    SetDataWallApiClient((IConfiguration)config);
                    ThreadAction.PostOnUiThread(Reload, 100);
                    break;
                }
            }
        }

        private void ExecuteOpenPreviewWindowCommand(ContentItem contentItem)
        {
            if (contentItem == null)
            {
                return;
            }
            ContentItem item = contentItem;
            switch (contentItem.ContentType.ToLower())
            {
                case "remote":
                    item.SetProperty("isPreview", "true");
                    break;
                case "media":
                case "camera":
                case "web":
                case "image":
                //case "ptz-camera":
                    break;
                case "ptz-camera":
                    PtzCameraControl ptzCameraControl = new PtzCameraControl(item, false, false, _dialogService);
                    ptzCameraControl.Show();
                    return;
            }
            try
            {
                PreviewWindow remotePreview = new PreviewWindow(item);
                remotePreview.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteOpenPreviewWindowCommand error.");
            }
        }

        private void ExecuteSetUsingContentsTreeCommand(string isUsingContentsTreeString)
        {
            IsUsingContentsTree = StringUtils.GetValue(isUsingContentsTreeString, true);
        }

        private void ExecuteSelectCellCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            SelectCell(id);
            RaisePropertyChanged("IsEnableAddPopup");
        }


        private void ExecuteMergeCellCommand()
        {
            MergeSelectedCells();
        }

        private void ExecuteSplitCellCommand()
        {
            try
            {
                _dialogService.ShowDialog("CellSplitOptionDialog", _lastCellSplitOptionDialogParameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        _lastCellSplitOptionDialogParameters = r.Parameters;
                        SplitSelectedCell(r.Parameters.GetValue<int>("columns"), r.Parameters.GetValue<int>("rows"));
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSplitCellCommand:");
            }
        }
        private void ExecuteOpenMonitorControlCommand()
        {
            if (MonitorDevice == null) return;

            DialogParameters parameters = new DialogParameters();
            parameters.Add("deviceBaseUri", _monitorController.DeviceBaseUri);
            _dialogService.ShowDialog("MonitorControlDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                }
            });
        }
        private void ExecuteMonitorOnAllCommand()
        {
            _monitorController?.ControlPowerAll(true);
        }
        private void ExecuteMonitorOffAllCommand()
        {
            _monitorController?.ControlPowerAll(false);
        }

        private void ExecuteCloseCellCommand()
        {
            CloseSelectedCell();
        }
        private void ExecuteCloseAllWindowsPopupsCommand()
        {
            ExecuteCloseAllWindowsPopups();
        }
        private void ExecuteToggleWallContentSelectionCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            if (_selectedContent != null)
            {
                string selectedContentId = _selectedContent.Id;
                _selectedContent.IsSelected = false;
                _selectedContent = null;
                if (selectedContentId == id)
                {
                    return;
                }
            }

            ClearAllSelected();

            foreach (var content in _contents.ToArray())
            {
                if (content.Id == id)
                {
                    _selectedContent = content;
                    _selectedContent.IsSelected = true;
                }
            }
            RaisePropertyChanged("IsEnableAddPopup");
        }
        private void ExecuteMouseDoubleClickWallContentCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            JObject requestJson = new JObject()
            {
                ["windowRect"] = $"{_popupRect}",
                ["contentId"] = id
            };
            var response = _nodeViewApiClient.Post("/popups", requestJson);
            if (response.IsSuccess)
            {
                ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                ClearAllSelected();
            }
        }
        private void ExecuteRunWallPresetCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }

            ClearAllSelected();

            DataWallPresetBindable preset = FindWallPreset(id);
            if (preset != null)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Add("title", "프리셋 적용");
                parameters.Add("message",$"\"{preset.Name}\"을(를) 적용하시겠습니까?");
                _dialogService.ShowDialog("confirm", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        ApplyDataWallPreset(preset.Id);
                    }
                });
            }
        }
        private void ExecuteCreatePopupCommand()
        {
            if (_selectedContent == null) return;

            DialogParameters parameters = new DialogParameters();
            _dialogService.ShowDialog("GetPopupRectDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                    if (r.Parameters.TryGetValue("popupRect", out string popupRect))
                    {
                        JObject requestJson = new JObject()
                        {
                            ["windowRect"] = $"{popupRect}",
                            ["contentId"] = _selectedContent.Id
                        };
                        var response = _nodeViewApiClient.Post("/popups", requestJson);
                        if (response.IsSuccess)
                        {
                            try
                            {
                                _cacheManager.Set("popup", popupRect);
                                _popupRect = IntRect.CreateFrom(popupRect);
                            }
                            catch (Exception e)
                            {
                                Logger.Error(e, "SaveWidgetItems:");
                            }
                            ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                            ClearAllSelected();
                        }
                    }
                    RaisePropertyChanged("IsEnableAddPopup");
                }

            });
        }
        private void ExecuteClosePopupCommand()
        {
            CloseSelectedCell();
            ThreadAction.PostOnUiThread(CheckDataWallUpdate);
        }

        private void ExecuteTogglePopupMovingCommand()
        {
            if (IsPopupCellSelected)
            {
                IsPopupMovingMode = !IsPopupMovingMode;
                _selectedScreenCells.ForEach((x)=> x.IsMovingMode = IsPopupMovingMode);
            }
        }
        private void ExecuteMouseDoubleClickWindowCommand(string cellId)
        {
            var contentId = WallWindows.First(window => window.Id.Equals(cellId)).ContentId;
            JObject requestJson = new JObject()
            {
                ["windowRect"] = $"{_popupRect}",
                ["contentId"] = contentId
            };
            var response = _nodeViewApiClient.Post("/popups", requestJson);
            if (response.IsSuccess)
            {
                ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                ClearAllSelected();
            }
        }
        private void ExecuteKeyUpCommand(KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.Key)
            {
                case Key.Delete:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        break;
                    }

                    if (_selectedScreenCells != null)
                    {
                        CloseSelectedCell();
                    }
                    break;
            }
        }
        public bool IsUsingContentsTree
        {
            get => _isUsingContentsTree;
            set
            {
                SetProperty(ref _isUsingContentsTree, value);
                if (value)
                {
                    ContentsTreeVisibility = Visibility.Visible;
                    ContentsBoxVisibility = Visibility.Collapsed;
                    ContentsTreeWidth = "300";
                    ContentsBoxHeight = "0";

                    WallContentList.Clear();
                    WallContentFolder = ContentTreeViewFolder.CreateFrom(_contents);

                    WallPresetsH.Clear();
                    WallPresetsV.Clear();
                    WallPresetsV.AddRange(_presets);
                }
                else
                {
                    ContentsTreeVisibility = Visibility.Collapsed;
                    ContentsBoxVisibility = Visibility.Visible;
                    ContentsTreeWidth = "0";
                    ContentsBoxHeight = "320";

                    WallContentFolder.ClearItems();
                    WallContentList.Clear();
                    WallContentList.AddRange(_contents);

                    WallPresetsV.Clear();
                    WallPresetsH.Clear();
                    WallPresetsH.AddRange(_presets);
                }
            }
        }
        public string ContentsTreeWidth
        {
            get => _contentsTreeWidth;
            set => SetProperty(ref _contentsTreeWidth, value);
        }

        public string ContentsBoxHeight
        {
            get => _contentsBoxHeight;
            set => SetProperty(ref _contentsBoxHeight, value);
        }
        public Visibility ContentsBoxVisibility
        {
            get => _contentsBoxVisibility;
            set => SetProperty(ref _contentsBoxVisibility, value);
        }
        public Visibility ContentsTreeVisibility
        {
            get => _contentsTreeVisibility;
            set => SetProperty(ref _contentsTreeVisibility, value);
        }

        public IntSize ScreenSize
        {
            get => _screenSize;
            set => SetProperty(ref _screenSize, value);
        }

        private bool SetDataWallApiClient(IConfiguration appNodeConfig)
        {
            if (appNodeConfig == null) return false;

            try
            {
                string appIp = appNodeConfig.GetValue("@ip", "");
                int appPort = appNodeConfig.GetValue("@port", 0);
                if (string.IsNullOrWhiteSpace(appIp) || appPort <= 0)
                {
                    return false;
                }
                string appUri = appNodeConfig.GetValue("@uri", "");
                if (string.IsNullOrWhiteSpace(appUri))
                {
                    appUri = $"http://{appIp}:{appPort}/api/v1";
                }
                _nodeViewApiClient = new NodeViewApiClient(appUri);
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetDataWallApiClient:");
                _nodeViewApiClient = null;
            }

            return _nodeViewApiClient != null;
        }

        private void SelectCell(string id)
        {
            var selectedScreenCells = _selectedScreenCells.ToArray();
            if (_selectedContent != null && selectedScreenCells.Length == 0)
            {
                string selectedContentId = _selectedContent.Id;
                ClearAllSelected();
                SetContentOnScreenCell(id, selectedContentId);
                return;
            }
            foreach (var screenCell in selectedScreenCells)
            {
                if (screenCell.Id == id)
                {
                    ClearSelectedScreenCells();
                    UpdateSelectedCellProperties();
                    return;
                }
            }

            ScreenCellViewModel foundScreenCell = FindScreenCell(id);
            if (foundScreenCell == null)
            {
                return;
            }
            if (selectedScreenCells.Length == 0)
            {
                foundScreenCell.IsSelected = true;
                _selectedScreenCells.Add(foundScreenCell);
            }
            else
            {
                if (foundScreenCell.CellType == ScreenCellType.Layout)
                {
                    bool isHandled = false;
                    IntRect selectedRect = new IntRect(foundScreenCell.Rect);
                    foreach (var selectedScreenCell in selectedScreenCells)
                    {
                        if (selectedScreenCell.CellType != ScreenCellType.Layout)
                        {
                            ClearSelectedScreenCells();
                            foundScreenCell.IsSelected = true;
                            _selectedScreenCells.Add(foundScreenCell);
                            isHandled = true;
                            break;
                        }
                        selectedRect.Union(selectedScreenCell.Rect);
                    }

                    IntRect unionRect = new IntRect(selectedRect);
                    List<ScreenCellViewModel> newSelectedScreenCellList = new List<ScreenCellViewModel>();
                    if (!isHandled)
                    {
                        foreach (var screenCell in ScreenCells.ToArray())
                        {
                            if (selectedRect.IsIntersectsAreaWith(screenCell.Rect))
                            {
                                if (screenCell.CellType != ScreenCellType.Layout)
                                {
                                    ClearSelectedScreenCells();
                                    foundScreenCell.IsSelected = true;
                                    _selectedScreenCells.Add(foundScreenCell);
                                    isHandled = true;
                                    break;
                                }
                                newSelectedScreenCellList.Add(screenCell);
                                unionRect.Union(screenCell.Rect);
                            }
                        }

                        if (!isHandled)
                        {
                            if (selectedRect != unionRect)
                            {
                                ClearSelectedScreenCells();
                                foundScreenCell.IsSelected = true;
                                _selectedScreenCells.Add(foundScreenCell);
                            }
                            else
                            {
                                ClearSelectedScreenCells();
                                foreach (ScreenCellViewModel screenCell in newSelectedScreenCellList.ToArray())
                                {
                                    screenCell.IsSelected = true;
                                    _selectedScreenCells.Add(screenCell);
                                }
                            }
                        }
                    }
                }
                else
                {
                    ClearSelectedScreenCells();
                    foundScreenCell.IsSelected = true;
                    _selectedScreenCells.Add(foundScreenCell);
                }
            }
            UpdateSelectedCellProperties();
        }

        private void SetContentOnScreenCell(string screenCellId, string contentId)
        {
            var screenCell = FindScreenCell(screenCellId);
            if (screenCell != null && !string.IsNullOrWhiteSpace(contentId))
            {

                if (screenCell.IsPopup)
                {
                    var popupRect = screenCell.Rect;

                    JArray windowIds = new JArray {screenCell.Id};

                    JObject requestJson = new JObject
                    {
                        ["action"] = "close",
                        ["param"] = new JObject()
                        {
                            ["ids"] = windowIds
                        }
                    };
                    var response = _nodeViewApiClient.Put("/popups", requestJson);

                    requestJson = new JObject()
                    {
                        ["windowRect"] = $"{popupRect}",
                        ["contentId"] = contentId
                    };
                    response = _nodeViewApiClient.Post("/popups", requestJson);
                    if (response.IsSuccess)
                    {
                        ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                        ClearAllSelected();
                    }
                }
                else
                {
                    CreateDataWallWindow(screenCell.Rect, contentId);
                }
            }
        }

        private ScreenCellViewModel FindScreenCell(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            ScreenCellViewModel foundScreenCell = null;
            foreach (var cell in ScreenCells.ToArray())
            {
                if (cell.Id == id)
                {
                    foundScreenCell = cell;
                    break;
                }
            }

            return foundScreenCell;
        }
        private void ClearSelectedScreenCells()
        {
            foreach (var screenCell in _selectedScreenCells)
            {
                screenCell.IsSelected = false;
                screenCell.IsMovingMode = false;
            }
            _selectedScreenCells.Clear();
            UpdateSelectedCellProperties();
        }

        private void ClearAllSelected()
        {
            ClearSelectedScreenCells();
            if (_selectedContent != null)
            {
                _selectedContent.IsSelected = false;
                _selectedContent = null;
            }
        }

        private void UpdateSelectedCellProperties()
        {
            bool isLayoutCellSelected = false;
            bool isMultiLayoutCellSelected = false;
            bool isWindowCellSelected = false;
            bool isPopupCellSelected = false;
            bool isPopupMovingMode = false;
            var selectedScreenCells = _selectedScreenCells.ToArray();
            if (selectedScreenCells.Length == 1)
            {
                isLayoutCellSelected = selectedScreenCells[0].CellType == ScreenCellType.Layout;
                isMultiLayoutCellSelected = false;
                isWindowCellSelected = selectedScreenCells[0].CellType == ScreenCellType.Window;
                isPopupCellSelected = selectedScreenCells[0].CellType == ScreenCellType.Popup;
                isPopupMovingMode = selectedScreenCells[0].IsMovingMode;
            }
            else if (selectedScreenCells.Length > 1)
            {
                isLayoutCellSelected = true;
                isMultiLayoutCellSelected = true;
                isWindowCellSelected = false;
                isPopupCellSelected = false;
            }
            IsLayoutCellSelected = isLayoutCellSelected;
            IsMultiLayoutCellSelected = isMultiLayoutCellSelected;
            IsWindowCellSelected = isWindowCellSelected;
            IsPopupCellSelected = isPopupCellSelected;
            IsPopupMovingMode = isPopupMovingMode;
        }

        private ScreenCellViewModel MergeAllCells()
        {
            ClearSelectedScreenCells();

            ScreenCells.Clear();
            ScreenCellViewModel mergeScreenCell = new ScreenCellViewModel(new IntRect(0, 0, _screenSize.Width, _screenSize.Height));
            ScreenCells.Insert(0, mergeScreenCell);
            return mergeScreenCell;
        }

        private ScreenCellViewModel MergeSelectedCells()
        {
            if (_selectedScreenCells.Count <= 1)
            {
                return null;
            }
            var selectedScreenCells = _selectedScreenCells.ToArray();
            ClearSelectedScreenCells();

            IntRect newScreenRect = IntRect.Empty;
            foreach (var screenCell in selectedScreenCells)
            {
                if (newScreenRect.IsEmpty)
                {
                    newScreenRect = screenCell.Rect;
                }
                else
                {
                    newScreenRect.Union(screenCell.Rect);
                }
                ScreenCells.Remove(screenCell);
            }

            ScreenCellViewModel mergeScreenCell = new ScreenCellViewModel(newScreenRect);
            ScreenCells.Insert(0, mergeScreenCell);
            return mergeScreenCell;
        }

        private void SplitSelectedCell(int columns, int rows)
        {
            if (columns <= 0 || rows <= 0)
            {
                return;
            }
            if (_selectedScreenCells.Count == 0)
            {
                return;
            }
            ScreenCellViewModel targetScreenCell = _selectedScreenCells[0];
            if (_selectedScreenCells.Count > 1)
            {
                targetScreenCell = MergeSelectedCells();
            }

            if (targetScreenCell == null)
            {
                return;
            }
            ClearSelectedScreenCells();

            ScreenCellViewModel[] newScreenCells = GetDividedCells(targetScreenCell.Rect, columns, rows);
            ScreenCells.Remove(targetScreenCell);
            foreach (ScreenCellViewModel newScreenCell in newScreenCells)
            {
                ScreenCells.Insert(0, newScreenCell);
            }
        }

        private void CloseSelectedCell()
        {
            if (_selectedScreenCells.Count == 1)
            {
                ScreenCellViewModel selectedScreenCell = _selectedScreenCells[0];
                ClearAllSelected();
                if (selectedScreenCell.CellType == ScreenCellType.Window)
                {
                    CloseDataWallWindow(selectedScreenCell.Id);
                }
                else if (selectedScreenCell.CellType == ScreenCellType.Popup)
                {
                    CloseDataWallPopup(selectedScreenCell.Id);
                }
            }
        }

        private void ExecuteCloseAllWindowsPopups()
        {
            if (WallWindows.Count > 0)
            {
                foreach (var wallWindow in WallWindows)
                {
                    CloseDataWallWindow(wallWindow.Id);
                }
            }

            if (WallPopupWindows.Count > 0)
            {
                foreach (var wallPopupWindow in WallPopupWindows)
                {
                    CloseDataWallPopup(wallPopupWindow.Id);
                }
            }
        }
        private void ExecuteReloadCommand()
        {
            ThreadAction.PostOnUiThread(Reload);
        }
        private void ExecuteOpenAddPresetDialogCommand()
        {
            void OpenAddPreSetDialog()
            {
                var parameters = new DialogParameters
                {
                    {"clientBaseUrl", $"{_nodeViewApiClient.BaseUrl}"}, {"wallPresets", _presets.ToArray()}
                };
                _dialogService.ShowDialog("AddPresetDialog", parameters, addPresetResult =>
                {
                    Task.Run(GetDataWallPresets);
                });
            }

            if (!_nodeViewApiClient.HasAccessToken)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Add("apiClient", _nodeViewApiClient);
                _dialogService.ShowDialog("LoginDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        OpenAddPreSetDialog();
                    }
                });
            }
            else
            {
                OpenAddPreSetDialog();
            }

        }
        private void Reload()
        {
            ReloadInternal();
        }

        private void ReloadInternal()
        {
            CheckDataWallUpdate();
        }

        private void CheckDataWallUpdate()
        {
            if (_nodeViewApiClient == null) return;

            try
            {
                var response = _nodeViewApiClient.Get("/infos");
                if (response.IsSuccess && response.IsJsonResponse)
                {
                    var responseJson = response.ResponseJsonObject;
                    IntSize screenSize = JsonUtils.GetValue<IntSize>(responseJson, "screen.size", null);
                    if (screenSize == null || screenSize.IsEmpty)
                    {
                        Logger.Error("content screenSize is empty.");
                    }
                    else
                    {
                        //if (!ScreenSize.Equals(screenSize))
                        if (ScreenSize != screenSize)
                        {
                            ScreenCells.Clear();
                            ScreenSize = screenSize;
                            if ((screenSize.Width / screenSize.Height) >= 3)
                            {
                                IsUsingContentsTree = false;
                            }
                        }
                    }

                    IntPoint screenMatrix = JsonUtils.GetValue<IntPoint>(responseJson, "screen.matrix", null);
                    if (screenMatrix != null && screenMatrix.X > 0 && screenMatrix.Y > 0)
                    {
                        _screenMatrix = screenMatrix;
                    }

                    bool needToUpdateContents = false;
                    string lastContentsUpdated = JsonUtils.GetStringValue(responseJson, "contents.updated");
                    if (_lastContentsUpdated != lastContentsUpdated)
                    {
                        _lastContentsUpdated = lastContentsUpdated;
                        needToUpdateContents = true;
                    }

                    bool needToUpdateScreen = false;
                    string lastScreenUpdated = JsonUtils.GetStringValue(responseJson, "screen.updated");
                    if (_lastScreenUpdated != lastScreenUpdated)
                    {
                        _lastScreenUpdated = lastScreenUpdated;
                        needToUpdateScreen = true;
                    }

                    bool needToUpdatePresets = false;
                    string lastPresetsUpdated = JsonUtils.GetStringValue(responseJson, "presets.updated");
                    if (_lastPresetsUpdated != lastPresetsUpdated)
                    {
                        _lastPresetsUpdated = lastPresetsUpdated;
                        needToUpdatePresets = true;
                    }

                    UpdateMonitorDeviceFromDataWall();
                    if (needToUpdateContents)
                    {
                        //GetDataWallContents();
                        Task.Factory.StartNew(GetDataWallContents);
                    }
                    if (needToUpdateScreen)
                    {
                        //GetDataWallWindows();
                        Task.Factory.StartNew(GetDataWallWindows);
                    }
                    if (needToUpdatePresets)
                    {
                        //GetDataWallPresets();
                        Task.Delay(200).ContinueWith(t=>GetDataWallPresets());
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CheckUpdate:");
            }
        }

        private void GetDataWallContents()
        {
            try
            {

                var response = _nodeViewApiClient.Get("/contents");
                if (response.IsSuccess && response.IsJsonResponse)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        List<ContentItem> contentList = new List<ContentItem>();
                        var responseArray = response.ResponseJsonArray;
                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                var nodeViewContent = ContentItem.CreateFrom(json);
                                if (nodeViewContent != null)
                                {
                                    contentList.Add(nodeViewContent);
                                }
                            }
                        }

                        _contents = contentList;
                        ThreadAction.PostOnUiThread(() =>
                        {
                            if (IsUsingContentsTree)
                            {
                                WallContentFolder = ContentTreeViewFolder.CreateFrom(_contents);
                            }
                            else
                            {
                                WallContentList.Clear();
                                foreach (var contentItem in _contents.ToArray())
                                {
                                    WallContentList.Add(contentItem);
                                }
                            }
                        });
                    }
                    else
                    {
                        Logger.Error("reponse of contents is not an array.");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetDataWallContents:");
            }
        }

        private void GetDataWallPresets()
        {
            try
            {
                var response = _nodeViewApiClient.Get("/presets");
                if (response.IsSuccess)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        List<DataWallPresetBindable> presetList = new List<DataWallPresetBindable>();
                        var responseArray = response.ResponseJsonArray;
                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                var nodeViewPreset = DataWallPresetBindable.CreateFrom(json);
                                if (nodeViewPreset != null)
                                {
                                    presetList.Add(nodeViewPreset);
                                }
                            }
                        }
                        _presets = presetList;
                        ThreadAction.PostOnUiThread(() =>
                        {
                            if (IsUsingContentsTree)
                            {
                                WallPresetsV.Clear();
                                foreach (var preset in _presets.ToArray())
                                {
                                    WallPresetsV.Add(preset);
                                }
                            }
                            else
                            {
                                WallPresetsH.Clear();
                                foreach (var preset in _presets.ToArray())
                                {
                                    WallPresetsH.Add(preset);
                                }
                            }
                        });
                    }
                    else
                    {
                        Logger.Error("reponse of presets is not an array.");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetDataWallPresets:");
            }
        }
        private void UpdateMonitorDeviceFromDataWall()
        {

            if (_monitorController == null)
            {
                _monitorController = new NodeViewMonitorControllerClient($"{_dataWllApiUri}/devices/monitor");
            }
            MonitorDevice = _monitorController.GetDevice();
        }
        private void GetDataWallWindows()
        {
            bool needToUpdate = false;
            List<DataWallWindowBindable> windowList = new List<DataWallWindowBindable>();
            List<DataWallWindowBindable> popupList = new List<DataWallWindowBindable>();
            try
            {
                var response = _nodeViewApiClient.Get("/windows");
                if (response.IsSuccess)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        var responseArray = response.ResponseJsonArray;
                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                var nodeViewWindow = DataWallWindowBindable.CreateFrom(json);
                                if (nodeViewWindow != null)
                                {
                                    windowList.Add(nodeViewWindow);
                                    ExistWindows = true;
                                }
                            }
                        }
                        if(responseArray.Count == 0)
                        {
                            ExistWindows = false;
                        }
                        needToUpdate = true;
                    }
                    else
                    {
                        Logger.Error("reponse of windows is not an array.");
                    }
                }

                response = _nodeViewApiClient.Get("/popups");
                if (response.IsSuccess)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        var responseArray = response.ResponseJsonArray;
                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                var nodeViewWindow = DataWallWindowBindable.CreateFrom(json);
                                if (nodeViewWindow != null)
                                {
                                    popupList.Add(nodeViewWindow);
                                    ExistWindows = true;
                                }
                            }
                            if (responseArray.Count == 0)
                            {
                                ExistWindows = false;
                            }
                        }
                        needToUpdate = true;
                    }
                    else
                    {
                        Logger.Error("reponse of popups is not an array.");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetDataWallWindows:");
            }

            if (needToUpdate)
            {
                ThreadAction.PostOnUiThread(() =>
                {
                    WallWindows.Clear();
                    WallWindows.AddRange(windowList);
                    WallPopupWindows.Clear();
                    WallPopupWindows.AddRange(popupList);
                    UpdateScreen();
                });
            }
        }

        private void CreateDataWallWindow(IntRect windowRect, string contentId)
        {
            if (string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
            {
                return;
            }
            try
            {
                JObject requestJson = new JObject()
                {
                    ["windowRect"] = $"{windowRect}",
                    ["contentId"] = contentId
                };
                var response = _nodeViewApiClient.Post("/windows", requestJson);
                if (response.IsSuccess)
                {
                    ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateDataWallWindow:");
            }
        }

        private void CloseDataWallWindow(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            try
            {
                JArray windowIds = new JArray();
                windowIds.Add(id);

                JObject requestJson = new JObject
                {
                    ["action"] = "close",
                    ["param"] = new JObject()
                    {
                        ["ids"] = windowIds
                    }
                };

                var response = _nodeViewApiClient.Put("/windows", requestJson);
                if (response.IsSuccess)
                {
                    ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CloseDataWallWindow:");
            }
        }

        private void CloseDataWallPopup(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            try
            {
                JArray windowIds = new JArray();
                windowIds.Add(id);

                JObject requestJson = new JObject
                {
                    ["action"] = "close",
                    ["param"] = new JObject()
                    {
                        ["ids"] = windowIds
                    }
                };

                var response = _nodeViewApiClient.Put("/popups", requestJson);
                if (response.IsSuccess)
                {
                    ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CloseDataWallPopup:");
            }
        }

        private void ApplyDataWallPreset(string presetId)
        {
            if (string.IsNullOrWhiteSpace(presetId))
            {
                return;
            }
            try
            {
                MergeAllCells();

                JObject requestJson = new JObject
                {
                    ["action"] = "applyPreset",
                    ["param"] = new JObject()
                    {
                        ["id"] = presetId
                    }
                };

                var response = _nodeViewApiClient.Put("/windows", requestJson);
                if (response.IsSuccess)
                {
                    ThreadAction.PostOnUiThread(CheckDataWallUpdate);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ApplyDataWallPreset:");
            }
        }

        private DataWallPresetBindable FindWallPreset(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            foreach (var preset in _presets.ToArray())
            {
                if (preset.Id == id)
                {
                    return preset;
                }
            }

            return null;
        }

        private void UpdateScreen()
        {
            if (ScreenCells.Count == 0)
            {
                if (WallWindows.Count == 0)
                {
                    var screenCells = GetDividedCells(new IntRect(0, 0, ScreenSize.Width, ScreenSize.Height), _screenMatrix.X, _screenMatrix.Y);
                    ScreenCells.AddRange(screenCells);
                }
                else
                {
                    MergeAllCells();
                }
            }

            List<ScreenCellViewModel> newScreenCellList = new List<ScreenCellViewModel>();
            foreach (var screenCell in ScreenCells.ToArray())
            {
                if (!screenCell.IsPopup)
                {
                    newScreenCellList.Add(new ScreenCellViewModel()
                    {
                        Rect = screenCell.Rect,
                    });
                }
            }

            foreach (var window in WallWindows.ToArray())
            {
                var currentCells = newScreenCellList.ToArray();
                foreach (var screenCell in currentCells)
                {
                    if (window.WindowRect.IsIntersectsAreaWith(screenCell.Rect))
                    {
                        var dividedCells = GetDividedCells(screenCell.Rect, window.WindowRect);
                        if (!newScreenCellList.Any(screen => screen.Id.Equals(window.Id, StringComparison.OrdinalIgnoreCase)))
                        {
                            newScreenCellList.Add(new ScreenCellViewModel(window.WindowRect)
                            {
                                Id = window.Id,
                                Name = window.Name,
                                IsPopup = false,
                                CellType = ScreenCellType.Window

                            });
                        }
                        newScreenCellList.Remove(screenCell);
                        foreach (var dividedCell in dividedCells)
                        {
                            if (!window.WindowRect.IsIntersectsAreaWith(dividedCell.Rect))
                            {
                                newScreenCellList.Add(dividedCell);
                            }
                        }
                    }
                }
            }
            foreach (var popupWindow in WallPopupWindows.ToArray())
            {
                newScreenCellList.Add(new ScreenCellViewModel(popupWindow.WindowRect)
                {
                    Id = popupWindow.Id,
                    Name = popupWindow.Name,
                    IsPopup = true,
                    CellType = ScreenCellType.Popup
                });
            }
            ScreenCells.Clear();
            ScreenCells.AddRange(newScreenCellList);
        }

        public static ScreenCellViewModel[] GetDividedCells(IntRect targetRect, IntRect sourceRect)
        {
            List<IntPoint> containsIntPoints = new List<IntPoint>();
            if (sourceRect.IsIntersectsAreaWith(targetRect))
            {
                if (targetRect.Contains(sourceRect.LeftTop))
                {
                    containsIntPoints.Add(sourceRect.LeftTop);
                }
                if (targetRect.Contains(sourceRect.RightTop))
                {
                    containsIntPoints.Add(sourceRect.RightTop);
                }
                if (targetRect.Contains(sourceRect.LeftBottom))
                {
                    containsIntPoints.Add(sourceRect.LeftBottom);
                }
                if (targetRect.Contains(sourceRect.RightBottom))
                {
                    containsIntPoints.Add(sourceRect.RightBottom);
                }
            }

            return GetDividedCells(targetRect, containsIntPoints.ToArray());
        }

        private static ScreenCellViewModel[] GetDividedCells(IntRect targetRect, IntPoint[] dividePoints)
        {
            List<IntRect> newRects = new List<IntRect>();
            newRects.Add(new IntRect(targetRect));

            foreach (var dividePoint in dividePoints)
            {
                var rects = newRects.ToArray();
                newRects.Clear();

                foreach (var rect in rects)
                {
                    if (rect.Contains(dividePoint))
                    {
                        IntRect newRect = new IntRect(rect.Left, rect.Top, (dividePoint.X - rect.Left), (dividePoint.Y - rect.Top));
                        if (!newRect.IsEmpty)
                        {
                            newRects.Add(newRect);
                        }
                        newRect = new IntRect(dividePoint.X, rect.Top, (rect.Right - dividePoint.X), (dividePoint.Y - rect.Top));
                        if (!newRect.IsEmpty)
                        {
                            newRects.Add(newRect);
                        }
                        newRect = new IntRect(rect.Left, dividePoint.Y, (dividePoint.X - rect.Left), (rect.Bottom - dividePoint.Y));
                        if (!newRect.IsEmpty)
                        {
                            newRects.Add(newRect);
                        }
                        newRect = new IntRect(dividePoint.X, dividePoint.Y, (rect.Right - dividePoint.X), (rect.Bottom - dividePoint.Y));
                        if (!newRect.IsEmpty)
                        {
                            newRects.Add(newRect);
                        }
                    }
                    else
                    {
                        newRects.Add(rect);
                    }
                }
            }

            List<ScreenCellViewModel> cells = new List<ScreenCellViewModel>();
            foreach (var rect in newRects)
            {
                if (!rect.IsEmpty)
                {
                    cells.Add(new ScreenCellViewModel(rect));
                }
            }

            return cells.ToArray();
        }

        public static ScreenCellViewModel[] GetDividedCells(IntRect targetRect, int divideColumns, int divideRows)
        {
            double dx = (double)targetRect.Width / (double)divideColumns;
            double dy = (double)targetRect.Height / (double)divideRows;

            List<ScreenCellViewModel> celList = new List<ScreenCellViewModel>();
            for (int y = 0; y < divideRows; y++)
            {
                int y1 = (int)(y * dy);
                int y2 = (int)((y + 1) * dy);
                for (int x = 0; x < divideColumns; x++)
                {
                    int x1 = (int)(x * dx);
                    int x2 = (int)((x + 1) * dx);
                    celList.Add(new ScreenCellViewModel()
                    {
                        Rect = new IntRect(targetRect.X + x1, targetRect.Y + y1, (x2 - x1), (y2 - y1))
                    });
                }
            }

            return celList.ToArray();
        }
    }
}
