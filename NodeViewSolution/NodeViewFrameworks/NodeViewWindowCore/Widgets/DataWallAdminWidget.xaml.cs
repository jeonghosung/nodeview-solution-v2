﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NodeView.Frameworks.Widgets;

namespace NodeView.Widgets
{
    /// <summary>
    /// DataWallAdminWidget.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataWallAdminWidget : UserControl, IWidgetControl
    {
        public DataWallAdminWidget()
        {
            InitializeComponent();
        }

        public IWidgetSource Source
        {
            get => (IWidgetSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IWidgetSource), typeof(DataWallAdminWidget));

       
    }
}
