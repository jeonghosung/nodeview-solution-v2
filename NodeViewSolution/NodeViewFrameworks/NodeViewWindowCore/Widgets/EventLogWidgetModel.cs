﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using NodeView.Tasks;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.Widgets
{
    public class EventLogWidgetModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDialogService _dialogService = null;
        private NodeViewApiClient _nodeViewCenterApiClient = null;
        private Timer _checkingLatestEventLogTimer = null;

        private ObservableCollection<EventLog> _recentEventLogList = new ObservableCollection<EventLog>();

        private DelegateCommand _openEventLogSearchDialogCommand;

        public ObservableCollection<EventLog> RecentEventLogList
        {
            get => _recentEventLogList;
            set => SetProperty(ref _recentEventLogList, value);
        }
        public DelegateCommand OpenEventLogSearchDialogCommand => _openEventLogSearchDialogCommand ?? (_openEventLogSearchDialogCommand = new DelegateCommand(OpenEventLogSearchDialog));

        public EventLogWidgetModel(IDialogService dialogService, IWidgetSource widgetSource)
        {
            _dialogService = dialogService;
            Init(widgetSource);
        }
        private void Init(IWidgetSource widgetSource)
        {
            foreach (var config in widgetSource.Config.ChildNodes)
            {
                string appType = config.GetValue("@appType", "");
                if (string.Compare(appType, "eventLog", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string appIp = config.GetValue("@ip", "");
                    int appPort = config.GetValue("@port", 0);
                    string appUri = config.GetValue("@uri", "");
                    if (string.IsNullOrWhiteSpace(appUri))
                    {
                        appUri = $"http://{appIp}:{appPort}/api/v1";
                    }

                    SetCenterApiClient((IConfiguration)config);
                    break;
                }
            }

            _checkingLatestEventLogTimer = new Timer(OnCheckingLatestEventLogTimer);
            _checkingLatestEventLogTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }
        private bool SetCenterApiClient(IConfiguration appNodeConfig)
        {
            if (appNodeConfig == null) return false;

            try
            {
                string appIp = appNodeConfig.GetValue("@ip", "");
                int appPort = appNodeConfig.GetValue("@port", 0);
                if (string.IsNullOrWhiteSpace(appIp) || appPort <= 0)
                {
                    return false;
                }
                string appUri = appNodeConfig.GetValue("@uri", "");
                if (string.IsNullOrWhiteSpace(appUri))
                {
                    appUri = $"http://{appIp}:{appPort}/api/v1";
                }
                _nodeViewCenterApiClient = new NodeViewApiClient(appUri);
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetCenterApiClient:");
                _nodeViewCenterApiClient = null;
            }

            return _nodeViewCenterApiClient != null;
        }
        private void OnCheckingLatestEventLogTimer(object state)
        {
            _checkingLatestEventLogTimer.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                ThreadAction.RunOnUiThread(GetRecentLog);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "GetRecentLog Error.");
            }
            _checkingLatestEventLogTimer.Change(TimeSpan.FromMinutes(1), TimeSpan.FromMinutes(1));
        }
        private void GetRecentLog()
        {
            var response = _nodeViewCenterApiClient.Get("/eventLogs");
            if (response.IsSuccess && response.IsJsonResponse)
            {
                if (response.ResponseType == RestApiResponseType.JsonArray)
                {
                    RecentEventLogList.Clear();
                    var responseArray = response.ResponseJsonArray;
                    foreach (var token in responseArray)
                    {
                        JObject json = token.Value<JObject>();
                        if (json != null)
                        {
                            var eventLog = EventLog.CreateFrom(json);
                            if (eventLog != null)
                            {
                                RecentEventLogList.Add(eventLog);
                            }
                        }
                    }
                }
                else
                {
                    Logger.Error("reponse of contents is not an array.");
                }
            }
        }
        private void OpenEventLogSearchDialog()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "EventLog");
            parameters.Add("apiclient", _nodeViewCenterApiClient);
            _dialogService.ShowDialog("EventLogSearchDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                }
            });
        }
    }
}
