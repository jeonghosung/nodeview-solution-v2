﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using NodeView.Protocols.NodeViews;
using NodeView.Tasks;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace NodeView.Widgets
{
    public class IotStationAdminWidgetModel : StationAndProcessAdminBaseViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDialogService _dialogService;
        private DelegateCommand _changePasswordCommand;

        public DelegateCommand ChangePasswordCommand => _changePasswordCommand ?? (_changePasswordCommand = new DelegateCommand(ExecuteChangePasswordCommand));

        public IotStationAdminWidgetModel(IDialogService dialogService, IWidgetSource widgetSource) : base(dialogService)
        {
            _dialogService = dialogService;

            foreach (var config in widgetSource.Config.ChildNodes)
            {
                string appType = config.GetValue("@appType", "");
                if (string.Compare(appType, "iotStation", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    InitStation((IConfiguration)config);
                    ThreadAction.PostOnUiThread(Reload);
                    break;
                }
            }
        }

        private void ExecuteChangePasswordCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "비밀번호 변경");
            parameters.Add("nodeViewApiClient", _nodeViewApiClient);
            _dialogService.ShowDialog("ChangePasswordDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                }
            });
        }
    }
}