﻿using System.Windows;
using System.Windows.Controls;
using NodeView.Frameworks.Widgets;

namespace NodeView.Widgets
{
    /// <summary>
    /// DummyWidget.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DummyWidget : UserControl, IWidgetControl
    {
        public DummyWidget()
        {
            InitializeComponent();
        }

        public IWidgetSource Source
        {
            get => (IWidgetSource)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IWidgetSource), typeof(DummyWidget), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }
    }
}
