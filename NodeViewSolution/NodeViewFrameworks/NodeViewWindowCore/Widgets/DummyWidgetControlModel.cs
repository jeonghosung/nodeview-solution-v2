﻿using NLog;
using NodeView.Frameworks.Widgets;
using Prism.Mvvm;

namespace NodeView.Widgets
{
    public class DummyWidgetControlModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _name;
        public string Id { get; }
        public string ContentType { get; }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }
        public DummyWidgetControlModel(IWidgetSource content)
        {
            Id = content.Id;
            ContentType = content.ContentType;
            _name = content.Name;
        }
    }
}
