﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Net;
using NodeView.Tasks;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using NodeView.Frameworks.Widgets;
using NodeView.Protocols.NodeViews;

namespace NodeView.Widgets
{
    public class NodeViewCenterWidgetModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private NodeViewProcessManagerClient _processManagerClient;
        private RestApiClient _centerApiClient = null;
        private IDialogService _dialogService = null;
        private DelegateCommand<string> _selectAppLicenseCommand;
        private DelegateCommand<string> _removeAppCommand;
        private DelegateCommand<string> _blockAppCommand;
        private DelegateCommand<string> _editAppCommand;
        private DelegateCommand _selectDevicesAppCommand;
        private DelegateCommand<string> _runSelectedDeviceCommandCommand;
        private DelegateCommand _reloadSelectedDeviceNodesCommand;

        //private DelegateCommand _selectActionsAppCommand;
        private DelegateCommand<string> _selectActionCommand;
        private DelegateCommand _createNewActionCommand;
        private DelegateCommand _duplicateSelectedActionCommand;
        private DelegateCommand _editSelectedActionCommand;
        private DelegateCommand _deleteSelectedActionCommand;

        private DelegateCommand _createNewEventActionCommand;
        private DelegateCommand<string> _duplicateSelectedEventActionCommand;
        private DelegateCommand<string> _editSelectedEventActionCommand;
        private DelegateCommand<string> _deleteSelectedEventActionCommand;
        private DelegateCommand<NodeViewAppItem> _stopControlCommand;
        private DelegateCommand<NodeViewAppItem> _startControlCommand;
        private DelegateCommand<NodeViewAppItem> _reStartControlCommand;
        private DelegateCommand _reloadCommand;

        private AppLicenseItem _selectedAppLicense;
        private INodeViewAppNode _centerAppNode;
        private INodeViewAppNode _selectedDevicesApp;
        //private INodeViewAppNode _selectedActionsApp;
        //private INodeViewAppNode _selectedEventActionsApp;
        private DeviceViewModel _selectedDevice;
        private NodeViewActionViewModel _selectedAction;

        private Visibility _duplicateSelectedActionVisibility = Visibility.Collapsed;
        private Visibility _editSelectedActionVisibility = Visibility.Collapsed;
        private Visibility _deleteSelectedActionVisibility = Visibility.Collapsed;

        private DelegateCommand<string> _selectDeviceCommand;
        //private EventActionViewModel _selectedEventAction;
        private string _selectedLog = "";
        

        public DelegateCommand<string> SelectAppLicenseCommand => _selectAppLicenseCommand ?? (_selectAppLicenseCommand = new DelegateCommand<string>(ExecuteSelectAppLicenseCommand));
        public DelegateCommand<string> RemoveAppCommand => _removeAppCommand ?? (_removeAppCommand = new DelegateCommand<string>(ExecuteRemoveAppCommand));
        public DelegateCommand<string> BlockAppCommand => _blockAppCommand ?? (_blockAppCommand = new DelegateCommand<string>(ExecuteBlockAppCommand));
        public DelegateCommand<string> EditAppCommand => _editAppCommand ?? (_editAppCommand = new DelegateCommand<string>(ExecuteEditAppCommand));

        public DelegateCommand SelectDevicesAppCommand => _selectDevicesAppCommand ?? (_selectDevicesAppCommand = new DelegateCommand(ExecuteSelectDevicesAppCommand));

        public DelegateCommand<string> SelectDeviceCommand => _selectDeviceCommand ?? (_selectDeviceCommand = new DelegateCommand<string>(ExecuteSelectDeviceCommand));

        public DelegateCommand<string> RunSelectedDeviceCommandCommand => _runSelectedDeviceCommandCommand ?? (_runSelectedDeviceCommandCommand = new DelegateCommand<string>(ExecuteRunSelectedDeviceCommandCommand));
        public DelegateCommand ReloadSelectedDeviceNodesCommand => _reloadSelectedDeviceNodesCommand ?? (_reloadSelectedDeviceNodesCommand = new DelegateCommand(ExecuteReloadSelectedDeviceNodesCommand));
        //public DelegateCommand SelectActionsAppCommand => _selectActionsAppCommand ?? (_selectActionsAppCommand = new DelegateCommand(ExecuteSelectActionsAppCommand));
        public DelegateCommand<string> SelectActionCommand => _selectActionCommand ?? (_selectActionCommand = new DelegateCommand<string>(ExecuteSelectActionCommand));
        public DelegateCommand CreateNewActionCommand => _createNewActionCommand ?? (_createNewActionCommand = new DelegateCommand(ExecuteCreateNewActionCommand));
        public DelegateCommand DuplicateSelectedActionCommand => _duplicateSelectedActionCommand ?? (_duplicateSelectedActionCommand = new DelegateCommand(ExecuteDuplicateSelectedActionCommand));
        public DelegateCommand EditSelectedActionCommand => _editSelectedActionCommand ?? (_editSelectedActionCommand = new DelegateCommand(ExecuteEditSelectedActionCommand));
        public DelegateCommand DeleteSelectedActionCommand => _deleteSelectedActionCommand ?? (_deleteSelectedActionCommand = new DelegateCommand(ExecuteDeleteSelectedActionCommand));

        public DelegateCommand CreateNewEventActionCommand => _createNewEventActionCommand ?? (_createNewEventActionCommand = new DelegateCommand(ExecuteCreateNewEventActionCommand));
        public DelegateCommand<string> DuplicateSelectedEventActionCommand => _duplicateSelectedEventActionCommand ?? (_duplicateSelectedEventActionCommand = new DelegateCommand<string>(ExecuteDuplicateSelectedEventActionCommand));
        public DelegateCommand<string> EditSelectedEventActionCommand => _editSelectedEventActionCommand ?? (_editSelectedEventActionCommand = new DelegateCommand<string>(ExecuteEditSelectedEventActionCommand));
        public DelegateCommand<string> DeleteSelectedEventActionCommand => _deleteSelectedEventActionCommand ?? (_deleteSelectedEventActionCommand = new DelegateCommand<string>(ExecuteDeleteSelectedEventActionCommand));

        public DelegateCommand<NodeViewAppItem> StopControlCommand => _stopControlCommand ?? (_stopControlCommand = new DelegateCommand<NodeViewAppItem>(ExecuteStopControlCommand));
        public DelegateCommand<NodeViewAppItem> StartControlCommand => _startControlCommand ?? (_startControlCommand = new DelegateCommand<NodeViewAppItem>(ExecuteStartControlCommand));
        public DelegateCommand<NodeViewAppItem> ReStartControlCommand => _reStartControlCommand ?? (_reStartControlCommand = new DelegateCommand<NodeViewAppItem>(ExecuteReStartControlCommand));
        public DelegateCommand ReloadCommand => _reloadCommand ?? (_reloadCommand = new DelegateCommand(ExecuteReloadCommand));
        public ObservableCollection<AppLicenseItem> AppLicenses { get; } = new ObservableCollection<AppLicenseItem>();
        public ObservableCollection<EventActionViewModel> EventActions { get; } = new ObservableCollection<EventActionViewModel>();
        public ObservableCollection<NodeViewActionViewModel> Actions { get; } = new ObservableCollection<NodeViewActionViewModel>();
        public ObservableCollection<DeviceViewModel> Devices { get; } = new ObservableCollection<DeviceViewModel>();
        public ObservableCollection<string> LogFiles { get; } = new ObservableCollection<string>();
        public Visibility DuplicateSelectedActionVisibility
        {
            get => _duplicateSelectedActionVisibility;
            set => SetProperty(ref _duplicateSelectedActionVisibility, value);
        }

        public Visibility EditSelectedActionVisibility
        {
            get => _editSelectedActionVisibility;
            set => SetProperty(ref _editSelectedActionVisibility, value);
        }

        public Visibility DeleteSelectedActionVisibility
        {
            get => _deleteSelectedActionVisibility;
            set => SetProperty(ref _deleteSelectedActionVisibility, value);
        }

        public AppLicenseItem SelectedAppLicense
        {
            get => _selectedAppLicense;
            set
            {
                if (_selectedAppLicense == value) return;

                if (_selectedAppLicense != null) _selectedAppLicense.IsSelected = false;
                if (value != null) value.IsSelected = true;
                SetProperty(ref _selectedAppLicense, value);
            }
        }

        public INodeViewAppNode SelectedDevicesApp
        {
            get => _selectedDevicesApp;
            set
            {
                if (_selectedDevicesApp == value) return;
                SetProperty(ref _selectedDevicesApp, value);
                UpdateDevicesFromApp();
            }
        }

        /*
        public INodeViewAppNode SelectedActionsApp
        {
            get => _selectedActionsApp;
            set
            {
                if (_selectedActionsApp == value) return;
                SetProperty(ref _selectedActionsApp, value);
                UpdateActionsFromApp();
            }
        }
        public INodeViewAppNode SelectedEventActionsApp
        {
            get => _selectedEventActionsApp;
            set
            {
                if (_selectedEventActionsApp == value) return;
                SetProperty(ref _selectedEventActionsApp, value);
                UpdateEventActionsFromApp();
            }
        }
        */
        public DeviceViewModel SelectedDevice
        {
            get => _selectedDevice;
            set
            {
                if (_selectedDevice == value) return;

                if (_selectedDevice != null) _selectedDevice.IsSelected = false;
                if (value != null) value.IsSelected = true;
                SetProperty(ref _selectedDevice, value);
            }
        }
        public NodeViewActionViewModel SelectedAction
        {
            get => _selectedAction;
            set
            {
                if (_selectedAction == value) return;

                if (_selectedAction != null) _selectedAction.IsSelected = false;
                if (value != null) value.IsSelected = true;
                SetProperty(ref _selectedAction, value);
                UpdateSelectedActionVisibilities();
            }
        }
        public string SelectedLog
        {
            get => _selectedLog;
            set => SetProperty(ref _selectedLog, value);
        }
        /*
        public EventActionViewModel SelectedEventAction
        {
            get => _selectedEventAction;
            set
            {
                if (_selectedEventAction == value) return;

                if (_selectedEventAction != null) _selectedEventAction.IsSelected = false;
                if (value != null) value.IsSelected = true;
                SetProperty(ref _selectedEventAction, value);
            }
        }
        */
        public NodeViewCenterWidgetModel(IDialogService dialogService, IWidgetSource widgetSource)
        {
            _dialogService = dialogService;

            foreach (var config in widgetSource.Config.ChildNodes)
            {
                string appType = config.GetValue("@appType", "");
                if (string.Compare(appType, "center", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    var appNode = CreateAppNode((IConfiguration)config);
                    _centerAppNode = appNode;
                    SetCenterApiClient(appNode);
                    break;
                }
            }

            //ThreadAction.PostOnUiThread(UpdateFromServer, 200);
            Task.Factory.StartNew(UpdateFromServer);
            //UpdateFromServer();
        }

        private NodeViewAppNode CreateAppNode(IConfiguration appNodeConfig)
        {
            if (appNodeConfig == null) return null;

            string id = appNodeConfig.Id;
            string appKey = appNodeConfig.GetValue("@appKey", "");
            string appType = appNodeConfig.GetValue("@appType", "");
            string name = appNodeConfig.GetValue("@name", id);
            string ip = appNodeConfig.GetValue("@ip", "");
            int port = appNodeConfig.GetValue("@port", 0);
            string uri = appNodeConfig.GetValue("@uri", "");
            int processManagerPort = appNodeConfig.GetValue("@processManagerPort", 0);

            NodeViewAppNode appNode = new NodeViewAppNode(id, appKey, appType, name, ip, port, uri);
            appNode.ProcessManagerPort = processManagerPort;
            return appNode;
        }


        void ExecuteSelectAppLicenseCommand(string appType)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(appType)) return;

                if (SelectedAppLicense?.Name == appType)
                {
                    return;
                }
                foreach (var appLicenseItem in AppLicenses.ToArray())
                {
                    if (appLicenseItem.Name == appType)
                    {
                        SelectedAppLicense = appLicenseItem;
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSelectAppLicenseCommand:");
            }

        }
        void ExecuteRemoveAppCommand(string appId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(appId)) return;

                (AppLicenseItem license, NodeViewAppItem app) = FindLicenseAndAppItem(appId);
                if (license != null && app != null)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("title", "삭제 확인");
                    parameters.Add("message", $"{app.Name}({app.Id})을 삭제 하시겠습니까?");
                    _dialogService.ShowDialog("warningConfirm", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var request = new JObject()
                            {
                                ["action"] = "removeApp",
                                ["param"] = new JObject()
                                {
                                    ["id"] = app.Id
                                }
                            };
                            var response = _centerApiClient.Put("/licenses", request);
                            if (response.IsSuccess)
                            {
                                license.RemoveApp(app.Id);
                            }
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteRemoveAppCommand:");
            }
        }
        void ExecuteBlockAppCommand(string appId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(appId)) return;

                (AppLicenseItem license, NodeViewAppItem app) = FindLicenseAndAppItem(appId);
                if (license != null && app != null)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("title", "차단 확인");
                    parameters.Add("message", $"{app.Name}({app.Id})을 차단 하시겠습니까?");
                    _dialogService.ShowDialog("confirm", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var request = new JObject()
                            {
                                ["action"] = "blockApp",
                                ["param"] = new JObject()
                                {
                                    ["id"] = app.Id
                                }
                            };
                            var response = _centerApiClient.Put("/licenses", request);
                            if (response.IsSuccess)
                            {
                                license.BlockApp(app.Id);
                            }
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteBlockAppCommand:");
            }
        }

        void ExecuteEditAppCommand(string appId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(appId)) return;

                (AppLicenseItem license, NodeViewAppItem app) = FindLicenseAndAppItem(appId);
                if (license != null && app != null)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("app", app);
                    parameters.Add("apiClient", _centerApiClient);
                    _dialogService.ShowDialog("NodeViewAppEditDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            app.Id = r.Parameters.GetValue<string>("id");
                            app.Name = r.Parameters.GetValue<string>("name");
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteEditAppCommand:");
            }

        }

        void ExecuteSelectDevicesAppCommand()
        {
            try
            {
                var apps = GetApps();
                
                if (apps == null || apps.Length == 0) return;

                apps = apps.Where(app => !app.AppType.Equals("manager", StringComparison.OrdinalIgnoreCase)).ToArray();
                var parameters = new DialogParameters();
                parameters.Add("apps", apps);
                _dialogService.ShowDialog("AppSelectionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        SelectedDevicesApp = r.Parameters.GetValue<INodeViewAppNode>("selectedApp");
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSelectDevicesAppCommand:");
            }
        }

        private void ExecuteSelectDeviceCommand(string deviceId)
        {
            try
            {
                if (SelectedDevice.Id != deviceId)
                {
                    var device = FindDevice(deviceId);
                    if (device != null)
                    {
                        SelectedDevice = device;
                    }

                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSelectDeviceForViewCommandsCommand:");
            }
        }
        
        private DeviceViewModel FindDevice(string deviceId)
        {
            if (string.IsNullOrWhiteSpace(deviceId)) return null;

            foreach (var device in Devices.ToArray())
            {
                if (device.Id == deviceId)
                {
                    return device;
                }
            }

            return null;
        }

        private void ExecuteRunSelectedDeviceCommandCommand(string command)
        {
            try
            {
                string uri = SelectedDevicesApp.Uri;

                var device = SelectedDevice;
                if(device == null) return;
                var commandDescription =  device.FindCommandDescription(command);
                if (commandDescription == null) return;

                var parameters = new DialogParameters();
                parameters.Add("appUri", uri);
                parameters.Add("device", device);
                parameters.Add("commandDescription", commandDescription);
                _dialogService.ShowDialog("DeviceCommandRunDialog", parameters, r =>
                {
                    //skip
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteRunSelectDeviceCommandCommand:");
            }
        }
        private void ExecuteReloadSelectedDeviceNodesCommand()
        {
            try
            {
                
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteReloadSelectedDeviceNodesCommand:");
            }
        }
        /*
        void ExecuteSelectActionsAppCommand()
        {
            try
            {
                var apps = GetApps();
                if (apps == null || apps.Length == 0) return;

                var parameters = new DialogParameters();
                parameters.Add("apps", apps);
                _dialogService.ShowDialog("AppSelectionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        SelectedActionsApp = r.Parameters.GetValue<INodeViewAppNode>("selectedApp");
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSelectActionsAppCommand:");
            }
        }
        */
        private void ExecuteSelectActionCommand(string actionId)
        {
            var action = FindAction(actionId);
            SelectedAction = action;
        }

        private NodeViewActionViewModel FindAction(string id)
        {
            foreach (var action in Actions.ToArray())
            {
                if (action.Id == id)
                {
                    return action;
                }
            }

            return null;
        }

        private void ExecuteCreateNewActionCommand()
        {
            try
            {
                var parameters = new DialogParameters();
                parameters.Add("apiClient", _centerApiClient);
                parameters.Add("isCreateNew", true);
                parameters.Add("apps", GetApps());
                _dialogService.ShowDialog("EditCenterActionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var retAction = r.Parameters.GetValue<NodeViewActionViewModel>("action");
                        Actions.Add(retAction);
                        SelectedAction = retAction;
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteCreateNewActionCommand:");
            }
        }
        private void ExecuteDuplicateSelectedActionCommand()
        {
            try
            {
                var selectedAction = SelectedAction;
                if (selectedAction == null) return;

                var parameters = new DialogParameters();
                parameters.Add("apiClient", _centerApiClient);
                parameters.Add("isCreateNew", true);
                parameters.Add("apps", GetApps());
                parameters.Add("action", selectedAction);
                _dialogService.ShowDialog("EditCenterActionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var retAction = r.Parameters.GetValue<NodeViewActionViewModel>("action");
                        Actions.Add(retAction);
                        SelectedAction = retAction;
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDuplicateSelectedActionCommand:");
            }
        }
        private void ExecuteEditSelectedActionCommand()
        {
            try
            {
                var selectedAction = SelectedAction;
                if (selectedAction == null) return;

                var parameters = new DialogParameters();
                parameters.Add("apiClient", _centerApiClient);
                parameters.Add("isCreateNew", false);
                parameters.Add("apps", GetApps());
                parameters.Add("action", selectedAction);
                _dialogService.ShowDialog("EditCenterActionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var retAction = r.Parameters.GetValue<NodeViewActionViewModel>("action");
                        selectedAction.Update(retAction);
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteEditSelectedActionCommand:");
            }
        }
        private void ExecuteDeleteSelectedActionCommand()
        {
            try
            {
                var selectedAction = SelectedAction;
                if (selectedAction == null) return;
                var parameters = new DialogParameters();
                parameters.Add("title", "삭제 확인");
                parameters.Add("message", $"{selectedAction.Id}을 삭제 하시겠습니까?");
                _dialogService.ShowDialog("warningConfirm", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var request = new JObject()
                        {
                            ["action"] = "delete",
                            ["param"] = new JObject()
                            {
                                ["ids"] = new JArray()
                                {
                                    selectedAction.Id
                                }
                            }
                        };
                        var response = _centerApiClient.Put("/actions", request);
                        if (response.IsSuccess)
                        {
                            Actions.Remove(selectedAction);
                            SelectedAction = Actions.Count > 0 ? Actions[0] : null;
                            UpdateActionsFromServer();
                        }
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDeleteSelectedActionCommand:");
            }
        }
        /*
        private void ExecuteSelectEventActionCommand(string id)
        {
            var eventAction = FindEventAction(id);
            SelectedEventAction = eventAction;
        }
        */
        private EventActionViewModel FindEventAction(string id)
        {
            foreach (var eventAction in EventActions.ToArray())
            {
                if (eventAction.Id == id)
                {
                    return eventAction;
                }
            }
            return null;
        }

        private void ExecuteCreateNewEventActionCommand()
        {
            try
            {
                var parameters = new DialogParameters();
                parameters.Add("apiClient", _centerApiClient);
                parameters.Add("isCreateNew", true);
                parameters.Add("actions", Actions.ToArray());
                _dialogService.ShowDialog("EditEventActionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var retEventAction = r.Parameters.GetValue<EventActionViewModel>("eventAction");
                        EventActions.Add(retEventAction);
                        //SelectedEventAction = retEventAction;
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteCreateNewEventActionCommand:");
            }
        }
        private void ExecuteDuplicateSelectedEventActionCommand(string id)
        {
            try
            {
                var eventAction = FindEventAction(id);
                if (eventAction == null) return;

                var parameters = new DialogParameters();
                parameters.Add("apiClient", _centerApiClient);
                parameters.Add("isCreateNew", true);
                parameters.Add("actions", Actions.ToArray());
                parameters.Add("eventAction", eventAction);
                _dialogService.ShowDialog("EditEventActionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var retEventAction = r.Parameters.GetValue<EventActionViewModel>("eventAction");
                        EventActions.Add(retEventAction);
                        //SelectedEventAction = retEventAction;
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDuplicateSelectedEventActionCommand:");
            }
        }
        private void ExecuteEditSelectedEventActionCommand(string id)
        {
            try
            {
                var eventAction = FindEventAction(id);
                if (eventAction == null) return;

                var parameters = new DialogParameters();
                parameters.Add("apiClient", _centerApiClient);
                parameters.Add("isCreateNew", false);
                parameters.Add("actions", Actions.ToArray());
                parameters.Add("eventAction", eventAction);
                _dialogService.ShowDialog("EditEventActionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var retEventAction = r.Parameters.GetValue<EventActionViewModel>("eventAction");
                        eventAction.Update(retEventAction);
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteEditSelectedEventActionCommand:");
            }
        }
        private void ExecuteDeleteSelectedEventActionCommand(string id)
        {
            try
            {
                var eventAction = FindEventAction(id);
                if (eventAction == null) return;

                var parameters = new DialogParameters();
                parameters.Add("title", "삭제 확인");
                parameters.Add("message", $"{eventAction.Id}을 삭제 하시겠습니까?");
                _dialogService.ShowDialog("warningConfirm", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        var request = new JObject()
                        {
                            ["action"] = "delete",
                            ["param"] = new JObject()
                            {
                                ["ids"] = new JArray()
                                {
                                    eventAction.Id
                                }
                            }
                        };
                        var response = _centerApiClient.Put("/events", request);
                        if (response.IsSuccess)
                        {
                            EventActions.Remove(eventAction);
                            //SelectedEventAction = EventActions.Count > 0 ? EventActions[0] : null;
                            UpdateEventActionsFromServer();
                        }
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDeleteSelectedEventActionCommand:");
            }
        }

        private void UpdateSelectedActionVisibilities()
        {
            if (SelectedAction == null)
            {
                DuplicateSelectedActionVisibility = Visibility.Collapsed;
                EditSelectedActionVisibility = Visibility.Collapsed;
                DeleteSelectedActionVisibility = Visibility.Collapsed;
            }
            else
            {
                DuplicateSelectedActionVisibility = Visibility.Visible;
                EditSelectedActionVisibility = Visibility.Visible;
                DeleteSelectedActionVisibility = Visibility.Visible;
            }
        }

        private void UpdateFromServer()
        {
            try
            {
                ThreadAction.PostOnUiThread(()=>
                {
                    UpdateActionsFromServer();
                    UpdateEventActionsFromServer();
                    UpdateAppLicensesFromServer();
                    UpdateLogFromProcessManager();
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateFromServer:");
            }
        }
        protected void UpdateLogFromProcessManager()
        {
            if (_processManagerClient != null)
            {
                string[] logFiles = _processManagerClient.GetLogFileNames(_centerAppNode.AppKey);
                if (logFiles != null)
                {
                    ThreadAction.PostOnUiThread(() =>
                    {
                        LogFiles.Clear();
                        foreach (var logFile in logFiles)
                        {
                            LogFiles.Add(logFile);
                        }

                    });
                }
            }
        }
        private INodeViewAppNode[] GetApps()
        {
            List<INodeViewAppNode> apps = new List<INodeViewAppNode>();
            if (_centerAppNode != null)
            {
                apps.Add(_centerAppNode);
            }
            foreach (var appLicenseItem in AppLicenses.ToArray())
            {
                apps.AddRange(appLicenseItem.RegisteredApps);
            }

            return apps.ToArray();
        }

        private void UpdateAppLicensesFromServer()
        {
            var response = _centerApiClient.Get("/licenses");
            if (response.IsSuccess)
            {
                ThreadAction.PostOnUiThread(() =>
                {
                    AppLicenses.Clear();
                    foreach (var appLicenseToken in response.ResponseJsonArray)
                    {
                        var appLicense = AppLicenseItem.CreateFrom(appLicenseToken.Value<JObject>());
                        if (appLicense != null)
                        {
                            AppLicenses.Add(appLicense);
                        }
                    }

                    SelectedAppLicense = AppLicenses.Count > 0 ? AppLicenses[0] : null;

                    Task.Factory.StartNew(UpdateAppStatusViaProcessManager);
                });
            }
        }

        private void UpdateAppStatusViaProcessManager()
        {
            foreach (var appLicense in AppLicenses)
            {
                foreach (NodeViewAppItem appItem in appLicense.RegisteredApps)
                {
                    var _processManagerClient =
                        new NodeViewProcessManagerClient(appItem.Ip, appItem.ProcessManagerPort);
                    var app = _processManagerClient.GetApp(appItem.AppKey);
                    if (app != null)
                    {
                        appItem.ProcessRunningStatus = app.Status;
                    }
                }
            }
        }

        private void UpdateDevicesFromApp()
        {
            Devices.Clear();
            if (!string.IsNullOrWhiteSpace(_selectedDevicesApp?.Uri))
            {
                var apiClient = new RestApiClient(_selectedDevicesApp.Uri);
                var response = apiClient.Get("/devices");
                if (response.IsSuccess)
                {
                    foreach (var deviceToken in response.ResponseJsonArray)
                    {
                        var device = DeviceViewModel.CreateFrom(deviceToken.Value<JObject>());
                        if (device != null)
                        {
                            Devices.Add(device);
                        }
                    }
                }
            }
            SelectedDevice = Devices.Count > 0 ? Devices[0] : null;
        }

        private void UpdateActionsFromServer()
        {
            Actions.Clear();
            if (_centerApiClient != null)
            {
                var response = _centerApiClient.Get("/actions");
                if (response.IsSuccess)
                {
                    foreach (var actionToken in response.ResponseJsonArray)
                    {
                        var action = NodeViewActionViewModel.CreateFrom(actionToken.Value<JObject>());
                        if (action != null)
                        {
                            Actions.Add(action);
                        }
                    }
                }
            }
            SelectedAction = Actions.Count > 0 ? Actions[0] : null;
        }

        private void UpdateEventActionsFromServer()
        {
            List<EventActionViewModel> eventActionList = new List<EventActionViewModel>();
            if (_centerApiClient != null)
            {
                var response = _centerApiClient.Get("/events");
                if (response.IsSuccess)
                {
                    foreach (var eventActionToken in response.ResponseJsonArray)
                    {
                        var eventAction = EventActionViewModel.CreateFrom(eventActionToken.Value<JObject>());
                        if (eventAction != null)
                        {
                            eventActionList.Add(eventAction);
                        }
                    }

                }
            }

            ThreadAction.PostOnUiThread(() =>
            {
                EventActions.Clear();
                foreach (var eventAction in eventActionList)
                {
                    EventActions.Add(eventAction);
                }
            });
            //SelectedEventAction = EventActions.Count > 0 ? EventActions[0] : null;
        }
        /*
        private void UpdateActionsFromApp()
        {
            NodeViewActions.Clear();
            if (!string.IsNullOrWhiteSpace(_selectedActionsApp?.Uri))
            {
                var apiClient = new RestApiClient(_selectedActionsApp.Uri);
                var response = apiClient.Get("/actions");
                if (response.IsSuccess)
                {
                    foreach (var actionToken in response.ResponseJsonArray)
                    {
                        var action = NodeViewActionViewModel.CreateFrom(actionToken.Value<JObject>());
                        if (action != null)
                        {
                            NodeViewActions.Add(action);
                        }
                    }
                }
            }
            SelectedAction = NodeViewActions.Count > 0 ? NodeViewActions[0] : null;
        }

        private void UpdateEventActionsFromApp()
        {
            EventActions.Clear();
            if (!string.IsNullOrWhiteSpace(_selectedEventActionsApp?.Uri))
            {
                var apiClient = new RestApiClient(_selectedEventActionsApp.Uri);
                var response = apiClient.Get("/event");
                if (response.IsSuccess)
                {
                    foreach (var eventActionToken in response.ResponseJsonArray)
                    {
                        var eventAction = EventActionViewModel.CreateFrom(eventActionToken.Value<JObject>());
                        if (eventAction != null)
                        {
                            EventActions.Add(eventAction);
                        }
                    }

                }
            }
            SelectedEventAction = EventActions.Count > 0 ? EventActions[0] : null;
        }
        */

        private bool SetCenterApiClient(INodeViewAppNode app)
        {
            if (string.IsNullOrWhiteSpace(app?.Ip) || app.Port <= 0)
            {
                return false;
            }
            try
            {
                if (string.IsNullOrWhiteSpace(app.Uri))
                {
                    _centerApiClient = new RestApiClient($"http://{app.Ip}:{app.Port}/api/v1");
                }
                _centerApiClient = new RestApiClient(app.Uri);
                if (!string.IsNullOrWhiteSpace(app.Ip))
                {
                    _processManagerClient = new NodeViewProcessManagerClient(app.Ip, app.ProcessManagerPort);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CheckDataWallUpdate:");
                _centerApiClient = null;
            }

            //ThreadAction.PostOnUiThread(CheckDataWallUpdate);
            return _centerApiClient != null;
        }
        private (AppLicenseItem license, NodeViewAppItem app) FindLicenseAndAppItem(string appId)
        {
            foreach (var appLicense in AppLicenses.ToArray())
            {
                var app = appLicense.FindApp(appId);
                if (app != null)
                {
                    return (appLicense, app);
                }
            }
            return (null, null);
        }
        void ExecuteStopControlCommand(NodeViewAppItem nodeViewAppItem)
        {
            if (string.IsNullOrWhiteSpace(nodeViewAppItem.AppKey)) return;
            var _processManagerClient =
                new NodeViewProcessManagerClient(nodeViewAppItem.Ip, nodeViewAppItem.ProcessManagerPort);
            _processManagerClient.StopApp(nodeViewAppItem.AppKey);
            UpdateAppLicensesFromServer();
        }
        void ExecuteStartControlCommand(NodeViewAppItem nodeViewAppItem)
        {
            if (string.IsNullOrWhiteSpace(nodeViewAppItem.AppKey)) return;
            var _processManagerClient =
                new NodeViewProcessManagerClient(nodeViewAppItem.Ip, nodeViewAppItem.ProcessManagerPort);
            _processManagerClient.StartApp(nodeViewAppItem.AppKey);
            UpdateAppLicensesFromServer();
        }
        void ExecuteReStartControlCommand(NodeViewAppItem nodeViewAppItem)
        {
            if (string.IsNullOrWhiteSpace(nodeViewAppItem.AppKey)) return;
            var _processManagerClient =
                new NodeViewProcessManagerClient(nodeViewAppItem.Ip, nodeViewAppItem.ProcessManagerPort);
            _processManagerClient.RestartApp(nodeViewAppItem.AppKey);
            UpdateAppLicensesFromServer();
        }
        private void ExecuteReloadCommand() => UpdateFromServer();
        public DelegateCommand<string> SelectedLogFileCommand
        {
            get
            {
                return new DelegateCommand<string>(args =>
                {
                    SelectedLog = _processManagerClient.GetLog(_centerAppNode.AppKey, args);
                });
            }
        }
    }
}