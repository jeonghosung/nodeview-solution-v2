﻿using System.Windows.Controls;
using NodeView.Frameworks.Widgets;

namespace NodeView.Widgets
{
    /// <summary>
    /// Interaction logic for EventLogWidget
    /// </summary>
    public partial class EventLogWidget : UserControl, IWidgetControl
    {
        public EventLogWidget()
        {
            InitializeComponent();
        }

        public IWidgetSource Source { get; set; }
    }
}
