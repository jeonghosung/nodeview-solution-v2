﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using NodeView.Protocols.NodeViews;
using NodeView.Tasks;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using NodeViewWindowCore.ViewModels;

namespace NodeView.Widgets
{
    public class DataWallAdminWidgetModel : StationAndProcessAdminBaseViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDialogService _dialogService = null;
        private ContentTreeViewFolder _wallContentFolder = new ContentTreeViewFolder("");
        private DataWallPresetBindable _currentPreset = null;
        private IntSize _screenSize = new IntSize(1920, 1080);
        private ContentEditViewModel _content = null;
        private string _errorMessage;
        private string _lastPresetsUpdated = "-";
        private string _lastContentsUpdated = "-";
        private DatawallAdminContentSettingViewModel _datawallAdminContentSettingViewModel;
        private DelegateCommand _changePasswordCommand;

        public DataWallPresetBindable CurrentPreset
        {
            get => _currentPreset;
            set => SetProperty(ref _currentPreset, value);
        }
        public DatawallAdminContentSettingViewModel DatawallAdminContentSettingViewModel
        {
            get => _datawallAdminContentSettingViewModel;
            set => SetProperty(ref _datawallAdminContentSettingViewModel, value);
        }
        public ObservableCollection<DataWallPresetBindable> WallPresets { get; } = new ObservableCollection<DataWallPresetBindable>();
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public IntSize ScreenSize
        {
            get => _screenSize;
            set => SetProperty(ref _screenSize, value);
        }

        public DelegateCommand SavePresetCommand { get; set; }
        public DelegateCommand DeletePresetCommand { get; set; }
        public DelegateCommand<DataWallPresetBindable> SelectedPresetTreeItemCommand
        {
            get
            {
                return new DelegateCommand<DataWallPresetBindable>(args =>
                {
                    CurrentPreset = args;
                });
            }
        }
        public DelegateCommand ChangePasswordCommand => _changePasswordCommand ?? (_changePasswordCommand = new DelegateCommand(ExecuteChangePasswordCommand));

        public DataWallAdminWidgetModel(IDialogService dialogService, IWidgetSource widgetSource) : base(dialogService)
        {
            _dialogService = dialogService;
         
            InitCommand();
            foreach (var config in widgetSource.Config.ChildNodes)
            {
                string appType = config.GetValue("@appType", "");
                if (string.Compare(appType, "dataWall", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    InitStation((IConfiguration)config);
                    DatawallAdminContentSettingViewModel =
                        new DatawallAdminContentSettingViewModel(_nodeViewApiClient, dialogService);
                    ThreadAction.PostOnUiThread(Reload);
                    break;
                }
            }
        }

        private void InitCommand()
        {
            DeletePresetCommand = new DelegateCommand(ExecuteDeletePresetCommand);
            SavePresetCommand = new DelegateCommand(ExecuteSavePresetCommand);
        }
        public override void Reload()
        {
            Task.Delay(200).ContinueWith(t=>ReloadInternal());
        }
        private void ReloadInternal()
        {
            base.Reload();
            CheckDataWallUpdate();
        }
        private void CheckDataWallUpdate()
        {
            if (_nodeViewApiClient == null) return;

            try
            {
                var response = _nodeViewApiClient.Get("/infos");
                if (response.IsSuccess && response.IsJsonResponse)
                {
                    var responseJson = response.ResponseJsonObject;

                    bool needToUpdateContents = false;
                    string lastContentsUpdated = JsonUtils.GetStringValue(responseJson, "contents.updated");
                    if (_lastContentsUpdated != lastContentsUpdated)
                    {
                        _lastContentsUpdated = lastContentsUpdated;
                        needToUpdateContents = true;
                    }

                    bool needToUpdatePresets = false;
                    string lastPresetsUpdated = JsonUtils.GetStringValue(responseJson, "presets.updated");
                    if (_lastPresetsUpdated != lastPresetsUpdated)
                    {
                        _lastPresetsUpdated = lastPresetsUpdated;
                        needToUpdatePresets = true;
                    }

                    IntSize screenSize = JsonUtils.GetValue<IntSize>(responseJson, "screen.size", null);
                    if (screenSize == null || screenSize.IsEmpty)
                    {
                        Logger.Error("content screenSize is empty.");
                    }
                    else
                    {
                        if (ScreenSize != screenSize)
                        {
                            ScreenSize = screenSize;
                        }
                    }
                    if (needToUpdateContents)
                    {
                        Task.Factory.StartNew(DatawallAdminContentSettingViewModel.Reload);
                    }
                    if (needToUpdatePresets)
                    {
                        Task.Factory.StartNew(UpdatePresetsFromDataWall);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CheckUpdate:");
            }
        }
        private void UpdatePresetsFromDataWall()
        {
            try
            {
                var response = _nodeViewApiClient.Get("/presets");
                if (response.IsSuccess)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        List<DataWallPresetBindable> presetList = new List<DataWallPresetBindable>();
                        var responseArray = response.ResponseJsonArray;
                        Logger.Info($"presets responseArray : {responseArray.Count}");

                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                var nodeViewPreset = DataWallPresetBindable.CreateFrom(json);
                                if (nodeViewPreset != null)
                                {
                                    presetList.Add(nodeViewPreset);
                                }
                            }
                        }
                        DataWallPresetBindable[] presets = presetList.ToArray();
                        ThreadAction.PostOnUiThread(() =>
                        {
                            WallPresets.Clear();
                            WallPresets.AddRange(presets);
                        });
                    }
                    else
                    {
                        Logger.Error("reponse of presets is not an array.");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdatePresetsFromDataWall:");
            }
        }
        private void ExecuteSavePresetCommand()
        {
            if (CheckTocken())
            {
                JObject contentJson = new JObject
                {
                    ["action"] = "rename",
                    ["param"] = new JObject()
                    {
                        ["id"] = CurrentPreset.Id,
                        ["name"] = CurrentPreset.Name
                    }
                };
                var response = _nodeViewApiClient.Put("/presets", contentJson);
                if (response.IsSuccess)
                {
                    //todo : 성공/실패시 처리
                }
            }
        }
        private void ExecuteDeletePresetCommand()
        {
            if (CheckTocken())
            {
                var parameters = new DialogParameters();
                parameters.Add("title", "삭제 확인");
                parameters.Add("message", $"{CurrentPreset.Id}를 삭제 하시겠습니까?");
                _dialogService.ShowDialog("warningConfirm", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        JObject contentJson = new JObject
                        {
                            ["action"] = "delete",
                            ["param"] = new JObject()
                            {
                                ["ids"] = new JArray() { CurrentPreset.Id }
                            }
                        };
                        var response = _nodeViewApiClient.Put("/presets", contentJson);
                        if (response.IsSuccess)
                        {
                            //todo : 성공/실패시 처리
                        }
                        WallPresets.Remove(CurrentPreset);
                        CurrentPreset = WallPresets.Count > 0 ? WallPresets[0] : null;
                    }
                });
            }
        }
        private void ExecuteChangePasswordCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "비밀번호 변경");
            parameters.Add("nodeViewApiClient", _nodeViewApiClient);
            _dialogService.ShowDialog("ChangePasswordDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                }
            });
        }
        private bool CheckTocken()
        {
            bool result = false;
            if (!_nodeViewApiClient.HasAccessToken)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Add("apiClient", _nodeViewApiClient);
                _dialogService.ShowDialog("LoginDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        result = true;
                    }
                });
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}