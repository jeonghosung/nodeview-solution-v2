﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class KeyValueCollectionBindable : BindableCollectionBase, IKeyValueCollection
    {
        private readonly List<IKeyValuePair> _list = new List<IKeyValuePair>();
        public int Count => _list.Count;
        public string[] Keys => _list.Select(x => x.Key).ToArray();

        public KeyValueCollectionBindable()
        {
        }
        public KeyValueCollectionBindable(IEnumerable<IKeyValuePair> keyValuePairs) : this()
        {
            if (keyValuePairs == null)
            {
                return;
            }

            foreach (var keyValuePair in keyValuePairs)
            {
                if (!string.IsNullOrWhiteSpace(keyValuePair.Key))
                {
                    _list.Add(new KeyValuePairBindable(keyValuePair.Key, keyValuePair.Value));
                }
            }
        }
        public IEnumerator<IKeyValuePair> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object this[string key]
        {
            get
            {
                if (TryGetValue(key, out var value))
                {
                    return value;
                }

                return "";
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(key))
                {
                    Set(key, value);
                }
            }
        }

        public bool TryGetValue(string key, out object value)
        {
            value = null;
            if (string.IsNullOrWhiteSpace(key)) return false;

            foreach (var item in _list)
            {
                if (item.Key == key)
                {
                    value = item.Value;
                    return true;
                }
            }

            return false;
        }
        public int FindIndex(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) return -1;

            var list = _list.ToArray();
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Key == key)
                {
                    return i;
                }
            }

            return -1;
        }
        public bool Contains(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) return false;
            return FindIndex(key) >= 0;
        }

        public T GetValue<T>(string key, T defaultValue)
        {
            if (TryGetValue(key, out var value))
            {
                return StringUtils.GetValue(value, defaultValue);
            }

            return defaultValue;
        }

        public string GetStringValue(string key, string defaultValue = "")
        {
            return GetValue(key, defaultValue);
        }

        #region set/remove/clear
        public bool Set(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;
            var keyValuePair = new KeyValuePair(key, value);
            return Set(keyValuePair);
        }

        public bool Set(IKeyValuePair keyValuePair)
        {
            if (string.IsNullOrWhiteSpace(keyValuePair?.Key) || keyValuePair.Value == null) return false;

            int index = FindIndex(keyValuePair.Key);
            if (index < 0)
            {
                _list.Add(keyValuePair);
                OnCollectionAdded(keyValuePair);
                RaisePropertyChanged("Keys");
                RaisePropertyChanged("Count");
            }
            else
            {
                var oldValue = _list[index];
                _list.RemoveAt(index);
                _list.Insert(index, keyValuePair);
                OnCollectionReplaced(keyValuePair, oldValue, index);
            }
            return true;
        }
        public void Sets(IEnumerable<IKeyValuePair> keyValuePairs)
        {
            if (keyValuePairs == null) return;

            foreach (var keyValuePair in keyValuePairs)
            {
                Set(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public bool Remove(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return false;
            }

            int index = FindIndex(key);
            if (index >= 0)
            {
                var oldValue = _list[index];
                _list.RemoveAt(index);
                OnCollectionRemoved(oldValue, index);
                RaisePropertyChanged("Keys");
                RaisePropertyChanged("Count");
                return true;
            }
            return false;
        }

        public void Clear()
        {
            _list.Clear();
            OnCollectionReset();
            RaisePropertyChanged("Keys");
            RaisePropertyChanged("Count");
        }
        #endregion

        public override string ToString()
        {
            List<string> keyValueStringList = new List<string>();
            foreach (var keyValuePair in _list.ToArray())
            {
                keyValueStringList.Add($"{keyValuePair.Key}={keyValuePair.Value}");
            }

            return string.Join("&", keyValueStringList);
        }

        public string ToQueryString()
        {
            List<string> keyValueStringList = new List<string>();

            foreach (var keyValuePair in _list)
            {
                keyValueStringList.Add($"{HttpUtility.UrlEncode(keyValuePair.Key)}={HttpUtility.UrlEncode(keyValuePair.GetValue(""))}");
            }

            return string.Join("&", keyValueStringList);
        }

        public static KeyValueCollectionBindable CreateFromQueryString(string queryString)
        {
            var collection = new KeyValueCollectionBindable();
            if (string.IsNullOrWhiteSpace(queryString)) return collection;

            try
            {
                var nameValueCollection = HttpUtility.ParseQueryString(queryString);
                foreach (string key in nameValueCollection.Keys)
                {
                    try
                    {
                        collection.Set(key, nameValueCollection.Get(key).Trim());
                    }
                    catch
                    {
                        //skip
                    }
                }
            }
            catch
            {
                //skip
            }
            return collection;
        }
    }
}
