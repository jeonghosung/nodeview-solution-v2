﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class PropertyCollectionBindable : BindableCollectionBase, IPropertyCollection
    {
        private readonly List<IProperty> _list = new List<IProperty>();
        public int Count => _list.Count;
        public string[] Ids => _list.Select(x=>x.Id).ToArray();
        public IProperty this[string id]
        {
            get
            {
                if (string.IsNullOrWhiteSpace(id)) return null;
                if (TryGetProperty(id, out var property))
                {
                    return property;
                }

                return null;
            }
        }

        
        public IEnumerator<IProperty> GetEnumerator()
        {
            return _list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public bool TryGetProperty(string id, out IProperty property)
        {
            property = null;
            if (string.IsNullOrWhiteSpace(id)) return false;

            foreach (var item in _list)
            {
                if (item.Id == id)
                {
                    property = item;
                    return true;
                }
            }

            return false;
        }

        public int FindIndex(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return -1;

            var list = _list.ToArray();
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Id == id)
                {
                    return i;
                }
            }

            return -1;
        }

        public bool Contains(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;
            return FindIndex(id) >= 0;
        }

        public T GetValue<T>(string id, T defaultValue = default(T))
        {
            if (TryGetProperty(id, out var property))
            {
                return StringUtils.GetValue(property.Value, defaultValue);
            }

            return defaultValue;
        }

        public string GetStringValue(string id, string defaultValue = "")
        {
            return GetValue(id, defaultValue);
        }

        #region set/remove/clear
        public bool Set(IProperty property)
        {
            if (string.IsNullOrWhiteSpace(property?.Id)) return false;

            int index = FindIndex(property.Id);
            if (index < 0)
            {
                _list.Add(property);
                OnCollectionAdded(property);
                RaisePropertyChanged("Ids");
                RaisePropertyChanged("Count");
            }
            else
            {
                var oldProperty = _list[index];
                _list.RemoveAt(index);
                _list.Insert(index, property);
                OnCollectionReplaced(property, oldProperty, index);
            }
            return true;

        }

        public bool Sets(IEnumerable<IProperty> properties)
        {
            if (properties == null) return false;

            foreach (var property in properties)
            {
                Set(property);
            }
            return true;
        }

        public bool Remove(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return false;
            }

            int index = FindIndex(id);
            if (index >= 0)
            {
                var oldProperty = _list[index];
                _list.RemoveAt(index);
                OnCollectionRemoved(oldProperty, index);
                RaisePropertyChanged("Ids");
                RaisePropertyChanged("Count");
                return true;
            }
            return false;
        }

        public void Clear()
        {
            _list.Clear();
            OnCollectionReset();
            RaisePropertyChanged("Ids");
            RaisePropertyChanged("Count");
        }
        #endregion
    }
}
