﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public class NodeFolderBindableBase : BindableBase, INodeFolder
    {
        private string _id = "";
        private string _path = "";

        protected readonly Dictionary<string, INode> _childNodeDictionary = new Dictionary<string, INode>();
        protected readonly ObservableCollection<INode> _childNodes = new ObservableCollection<INode>();

        protected readonly Dictionary<string, INodeFolder> _childFolderDictionary = new Dictionary<string, INodeFolder>();
        protected readonly ObservableCollection<INodeFolder> _childFolders = new ObservableCollection<INodeFolder>();


        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Path
        {
            get => _path;
            set => SetProperty(ref _path, value);
        }

        public IEnumerable<INode> ChildNodes => _childNodes;
        public IEnumerable<INodeFolder> ChildFolders => _childFolders;

        protected NodeFolderBindableBase(string id)
        {
            Id = id;
            Path = "";
        }

        public bool TryGetNode(string path, out INode node)
        {
            node = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            (string pathPart, string id) = StringUtils.SplitPathToOthersAndLast(path);
            if (string.IsNullOrWhiteSpace(pathPart))
            {
                if (_childNodeDictionary.TryGetValue(id, out var foundNode))
                {
                    node = foundNode;
                    return true;
                }
            }
            else
            {
                if (TryGetFolder(pathPart, out var folder))
                {
                    if (folder.TryGetNode(id, out var foundNode))
                    {
                        node = foundNode;
                        return true;
                    }
                }
            }

            return false;
        }

        public bool SetChildNode(INode node)
        {
            if (string.IsNullOrWhiteSpace(node?.Id)) return false;
            node.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";

            if (_childNodeDictionary.TryGetValue(node.Id, out var oldNode))
            {
                _childNodes.Remove(oldNode);
            }
            _childNodes.Add(node);
            _childNodeDictionary[node.Id] = node;
            return true;
        }

        public bool RemoveChildNode(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (_childNodeDictionary.TryGetValue(id, out var oldNode))
            {
                _childNodes.Remove(oldNode);
                _childNodeDictionary.Remove(id);
                return true;
            }

            return false;
        }

        public void ClearChildNodes()
        {
            _childNodes.Clear();
            _childNodeDictionary.Clear();
        }

        public bool TryGetFolder(string path, out INodeFolder folder)
        {
            folder = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            (string id, string subPath) = StringUtils.SplitPathToFirstAndOthers(path);
            if (_childFolderDictionary.TryGetValue(id, out var foundFolder))
            {
                if (string.IsNullOrWhiteSpace(subPath))
                {
                    folder = foundFolder;
                    return true;
                }
                return foundFolder.TryGetFolder(subPath, out folder);
            }

            return false;
        }

        public bool SetChildFolder(INodeFolder folder)
        {
            if (string.IsNullOrWhiteSpace(folder?.Id)) return false;
            folder.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";

            if (_childFolderDictionary.TryGetValue(folder.Id, out var oldFolder))
            {
                _childFolders.Remove(oldFolder);
            }
            _childFolders.Add(folder);
            _childFolderDictionary[folder.Id] = folder;
            return true;
        }

        public bool RemoveChildFolder(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (_childFolderDictionary.TryGetValue(id, out var oldFolder))
            {
                _childFolders.Remove(oldFolder);
                _childFolderDictionary.Remove(id);
                return true;
            }

            return false;
        }

        public void ClearChildFolders()
        {
            foreach (var childFolder in _childFolders)
            {
                childFolder.Clear();
            }
            _childFolders.Clear();
            foreach (var childFolder in _childFolderDictionary.Values)
            {
                childFolder.Clear();
            }
            _childFolderDictionary.Clear();
        }

        public void Clear()
        {
            ClearChildNodes();
            ClearChildFolders();
        }
    }
}
