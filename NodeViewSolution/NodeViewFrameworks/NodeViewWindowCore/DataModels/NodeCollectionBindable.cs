﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public class NodeCollectionBindable : BindableBase, INodeCollection
    {
        private string _path = "";

        private ObservableCollection<INode> _nodes = new ObservableCollection<INode>();
        private ObservableCollection<IPathNode> _nodePaths = new ObservableCollection<IPathNode>();
        private readonly Dictionary<string, PathNode> _nodePathDictionary = new Dictionary<string, PathNode>();

        public string Path
        {
            get => _path;
            set => SetProperty(ref _path, value);
        }

        public IEnumerable<IPathNode> NodePaths => _nodePaths;

        public int Count => _nodes.Count;

        public NodeCollectionBindable(string path = "")
        {
            _path = path ?? "";
        }

        public IEnumerator<INode> GetEnumerator()
        {
            return _nodes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        public bool Set(INode node)
        {
            if (string.IsNullOrWhiteSpace(node?.Id)) return false;
            if (node.Path == null) node.Path = "";

            if (!string.IsNullOrWhiteSpace(Path) && node.Path.StartsWith(Path))
            {
                node.Path = node.Path.Substring(Path.Length).Trim('/');
            }

            if (_nodePathDictionary.TryGetValue(node.Path, out var foundPath))
            {
                if (foundPath.TryGetNode(node.Id, out var oldNode))
                {
                    _nodes.Remove(oldNode);
                    _nodes.Add(node);
                    foundPath.Remove(node.Id);
                    foundPath.Set(node);
                }
                else
                {
                    _nodes.Add(node);
                    foundPath.Set(node);
                }
            }
            else
            {
                var newPath = new PathNode(node.Path);
                _nodePathDictionary[newPath.Path] = newPath;
                newPath.Set(node);
                _nodes.Add(node);
                _nodePaths.Add(newPath);
            }

            return true;
        }

        public bool Set(string path, INode node)
        {
            if (!string.IsNullOrWhiteSpace(Path) && path.StartsWith(Path))
            {
                path = path.Substring(Path.Length).Trim('/');
            }

            node.Path = path;
            return Set(node);
        }

        public bool Sets(IEnumerable<INode> nodes)
        {
            if (nodes == null) return false;

            foreach (var node in nodes)
            {
                Set(node);
            }
            return true;
        }

        public bool Sets(string path, IEnumerable<INode> nodes)
        {
            if (nodes == null) return false;

            foreach (var node in nodes)
            {
                Set(path, node);
            }
            return true;
        }

        public bool Remove(string path, string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;
            path = path ?? "";

            if (_nodePathDictionary.TryGetValue(path, out var foundPath))
            {
                if (foundPath.TryGetNode(id, out var oldNode))
                {
                    _nodes.Remove(oldNode);
                    foundPath.Remove(id);
                    return true;
                }
            }

            return false;
        }

        public bool Remove(string pathId)
        {
            (string path, string id) = StringUtils.SplitPathToOthersAndLast(pathId);
            if (string.IsNullOrWhiteSpace(id))
            {
                return false;
            }

            return Remove(path, id);
        }

        public void Clear()
        {
            _nodePathDictionary.Clear();
            _nodePaths.Clear();
            _nodes.Clear();
        }
        public INode this[string pathId]
        {
            get
            {
                if (TryGetNode(pathId, out var node))
                {
                    return node;
                }

                return null;
            }
        }

        public bool Contains(string path, string id)
        {
            return Contains($"{path}/{id}");
        }

        public bool Contains(string pathId)
        {
            return TryGetNode(pathId, out var node);
        }

        public bool TryGetNodePath(string path, out IPathNode pathNode)
        {
            pathNode = null;

            if (string.IsNullOrWhiteSpace(path)) return false;
            path = path.Replace('.', '/').Trim().Trim('/');

            if (_nodePathDictionary.TryGetValue(path, out var foundPath))
            {
                pathNode = foundPath;
                return true;
            }

            return false;
        }

        public bool TryGetNode(string pathId, out INode node)
        {
            node = null;

            if (string.IsNullOrWhiteSpace(pathId)) return false;
            pathId = pathId.Replace('.', '/').Trim().Trim('/');

            (string path, string id) = StringUtils.SplitPathToOthersAndLast(pathId);
            if (_nodePathDictionary.TryGetValue(path, out var foundPath))
            {
                return foundPath.TryGetNode(id, out node);
            }

            return false;
        }

        public bool TryGetObject(string search, out object @object)
        {
            @object = null;
            if (string.IsNullOrWhiteSpace(search)) return false;
            search = search.Replace('.', '/').Trim().Trim('/');

            (string path, string key) = StringUtils.SplitPathToOthersAndLast(search);
            if (TryGetNode(path, out var node))
            {
                if (node.TryGetObject(key, out @object))
                {
                    return true;
                }
            }
            if (TryGetNode(search, out node))
            {
                @object = node;
                return true;
            }

            if (TryGetNodePath(search, out var nodePath))
            {
                @object = nodePath;
                return true;
            }
            return false;
        }

        public T GetValue<T>(string search, T defaultValue)
        {
            if (TryGetObject(search, out var @object))
            {
                return StringUtils.GetValue(@object, defaultValue);
            }

            return defaultValue;
        }

        public string GetStringValue(string search, string defaultValue = "")
        {
            if (TryGetObject(search, out var @object))
            {
                return $"{@object}";
            }

            return defaultValue;
        }

        public IEnumerable<INode> SelectNodes(string path, bool includeSubNodes = false)
        {
            path = path?.Trim().Trim('/') ?? "";
            string pathSub = $"{path}/";
            List<INode> selectedNodeList = new List<INode>();
            foreach (var pathToken in _nodePathDictionary)
            {
                if (path == "")
                {
                    selectedNodeList.AddRange(pathToken.Value.Nodes);
                }
                else
                {
                    if (pathToken.Key == path)
                    {
                        selectedNodeList.AddRange(pathToken.Value.Nodes);
                    }
                    else if (includeSubNodes && pathToken.Key.StartsWith(pathSub))
                    {
                        selectedNodeList.AddRange(pathToken.Value.Nodes);
                    }
                }
            }

            return selectedNodeList;
        }
        
        public INodeCollection Clone()
        {
            var clone = new NodeCollection(Path);
            foreach (var node in SelectNodes("", true))
            {
                Set(node.Clone());
            }

            return clone;
        }

        public JToken ToJson()
        {
            JArray nodeArray = new JArray();
            //var nodeList = SelectNodes("", true);
            //foreach (var node in nodeList)
            foreach (var node in this)
            {
                nodeArray.Add(node.ToJson());
            }
            return nodeArray;
        }

        public bool LoadFrom(JArray jsonArray)
        {
            if (jsonArray == null) return false;
            foreach (var nodeToken in jsonArray)
            {
                JObject nodeJson = nodeToken.Value<JObject>();
                var node = Node.CreateFrom(nodeJson);
                if (node != null && !string.IsNullOrWhiteSpace(node.Id))
                {
                    Set(node);
                }
            }

            return true;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Nodes" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("path", Path);
            foreach (var node in this)
            {
                node.WriteXml(xmlWriter);
            }
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNodeList xmlNodeList)
        {
            if (xmlNodeList == null) return true;

            foreach (XmlNode xmlNode in xmlNodeList)
            {
                var node = Node.CreateFrom(xmlNode);
                if (node != null)
                {
                    Set(node);
                }
            }

            return true;
        }

        public static NodeCollectionBindable CreateFrom(JArray jsonArray, string path)
        {
            var nodeCollection = new NodeCollectionBindable(path);
            nodeCollection.LoadFrom(jsonArray);
            return nodeCollection;
        }

        public static NodeCollectionBindable CreateFrom(XmlNodeList xmlNodeList, string path)
        {
            var nodeCollection = new NodeCollectionBindable(path);
            nodeCollection.LoadFrom(xmlNodeList);
            return nodeCollection;
        }
    }
}
