﻿using System;
using System.Collections.Specialized;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public class BindableCollectionBase : BindableBase, INotifyCollectionChanged
    {
        private BindableCollectionBase.SimpleMonitor _monitor = new BindableCollectionBase.SimpleMonitor();

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        protected virtual void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            if (CollectionChanged != null)
            {
                using (this.BlockReentrancy())
                {
                    CollectionChanged?.Invoke((object)this, args);
                }
            }
            return;
        }

        protected void OnCollectionChanged(NotifyCollectionChangedAction action, object item, int index) => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(action, item, index));

        protected void OnCollectionReset() => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        protected void OnCollectionAdded(object item) => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item));
        protected void OnCollectionRemoved(object item, int index) => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, index));
        protected void OnCollectionReplaced(object newItem, object oldItem, int index) => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Replace, newItem, oldItem, index));
        protected void OnCollectionMoved(object item, int newIndex, int oldIndex) => this.OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move, item, newIndex, oldIndex));

        protected IDisposable BlockReentrancy()
        {
            this._monitor.Enter();
            return (IDisposable)this._monitor;
        }

        protected void CheckReentrancy()
        {
            if (this._monitor.Busy && this.CollectionChanged != null && this.CollectionChanged.GetInvocationList().Length > 1)
                throw new InvalidOperationException("ObservableCollectionReentrancyNotAllowed");
        }

        private class SimpleMonitor : IDisposable
        {
            private int _busyCount;
            public void Enter() => ++this._busyCount;
            public void Dispose() => --this._busyCount;
            public bool Busy => this._busyCount > 0;
        }
    }
}
