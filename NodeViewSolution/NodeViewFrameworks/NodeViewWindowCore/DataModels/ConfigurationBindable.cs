﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class ConfigurationBindable : TreeNodeBindableBase, IConfiguration
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ConfigurationBindable(string id = "")
        {
            Id = id;
        }

        public override INode CreateNode()
        {
            return new ConfigurationBindable();
        }

        public static ConfigurationBindable CreateFrom(JObject json)
        {
            ConfigurationBindable configuration = new ConfigurationBindable();
            configuration.LoadFrom(json);
            return configuration;
        }

        public static ConfigurationBindable CreateFrom(XmlNode xmlNode)
        {
            ConfigurationBindable configuration = new ConfigurationBindable();
            configuration.LoadFrom(xmlNode);
            return configuration;
        }

        public static ConfigurationBindable Load(string filePath)
        {
            try
            {
                string configFileName = filePath;
                if (!File.Exists(configFileName))
                {
                    configFileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filePath);
                    if (!File.Exists(configFileName))
                    {
                        Logger.Error($"Cannot find '{filePath}' file.");
                        return null;
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                ConfigurationBindable configuration = CreateFrom(doc.DocumentElement);
                return configuration;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Load('{filePath}'):");
            }

            return null;
        }
        public static bool Save(string filePath, IConfiguration configuration)
        {
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(filePath))
                {
                    configuration.WriteXml(xmlWriter);
                    xmlWriter.Close();
                }
                
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Save '{filePath}':");
            }

            return false;
        }
    }
}
