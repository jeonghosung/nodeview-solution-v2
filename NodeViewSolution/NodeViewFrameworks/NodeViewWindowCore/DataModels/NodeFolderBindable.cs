﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class NodeFolderBindable : NodeFolderBindableBase
    {
        public NodeFolderBindable(string id) : base(id)
        {
        }
        protected void GetNodesInternal(List<INode> nodelist)
        {
            nodelist.AddRange(ChildNodes);
            foreach (var childFolder in ChildFolders)
            {
                if (childFolder is NodeFolderBindable folder)
                {
                    folder.GetNodesInternal(nodelist);
                }
            }
        }
        public INode[] ToArray()
        {
            List<INode> nodelist = new List<INode>();
            GetNodesInternal(nodelist);
            return nodelist.ToArray();
        }

        public static NodeFolderBindable CreateFrom(INodeCollection sourceNodeCollection)
        {
            if (sourceNodeCollection == null) return null;
            NodeFolderBindable rootFolder = new NodeFolderBindable("");
            foreach (var node in sourceNodeCollection)
            {
                string[] splitPath = StringUtils.Split(node.Path, '/');
                INodeFolder currentFolder = rootFolder;
                foreach (var pathName in splitPath)
                {
                    if (string.IsNullOrWhiteSpace(pathName))
                    {
                        continue;
                    }
                    if (currentFolder.TryGetFolder(pathName, out var foundFolder))
                    {
                        currentFolder = foundFolder;
                    }
                    else
                    {
                        var newFolder = new NodeFolderBindable(pathName);
                        currentFolder.SetChildFolder(newFolder);
                        currentFolder = newFolder;
                    }
                }
                currentFolder.SetChildNode(node);
            }
            return rootFolder;
        }
    }
}
