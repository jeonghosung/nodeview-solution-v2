﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class PropertyBindable: PropertyBindableBase
    {
        public override IProperty Clone()
        {
            PropertyBindable property = new PropertyBindable();
            Clone(property);
            return property;
        }

        public static PropertyBindable CreateFrom(JObject json)
        {
            PropertyBindable property = new PropertyBindable();
            property.LoadFrom(json);
            return property;
        }

        public static PropertyBindable CreateFrom(XmlNode xmlNode)
        {
            PropertyBindable property = new PropertyBindable();
            property.LoadFrom(xmlNode);
            return property;
        }
    }
}
