﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class NodeBindable : NodeBindableBase
    {
        public override INode CreateNode()
        {
            return new NodeBindable();
        }

        public static NodeBindableBase CreateFrom(INode sourceNode)
        {
            NodeBindableBase node = new NodeBindable();
            node.LoadFrom(sourceNode);
            return node;
        }

        public static NodeBindable CreateFrom(JObject json)
        {
            NodeBindable node = new NodeBindable();
            node.LoadFrom(json);
            return node;
        }

        public static NodeBindableBase CreateFrom(XmlNode xmlNode)
        {
            NodeBindable node = new NodeBindable();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}
