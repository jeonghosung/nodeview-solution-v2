﻿using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public abstract class NodeBindableBase : BindableBase, INode
    {
        protected string _id;
        protected string _path = "";
        protected NodeStatus _status = NodeStatus.None;
        protected string _statusMessage = "";
        protected string _updated = ""; //StringUtils.GetCurrentTimeMillisecondString(); 고민좀 해보자.
        protected string _nodeType;
        protected KeyValueCollectionBindable _attributes = new KeyValueCollectionBindable();
        protected PropertyCollectionBindable _properties = new PropertyCollectionBindable();

        public string PathId => string.IsNullOrWhiteSpace(Path) ? Id : $"{Path}/{Id}";

        public string Id
        {
            get => _id;
            set
            {
                SetProperty(ref _id, value);
                RaisePropertyChanged("PathId");
            } 
        }
        
        public string Path
        {
            get => _path;
            set
            {
                SetProperty(ref _path, value);
                RaisePropertyChanged("PathId");
            }
        }

        public NodeStatus Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }
        public string StatusMessage
        {
            get => _statusMessage;
            set => SetProperty(ref _statusMessage, value);
        }
        public string Updated
        {
            get => _updated;
            set => SetProperty(ref _updated, value);
        }
        public string NodeType
        {
            get => _nodeType;
            set => SetProperty(ref _nodeType, value);
        }

        public IKeyValueCollection Attributes => _attributes;
        public IPropertyCollection Properties => _properties;

        public object this[string search]
        {
            get
            {
                if (TryGetObject(search, out var @object))
                {
                    return @object;
                }

                return null;
            }
        }

        public bool Contains(string search)
        {
            if (TryGetObject(search, out var @object))
            {
                return true;
            }

            return false;
        }

        public virtual bool TryGetObject(string search, out object @object)
        {
            @object = null;
            if (string.IsNullOrWhiteSpace(search)) return false;

            if (search.StartsWith("@"))
            {
                string attrKey = search.Substring(1);
                if (string.IsNullOrWhiteSpace(attrKey)) return false;

                switch (attrKey)
                {
                    case "id":
                        @object = Id;
                        return true;
                    case "path":
                        @object = Path;
                        return true;
                    case "nodeType":
                        @object = NodeType;
                        return true;
                    case "status":
                        @object = Status;
                        return true;
                    case "statusMessage":
                        @object = StatusMessage;
                        return true;
                    case "updated":
                        @object = Updated;
                        return true;
                }

                if (Attributes.TryGetValue(attrKey, out var value))
                {
                    @object = value;
                    return true;
                }
            }
            else
            {
                if (Properties.TryGetProperty(search, out var property))
                {
                    @object = property;
                    return true;
                }
            }

            return false;
        }

        public T GetValue<T>(string search, T defaultValue)
        {
            return StringUtils.GetValue(GetStringValue(search), defaultValue);
        }

        public string GetStringValue(string search, string defaultValue = "")
        {
            string foundValueString = "";
            if (TryGetObject(search, out var @object))
            {
                if (@object is IPathNode path)
                {
                    foundValueString = $"{path.Path}";
                }
                else if (@object is INode node)
                {
                    foundValueString = $"{node.Path}/{node.Id}:{node.NodeType}";
                }
                else if (@object is IProperty property)
                {
                    foundValueString = property.Value;
                }
                else
                {
                    foundValueString = $"{@object}";
                }
            }

            return foundValueString;
        }

        public virtual bool SetAttribute(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;
            switch (key)
            {
                case "id":
                    Id = $"{value}";
                    break;
                case "path":
                    Path = $"{value}";
                    break;
                case "status":
                    Status = StringUtils.GetValue(value, Status);
                    break;
                case "statusMessage":
                    StatusMessage = $"{value}";
                    break;
                case "updated":
                    Updated = $"{value}";
                    break;
                case "nodeType":
                    NodeType = $"{value}";
                    break;
                case "properties":
                    break;
                default:
                    _attributes.Set(key, value);
                    break;
            }

            return true;
        }

        public bool SetAttribute(IKeyValuePair keyValuePair)
        {
            return SetAttribute(keyValuePair.Key, keyValuePair.Value);
        }

        public void SetAttributes(IEnumerable<IKeyValuePair> keyValuePairs)
        {
            _attributes.Sets(keyValuePairs);
        }

        public bool RemoveAttribute(string key)
        {
            return _attributes.Remove(key);
        }

        public void ClearAttributes()
        {
            _attributes.Clear();
        }

        public bool SetProperty(string id, string value)
        {
            IProperty property = new Property()
            {
                Id = id,
                Value = value
            };
            return SetProperty(property);
        }

        public virtual bool SetProperty(IProperty property)
        {
            return _properties.Set(property);
        }

        public void SetProperties(IEnumerable<IProperty> properties)
        {
            //_properties.Sets(properties);
            if (properties == null) return;

            foreach (var property in properties)
            {
                SetProperty(property);
            }
        }

        public bool RemoveProperty(string id)
        {
            return _properties.Remove(id);
        }

        public void ClearProperties()
        {
            _properties.Clear();
        }

        public INode Clone()
        {
            var node = CreateNode();
            Clone(node);
            return node;
        }
        protected virtual void Clone(INode node)
        {
            node?.LoadFrom((INode)this);
        }
        public abstract INode CreateNode();

        public virtual bool LoadFrom(INode sourceNode)
        {
            if (sourceNode == null) return false;

            _id = sourceNode.Id;
            _path = sourceNode.Path;
            _nodeType = sourceNode.NodeType;
            _status = sourceNode.Status;
            _statusMessage = sourceNode.StatusMessage;
            _updated = sourceNode.Updated;
            SetAttributes(sourceNode.Attributes);
            foreach (var property in sourceNode.Properties)
            {
                SetProperty(property.Clone());
            }

            return !string.IsNullOrWhiteSpace(Id);
        }
        public virtual JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            if (!string.IsNullOrWhiteSpace(Path)) json["path"] = Path;
            if (Status != NodeStatus.None) json["status"] = $"{Status}";
            if (!string.IsNullOrWhiteSpace(StatusMessage)) json["statusMessage"] = StatusMessage;
            if (!string.IsNullOrWhiteSpace(Updated)) json["updated"] = Updated;
            json["nodeType"] = NodeType;

            foreach (var attribute in Attributes)
            {
                json[attribute.Key] = $"{attribute.Value}";
            }

            JArray properties = new JArray();
            foreach (var property in Properties)
            {
                properties.Add(property.ToJson());
            }

            json["properties"] = properties;

            return json;
        }

        public virtual bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            foreach (var objectProperty in json.Properties())
            {
                switch (objectProperty.Name.ToLower())
                {
                    case "properties":
                        JArray propertyArray = objectProperty.Value.Value<JArray>();
                        if (propertyArray != null)
                        {
                            foreach (var propertyToken in propertyArray)
                            {
                                JObject propertyJson = propertyToken.Value<JObject>();
                                PropertyBindable property = PropertyBindable.CreateFrom(propertyJson);
                                if (property != null && !string.IsNullOrWhiteSpace(property.Id))
                                {
                                    SetProperty(property);
                                }
                            }
                        }
                        break;
                    default:
                        if (objectProperty.Value.Type != JTokenType.Array)
                        {
                            SetAttribute(objectProperty.Name, $"{objectProperty.Value}");
                        }
                        break;
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }

        public virtual void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = string.IsNullOrWhiteSpace(NodeType) ? "Node" : NodeType;
            }
            xmlWriter.WriteStartElement(tagName);
            if (tagName != NodeType)
            {
                xmlWriter.WriteAttributeString("nodeType", NodeType);
            }
            WriteXmlBody(xmlWriter);
            xmlWriter.WriteEndElement();
        }
        public virtual void WriteXmlBody(XmlWriter xmlWriter)
        {
            xmlWriter.WriteAttributeString("id", Id);
            if (!string.IsNullOrWhiteSpace(Path)) xmlWriter.WriteAttributeString("path", Path);
            if (Status != NodeStatus.None) xmlWriter.WriteAttributeString("status", $"{Status}");
            if (!string.IsNullOrWhiteSpace(StatusMessage)) xmlWriter.WriteAttributeString("statusMessage", StatusMessage);
            if (!string.IsNullOrWhiteSpace(Updated)) xmlWriter.WriteAttributeString("updated", Updated);

            foreach (var attribute in Attributes)
            {
                WriteXmlAttribute(xmlWriter, attribute.Key, $"{ attribute.Value}");
            }
            WriteXmlAttributeAfter(xmlWriter);
            foreach (var property in Properties)
            {
                property.WriteXml(xmlWriter);
            }
        }
        protected virtual void WriteXmlAttribute(XmlWriter xmlWriter, string key, string value)
        {
            xmlWriter.WriteAttributeString(key, value);
        }
        protected virtual void WriteXmlAttributeAfter(XmlWriter xmlWriter)
        {
        }

        public virtual bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (xmlNode.LocalName != "Node")
            {
                NodeType = xmlNode.LocalName;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    SetAttribute(attribute.Name, attribute.Value);
                }
            }

            XmlNodeList propertyNodes = xmlNode.SelectNodes("Property");
            if (propertyNodes != null)
            {
                foreach (XmlNode propertyNode in propertyNodes)
                {
                    PropertyBindable property = PropertyBindable.CreateFrom(propertyNode);
                    if (!string.IsNullOrWhiteSpace(property?.Id))
                    {
                        SetProperty(property);
                    }
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }
    }
}
