﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public class PathNodeBindable : BindableBase, IPathNode
    {
        private readonly Dictionary<string, INode> _dictionary = new Dictionary<string, INode>();
        private ObservableCollection<INode> _nodes = new ObservableCollection<INode>();
        private string _path = "";

        public string Path
        {
            get => _path;
            set => SetProperty(ref _path, value);
        }

        public IEnumerable<INode> Nodes => _nodes;
        public int Count => _nodes.Count;
        public string[] Ids => _dictionary.Keys.ToArray();
        public bool Contains(string id)
        {
            return _dictionary.ContainsKey(id);
        }

        public INode this[string id]
        {
            get
            {
                if (TryGetNode(id, out var node))
                {
                    return node;
                }

                return null;
            }
        }

        public bool TryGetNode(string id, out INode node)
        {
            return _dictionary.TryGetValue(id, out node);
        }

        #region set/remove/clear

        public bool Set(INode node)
        {
            if (string.IsNullOrWhiteSpace(node?.Id)) return false;

            if (_dictionary.TryGetValue(node.Id, out var oldNode))
            {
                _nodes.Remove(oldNode);
            }
            _nodes.Add(node);
            _dictionary[node.Id] = node;
            return true;
        }

        public bool Sets(IEnumerable<INode> nodes)
        {
            if (nodes == null) return false;

            foreach (var node in nodes)
            {
                Set(node);
            }

            return true;
        }

        public bool Remove(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (_dictionary.TryGetValue(id, out var oldNode))
            {
                _nodes.Remove(oldNode);
                _dictionary.Remove(id);
                return true;
            }

            return false;
        }

        public void Clear()
        {
            _nodes.Clear();
            _dictionary.Clear();
        }
        #endregion
    }
}
