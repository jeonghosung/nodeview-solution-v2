﻿using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public class KeyValuePairBindable : BindableBase, IKeyValuePair
    {
        private string _key;
        private object _value;

        public string Key
        {
            get => _key;
            set => SetProperty(ref _key, value);
        }

        public object Value
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }

        public KeyValuePairBindable(string key, object value)
        {
            _key = key;
            _value = value;
        }
        public KeyValuePairBindable() : this("", null)
        {
        }
        public KeyValuePairBindable(IKeyValuePair keyValuePair) : this(keyValuePair.Key, keyValuePair.Value)
        {
        }
        public T GetValue<T>(T defaultValue = default(T))
        {
            return StringUtils.GetValue(Value, defaultValue);
        }
    }
}
