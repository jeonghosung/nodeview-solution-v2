﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public abstract class TreeNodeBindableBase : NodeBindableBase, ITreeNode
    {
        private Dictionary<string, ITreeNode> _childNodeDictionary = new Dictionary<string, ITreeNode>();
        private ObservableCollection<ITreeNode> _childNodes = new ObservableCollection<ITreeNode>();
        public IEnumerable<ITreeNode> ChildNodes => _childNodes;

        public override bool TryGetObject(string search, out object @object)
        {
            @object = null;

            if (string.IsNullOrWhiteSpace(search)) return false;
            search = search.Replace('.', '/').Trim().Trim('/');
            
            (string path, string key) = StringUtils.SplitPathToOthersAndLast(search);
            if (!string.IsNullOrWhiteSpace(path))
            {
                if (TryGetNode(path, out var foundNode))
                {
                    if (foundNode.TryGetObject(key, out @object))
                    {
                        return true;
                    }
                }

                if (TryGetNode(search, out foundNode))
                {
                    @object = foundNode;
                    return true;
                }

                return false;
            }

            return base.TryGetObject(key, out @object);
        }

        public bool TryGetNode(string path, out ITreeNode node)
        {
            node = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            (string id, string subPath) = StringUtils.SplitPathToFirstAndOthers(path);
            if (_childNodeDictionary.TryGetValue(id, out var foundNode))
            {
                if (string.IsNullOrWhiteSpace(subPath))
                {
                    node = foundNode;
                    return true;
                }
                return foundNode.TryGetNode(subPath, out node);
            }
            return false;
        }

        public bool SetChildNode(ITreeNode node)
        {
            if (string.IsNullOrWhiteSpace(node?.Id)) return false;
            node.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";

            if (_childNodeDictionary.TryGetValue(node.Id, out var oldNode))
            {
                _childNodes.Remove(oldNode);
            }
            _childNodes.Add(node);
            _childNodeDictionary[node.Id] = node;
            return true;
        }

        public bool RemoveChildNode(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (_childNodeDictionary.TryGetValue(id, out var oldNode))
            {
                _childNodes.Remove(oldNode);
                _childNodeDictionary.Remove(id);
                return true;
            }

            return false;
        }

        public void Clear()
        {
            _childNodes.Clear();
            _childNodeDictionary.Clear();
        }

        public override bool LoadFrom(INode sourceNode)
        {
            if (sourceNode == null) return false;

            base.LoadFrom(sourceNode);
            if (sourceNode is ITreeNode sourceTreeNode)
            {
                foreach (var childNode in sourceTreeNode.ChildNodes)
                {
                    var node = CreateNode();
                    if (node is ITreeNode childTreeNode)
                    {
                        childTreeNode.LoadFrom(childNode);
                        _childNodeDictionary[childTreeNode.Id] = childTreeNode;
                        _childNodes.Add(childTreeNode);
                    }
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }

        public override JToken ToJson()
        {
            var json = base.ToJson().Value<JObject>();
            json = json ?? new JObject();

            JArray childNodes = new JArray();
            foreach (var childNode in _childNodeDictionary.Values)
            {
                childNodes.Add(childNode.ToJson());
            }

            json["childNodes"] = childNodes;
            return json;
        }

        public override bool LoadFrom(JObject json)
        {
            if (!base.LoadFrom(json)) return false;

            JArray childNodeArray = JsonUtils.GetValue(json, "childNodes", new JArray());

            foreach (var childNodeToken in childNodeArray)
            {
                JObject childNodeJson = childNodeToken.Value<JObject>();
                TreeNodeBindableBase childNode = (TreeNodeBindableBase)CreateNode();
                if (childNode?.LoadFrom(childNodeJson) ?? false)
                {
                    SetChildNode(childNode);
                }
            }
            return true;
        }

        public override void WriteXmlBody(XmlWriter xmlWriter)
        {
            base.WriteXmlBody(xmlWriter);
            foreach (var childNode in _childNodeDictionary.Values)
            {
                childNode.WriteXml(xmlWriter);
            }
        }

        public override bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (!base.LoadFrom(xmlNode)) return false;

            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                if (childNode.NodeType == XmlNodeType.Element && childNode.LocalName.ToLower() != "property")
                {
                    var node = CreateNode();
                    if (node is ITreeNode treeNode)
                    {
                        treeNode.LoadFrom(childNode);
                        if (!string.IsNullOrWhiteSpace(treeNode.Id))
                        {
                            SetChildNode(treeNode);
                        }
                    }
                }
            }

            return !string.IsNullOrWhiteSpace(Id);
        }
    }
}
