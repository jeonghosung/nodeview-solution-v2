﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class TreeNodeBindable : TreeNodeBindableBase
    {
        public override INode CreateNode()
        {
            return new TreeNode();
        }

        public static TreeNodeBindable CreateFrom(INode sourceNode)
        {
            TreeNodeBindable node = new TreeNodeBindable();
            node.LoadFrom(sourceNode);
            return node;
        }
        public static TreeNodeBindable CreateFrom(JObject json)
        {
            TreeNodeBindable node = new TreeNodeBindable();
            node.LoadFrom(json);
            return node;
        }

        public static TreeNodeBindable CreateFrom(XmlNode xmlNode)
        {
            TreeNodeBindable node = new TreeNodeBindable();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}
