﻿using System.Reflection;

namespace NodeView.Extensions
{
    public static class WindowExtensions
    {
        public static bool GetIsDisposed(this System.Windows.Window window)
        {
            bool isDisposed = true;
            try
            {
                var propertyInfo = typeof(System.Windows.Window).GetProperty("IsDisposed", BindingFlags.NonPublic | BindingFlags.Instance);

                if (propertyInfo != null)
                {
                    isDisposed = (bool)propertyInfo.GetValue(window);
                }
            }
            catch
            {
                //skip
            }

            return isDisposed;
        }
    }
}
