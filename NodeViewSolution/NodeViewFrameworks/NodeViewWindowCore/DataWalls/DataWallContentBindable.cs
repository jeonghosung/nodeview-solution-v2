﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataWalls
{
    public class DataWallContentBindable : DataWallContentBindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public override INode CreateNode()
        {
            return new DataWallContentBindable();
        }

        public static DataWallContentBindable CreateFrom(IDataWallContent sourceContent)
        {
            var content = new DataWallContentBindable();
            content.LoadFrom(sourceContent);
            return content;
        }

        public static DataWallContentBindable CreateFrom(INode sourceNode)
        {
            var content = new DataWallContentBindable();
            content.LoadFrom(sourceNode);
            return content;
        }

        public static DataWallContentBindable CreateFrom(JObject json)
        {
            var content = new DataWallContentBindable();
            content.LoadFrom(json);
            return content;
        }

        public static DataWallContentBindable CreateFrom(XmlNode xmlNode)
        {
            var content = new DataWallContentBindable();
            content.LoadFrom(xmlNode);
            return content;
        }
    }
}