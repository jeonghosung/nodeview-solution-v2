﻿using System;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataWalls
{
    public class DataWallWindowBindableBase : BindableBase, IDataWallWindow
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _name;
        private bool _isPopup = false;
        private IntRect _windowRect = IntRect.Empty;
        private string _contentId = "";
        private IDataWallContent _content = null;

        public string Id { get; }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public bool IsPopup
        {
            get => _isPopup;
            set => SetProperty(ref _isPopup, value);
        }

        public IntRect WindowRect
        {
            get => _windowRect;
            set => SetProperty(ref _windowRect, value);
        }

        public string ContentId
        {
            get => _contentId;
            set => SetProperty(ref _contentId, value);
        }

        public IDataWallContent Content
        {
            get => _content;
            set => SetProperty(ref _content, value);
        }

        public DataWallWindowBindableBase(IDataWallWindow window)
        {
            Id = window.Id;
            _name = window.Name;
            _isPopup = window.IsPopup;
            _windowRect = new IntRect(window.WindowRect);
            _contentId = window.ContentId;
            _content = window.Content == null ? null : DataWallContentBindable.CreateFrom(window.Content);
        }
        public DataWallWindowBindableBase(string id, string name = "", bool isPopup = false, IntRect screenRect = null, string contentId = "", DataWallContent content = null)
        {
            Id = string.IsNullOrWhiteSpace(id) ? Uuid.NewUuid : id;
            _isPopup = isPopup;
            _windowRect = screenRect ?? IntRect.Empty;
            _contentId = contentId;
            _content = content == null ? null : DataWallContentBindable.CreateFrom(content);
            _name = content != null ? content.Name : name;
        }

        public void SetContent(IDataWallContent content)
        {
            if (content != null)
            {
                Content = DataWallContentBindable.CreateFrom(content);
                Name = Content.Name;
            }
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                _name = JsonUtils.GetValue(json, "name", Name);
                _isPopup = JsonUtils.GetValue(json, "isPopup", IsPopup);
                _windowRect = JsonUtils.GetValue(json, "windowRect", new IntRect(WindowRect));
                _contentId = JsonUtils.GetValue(json, "contentId", ContentId);
                DataWallContentBindable content = null;
                JObject contentObject = JsonUtils.GetValue<JObject>(json, "content", null);
                if (contentObject != null)
                {
                    content = DataWallContentBindable.CreateFrom(contentObject);
                }

                _content = content;

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public JToken ToJson()
        {
            var jObject = new JObject
            {
                ["id"] = Id,
                ["name"] = Name,
                ["isPopup"] = IsPopup,
                ["windowRect"] = WindowRect.ToString()
            };
            if (!string.IsNullOrWhiteSpace(ContentId))
            {
                jObject["contentId"] = ContentId;
            }
            if (Content != null)
            {
                jObject["content"] = Content.ToJson();
            }
            return jObject;
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                _name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
                _isPopup = XmlUtils.GetAttrValue(xmlNode, "isPopup", IsPopup);
                _windowRect = XmlUtils.GetAttrValue(xmlNode, "windowRect", new IntRect(WindowRect));
                _contentId = XmlUtils.GetAttrValue(xmlNode, "contentId", ContentId);
                DataWallContentBindable content = null;
                XmlNode contentNode = xmlNode.SelectSingleNode("Content");
                if (contentNode != null)
                {
                    content = DataWallContentBindable.CreateFrom(contentNode);
                }

                _content = content;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Window";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("isPopup", $"{IsPopup}");
            xmlWriter.WriteAttributeString("windowRect", $"{WindowRect}");
            if (!string.IsNullOrWhiteSpace(ContentId))
            {
                xmlWriter.WriteAttributeString("contentId", $"{ContentId}");
            }

            Content?.WriteXml(xmlWriter, "Content");

            xmlWriter.WriteEndElement();
        }
    }
}
