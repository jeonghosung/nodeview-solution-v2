﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;

namespace NodeView.DataWalls
{
    public class DataWallWindowBindable : DataWallWindowBindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DataWallWindowBindable(IDataWallWindow window) : base(window)
        {
        }

        public DataWallWindowBindable(string id, string name = "", bool isPopup = false, IntRect screenRect = null, string contentId = "", DataWallContent content = null) : base(id, name, isPopup, screenRect, contentId, content)
        {
        }
        public static DataWallWindowBindable CreateFrom(JObject json)
        {
            try
            {
                string id = JsonUtils.GetStringValue(json, "id", Uuid.NewUuid);
                if (json == null || string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }

                var window = new DataWallWindowBindable(id);
                if (window.LoadFrom(json))
                {
                    return window;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public static DataWallWindowBindable CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetAttrValue(xmlNode, "id", Uuid.NewUuid);
                var window = new DataWallWindowBindable(id);
                if (window.LoadFrom(xmlNode))
                {
                    return window;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
    }
}
