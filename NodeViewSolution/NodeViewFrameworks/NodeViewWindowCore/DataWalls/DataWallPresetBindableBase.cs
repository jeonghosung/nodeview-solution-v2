﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataWalls
{
    public class DataWallPresetBindableBase : BindableBase, IDataWallPreset
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private string _name;
        private List<DataWallWindowBindable> _windowList;
        public string Id { get; }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public IEnumerable<IDataWallWindow> Windows => _windowList;


        public DataWallPresetBindableBase(IDataWallPreset preset)
        {
            Id = preset.Id;
            _name = preset.Name;
            _windowList = new List<DataWallWindowBindable>();
            if (preset.Windows != null)
            {
                foreach (var window in preset.Windows)
                {
                    _windowList.Add(new DataWallWindowBindable(window));
                }
            }
        }
        public DataWallPresetBindableBase(string id, string name = "", IEnumerable<IDataWallWindow> windows = null)
        {
            Id = string.IsNullOrWhiteSpace(id) ? "" : id;
            _name = string.IsNullOrWhiteSpace(name) ? Id : name;
            _windowList = new List<DataWallWindowBindable>();
            if (windows != null)
            {
                foreach (var window in windows)
                {
                    _windowList.Add(new DataWallWindowBindable(window));
                }
            }
        }


        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                Name = JsonUtils.GetStringValue(json, "name", Name);

                List<DataWallWindowBindable> windowList = new List<DataWallWindowBindable>();
                JArray windowJArray = JsonUtils.GetValue<JArray>(json, "windows", null);
                if (windowJArray != null)
                {
                    foreach (JToken windowToken in windowJArray)
                    {
                        JObject windowJsonObject = windowToken.Value<JObject>();
                        var window = DataWallWindowBindable.CreateFrom(windowJsonObject);
                        if (window != null)
                        {
                            windowList.Add(window);
                        }
                    }
                }

                _windowList = windowList;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public JToken ToJson()
        {
            var jObject = new JObject();
            jObject["id"] = Id;
            jObject["name"] = Name;
            JArray windowArray = new JArray();
            foreach (var window in Windows)
            {
                windowArray.Add(window.ToJson());
            }
            jObject["windows"] = windowArray;

            return jObject;
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);

                List<DataWallWindowBindable> windowList = new List<DataWallWindowBindable>();
                XmlNodeList windowNodeList = xmlNode.SelectNodes("Window");
                if (windowNodeList != null)
                {
                    foreach (XmlNode windowNode in windowNodeList)
                    {
                        var window = DataWallWindowBindable.CreateFrom(windowNode);
                        if (window != null)
                        {
                            windowList.Add(window);
                        }
                    }
                }

                _windowList = windowList;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Preset";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);

            if (Windows != null)
            {
                foreach (var window in Windows)
                {
                    window.WriteXml(xmlWriter, "Window");
                }
            }

            xmlWriter.WriteEndElement();
        }
    }
}
