﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataWalls
{
    public class DataWallPresetBindable : DataWallPresetBindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private bool _isSelected = false;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }
        public DataWallPresetBindable(IDataWallPreset preset) : base(preset)
        {
        }

        public DataWallPresetBindable(string id, string name = "", IEnumerable<IDataWallWindow> windows = null) : base(id, name, windows)
        {
        }

        public static DataWallPresetBindable CreateFrom(JObject json)
        {
            if (json == null)
            {
                return null;
            }
            try
            {
                string id = JsonUtils.GetStringValue(json, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var preset = new DataWallPresetBindable(id);
                if (preset.LoadFrom(json))
                {
                    return preset;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public static DataWallWindowBindable CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetStringAttrValue(xmlNode, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var preset = new DataWallWindowBindable(id);
                if (preset.LoadFrom(xmlNode))
                {
                    return preset;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
    }
}
