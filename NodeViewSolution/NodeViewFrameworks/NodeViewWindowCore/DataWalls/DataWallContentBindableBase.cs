﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataWalls
{
    public abstract class DataWallContentBindableBase : NodeBindableBase, IDataWallContent
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string ContentType
        {
            get => _attributes.GetStringValue("contentType");
            set
            {
                if (_attributes.Set("contentType", value))
                {
                    RaisePropertyChanged();
                }
            } 
        }

        public string Name
        {
            get => _attributes.GetStringValue("name");
            set
            {
                if (_attributes.Set("name", value))
                {
                    RaisePropertyChanged();
                }
            }
        }
        public string Uri
        {
            get => _attributes.GetStringValue("uri");
            set
            {
                if (_attributes.Set("uri", value))
                {
                    RaisePropertyChanged();
                }
            }
        }

        public string UserId
        {
            get => _attributes.GetStringValue("userId");
            set
            {
                if (_attributes.Set("userId", value))
                {
                    RaisePropertyChanged();
                }
            }

        }

        public string UserPassword
        {
            get => _attributes.GetStringValue("userPassword");
            set
            {
                if (_attributes.Set("userPassword", value))
                {
                    RaisePropertyChanged();
                }
            }
        }
        public DataWallContentBindableBase(IDataWallContent content = null)
        {
            NodeType = "DataWallContent";

            if (content != null)
            {
                Id = content.Id;
                ContentType = content.ContentType;
                Path = content.Path;
                Name = content.Name;
                Uri = content.Uri;
                _properties.Sets(content.Properties);
            }
        }

        public override bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                if (base.LoadFrom(xmlNode))
                {
                    Uri = XmlUtils.GetStringInnerText(xmlNode, "Uri", Uri);
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        protected override void WriteXmlAttribute(XmlWriter xmlWriter, string key, string value)
        {
            if (key == "uri")
            {
                //move on "WriteXmlAttributeAfter"
            }
            else
            {
                base.WriteXmlAttribute(xmlWriter, key, value);
            }
        }
        protected override void WriteXmlAttributeAfter(XmlWriter xmlWriter)
        {
            xmlWriter.WriteElementString("Uri", Uri);
        }
    }
}