﻿using System;
using System.Windows;
using NodeView.Extensions;

namespace NodeView
{
    public class StylesExtension : StyleRefExtension
    {
        static StylesExtension()
        {
            RD = new ResourceDictionary()
            {
                Source = new Uri("pack://application:,,,/NodeViewWindowCore;component/CoreStyles.xaml")
            };
        }
    }
}
