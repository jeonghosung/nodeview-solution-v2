﻿using NLog;
using NodeView.DataWalls;
using Prism.Mvvm;

namespace NodeView.DataWallContentControls
{
    public class DummyDataWallContentControlModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _name;
        public string Id { get; }
        public string ContentType { get; }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }
        public DummyDataWallContentControlModel(IDataWallContent content)
        {
            Id = content.Id;
            ContentType = content.ContentType;
            _name = content.Name;
        }
    }
}
