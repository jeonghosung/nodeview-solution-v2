﻿using System;
using System.Windows;
using System.Windows.Controls;
using NodeView.DataWalls;
using NodeView.Frameworks.DataWallContentControls;

namespace NodeView.DataWallContentControls
{
    /// <summary>
    /// DummyViewControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DummyDataWallContentControl : UserControl , IDataWallContentControl
    {
        public DummyDataWallContentControl()
        {
            InitializeComponent();
        }

        public IDataWallContent Source
        {
            get => (IDataWallContent)GetValue(SourceProperty);
            set => SetValue(SourceProperty, value);
        }

        public static readonly DependencyProperty SourceProperty =
            DependencyProperty.Register("Source", typeof(IDataWallContent), typeof(DummyDataWallContentControl), new PropertyMetadata(OnSourcePropertyChanged));

        private static void OnSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            /*
            if (d is DummyDataWallContentControl owner)
            {
                if (e.NewValue is IDataWallContent nodeViewContent)
                {
                    if (owner.DataContext != nodeViewContent)
                    {
                        owner.DataContext = nodeViewContent;
                    }
                }
            }
            */
        }
    }
}
