﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class ContentEditViewModel : BindableBase, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IDataWallContent  _originDataWallContent;
        private ContentItem _changeDatawallContent;
        private bool _canEditId;
        private ObservableCollection<PropertyEditModel> _properties = new ObservableCollection<PropertyEditModel>();
        private bool disposed = false;
        private bool _isContentModify = false;
        private bool _isPropertyModified = false;
        private bool _isCreateNew = false;
        private string _contentPath = string.Empty;

        public bool IsContentModify
        {
            get => _isContentModify;
            set => SetProperty(ref _isContentModify, value);
        }
        public EditingField IdField { get; } 
        public EditingField NameField { get; } 
        public EditingField ContentTypeField { get; } 
        public EditingField UriField { get; } 
        public EditingField UserIdField { get; } 
        public EditingField UserPasswordField { get; }
        public bool IsCreateNew => _isCreateNew;
        public bool CanEditId
        {
            get => _canEditId;
            set => SetProperty(ref _canEditId, value);
        }
        public ObservableCollection<PropertyEditModel> Properties
        {
            get => _properties;
            set => SetProperty(ref _properties, value);
        }
        public ContentEditViewModel(ContentItem dataWallContent, bool isCreateNew)
        {
            foreach (var contentItemProperty in dataWallContent.Properties)
            {
                PropertyViewModel propertyViewModel = PropertyViewModel.CreateFrom(contentItemProperty);
                var propertyEditModel = new PropertyEditModel(propertyViewModel);
                propertyEditModel.IsModifyChangeHandler += IsModifyChangeEvent;
                Properties.Add(propertyEditModel);
            }

            _changeDatawallContent = dataWallContent;
            _originDataWallContent = DataWallContent.CreateFrom(dataWallContent);
            _contentPath = dataWallContent.Path;
            _isCreateNew = isCreateNew;
            IdField = new("Id", dataWallContent.Id,valueType:"id");
            NameField = new("Name", dataWallContent.Name, valueType: "string");
            ContentTypeField = new("ContentType",  dataWallContent.ContentType, valueType: "string");
            UriField = new("Uri", dataWallContent.Uri, valueType: "string");
            UserIdField = new("UserId", dataWallContent.UserId  , valueType: "string");
            UserPasswordField = new("UserPassword", dataWallContent.UserPassword, valueType: "string");
           
            SetEditingFieldIsModifiedChangedEvent();
            IdField.IsReadOnly = !_isCreateNew;
        }
        public void SetUpdateMode()
        {
            _isCreateNew = false;
        }
        private void SetEditingFieldIsModifiedChangedEvent()
        {
            IdField.IsModifiedChanged += Field_IsModifiedChanged;
            NameField.IsModifiedChanged += Field_IsModifiedChanged;
            ContentTypeField.IsModifiedChanged += Field_IsModifiedChanged;
            UriField.IsModifiedChanged += Field_IsModifiedChanged;
            UserIdField.IsModifiedChanged += Field_IsModifiedChanged;
            UserPasswordField.IsModifiedChanged += Field_IsModifiedChanged;
        }
        private void Field_IsModifiedChanged(object? sender, EventArgs e)
        {
            CheckContentModified();
        }
        private void ResetProperties()
        {
            if (Properties != null)
            {
                foreach (var property in Properties)
                {
                    property.IsModifyChangeHandler -= IsModifyChangeEvent;
                }
                Properties.Clear();
            }

            foreach (var property in _originDataWallContent.Properties)
            {
                PropertyViewModel propertyViewModel = PropertyViewModel.CreateFrom(property);
                var contentPropertyEditModel = new PropertyEditModel(propertyViewModel);
                contentPropertyEditModel.IsModifyChangeHandler += IsModifyChangeEvent;
                Properties.Add(contentPropertyEditModel);
            }
        }
        private void CheckContentModified()
        {
            IsContentModify = IdField.IsModified || NameField.IsModified || ContentTypeField.IsModified ||
                              UriField.IsModified || UserIdField.IsModified || UserPasswordField.IsModified || _isPropertyModified;
        }
        public void Reset()
        {
            IdField.Value = IdField.DefaultValue = _originDataWallContent.Id;
            NameField.Value = NameField.DefaultValue = _originDataWallContent.Name;
            ContentTypeField.Value = _originDataWallContent.ContentType;
            UriField.Value = _originDataWallContent.Uri;
            UserIdField.Value = _originDataWallContent.UserId;
            UserPasswordField.Value = _originDataWallContent.UserPassword;
            ResetProperties();
            IsContentModify = false;
        }
        public ContentItem GetCurrentDataWallContent()
        {
            _changeDatawallContent.Id = IdField.Value;
            _changeDatawallContent.Name = NameField.Value;
            _changeDatawallContent.Uri = UriField.Value;
            _changeDatawallContent.ContentType = ContentTypeField.Value;
            _changeDatawallContent.UserPassword = UserPasswordField.Value;
            _changeDatawallContent.UserId = UserIdField.Value;
            _changeDatawallContent.Path = _contentPath;
            _changeDatawallContent.ClearProperties();
            foreach (var propertyEditModel in Properties)
            {
                var property = new PropertyViewModel()
                {
                    Id = propertyEditModel.Id,
                    Value = propertyEditModel.Value
                };
                property.SetAttribute("description", propertyEditModel.Description);
                _changeDatawallContent.SetProperty(property);
            }
            return _changeDatawallContent;
        }
        public void SetContentDataFromCurrentData()
        {
            IdField.DefaultValue = IdField.Value;
            IdField.IsModified = false;
            NameField.DefaultValue = NameField.Value;
            NameField.IsModified = false;
            UriField.DefaultValue = UriField.Value;
            UriField.IsModified = false;
            ContentTypeField.DefaultValue = ContentTypeField.Value;
            ContentTypeField.IsModified = false;
            UserPasswordField.DefaultValue = UserPasswordField.Value;
            UserPasswordField.IsModified = false;
            UserIdField.DefaultValue = UserIdField.Value;
            UserIdField.IsModified = false;
            IsContentModify = false;
            foreach (var propertyEditModel in Properties)
            {
                propertyEditModel.SetEditFieldDataFromCurrentData();
            }

            CanEditId = false;
            _originDataWallContent = GetCurrentDataWallContent();
            _isPropertyModified = false;
            _isCreateNew = false;
        }
        public void AddProperty(PropertyViewModel property)
        {
            if(property == null)
            {
                Logger.Warn("AddProperty property is null");
                return;
            }
            var contentPropertyEditModel = new PropertyEditModel(property);
            contentPropertyEditModel.IsModifyChangeHandler += IsModifyChangeEvent;
            Properties.Add(contentPropertyEditModel);
            CheckPropertiesIsModified();
        }
        public void RemoveProperty(string id)
        {
            var removeItem = Properties.First(list => list.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
            removeItem.IsModifyChangeHandler -= IsModifyChangeEvent;
            Properties.Remove(removeItem);
            CheckPropertiesIsModified();
        }
        private void CheckPropertiesIsModified()
        {
            var originPropertyIdList = _originDataWallContent.Properties.Select(property => property.Id).OrderBy(property => property);
            var currentPropertyIdList = Properties.Select(property => property.Id).OrderBy(property => property); ;
            _isPropertyModified = !originPropertyIdList.SequenceEqual(currentPropertyIdList);
            CheckContentModified();
        }
        public ContentEditViewModel GetCopyContentEditViewModel()
        {
            var copyContentItem = ContentItem.CreateFrom(_originDataWallContent);
            copyContentItem.Id = $"{copyContentItem.Id}_copy";
            ContentEditViewModel contentEditViewModel = new ContentEditViewModel(copyContentItem, true);
            contentEditViewModel.CanEditId = true;
            contentEditViewModel.IdField.IsReadOnly = false;
            return contentEditViewModel;
        }
        private void IsModifyChangeEvent(object? sender, EventArgs e)
        {
            _isPropertyModified = Properties.Any(property => property.IsPropertyModified);
            CheckContentModified();
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    foreach (var property in Properties)
                    {
                        property.IsModifyChangeHandler -= IsModifyChangeEvent;
                    }
                    IdField.IsModifiedChanged -= Field_IsModifiedChanged;
                    NameField.IsModifiedChanged -= Field_IsModifiedChanged;
                    ContentTypeField.IsModifiedChanged -= Field_IsModifiedChanged;
                    UriField.IsModifiedChanged -= Field_IsModifiedChanged;
                    UserIdField.IsModifiedChanged -= Field_IsModifiedChanged;
                    UserPasswordField.IsModifiedChanged -= Field_IsModifiedChanged;
                }
                disposed = true;
            }
        }
    }

    public class PropertyEditModel : BindableBase
    {
        private bool _isPropertyModified = false;
        public bool IsPropertyModified => _isPropertyModified;
        public EventHandler IsModifyChangeHandler;
        public EditingField ValueField { get; }
        public EditingField DescriptionField { get; }
        public string Id { get; }
        public string Value => ValueField.Value;
        public string Description => DescriptionField.Value;
        public PropertyEditModel(PropertyViewModel propertyViewModel)
        {
            Id = propertyViewModel.Id;
            ValueField = new ("value", propertyViewModel.Value, valueType: "string");
            ValueField.IsModifiedChanged += EditField_IsModifiedChanged;
            DescriptionField = new("description",propertyViewModel.Description, valueType: "string");
            DescriptionField.IsModifiedChanged += EditField_IsModifiedChanged;
        }
        private void EditField_IsModifiedChanged(object? sender, EventArgs e)
        {
            _isPropertyModified = ValueField.IsModified || DescriptionField.IsModified;
            IsModifyChangeHandler?.Invoke(this, null);
        }
        public void SetEditFieldDataFromCurrentData()
        {
            ValueField.DefaultValue = ValueField.Value;
            ValueField.IsModified = false;
            DescriptionField.DefaultValue = DescriptionField.Value;
            DescriptionField.IsModified = false;
            _isPropertyModified = false;
        }
    }
}
