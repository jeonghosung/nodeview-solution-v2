﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class AppLicenseItem : BindableBase
    {
        private string _name;
        private int _licenseCount;
        private bool _isSelected;

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public int LicenseCount
        {
            get => _licenseCount;
            set => SetProperty(ref _licenseCount, value);
        }

        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public ObservableCollection<NodeViewAppItem> RegisteredApps { get; } = new ObservableCollection<NodeViewAppItem>();
        public ObservableCollection<NodeViewAppItem> BlockedApps { get; } = new ObservableCollection<NodeViewAppItem>();

        public static AppLicenseItem CreateFrom(JObject json)
        {
            var item = new AppLicenseItem();
            item._name = JsonUtils.GetStringValue(json, "name");
            item._licenseCount = JsonUtils.GetIntValue(json, "licenseCount");
            var registeredArray = JsonUtils.GetValue(json, "registered", new JArray());
            foreach (var registeredAppToken in registeredArray)
            {
                var app = NodeViewAppItem.CreateFrom(registeredAppToken.Value<JObject>());
                if (app != null)
                {
                    item.RegisteredApps.Add(app);
                }
            }
            var blockedArray = JsonUtils.GetValue(json, "blocked", new JArray());
            foreach (var blockedAppToken in blockedArray)
            {
                var app = NodeViewAppItem.CreateFrom(blockedAppToken.Value<JObject>());
                if (app != null)
                {
                    item.BlockedApps.Add(app);
                }
            }
            return item;
        }

        public NodeViewAppItem FindApp(string appId)
        {
            foreach (var app in RegisteredApps.ToArray())
            {
                if (app.Id == appId)
                {
                    return app;
                }
            }
            foreach (var app in BlockedApps.ToArray())
            {
                if (app.Id == appId)
                {
                    return app;
                }
            }

            return null;
        }

        public bool RemoveApp(string appId)
        {
            foreach (var app in RegisteredApps.ToArray())
            {
                if (app.Id == appId)
                {
                    RegisteredApps.Remove(app);
                    return true;
                }
            }
            foreach (var app in BlockedApps.ToArray())
            {
                if (app.Id == appId)
                {
                    BlockedApps.Remove(app);
                    return true;
                }
            }

            return false;
        }

        public bool BlockApp(string appId)
        {
            foreach (var app in RegisteredApps.ToArray())
            {
                if (app.Id == appId)
                {
                    RegisteredApps.Remove(app);
                    app.IsBlocked = true;
                    app.Path = $"{Name}/blocked";
                    BlockedApps.Add(app);
                    return true;
                }
            }
            return false;
        }
    }
}
