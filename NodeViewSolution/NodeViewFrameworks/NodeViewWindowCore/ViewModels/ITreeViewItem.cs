﻿using System.Collections.ObjectModel;
using NodeView.DataModels;

namespace NodeView.ViewModels
{
    public interface ITreeViewItem : IObject
    {
        string Path { get; set; }
        ObservableCollection<ITreeViewItem> ChildItems { get; }
    }
}