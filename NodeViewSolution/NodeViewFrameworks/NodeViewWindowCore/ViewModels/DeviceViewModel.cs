﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class DeviceViewModel : BindableBase, IDevice
    {
        private CommandDescription[] _commandDescriptions = null;
        protected KeyValueCollection _attributes = new KeyValueCollection();
        private string _id = "";
        protected string _path = "";
        protected NodeStatus _status = NodeStatus.None;
        protected string _statusMessage = "";
        protected string _updated = "";
        private string _deviceType = "device";
        private string _name = "";
        private string _description = "";
        private string _connectionString = "";
        private bool _isSelected;

        public string PathId => string.IsNullOrWhiteSpace(Path) ? Id : $"{Path}/{Id}";

        public string Id
        {
            get => _id;
            set
            {
                SetProperty(ref _id, value);
                RaisePropertyChanged("PathId");
            }
        }

        public string Path
        {
            get => _path;
            set
            {
                SetProperty(ref _path, value);
                RaisePropertyChanged("PathId");
            }
        }

        public NodeStatus Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }
        public string StatusMessage
        {
            get => _statusMessage;
            set => SetProperty(ref _statusMessage, value);
        }
        public string Updated
        {
            get => _updated;
            set => SetProperty(ref _updated, value);
        }

        public string DeviceType
        {
            get => _deviceType;
            set => SetProperty(ref _deviceType, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public string ConnectionString
        {
            get => _connectionString;
            set => SetProperty(ref _connectionString, value);
        }
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public IKeyValueCollection Attributes => _attributes;

        public CommandDescription[] CommandDescriptions
        {
            get => _commandDescriptions ?? (_commandDescriptions = GetCommandDescriptions());
            protected set => _commandDescriptions = value;
        }

        protected virtual CommandDescription[] GetCommandDescriptions()
        {
            return new CommandDescription[0];
        }
        public virtual INode[] GetNodes()
        {
            return new INode[0];
        }

        public virtual bool RunCommand(string command, KeyValueCollection parameters)
        {
            return false;
        }

        public CommandDescription FindCommandDescription(string command)
        {
            if (string.IsNullOrWhiteSpace(command)) return null;
            foreach (var commandDescription in CommandDescriptions)
            {
                if (commandDescription.Command == command)
                {
                    return commandDescription;
                }
            }

            return null;
        }

        protected virtual bool SetAttribute(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;
            switch (key)
            {
                case "id":
                    Id = $"{value}";
                    break;
                case "path":
                    Path = $"{value}";
                    break;
                case "status":
                    Status = StringUtils.GetValue(value, Status);
                    break;
                case "statusMessage":
                    StatusMessage = $"{value}";
                    break;
                case "updated":
                    Updated = $"{value}";
                    break;
                case "deviceType":
                    DeviceType = $"{value}";
                    break;
                case "name":
                    Name = $"{value}";
                    break;
                case "description":
                    Description = $"{value}";
                    break;
                case "connectionString":
                    ConnectionString = $"{value}";
                    break;
                case "propertyDescriptions":
                case "commandDescriptions":
                    break;
                default:
                    _attributes.Set(key, value);
                    break;
            }

            return true;
        }

        public virtual JToken ToJson()
        {
            JObject json = new JObject();
            WriteBodyJson(json);
            return json;
        }

        protected virtual void WriteBodyJson(JObject json)
        {
            json["id"] = Id;
            if (!string.IsNullOrWhiteSpace(Path)) json["path"] = Path;
            if (Status != NodeStatus.None) json["status"] = $"{Status}";
            if (!string.IsNullOrWhiteSpace(StatusMessage)) json["statusMessage"] = StatusMessage;
            if (!string.IsNullOrWhiteSpace(Updated)) json["updated"] = Updated;

            json["deviceType"] = DeviceType;
            json["name"] = Name;
            json["description"] = Description;
            json["connectionString"] = ConnectionString;
            WriteAttributesJson(json);

            JArray commandDescriptionJsonArray = new JArray();
            foreach (var commandDescription in CommandDescriptions)
            {
                commandDescriptionJsonArray.Add(commandDescription.ToJson());
            }
            json["commandDescriptions"] = commandDescriptionJsonArray;
        }
        protected virtual void WriteAttributesJson(JObject json)
        {
            var attributes = _attributes.ToArray();
            foreach (var attribute in attributes)
            {
                json[attribute.Key] = $"{attribute.Value}";
            }
        }
        public virtual bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            foreach (var objectProperty in json.Properties())
            {
                switch (objectProperty.Name)
                {
                    case "commandDescriptions":
                        JArray commandDescriptionJsonArray = objectProperty.Value.Value<JArray>();
                        List<CommandDescription> commandDescriptions = new List<CommandDescription>();
                        if (commandDescriptionJsonArray != null)
                        {
                            foreach (var commandDescriptionToken in commandDescriptionJsonArray)
                            {
                                var commandDescription = CommandDescription.CreateFrom(commandDescriptionToken.Value<JObject>());
                                if (commandDescription != null)
                                {
                                    commandDescriptions.Add(commandDescription);
                                }
                            }
                        }
                        _commandDescriptions = commandDescriptions.ToArray();
                        break;
                    default:
                        if (objectProperty.Value.Type != JTokenType.Array)
                        {
                            SetAttribute(objectProperty.Name, $"{objectProperty.Value}");
                        }
                        break;
                }
            }

            return !string.IsNullOrWhiteSpace(Id);
        }

        public virtual void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Device" : tagName;
            xmlWriter.WriteStartElement(tagName);

            WriteBodyXml(xmlWriter);

            xmlWriter.WriteEndElement();
        }

        protected virtual void WriteBodyXml(XmlWriter xmlWriter)
        {
            xmlWriter.WriteAttributeString("id", Id);
            if (!string.IsNullOrWhiteSpace(Path)) xmlWriter.WriteAttributeString("path", Path);
            if (Status != NodeStatus.None) xmlWriter.WriteAttributeString("status", $"{Status}");
            if (!string.IsNullOrWhiteSpace(StatusMessage)) xmlWriter.WriteAttributeString("statusMessage", StatusMessage);
            if (!string.IsNullOrWhiteSpace(Updated)) xmlWriter.WriteAttributeString("updated", Updated);

            xmlWriter.WriteAttributeString("deviceType", DeviceType);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("description", Description);
            xmlWriter.WriteStartElement("ConnectionString", ConnectionString);
            WriteAttributeXml(xmlWriter);
            if (CommandDescriptions.Length > 0)
            {
                xmlWriter.WriteStartElement("CommandDescriptions");
                foreach (var commandDescription in CommandDescriptions)
                {
                    commandDescription.WriteXml(xmlWriter, "Command");
                }
                xmlWriter.WriteEndElement();
            }
        }

        protected virtual void WriteAttributeXml(XmlWriter xmlWriter)
        {
            var attributes = _attributes.ToArray();
            foreach (var attribute in attributes)
            {
                xmlWriter.WriteAttributeString(attribute.Key, $"{attribute.Value}");
            }
        }

        public virtual bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    SetAttribute(attribute.Name, attribute.Value);
                }
            }

            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            XmlNodeList commandDescriptionNodes = xmlNode.SelectNodes("CommandDescriptions/Command");
            if (commandDescriptionNodes != null)
            {
                foreach (XmlNode commandDescriptionNode in commandDescriptionNodes)
                {
                    CommandDescription.CreateFrom(commandDescriptionNode);
                }
            }

            return !string.IsNullOrWhiteSpace(Id);
        }

        public static DeviceViewModel CreateFrom(JObject json)
        {
            var device = new DeviceViewModel();
            device.LoadFrom(json);
            return device;
        }

        public static DeviceViewModel CreateFrom(XmlNode xmlNode)
        {
            var device = new DeviceViewModel();
            device.LoadFrom(xmlNode);
            return device;
        }
    }
}
