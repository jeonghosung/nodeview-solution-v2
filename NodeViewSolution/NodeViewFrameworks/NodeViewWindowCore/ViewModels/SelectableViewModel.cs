﻿using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class SelectableViewModel : BindableBase
    {
        private bool _isSelected = false;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }
    }
}
