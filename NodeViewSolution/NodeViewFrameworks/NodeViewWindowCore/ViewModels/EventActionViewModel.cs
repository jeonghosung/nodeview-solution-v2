﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class EventActionViewModel : BindableBase, IEventAction
    {
        private string _id = "";
        private AlarmEventLevel _level;
        private string _description = "";
        private string _eventOnActionId = "";
        private string _eventOffActionId = "";
        private bool _isSelected;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public AlarmEventLevel Level
        {
            get => _level;
            set => SetProperty(ref _level, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public string EventOnActionId
        {
            get => _eventOnActionId;
            set => SetProperty(ref _eventOnActionId, value);
        }

        public string EventOffActionId
        {
            get => _eventOffActionId;
            set => SetProperty(ref _eventOffActionId, value);
        }
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["level"] = $"{Level}";
            json["description"] = Description;
            json["eventOnActionId"] = EventOnActionId;
            json["eventOffActionId"] = EventOffActionId;

            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", "");
            Level = JsonUtils.GetValue(json, "level", AlarmEventLevel.Alarm);
            Description = JsonUtils.GetValue(json, "description", "");
            EventOnActionId = JsonUtils.GetValue(json, "eventOnActionId", "");
            EventOffActionId = JsonUtils.GetValue(json, "eventOffActionId", "");

            return !string.IsNullOrWhiteSpace(Id);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Event" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("level", $"{Level}");
            xmlWriter.WriteAttributeString("description", Description);
            xmlWriter.WriteAttributeString("eventOnActionId", EventOnActionId);
            xmlWriter.WriteAttributeString("eventOffActionId", EventOffActionId);
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", "");
            Level = XmlUtils.GetAttrValue(xmlNode, "level", AlarmEventLevel.Alarm);
            Description = XmlUtils.GetAttrValue(xmlNode, "description", "");
            EventOnActionId = XmlUtils.GetAttrValue(xmlNode, "eventOnActionId", "");
            EventOffActionId = XmlUtils.GetAttrValue(xmlNode, "eventOffActionId", "");

            return !string.IsNullOrWhiteSpace(Id);
        }

        public static EventActionViewModel CreateFrom(JObject json)
        {
            var alarmEvent = new EventActionViewModel();
            alarmEvent.LoadFrom(json);
            return alarmEvent;
        }

        public static EventActionViewModel CreateFrom(XmlNode xmlNode)
        {
            var alarmEvent = new EventActionViewModel();
            alarmEvent.LoadFrom(xmlNode);
            return alarmEvent;
        }

        public void Update(EventActionViewModel source)
        {
            if (source == null) return;

            Level = source.Level;
            Description = source.Description;
            EventOnActionId = source.EventOnActionId;
            EventOffActionId = source.EventOffActionId;
        }
        private void LoadFrom(EventActionViewModel source)
        {
            Id = source.Id;
            Level = source.Level;
            Description = source.Description;
            EventOnActionId = source.EventOnActionId;
            EventOffActionId = source.EventOffActionId;
        }

        public static EventActionViewModel CreateFrom(EventActionViewModel source)
        {
            var alarmEvent = new EventActionViewModel();
            alarmEvent.LoadFrom(source);
            return alarmEvent;
        }

    }
}
