﻿using System.Collections.ObjectModel;
using Prism.Mvvm;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Windows.Markup;
using NodeView.Utils;

namespace NodeView.ViewModels
{
    public class NodeFolderTreeItem : BindableBase
    {
        private string _id = "";
        private string _path = "";

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Path
        {
            get => _path;
            set => SetProperty(ref _path, value);
        }

        public ObservableCollection<NodeFolderTreeItem> ChildFolders { get; } = new ObservableCollection<NodeFolderTreeItem>();

        public NodeFolderTreeItem(string uid)
        {
            if (!string.IsNullOrWhiteSpace(uid))
            {
                Id = System.IO.Path.GetFileName(uid);
                Path = uid;
            }
        }

        public void AddChild(string uid)
        {
            string localPath = uid;
            if (localPath.StartsWith(Path))
            {
                localPath = localPath.Substring(Path.Length).Trim('/');
            }
            string[] splitPath = StringUtils.Split(localPath, '/');
            if (splitPath.Length > 1)
            {
                NodeFolderTreeItem matchedFolder = null;
                foreach (var childFolder in ChildFolders.ToArray())
                {
                    if (childFolder.Id == splitPath[0])
                    {
                        matchedFolder = childFolder;
                        break;
                    }
                }

                if (matchedFolder == null)
                {
                    string subPath = string.IsNullOrWhiteSpace(Path) ? splitPath[0] : $"{Path}/{splitPath[0]}";
                    matchedFolder = new NodeFolderTreeItem(subPath);
                    ChildFolders.Add(matchedFolder);
                }

                matchedFolder.AddChild(splitPath.SubArray(1, splitPath.Length - 1));
            }
            else if (splitPath.Length == 1)
            {
                NodeFolderTreeItem matchedFolder = null;
                foreach (var childFolder in ChildFolders.ToArray())
                {
                    if (childFolder.Id == splitPath[0])
                    {
                        matchedFolder = childFolder;
                        break;
                    }
                }

                if (matchedFolder == null)
                {
                    string subPath = string.IsNullOrWhiteSpace(Path) ? splitPath[0] : $"{Path}/{splitPath[0]}";
                    ChildFolders.Add(new NodeFolderTreeItem(subPath));
                }
            }
        }

        public void AddChild(string[] splitPath)
        {
            if (splitPath.Length > 1)
            {
                NodeFolderTreeItem matchedFolder = null;
                foreach (var childFolder in ChildFolders.ToArray())
                {
                    if (childFolder.Id == splitPath[0])
                    {
                        matchedFolder = childFolder;
                        break;
                    }
                }

                if (matchedFolder == null)
                {
                    string subPath = string.IsNullOrWhiteSpace(Path) ? splitPath[0] : $"{Path}/{splitPath[0]}";
                    matchedFolder = new NodeFolderTreeItem(subPath);
                    ChildFolders.Add(matchedFolder);
                }

                matchedFolder.AddChild(splitPath.SubArray(1, splitPath.Length - 1));
            }
            else if (splitPath.Length == 1)
            {
                string subPath = string.IsNullOrWhiteSpace(Path) ? splitPath[0] : $"{Path}/{splitPath[0]}";
                ChildFolders.Add(new NodeFolderTreeItem(subPath));
            }
        }
    }
}
