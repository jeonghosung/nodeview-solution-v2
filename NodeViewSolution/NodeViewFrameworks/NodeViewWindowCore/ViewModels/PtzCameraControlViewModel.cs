﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using LibVLCSharp.Shared;
using Microsoft.VisualBasic.Logging;
using NLog;
using NodeView.Extension.Plugins;
using NodeView.Net;
using NodeView.Tasks;
using NodeView.ViewModels;
using NodeViewCore.DataModels;
using NodeViewCore.DeviceControllers;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeViewWindowCore.ViewModels
{
    public class PtzCameraControlViewModel: BindableBase, IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IDialogService _dialogService = null;
        private LibVLC _libVlc;
        private string? _url = "";
        private bool _isTestMode = false;
        private bool _isVerbose = false;
        private ContentItem _content;
        private MediaPlayer _mediaPlayer;
        private string _streamingUri = "";
        private string _baseClientUri = "";
        private string _name = "";
        private string _id = "";
        private string _ptzType = "";
        private PtzPreset[] _ptzPresets;
        private PtzPreset _selectedPtzPreset;

        public ICameraClient CameraClientClient { get; set; }

        private DelegateCommand<string> _ptzControlCommand;
        private DelegateCommand<PtzPreset> _applyPresetCommand;
        private DelegateCommand<Window> _closeCommand;
        private ObservableCollection<string> _logList = new();
        private DelegateCommand _addPtzPresetCommand;
        private DelegateCommand _goHomeCommand;
        private DelegateCommand<string> _setFocusCommand;

        public string SelectedLog { get; set; }
        public MediaPlayer MediaPlayer
        {
            get => _mediaPlayer;
            set => SetProperty(ref _mediaPlayer, value);
        }

        public ObservableCollection<string> LogList
        {
            get => _logList;
            set => SetProperty(ref _logList, value);
        }

        public string StreamingUri
        {
            get => _streamingUri;
            set => SetProperty(ref _streamingUri, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public PtzPreset[] PtzPresets
        {
            get=> _ptzPresets;
            set => SetProperty(ref _ptzPresets, value);
        }

        public PtzPreset SelectedPtzPreset
        {
            get => _selectedPtzPreset;
            set => SetProperty(ref _selectedPtzPreset, value);
        }
        public DelegateCommand<string> PtzControlCommand => _ptzControlCommand ??= new DelegateCommand<string>(PtzControl);
        public DelegateCommand<PtzPreset> ApplyPresetCommand => _applyPresetCommand ??= new DelegateCommand<PtzPreset>(ApplyPreset);

        public DelegateCommand AddPtzPresetCommand => _addPtzPresetCommand ??= new DelegateCommand(ExecuteAddPtzPresetCommand);
        public DelegateCommand GoHomeCommand => _goHomeCommand ??= new DelegateCommand(ExecuteGoHomeCommand);
        public DelegateCommand<string> SetFocusCommand => _setFocusCommand ??= new DelegateCommand<string>(ExecuteSetFocusCommand);

       

        public DelegateCommand<Window> CloseCommand => _closeCommand ??= new DelegateCommand<Window>(ExecuteClose);


        public PtzCameraControlViewModel(ContentItem contentItem, bool isTestMode, bool isVerbose, IDialogService dialogService)
        {
            _dialogService = dialogService;
            _content = contentItem;
            _isTestMode = isTestMode;
            _isVerbose = isVerbose;
            Init();
            ThreadAction.PostOnUiThread(PlayContent);
        }

        private void Init()
        {
            _libVlc = new LibVLC(enableDebugLogs: true);
            _mediaPlayer = new MediaPlayer(_libVlc) { EnableHardwareDecoding = true, Mute = true, Fullscreen = true };
            _mediaPlayer.Playing += _mediaPlayer_Playing;
            _mediaPlayer.EncounteredError += _mediaPlayer_EncounteredError;
            _mediaPlayer.EndReached += _mediaPlayer_EndReached;
            _mediaPlayer.Stopped += _mediaPlayer_Stopped;
            _ptzType = _content.Properties.GetStringValue("ptztype", "");
            StreamingUri = _content.Uri;
            Name = _content.Name;
            Id= _content.Id;
            Match match = Regex.Match(_content.Uri, @"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            if (match.Success)
            {
                _baseClientUri = match.Value;
                if (_ptzType.Equals("hanwhaTechwin", StringComparison.OrdinalIgnoreCase))
                {
                    CameraClientClient = new HanwhaTechwinCameraClient(match.Value, _content.UserId, _content.UserPassword, _isTestMode, _isVerbose);
                }
                else if (_ptzType.Equals("truen", StringComparison.OrdinalIgnoreCase))
                {
                    CameraClientClient = new TruenCameraControlClient(match.Value, _content.UserId, _content.UserPassword,  _isTestMode, _isVerbose);
                }
                else
                {
                    Logger.Warn($"Not suppored PTZ type. type : {_ptzType}");
                }
            }
            PtzPresets = CameraClientClient.GetPresets();
            LogList.Add("Init.");
        }

        private void _mediaPlayer_Stopped(object? sender, EventArgs e)
        {
            ThreadAction.PostOnUiThread(() =>
            {
                {
                    LogList.Add("Player stop.");
                }
            });
        }

        private void _mediaPlayer_EndReached(object? sender, EventArgs e)
        {
            ThreadAction.PostOnUiThread(() =>
            {
                {
                    LogList.Add("endReached.");
                }
            });
        }

        private void _mediaPlayer_EncounteredError(object? sender, EventArgs e)
        {
            ThreadAction.PostOnUiThread(() =>
            {
                {
                    LogList.Add("encounterederror");
                }
            });
        }

        private void _mediaPlayer_Playing(object? sender, EventArgs e)
        {
            ThreadAction.PostOnUiThread(()=>
            {
                {
                    LogList.Add("play start.");
                }
            });
        }

        private void PlayContent()
        {
            if (string.IsNullOrWhiteSpace(_content?.Uri))
            {
                return;
            }
            string? userId = _content.UserId;
            string? userPassword = _content.UserPassword;
            if (!string.IsNullOrWhiteSpace(userId) && !string.IsNullOrWhiteSpace(userPassword))
            {
                _url = _content.Uri.Replace("://", $"://{userId}:{userPassword}@");
            }
            else
            {
                _url = _content.Uri;
            }

            using var _media = new Media(_libVlc, new Uri(_url));
            _mediaPlayer.Play(_media);
        }
        private void PtzControl(string controlType)
        {
            switch (controlType.ToLower())
            {
                case "zoomin":
                    CameraClientClient.ControlZoom(1);
                    break;
                case "zoomout":
                    CameraClientClient.ControlZoom(-1);
                    break;
            }
        }
        private void ApplyPreset(PtzPreset ptzPreset)
        {
            if (ptzPreset == null)
            {
                return;
            }
            CameraClientClient.ApplyPreset(ptzPreset.Id);
        }
        private void ExecuteAddPtzPresetCommand()
        {
            if (_ptzType.Equals("hanwhaTechwin", StringComparison.OrdinalIgnoreCase))
            {
                var parameters = new DialogParameters
                {
                    { "clientBaseUrl", $"{_baseClientUri}" }, { "id", _content.UserId }, { "password", _content.UserPassword }
                };
          
                _dialogService.ShowDialog("AddPtzPresetDialog", parameters, addPresetResult =>
                {
                });
            }
            else if (_ptzType.Equals("truen", StringComparison.OrdinalIgnoreCase))
            {

            }
        }
        private void ExecuteGoHomeCommand()
        {
            CameraClientClient.GoHome();
        }
        private void ExecuteSetFocusCommand(string type)
        {
            if (type.Equals("near", StringComparison.OrdinalIgnoreCase))
            {
                CameraClientClient.SetFocus(FocusType.NEAR);
            }
            else if(type.Equals("far", StringComparison.OrdinalIgnoreCase))
            {
                CameraClientClient.SetFocus(FocusType.FAR);
            }
            else if (type.Equals("auto", StringComparison.OrdinalIgnoreCase))
            {
                CameraClientClient.SetFocus(FocusType.AUTO);
            }
            else
            {
                Logger.Warn($"Not supported focustype. typy : {type}");
            }
        }
        private void ExecuteClose(Window window)
        {
            Dispose();
            window?.Close();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _libVlc?.Dispose();
                _mediaPlayer?.Dispose();
                CameraClientClient.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void MoveCamera(int x, int y)
        {
            CameraClientClient.ControlPanTilt(x, y);
        }
    }
}
