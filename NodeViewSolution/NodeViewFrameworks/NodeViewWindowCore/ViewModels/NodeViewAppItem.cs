﻿using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class NodeViewAppItem : BindableBase, INodeViewAppNode
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _id = "";
        private string _appKey = "";
        private string _name = "";
        private string _appType = "";
        private string _ip = "";
        private int _port = 0;
        private string _uri = "";
        private bool _isBlocked = false;
        private string _path = "";
        private NodeStatus _status = NodeStatus.None;
        private string _statusMessage = "";
        private string _updated = "";
        private int _processManagerPort = 20102;
        private ProcessRunningStatus _processRunningStatus;

        public string Id
        {
            get => _id;
            set
            {
                SetProperty(ref _id, value);
                RaisePropertyChanged("PathId");
            }
        }
        public string PathId => string.IsNullOrWhiteSpace(Path) ? Id : $"{Path}/{Id}";

        public string Path
        {
            get => _path;
            set
            {
                SetProperty(ref _path, value);
                RaisePropertyChanged("PathId");
            }
        }

        public NodeStatus Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }

        public string StatusMessage
        {
            get => _statusMessage;
            set => SetProperty(ref _statusMessage, value);
        }

        public string Updated
        {
            get => _updated;
            set => SetProperty(ref _updated, value);
        }

        public string AppKey
        {
            get => _appKey;
            set => SetProperty(ref _appKey, value);
        }
        public string AppType
        {
            get => _appType;
            set => SetProperty(ref _appType, value);
        }
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }
        public string Ip
        {
            get => _ip;
            set => SetProperty(ref _ip, value);
        }
        public int Port
        {
            get => _port;
            set => SetProperty(ref _port, value);
        }
        public string Uri
        {
            get => _uri;
            set => SetProperty(ref _uri, value);
        }
        public bool IsBlocked
        {
            get => _isBlocked;
            set => SetProperty(ref _isBlocked, value);
        }

        public int ProcessManagerPort
        {
            get => _processManagerPort;
            set => SetProperty(ref _processManagerPort, value);
        }

        public ProcessRunningStatus ProcessRunningStatus
        {
            get => _processRunningStatus;
            set => SetProperty(ref _processRunningStatus, value);
        }
        public NodeViewAppItem()
        {
        }

        public NodeViewAppItem(string id, string appKey, string appType, string name = "", string ip = "127.0.0.1", int port = 0, string uri = "") : this()
        {
            Id = id;
            AppKey = appKey;
            AppType = appType;
            Name = string.IsNullOrWhiteSpace(name) ? Id : name; ;
            Ip = ip;
            Port = port;
            Uri = string.IsNullOrWhiteSpace(uri) ? $"http://{Ip}:{Port}/api/v1" : uri;
        }

        private void LoadFrom(INodeViewAppNode source)
        {
            Id = source.Id;
            Path = source.Path;
            Status = source.Status;
            StatusMessage = source.StatusMessage;
            Updated = source.Updated;
            AppKey = source.AppKey;
            AppType = source.AppType;
            Name = source.Name;
            Ip = source.Ip;
            Port = source.Port;
            Uri = source.Uri;
            IsBlocked = source.IsBlocked;
            ProcessManagerPort = source.ProcessManagerPort;
        }
        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            if (string.IsNullOrWhiteSpace(Path))
            {
                json["path"] = Path;
            }
            if (Status != NodeStatus.None)
            {
                json["status"] = $"{Status}";
                json["statusMessage"] = StatusMessage;
            }

            if (string.IsNullOrWhiteSpace(Updated))
            {
                json["updated"] = Updated;
            }
            json["appKey"] = AppKey;
            json["appType"] = AppType;
            json["name"] = Name;
            json["ip"] = Ip;
            json["port"] = $"{Port}";
            json["uri"] = Uri;
            if (IsBlocked)
            {
                json["isBlocked"] = $"{IsBlocked}";
            }
            json["processManagerPort"] = $"{ProcessManagerPort}";
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", Id);

            Path = JsonUtils.GetValue(json, "path", Path);
            Status = JsonUtils.GetValue(json, "status", Status);
            StatusMessage = JsonUtils.GetValue(json, "statusMessage", StatusMessage);
            Updated = JsonUtils.GetValue(json, "updated", Updated);

            AppKey = JsonUtils.GetValue(json, "appKey", AppKey);
            AppType = JsonUtils.GetValue(json, "appType", AppType);
            Name = JsonUtils.GetValue(json, "name", Name);
            Ip = JsonUtils.GetValue(json, "ip", Ip);
            Port = JsonUtils.GetValue(json, "port", Port);
            Uri = JsonUtils.GetValue(json, "uri", Uri);
            IsBlocked = JsonUtils.GetValue(json, "isBlocked", IsBlocked);
            ProcessManagerPort = JsonUtils.GetValue(json, "processManagerPort", ProcessManagerPort);

            if (string.IsNullOrWhiteSpace(Name))
            {
                Name = Id;
            }
            if (string.IsNullOrWhiteSpace(Uri))
            {
                Uri = $"http://{Ip}:{Port}/api/v1";
            }

            return !(string.IsNullOrWhiteSpace(Id) || string.IsNullOrWhiteSpace(AppKey));
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "App" : tagName;
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            if (string.IsNullOrWhiteSpace(Path))
            {
                xmlWriter.WriteAttributeString("path", Path);
            }
            if (Status != NodeStatus.None)
            {
                xmlWriter.WriteAttributeString("status", $"{Status}");
                xmlWriter.WriteAttributeString("statusMessage", StatusMessage);
            }

            if (string.IsNullOrWhiteSpace(Updated))
            {
                xmlWriter.WriteAttributeString("updated", Updated);
            }
            xmlWriter.WriteAttributeString("appKey", AppKey);
            xmlWriter.WriteAttributeString("appType", AppType);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("ip", Ip);
            xmlWriter.WriteAttributeString("port", $"{Port}");
            xmlWriter.WriteAttributeString("uri", Uri);
            if (IsBlocked)
            {
                xmlWriter.WriteAttributeString("isBlocked", $"{IsBlocked}");
            }
            xmlWriter.WriteAttributeString("processManagerPort", $"{ProcessManagerPort}");

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", Id);

            Path = XmlUtils.GetAttrValue(xmlNode, "path", Path);
            Status = XmlUtils.GetAttrValue(xmlNode, "status", Status);
            StatusMessage = XmlUtils.GetAttrValue(xmlNode, "statusMessage", StatusMessage);
            Updated = XmlUtils.GetAttrValue(xmlNode, "updated", Updated);

            AppKey = XmlUtils.GetAttrValue(xmlNode, "appKey", AppKey);
            AppType = XmlUtils.GetAttrValue(xmlNode, "appType", AppType);
            Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
            Ip = XmlUtils.GetAttrValue(xmlNode, "ip", Ip);
            Port = XmlUtils.GetAttrValue(xmlNode, "port", Port);
            Uri = XmlUtils.GetAttrValue(xmlNode, "uri", Uri);
            IsBlocked = XmlUtils.GetAttrValue(xmlNode, "isBlocked", IsBlocked);
            ProcessManagerPort = XmlUtils.GetAttrValue(xmlNode, "processManagerPort", ProcessManagerPort);

            if (string.IsNullOrWhiteSpace(Name))
            {
                Name = Id;
            }
            if (string.IsNullOrWhiteSpace(Uri))
            {
                Uri = $"http://{Ip}:{Port}/api/v1";
            }

            return !(string.IsNullOrWhiteSpace(Id) || string.IsNullOrWhiteSpace(AppKey));
        }
        public static NodeViewAppItem CreateFrom(INodeViewAppNode source)
        {
            var node = new NodeViewAppItem();
            node.LoadFrom(source);
            return node;
        }

        public static NodeViewAppItem CreateFrom(JObject json)
        {
            var node = new NodeViewAppItem();
            node.LoadFrom(json);
            return node;
        }

        public static NodeViewAppItem CreateFrom(XmlNode xmlNode)
        {
            var node = new NodeViewAppItem();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}