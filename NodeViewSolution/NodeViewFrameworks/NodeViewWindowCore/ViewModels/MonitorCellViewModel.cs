﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Drawing;
using NodeView.Utils;

namespace NodeView.ViewModels
{
    public  class MonitorCellViewModel : SelectableViewModel
    {
        private string _id = Uuid.NewUuid;
        private int _seq;

        private IntRect _rect = IntRect.Empty;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public int Seq
        {
            get => _seq;
            set => SetProperty(ref _seq, value);
        }
        public IntRect BaseRect { get; set; }
        public IntRect Rect
        {
            get => _rect;
            set => SetProperty(ref _rect, value);
        }

        public MonitorCellViewModel(int seq, IntRect rect, IntRect baseRect = null)
        {
            Seq = seq;
            _rect = rect.Clone();
            if (baseRect == null)
            {
                BaseRect = rect.Clone();
            }
        }

        public void UpdateRectFromBaseRect(IntPoint offset, double scale)
        {
            Rect = new IntRect(offset.X + (int)(scale * BaseRect.X), offset.Y + (int)(scale * BaseRect.Y), (int)(scale * BaseRect.Width), (int)(scale * BaseRect.Height));
        }
    }
}
