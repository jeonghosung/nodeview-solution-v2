﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public abstract class TreeViewFolderBase : BindableBase, ITreeViewItem
    {
        protected string _id = "";
        protected string _path = "";
        protected bool _isSelected = false;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Path
        {
            get => _path;
            set => SetProperty(ref _path, value);
        }

        public string Name => string.IsNullOrWhiteSpace(Id) ? "컨텐츠" : Id;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public ObservableCollection<ITreeViewItem> ChildItems { get; } = new ObservableCollection<ITreeViewItem>();

        protected TreeViewFolderBase(string id)
        {
            Id = id;
            Path = "";
        }

        protected bool TryGetItem(string id, out ITreeViewItem item)
        {
            item = null;
            foreach (var childItem in ChildItems)
            {
                if (childItem.Id == id)
                {
                    item = childItem;
                    return true;
                }
            }

            return false;
        }
        
        public bool AddChildItem(ITreeViewItem item)
        {
            if (string.IsNullOrWhiteSpace(item?.Id)) return false;

            item.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";

            if (TryGetItem(item.Id, out var oldItem))
            {
                ChildItems.Remove(oldItem);
            }

            ChildItems.Add(item);
            return true;
        }

        public bool RemoveChildItem(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (TryGetItem(id, out var oldItem))
            {
                ChildItems.Remove(oldItem);
                return true;
            }

            return false;
        }
        
        public void ClearItems()
        {
            ChildItems.Clear();
        }
    }
}