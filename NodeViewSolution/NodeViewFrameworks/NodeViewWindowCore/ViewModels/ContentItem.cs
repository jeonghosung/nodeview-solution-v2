﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.DataWalls;

namespace NodeView.ViewModels
{
    public class ContentItem: DataWallContentBindableBase, ITreeViewItem
    {
        private bool _isSelected = false;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public bool CanPlay { get; set; } = true;
        public Visibility PlayVisibility => CanPlay ? Visibility.Visible : Visibility.Collapsed;

        public override bool SetProperty(IProperty property)
        {

            return _properties.Set(PropertyViewModel.CreateFrom(property));
        }

        public ObservableCollection<ITreeViewItem> ChildItems { get; } = new ObservableCollection<ITreeViewItem>();
        public override INode CreateNode()
        {
            return new ContentItem();
        }
        public static ContentItem CreateFrom(IDataWallContent sourceContent)
        {
            var content = new ContentItem();
            content.LoadFrom(sourceContent);
            return content;
        }

        public static ContentItem CreateFrom(INode sourceNode)
        {
            var content = new ContentItem();
            content.LoadFrom(sourceNode);
            return content;
        }

        public static ContentItem CreateFrom(JObject json)
        {
            var content = new ContentItem();
            content.LoadFrom(json);
            return content;
        }

        public static ContentItem CreateFrom(XmlNode xmlNode)
        {
            var content = new ContentItem();
            content.LoadFrom(xmlNode);
            return content;
        }
    }
}
