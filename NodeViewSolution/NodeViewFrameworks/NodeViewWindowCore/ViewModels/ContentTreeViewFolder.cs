﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class ContentTreeViewFolder : BindableBase, ITreeViewItem
    {
        private string _id = "";
        private string _path = "";
        private bool _isSelected = false;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Path
        {
            get => _path;
            set => SetProperty(ref _path, value);
        }

        public string Name => string.IsNullOrWhiteSpace(Id) ? "컨텐츠" : Id;
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public bool CanPlay { get; set; } = false;

        public Visibility PlayVisibility => CanPlay ? Visibility.Visible : Visibility.Collapsed;

        public ObservableCollection<ITreeViewItem> ChildItems { get; } = new ObservableCollection<ITreeViewItem>();

        public ContentTreeViewFolder(string id)
        {
            Id = id;
            Path = "";
        }

        private bool TryGetFolder(string id, out ContentTreeViewFolder folder)
        {
            folder = null;
            foreach (var childItem in ChildItems)
            {
                if (childItem is ContentTreeViewFolder childFolder)
                {
                    if (childFolder.Id == id)
                    {
                        folder = childFolder;
                        return true;
                    }
                }
            }

            return false;
        }

        private bool TryGetContent(string id, out ContentItem content)
        {
            content = null;
            foreach (var childItem in ChildItems)
            {
                if (childItem is ContentItem childContent)
                {
                    if (childContent.Id == id)
                    {
                        content = childContent;
                        return true;
                    }
                }
            }

            return false;
        }

        public bool AddChildItem(ITreeViewItem item)
        {
            if (string.IsNullOrWhiteSpace(item?.Id)) return false;

            if(string.IsNullOrWhiteSpace(item.Path))
            {
                item.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";
            }


            if (item is ContentTreeViewFolder)
            {
                if (TryGetFolder(item.Id, out var oldItem))
                {
                    ChildItems.Remove(oldItem);
                }
            }
            else if (item is ContentItem)
            {
                if (TryGetContent(item.Id, out var oldItem))
                {
                    ChildItems.Remove(oldItem);
                }
            }
            ChildItems.Add(item);
            return true;
        }

        public bool RemoveChildFolder(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (TryGetFolder(id, out var oldItem))
            {
                ChildItems.Remove(oldItem);
                return true;
            }

            return false;
        }

        public bool RemoveChildContent(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            if (TryGetContent(id, out var oldItem))
            {
                ChildItems.Remove(oldItem);
                return true;
            }

            return false;
        }

        public void ClearItems()
        {
            ChildItems.Clear();
        }

        public static ContentTreeViewFolder CreateFrom(IEnumerable<ContentItem> contentItems)
        {
            if (contentItems == null) return null;

            var rootFolder = new ContentTreeViewFolder("");
            foreach (var contentItem in contentItems)
            {
                string[] splitPath = StringUtils.Split(contentItem.Path, '/');
                ContentTreeViewFolder currentFolder = rootFolder;
                foreach (var pathName in splitPath)
                {
                    if (string.IsNullOrWhiteSpace(pathName))
                    {
                        continue;
                    }
                    if (currentFolder.TryGetFolder(pathName, out var foundFolder))
                    {
                        currentFolder = foundFolder;
                    }
                    else
                    {
                        var newFolder = new ContentTreeViewFolder(pathName);
                        currentFolder.AddChildItem(newFolder);
                        currentFolder = newFolder;
                    }
                }
                currentFolder.AddChildItem(contentItem);
            }

            return rootFolder;
        }

        protected void GetContentsInternal(List<IDataWallContent> list)
        {
            
            List<ContentTreeViewFolder> folders = new List<ContentTreeViewFolder>();
            foreach (var childItem in ChildItems)
            {
                if (childItem is ContentTreeViewFolder folder)
                {
                    folders.Add(folder);
                }
                else if (childItem is IDataWallContent content)
                {
                    list.Add(content);
                }
            }

            foreach (var folder in folders)
            {
                folder.GetContentsInternal(list);
            }
        }
        public IDataWallContent[] ToArray()
        {
            List<IDataWallContent> list = new List<IDataWallContent>();
            GetContentsInternal(list);
            return list.ToArray();
        }
    }
}