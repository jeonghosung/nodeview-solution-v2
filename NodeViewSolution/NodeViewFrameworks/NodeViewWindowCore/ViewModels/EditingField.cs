﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Tasks;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class EditingField : BindableBase
    {
        private string _id = "";
        private string _value = "";
        private string _defaultValue = "";
        private string _description = "";
        private string _valueType = "string";
        private string[] _items = new string[0];
        private readonly KeyValueCollectionBindable _attributes = new KeyValueCollectionBindable();
        private bool _isValid = true;
        private string _validMessage = "";
        private string _validationRule = "";
        private bool _isModified = false;

        public event EventHandler IsModifiedChanged;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Value
        {
            get => _value;
            set
            {
                string oldValue = _value;
                SetProperty(ref _value, value);
                if (oldValue != value)
                {
                    IsModified = _value != _defaultValue;
                    CheckValidation(value);
                }
            }
        }
        public string DefaultValue
        {
            get => _defaultValue;
            set => SetProperty(ref _defaultValue, value);
        }
        public string Description
        {
            get => 
            _description;
            set => SetProperty(ref _description, value);
        }
        public string ValueType
        {
            get => _valueType;
            set => SetProperty(ref _valueType, value);
        }

        public EditingFieldControlType ControlType { get; set; }
        public string[] Items
        {
            get => _items;
            set
            {
                SetProperty(ref _items, value);
                RaisePropertyChanged("ItemsString");
            }
        }

        public string ItemsString => string.Join(",", Items);

        public IKeyValueCollection Attributes => _attributes;

        public string ValidationRule
        {
            get => GetValidationRule();
            set => _validationRule = value;
        }

        public bool IsReadOnly
        {
            get => GetAttrValue("isReadOnly", false);
            set => SetAttribute("isReadOnly", value);
        } 
        public bool IsHidden => GetAttrValue("isHidden", false);
        public Visibility Visibility => IsHidden ? Visibility.Collapsed : Visibility.Visible;

        public bool IsModified
        {
            get => _isModified;
            set
            {
                if (_isModified != value)
                {
                    SetProperty(ref _isModified, value);
                    ThreadAction.PostOnUiThread(() =>
                    {
                        IsModifiedChanged?.Invoke(this, EventArgs.Empty);
                    });
                }
            }
        }

        public bool IsValid
        {
            get => _isValid;
            set => SetProperty(ref _isValid, value);
        }
        public string ValidMessage
        {
            get => _validMessage;
            set => SetProperty(ref _validMessage, value);
        }

        public EditingField(string id = "", string value = "", string description = "", string valueType = "", string items = "", EditingFieldControlType controlType = EditingFieldControlType.TextBox)
        {
            Id = id;
            DefaultValue = value;
            Value = value;
            Description = description;
            if (!string.IsNullOrWhiteSpace(valueType))
            {
                ValueType = valueType;
            }
            if (!string.IsNullOrWhiteSpace(items))
            {
                Items = StringUtils.Split(items, ',');
            }

            if (controlType == EditingFieldControlType.TextBox)
            {
                if (Items.Length > 0)
                {
                    controlType = EditingFieldControlType.DropDownList;
                }
            }

            ControlType = controlType;
        }

        private void CheckValidation(string value)
        {
            if (IsHidden || IsReadOnly) return;

            if (Items.Length == 0)
            {
                string validationRule = ValidationRule;
                if (string.IsNullOrWhiteSpace(validationRule))
                {
                    IsValid = true;
                    return;
                }

                IsValid = Regex.IsMatch(value, validationRule);
            }
            else
            {
                IsValid = Items.Contains(value);
            }
        }

        private string GetValidationRule()
        {
            if (Items.Length > 0) return "";
            
            if (!string.IsNullOrWhiteSpace(_validationRule))
            {
                return _validationRule;
            }

            switch (ValueType)
            {
                case "int":
                    return @"^[0-9]+$";
                case "double":
                    return @"^[0-9]+([.][0-9]+)?$";
                case "intPoint":
                case "intSize":
                    return @"^[0-9]+,[0-9]+$";
                case "intRect":
                    return @"^[0-9]+,[0-9]+,[0-9]+,[0-9]+$";
                case "id":
                    return @"^[A-Za-z][A-Za-z0-9_\-]*$";
                case "key":
                    return @"^[A-Za-z0-9_\-]+$";
            }
            return "";
        }

        public object this[string key]
        {
            get
            {
                if (string.IsNullOrWhiteSpace(key)) return "";
                switch (key)
                {
                    case "id":
                        return Id;
                    case "value":
                        return Value;
                    case "defaultValue":
                        return DefaultValue;
                    case "description":
                        return Description;
                    case "valueType":
                        return ValueType;
                    case "items":
                        return Items;
                }

                return Attributes[key];
            }
        }

        public T GetValue<T>(T defaultValue)
        {
            return StringUtils.GetValue(Value, defaultValue);
        }

        public string GetStringValue(string defaultValue = "")
        {
            return GetValue(defaultValue);
        }

        public T GetAttrValue<T>(string key, T defaultValue)
        {
            return StringUtils.GetValue(Attributes[key], defaultValue);
        }

        public virtual bool SetAttribute(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;
            switch (key)
            {
                case "id":
                    Id = $"{value}";
                    break;
                case "value":
                    Value = $"{value}";
                    break;
                case "defaultValue":
                    DefaultValue = $"{value}";
                    break;
                case "description":
                    Description = $"{value}";
                    break;
                case "valueType":
                    ValueType = $"{value}";
                    break;
                case "items":
                    Items = StringUtils.Split($"{value}", ',');
                    break;
                default:
                    _attributes.Set(key, value);
                    break;
            }

            return true;
        }

        public bool SetAttribute(IKeyValuePair keyValuePair)
        {
            if (string.IsNullOrWhiteSpace(keyValuePair?.Key)) return false;
            return SetAttribute(keyValuePair.Key, keyValuePair.Value);
        }

        public void SetAttributes(IEnumerable<IKeyValuePair> keyValuePairs)
        {
            if (keyValuePairs == null) return;
            foreach (var keyValuePair in keyValuePairs)
            {
                SetAttribute(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public bool RemoveAttribute(string key)
        {
            return _attributes.Remove(key);
        }

        public void ClearAttributes()
        {
            _attributes.Clear();
        }

        public bool LoadFrom(IProperty sourceProperty)
        {
            if (sourceProperty == null) return false;

            Id = sourceProperty.Id;
            Value = sourceProperty.Value;
            SetAttributes(sourceProperty.Attributes);

            return true;
        }
        
        public static EditingField CreateFrom(IProperty source)
        {
            var field = new EditingField();
            field.LoadFrom(source);
            if (string.IsNullOrWhiteSpace(field.DefaultValue)) field.DefaultValue = field.Value;
            return field;
        }
    }

    public enum EditingFieldControlType
    {
        TextBox,
        PasswordBox,
        DropDownList,
        ComboBox
    }
}
