﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class NodeViewActionViewModel : BindableBase
    {
        private readonly ObservableCollection<NodeViewCommandBindable> _commands = new ObservableCollection<NodeViewCommandBindable>();
        private string _id;
        private string _description;
        private bool _isSelected;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }
        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }
        public ObservableCollection<NodeViewCommandBindable> Commands => _commands;

        public NodeViewActionViewModel(string id = "", string description = "", IEnumerable<NodeViewCommandBindable> commands = null)
        {
            Id = id;
            Description = description;
            if (commands != null)
            {
                foreach (var command in commands)
                {
                    _commands.Add(command);
                }
            }
        }
        public void Update(NodeViewActionViewModel action)
        {
            Description = action.Description;
            _commands.Clear();
            if (action.Commands != null)
            {
                foreach (var command in action.Commands)
                {
                    _commands.Add(command);
                }
            }
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["description"] = Description;
            var commandArray = new JArray();
            foreach (var command in _commands.ToArray())
            {
                commandArray.Add(command.ToJson());
            }
            json["commands"] = commandArray;
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", Id);
            Description = JsonUtils.GetValue(json, "description", Description);
            var commandArray = JsonUtils.GetValue(json, "commands", new JArray());
            foreach (var commandToken in commandArray)
            {
                var command = NodeViewCommandBindable.CreateFrom(commandToken.Value<JObject>());
                if (command != null)
                {
                    _commands.Add(command);
                }
            }

            return !string.IsNullOrWhiteSpace(Id);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Action" : tagName;
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("description", Description);

            foreach (var command in _commands.ToArray())
            {
                command.WriteXml(xmlWriter, "Command");
            }

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", Id);
            Description = XmlUtils.GetAttrValue(xmlNode, "description", Description);

            XmlNodeList commandNodeList = xmlNode.SelectNodes("Command");
            if (commandNodeList != null)
            {
                foreach (XmlNode commandNode in commandNodeList)
                {
                    var command = NodeViewCommandBindable.CreateFrom(commandNode);
                    if (command != null)
                    {
                        _commands.Add(command);
                    }
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }
        private bool LoadFrom(NodeViewActionViewModel source)
        {
            if (source == null) return false;

            Id = source.Id;
            Description = source.Description;

            foreach (var sourceCommand in source.Commands)
            {
                var command = NodeViewCommandBindable.CreateFrom(sourceCommand);
                if (command != null)
                {
                    _commands.Add(command);
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }
        public static NodeViewActionViewModel CreateFrom(JObject json)
        {
            if (json == null) return null;
            var action = new NodeViewActionViewModel();
            if (action.LoadFrom(json))
            {
                return action;
            }

            return null;
        }
        public static NodeViewActionViewModel CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return null;
            var action = new NodeViewActionViewModel();
            if (action.LoadFrom(xmlNode))
            {
                return action;
            }

            return null;
        }
        public static NodeViewActionViewModel CreateFrom(NodeViewActionViewModel source)
        {
            if (source == null) return null;
            var action = new NodeViewActionViewModel();
            if (action.LoadFrom(source))
            {
                return action;
            }

            return null;
        }
    }
}
