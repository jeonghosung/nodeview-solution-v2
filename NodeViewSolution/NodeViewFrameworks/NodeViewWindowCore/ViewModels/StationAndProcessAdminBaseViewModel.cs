﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Protocols.NodeViews;
using NodeView.Tasks;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeView.ViewModels
{
    public class StationAndProcessAdminBaseViewModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IDialogService _dialogService;
        protected NodeViewApiClient _nodeViewApiClient = null;
        protected NodeViewProcessManagerClient _processManagerClient = null;

        public string _appKey = "";
        private ProcessNode _appProcessNode = null;
        private string _selectedNodePath;
        private DeviceViewModel _selectedDevice;
        private NodeViewActionViewModel _selectedAction;
        private ConfigurationEditSectionModel _configuration;
        private ConfigurationEditSectionModel _selectedConfiguration;
        private string _selectedLog = "";

        private Visibility _duplicateSelectedActionVisibility = Visibility.Collapsed;
        private Visibility _editSelectedActionVisibility = Visibility.Collapsed;
        private Visibility _deleteSelectedActionVisibility = Visibility.Collapsed;

        
        private DelegateCommand _reloadCommand;

        private DelegateCommand<string> _selectNodeFolderItemCommand;
        
        private DelegateCommand<string> _selectDeviceCommand;
        private DelegateCommand<string> _runSelectedDeviceCommandCommand;
        
        private DelegateCommand<string> _selectActionCommand;
        private DelegateCommand _createNewActionCommand;
        private DelegateCommand _duplicateSelectedActionCommand;
        private DelegateCommand _editSelectedActionCommand;
        private DelegateCommand _deleteSelectedActionCommand;
        
        private DelegateCommand _createNewEventActionCommand;
        private DelegateCommand<string> _duplicateSelectedEventActionCommand;
        private DelegateCommand<string> _editSelectedEventActionCommand;
        private DelegateCommand<string> _deleteSelectedEventActionCommand;

        private DelegateCommand<ConfigurationEditSectionModel> _selectConfigTreeItemCommand;
        private DelegateCommand _saveConfigCommand;
        private DelegateCommand<string> _processControlCommand;

        public DelegateCommand ReloadCommand => _reloadCommand ?? (_reloadCommand = new DelegateCommand(ExecuteReloadCommand));

        public DelegateCommand<string> SelectNodeFolderItemCommand => _selectNodeFolderItemCommand ?? (_selectNodeFolderItemCommand = new DelegateCommand<string>(ExecuteSelectNodeFolderItemCommand));
        
        public DelegateCommand<string> SelectDeviceCommand => _selectDeviceCommand ?? (_selectDeviceCommand = new DelegateCommand<string>(ExecuteSelectDeviceCommand));
        public DelegateCommand<string> RunSelectedDeviceCommandCommand => _runSelectedDeviceCommandCommand ?? (_runSelectedDeviceCommandCommand = new DelegateCommand<string>(ExecuteRunSelectedDeviceCommandCommand));
        public DelegateCommand<string> SelectActionCommand => _selectActionCommand ?? (_selectActionCommand = new DelegateCommand<string>(ExecuteSelectActionCommand));
        
        public DelegateCommand CreateNewActionCommand => _createNewActionCommand ?? (_createNewActionCommand = new DelegateCommand(ExecuteCreateNewActionCommand));
        public DelegateCommand DuplicateSelectedActionCommand => _duplicateSelectedActionCommand ?? (_duplicateSelectedActionCommand = new DelegateCommand(ExecuteDuplicateSelectedActionCommand));
        public DelegateCommand EditSelectedActionCommand => _editSelectedActionCommand ?? (_editSelectedActionCommand = new DelegateCommand(ExecuteEditSelectedActionCommand));
        public DelegateCommand DeleteSelectedActionCommand => _deleteSelectedActionCommand ?? (_deleteSelectedActionCommand = new DelegateCommand(ExecuteDeleteSelectedActionCommand));

        public DelegateCommand CreateNewEventActionCommand => _createNewEventActionCommand ?? (_createNewEventActionCommand = new DelegateCommand(ExecuteCreateNewEventActionCommand));
        public DelegateCommand<string> DuplicateSelectedEventActionCommand => _duplicateSelectedEventActionCommand ?? (_duplicateSelectedEventActionCommand = new DelegateCommand<string>(ExecuteDuplicateSelectedEventActionCommand));
        public DelegateCommand<string> EditSelectedEventActionCommand => _editSelectedEventActionCommand ?? (_editSelectedEventActionCommand = new DelegateCommand<string>(ExecuteEditSelectedEventActionCommand));
        public DelegateCommand<string> DeleteSelectedEventActionCommand => _deleteSelectedEventActionCommand ?? (_deleteSelectedEventActionCommand = new DelegateCommand<string>(ExecuteDeleteSelectedEventActionCommand));

        public DelegateCommand<ConfigurationEditSectionModel> SelectConfigTreeItemCommand => _selectConfigTreeItemCommand ?? (_selectConfigTreeItemCommand = new DelegateCommand<ConfigurationEditSectionModel>(ExecuteSelectConfigTreeItemCommand));
        public DelegateCommand SaveConfigCommand => _saveConfigCommand ?? (_saveConfigCommand = new DelegateCommand(ExecuteSaveConfigCommand));
        public DelegateCommand<string> ProcessControlCommand => _processControlCommand ?? (_processControlCommand = new DelegateCommand<string>(ExecuteProcessControlCommand));

        public ObservableCollection<NodeFolderTreeItem> NodeFolderItems { get; } = new ObservableCollection<NodeFolderTreeItem>();
        public ObservableCollection<INode> Nodes { get; } = new ObservableCollection<INode>();
        public ObservableCollection<DeviceViewModel> Devices { get; } = new ObservableCollection<DeviceViewModel>();
        public ObservableCollection<NodeViewActionViewModel> Actions { get; } = new ObservableCollection<NodeViewActionViewModel>();
        public ObservableCollection<EventActionViewModel> EventActions { get; } = new ObservableCollection<EventActionViewModel>();
        public ObservableCollection<string> LogFiles { get; } = new ObservableCollection<string>();
        public ProcessNode AppProcessNode
        {
            get => _appProcessNode;
            set
            {
                SetProperty(ref _appProcessNode, value);
                RaisePropertyChanged("ProcessControlVisibility");
            }
        }

        public string SelectedNodePath
        {
            get => _selectedNodePath;
            set => SetProperty(ref _selectedNodePath, value);
        }

        public DeviceViewModel SelectedDevice
        {
            get => _selectedDevice;
            set
            {
                if (_selectedDevice == value) return;

                if (_selectedDevice != null) _selectedDevice.IsSelected = false;
                if (value != null) value.IsSelected = true;
                SetProperty(ref _selectedDevice, value);
            }
        }
        public NodeViewActionViewModel SelectedAction
        {
            get => _selectedAction;
            set
            {
                if (_selectedAction == value) return;

                if (_selectedAction != null) _selectedAction.IsSelected = false;
                if (value != null) value.IsSelected = true;
                SetProperty(ref _selectedAction, value);
                UpdateSelectedActionVisibilities();
            }
        }

        public ConfigurationEditSectionModel AppConfiguration
        {
            get => _configuration;
            set => SetProperty(ref _configuration, value);
        }
        public ConfigurationEditSectionModel SelectedConfiguration
        {
            get => _selectedConfiguration;
            set => SetProperty(ref _selectedConfiguration, value);
        }

        public string SelectedLog
        {
            get => _selectedLog;
            set => SetProperty(ref _selectedLog, value);
        }
        public Visibility ProcessControlVisibility => (_appProcessNode == null) ? Visibility.Collapsed : Visibility.Visible;
        public Visibility DuplicateSelectedActionVisibility
        {
            get => _duplicateSelectedActionVisibility;
            set => SetProperty(ref _duplicateSelectedActionVisibility, value);
        }
        public Visibility EditSelectedActionVisibility
        {
            get => _editSelectedActionVisibility;
            set => SetProperty(ref _editSelectedActionVisibility, value);
        }
        public Visibility DeleteSelectedActionVisibility
        {
            get => _deleteSelectedActionVisibility;
            set => SetProperty(ref _deleteSelectedActionVisibility, value);
        }
        private void UpdateSelectedActionVisibilities()
        {
            if (SelectedAction == null)
            {
                DuplicateSelectedActionVisibility = Visibility.Collapsed;
                EditSelectedActionVisibility = Visibility.Collapsed;
                DeleteSelectedActionVisibility = Visibility.Collapsed;
            }
            else
            {
                DuplicateSelectedActionVisibility = Visibility.Visible;
                EditSelectedActionVisibility = Visibility.Visible;
                DeleteSelectedActionVisibility = Visibility.Visible;
            }
        }

        public StationAndProcessAdminBaseViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
        }

        protected void InitStation(IConfiguration appNodeConfig)
        {
            if(appNodeConfig == null) return;

            _appKey = appNodeConfig.GetValue("@appKey", _appKey);
            SetAppApiClient(appNodeConfig);
            SetProcessManagerClient(appNodeConfig);
        }

        private bool SetAppApiClient(IConfiguration appNodeConfig)
        {
            if (appNodeConfig == null) return false;

            try
            {
                string appIp = appNodeConfig.GetValue("@ip", "");
                int appPort = appNodeConfig.GetValue("@port", 0);
                if (string.IsNullOrWhiteSpace(appIp) || appPort <= 0)
                {
                    return false;
                }
                string appUri = appNodeConfig.GetValue("@uri", "");
                if (string.IsNullOrWhiteSpace(appUri))
                {
                    appUri = $"http://{appIp}:{appPort}/api/v1";
                }
                _nodeViewApiClient = new NodeViewApiClient(appUri);
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetDataWallApiClient:");
                _nodeViewApiClient = null;
            }

            return _nodeViewApiClient != null;
        }
        private bool SetProcessManagerClient(IConfiguration appNodeConfig)
        {
            if (appNodeConfig == null) return false;

            try
            {
                string appIp = appNodeConfig.GetValue("@ip", "");
                int processManagerPort = appNodeConfig.GetValue("@processManagerPort", 0);
                if (string.IsNullOrWhiteSpace(appIp) || processManagerPort <= 0)
                {
                    return false;
                }
                _processManagerClient = new NodeViewProcessManagerClient(appIp, processManagerPort);
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetProcessManagerClient:");
                _processManagerClient = null;
            }

            return _processManagerClient != null;
        }

        private void ExecuteReloadCommand() => Reload();
        private void ExecuteSelectNodeFolderItemCommand(string path)
        {
            UpdateNodesFromStation(path);
        }
        private void ExecuteSelectDeviceCommand(string deviceId)
        {
            try
            {
                if (SelectedDevice.Id != deviceId)
                {
                    var device = FindDevice(deviceId);
                    if (device != null)
                    {
                        SelectedDevice = device;
                    }

                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSelectDeviceCommand:");
            }
        }
        private void ExecuteRunSelectedDeviceCommandCommand(string command)
        {
            try
            {
                string uri = _nodeViewApiClient.BaseUrl;

                var device = SelectedDevice;
                if (device == null) return;
                var commandDescription = device.FindCommandDescription(command);
                if (commandDescription == null) return;

                var parameters = new DialogParameters();
                parameters.Add("appUri", uri);
                parameters.Add("device", device);
                parameters.Add("commandDescription", commandDescription);
                _dialogService.ShowDialog("DeviceCommandRunDialog", parameters, r =>
                {
                    //skip
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteRunSelectDeviceCommandCommand:");
            }
        }
        private void ExecuteSelectActionCommand(string actionId)
        {
            var action = FindAction(actionId);
            SelectedAction = action;
        }

        private void ExecuteCreateNewActionCommand()
        {
            try
            {
                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("apiClient", _nodeViewApiClient);
                    parameters.Add("isCreateNew", true);
                    parameters.Add("devices", Devices.ToArray());
                    _dialogService.ShowDialog("EditAppActionDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var retAction = r.Parameters.GetValue<NodeViewActionViewModel>("action");
                            Actions.Add(retAction);
                            SelectedAction = retAction;
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteCreateNewActionCommand:");
            }
        }
        private void ExecuteDuplicateSelectedActionCommand()
        {
            try
            {
                var selectedAction = SelectedAction;
                if (selectedAction == null) return;

                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("apiClient", _nodeViewApiClient);
                    parameters.Add("isCreateNew", true);
                    parameters.Add("devices", Devices.ToArray());
                    parameters.Add("action", selectedAction);
                    _dialogService.ShowDialog("EditAppActionDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var retAction = r.Parameters.GetValue<NodeViewActionViewModel>("action");
                            Actions.Add(retAction);
                            SelectedAction = retAction;
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDuplicateSelectedActionCommand:");
            }
        }
        private void ExecuteEditSelectedActionCommand()
        {
            try
            {
                var selectedAction = SelectedAction;
                if (selectedAction == null) return;

                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("apiClient", _nodeViewApiClient);
                    parameters.Add("isCreateNew", false);
                    parameters.Add("devices", Devices.ToArray());
                    parameters.Add("action", selectedAction);
                    _dialogService.ShowDialog("EditAppActionDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var retAction = r.Parameters.GetValue<NodeViewActionViewModel>("action");
                            selectedAction.Update(retAction);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteEditSelectedActionCommand:");
            }
        }
        private void ExecuteDeleteSelectedActionCommand()
        {
            try
            {
                var selectedAction = SelectedAction;
                if (selectedAction == null) return;

                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("title", "삭제 확인");
                    parameters.Add("message", $"{selectedAction.Id}을 삭제 하시겠습니까?");
                    _dialogService.ShowDialog("warningConfirm", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var request = new JObject()
                            {
                                ["action"] = "delete",
                                ["param"] = new JObject()
                                {
                                    ["ids"] = new JArray()
                                    {
                                        selectedAction.Id
                                    }
                                }
                            };
                            var response = _nodeViewApiClient.Put("/actions", request);
                            if (response.IsSuccess)
                            {
                                Actions.Remove(selectedAction);
                                SelectedAction = Actions.Count > 0 ? Actions[0] : null;
                                UpdateActionsFromStation();
                            }
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDeleteSelectedActionCommand:");
            }
        }
        private void ExecuteCreateNewEventActionCommand()
        {
            try
            {
                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("apiClient", _nodeViewApiClient);
                    parameters.Add("isCreateNew", true);
                    parameters.Add("actions", Actions.ToArray());
                    _dialogService.ShowDialog("EditEventActionDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var retEventAction = r.Parameters.GetValue<EventActionViewModel>("eventAction");
                            EventActions.Add(retEventAction);
                            //SelectedEventAction = retEventAction;
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteCreateNewEventActionCommand:");
            }
        }
        private void ExecuteDuplicateSelectedEventActionCommand(string id)
        {
            try
            {
                var eventAction = FindEventAction(id);
                if (eventAction == null) return;

                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("apiClient", _nodeViewApiClient);
                    parameters.Add("isCreateNew", true);
                    parameters.Add("actions", Actions.ToArray());
                    parameters.Add("eventAction", eventAction);
                    _dialogService.ShowDialog("EditEventActionDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var retEventAction = r.Parameters.GetValue<EventActionViewModel>("eventAction");
                            EventActions.Add(retEventAction);
                            //SelectedEventAction = retEventAction;
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDuplicateSelectedEventActionCommand:");
            }
        }
        private void ExecuteEditSelectedEventActionCommand(string id)
        {
            try
            {
                var eventAction = FindEventAction(id);
                if (eventAction == null) return;

                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("apiClient", _nodeViewApiClient);
                    parameters.Add("isCreateNew", false);
                    parameters.Add("actions", Actions.ToArray());
                    parameters.Add("eventAction", eventAction);
                    _dialogService.ShowDialog("EditEventActionDialog", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var retEventAction = r.Parameters.GetValue<EventActionViewModel>("eventAction");
                            eventAction.Update(retEventAction);
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteEditSelectedEventActionCommand:");
            }
        }
        private void ExecuteDeleteSelectedEventActionCommand(string id)
        {
            try
            {
                var eventAction = FindEventAction(id);
                if (eventAction == null) return;

                if (CheckTocken())
                {
                    var parameters = new DialogParameters();
                    parameters.Add("title", "삭제 확인");
                    parameters.Add("message", $"{eventAction.Id}을 삭제 하시겠습니까?");
                    _dialogService.ShowDialog("warningConfirm", parameters, r =>
                    {
                        if (r.Result == ButtonResult.OK)
                        {
                            var request = new JObject()
                            {
                                ["action"] = "delete",
                                ["param"] = new JObject()
                                {
                                    ["ids"] = new JArray()
                                    {
                                        eventAction.Id
                                    }
                                }
                            };
                            var response = _nodeViewApiClient.Put("/events", request);
                            if (response.IsSuccess)
                            {
                                EventActions.Remove(eventAction);
                                UpdateEventActionsFromStation();
                            }
                        }
                    });
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteDeleteSelectedEventActionCommand:");
            }
        }

        private void ExecuteSelectConfigTreeItemCommand(ConfigurationEditSectionModel selectedItem)
        {
  //          SelectedConfiguration.IsSelected = false;
            SelectedConfiguration = selectedItem;
        }

        private void ExecuteSaveConfigCommand()
        {
            if (CheckTocken())
            {
                var config = Configuration.CreateFrom(AppConfiguration);
                var response = _processManagerClient.SetConfig(_appKey, config);
            }
        }
        void ExecuteProcessControlCommand(string command)
        {
            if (string.IsNullOrWhiteSpace(AppProcessNode?.Id)) return;

            if (command.Equals("start", StringComparison.OrdinalIgnoreCase))
            {
                _processManagerClient.StartApp(AppProcessNode.Id);
            }
            else if (command.Equals("stop", StringComparison.OrdinalIgnoreCase))
            {
                _processManagerClient.StopApp(AppProcessNode.Id);
            }
            else if (command.Equals("restart", StringComparison.OrdinalIgnoreCase))
            {
                _processManagerClient.RestartApp(AppProcessNode.Id);
            }
            AppStatusAndConfigurationFromProcessManagerUpdate();
        }
        public DelegateCommand<string> SelectedLogFileCommand
        {
            get
            {
                return new DelegateCommand<string>(args =>
                {
                    SelectedLog  = _processManagerClient.GetLog(AppProcessNode.Id, args);
                });
            }
        }
        public virtual void Reload()
        {
            Task.Factory.StartNew(AppStatusAndConfigurationFromProcessManagerUpdate);
            UpdateNodesFromStation();
            UpdateDevicesFromStation();
            UpdateActionsFromStation();
            UpdateEventActionsFromStation();
            Task.Factory.StartNew(UpdateLogFromProcessManager);
        }

        private void AppStatusAndConfigurationFromProcessManagerUpdate()
        {
            if (_processManagerClient == null || string.IsNullOrWhiteSpace(_appKey)) return;

            try
            {
                AppProcessNode = _processManagerClient.GetApp(_appKey);
                AppConfiguration = ConfigurationEditSectionModel.CreateFrom(_processManagerClient.GetConfig(_appKey));
                if (AppConfiguration.ChildItems.Any(config =>
                    config.GetType() == typeof(ConfigurationEditSectionModel)))
                {
                    SelectedConfiguration = (ConfigurationEditSectionModel)AppConfiguration.ChildItems.First(config => config.GetType() == typeof(ConfigurationEditSectionModel));
                }
                else
                {
                    SelectedConfiguration = AppConfiguration;
                }

                SelectedConfiguration.IsSelected = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "AppStatusAndConfigurationFromProcessManagerUpdate:");
            }
        }

        protected void UpdateNodesFromStation()
        {
            Task.Factory.StartNew(() => UpdateNodesFromStation(""));

            List<string> pathList = new List<string>();
            try
            {
                if (_nodeViewApiClient != null)
                {
                    var response = _nodeViewApiClient.Get("/folders/node");
                    if (response.IsSuccess)
                    {
                        foreach (var pathToken in response.ResponseJsonArray)
                        {
                            string path = pathToken.Value<string>();
                            if (path != null) pathList.Add(path);
                        }
                        string[] paths = pathList.ToArray();
                        ThreadAction.PostOnUiThread(() => UpdateNodeFoldersOnUi(paths));
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateNodesFromDataWall:");
            }

        }
        protected void UpdateNodeFoldersOnUi(string[] paths)
        {
            NodeFolderItems.Clear();
            NodeFolderTreeItem rooTreeItem = new NodeFolderTreeItem("");
            rooTreeItem.Id = "Nodes";
            NodeFolderItems.Add(rooTreeItem);

            if (paths != null)
            {
                foreach (var path in paths)
                {
                    rooTreeItem.AddChild(path);
                }
            }
        }

        protected void UpdateNodesFromStation(string path)
        {
            List<INode> nodeList = new List<INode>();
            try
            {
                path = string.IsNullOrWhiteSpace(path) ? "": path.Trim('/');
                SelectedNodePath = "/" + path;
                string requestUrl = string.IsNullOrWhiteSpace(path) ? "/nodes" : "/nodes/" + path;
                if (_nodeViewApiClient != null)
                {
                    var response = _nodeViewApiClient.Get(requestUrl);
                    if (response.IsSuccess)
                    {
                        foreach (var nodeToken in response.ResponseJsonArray)
                        {
                            var node = NodeBindable.CreateFrom(nodeToken.Value<JObject>());
                            if (node != null)
                            {
                                nodeList.Add(node);
                            }
                        }
                        INode[] nodes = nodeList.ToArray();
                        ThreadAction.PostOnUiThread(() => UpdateNodesOnUi(nodes));
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetNodesFromDataWall:");
            }
        }
        protected void UpdateNodesOnUi(INode[] nodes)
        {
            Nodes.Clear();
            if (nodes != null)
            {
                foreach (var node in nodes)
                {
                    Nodes.Add(node);
                }
            }
        }

        protected void UpdateDevicesFromStation()
        {
            List<DeviceViewModel> deviceList = new List<DeviceViewModel>();
            if (_nodeViewApiClient != null)
            {
                var response = _nodeViewApiClient.Get("/devices");
                if (response.IsSuccess)
                {
                    foreach (var deviceToken in response.ResponseJsonArray)
                    {
                        var device = DeviceViewModel.CreateFrom(deviceToken.Value<JObject>());
                        if (device != null)
                        {
                            deviceList.Add(device);
                        }
                    }
                    DeviceViewModel[] devices = deviceList.ToArray();
                    ThreadAction.PostOnUiThread(() => UpdateDevicesOnUi(devices));
                }
            }
        }

        protected void UpdateDevicesOnUi(DeviceViewModel[] devices)
        {
            Devices.Clear();
            if (devices != null)
            {
                foreach (var device in devices)
                {
                    Devices.Add(device);
                }
            }
            SelectedDevice = Devices.Count > 0 ? Devices[0] : null;
        }

        protected void UpdateActionsFromStation()
        {
            List<NodeViewActionViewModel> actionList = new List<NodeViewActionViewModel>();
            if (_nodeViewApiClient != null)
            {
                var response = _nodeViewApiClient.Get("/actions");
                if (response.IsSuccess)
                {
                    foreach (var actionToken in response.ResponseJsonArray)
                    {
                        var action = NodeViewActionViewModel.CreateFrom(actionToken.Value<JObject>());
                        if (action != null)
                        {
                            actionList.Add(action);
                        }
                    }
                    NodeViewActionViewModel[] actions = actionList.ToArray();
                    ThreadAction.PostOnUiThread(() => UpdateActionsOnUi(actions));
                }
            }
        }
        protected void UpdateActionsOnUi(NodeViewActionViewModel[] actions)
        {
            Actions.Clear();
            if (actions != null)
            {
                foreach (var action in actions)
                {
                    Actions.Add(action);
                }
            }
            SelectedAction = Actions.Count > 0 ? Actions[0] : null;
        }

        protected void UpdateEventActionsFromStation()
        {
            List<EventActionViewModel> eventActionList = new List<EventActionViewModel>();
            if (_nodeViewApiClient != null)
            {
                var response = _nodeViewApiClient.Get("/events");
                if (response.IsSuccess)
                {
                    foreach (var eventActionToken in response.ResponseJsonArray)
                    {
                        var eventAction = EventActionViewModel.CreateFrom(eventActionToken.Value<JObject>());
                        if (eventAction != null)
                        {
                            eventActionList.Add(eventAction);
                        }
                    }
                    EventActionViewModel[] eventActions = eventActionList.ToArray();
                    ThreadAction.PostOnUiThread(() => UpdateEventActionsOnUi(eventActions));
                }
            }
        }
        protected void UpdateEventActionsOnUi(EventActionViewModel[] eventActions)
        {
            EventActions.Clear();
            if (eventActions != null)
            {
                foreach (var eventAction in eventActions)
                {
                    EventActions.Add(eventAction);
                }
            }
        }


        protected void UpdateLogFromProcessManager()
        {
            if (_processManagerClient != null)
            {
                if (AppProcessNode == null)
                {
                    return;
                }
                string[] logFiles = _processManagerClient.GetLogFileNames(AppProcessNode.Id);
                ThreadAction.PostOnUiThread(() => UpdateLogOnUi(logFiles));
            }
        }

        protected void UpdateLogOnUi(string[] logFiles)
        {
            LogFiles.Clear();
            if (logFiles != null)
            {
                foreach (var logFile in logFiles)
                {
                    LogFiles.Add(logFile);
                }
            }
        }

        private DeviceViewModel FindDevice(string deviceId)
        {
            if (string.IsNullOrWhiteSpace(deviceId)) return null;

            foreach (var device in Devices.ToArray())
            {
                if (device.Id == deviceId)
                {
                    return device;
                }
            }

            return null;
        }
        private NodeViewActionViewModel FindAction(string id)
        {
            foreach (var action in Actions.ToArray())
            {
                if (action.Id == id)
                {
                    return action;
                }
            }

            return null;
        }
        private EventActionViewModel FindEventAction(string id)
        {
            foreach (var eventAction in EventActions.ToArray())
            {
                if (eventAction.Id == id)
                {
                    return eventAction;
                }
            }
            return null;
        }
        private bool CheckTocken()
        {
            bool result = false;
            if (!_nodeViewApiClient.HasAccessToken)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Add("apiClient", _nodeViewApiClient);
                _dialogService.ShowDialog("LoginDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        result = true;
                    }
                });
            }
            else
            {
                result = true;
            }
            return result;
        }
    }
}
