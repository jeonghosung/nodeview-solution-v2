﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;

namespace NodeView.ViewModels
{
    public interface IConfigurationEditModel
    {
    }

    public class ConfigurationEditSectionModel : TreeNodeBindableBase, IConfiguration, IConfigurationEditModel
    {
        private bool _isSelected;
        private ObservableCollection<IConfigurationEditModel> _childItems = new ObservableCollection<IConfigurationEditModel>();

        public bool IsSelected
        {
            get => _isSelected;
            set => SetProperty(ref _isSelected, value);
        }

        public ObservableCollection<IConfigurationEditModel> ChildItems
        {
            get
            {
                //Todo:이건 제대로 수정 해야 함
                if (_childItems.Count == 0)
                {
                    foreach (var property in Properties)
                    {
                        if (property is PropertyViewModel editPropertyModel)
                        {
                            _childItems.Add(editPropertyModel);
                        }
                    }
                    foreach (var childNode in ChildNodes)
                    {
                        if (childNode is ConfigurationEditSectionModel editSectionModel)
                        {
                            _childItems.Add(editSectionModel);
                        }
                    }
                }

                return _childItems;
            }
        }

        public override INode CreateNode()
        {
            return new ConfigurationEditSectionModel();
        }

        public override bool SetProperty(IProperty property)
        {

            return _properties.Set(PropertyViewModel.CreateFrom(property));
        }

        public static ConfigurationEditSectionModel CreateFrom(JObject json)
        {
            var configuration = new ConfigurationEditSectionModel();
            configuration.LoadFrom(json);
            return configuration;
        }

        public static ConfigurationEditSectionModel CreateFrom(XmlNode xmlNode)
        {
            var configuration = new ConfigurationEditSectionModel();
            configuration.LoadFrom(xmlNode);
            return configuration;
        }

        public static ConfigurationEditSectionModel CreateFrom(IConfiguration source)
        {
            var configuration = new ConfigurationEditSectionModel();
            configuration.LoadFrom(source);
            return configuration;
        }
    }
}
