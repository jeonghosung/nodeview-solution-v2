﻿using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.Utils;
using Prism.Mvvm;
using System.Xml;

namespace NodeView.ViewModels
{
    public class NodeViewCommandBindable : BindableBase, INodeViewSerialize
    {
        private string _appId;
        private string _deviceId;
        private string _command;

        public string Id { get; } = Uuid.NewUuid;
        public string AppId
        {
            get => _appId;
            set => SetProperty(ref _appId, value);
        }

        public string DeviceId
        {
            get => _deviceId;
            set => SetProperty(ref _deviceId, value);
        }

        public string Command
        {
            get => _command;
            set => SetProperty(ref _command, value);
        }

        public KeyValueCollectionBindable Parameters { get; set; } = new KeyValueCollectionBindable();

        public NodeViewCommandBindable(string appId = "", string deviceId = "", string command = "", KeyValueCollection parameters = null)
        {
            AppId = appId;
            DeviceId = deviceId;
            Command = command;
            if (parameters != null)
            {
                Parameters.Sets(parameters);
            }
        }

        public NodeViewCommandBindable(string deviceId, string command, KeyValueCollection parameters = null) : this("", deviceId, command, parameters)
        {

        }

        public JObject GetCommandRequestJson()
        {
            JObject json = new JObject();
            json["command"] = Command;
            var paramJson = new JObject();
            foreach (var parameter in Parameters)
            {
                paramJson[parameter.Key] = $"{parameter.Value}";
            }
            json["param"] = paramJson;
            return json;
        }
        public JToken ToJson()
        {
            JObject json = new JObject();
            if (!string.IsNullOrWhiteSpace(AppId))
            {
                json["appId"] = AppId;
            }
            json["deviceId"] = DeviceId;
            json["command"] = Command;
            var paramJson = new JObject();
            foreach (var parameter in Parameters)
            {
                paramJson[parameter.Key] = $"{parameter.Value}";
            }
            json["param"] = paramJson;
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            AppId = JsonUtils.GetValue(json, "appId", AppId);
            DeviceId = JsonUtils.GetValue(json, "deviceId", DeviceId);
            Command = JsonUtils.GetValue(json, "command", Command);
            var paramJson = JsonUtils.GetValue(json, "param", new JObject());
            foreach (var paramProperty in paramJson.Properties())
            {
                Parameters[paramProperty.Name] = $"{paramProperty.Value}";
            }

            if (string.IsNullOrWhiteSpace(DeviceId) || string.IsNullOrWhiteSpace(Command))
            {
                return false;
            }

            return true;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Command" : tagName;
            xmlWriter.WriteStartElement(tagName);
            if (!string.IsNullOrWhiteSpace(AppId))
            {
                xmlWriter.WriteAttributeString("appId", AppId);
            }
            xmlWriter.WriteAttributeString("deviceId", DeviceId);
            xmlWriter.WriteAttributeString("command", Command);

            xmlWriter.WriteStartElement("Param");
            foreach (var parameter in Parameters)
            {
                xmlWriter.WriteAttributeString(parameter.Key, $"{parameter.Value}");
            }
            xmlWriter.WriteEndElement();

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            AppId = XmlUtils.GetAttrValue(xmlNode, "appId", AppId);
            DeviceId = XmlUtils.GetAttrValue(xmlNode, "deviceId", DeviceId);
            Command = XmlUtils.GetAttrValue(xmlNode, "command", Command);

            XmlNode paramNode = xmlNode.SelectSingleNode("Param");
            if (paramNode?.Attributes != null)
            {
                foreach (XmlAttribute attribute in paramNode.Attributes)
                {
                    Parameters[attribute.Name] = $"{attribute.Value}";
                }
            }

            if (string.IsNullOrWhiteSpace(DeviceId) || string.IsNullOrWhiteSpace(Command))
            {
                return false;
            }

            return true;
        }

        private bool LoadFrom(NodeViewCommandBindable source)
        {
            if (source == null) return false;

            AppId = source.AppId;
            DeviceId = source.DeviceId;
            Command = source.Command;
            Parameters.Sets(source.Parameters);

            if (string.IsNullOrWhiteSpace(DeviceId) || string.IsNullOrWhiteSpace(Command))
            {
                return false;
            }

            return true;
        }
        public void Update(NodeViewCommandBindable source)
        {
            AppId = source.AppId;
            DeviceId = source.DeviceId;
            Command = source.Command;
            Parameters.Clear();
            Parameters.Sets(source.Parameters);
            RaisePropertyChanged("Parameters");
        }

        public static NodeViewCommandBindable CreateFrom(JObject json)
        {
            if (json == null) return null;
            var command = new NodeViewCommandBindable();
            if (command.LoadFrom(json))
            {
                return command;
            }

            return null;
        }
        public static NodeViewCommandBindable CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return null;
            var command = new NodeViewCommandBindable();
            if (command.LoadFrom(xmlNode))
            {
                return command;
            }

            return null;
        }

        public static NodeViewCommandBindable CreateFrom(NodeViewCommandBindable source)
        {
            if (source == null) return null;
            var command = new NodeViewCommandBindable();
            if (command.LoadFrom(source))
            {
                return command;
            }

            return null;
        }
    }
}
