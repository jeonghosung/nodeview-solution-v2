﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Net;
using NodeView.Tasks;
using NodeView.ViewModels;
using Prism.Services.Dialogs;

namespace NodeViewWindowCore.ViewModels
{
    public class DatawallAdminContentSettingViewModel : ContentSettingBaseViewModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public DatawallAdminContentSettingViewModel(NodeViewApiClient nodeViewApiClient, IDialogService dialogService) : base(dialogService)
        {
            _nodeViewApiClient = nodeViewApiClient;
            Task.Delay(200).ContinueWith(t=>LoadContents());
        }

        protected override void LoadContents()
        {
            try
            {
                var response = _nodeViewApiClient.Get("/contents");
                if (response.IsSuccess && response.IsJsonResponse)
                {
                    if (response.ResponseType == RestApiResponseType.JsonArray)
                    {
                        List<ContentItem> contentList = new List<ContentItem>();
                        var responseArray = response.ResponseJsonArray;
                        foreach (var token in responseArray)
                        {
                            JObject json = token.Value<JObject>();
                            if (json != null)
                            {
                                var nodeViewContent = ContentItem.CreateFrom(json);
                                if (nodeViewContent != null)
                                {
                                    contentList.Add(nodeViewContent);
                                }
                            }
                        }

                        ThreadAction.PostOnUiThread(() =>
                        {
                            WallContents.Clear();
                            WallContents.AddRange(contentList);
                            WallContentFolder = ContentTreeViewFolder.CreateFrom(WallContents);
                        });
                    }
                    else
                    {
                        Logger.Error("reponse of contents is not an array.");
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateContentsFromDataWall:");
            }
        }
        protected override bool UpdateContent(DataWallContent dataWallContent)
        {
            RestApiResponse response = null;
            var content = Content.GetCurrentDataWallContent().ToJson();
            var contentJson = new JObject
            {
                ["action"] = "update",
                ["param"] = content
            };
            response = _nodeViewApiClient.Put("/contents", contentJson);
            if (response != null && response.IsSuccess)
            {
                return true;
            }

            return false;
        }
        protected override bool CreateContent(DataWallContent[] dataWallContent)
        {
            RestApiResponse response = null;
            var content = Content.GetCurrentDataWallContent().ToJson();
            response = _nodeViewApiClient.Post("/contents", content);
            if (response != null && response.IsSuccess)
            {
                return true;
            }

            return false;
        }
        protected override bool DeleteContent(string[] ids)
        {
            JObject contentJson = new JObject
            {
                ["action"] = "delete",
                ["param"] = new JObject()
                {
                    ["ids"] = new JArray() { Content.IdField.Value }
                }
            };
            var response = _nodeViewApiClient.Put("/contents", contentJson);
            if (response != null && response.IsSuccess)
            {
                return true;
            }

            return false;
        }
        protected override bool CheckTocken()
        {
            bool result = false;
            if (!_nodeViewApiClient.HasAccessToken)
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Add("apiClient", _nodeViewApiClient);
                _dialogService.ShowDialog("LoginDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        result = true;
                    }
                });
            }
            else
            {
                result = true;
            }
            return result;
        }

        public void Reload()
        {
            LoadContents();
        }
    }
}