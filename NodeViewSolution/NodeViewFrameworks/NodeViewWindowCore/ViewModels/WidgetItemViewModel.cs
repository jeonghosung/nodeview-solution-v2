﻿using System.Windows;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Frameworks.Widgets;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.ViewModels
{
    public class WidgetItemViewModel : BindableBase
    {
        private string _name;
        private string _contentType = "Dashboard";
        private WidgetSource _content;
        private bool _isSelected = false;
        private NodeStatus _status = NodeStatus.Normal;

        public string Id { get; private set; }

        public string ContentType
        {
            get => _contentType;
            set => SetProperty(ref _contentType, value);
        }
        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public WidgetSource Content
        {
            get => _content;
            set => SetProperty(ref _content, value);
        }
        public bool IsSelected
        {
            get => _isSelected;
            set
            {
                SetProperty(ref _isSelected, value);
                RaisePropertyChanged("Visibility");
            }
        }

        public Visibility Visibility => IsSelected ? Visibility.Visible : Visibility.Collapsed;
        public NodeStatus Status
        {
            get => _status;
            set => SetProperty(ref _status, value);
        }

        public WidgetItemViewModel(WidgetSource widgetSource)
        {
            Id = Uuid.NewUuid;
            ContentType = string.IsNullOrWhiteSpace(widgetSource?.ContentType) ? "": widgetSource.ContentType;
            _name = widgetSource?.Name ?? Id;
            _content = widgetSource ?? new WidgetSource("");
        }
    }
}
