﻿using System.Windows;
using NodeView.DataModels;

namespace NodeView.ViewModels
{
    public class PropertyViewModel : PropertyBindableBase, IConfigurationEditModel
    {
        public string Description => Attributes["description"]?.ToString();

        public bool IsReadOnly
        {
            get
            {
                if (Attributes["isReadOnly"] != null && 
                    bool.TryParse(Attributes["isReadOnly"].ToString(), out bool result))
                {
                    return result;
                }
                return false;
            }
        }
        public Visibility PropertyVisibility
        {
            get
            {
                if (Attributes["isHidden"] != null &&
                    bool.TryParse(Attributes["isHidden"].ToString(), out bool result))
                {
                    return result ? Visibility.Collapsed : Visibility.Visible;
                }

                return Visibility.Visible;
            }
           
        }

        public string ControlType
        {
            get
            {
                var controlType = "";
                switch (Attributes["type"].ToString())
                {
                    case "bool":
                        controlType = "combobox";
                        break;
                    case "int":
                        controlType = "textbox";
                        break;
                    case "enum":
                        controlType = "combobox";
                        break;
                    case "intPoint":
                        controlType = "textbox";
                        break;
                    case "intSize":
                        controlType = "textbox";
                        break;
                    case "password":
                        break;
                    default:
                        controlType = "textbox";
                        break;
                }
                return controlType;

            }
        }

        public object Items
        {
            get
            {
                if (Attributes["type"].ToString().Equals("bool", System.StringComparison.OrdinalIgnoreCase))
                {
                    return new string[]{"true", "false"};
                }
                return Attributes["items"]?.ToString().Split(',');
            }
        } 

        public override IProperty Clone()
        {
            var property = new PropertyViewModel();
            Clone(property);
            return property;
        }

        public static PropertyViewModel CreateFrom(IProperty sourceProperty)
        {
            var property = new PropertyViewModel();
            property.LoadFrom(sourceProperty);
            return property;
        }
    }
}