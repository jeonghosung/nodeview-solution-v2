﻿using NodeView.Drawing;

namespace NodeView.ViewModels
{
    public interface IScreenAreaViewModel
    {
        void UpdatedScreenAreaSize(IntSize screenAreaSize);
    }
}