﻿using NodeView.Drawing;
using NodeView.Utils;

namespace NodeView.ViewModels
{
    public class ScreenCellViewModel : SelectableViewModel
    {
        private string _id = Uuid.NewUuid;
        private string _name = "";
        private ScreenCellType _cellType = ScreenCellType.Layout;
        private IntRect _rect = IntRect.Empty;
        private bool _isPopup = false;
        private bool _isMovingMode = false;

        public ScreenCellViewModel()
        {
        }
        public ScreenCellViewModel(IntRect rect)
        {
            _rect = rect;
        }

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }
        public ScreenCellType CellType
        {
            get => _cellType;
            set => SetProperty(ref _cellType, value);
        }

        public string Name
        {
            get => _name;
            set => SetProperty(ref _name, value);
        }

        public IntRect Rect
        {
            get => _rect;
            set => SetProperty(ref _rect, value);
        }

        public bool IsPopup
        {
            get => _isPopup;
            set => SetProperty(ref _isPopup, value);
        }

        public bool IsMovingMode
        {
            get => _isMovingMode;
            set => SetProperty(ref _isMovingMode, value);
        }
    }

    public enum ScreenCellType
    {
        Layout,
        Window,
        Popup
    }
}