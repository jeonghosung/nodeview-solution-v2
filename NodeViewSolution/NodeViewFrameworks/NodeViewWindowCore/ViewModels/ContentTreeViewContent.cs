﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.DataWalls;

namespace NodeView.ViewModels
{
    public class ContentTreeViewContent : DataWallContentBindable, ITreeViewItem
    {
        public ObservableCollection<ITreeViewItem> ChildItems { get; } = new ObservableCollection<ITreeViewItem>();
    }
}
