﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NodeView.DataWalls;
using NodeView.Net;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;

namespace NodeViewWindowCore.ViewModels
{
    public abstract class ContentSettingBaseViewModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private ContentTreeViewFolder _wallContentFolder = new ContentTreeViewFolder("");
        protected NodeViewApiClient _nodeViewApiClient = null;
        private ContentEditViewModel _content = null;
        protected readonly IDialogService _dialogService = null;
        private bool _isContentModified = false;
        private string _errorMessage;
        private bool _isContentSelected = false;

        public bool IsContentSelected
        {
            get => _isContentSelected;
            set => SetProperty(ref _isContentSelected, value);
        }
        public ContentTreeViewFolder WallContentFolder
        {
            get => _wallContentFolder;
            set => SetProperty(ref _wallContentFolder, value);
        }
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public ObservableCollection<ContentItem> WallContents { get; } = new ObservableCollection<ContentItem>();
        public ContentEditViewModel Content
        {
            get => _content;
            set => SetProperty(ref _content, value);
        }
        public DelegateCommand SaveContentCommand { get; set; }
        public DelegateCommand<string> DeletePropertyCommand { get; set; }
        public DelegateCommand AddPropertyCommand { get; set; }
        public DelegateCommand ResetCurrentContentCommand { get; set; }
        public DelegateCommand DeleteCurrentContentCommand { get; set; }
        public DelegateCommand CopyCurrentContentCommand { get; set; }
        public DelegateCommand CreateContentCommand { get; set; }
        public DelegateCommand<object> SelectedContentsTreeItemCommand
        {
            get
            {

                return new DelegateCommand<object>(args =>
                {
                    if (args is ContentItem contentItem)
                    {
                        if (Content != null && Content.GetCurrentDataWallContent().Id.Equals(contentItem.Id, StringComparison.OrdinalIgnoreCase))
                        {
                            return;
                        }

                        if (Content != null && (Content.IsCreateNew || Content.IsContentModify))
                        {
                            var parameters = new DialogParameters
                            {
                                {"title", "저장 확인"}, {"message", $"현재 컨텐츠를 저장 하시겠습니까?"}
                            };
                            _dialogService.ShowDialog("warningConfirm", parameters, r =>
                            {
                                if (r.Result == ButtonResult.OK)
                                {
                                    if (SaveCurrentContent())
                                    {
                                        CreateCurrentContentEditViewModel(contentItem);
                                    }
                                }
                                else
                                {
                                    if (Content.IsCreateNew)
                                    {
                                        DeleteCurrentContent(WallContentFolder.ChildItems);
                                    }
                                    CreateCurrentContentEditViewModel(contentItem);
                                }
                            });
                        }
                        else
                        {
                            CreateCurrentContentEditViewModel(contentItem);
                        }
                    }
                });
            }
        }

        public ContentSettingBaseViewModel(IDialogService dialogService)
        {
            _dialogService = dialogService;
            InitCommand();
        }
        protected abstract void LoadContents();
        protected abstract bool UpdateContent(DataWallContent dataWallContent);
        protected abstract bool CreateContent(DataWallContent[] dataWallContent);
        protected abstract bool DeleteContent(string[] ids);

        protected virtual bool CheckTocken()
        {
            return true;
        }
        private void InitCommand()
        {
            SaveContentCommand = new DelegateCommand(ExecuteSaveContentsCommand);
            DeletePropertyCommand = new DelegateCommand<string>(ExecuteDeletePropertyCommand);
            AddPropertyCommand = new DelegateCommand(ExecuteAddPropertyCommand);
            ResetCurrentContentCommand = new DelegateCommand(ExecuteResetCurrentContentCommand);
            DeleteCurrentContentCommand = new DelegateCommand(ExecuteDeleteCurrentContentCommand);
            CopyCurrentContentCommand = new DelegateCommand(ExecuteCopyCurrentContentCommand);
            CreateContentCommand = new DelegateCommand(ExecuteCreateContentCommand);
        }
        private void ClearAllSelectedContentItem(ContentTreeViewFolder contentTreeViewFolder)
        {
            if (contentTreeViewFolder == null)
            {
                return;
            }
            contentTreeViewFolder.IsSelected = false;
            foreach (var childItem in contentTreeViewFolder.ChildItems)
            {
                if (childItem is ContentTreeViewFolder treeViewFolder)
                {
                    treeViewFolder.IsSelected = false;
                    if (treeViewFolder.ChildItems.Count > 0)
                    {
                        ClearAllSelectedContentItem(treeViewFolder);
                    }
                }
                else if (childItem is ContentItem contentItem)
                {
                    contentItem.IsSelected = false;
                }
            }
        }
        private void CreateCurrentContentEditViewModel(ContentItem contentItem)
        {
            Content?.Dispose();
            Content = new ContentEditViewModel(contentItem, false);
            IsContentSelected = true;
            ClearAllSelectedContentItem(WallContentFolder);
            contentItem.IsSelected = true;
        }
        private bool SaveCurrentContent()
        {
            if (Content == null)
            {
                return false;
            }
            if (Content.IsCreateNew && WallContents.Any(c => c.Id.Equals(Content.IdField.Value)))
            {
                ErrorMessage = "동일한 Id가 존재합니다.";
                return false;
            }
            var datawallContent = DataWallContent.CreateFrom(Content.GetCurrentDataWallContent());
            if (!Content.IsCreateNew)
            {
                if (UpdateContent(datawallContent))
                {
                    _isContentModified = true;
                    Content.SetContentDataFromCurrentData();
                }
            }
            else
            {
                if (CreateContent(new[] { datawallContent }))
                {
                    _isContentModified = true;
                    Content.SetContentDataFromCurrentData();
                }
            }
            return false;
        }
        private void DeleteCurrentContent(ObservableCollection<ITreeViewItem> childItems)
        {
            foreach (var childItem in childItems)
            {
                if (childItem is ContentTreeViewFolder contentTreeViewFolder)
                {
                    if (!contentTreeViewFolder.RemoveChildContent(Content.IdField.Value))
                    {
                        if (contentTreeViewFolder.ChildItems.Any())
                        {
                            DeleteCurrentContent(contentTreeViewFolder.ChildItems);
                        }
                    }

                }
            }
        }
        private void ExecuteSaveContentsCommand()
        {
            if (!CheckTocken())
            {
                return;
            }
            SaveCurrentContent();
        }
        private void ExecuteDeletePropertyCommand(string id)
        {
            if (!CheckTocken())
            {
                return;
            }
            if (string.IsNullOrWhiteSpace(id))
            {
                Logger.Warn("DeleteProperty id is null");
                return;
            }
            Content.RemoveProperty(id);
        }
        private void ExecuteAddPropertyCommand()
        {
            if (!CheckTocken())
            {
                return;
            }
            _dialogService.ShowDialog("AddPropertyDialog", null, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                    var resultParameter = r.Parameters;
                    resultParameter.TryGetValue("id", out string id);
                    resultParameter.TryGetValue("value", out string value);
                    resultParameter.TryGetValue("description", out string description);
                    var property = new PropertyViewModel()
                    {
                        Id = id,
                        Value = value
                    };
                    property.SetAttribute("description", description);
                    Content.AddProperty(property);
                }
            });
        }
        private void ExecuteResetCurrentContentCommand()
        {
            Content.Reset();
        }
        private void ExecuteDeleteCurrentContentCommand()
        {
            if (!CheckTocken())
            {
                return;
            }
            var parameters = new DialogParameters();
            parameters.Add("title", "삭제 확인");
            parameters.Add("message", $"ID : {Content.IdField.Value} Name : {Content.NameField.Value}을 삭제 하시겠습니까?");
            _dialogService.ShowDialog("warningConfirm", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                    if (DeleteContent(new[] { Content.IdField.Id }))
                    {
                        DeleteCurrentContent(WallContentFolder.ChildItems);
                        Content.Dispose();
                        Content = null;
                    }
                }
            });
           
        }
        private void ExecuteCopyCurrentContentCommand()
        {
            try
            {
                if (!CheckTocken())
                {
                    return;
                }
                if (Content == null)
                {
                    return;
                }
                else
                {
                    var copyContentEditViewModel = Content.GetCopyContentEditViewModel();
                    var datawallContent = copyContentEditViewModel.GetCurrentDataWallContent();
                    string[] pathSplit = datawallContent.Path.Split('/');
                    string parentPath = pathSplit[pathSplit.Length - 1];
                    ContentTreeViewFolder parentTreeFolder = FindContentTreeViewFolderFromId(WallContentFolder, parentPath);
                    if (parentTreeFolder == null)
                    {
                        return;
                    }
                    ClearAllSelectedContentItem(parentTreeFolder);
                    parentTreeFolder.AddChildItem(datawallContent);
                    datawallContent.IsSelected = true;
                    Content?.Dispose();
                    Content = copyContentEditViewModel;
                }

            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteCopyCurrentContentCommand error.");
            }
        }
        private void ExecuteCreateContentCommand()
        {
            if (!CheckTocken())
            {
                return;
            }
            if (Content == null)
            {
                var parameters = new DialogParameters();
                parameters.Add("title", "Path 확인");
                parameters.Add("message", $"추가할 Path의 컨텐츠를 선택해주세요.");
                _dialogService.ShowDialog("MessageDialog", parameters, r => { });
            }
            else
            {

                var copyContentItem = Content.GetCurrentDataWallContent();
                var newContentItem = new ContentItem()
                {
                    Id = "newId",
                    Path = copyContentItem.Path
                };
                string[] pathSplit = copyContentItem.Path.Split('/');
                string parentPath = copyContentItem.Path.Replace($"/{pathSplit[pathSplit.Length - 1]}", "");
                ContentTreeViewFolder parentTreeFolder = FindContentTreeViewFolderFromId(WallContentFolder, parentPath);
                if (parentTreeFolder == null)
                {
                    return;
                }
                ContentEditViewModel contentEditViewModel = new ContentEditViewModel(newContentItem, true);
                contentEditViewModel.CanEditId = true;
                parentTreeFolder.AddChildItem(newContentItem);
                ClearAllSelectedContentItem(WallContentFolder);
                newContentItem.IsSelected = true;
                Content.Dispose();
                Content = contentEditViewModel;
            }

        }
        private ContentTreeViewFolder FindContentTreeViewFolderFromId(ContentTreeViewFolder contentTreeViewFolder, string path)
        {
            ContentTreeViewFolder findContentTreeViewFolder = null;
            if (contentTreeViewFolder.Id.Equals(path))
            {
                return contentTreeViewFolder;
            }

            foreach (var childItem in contentTreeViewFolder.ChildItems)
            {
                if (childItem is ContentTreeViewFolder treeViewFolder)
                {
                    findContentTreeViewFolder = FindContentTreeViewFolderFromId(treeViewFolder, path);
                    if (findContentTreeViewFolder != null)
                    {
                        return findContentTreeViewFolder;
                    }
                }
            }
            return null;
        }
    }
}
