﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Modularity;
using NodeView.Frameworks.Plugins;
using NodeView.Frameworks.Stations;
using NodeView.Ioc;
using Prism.Ioc;

namespace NodeView.Frameworks.Widgets
{
    public class WidgetControlManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static IWidgetControl CreateWidgetControl(IWidgetSource source)
        {
            try
            {
                IContainerProvider containerProvider = NodeViewContainerExtension.GetContainerProvider();
                if (source == null || containerProvider == null)
                {
                    return null;
                }

                IWidgetControl widgetControl = null;
                List<(Type Type, object Instance)> paramList = new List<(Type Type, object Instance)>();
                paramList.Add((typeof(IWidgetSource), source));

                if (!string.IsNullOrWhiteSpace(source.Module))
                {
                    string moduleName = source.Module;
                    var moduleManager = containerProvider.Resolve<ModuleManager>();
                    Type widgetType = moduleManager.FindModuleType(moduleName);
                    if (widgetType == null)
                    {
                        Logger.Error($"[{moduleName}]을 찾을 수없습니다.");
                        return null;
                    }
                    var widget = containerProvider.ResolveView(widgetType, paramList.ToArray());
                    if (widget is FrameworkElement widgetElement)
                    {
                        if (widgetElement.DataContext == null)
                        {
                            Type widgetModelType = moduleManager.FindModuleType(moduleName + "Model");
                            if (widgetModelType != null)
                            {
                                var viewModel = containerProvider.Resolve(widgetModelType, paramList.ToArray());
                                if (viewModel != null)
                                {
                                    widgetElement.DataContext = viewModel;
                                }
                            }
                        }
                    }
                    if (widget is IWidgetControl control)
                    {
                        widgetControl = control;
                    }
                    else
                    {
                        Logger.Error($"[{moduleName}]은 WidgetControl이 아닙니다.");
                        return null;
                    }
                }
                else if (!string.IsNullOrWhiteSpace(source.ContentType))
                {
                    widgetControl = containerProvider.ResolveView<IWidgetControl>(source.ContentType, paramList.ToArray());
                }


                if(widgetControl == null)
                {
                    Logger.Error("Module and ContentType 정보가 잘못되었습니다.");
                    return null;
                }
                
                widgetControl.Source = source;

                return widgetControl;
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateWidgetControl:");
            }

            return null;
        }
    }
}
