﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class TreeNode : TreeNodeBase
    {
        public override INode CreateNode()
        {
            return new TreeNode();
        }

        public static TreeNode CreateFrom(INode sourceNode)
        {
            TreeNode node = new TreeNode();
            node.LoadFrom(sourceNode);
            return node;
        }
        public static TreeNode CreateFrom(JObject json)
        {
            TreeNode node = new TreeNode();
            node.LoadFrom(json);
            return node;
        }

        public static TreeNode CreateFrom(XmlNode xmlNode)
        {
            TreeNode node = new TreeNode();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}
