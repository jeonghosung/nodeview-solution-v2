﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class EventAction : IEventAction
    {
        public string Id { get; set; } = "";
        public AlarmEventLevel Level { get; set; }
        public string Description { get; set; } = "";
        public string EventOnActionId { get; set; } = "";
        public string EventOffActionId { get; set; } = "";
        

        public void Update(EventAction @event)
        {
            if (@event == null) return;

            Level = @event.Level;
            Description = @event.Description;
            EventOnActionId = @event.EventOnActionId;
            EventOffActionId = @event.EventOffActionId;
        }

        public EventAction Clone()
        {
            var eventAction = new EventAction();

            eventAction.Id = Id;
            eventAction.Level = Level;
            eventAction.Description = Description;
            eventAction.EventOnActionId = EventOnActionId;
            eventAction.EventOffActionId = EventOffActionId;

            return eventAction;
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["level"] = $"{Level}";
            json["description"] = Description;
            json["eventOnActionId"] = EventOnActionId;
            json["eventOffActionId"] = EventOffActionId;

            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", "");
            Level = JsonUtils.GetValue(json, "level", AlarmEventLevel.Alarm);
            Description = JsonUtils.GetValue(json, "description", "");
            EventOnActionId = JsonUtils.GetValue(json, "eventOnActionId", "");
            EventOffActionId = JsonUtils.GetValue(json, "eventOffActionId", "");

            return !string.IsNullOrWhiteSpace(Id);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Event" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("level", $"{Level}");
            xmlWriter.WriteAttributeString("description", Description);
            xmlWriter.WriteAttributeString("eventOnActionId", EventOnActionId);
            xmlWriter.WriteAttributeString("eventOffActionId", EventOffActionId);
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", "");
            Level = XmlUtils.GetAttrValue(xmlNode, "level", AlarmEventLevel.Alarm);
            Description = XmlUtils.GetAttrValue(xmlNode, "description", "");
            EventOnActionId = XmlUtils.GetAttrValue(xmlNode, "eventOnActionId", "");
            EventOffActionId = XmlUtils.GetAttrValue(xmlNode, "eventOffActionId", "");

            return !string.IsNullOrWhiteSpace(Id);
        }

        public static EventAction CreateFrom(JObject json)
        {
            var alarmEvent = new EventAction();
            alarmEvent.LoadFrom(json);
            return alarmEvent;
        }

        public static EventAction CreateFrom(XmlNode xmlNode)
        {
            var alarmEvent = new EventAction();
            alarmEvent.LoadFrom(xmlNode);
            return alarmEvent;
        }
    }
}
