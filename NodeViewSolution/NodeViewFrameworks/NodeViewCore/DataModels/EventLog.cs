﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public interface IEventLog
    {
        string Timestamp { get; }
        AlarmEventLevel Level { get; }
        string SenderName { get; }
        string SenderIp { get; }
        string Message { get; }
    }

    public static class EventLogExtensions
    {
        public static JObject ToJson(this IEventLog obj)
        {
            return new JObject()
            {
                ["timestamp"] = obj.Timestamp,
                ["level"] = $"{obj.Level}",
                ["senderName"] = obj.SenderName,
                ["senderIp"] = obj.SenderIp,
                ["message"] = obj.Message
            };
        }
    }
    public class EventLog : IEventLog
    {
        public string Timestamp { get; }
        public AlarmEventLevel Level { get; set; }
        public string SenderName { get; set; }
        public string SenderIp { get; set; }
        public string Message { get; set; }

        public EventLog(string timestamp, AlarmEventLevel eventLevel, string senderName, string senderIp, string message)
        {
            Timestamp = string.IsNullOrWhiteSpace(timestamp) ? StringUtils.GetCurrentDateTimeString() : timestamp;
            Level = eventLevel;
            SenderName = senderName;
            SenderIp = senderIp;
            Message = message;
        }
        public EventLog(AlarmEventLevel eventLevel, string senderName, string message): this("", eventLevel, senderName, "", message)
        {
        }
        public EventLog( string senderName, string message) : this(AlarmEventLevel.Normal, senderName, message)
        {
        }
        public EventLog(string message) : this("", message)
        {
        }
        public EventLog(IEventLog eventLog) : this(eventLog.Timestamp, eventLog.Level, eventLog.SenderName, eventLog.SenderIp, eventLog.Message)
        {
        }

        public static EventLog CreateFrom(JObject json)
        {
            if (json == null) return null;
            string timestamp = JsonUtils.GetStringValue(json, "timestamp");
            if (string.IsNullOrWhiteSpace(timestamp))
            {
                timestamp = StringUtils.GetCurrentDateTimeString();
            }
            AlarmEventLevel eventLevel = JsonUtils.GetValue<AlarmEventLevel>(json, "level", AlarmEventLevel.Normal);
            string senderName = JsonUtils.GetStringValue(json, "senderName");
            string senderIp = JsonUtils.GetStringValue(json, "senderIp"); 
            string message = JsonUtils.GetStringValue(json, "message");

            return new EventLog(timestamp, eventLevel, senderName, senderIp, message);
        }
    }
}
