﻿using System.Runtime.InteropServices;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class FieldDescription : PropertyBase
    {
        public string Description
        {
            get => GetAttrValue("description", "");
            set => SetAttribute("description", value);
        }
        public string ValueType
        {
            get => GetAttrValue("type", "string");
            set => SetAttribute("type", value);
        }
        public string[] Items
        {
            get => StringUtils.Split(GetAttrValue("items", ""), ',');
            set => SetAttribute("items", string.Join(",", value));
        }

        public bool IsOptional
        {
            get => GetAttrValue("isOptional", false);
            set => SetAttribute("isOptional", value);
        }

        public FieldDescription(string id = "", string value = "", string description = "", string valueType = "", string items = "", bool isOptional = false)
        {
            Id = id;
            Value = value;
            Description = description;
            if (!string.IsNullOrWhiteSpace(valueType))
            {
                ValueType = valueType;
            }
            if (!string.IsNullOrWhiteSpace(items))
            {
                Items = StringUtils.Split(items, ',');
            }

            if (isOptional)
            {
                IsOptional = true;
            }
        }
        public override IProperty Clone()
        {
            var property = new FieldDescription();
            Clone(property);
            return property;
        }

        public static FieldDescription CreateFrom(JObject json)
        {
            var property = new FieldDescription();
            property.LoadFrom(json);
            return property;
        }

        public static FieldDescription CreateFrom(XmlNode xmlNode)
        {
            var property = new FieldDescription();
            property.LoadFrom(xmlNode);
            return property;
        }
    }
}