﻿using System;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class NodeViewAppNode : INodeViewAppNode
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string Id { get; set; } = "";
        public string PathId => string.IsNullOrWhiteSpace(Path) ? Id : $"{Path}/{Id}";
        public string Path { get; set; } = "";
        public NodeStatus Status { get; set; } = NodeStatus.None;
        public string StatusMessage { get; set; } = "";
        public string Updated { get; set; } = "";
        public string AppKey { get; set; } = "";
        public string Name { get; set; } = "";
        public string AppType { get; set; } = "";
        public string Ip { get; set; } = "";
        public int Port { get; set; } = 0;
        public string Uri { get; set; } = "";
        public bool IsBlocked { get; set; } = false;
        public int ProcessManagerPort { get; set; } = 20102;

        public NodeViewAppNode()
        {
        }

        public NodeViewAppNode(string id, string appKey, string appType, string name = "", string ip = "127.0.0.1", int port = 0, string uri = "", int processManagerPort = 20102) : this()
        {
            Id = id;
            AppKey = appKey;
            AppType = appType;
            Name = string.IsNullOrWhiteSpace(name) ? Id : name; ;
            Ip = ip;
            Port = port;
            Uri = string.IsNullOrWhiteSpace(uri) ? $"http://{Ip}:{Port}/api/v1" : uri;
            ProcessManagerPort = processManagerPort;
        }

        private void LoadFrom(INodeViewAppNode source)
        {
            Id = source.Id;
            Path = source.Path;
            Status = source.Status;
            StatusMessage = source.StatusMessage;
            Updated = source.Updated;
            AppKey = source.AppKey;
            AppType = source.AppType;
            Name = source.Name;
            Ip = source.Ip;
            Port = source.Port;
            Uri = source.Uri;
            IsBlocked = source.IsBlocked;
            ProcessManagerPort = source.ProcessManagerPort;
        }
        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            if (string.IsNullOrWhiteSpace(Path))
            {
                json["path"] = Path;
            }
            if (Status != NodeStatus.None)
            {
                json["status"] = $"{Status}";
                json["statusMessage"] = StatusMessage;
            }

            if (string.IsNullOrWhiteSpace(Updated))
            {
                json["updated"] = Updated;
            }
            json["appKey"] = AppKey;
            json["appType"] = AppType;
            json["name"] = Name;
            json["ip"] = Ip;
            json["port"] = $"{Port}";
            json["uri"] = Uri;
            if (IsBlocked)
            {
                json["isBlocked"] = $"{IsBlocked}";
            }
            json["processManagerPort"] = $"{ProcessManagerPort}";
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", Id);

            Path = JsonUtils.GetValue(json, "path", Path);
            Status = JsonUtils.GetValue(json, "status", Status);
            StatusMessage = JsonUtils.GetValue(json, "statusMessage", StatusMessage);
            Updated = JsonUtils.GetValue(json, "updated", Updated);

            AppKey = JsonUtils.GetValue(json, "appKey", AppKey);
            AppType = JsonUtils.GetValue(json, "appType", AppType);
            Name = JsonUtils.GetValue(json, "name", Name);
            Ip = JsonUtils.GetValue(json, "ip", Ip);
            Port = JsonUtils.GetValue(json, "port", Port);
            Uri = JsonUtils.GetValue(json, "uri", Uri);
            IsBlocked = JsonUtils.GetValue(json, "isBlocked", IsBlocked);
            ProcessManagerPort = JsonUtils.GetValue(json, "processManagerPort", ProcessManagerPort);

            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = AppKey;
            }
            if (string.IsNullOrWhiteSpace(Name))
            {
                Name = Id;
            }
            if (string.IsNullOrWhiteSpace(Uri))
            {
                Uri = $"http://{Ip}:{Port}/api/v1";
            }

            return !(string.IsNullOrWhiteSpace(Id) || string.IsNullOrWhiteSpace(AppKey));
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "App" : tagName;
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            if (string.IsNullOrWhiteSpace(Path))
            {
                xmlWriter.WriteAttributeString("path", Path);
            }
            if (Status != NodeStatus.None)
            {
                xmlWriter.WriteAttributeString("status", $"{Status}");
                xmlWriter.WriteAttributeString("statusMessage", StatusMessage);
            }

            if (string.IsNullOrWhiteSpace(Updated))
            {
                xmlWriter.WriteAttributeString("updated", Updated);
            }
            xmlWriter.WriteAttributeString("appKey", AppKey);
            xmlWriter.WriteAttributeString("appType", AppType);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("ip", Ip);
            xmlWriter.WriteAttributeString("port", $"{Port}");
            xmlWriter.WriteAttributeString("uri", Uri);
            if (IsBlocked)
            {
                xmlWriter.WriteAttributeString("isBlocked", $"{IsBlocked}");
            }
            xmlWriter.WriteAttributeString("processManagerPort", $"{ProcessManagerPort}");
            
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", Id);
            
            Path = XmlUtils.GetAttrValue(xmlNode, "path", Path);
            Status = XmlUtils.GetAttrValue(xmlNode, "status", Status);
            StatusMessage = XmlUtils.GetAttrValue(xmlNode, "statusMessage", StatusMessage);
            Updated = XmlUtils.GetAttrValue(xmlNode, "updated", Updated);

            AppKey = XmlUtils.GetAttrValue(xmlNode, "appKey", AppKey);
            AppType = XmlUtils.GetAttrValue(xmlNode, "appType", AppType);
            Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
            Ip = XmlUtils.GetAttrValue(xmlNode, "ip", Ip);
            Port = XmlUtils.GetAttrValue(xmlNode, "port", Port);
            Uri = XmlUtils.GetAttrValue(xmlNode, "uri", Uri);
            IsBlocked = XmlUtils.GetAttrValue(xmlNode, "isBlocked", IsBlocked);
            ProcessManagerPort = XmlUtils.GetAttrValue(xmlNode, "processManagerPort", ProcessManagerPort);

            if (string.IsNullOrWhiteSpace(Id))
            {
                Id = AppKey;
            }
            if (string.IsNullOrWhiteSpace(Name))
            {
                Name = Id;
            }
            if (string.IsNullOrWhiteSpace(Uri))
            {
                Uri = $"http://{Ip}:{Port}/api/v1";
            }

            return !(string.IsNullOrWhiteSpace(Id) || string.IsNullOrWhiteSpace(AppKey));
        }
        public static NodeViewAppNode CreateFrom(INodeViewAppNode source)
        {
            var node = new NodeViewAppNode();
            node.LoadFrom(source);
            return node;
        }

        public static NodeViewAppNode CreateFrom(JObject json)
        {
            var node = new NodeViewAppNode();
            node.LoadFrom(json);
            return node;
        }

        public static NodeViewAppNode CreateFrom(XmlNode xmlNode)
        {
            var node = new NodeViewAppNode();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}