﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class NodeViewAction : IObject, INodeViewSerialize
    {
        private readonly List<NodeViewCommand> _commands = new List<NodeViewCommand>();
        public string Id { get; set; }
        public string Description { get; set; }
        public NodeViewCommand[] Commands => _commands.ToArray();

        public NodeViewAction(string id = "", string description = "", IEnumerable<NodeViewCommand> commands = null)
        {
            Id = id;
            Description = description;
            if (commands != null) _commands.AddRange(commands);
        }
        public void Update(NodeViewAction action)
        {
            Description = action.Description;
            _commands.Clear();
            _commands.AddRange(action.Commands);
        }

        public bool AddCommand(NodeViewCommand command)
        {
            if (command == null || string.IsNullOrWhiteSpace(command.DeviceId) || string.IsNullOrWhiteSpace(command.Command))
            {
                return false;
            }
            _commands.Add(command);
            return true;
        }

        public NodeViewAction Clone()
        {
            List<NodeViewCommand> commands = new List<NodeViewCommand>();
            if (Commands != null && Commands.Length > 0)
            {
                foreach (var command in Commands)
                {
                    commands.Add(command.Clone());
                }
            }
            var action = new NodeViewAction(Id, Description, commands.ToArray());

            return action;
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["description"] = Description;
            var commandArray = new JArray();
            foreach (var command in _commands.ToArray())
            {
                commandArray.Add(command.ToJson());
            }
            json["commands"] = commandArray;
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", Id);
            Description = JsonUtils.GetValue(json, "description", Description);
            var commandArray = JsonUtils.GetValue(json, "commands", new JArray());
            foreach (var commandToken in commandArray)
            {
                var command = NodeViewCommand.CreateFrom(commandToken.Value<JObject>());
                if (command != null)
                {
                    _commands.Add(command);
                }
            }

            return !string.IsNullOrWhiteSpace(Id);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Action" : tagName;

            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("description", Description);
            
            foreach (var command in _commands.ToArray())
            {
                command.WriteXml(xmlWriter, "Command");
            }

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", Id);
            Description = XmlUtils.GetAttrValue(xmlNode, "description", Description);

            XmlNodeList commandNodeList = xmlNode.SelectNodes("Command");
            if (commandNodeList != null)
            {
                foreach (XmlNode commandNode in commandNodeList)
                {
                    var command = NodeViewCommand.CreateFrom(commandNode);
                    if (command != null)
                    {
                        _commands.Add(command);
                    }
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }

        public static NodeViewAction CreateFrom(JObject json)
        {
            if (json == null) return null;
            var action = new NodeViewAction();
            if (action.LoadFrom(json))
            {
                return action;
            }

            return null;
        }
        public static NodeViewAction CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return null;
            var action = new NodeViewAction();
            if (action.LoadFrom(xmlNode))
            {
                return action;
            }

            return null;
        }
    }
}
