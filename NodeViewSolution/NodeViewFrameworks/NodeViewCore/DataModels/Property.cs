﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class Property : PropertyBase
    {
        public override IProperty Clone()
        {
            Property property = new Property();
            Clone(property);
            return property;
        }

        public static Property CreateFrom(JObject json)
        {
            Property property = new Property();
            property.LoadFrom(json);
            return property;
        }

        public static Property CreateFrom(XmlNode xmlNode)
        {
            Property property = new Property();
            property.LoadFrom(xmlNode);
            return property;
        }
    }
}
