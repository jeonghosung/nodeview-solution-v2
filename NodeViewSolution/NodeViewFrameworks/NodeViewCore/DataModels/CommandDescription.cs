﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class CommandDescription
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly List<FieldDescription> _parameters = new List<FieldDescription>();
        public string Command { get; set; }
        public string Description { get; set; }
        public FieldDescription[] Parameters => _parameters.ToArray();


        public CommandDescription(string command = "", string description = "", IEnumerable<FieldDescription> parameters = null)
        {
            Command = command;
            Description = (string.IsNullOrWhiteSpace(description) ? "" : description);
            if (parameters != null)
            {
                _parameters.AddRange(parameters);
            }
        }
        
        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            Command = JsonUtils.GetValue(json, "command", Command);
            var parameterJsonArray = JsonUtils.GetValue(json, "parameters", new JArray());
            foreach (var parameterToken in parameterJsonArray)
            {
                var parameter = FieldDescription.CreateFrom(parameterToken.Value<JObject>());
                if (parameter != null)
                {
                    _parameters.Add(parameter);
                }
            }
            Description = JsonUtils.GetValue(json, "description", Description);

            return !string.IsNullOrWhiteSpace(Command);
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            try
            {
                json["command"] = Command;
                json["description"] = Description;
                var parameterJsonArray = new JArray();
                foreach (var parameter in _parameters)
                {
                    parameterJsonArray.Add(parameter.ToJson());
                }
                json["parameters"] = parameterJsonArray;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ToJson");
                throw;
            }

            return json;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "DeviceCommand" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("command", $"{Command}");
            xmlWriter.WriteAttributeString("description", $"{Description}");
            foreach (var parameter in _parameters)
            {
                parameter.WriteXml(xmlWriter, "Parameter");
            }
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Command = XmlUtils.GetAttrValue(xmlNode, "command", Command);
            Description = XmlUtils.GetAttrValue(xmlNode, "description", Description);

            XmlNodeList parameterNodeList = xmlNode.SelectNodes("Parameter");
            if (parameterNodeList != null)
            {
                foreach (XmlNode parameterNode in parameterNodeList)
                {
                    var parameter = FieldDescription.CreateFrom(parameterNode);
                    if (parameter != null)
                    {
                        _parameters.Add(parameter);
                    }
                }
            }
            return !string.IsNullOrWhiteSpace(Command);
        }

        public static CommandDescription CreateFrom(JObject json)
        {
            CommandDescription node = new CommandDescription();
            node.LoadFrom(json);
            return node;
        }

        public static CommandDescription CreateFrom(XmlNode xmlNode)
        {
            CommandDescription node = new CommandDescription();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}
