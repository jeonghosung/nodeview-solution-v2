﻿using System.Collections.Generic;

namespace NodeView.DataModels
{
    public interface IKeyValueCollection : IEnumerable<IKeyValuePair>
    {
        int Count { get; }
        string[] Keys { get; }
        object this[string key] { get; }
        bool TryGetValue(string key, out object value);
        bool Contains(string key);
        T GetValue<T>(string key, T defaultValue);
        string GetStringValue(string key, string defaultValue = "");
    }
}
