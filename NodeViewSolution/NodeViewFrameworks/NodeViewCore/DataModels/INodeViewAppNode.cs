﻿namespace NodeView.DataModels
{
    public interface INodeViewAppNode : INodeObject, INodeViewSerialize
    {
        string AppKey { get; }
        string AppType { get; }
        string Name { get; }
        string Ip { get; }
        int Port { get; }
        string Uri { get; }
        bool IsBlocked { get; }
        int ProcessManagerPort { get; }
    }
}