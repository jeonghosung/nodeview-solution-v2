﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeView.DataModels
{
    public interface ILicenseCard
    {
        string Package { get; }
        string Version { get; }
        string User { get; }
        string Key { get; }
        string Created { get; }
        string ExpireDate { get; }
        bool IsTrial { get; }
        string Note { get; }
        IAppLicense[] AppLicenses { get; }
    }

    public interface IAppLicense
    {
        string Name { get; }
        int Count { get; }
    }
}
