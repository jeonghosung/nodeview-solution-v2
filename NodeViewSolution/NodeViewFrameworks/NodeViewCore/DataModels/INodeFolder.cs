﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace NodeView.DataModels
{
    public interface INodeFolder : IObject
    {
        string Path { get; set; }
        IEnumerable<INode> ChildNodes { get; }
        IEnumerable<INodeFolder> ChildFolders { get; }

        bool TryGetNode(string path, out INode node);
        bool SetChildNode(INode node);
        bool RemoveChildNode(string id);
        void ClearChildNodes();

        bool TryGetFolder(string path, out INodeFolder folder);
        bool SetChildFolder(INodeFolder folder);
        bool RemoveChildFolder(string id);
        void ClearChildFolders();

        void Clear();
    }
}
