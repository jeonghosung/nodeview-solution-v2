﻿using System;
using System.Text.RegularExpressions;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class MonitorInputSource
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static Regex _hexStringRegex = new Regex("0x[0-9a-fA-F]+");
        private static Regex _intStringRegex = new Regex("[0-9]+");
        public string Name { get; set; }
        public int Value { get; set; }

        public MonitorInputSource(string name, int value)
        {
            Name = name;
            Value = value;
        }

        public static MonitorInputSource CreateFrom(IProperty property)
        {
            if (property == null)
            {
                Logger.Error("property == null");
                return null;
            }
            try
            {
                string name = property.Id;
                string value = property.GetValue("");
                int intValue = 0;
                if (string.IsNullOrWhiteSpace(name))
                {
                    Logger.Error("name is empty!");
                    return null;
                }
                if (string.IsNullOrWhiteSpace(value))
                {
                    Logger.Error("value is empty!");
                    return null;
                }

                if (_intStringRegex.IsMatch(value))
                {
                    intValue = StringUtils.GetIntValue(value);
                }
                else if (_hexStringRegex.IsMatch(value))
                {
                    intValue = Int32.Parse(value.Substring(2), System.Globalization.NumberStyles.HexNumber);
                }
                else
                {
                    Logger.Error($"value({value}) is not a number!");
                    return null;
                }

                return new MonitorInputSource(name, intValue);
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public static MonitorInputSource CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                Logger.Error("xmlNode == null");
                return null;
            }
            try
            {
                string name = XmlUtils.GetAttrValue(xmlNode, "name", "");
                string value = XmlUtils.GetAttrValue(xmlNode, "value", "").ToLower();
                int intValue = 0;
                if (string.IsNullOrWhiteSpace(name))
                {
                    Logger.Error("name is empty!");
                    return null;
                }
                if (string.IsNullOrWhiteSpace(value))
                {
                    Logger.Error("value is empty!");
                    return null;
                }

                if (_intStringRegex.IsMatch(value))
                {
                    intValue = StringUtils.GetIntValue(value);
                }
                else if (_hexStringRegex.IsMatch(value))
                {
                    intValue = Int32.Parse(value.Substring(2), System.Globalization.NumberStyles.HexNumber);
                }
                else
                {
                    Logger.Error($"value({value}) is not a number!");
                    return null;
                }
               
                return new MonitorInputSource(name, intValue);
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public static MonitorInputSource CreateFrom(JObject jsonObject)
        {
            if (jsonObject == null)
            {
                Logger.Error("jsonObject == null");
                return null;
            }
            try
            {
                string name = JsonUtils.GetStringValue(jsonObject, "name", "");
                string value = JsonUtils.GetStringValue(jsonObject, "value", "").ToLower();
                int intValue = 0;
                if (string.IsNullOrWhiteSpace(name))
                {
                    Logger.Error("name is empty!");
                    return null;
                }
                if (string.IsNullOrWhiteSpace(value))
                {
                    Logger.Error("value is empty!");
                    return null;
                }
                if (_intStringRegex.IsMatch(value))
                {
                    intValue = StringUtils.GetIntValue(value);
                }
                else if (_hexStringRegex.IsMatch(value))
                {
                    intValue = Int32.Parse(value.Substring(2), System.Globalization.NumberStyles.HexNumber);
                }
                else
                {
                    Logger.Error($"value({value}) is not a number!");
                    return null;
                }

                return new MonitorInputSource(name, intValue);
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public JObject ToJson()
        {
            JObject jsonObject = new JObject();
            try
            {
                jsonObject["name"] = Name;
                jsonObject["value"] = $"{Value}";
            }
            catch (Exception e)
            {
                Logger.Error(e, "ToJson");
                throw;
            }

            return jsonObject;
        }

        public void WriteXml(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("InputSource");
            xmlWriter.WriteAttributeString("name", $"{Name}");
            xmlWriter.WriteAttributeString("value", $"{Value}");
            xmlWriter.WriteEndElement();
        }
    }
}
