﻿using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public interface INode : INodeObject, INodeViewSerialize
    {
        string NodeType { get; }
        IKeyValueCollection Attributes { get; }
        IPropertyCollection Properties { get; }
        object this[string search] { get; }
        bool Contains(string search);
        bool TryGetObject(string search, out object @object);
        T GetValue<T>(string search, T defaultValue);
        string GetStringValue(string search, string defaultValue = "");
        bool SetAttribute(string key, object value);
        bool SetAttribute(IKeyValuePair keyValuePair);
        void SetAttributes(IEnumerable<IKeyValuePair> keyValuePairs);
        bool RemoveAttribute(string key);
        void ClearAttributes();
        bool SetProperty(string id, string value);
        bool SetProperty(IProperty property);
        void SetProperties(IEnumerable<IProperty> properties);
        bool RemoveProperty(string id);
        void ClearProperties();

        INode Clone();
        INode CreateNode();
        bool LoadFrom(INode sourceNode);
    }
}

    
