﻿namespace NodeView.DataModels
{
    public interface IKeyValuePair
    {
        string Key { get; set; }
        object Value { get; set; }
        T GetValue<T>(T defaultValue = default(T));
    }
}
