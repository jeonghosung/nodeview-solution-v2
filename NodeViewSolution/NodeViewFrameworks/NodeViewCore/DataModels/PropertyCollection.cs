﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class PropertyCollection : IPropertyCollection
    {
        private readonly Dictionary<string, IProperty> _dictionary = new Dictionary<string, IProperty>();

        public int Count => _dictionary.Count;
        public IEnumerator<IProperty> GetEnumerator()
        {
            return _dictionary.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public IProperty this[string id]
        {
            get
            {
                if (_dictionary.TryGetValue(id, out var property))
                {
                    return property;
                }

                return null;
            }
        }
        public bool TryGetProperty(string id, out IProperty property)
        {
            property = null;
            if (string.IsNullOrWhiteSpace(id)) return false;

            return _dictionary.TryGetValue(id, out property);
        }

        public string[] Ids => _dictionary.Keys.ToArray();
        public bool Contains(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            return _dictionary.ContainsKey(id);
        }

        public T GetValue<T>(string id, T defaultValue)
        {
            if (TryGetProperty(id, out var property))
            {
                return StringUtils.GetValue(property.Value, defaultValue);
            }

            return defaultValue;
        }

        public string GetStringValue(string id, string defaultValue = "")
        {
            return GetValue(id, defaultValue);
        }

        #region set/remove/clear

        public bool Set(IProperty property)
        {
            if (property == null) return false;

            if (string.IsNullOrWhiteSpace(property.Id))
            {
                return false;
            }
            _dictionary[property.Id] = property.Clone();
            return true;
        }

        public bool Sets(IEnumerable<IProperty> properties)
        {
            if (properties == null) return false;

            foreach (var property in properties)
            {
                Set(property);
            }
            return true;
        }

        public bool Remove(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            return _dictionary.Remove(id);
        }

        public void Clear()
        {
            _dictionary.Clear();
        }

        #endregion
    }
}
