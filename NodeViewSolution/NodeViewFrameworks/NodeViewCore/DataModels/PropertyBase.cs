﻿using Newtonsoft.Json.Linq;
using NodeView.Utils;
using System.Collections.Generic;
using System.Xml;

namespace NodeView.DataModels
{
    public abstract class PropertyBase : IProperty
    {
        private readonly KeyValueCollection _attributes;
        public string Id { get; set; }
        public string Value { get; set; }

        public IKeyValueCollection Attributes
        {
            get => _attributes;
        }

        protected PropertyBase()
        {
            Id = "";
            Value = "";
            _attributes = new KeyValueCollection();
        }

        public object this[string key]
        {
            get
            {
                if (string.IsNullOrWhiteSpace(key)) return "";
                switch (key)
                {
                    case "id":
                        return Id;
                    case "value":
                        return Value;
                }

                return Attributes[key];
            }
        }
        public T GetValue<T>(T defaultValue)
        {
            return StringUtils.GetValue(Value, defaultValue);
        }

        public string GetStringValue(string defaultValue = "")
        {
            return GetValue(defaultValue);
        }

        public T GetAttrValue<T>(string key, T defaultValue)
        {
            return StringUtils.GetValue(Attributes[key], defaultValue);
        }

        public bool SetAttribute(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;

            switch (key)
            {
                case "id":
                    Id = $"{value}";
                    break;
                case "value":
                    Value = $"{value}";
                    break;
                default:
                    _attributes.Set(key, value);
                    break;
            }

            return true;
        }

        public bool SetAttribute(IKeyValuePair keyValuePair)
        {
            if (string.IsNullOrWhiteSpace(keyValuePair?.Key)) return false;
            return SetAttribute(keyValuePair.Key, keyValuePair.Value);
        }

        public void SetAttributes(IEnumerable<IKeyValuePair> keyValuePairs)
        {
            if(keyValuePairs == null) return;
            foreach (var keyValuePair in keyValuePairs)
            {
                SetAttribute(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public bool RemoveAttribute(string key)
        {
            return _attributes.Remove(key);
        }

        public void ClearAttributes()
        {
            _attributes.Clear();
        }

        public abstract IProperty Clone();

        public void Clone(PropertyBase property)
        {
            if (property == null) return;

            property.Id = Id;
            property.Value = Value;
            property.SetAttributes(Attributes);
        }

        public bool LoadFrom(IProperty sourceProperty)
        {
            if (sourceProperty == null) return false;

            Id = sourceProperty.Id;
            Value = sourceProperty.Value;
            SetAttributes(sourceProperty.Attributes);

            return true;
        }
        public virtual JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["value"] = Value;
            foreach (var attribute in Attributes)
            {
                json[attribute.Key] = $"{attribute.Value}";
            }
            return json;
        }

        public virtual bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            foreach (var property in json.Properties())
            {
                SetAttribute(property.Name, $"{property.Value}");
            }
            return !string.IsNullOrWhiteSpace(Id);
        }

        public virtual void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Property" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("value", Value);
            foreach (var attribute in Attributes)
            {
                xmlWriter.WriteAttributeString(attribute.Key, $"{attribute.Value}");
            }
            xmlWriter.WriteEndElement();
        }

        public virtual bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    SetAttribute(attribute.Name, attribute.Value);
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }
    }
}
