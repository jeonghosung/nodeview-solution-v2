﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class Device : DeviceBase
    {
        public static Device CreateFrom(JObject json)
        {
            var device = new Device();
            device.LoadFrom(json);
            return device;
        }

        public static Device CreateFrom(XmlNode xmlNode)
        {
            var device = new Device();
            device.LoadFrom(xmlNode);
            return device;
        }
    }
}
