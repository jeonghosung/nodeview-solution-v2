﻿using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public interface INodeCollection : IEnumerable<INode>
    {
        string Path { get; }
        IEnumerable<IPathNode> NodePaths { get; }
        int Count { get; }
        bool Set(INode node);
        bool Set(string path, INode node);
        bool Sets(IEnumerable<INode> nodes);
        bool Sets(string path, IEnumerable<INode> nodes);
        bool Remove(string path, string id);
        bool Remove(string pathId);
        void Clear();
        INode this[string pathId] { get; }
        bool Contains(string path, string id);
        bool Contains(string pathId);
        bool TryGetNodePath(string path, out IPathNode pathNode);
        bool TryGetNode(string pathId, out INode node);
        bool TryGetObject(string search, out object @object);
        T GetValue<T>(string search, T defaultValue);
        string GetStringValue(string search, string defaultValue = "");
        IEnumerable<INode> SelectNodes(string path, bool includeSubNodes = false);
        INodeCollection Clone();
        JToken ToJson();
        bool LoadFrom(JArray jsonArray);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNodeList xmlNodeList);
    }
}