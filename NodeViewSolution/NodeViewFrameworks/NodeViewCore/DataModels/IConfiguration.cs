﻿using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public interface IConfiguration : ITreeNode
    {

    }

    public static class ConfigurationExtensions
    {
        public static bool TryGetConfiguration(this IConfiguration configuration, string path, out IConfiguration foundConfiguration)
        {
            if (configuration.TryGetNode(path, out ITreeNode node))
            {
                foundConfiguration = (IConfiguration)node;
                return true;
            }

            foundConfiguration = null;
            return false;
        }
    }
}
