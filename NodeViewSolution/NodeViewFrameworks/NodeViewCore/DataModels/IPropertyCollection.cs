﻿using System.Collections.Generic;

namespace NodeView.DataModels
{
    public interface IPropertyCollection : IEnumerable<IProperty>
    {
        int Count { get; }
        IProperty this[string id] { get; }
        bool TryGetProperty(string id, out IProperty property);
        string[] Ids { get; }
        bool Contains(string id);
        T GetValue<T>(string id, T defaultValue = default(T));
        string GetStringValue(string id, string defaultValue = "");
    }
}