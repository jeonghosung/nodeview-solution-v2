﻿namespace NodeView.DataModels
{
    public enum ProcessRunningStatus
    {
        None,
        Running,
        Stopped
    }
}