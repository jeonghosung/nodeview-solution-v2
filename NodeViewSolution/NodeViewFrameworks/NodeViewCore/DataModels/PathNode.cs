﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeView.DataModels
{
    public class PathNode : IPathNode
    {
        private readonly Dictionary<string, INode> _dictionary = new Dictionary<string, INode>();

        public PathNode(string path)
        {
            Path = path ?? "";
        }

        public string Path { get; set; }
        public IEnumerable<INode> Nodes => _dictionary.Values;
        public int Count => _dictionary.Count;
        public string[] Ids => _dictionary.Keys.ToArray();

        public bool Contains(string id)
        {
            return _dictionary.ContainsKey(id);
        }
        
        public INode this[string id]
        {
            get
            {
                if (TryGetNode(id, out var node))
                {
                    return node;
                }

                return null;
            }
        }

        public bool TryGetNode(string id, out INode node)
        {
            return _dictionary.TryGetValue(id, out node);
        }

        #region set/remove/clear

        public bool Set(INode node)
        {
            if (string.IsNullOrWhiteSpace(node?.Id)) return false;

            // NodeCollection에서 패스 정리 하고 와야 한다.
            //node.Path = Path;
            _dictionary[node.Id] = node;
            return true;
        }

        public bool Sets(IEnumerable<INode> nodes)
        {
            if (nodes == null) return false;

            foreach (var node in nodes)
            {
                Set(node);
            }

            return true;
        }

        public bool Remove(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            return _dictionary.Remove(id);
        }

        public void Clear()
        {
            _dictionary.Clear();
        }
        #endregion
    }
}
