﻿using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class NodeViewCommand: INodeViewSerialize
    {
        public string AppId { get; set; }
        public string DeviceId { get; set; }
        public string Command { get; set; }
        public KeyValueCollection Parameters { get; set; } = new KeyValueCollection();

        public NodeViewCommand(string appId = "", string deviceId = "", string command = "", KeyValueCollection parameters = null)
        {
            AppId = appId;
            DeviceId = deviceId;
            Command = command;
            if (parameters != null)
            {
                Parameters.Sets(parameters);
            }
        }

        public NodeViewCommand(string deviceId, string command, KeyValueCollection parameters = null) : this("", deviceId, command, parameters)
        {

        }

        public JObject GetCommandRequestJson()
        {
            JObject json = new JObject();
            json["action"] = Command;
            var paramJson = new JObject();
            foreach (var parameter in Parameters)
            {
                paramJson[parameter.Key] = $"{parameter.Value}";
            }
            json["param"] = paramJson;
            return json;
        }

        public NodeViewCommand Clone()
        {
            var command = new NodeViewCommand(AppId, DeviceId, Command, Parameters);
            return command;
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            if (!string.IsNullOrWhiteSpace(AppId))
            {
                json["appId"] = AppId;
            }
            json["deviceId"] = DeviceId;
            json["command"] = Command;
            var paramJson = new JObject();
            foreach (var parameter in Parameters)
            {
                paramJson[parameter.Key] = $"{parameter.Value}";
            }
            json["param"] = paramJson;
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            AppId = JsonUtils.GetValue(json, "appId", AppId);
            DeviceId = JsonUtils.GetValue(json, "deviceId", DeviceId);
            Command = JsonUtils.GetValue(json, "command", Command);
            var paramJson = JsonUtils.GetValue(json, "param", new JObject());
            foreach (var paramProperty in paramJson.Properties())
            {
                Parameters[paramProperty.Name] = $"{paramProperty.Value}";
            }

            if (string.IsNullOrWhiteSpace(DeviceId) || string.IsNullOrWhiteSpace(Command))
            {
                return false;
            }

            return true;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Command" : tagName;
            xmlWriter.WriteStartElement(tagName);
            if (!string.IsNullOrWhiteSpace(AppId))
            {
                xmlWriter.WriteAttributeString("appId", AppId);
            }
            xmlWriter.WriteAttributeString("deviceId", DeviceId);
            xmlWriter.WriteAttributeString("command", Command);

            xmlWriter.WriteStartElement("Param");
            foreach (var parameter in Parameters)
            {
                xmlWriter.WriteAttributeString(parameter.Key, $"{parameter.Value}");
            }
            xmlWriter.WriteEndElement();
            
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            AppId = XmlUtils.GetAttrValue(xmlNode, "appId", AppId);
            DeviceId = XmlUtils.GetAttrValue(xmlNode, "deviceId", DeviceId);
            Command = XmlUtils.GetAttrValue(xmlNode, "command", Command);

            XmlNode paramNode = xmlNode.SelectSingleNode("Param");
            if (paramNode?.Attributes != null)
            {
                foreach (XmlAttribute attribute in paramNode.Attributes)
                {
                    Parameters[attribute.Name] = $"{attribute.Value}";
                }
            }

            if (string.IsNullOrWhiteSpace(DeviceId) || string.IsNullOrWhiteSpace(Command))
            {
                return false;
            }

            return true;
        }

        public static NodeViewCommand CreateFrom(JObject json)
        {
            if (json == null) return null;
            var command = new NodeViewCommand();
            if (command.LoadFrom(json))
            {
                return command;
            }

            return null;
        }
        public static NodeViewCommand CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return null;
            var command = new NodeViewCommand();
            if (command.LoadFrom(xmlNode))
            {
                return command;
            }

            return null;
        }
    }
}