﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class Configuration : TreeNodeBase, IConfiguration
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public Configuration(string id = "")
        {
            Id = id;
        }

        public bool TryGetConfig(string path, out Configuration config)
        {
            if (TryGetNode(path, out var node))
            {
                if (node is Configuration configuration)
                {
                    config = configuration;
                    return true;
                }
            }

            config = null;
            return false;
        }

        public override INode CreateNode()
        {
            return new Configuration();
        }

        public static Configuration CreateFrom(JObject json)
        {
            Configuration configuration = new Configuration();
            configuration.LoadFrom(json);
            return configuration;
        }

        public static Configuration CreateFrom(XmlNode xmlNode)
        {
            Configuration configuration = new Configuration();
            configuration.LoadFrom(xmlNode);
            return configuration;
        }

        public static Configuration CreateFrom(IConfiguration config)
        {
            if (config == null) return null;

            Configuration configuration = new Configuration();
            configuration.LoadFrom(config);
            return configuration;
        }
        public static Configuration Load(string filePath)
        {
            try
            {
                string configFileName = filePath;
                if (!File.Exists(configFileName))
                {
                    configFileName = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, filePath);
                    if (!File.Exists(configFileName))
                    {
                        Logger.Error($"Cannot find '{filePath}' file.");
                        return null;
                    }
                }

                XmlDocument doc = new XmlDocument();
                doc.Load(filePath);
                Configuration configuration = CreateFrom(doc.DocumentElement);
                return configuration;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Load('{filePath}'):");
            }

            return null;
        }
        public static bool Save(string filePath, IConfiguration configuration)
        {
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(filePath))
                {
                    configuration.WriteXml(xmlWriter);
                    xmlWriter.Close();
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Save '{filePath}':");
            }

            return false;
        }
    }
}
