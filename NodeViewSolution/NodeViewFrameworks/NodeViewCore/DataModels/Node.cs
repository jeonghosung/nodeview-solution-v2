﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public class Node : NodeBase
    {
        public override INode CreateNode()
        {
            return new Node();
        }

        public static Node CreateFrom(INode sourceNode)
        {
            Node node = new Node();
            node.LoadFrom(sourceNode);
            return node;
        }

        public static Node CreateFrom(JObject json)
        {
            Node node = new Node();
            node.LoadFrom(json);
            return node;
        }

        public static Node CreateFrom(XmlNode xmlNode)
        {
            Node node = new Node();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}
