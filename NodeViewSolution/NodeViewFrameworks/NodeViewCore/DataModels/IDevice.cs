﻿namespace NodeView.DataModels
{
    public interface IDevice : INodeObject, INodeViewSerialize
    {
        string DeviceType { get;}
        string Name { get; set; }
        string Description { get; set; }
        string ConnectionString { get; set; }
        IKeyValueCollection Attributes { get; }
        CommandDescription[] CommandDescriptions { get; }
        bool RunCommand(string command, KeyValueCollection parameters);

    }
}
