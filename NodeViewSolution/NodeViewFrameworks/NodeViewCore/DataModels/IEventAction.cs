﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeView.DataModels
{
    public interface IEventAction : IObject, INodeViewSerialize
    {
        AlarmEventLevel Level { get; }
        string Description { get; }
        string EventOnActionId { get; }
        string EventOffActionId { get; }
    }
}
