﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using NLog;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class KeyValueCollection : IKeyValueCollection
    {
        private readonly Dictionary<string, IKeyValuePair> _dictionary = new Dictionary<string, IKeyValuePair>();
        public int Count => _dictionary.Count;

        public KeyValueCollection()
        {
        }
        public KeyValueCollection(IEnumerable<IKeyValuePair> keyValuePairs) :this()
        {
            if (keyValuePairs == null)
            {
                return;
            }

            foreach (var keyValuePair in keyValuePairs)
            {
                if (!string.IsNullOrWhiteSpace(keyValuePair.Key))
                {
                    _dictionary[keyValuePair.Key] = new KeyValuePair(keyValuePair.Key, keyValuePair.Value);
                }
            }
        }

        public IEnumerator<IKeyValuePair> GetEnumerator()
        {
            return _dictionary.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public object this[string key]
        {
            get
            {
                if (_dictionary.TryGetValue(key, out var keyValue))
                {
                    return $"{keyValue.Value}";
                }

                return "";
            }
            set
            {
                if (!string.IsNullOrWhiteSpace(key))
                {
                    Set(key, value);
                }
            }
        }

        public bool TryGetValue(string key, out object value)
        {
            bool res = _dictionary.TryGetValue(key, out var keyValue);

            value = res ? keyValue.Value : "";
            return res;
        }

        public string[] Keys => _dictionary.Keys.ToArray();
        public bool Contains(string key)
        {
            return _dictionary.ContainsKey(key);
        }

        public T GetValue<T>(string key, T defaultValue)
        {
            if (TryGetValue(key, out var value))
            {
                return StringUtils.GetValue(value, defaultValue);
            }

            return defaultValue;
        }

        public string GetStringValue(string key, string defaultValue = "")
        {
            return GetValue(key, defaultValue);
        }

        #region set/remove/clear
        public bool Set(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null)
            {
                return false;
            }
            _dictionary[key] = new KeyValuePair(key, value);
            return true;
        }

        public bool Set(IKeyValuePair keyValuePair)
        {
            return Set(keyValuePair.Key, keyValuePair.Value);
        }
        public void Sets(IEnumerable<IKeyValuePair> keyValuePairs)
        {
            if (keyValuePairs == null)
            {
                return;
            }

            foreach (var keyValuePair in keyValuePairs)
            {
                Set(keyValuePair.Key, keyValuePair.Value);
            }
        }

        public bool Remove(string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return false;
            }
            return _dictionary.Remove(key);
        }
        public void Clear()
        {
            _dictionary.Clear();
        }
        #endregion

        public string ToQueryString()
        {
            List<string> keyValueStringList = new List<string>();

            foreach (var keyValuePair in _dictionary.Values)
            {
                keyValueStringList.Add($"{HttpUtility.UrlEncode(keyValuePair.Key)}={HttpUtility.UrlEncode(keyValuePair.GetValue(""))}");
            }

            return string.Join("&", keyValueStringList);
        }

        public static KeyValueCollection CreateFromQueryString(string queryString)
        {
            var collection = new KeyValueCollection();
            if (string.IsNullOrWhiteSpace(queryString)) return collection;

            try
            {
                var nameValueCollection = HttpUtility.ParseQueryString(queryString);
                foreach (string key in nameValueCollection.Keys)
                {
                    try
                    {
                        collection.Set(key, nameValueCollection.Get(key).Trim());
                    }
                    catch
                    {
                        //skip
                    }
                }
            }
            catch
            {
                //skip
            }
            return collection;
        }

        public static KeyValueCollection CreateFrom(NameValueCollection nameValueCollection)
        {
            var collection = new KeyValueCollection();
            if (nameValueCollection != null)
            {
                foreach (string key in nameValueCollection.Keys)
                {
                    try
                    {
                        collection[key] = nameValueCollection.Get(key)?.Trim() ?? "";
                    }
                    catch
                    {
                        //Ignore;
                    }
                }

            }
            return collection;
        }
    }
}
