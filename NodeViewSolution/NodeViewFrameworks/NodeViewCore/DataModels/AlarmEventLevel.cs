﻿namespace NodeView.DataModels
{
    public enum AlarmEventLevel
    {
        Normal,
        Warn,
        Alarm
    }
}