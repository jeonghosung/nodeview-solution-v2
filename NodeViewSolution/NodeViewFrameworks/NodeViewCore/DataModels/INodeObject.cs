﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeView.DataModels
{
    public interface INodeObject : IObject
    {
        string PathId { get;}
        string Path { get; set; }
        NodeStatus Status { get; }
        string StatusMessage { get; }
        string Updated { get; }
    }
}
