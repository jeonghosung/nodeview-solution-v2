﻿namespace NodeView.DataModels
{
    public enum NodeStatus
    {
        None = 0,
        Normal,
        Warn,
        Alarm,
        Fault // Client관점에서 오류 (연결 오류, 등록정보 오류등)
    }
}