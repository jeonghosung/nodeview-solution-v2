﻿using System.Collections.Generic;

namespace NodeView.DataModels
{
    public interface IPathNode
    {
        string Path { get; set; }
        IEnumerable<INode> Nodes { get; }
        int Count { get; }
        string[] Ids { get; }
        bool Contains(string id);
        INode this[string id] { get; }
        bool TryGetNode(string id, out INode node);
    }
}
