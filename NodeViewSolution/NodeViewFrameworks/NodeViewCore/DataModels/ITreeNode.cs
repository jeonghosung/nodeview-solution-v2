﻿using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public interface ITreeNode : INode
    {
        IEnumerable<ITreeNode> ChildNodes { get; }
        bool TryGetNode(string path, out ITreeNode node);
        bool SetChildNode(ITreeNode node);
        bool RemoveChildNode(string id);
        void Clear();
    }
}
