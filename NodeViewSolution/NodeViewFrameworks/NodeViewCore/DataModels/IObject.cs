﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeView.DataModels
{
    public interface IObject
    {
        string Id { get; }
    }
}
