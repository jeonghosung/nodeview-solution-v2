﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class NodeViewEvent : IObject, INodeViewSerialize
    {
        public string Id { get; set; } = "";
        public bool IsOn { get; set; } = true;
        public string Requester { get; set; } = "";
        public string[] Targets { get; set; } = new string[0];
        public string Message { get; set; } = "";
        public string Param { get; set; } = "";
        public KeyValueCollection Parameters { get; } = new KeyValueCollection();

        public NodeViewEvent(string id = "", bool isOn = true, string message = "", string param = "", IEnumerable<IKeyValuePair> parameters = null)
        {
            Id = id;
            IsOn = isOn;
            Message = message;
            Param = param;
            if (parameters != null)
            {
                Parameters.Sets(parameters);
            }
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["requester"] = Requester;
            json["isOn"] = IsOn;
            json["targets"] = Targets.Length == 0 ? "" : string.Join(",", Targets);
            json["message"] = Message;
            json["param"] = Param;
            foreach (var parameter in Parameters)
            {
                json[parameter.Key] = $"{parameter.Value}";
            }
            
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            foreach (var objectProperty in json.Properties())
            {
                switch (objectProperty.Name)
                {
                    case "id":
                        Id = StringUtils.GetValue(objectProperty.Value.Value<object>(), "");
                        break;
                    case "isOn":
                        IsOn = StringUtils.GetValue(objectProperty.Value.Value<object>(), true);
                        break;
                    case "requester":
                        Requester = StringUtils.GetValue(objectProperty.Value.Value<object>(), "");
                        break;
                    case "targets":
                        Targets = StringUtils.GetValue(objectProperty.Value.Value<object>(), "").Split(',');
                        break;
                    case "message":
                        Message = StringUtils.GetValue(objectProperty.Value.Value<object>(), "");
                        break;
                    case "param":
                        Param = StringUtils.GetValue(objectProperty.Value.Value<object>(), "");
                        break;
                    default:
                        Parameters.Set(objectProperty.Name, $"{objectProperty.Value}");
                        break;
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Event" : tagName;
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id );
            xmlWriter.WriteAttributeString("isOn", $"{IsOn}");
            xmlWriter.WriteAttributeString("requester", Requester);
            xmlWriter.WriteAttributeString("targets", Targets.Length == 0 ? "" : string.Join(",", Targets));
            xmlWriter.WriteAttributeString("message", Message);
            xmlWriter.WriteAttributeString("param", Param);
            foreach (var parameter in Parameters)
            {
                xmlWriter.WriteAttributeString("parameter.Key", $"{parameter.Value}");
            }
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    switch (attribute.Name)
                    {
                        case "id":
                            Id = StringUtils.GetValue(attribute.Value, "");
                            break;
                        case "isOn":
                            IsOn = StringUtils.GetValue(attribute.Value, true);
                            break;
                        case "requester":
                            Requester = StringUtils.GetValue(attribute.Value, "");
                            break;
                        case "targets":
                            Targets = StringUtils.GetValue(attribute.Value, "").Split(',');
                            break;
                        case "message":
                            Message = StringUtils.GetValue(attribute.Value, "");
                            break;
                        case "param":
                            Param = StringUtils.GetValue(attribute.Value, "");
                            break;
                        default:
                            Parameters.Set(attribute.Name, $"{attribute.Value}");
                            break;
                    }
                }
            }
            
            return !string.IsNullOrWhiteSpace(Id);
        }

        public static NodeViewEvent CreateFrom(JObject json)
        {
            var alarmEvent = new NodeViewEvent();
            alarmEvent.LoadFrom(json);
            return alarmEvent;
        }

        public static NodeViewEvent CreateFrom(XmlNode xmlNode)
        {
            var alarmEvent = new NodeViewEvent();
            alarmEvent.LoadFrom(xmlNode);
            return alarmEvent;
        }

        public NodeViewEvent Clone()
        {
            var nodeViewEvent = new NodeViewEvent();
            nodeViewEvent.Id = Id;
            nodeViewEvent.IsOn = IsOn;
            nodeViewEvent.Requester = Requester;
            nodeViewEvent.Targets = Targets.ToArray();
            nodeViewEvent.Message = Message;
            nodeViewEvent.Param = Param;
            nodeViewEvent.Parameters.Sets(Parameters);
            return nodeViewEvent;
        }
    }
}
