﻿using System.Collections.Generic;

namespace NodeView.DataModels
{
    public interface IProperty :INodeViewSerialize
    {
        string Id { get; }
        string Value { get; set; }
        IKeyValueCollection Attributes { get; }
        object this[string key] { get; }
        T GetValue<T>(T defaultValue);
        string GetStringValue(string defaultValue = "");
        T GetAttrValue<T>(string key, T defaultValue);
        bool SetAttribute(string key, object value);
        bool SetAttribute(IKeyValuePair keyValuePair);
        void SetAttributes(IEnumerable<IKeyValuePair> keyValuePairs);
        bool RemoveAttribute(string key);
        void ClearAttributes();
        IProperty Clone();
        bool LoadFrom(IProperty sourceProperty);
    }
}
