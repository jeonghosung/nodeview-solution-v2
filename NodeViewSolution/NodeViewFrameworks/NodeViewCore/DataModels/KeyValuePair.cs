﻿using NodeView.Utils;

namespace NodeView.DataModels
{
    public class KeyValuePair : IKeyValuePair
    {
        public string Key { get; set; }
        public object Value { get; set; }

        public KeyValuePair(string key, object value)
        {
            Key = key;
            Value = value;
        }
        public KeyValuePair(): this("", null)
        {
        }
        public KeyValuePair(IKeyValuePair keyValuePair) : this(keyValuePair.Key, keyValuePair.Value)
        {
        }


        public T GetValue<T>(T defaultValue = default(T))
        {
            return StringUtils.GetValue(Value, defaultValue);
        }
    }
}
