﻿using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataModels
{
    public interface INodeViewSerialize
    {
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
