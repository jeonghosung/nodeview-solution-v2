﻿using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public abstract class DeviceBase : IDevice
    {
        private CommandDescription[] _commandDescriptions = null;
        protected KeyValueCollection _attributes = new KeyValueCollection();

        public string Id { get; set; } = "";
        public string PathId => string.IsNullOrWhiteSpace(Path) ? Id : $"{Path}/{Id}";
        public string Path { get; set; } = "";
        public NodeStatus Status { get; set; } = NodeStatus.None;
        public string StatusMessage { get; set; } = "";
        public string Updated { get; set; } = "";
        public string DeviceType { get; set; } = "device";
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string ConnectionString { get; set; } = "";

        public IKeyValueCollection Attributes => _attributes;

        public CommandDescription[] CommandDescriptions
        {
            get => _commandDescriptions ?? (_commandDescriptions = GetCommandDescriptions());
            protected set => _commandDescriptions = value;
        }

        protected virtual CommandDescription[] GetCommandDescriptions()
        {
            return new CommandDescription[0];
        }
        public virtual INode[] GetNodes()
        {
            return new INode[0];
        }

        public virtual bool RunCommand(string command, KeyValueCollection parameters)
        {
            return false;
        }

        protected virtual bool SetAttribute(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;
            switch (key)
            {
                case "id":
                    Id = $"{value}";
                    break;
                case "path":
                    Path = $"{value}";
                    break;
                case "status":
                    Status = StringUtils.GetValue(value, Status);
                    break;
                case "statusMessage":
                    StatusMessage = $"{value}";
                    break;
                case "updated":
                    Updated = $"{value}";
                    break;
                case "deviceType":
                    DeviceType = $"{value}";
                    break;
                case "name":
                    Name = $"{value}";
                    break;
                case "description":
                    Description = $"{value}";
                    break;
                case "connectionString":
                    ConnectionString = $"{value}";
                    break;
                case "propertyDescriptions":
                case "commandDescriptions":
                    break;
                default:
                    _attributes.Set(key, value);
                    break;
            }

            return true;
        }

        public virtual JToken ToJson()
        {
            JObject json = new JObject();
            WriteBodyJson(json);
            return json;
        }

        protected virtual void WriteBodyJson(JObject json)
        {
            json["id"] = Id;

            if (!string.IsNullOrWhiteSpace(Path)) json["path"] = Path;
            if (Status != NodeStatus.None) json["status"] = $"{Status}";
            if (!string.IsNullOrWhiteSpace(StatusMessage)) json["statusMessage"] = StatusMessage;
            if (!string.IsNullOrWhiteSpace(Updated)) json["updated"] = Updated;

            json["deviceType"] = DeviceType;
            json["name"] = Name;
            json["description"] = Description;
            json["connectionString"] = ConnectionString;
            WriteAttributesJson(json);

            JArray commandDescriptionJsonArray = new JArray();
            foreach (var commandDescription in CommandDescriptions)
            {
                commandDescriptionJsonArray.Add(commandDescription.ToJson());
            }
            json["commandDescriptions"] = commandDescriptionJsonArray;
        }
        protected virtual void WriteAttributesJson(JObject json)
        {
            var attributes = _attributes.ToArray();
            foreach (var attribute in attributes)
            {
                json[attribute.Key] = $"{attribute.Value}";
            }
        }
        public virtual bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            foreach (var objectProperty in json.Properties())
            {
                switch (objectProperty.Name)
                {
                    case "commandDescriptions":
                        JArray commandDescriptionJsonArray = objectProperty.Value.Value<JArray>();
                        List<CommandDescription> commandDescriptions = new List<CommandDescription>();
                        if (commandDescriptionJsonArray != null)
                        {
                            foreach (var commandDescriptionToken in commandDescriptionJsonArray)
                            {
                                var commandDescription = CommandDescription.CreateFrom(commandDescriptionToken.Value<JObject>());
                                if (commandDescription != null)
                                {
                                    commandDescriptions.Add(commandDescription);
                                }
                            }
                        }
                        _commandDescriptions = commandDescriptions.ToArray();
                        break;
                    default:
                        if (objectProperty.Value.Type != JTokenType.Array)
                        {
                            SetAttribute(objectProperty.Name, $"{objectProperty.Value}");
                        }
                        break;
                }
            }
            
            return !string.IsNullOrWhiteSpace(Id);
        }

        public virtual void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Device" : tagName;
            xmlWriter.WriteStartElement(tagName);

            WriteBodyXml(xmlWriter);
            
            xmlWriter.WriteEndElement();
        }

        protected virtual void WriteBodyXml(XmlWriter xmlWriter)
        {
            xmlWriter.WriteAttributeString("id", Id);

            if (!string.IsNullOrWhiteSpace(Path)) xmlWriter.WriteAttributeString("path", Path);
            if (Status != NodeStatus.None) xmlWriter.WriteAttributeString("status", $"{Status}");
            if (!string.IsNullOrWhiteSpace(StatusMessage)) xmlWriter.WriteAttributeString("statusMessage", StatusMessage);
            if (!string.IsNullOrWhiteSpace(Updated)) xmlWriter.WriteAttributeString("updated", Updated);

            xmlWriter.WriteAttributeString("deviceType", DeviceType);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("description", Description);
            xmlWriter.WriteStartElement("ConnectionString", ConnectionString);
            WriteAttributeXml(xmlWriter);
            if (CommandDescriptions.Length > 0)
            {
                xmlWriter.WriteStartElement("CommandDescriptions");
                foreach (var commandDescription in CommandDescriptions)
                {
                    commandDescription.WriteXml(xmlWriter, "Command");
                }
                xmlWriter.WriteEndElement();
            }
        }

        protected virtual void WriteAttributeXml(XmlWriter xmlWriter)
        {
            var attributes = _attributes.ToArray();
            foreach (var attribute in attributes)
            {
                xmlWriter.WriteAttributeString(attribute.Key, $"{attribute.Value}");
            }
        }

        public virtual bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    SetAttribute(attribute.Name, attribute.Value);
                }
            }

            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            XmlNodeList commandDescriptionNodes = xmlNode.SelectNodes("CommandDescriptions/Command");
            if (commandDescriptionNodes != null)
            {
                foreach (XmlNode commandDescriptionNode in commandDescriptionNodes)
                {
                    var commandDescription = CommandDescription.CreateFrom(commandDescriptionNode);
                    if (commandDescription != null)
                    {
                        commandDescriptions.Add(commandDescription);
                    }
                }
            }

            _commandDescriptions = commandDescriptions.ToArray();

            return !string.IsNullOrWhiteSpace(Id);
        }
    }
}
