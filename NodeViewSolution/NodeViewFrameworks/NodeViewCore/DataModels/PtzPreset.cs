﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeViewCore.DataModels
{
    public class PtzPreset
    {
        public int Id { get; }
        public string Name { get; }
        public PtzPreset(int id, string name)
        {
            Id = id;
            Name = name;
        }

    }
}
