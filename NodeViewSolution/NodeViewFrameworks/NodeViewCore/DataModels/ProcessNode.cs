﻿using System;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public class ProcessNode : IObject, INodeViewSerialize
    {
        public string Id { get; set; } = ""; //AppKey
        public string AppType { get; set; } = "";
        public string Name { get; set; } = ""; //ProcessName
        public string StopApiUrl { get; set; } = "";
        public ProcessType ProcessType { get; set; } = ProcessType.Application;
        public ProcessRunningStatus Status { get; set; } = ProcessRunningStatus.None;
        public string WorkingFolder { get; set; } = "";
        public string Execution { get; set; } = "";
        public string Argument { get; set; } = "";
        public string ConfigFilename { get; set; } = "";
        public string Updated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);

        public void Update(ProcessNode source)
        {
            Name = StringUtils.GetValue(source.Name, Name);
            StopApiUrl = StringUtils.GetValue(source.StopApiUrl, StopApiUrl);
            ProcessType = StringUtils.GetValue(source.ProcessType, ProcessType);
            Status = ProcessRunningStatus.Running;
            WorkingFolder = StringUtils.GetValue(source.WorkingFolder, WorkingFolder);
            Execution = StringUtils.GetValue(source.Execution, Execution);
            Argument = StringUtils.GetValue(source.ConfigFilename, Argument);
            ConfigFilename = StringUtils.GetValue(source.ConfigFilename, ConfigFilename);
            Updated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        public JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            json["appType"] = AppType;
            json["name"] = Name;
            json["stopApiUrl"] = StopApiUrl;
            json["processType"] = $"{ProcessType}";
            json["status"] = $"{Status}";
            json["workingFolder"] = WorkingFolder;
            json["execution"] = Execution;
            json["argument"] = Argument;
            json["configFilename"] = ConfigFilename;
            json["updated"] = Updated;
            return json;
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Id = JsonUtils.GetValue(json, "id", Id);
            AppType = JsonUtils.GetValue(json, "appType", AppType);
            Name = JsonUtils.GetValue(json, "name", Name);
            StopApiUrl = JsonUtils.GetValue(json, "stopApiUrl", StopApiUrl);
            ProcessType = JsonUtils.GetValue(json, "processType", ProcessType);
            Status = JsonUtils.GetValue(json, "status", Status);
            WorkingFolder = JsonUtils.GetValue(json, "workingFolder", WorkingFolder);
            Execution = JsonUtils.GetValue(json, "execution", Execution);
            Argument = JsonUtils.GetValue(json, "argument", Argument);
            ConfigFilename = JsonUtils.GetValue(json, "configFilename", ConfigFilename);
            Updated = JsonUtils.GetValue(json, "updated", Updated);

            return !(string.IsNullOrWhiteSpace(Id));
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "Process" : tagName;
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("appType", AppType);
            xmlWriter.WriteAttributeString("stopApiUrl", StopApiUrl);
            xmlWriter.WriteAttributeString("processType", $"{ProcessType}");
            xmlWriter.WriteAttributeString("status", $"{Status}");
            xmlWriter.WriteAttributeString("workingFolder", WorkingFolder);
            xmlWriter.WriteAttributeString("execution", Execution);
            xmlWriter.WriteAttributeString("argument", Argument);
            xmlWriter.WriteAttributeString("configFilename", ConfigFilename);
            xmlWriter.WriteAttributeString("updated", Updated);

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Id = XmlUtils.GetAttrValue(xmlNode, "id", Id);
            AppType = XmlUtils.GetAttrValue(xmlNode, "appType", AppType);
            Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
            StopApiUrl = XmlUtils.GetAttrValue(xmlNode, "stopApiUrl", StopApiUrl);
            ProcessType = XmlUtils.GetAttrValue(xmlNode, "processType", ProcessType);
            Status = XmlUtils.GetAttrValue(xmlNode, "status", Status);
            WorkingFolder = XmlUtils.GetAttrValue(xmlNode, "workingFolder", WorkingFolder);
            Execution = XmlUtils.GetAttrValue(xmlNode, "execution", Execution);
            Argument = XmlUtils.GetAttrValue(xmlNode, "argument", Argument);
            ConfigFilename = XmlUtils.GetAttrValue(xmlNode, "configFilename", ConfigFilename);
            Updated = XmlUtils.GetAttrValue(xmlNode, "updated", Updated);

            return !(string.IsNullOrWhiteSpace(Id));
        }

        public static ProcessNode CreateFrom(JObject json)
        {
            var node = new ProcessNode();
            node.LoadFrom(json);
            return node;
        }

        public static ProcessNode CreateFrom(XmlNode xmlNode)
        {
            var node = new ProcessNode();
            node.LoadFrom(xmlNode);
            return node;
        }
    }
}