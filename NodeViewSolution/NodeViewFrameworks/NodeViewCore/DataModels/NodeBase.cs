﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Microsoft.SqlServer.Server;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.DataModels
{
    public abstract class NodeBase : INode
    {
        protected KeyValueCollection _attributes = new KeyValueCollection();
        protected PropertyCollection _properties = new PropertyCollection();
        public string Id { get; set; } = "";
        public string PathId => string.IsNullOrWhiteSpace(Path) ? Id : $"{Path}/{Id}";
        public string Path { get; set; } = "";
        public string NodeType { get; set; } = "";
        public NodeStatus Status { get; set; } = NodeStatus.None;
        public string StatusMessage { get; set; } = "";
        public string Updated { get; set; } = ""; //StringUtils.GetCurrentTimeMillisecondString(); 고민좀 해보자.
        public IKeyValueCollection Attributes
        {
            get => _attributes;
        }

        public IPropertyCollection Properties
        {
            get => _properties;
        }

        public object this[string search]
        {
            get
            {
                if (TryGetObject(search, out var @object))
                {
                    return @object;
                }

                return null;
            }
        }

        public bool Contains(string search)
        {
            if (TryGetObject(search, out var @object))
            {
                return true;
            }

            return false;
        }

        public virtual bool TryGetObject(string search, out object @object)
        {
            @object = null;
            if (string.IsNullOrWhiteSpace(search)) return false;

            if (search.StartsWith("@"))
            {
                string attrKey = search.Substring(1);
                if (string.IsNullOrWhiteSpace(attrKey)) return false;

                switch (attrKey)
                {
                    case "id":
                        @object = Id;
                        return true;
                    case "path":
                        @object = Path;
                        return true;
                    case "nodeType":
                        @object = NodeType;
                        return true;
                    case "status":
                        @object = Status;
                        return true;
                    case "statusMessage":
                        @object = StatusMessage;
                        return true;
                    case "updated":
                        @object = Updated;
                        return true;
                }

                if (Attributes.TryGetValue(attrKey, out var value))
                {
                    @object = value;
                    return true;
                }
            }
            else
            {
                if (Properties.TryGetProperty(search, out var property))
                {
                    @object = property;
                    return true;
                }
            }

            return false;
        }

        public T GetValue<T>(string search, T defaultValue)
        {
            return StringUtils.GetValue(GetStringValue(search), defaultValue);
        }

        public string GetStringValue(string search, string defaultValue = "")
        {
            string foundValueString = "";
            if (TryGetObject(search, out var @object))
            {
                if (@object is IPathNode path)
                {
                    foundValueString = $"{path.Path}";
                }
                else if (@object is INode node)
                {
                    foundValueString = $"{node.Path}/{node.Id}:{node.NodeType}";
                }
                else if (@object is IProperty property)
                {
                    foundValueString = property.Value;
                }
                else
                {
                    foundValueString = $"{@object}";
                }
            }

            return foundValueString;
        }
        public virtual bool SetAttribute(string key, object value)
        {
            if (string.IsNullOrWhiteSpace(key) || value == null) return false;
            switch (key)
            {
                case "id":
                    Id = $"{value}";
                    break;
                case "path":
                    Path = $"{value}";
                    break;
                case "nodeType":
                    NodeType = $"{value}";
                    break;
                case "status":
                    Status = StringUtils.GetValue(value, Status);
                    break;
                case "statusMessage":
                    StatusMessage = $"{value}";
                    break;
                case "updated":
                    Updated = $"{value}";
                    break;
                case "properties":
                    break;
                default:
                    _attributes.Set(key, value);
                    break;
            }

            return true;
        }

        public bool SetAttribute(IKeyValuePair keyValuePair)
        {
            return SetAttribute(keyValuePair.Key, keyValuePair.Value);
        }

        public void SetAttributes(IEnumerable<IKeyValuePair> keyValuePairs)
        {
            _attributes.Sets(keyValuePairs);
        }

        public bool RemoveAttribute(string key)
        {
            return _attributes.Remove(key);
        }

        public void ClearAttributes()
        {
            _attributes.Clear();
        }

        public bool SetProperty(string id, string value)
        {
            IProperty property = new Property()
            {
                Id = id,
                Value = value
            };
            return SetProperty(property);
        }

        public virtual bool SetProperty(IProperty property)
        {
            return _properties.Set(property);
        }
        public void SetProperties(IEnumerable<IProperty> properties)
        {
            _properties.Sets(properties);
        }
        public bool RemoveProperty(string id)
        {
            return _properties.Remove(id);
        }

        public void ClearProperties()
        {
            _properties.Clear();
        }

        public void Update(INode source)
        {
            if (source == null || PathId != source.PathId) return;

            Status = source.Status;
            StatusMessage = source.StatusMessage;
            Updated = source.Updated;
            ClearAttributes();
            SetAttributes(source.Attributes);
            ClearProperties();
            foreach (var property in source.Properties)
            {
                SetProperty(property.Clone());
            }
        }
        public void Patch(INode source)
        {
            if (source == null || PathId != source.PathId) return;

            SetAttributes(source.Attributes);
            foreach (var property in source.Properties)
            {
                SetProperty(property.Clone());
            }
        }

        public INode Clone()
        {
            var node = CreateNode();
            Clone(node);
            return node;
        }
        protected virtual void Clone(INode node)
        {
            node?.LoadFrom((INode)this);
        }
        public abstract INode CreateNode();

        public virtual bool LoadFrom(INode sourceNode)
        {
            if (sourceNode == null) return false;

            Id = sourceNode.Id;
            Path = sourceNode.Path;
            NodeType = sourceNode.NodeType;
            Status = sourceNode.Status;
            StatusMessage = sourceNode.StatusMessage;
            Updated = sourceNode.Updated;
            SetAttributes(sourceNode.Attributes);
            foreach (var property in sourceNode.Properties)
            {
                SetProperty(property.Clone());
            }

            return !string.IsNullOrWhiteSpace(Id);
        }

        public virtual JToken ToJson()
        {
            JObject json = new JObject();
            json["id"] = Id;
            if (!string.IsNullOrWhiteSpace(Path)) json["path"] = Path;
            if (Status != NodeStatus.None) json["status"] = $"{Status}";
            if (!string.IsNullOrWhiteSpace(StatusMessage)) json["statusMessage"] = StatusMessage;
            if (!string.IsNullOrWhiteSpace(Updated)) json["updated"] = Updated;
            json["nodeType"] = NodeType;

            foreach (var attribute in Attributes)
            {
                json[attribute.Key] = $"{attribute.Value}";
            }

            JArray properties = new JArray();
            foreach (var property in Properties)
            {
                properties.Add(property.ToJson());
            }

            json["properties"] = properties;

            return json;
        }

        public virtual bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            foreach (var objectProperty in json.Properties())
            {
                switch (objectProperty.Name)
                {
                    case "properties":
                        JArray propertyArray = objectProperty.Value.Value<JArray>();
                        if (propertyArray != null)
                        {
                            foreach (var propertyToken in propertyArray)
                            {
                                JObject propertyJson = propertyToken.Value<JObject>();
                                Property property = Property.CreateFrom(propertyJson);
                                if (property != null && !string.IsNullOrWhiteSpace(property.Id))
                                {
                                    SetProperty(property);
                                }
                            }
                        }
                        break;
                    default:
                        if (objectProperty.Value.Type != JTokenType.Array)
                        {
                            SetAttribute(objectProperty.Name, $"{objectProperty.Value}");
                        }
                        break;
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }

        public virtual void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = string.IsNullOrWhiteSpace(NodeType) ? "Node" : NodeType;
            }
            xmlWriter.WriteStartElement(tagName);
            if (tagName != NodeType)
            {
                xmlWriter.WriteAttributeString("nodeType", NodeType);
            }
            WriteXmlBody(xmlWriter);
            xmlWriter.WriteEndElement();
        }
        protected virtual void WriteXmlBody(XmlWriter xmlWriter)
        {
            xmlWriter.WriteAttributeString("id", Id);
            if (!string.IsNullOrWhiteSpace(Path)) xmlWriter.WriteAttributeString("path", Path);
            if (Status != NodeStatus.None) xmlWriter.WriteAttributeString("status", $"{Status}");
            if (!string.IsNullOrWhiteSpace(StatusMessage)) xmlWriter.WriteAttributeString("statusMessage", StatusMessage);
            if (!string.IsNullOrWhiteSpace(Updated)) xmlWriter.WriteAttributeString("updated", Updated);

            foreach (var attribute in Attributes)
            {
                WriteXmlAttribute(xmlWriter, attribute.Key, $"{ attribute.Value}");
            }
            WriteXmlAttributeAfter(xmlWriter);

            foreach (var property in Properties)
            {
                property.WriteXml(xmlWriter);
            }
        }
        protected virtual void WriteXmlAttribute(XmlWriter xmlWriter, string key, string value)
        {
            xmlWriter.WriteAttributeString(key, value);
        }

        protected virtual void WriteXmlAttributeAfter(XmlWriter xmlWriter)
        {
        }

        public virtual bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            if (xmlNode.LocalName != "Node")
            {
                NodeType = xmlNode.LocalName;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    SetAttribute(attribute.Name, attribute.Value);
                }
            }

            XmlNodeList propertyNodes = xmlNode.SelectNodes("Property");
            if (propertyNodes != null)
            {
                foreach (XmlNode propertyNode in propertyNodes)
                {
                    Property property = Property.CreateFrom(propertyNode);
                    if (!string.IsNullOrWhiteSpace(property?.Id))
                    {
                        SetProperty(property);
                    }
                }
            }
            return !string.IsNullOrWhiteSpace(Id);
        }
    }
}
