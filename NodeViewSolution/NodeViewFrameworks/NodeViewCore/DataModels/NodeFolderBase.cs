﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeView.DataModels
{
    public abstract class NodeFolderBase : BindableBase, INodeFolder
    {
        protected readonly Dictionary<string, INode> _childNodeDictionary = new Dictionary<string, INode>();
        protected readonly Dictionary<string, INodeFolder> _childFolderDictionary = new Dictionary<string, INodeFolder>();

        public string Id { get; set; } 
        public string Path { get; set; }
        public IEnumerable<INode> ChildNodes => _childNodeDictionary.Values;
        public IEnumerable<INodeFolder> ChildFolders => _childFolderDictionary.Values;
        
        protected NodeFolderBase(string id)
        {
            Id = id;
            Path = "";
        }

        public bool TryGetNode(string path, out INode node)
        {
            node = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            (string pathPart, string id) = StringUtils.SplitPathToOthersAndLast(path);
            if (string.IsNullOrWhiteSpace(pathPart))
            {
                if (_childNodeDictionary.TryGetValue(id, out var foundNode))
                {
                    node = foundNode;
                    return true;
                }
            }
            else
            {
                if (TryGetFolder(pathPart, out var folder))
                {
                    if (folder.TryGetNode(id, out var foundNode))
                    {
                        node = foundNode;
                        return true;
                    }
                }
            }

            return false;
        }

        public bool SetChildNode(INode node)
        {
            if (string.IsNullOrWhiteSpace(node?.Id)) return false;

            node.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";
            _childNodeDictionary[node.Id] = node;
            return true;
        }

        public bool RemoveChildNode(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            return _childNodeDictionary.Remove(id);
        }

        public void ClearChildNodes()
        {
            _childNodeDictionary.Clear();
        }

        public bool TryGetFolder(string path, out INodeFolder folder)
        {
            folder = null;
            if (string.IsNullOrWhiteSpace(path)) return false;

            (string id, string subPath) = StringUtils.SplitPathToFirstAndOthers(path);
            if (_childFolderDictionary.TryGetValue(id, out var foundFolder))
            {
                if (string.IsNullOrWhiteSpace(subPath))
                {
                    folder = foundFolder;
                    return true;
                }
                return foundFolder.TryGetFolder(subPath, out folder);
            }

            return false;
        }

        public bool SetChildFolder(INodeFolder folder)
        {
            if (string.IsNullOrWhiteSpace(folder?.Id)) return false;
            folder.Path = string.IsNullOrWhiteSpace(Path) ? $"{Id}" : $"{Path}/{Id}";
            _childFolderDictionary[folder.Id] = folder;
            return true;
        }

        public bool RemoveChildFolder(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return false;

            return _childFolderDictionary.Remove(id);
        }

        public void ClearChildFolders()
        {
            foreach (var childFolder in _childFolderDictionary.Values)
            {
                childFolder.Clear();
            }
            _childFolderDictionary.Clear();
        }

        public void Clear()
        {
            ClearChildNodes();
            ClearChildFolders();
        }
    }
}
