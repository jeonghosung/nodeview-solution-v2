﻿namespace NodeView.Drawing
{
    public enum AreaAlignment
    {
        Manual,
        LeftTop,
        RightTop,
        LeftBottom,
        RightBottom
    }
}