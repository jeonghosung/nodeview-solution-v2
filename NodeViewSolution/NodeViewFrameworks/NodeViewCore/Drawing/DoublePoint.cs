﻿using NodeView.Utils;
using System;
using System.ComponentModel;
using System.Globalization;

namespace NodeView.Drawing
{
    [TypeConverter(typeof(DoublePointConverter))]

    public class DoublePoint
    {
        public double X { get; set; }
        public double Y { get; set; }

        public DoublePoint(double x, double y)
        {
            X = x;
            Y = y;
        }
        public DoublePoint(DoublePoint point) : this(point.X, point.Y)
        {
        }

        public void Offset(double offsetX, double offsetY)
        {
            X += offsetX;
            Y += offsetY;
        }

        public override string ToString()
        {
            return $"{X},{Y}";
        }
        
        public static DoublePoint CreateFrom(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return new DoublePoint(0, 0);
            }
            source = source.Trim();
            string[] splitStrings = StringUtils.Split(source, ',', 2);
            return new DoublePoint(StringUtils.GetDoubleValue(splitStrings[0]), StringUtils.GetDoubleValue(splitStrings[1]));
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as DoublePoint);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X.GetHashCode() * 397) ^ Y.GetHashCode();
            }
        }

        public bool Equals(DoublePoint point)
        {
            if (Object.ReferenceEquals(point, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(this, point))
            {
                return true;
            }

            if (this.GetType() != point.GetType())
            {
                return false;
            }
            return (X == point.X) && (Y == point.Y);
        }

        public static bool operator ==(DoublePoint left, DoublePoint right)
        {
            if (Object.ReferenceEquals(left, null))
            {
                if (Object.ReferenceEquals(right, null))
                {
                    return true;
                }
                return false;
            }
            return left.Equals(right);
        }

        public static bool operator !=(DoublePoint left, DoublePoint right)
        {
            return !(left == right);
        }
    }

    public class DoublePointConverter :TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value)
        {
            if (value == null)
                throw this.GetConvertFromException(null);
            if (value is string source)
                return (object)DoublePoint.CreateFrom(source);
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (value is DoublePoint point)
            {
                if (destinationType == typeof(string))
                    return (object)point.ToString();
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
