﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.Drawing
{
    [TypeConverter(typeof(IntRectConverter))]
    public class IntRect
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public int Left => X;
        public int Top => Y;
        public int Right => X + Width;
        public int Bottom => Y + Height;

        public IntPoint LeftTop => new IntPoint(Left, Top);

        public IntPoint RightTop => new IntPoint(Right, Top);

        public IntPoint LeftBottom => new IntPoint(Left, Bottom);

        public IntPoint RightBottom => new IntPoint(Right, Bottom);
        public IntSize Size => new IntSize(Width, Height);
        public bool IsEmpty => Width == 0 || Height == 0;
        public static IntRect Empty => new IntRect(0, 0, 0, 0);

        public IntRect(int x, int y, int width, int height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
        public IntRect(IntPoint point, IntSize size) : this(point.X, point.Y, size.Width, size.Height)
        {
        }
        public IntRect(IntRect rect) : this(rect.X, rect.Y, rect.Width, rect.Height)
        {
        }
        public override string ToString()
        {
            return $"{X},{Y},{Width},{Height}";
        }
        public bool Contains(IntPoint point)
        {
            return Contains(point.X, point.Y);
        }

        public bool Contains(int x, int y)
        {
            if (IsEmpty)
                return false;

            if (x >= Left && x <= Right && y >= Top)
                return y <= Bottom;
            return false;
        }

        public bool Contains(IntRect rect)
        {
            if (IsEmpty || rect.IsEmpty || (Left > rect.Left || Top > rect.Top) || Right < rect.Right)
                return false;
            return Bottom >= rect.Bottom;
        }

        //경계선 겹치는것 제외
        public bool IsIntersectsAreaWith(IntRect rect)
        {
            if (IsEmpty || rect.IsEmpty || rect.Left >= Right || rect.Right <= Left || rect.Top >= Bottom || rect.Bottom <= Top)
                return false;
            return true;
        }

        public bool IsIntersectsWith(IntRect rect)
        {
            if (IsEmpty || rect.IsEmpty || rect.Left > Right || rect.Right < Left || rect.Top > Bottom || rect.Bottom < Top)
                return false;
            return true;
        }

        public void Intersect(IntRect rect)
        {
            if (!IsEmpty)
            {
                if (!IsIntersectsWith(rect))
                {
                    Width = 0;
                    Height = 0;
                }
            }
            else
            {
                int left = Math.Max(Left, rect.Left);
                int top = Math.Max(Top, rect.Top);
                Width = Math.Max(Math.Min(Right, rect.Right) - left, 0);
                Height = Math.Max(Math.Min(Bottom, rect.Bottom) - top, 0);
                X = left;
                Y = top;
            }
        }
        public static IntRect Intersect(IntRect rect1, IntRect rect2)
        {
            IntRect intersectRect = new IntRect(rect1);
            intersectRect.Intersect(rect2);
            return intersectRect;
        }
        public void Union(IntRect rect)
        {

            if (rect == null || rect.IsEmpty)
            {
                return;
            }

            if (IsEmpty)
            {
                X = rect.X;
                Y = rect.Y;
                Width = rect.Width;
                Height = rect.Height;
            }
            else
            {
                int left = Math.Min(Left, rect.Left);
                int top = Math.Min(Top, rect.Top);
                int right = Math.Max(Right, rect.Right);
                int bottom = Math.Max(Bottom, rect.Bottom);
                X = left;
                Y = top;
                Width = right - left;
                Height = bottom - top;
            }
        }

        public static IntRect Union(IntRect rect1, IntRect rect2)
        {
            IntRect unionRect = new IntRect(rect1);
            unionRect.Union(rect2);
            return unionRect;
        }
        public void Offset(int offsetX, int offsetY)
        {
            X += offsetX;
            Y += offsetY;
        }

        public static IntRect Offset(IntRect rect, int offsetX, int offsetY)
        {
            IntRect resRect = new IntRect(rect);
            resRect.Offset(offsetX, offsetY);
            return resRect;
        }
        public void Inflate(int width, int height)
        {
            X -= width;
            X -= height;
            Width -= width;
            Width -= width;
            Height -= height;
            Height -= height;
            if (Width < 0 || Height < 0)
            {
                Width = 0;
                Height = 0;
            }
        }
        public static IntRect Inflate(IntRect rect, int width, int height)
        {
            IntRect resRect = new IntRect(rect);
            if (resRect.IsEmpty)
            {
                return resRect;
            }
            resRect.Inflate(width, height);
            return resRect;
        }

        public void Scale(double scaleX, double scaleY)
        {
            if (IsEmpty)
                return;
            X = (int)(X * scaleX);
            Y = (int)(Y * scaleY);
            Width = (int)(Width * scaleX);
            Height = (int)(Height * scaleY);
            if (scaleX < 0)
            {
                X += Width;
                Width *= -1;
            }
            if (scaleY >= 0)
                return;
            Y += Height;
            Height *= -1;
        }
        public static IntRect Scale(IntRect rect, double scaleX, double scaleY)
        {
            IntRect resRect = new IntRect(rect);
            if (resRect.IsEmpty)
            {
                return resRect;
            }
            resRect.Scale(scaleX, scaleY);
            return resRect;
        }

        public IntRect Clone()
        {
            return new IntRect(X, Y, Width, Height);
        }
        public static IntRect CreateFrom(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return new IntRect(0, 0, 0, 0);
            }
            source = source.Trim();
            string[] splitStrings = StringUtils.Split(source, ',', 4);
            return new IntRect(StringUtils.GetIntValue(splitStrings[0]), StringUtils.GetIntValue(splitStrings[1]), StringUtils.GetIntValue(splitStrings[2]), StringUtils.GetIntValue(splitStrings[3]));
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as IntRect);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = X;
                hashCode = (hashCode * 397) ^ Y;
                hashCode = (hashCode * 397) ^ Width;
                hashCode = (hashCode * 397) ^ Height;
                return hashCode;
            }
        }


        public bool Equals(IntRect rect)
        {
            if (Object.ReferenceEquals(rect, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(this, rect))
            {
                return true;
            }

            if (this.GetType() != rect.GetType())
            {
                return false;
            }
            return (X == rect.X) && (Y == rect.Y) && (Width == rect.Width) && (Height == rect.Height);
        }

        public static bool operator ==(IntRect left, IntRect right)
        {
            if (Object.ReferenceEquals(left, null))
            {
                if (Object.ReferenceEquals(right, null))
                {
                    return true;
                }
                return false;
            }
            return left.Equals(right);
        }

        public static bool operator !=(IntRect left, IntRect right)
        {
            return !(left == right);
        }
    }

    public sealed class IntRectConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value)
        {
            switch (value)
            {
                case null:
                    throw this.GetConvertFromException(null);
                case string source:
                    return (object)IntRect.CreateFrom(source);
                default:
                    return base.ConvertFrom(context, culture, value);
            }
        }

        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (!(value is IntRect)) return base.ConvertTo(context, culture, value, destinationType);
            IntRect rect = (IntRect)value;
            if (destinationType == typeof(string))
                return (object)rect.ToString();
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
