﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.Drawing
{
    [TypeConverter(typeof(DoubleRectConverter))]
    public class DoubleRect
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double  Width { get; set; }
        public double Height { get; set; }

        public double Left => X;
        public double Top => Y;
        public double Right => X + Width;
        public double Bottom => Y + Height;

        public DoublePoint LeftTop => new DoublePoint(Left, Top);

        public DoublePoint RightTop => new DoublePoint(Right, Top);

        public DoublePoint LeftBottom => new DoublePoint(Left, Bottom);

        public DoublePoint RightBottom => new DoublePoint(Right, Bottom);
        public DoubleSize Size => new DoubleSize(Width, Height);
        public bool IsEmpty => Width == 0 || Height == 0;
        public static DoubleRect Empty => new DoubleRect(0, 0, 0, 0);

        public DoubleRect(double x, double y, double width, double height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }
        public DoubleRect(DoublePoint point, DoubleSize size) : this(point.X, point.Y, size.Width, size.Height)
        {
        }
        public DoubleRect(DoubleRect rect) : this(rect.X, rect.Y, rect.Width, rect.Height)
        {
        }
        public override string ToString()
        {
            return $"{X},{Y},{Width},{Height}";
        }
        public bool Contains(DoublePoint point)
        {
            return Contains(point.X, point.Y);
        }

        public bool Contains(double x, double y)
        {
            if (IsEmpty)
                return false;

            if (x >= Left && x <= Right && y >= Top)
                return y <= Bottom;
            return false;
        }

        public bool Contains(DoubleRect rect)
        {
            if (IsEmpty || rect.IsEmpty || (Left > rect.Left || Top > rect.Top) || Right < rect.Right)
                return false;
            return Bottom >= rect.Bottom;
        }

        //경계선 겹치는것 제외
        public bool IsIntersectsAreaWith(DoubleRect rect)
        {
            if (IsEmpty || rect.IsEmpty || rect.Left >= Right || rect.Right <= Left || rect.Top >= Bottom || rect.Bottom <= Top)
                return false;
            return true;
        }

        public bool IsIntersectsWith(DoubleRect rect)
        {
            if (IsEmpty || rect.IsEmpty || rect.Left > Right || rect.Right < Left || rect.Top > Bottom || rect.Bottom < Top)
                return false;
            return true;
        }

        public void Intersect(DoubleRect rect)
        {
            if (!IsEmpty)
            {
                if (!IsIntersectsWith(rect))
                {
                    Width = 0;
                    Height = 0;
                }
            }
            else
            {
                double left = Math.Max(Left, rect.Left);
                double top = Math.Max(Top, rect.Top);
                Width = Math.Max(Math.Min(Right, rect.Right) - left, 0);
                Height = Math.Max(Math.Min(Bottom, rect.Bottom) - top, 0);
                X = left;
                Y = top;
            }
        }
        public static DoubleRect Intersect(DoubleRect rect1, DoubleRect rect2)
        {
            DoubleRect intersectRect = new DoubleRect(rect1);
            intersectRect.Intersect(rect2);
            return intersectRect;
        }
        public void Union(DoubleRect rect)
        {

            if (rect == null || rect.IsEmpty)
            {
                return;
            }

            if (IsEmpty)
            {
                X = rect.X;
                Y = rect.Y;
                Width = rect.Width;
                Height = rect.Height;
            }
            else
            {
                double left = Math.Min(Left, rect.Left);
                double top = Math.Min(Top, rect.Top);
                double right = Math.Max(Right, rect.Right);
                double bottom = Math.Max(Bottom, rect.Bottom);
                X = left;
                Y = top;
                Width = right - left;
                Height = bottom - top;
            }
        }

        public static DoubleRect Union(DoubleRect rect1, DoubleRect rect2)
        {
            DoubleRect unionRect = new DoubleRect(rect1);
            unionRect.Union(rect2);
            return unionRect;
        }
        public void Offset(double offsetX, double offsetY)
        {
            X += offsetX;
            Y += offsetY;
        }

        public static DoubleRect Offset(DoubleRect rect, double offsetX, double offsetY)
        {
            DoubleRect resRect = new DoubleRect(rect);
            resRect.Offset(offsetX, offsetY);
            return resRect;
        }
        public void Inflate(double width, double height)
        {
            X -= width;
            X -= height;
            Width += width;
            Width += width;
            Height += height;
            Height += height;
            if (Width < 0 || Height < 0)
            {
                Width = 0;
                Height = 0;
            }
        }
        public static DoubleRect Inflate(DoubleRect rect, double width, double height)
        {
            DoubleRect resRect = new DoubleRect(rect);
            if (resRect.IsEmpty)
            {
                return resRect;
            }
            resRect.Inflate(width, height);
            return resRect;
        }

        public void Scale(double scaleX, double scaleY)
        {
            if (IsEmpty)
                return;
            X = (double)(X * scaleX);
            Y = (double)(Y * scaleY);
            Width = (double)(Width * scaleX);
            Height = (double)(Height * scaleY);
            if (scaleX < 0)
            {
                X += Width;
                Width *= -1;
            }
            if (scaleY >= 0)
                return;
            Y += Height;
            Height *= -1;
        }
        public static DoubleRect Scale(DoubleRect rect, double scaleX, double scaleY)
        {
            DoubleRect resRect = new DoubleRect(rect);
            if (resRect.IsEmpty)
            {
                return resRect;
            }
            resRect.Scale(scaleX, scaleY);
            return resRect;
        }

        public static DoubleRect CreateFrom(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return new DoubleRect(0, 0, 0, 0);
            }
            source = source.Trim();
            string[] splitStrings = StringUtils.Split(source, ',', 4);
            return new DoubleRect(StringUtils.GetDoubleValue(splitStrings[0]), StringUtils.GetDoubleValue(splitStrings[1]), StringUtils.GetDoubleValue(splitStrings[2]), StringUtils.GetDoubleValue(splitStrings[3]));
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as DoubleRect);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = X.GetHashCode();
                hashCode = (hashCode * 397) ^ Y.GetHashCode();
                hashCode = (hashCode * 397) ^ Width.GetHashCode();
                hashCode = (hashCode * 397) ^ Height.GetHashCode();
                return hashCode;
            }
        }

        public bool Equals(DoubleRect rect)
        {
            if (Object.ReferenceEquals(rect, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(this, rect))
            {
                return true;
            }

            if (this.GetType() != rect.GetType())
            {
                return false;
            }
            return (X == rect.X) && (Y == rect.Y) && (Width == rect.Width) && (Height == rect.Height);
        }

        public static bool operator ==(DoubleRect left, DoubleRect right)
        {
            if (Object.ReferenceEquals(left, null))
            {
                if (Object.ReferenceEquals(right, null))
                {
                    return true;
                }
                return false;
            }
            return left.Equals(right);
        }

        public static bool operator !=(DoubleRect left, DoubleRect right)
        {
            return !(left == right);
        }
    }

    public sealed class DoubleRectConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return sourceType == typeof(string) || base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            return destinationType == typeof(string) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value)
        {
            switch (value)
            {
                case null:
                    throw this.GetConvertFromException(null);
                case string source:
                    return (object)DoubleRect.CreateFrom(source);
                default:
                    return base.ConvertFrom(context, culture, value);
            }
        }

        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (!(value is DoubleRect)) return base.ConvertTo(context, culture, value, destinationType);
            DoubleRect rect = (DoubleRect)value;
            if (destinationType == typeof(string))
                return (object)rect.ToString();
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
