﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.Drawing
{
    [TypeConverter(typeof(IntSizeConverter))]
    public class IntSize
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public bool IsEmpty => Width == 0 || Height == 0;
        public static IntSize Empty => new IntSize(0, 0);

        public IntSize(int width, int height)
        {
            Width = width;
            Height = height;
        }
        public IntSize(IntPoint point) : this(point.X, point.Y)
        {
        }
        public IntSize(IntSize size) : this(size.Width, size.Height)
        {
        }

        public override string ToString()
        {
            return $"{Width},{Height}";
        }

        public static IntSize CreateFrom(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return new IntSize(0, 0);
            }
            source = source.Trim();
            string[] splitStrings = StringUtils.Split(source, ',', 2);
            return new IntSize(StringUtils.GetIntValue(splitStrings[0]), StringUtils.GetIntValue(splitStrings[1]));
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as IntSize);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Width * 397) ^ Height;
            }
        }

        public bool Equals(IntSize size)
        {
            if (Object.ReferenceEquals(size, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(this, size))
            {
                return true;
            }

            if (this.GetType() != size.GetType())
            {
                return false;
            }
            return (Width == size.Width) && (Height == size.Height);
        }

        public static bool operator ==(IntSize left, IntSize right)
        {
            if (Object.ReferenceEquals(left, null))
            {
                if (Object.ReferenceEquals(right, null))
                {
                    return true;
                }
                return false;
            }
            return left.Equals(right);
        }

        public static bool operator !=(IntSize left, IntSize right)
        {
            return !(left == right);
        }
    }

    public class IntSizeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value)
        {
            if (value == null)
                throw this.GetConvertFromException(null);
            if (value is string source)
            {
                return (object)IntSize.CreateFrom(source);
            }
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (value is IntSize point)
            {
                if (destinationType == typeof(string))
                    return (object)point.ToString();
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
