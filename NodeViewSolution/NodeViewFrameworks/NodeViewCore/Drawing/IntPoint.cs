﻿using NodeView.Utils;
using System;
using System.ComponentModel;
using System.Globalization;

namespace NodeView.Drawing
{
    [TypeConverter(typeof(IntPointConverter))]

    public class IntPoint
    {
        public int X { get; set; }
        public int Y { get; set; }

        public IntPoint(int x, int y)
        {
            X = x;
            Y = y;
        }
        public IntPoint(IntPoint point) : this(point.X, point.Y)
        {
        }

        public void Offset(int offsetX, int offsetY)
        {
            X += offsetX;
            Y += offsetY;
        }

        public override string ToString()
        {
            return $"{X},{Y}";
        }
        
        public static IntPoint CreateFrom(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return new IntPoint(0, 0);
            }
            source = source.Trim();
            string[] splitStrings = StringUtils.Split(source, ',', 2);
            return new IntPoint(StringUtils.GetIntValue(splitStrings[0]), StringUtils.GetIntValue(splitStrings[1]));
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as IntPoint);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (X * 397) ^ Y;
            }
        }

        public bool Equals(IntPoint point)
        {
            if (Object.ReferenceEquals(point, null))
            {
                return false;
            }

            if (Object.ReferenceEquals(this, point))
            {
                return true;
            }

            if (this.GetType() != point.GetType())
            {
                return false;
            }
            return (X == point.X) && (Y == point.Y);
        }

        public static bool operator ==(IntPoint left, IntPoint right)
        {
            if (Object.ReferenceEquals(left, null))
            {
                if (Object.ReferenceEquals(right, null))
                {
                    return true;
                }
                return false;
            }
            return left.Equals(right);
        }

        public static bool operator !=(IntPoint left, IntPoint right)
        {
            return !(left == right);
        }
    }

    public class IntPointConverter :TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(string))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value)
        {
            if (value == null)
                throw this.GetConvertFromException(null);
            if (value is string source)
                return (object)IntPoint.CreateFrom(source);
            return base.ConvertFrom(context, culture, value);
        }

        public override object ConvertTo(
            ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (value is IntPoint point)
            {
                if (destinationType == typeof(string))
                    return (object)point.ToString();
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }
}
