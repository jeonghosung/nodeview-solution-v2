﻿using System.Collections.Generic;
using NodeView.DataModels;
using NodeView.Frameworks.ContentControls;

namespace NodeView.Frameworks.Widgets
{
    public interface IWidgetSource : IContentSource, INodeViewSerialize
    {
        string Module { get; }
        IConfiguration Config { get; }
    }
}
