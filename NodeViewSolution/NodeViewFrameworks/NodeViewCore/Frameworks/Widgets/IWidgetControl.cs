﻿namespace NodeView.Frameworks.Widgets
{
    public interface IWidgetControl
    {
        IWidgetSource Source { get; set; }
    }
}
