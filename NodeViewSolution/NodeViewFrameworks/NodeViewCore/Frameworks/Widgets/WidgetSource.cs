﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;

namespace NodeView.Frameworks.Widgets
{
    public class WidgetSource : IWidgetSource
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private KeyValueCollection _properties = new KeyValueCollection();
        private Configuration _config;
        public string Id { get; set; }
        public string ContentType { get; set; }
        public string Name { get; set; }
        public string Module { get; set; }

        public IConfiguration Config => _config;

        public WidgetSource() : this(Uuid.NewUuid)
        {
        }

        public WidgetSource(string id, string contentType = "", string name = "", IConfiguration config = null)
        {
            Id = string.IsNullOrWhiteSpace(id) ? Uuid.NewUuid : id;
            Module = "";
            ContentType = contentType ?? "";
            Name = string.IsNullOrWhiteSpace(name) ? id : name;
            LoadFrom(config);
        }

        public bool LoadFrom(IConfiguration config)
        {
            _config = Configuration.CreateFrom(config) ?? new Configuration(Uuid.NewUuid);

            Id = _config.Id;
            Module = _config.GetValue("@module", Module);
            ContentType = _config.GetValue("@contentType", ContentType);
            Name = _config.GetValue("@name", Name);

            _config.SetAttribute("id", Id);
            _config.SetAttribute("contentType", ContentType);
            _config.SetAttribute("name", Name);

            return !string.IsNullOrWhiteSpace(Module) || !string.IsNullOrWhiteSpace(ContentType);
        }

        public JToken ToJson()
        {
            return Config.ToJson();
        }

        public bool LoadFrom(JObject json)
        {
            if (json == null) return false;

            Configuration config = Configuration.CreateFrom(json);
            return LoadFrom(config);
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            tagName = string.IsNullOrWhiteSpace(tagName) ? "WidgetSource" : tagName;
            Config.WriteXml(xmlWriter, tagName);
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null) return false;

            Configuration config = Configuration.CreateFrom(xmlNode);
            return LoadFrom(config);
        }

        public static WidgetSource CreateFrom(JObject json)
        {
            var widgetSource = new WidgetSource();
            widgetSource.LoadFrom(json);
            return widgetSource;
        }

        public static WidgetSource CreateFrom(XmlNode xmlNode)
        {
            var widgetSource = new WidgetSource();
            widgetSource.LoadFrom(xmlNode);
            return widgetSource;
        }
        public static WidgetSource CreateFrom(IConfiguration config)
        {
            var widgetSource = new WidgetSource();
            widgetSource.LoadFrom(config);
            return widgetSource;
        }
    }
}
