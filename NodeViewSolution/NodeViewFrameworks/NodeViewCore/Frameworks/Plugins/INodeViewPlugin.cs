﻿using System;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;

namespace NodeView.Frameworks.Plugins
{
    public interface INodeViewPlugin : IObject, IDisposable
    {
        bool IsStarted { get; }
        bool Init(Configuration config, IStationService stationService);
        bool Start();
        void Stop();
    }
}
