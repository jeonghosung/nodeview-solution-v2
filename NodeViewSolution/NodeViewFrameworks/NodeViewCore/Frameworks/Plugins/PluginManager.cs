﻿using System;
using System.Collections.Generic;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Modularity;
using NodeView.Frameworks.Stations;
using Prism.Ioc;

namespace NodeView.Frameworks.Plugins
{
    public class PluginManager : IDisposable
    {
        private readonly IContainerProvider _containerProvider;
        private readonly IStationService _stationService;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<string, INodeViewPlugin> _pluginDictionary = new Dictionary<string, INodeViewPlugin>();

        public PluginManager(IContainerProvider containerProvider, IStationService stationService)
        {
            _containerProvider = containerProvider;
            _stationService = stationService;
        }

        public int Count => _pluginDictionary.Count;

        public bool LoadPlugin(Configuration config, Type pluginType = null)
        {
            bool res = false;

            if (pluginType == null)
            {
                string moduleName = config.GetStringValue("module");
                if (string.IsNullOrWhiteSpace(moduleName)) return false;
                var moduleManager = _containerProvider.Resolve<ModuleManager>();
                pluginType = moduleManager.FindModuleType(moduleName);
                if (pluginType == null)
                {
                    return false;
                }
            }

            try
            {
                var plugin = _containerProvider.Resolve(pluginType);
                if (plugin is INodeViewPlugin nodeViewPlugin)
                {
                    if (nodeViewPlugin.Init(config, _stationService))
                    {
                        res = AddPlugin(nodeViewPlugin);
                    }
                    else
                    {
                        Logger.Error($"Cannot init for [{pluginType.FullName}].");
                    }
                }
                else
                {
                    Logger.Error($"[{pluginType.FullName}] is not a IPlugin.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load:");
                res = false;
            }

            return res;
        }

        public bool AddPlugin(INodeViewPlugin plugin)
        {
            if (string.IsNullOrWhiteSpace(plugin?.Id)) return false;

            if (_pluginDictionary.TryGetValue(plugin.Id, out var fountPlugin))
            {
                Logger.Error($"[{plugin.Id}] is already registered.");
            }
            else
            {
                _pluginDictionary[plugin.Id] = plugin;
                return true;
            }

            return false;
        }

        public int Start()
        {
            int startedCount = 0;
            foreach (var plugin in _pluginDictionary.Values)
            {
                try
                {
                    if (plugin.Start())
                    {
                        startedCount++;
                    }
                    else
                    {
                        Logger.Error( $"Cannot start plugin [{plugin.Id}]");
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Cannot stop plugin [{plugin.Id}]");
                }
            }

            return startedCount;
        }

        public void Stop()
        {
            foreach (var plugin in _pluginDictionary.Values)
            {
                try
                {
                    plugin.Stop();
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Cannot stop plugin [{plugin.Id}]");
                }
            }
        }

        public void Clear()
        {
            foreach (var plugin in _pluginDictionary.Values)
            {
                try
                {
                    plugin.Dispose();
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Cannot dispose plugin [{plugin.Id}]");
                }
            }
            _pluginDictionary.Clear();
        }
        public void Dispose()
        {
            Clear();
        }
    }
}
