﻿using NodeView.DataWalls;

namespace NodeView.Frameworks.DataWallContentControls
{
    public interface IDataWallContentControl
    {
        IDataWallContent Source { get; set; }
    }
}
