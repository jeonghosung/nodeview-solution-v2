﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;

namespace NodeView.Frameworks.Stations
{
    //DefaultStationRepository로 SqliteStationRepository를 사용하자 (2022.03.24 by charles)
    /*
    public class DefaultStationRepository : IStationRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly Dictionary<string, NodeViewAction> _actionDictionary = new Dictionary<string, NodeViewAction>();
        private readonly Dictionary<string, EventAction> _eventActionDictionary = new Dictionary<string, EventAction>();
        private readonly Dictionary<string, INode> _nodeDictionary = new Dictionary<string, INode>();

        public string CachesFolder { get; protected set; } = "";
        public string ConfigsFolder { get; protected set; } = "";

        public DefaultStationRepository()
        {
            CachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(CachesFolder);
            ConfigsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            Directory.CreateDirectory(ConfigsFolder);

            LoadActions();
            LoadEventActions();
            LoadNodes();
        }

        public IEnumerable<NodeViewAction> GetActions()
        {
            return _actionDictionary.Values.ToArray();
        }

        public int CreateActions(IEnumerable<NodeViewAction> actions)
        {
            if (actions == null) return 0;

            int updatedCount = 0;
            foreach (var action in actions)
            {
                if (string.IsNullOrWhiteSpace(action.Id))
                {
                    continue;
                }

                _actionDictionary[action.Id] = action;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveActions();
            }

            return updatedCount;
        }

        public int UpdateActions(IEnumerable<NodeViewAction> actions)
        {
            if (actions == null) return 0;

            int updatedCount = 0;
            foreach (var action in actions)
            {
                if (string.IsNullOrWhiteSpace(action.Id))
                {
                    continue;
                }

                _actionDictionary[action.Id] = action;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveActions();
            }

            return updatedCount;
        }

        public int DeleteActions(IEnumerable<string> uids)
        {
            if (uids == null) return 0;

            int updatedCount = 0;
            foreach (var uid in uids)
            {
                if (string.IsNullOrWhiteSpace(uid))
                {
                    continue;
                }

                if (_actionDictionary.ContainsKey(uid))
                {
                    _actionDictionary.Remove(uid);
                    updatedCount++;
                }
            }
            if (updatedCount > 0)
            {
                SaveActions();
            }
            return updatedCount;
        }

        public void DeleteAllActions()
        {
            if (_actionDictionary.Count > 0)
            {
                _actionDictionary.Clear();
                SaveActions();
            }
        }

        public IEnumerable<EventAction> GetEventActions()
        {
            return _eventActionDictionary.Values.ToArray();
        }

        public int CreateEventActions(IEnumerable<EventAction> eventActions)
        {
            if (eventActions == null) return 0;

            int updatedCount = 0;
            foreach (var eventAction in eventActions)
            {
                if (string.IsNullOrWhiteSpace(eventAction.Id))
                {
                    continue;
                }

                _eventActionDictionary[eventAction.Id] = eventAction;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveEventActions();
            }

            return updatedCount;
        }

        public int UpdateEventActions(IEnumerable<EventAction> eventActions)
        {
            if (eventActions == null) return 0;

            int updatedCount = 0;
            foreach (var eventAction in eventActions)
            {
                if (string.IsNullOrWhiteSpace(eventAction.Id))
                {
                    continue;
                }

                _eventActionDictionary[eventAction.Id] = eventAction;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveActions();
            }

            return updatedCount;
        }

        public int DeleteEventActions(IEnumerable<string> uids)
        {
            if (uids == null) return 0;

            int updatedCount = 0;
            foreach (var uid in uids)
            {
                if (string.IsNullOrWhiteSpace(uid))
                {
                    continue;
                }

                if (_eventActionDictionary.ContainsKey(uid))
                {
                    _eventActionDictionary.Remove(uid);
                    updatedCount++;
                }
            }
            if (updatedCount > 0)
            {
                SaveEventActions();
            }
            return updatedCount;
        }

        public void DeleteAllEventActions()
        {
            if (_eventActionDictionary.Count > 0)
            {
                _eventActionDictionary.Clear();
                SaveEventActions();
            }
        }

        public IEnumerable<INode> GetNodes()
        {
            return _nodeDictionary.Values;
        }

        public int CreateNodes(IEnumerable<INode> nodes)
        {
            if (nodes == null) return 0;

            int updatedCount = 0;
            foreach (var node in nodes)
            {
                if (string.IsNullOrWhiteSpace(node.PathId))
                {
                    continue;
                }

                _nodeDictionary[node.PathId] = node;
                updatedCount++;
            }
            if (updatedCount > 0)
            {
                SaveNodes();
            }
            return updatedCount;
        }

        public int UpdateNodes(IEnumerable<INode> nodes)
        {
            if (nodes == null) return 0;

            int updatedCount = 0;
            foreach (var node in nodes)
            {
                if (string.IsNullOrWhiteSpace(node.PathId))
                {
                    continue;
                }

                _nodeDictionary[node.PathId] = node;
                updatedCount++;
            }
            if (updatedCount > 0)
            {
                SaveNodes();
            }
            return updatedCount;
        }

        public int DeleteNodes(IEnumerable<string> uids)
        {
            if (uids == null) return 0;

            int updatedCount = 0;
            foreach (var uid in uids)
            {
                if (string.IsNullOrWhiteSpace(uid))
                {
                    continue;
                }

                if (_nodeDictionary.ContainsKey(uid))
                {
                    _nodeDictionary.Remove(uid);
                    updatedCount++;
                }
            }
            if (updatedCount > 0)
            {
                SaveNodes();
            }
            return updatedCount;
        }

        public void DeleteAllNodes(string path, bool recursive = false)
        {
            bool isSomeDeleted = false;
            if (_nodeDictionary.Count == 0)
            {
                return;
            }
            if (recursive)
            {
                if (string.IsNullOrWhiteSpace(path))
                {
                    _nodeDictionary.Clear();
                    isSomeDeleted = true;
                }
                else
                {
                    foreach (var node in _nodeDictionary.Values.ToArray())
                    {
                        if (node.Path.StartsWith(path))
                        {
                            _nodeDictionary.Remove(node.PathId);
                            isSomeDeleted = true;
                        }
                    }
                }
            }
            else
            {
                foreach (var node in _nodeDictionary.Values.ToArray())
                {
                    if (node.Path == path)
                    {
                        _nodeDictionary.Remove(node.PathId);
                        isSomeDeleted = true;
                    }
                }
            }

            if (isSomeDeleted)
            {
                SaveNodes();
            }
        }

        public IEnumerable<IEventLog> GetLatestEventLogs()
        {
            throw new NotImplementedException();
        }

        public bool AppendEventLog(IEventLog eventLog)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IEventLog> SearchEventLogs(string fromDate, string toDate, string searchKey, bool ascendingOrder = true,
            int maxLength = 1000)
        {
            throw new NotImplementedException();
        }

        private void LoadActions()
        {
            string eventsFile = Path.Combine(ConfigsFolder, "actions.xml");
            if (!File.Exists(eventsFile))
            {
                return;
            }

            try
            {
                _actionDictionary.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(eventsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Actions/Action");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        var action = NodeViewAction.CreateFrom(node);
                        if (action != null)
                        {
                            _actionDictionary[action.Id] = action;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadActions:");
            }
        }
        private void SaveActions()
        {
            string eventsFile = Path.Combine(ConfigsFolder, "actions.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(eventsFile))
                {
                    xmlWriter.WriteStartElement("Actions");
                    foreach (var action in _actionDictionary.Values.ToArray())
                    {
                        action.WriteXml(xmlWriter, "Action");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveActions:");
            }
        }

        private void LoadEventActions()
        {
            string eventActionsFile = Path.Combine(ConfigsFolder, "eventActions.xml");
            if (!File.Exists(eventActionsFile))
            {
                //Logger.Error($"Cannot find presets file[{presetsFile}].");
                return;
            }

            try
            {
                _eventActionDictionary.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(eventActionsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/EventActions/EventAction");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        var evnetAction = EventAction.CreateFrom(node);
                        if (evnetAction != null)
                        {
                            _eventActionDictionary[evnetAction.Id] = evnetAction;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadEventActions:");
            }
        }
        private void SaveEventActions()
        {
            string eventActionsFile = Path.Combine(ConfigsFolder, "eventActions.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(eventActionsFile))
                {
                    xmlWriter.WriteStartElement("EventActions");
                    foreach (var eventAction in _eventActionDictionary.Values.ToArray())
                    {
                        eventAction.WriteXml(xmlWriter, "EventAction");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveEventActions:");
            }
        }

        private void LoadNodes()
        {
         //skip   
        }
        private void SaveNodes()
        {
            //skip   
        }
    }
    */
}
