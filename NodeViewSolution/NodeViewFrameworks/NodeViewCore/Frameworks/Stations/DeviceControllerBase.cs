﻿using System;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;

namespace NodeView.Frameworks.Stations
{
    public abstract class DeviceControllerBase : DeviceBase, IDeviceController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected IStationService StationService = null;
        public bool IsTestMode { get; set; } = false;
        public bool IsVerbose { get; set; } = false;
        public bool IsStarted { get; protected set; } = false;

        public virtual bool Init(Configuration config, IStationService stationService)
        {
            StationService = stationService;
            if (!LoadConfig(config))
            {
                return false;
            }
            if (!RegisterOnStation())
            {
                return false;
            }

            return true;
        }

        public virtual bool Start()
        {
            if (!IsStarted)
            {
                IsStarted = true;
            }
            return IsStarted;
        }

        public virtual void Stop()
        {
            UnRegisterOnStation();
            StationService = null;
            IsStarted = false;
        }

        private bool RegisterOnStation()
        {
            return StationService?.RegisterDeviceController(this) ?? false;
        }

        protected virtual void UnRegisterOnStation()
        {
            StationService?.UnRegisterDeviceController(this);
        }

        protected virtual bool LoadConfig(Configuration config)
        {
            if (config == null)
            {
                if(string.IsNullOrWhiteSpace(Id)) return false;
                return true;
            }
            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    Id = StringUtils.GetStringValue(config.Id, Uuid.NewUuid);
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    Name = this.GetType().Name;
                }
                Description = config.GetValue("@description", "");
                ConnectionString = config.GetValue("connectionString", "");
                IsTestMode = config.GetValue("isTestMode", false); ;
                IsVerbose = IsTestMode || config.GetValue("isVerbose", false);
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }

        public void Dispose()
        {
            Stop();
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
