﻿using System;
using System.Collections.Generic;
using NodeView.DataModels;

namespace NodeView.Frameworks.Stations
{
    public interface IStationRepository
    {
        IEnumerable<NodeViewAction> GetActions();
        int CreateActions(IEnumerable<NodeViewAction> actions);
        int UpdateActions(IEnumerable<NodeViewAction> actions);
        int DeleteActions(IEnumerable<string> uids);
        void DeleteAllActions();

        IEnumerable<EventAction> GetEventActions();
        int CreateEventActions(IEnumerable<EventAction> eventActions);
        int UpdateEventActions(IEnumerable<EventAction> eventActions);
        int DeleteEventActions(IEnumerable<string> uids);
        void DeleteAllEventActions();

        IEnumerable<INode> GetNodes();
        int CreateNodes(IEnumerable<INode> nodes);
        int UpdateNodes(IEnumerable<INode> nodes);
        int DeleteNodes(IEnumerable<string> uids);
        void DeleteAllNodes(string path, bool recursive = false);

        IEnumerable<IEventLog> GetLatestEventLogs();
        bool AppendEventLog(IEventLog eventLog);

        IEnumerable<IEventLog> SearchEventLogs(string fromDate, string toDate, string searchKey, bool ascendingOrder = true, int maxLength = 1000);
    }
}
