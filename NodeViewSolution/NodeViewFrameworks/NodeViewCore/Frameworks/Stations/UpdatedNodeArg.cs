﻿using NodeView.DataModels;

namespace NodeView.Frameworks.Stations
{
    public class UpdatedNodeArg
    {
        public string Path { get; set; }
        public INode NewNode { get; set; }
        public INode OldNode { get; set; }

        public UpdatedNodeArg(string path, INode newNode, INode oldNode)
        {
            Path = path;
            NewNode = newNode?.Clone();
            OldNode = oldNode?.Clone();
        }
    }
}