﻿using NodeView.DataModels;
using NodeView.Frameworks.Plugins;

namespace NodeView.Frameworks.Stations
{
    public interface IDeviceController : IDevice, INodeViewPlugin
    {
    }
}
