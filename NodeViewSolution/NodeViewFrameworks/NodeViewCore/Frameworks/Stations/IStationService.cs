﻿using NodeView.DataModels;

namespace NodeView.Frameworks.Stations
{
    public interface IStationService
    {
        bool RegisterDataListener(IDataListener dataListener);
        void UnRegisterDataListener(IDataListener dataListener);
        bool RegisterDeviceController(IDeviceController deviceController);
        void UnRegisterDeviceController(IDeviceController deviceController);

        //actions
        NodeViewAction[] GetActions();
        bool CreateAction(NodeViewAction action);
        bool UpdateAction(NodeViewAction action);
        bool DeleteAction(string id);
        int DeleteActions(string[] ids);
        void DeleteAllActions();
        bool RunAction(string id);

        //events
        EventAction[] GetEventActions();
        bool CreateEventActions(EventAction eventAction);
        bool UpdateEventAction(EventAction eventAction);
        bool DeleteEventAction(string id);
        int DeleteEventActions(string[] ids);
        void DeleteAllEventActions();
        bool OnEvent(NodeViewEvent nodeViewEvent);

        //nodes
        string[] GetNodeFolders();
        INode GetNode(string path, string id);
        bool CreateNode(string path, INode node, bool keepPathInNode = false);
        bool UpdateNode(INode node);
        bool PatchNode(INode node);
        bool DeleteNode(string uid);

        INode[] GetNodes(string path, bool recursive = false);
        int CreateNodes(string path, INode[] nodes, bool keepPathInNode = false);
        int UpdateNodes(INode[] nodes);
        int PatchNodes(INode[] nodes);
        int DeleteNodes(string[] uIds);
        int DeleteNodes(string path, string[] ids);
        void DeleteAllNodes(string path, bool recursive = false);

        //devices
        IDevice[] GetDevices();
        IDevice GetDevice(string id);
        bool RunDeviceCommand(string id, string command, KeyValueCollection parameters);

        //eventLogs
        IEventLog[] GetLatestEventLogs();
        IEventLog[] SearchEventLogs(string fromDate, string toDate, string searchKey, bool ascendingOrder = true, int maxLength = 1000);
        bool AppendEventLog(IEventLog eventLog);
    }
}
