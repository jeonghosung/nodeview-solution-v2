﻿using NodeView.Frameworks.Plugins;

namespace NodeView.Frameworks.Stations
{
    public interface IDataListener : INodeViewPlugin
    {
        void UpdatedNodes(UpdatedNodeArg[] updatedNodeItems);
        void DeleteAllNodes(string path, bool recursive);
    }
}
