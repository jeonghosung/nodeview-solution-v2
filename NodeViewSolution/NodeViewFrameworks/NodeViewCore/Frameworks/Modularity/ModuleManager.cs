﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NLog;
using NodeView.Utils;
using Prism.Ioc;

namespace NodeView.Frameworks.Modularity
{
    public class ModuleManager :  IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public static readonly Type NodeViewModuleType = typeof(INodeViewModule);

        private readonly Dictionary<string, Assembly> _assemblyDictionary = new Dictionary<string, Assembly>();
        private readonly List<INodeViewModule> _nodeViewModules = new List<INodeViewModule>();

        public bool AddAssembly(string fileName)
        {
            string filepath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
            if (!File.Exists(filepath))
            {
                return false;
            }
            if (_assemblyDictionary.ContainsKey(filepath.ToLower()))
            {
                return true;
            }
            try
            {
                Assembly assembly = Assembly.LoadFile(filepath);
                _assemblyDictionary[filepath.ToLower()] = assembly;
                foreach (var type in assembly.GetTypes())
                {
                    try
                    {
                        if (type.CanCreateInstanceWithNullParameter(NodeViewModuleType))
                        {
                            if (Activator.CreateInstance(type) is INodeViewModule module)
                            {
                                _nodeViewModules.Add(module);
                            }
                            else
                            {
                                Logger.Error($"Cannot create INodeViewModule form '{type.FullName}'.");
                                return false;
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Error(e);
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot load '{fileName}'.");
            }

            return false;
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            foreach (var module in _nodeViewModules)
            {
                try
                {
                    module.RegisterTypes(containerRegistry);
                }
                catch (Exception e)
                {
                    string errorMessage = $"Error RegisterTypes for '{module.GetType().FullName}'.";
                    Logger.Error(e, errorMessage);
                }
            }
        }

        public void OnCreatedShell(IContainerProvider container, object shell)
        {
            foreach (var module in _nodeViewModules)
            {
                try
                {
                    module.OnCreatedShell(container, shell);
                }
                catch (Exception e)
                {
                    string errorMessage = $"Error OnCreatedShell for '{module.GetType().FullName}'.";
                    Logger.Error(e, errorMessage);
                }
            }
        }

        public void OnInitialized(IContainerProvider container)
        {
            foreach (var module in _nodeViewModules)
            {
                try
                {
                    module.OnInitialized(container);
                }
                catch (Exception e)
                {
                    string errorMessage = $"Error OnInitialized for '{module.GetType().FullName}'.";
                    Logger.Error(e, errorMessage);
                }
            }
        }

        public void Dispose()
        {
            _assemblyDictionary.Clear();
            _nodeViewModules.Clear();
        }

        public Type FindModuleType(string moduleName)
        {
            if (string.IsNullOrWhiteSpace(moduleName)) return null;

            try
            {
                Type moduleType = Type.GetType(moduleName);
                if (moduleType != null)
                {
                    return moduleType;
                }

                foreach (var assembly in _assemblyDictionary.Values.ToArray())
                {
                    moduleType = assembly.GetType(moduleName);
                    if (moduleType != null)
                    {
                        return moduleType;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "FindModuleType:");
            }

            return null;
        }
    }
}
