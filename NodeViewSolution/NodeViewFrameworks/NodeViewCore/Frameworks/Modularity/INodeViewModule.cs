﻿using Prism.Ioc;

namespace NodeView.Frameworks.Modularity
{
    public interface INodeViewModule
    {
        void RegisterTypes(IContainerRegistry containerRegistry);
        void OnCreatedShell(IContainerProvider containerProvider, object shell);
        void OnInitialized(IContainerProvider containerProvider);
    }
}
