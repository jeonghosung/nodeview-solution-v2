﻿using NodeView.DataModels;

namespace NodeView.Frameworks.ContentControls
{
    public interface IContentSource : IObject
    {
        string ContentType { get; set; }
        string Name { get; set; }
    }
}
