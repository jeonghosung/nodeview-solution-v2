﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using NodeView.Systems;
using NodeView.Utils;

namespace NodeView.Apps
{
    public class AppUtils
    {
        private static string _appId = "";
        private static Mutex _mutex = null;

        public static string AppId
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_appId))
                {
                    string[] splitVersions = StringUtils.Split(Application.ProductVersion, '.', 2);
                    string appKey = $"{Application.ProductName}:{splitVersions[0]}.{splitVersions[1]}";
                    byte[] keyBytes = CryptUtils.CreateMd5(SystemInfo.GetMachineKeyString() + appKey);

                    StringBuilder idStringBuilder = new StringBuilder();
                    idStringBuilder.Append("u");
                    idStringBuilder.Append(Convert.ToBase64String(keyBytes).Substring(0, 22));
                    idStringBuilder.Replace("+", "-");
                    idStringBuilder.Replace("/", "_");
                    _appId = idStringBuilder.ToString();
                }
                return _appId;
            }
        }

        public static string GetAppName()
        {
            List<string> splitNames = new List<string>();
            splitNames.AddRange(GetAppFileName().Split('.'));
            if (splitNames.Count > 1)
            {
                splitNames.RemoveAt(splitNames.Count - 1);
            }

            return string.Join(".", splitNames);
        }

        public static string GetAppFileName()
        {
            return AppDomain.CurrentDomain.FriendlyName;
        }
        public static string GetWorkingFolder()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

        public static string GetAppType()
        {
            string appName = GetAppName();
            string appType = appName.StartsWith("NodeView") ? appName.Substring(8) : appName;
            if (appType.EndsWith("Service"))
            {
                appType = appType.Substring(0, appType.Length - 7);
            }
            if (appType.EndsWith("Console"))
            {
                appType = appType.Substring(0, appType.Length - 7);
            }
            if (appType.EndsWith("Window"))
            {
                appType = appType.Substring(0, appType.Length - 6);
            }
            //todo : 윈도우 서비스 이름 변경후 제거
            if (appType.EndsWith("Windows"))
            {
                appType = appType.Substring(0, appType.Length - 7);
            }
            return appType;
        }

        public static bool IsDuplicateExecution()
        {
            string appName = GetAppFileName();
            try
            {
                _mutex = new Mutex(true, appName, out bool isCreatedNew);
                return !isCreatedNew;
            }
            catch
            {
                //skip
            }

            return true;
        }

        public static void Exit()
        {
            if (System.Windows.Forms.Application.MessageLoop)
            { 
                // WinForms app
                System.Windows.Forms.Application.Exit();

            } 
            else
            { 
                // Console app
                System.Environment.Exit(1);
            }
        }
    }
}
