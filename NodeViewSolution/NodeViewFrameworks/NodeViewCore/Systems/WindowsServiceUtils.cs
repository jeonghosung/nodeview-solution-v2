﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using NLog;

namespace NodeView.Systems
{
    public class WindowsServiceUtils
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static IntPtr Advap32Module;
        private static IntPtr UserenvModule;
        private static IntPtr Kernel32Module;
        private static IntPtr Wtsapi32module;

        #region Win32 Constants

        private const int CREATE_UNICODE_ENVIRONMENT = 0x00000400;
        private const int CREATE_NO_WINDOW = 0x08000000;
        private const int CREATE_NEW_CONSOLE = 0x00000010;
        private const uint INVALID_SESSION_ID = 0xFFFFFFFF;
        private static readonly IntPtr WTS_CURRENT_SERVER_HANDLE = IntPtr.Zero;


        private const int TOKEN_DUPLICATE = 0x0002;
        private const uint MAXIMUM_ALLOWED = 0x2000000;

        #endregion

        #region DllImports

        [DllImport("kernel32.dll", EntryPoint = "LoadLibrary")]
        private static extern IntPtr LoadLibrary(string librayName);

        [DllImport("kernel32.dll", EntryPoint = "GetProcAddress", CharSet = CharSet.Ansi)]
        private static extern IntPtr GetProcAddress(IntPtr hwnd, string procedureName);

        [DllImport("kernel32.dll", EntryPoint = "FreeLibrary")]
        private static extern bool FreeLibrary(IntPtr hModule);

        //advap32.dll
        [UnmanagedFunctionPointer(CallingConvention.StdCall, SetLastError = true, CharSet = CharSet.Ansi)]
        private delegate bool _CreateProcessAsUser(IntPtr hToken, String lpApplicationName, String lpCommandLine,
            IntPtr lpProcessAttributes, IntPtr lpThreadAttributes, bool bInheritHandle, uint dwCreationFlags,
            IntPtr lpEnvironment, String lpCurrentDirectory, ref STARTUPINFO lpStartupInfo,
            out PROCESS_INFORMATION lpProcessInformation);

        private static _CreateProcessAsUser CreateProcessAsUser;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private  delegate bool _DuplicateTokenEx(IntPtr ExistingTokenHandle, uint dwDesiredAccess, IntPtr lpThreadAttributes, int TokenType, int ImpersonationLevel, ref IntPtr DuplicateTokenHandle);

        private static _DuplicateTokenEx DuplicateTokenEx;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate bool _OpenProcessToken(IntPtr ProcessHandle, int DesiredAccess, ref IntPtr TokenHandle);

        private static _OpenProcessToken OpenProcessToken;

        //userenv.dll
        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate bool _CreateEnvironmentBlock(ref IntPtr lpEnvironment, IntPtr hToken, bool bInherit);

        private static _CreateEnvironmentBlock CreateEnvironmentBlock;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private delegate bool _DestroyEnvironmentBlock(IntPtr lpEnvironment);

        private static _DestroyEnvironmentBlock DestroyEnvironmentBlock;

        //kernel32.dll
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate  bool _CloseHandle(IntPtr hSnapshot);

        private static _CloseHandle CloseHandle;

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate uint _WTSGetActiveConsoleSessionId();

        private static _WTSGetActiveConsoleSessionId WTSGetActiveConsoleSessionId;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate IntPtr _OpenProcess(uint dwDesiredAccess, bool bInheritHandle, uint dwProcessId);

        private static _OpenProcess OpenProcess;
        //Wtsapi32.dll
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate uint _WTSQueryUserToken(uint SessionId, ref IntPtr phToken);

        private static _WTSQueryUserToken WTSQueryUserToken;

        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate uint _WTSEnumerateSessions(IntPtr hServer, int Reserved, int Version, ref IntPtr ppSessionInfo, ref int pCount);

        private static _WTSEnumerateSessions WTSEnumerateSessions;

        #endregion

        #region Win32 Structs

        private enum SW
        {
            SW_HIDE = 0,
            SW_SHOWNORMAL = 1,
            SW_NORMAL = 1,
            SW_SHOWMINIMIZED = 2,
            SW_SHOWMAXIMIZED = 3,
            SW_MAXIMIZE = 3,
            SW_SHOWNOACTIVATE = 4,
            SW_SHOW = 5,
            SW_MINIMIZE = 6,
            SW_SHOWMINNOACTIVE = 7,
            SW_SHOWNA = 8,
            SW_RESTORE = 9,
            SW_SHOWDEFAULT = 10,
            SW_MAX = 10
        }

        private enum WTS_CONNECTSTATE_CLASS
        {
            WTSActive,
            WTSConnected,
            WTSConnectQuery,
            WTSShadow,
            WTSDisconnected,
            WTSIdle,
            WTSListen,
            WTSReset,
            WTSDown,
            WTSInit
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct PROCESS_INFORMATION
        {
            public IntPtr hProcess;
            public IntPtr hThread;
            public uint dwProcessId;
            public uint dwThreadId;
        }

        private enum SECURITY_IMPERSONATION_LEVEL
        {
            SecurityAnonymous = 0,
            SecurityIdentification = 1,
            SecurityImpersonation = 2,
            SecurityDelegation = 3,
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct STARTUPINFO
        {
            public int cb;
            public String lpReserved;
            public String lpDesktop;
            public String lpTitle;
            public uint dwX;
            public uint dwY;
            public uint dwXSize;
            public uint dwYSize;
            public uint dwXCountChars;
            public uint dwYCountChars;
            public uint dwFillAttribute;
            public uint dwFlags;
            public short wShowWindow;
            public short cbReserved2;
            public IntPtr lpReserved2;
            public IntPtr hStdInput;
            public IntPtr hStdOutput;
            public IntPtr hStdError;
        }

        private enum TOKEN_TYPE
        {
            TokenPrimary = 1,
            TokenImpersonation = 2
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct WTS_SESSION_INFO
        {
            public readonly UInt32 SessionID;
            [MarshalAs(UnmanagedType.LPStr)] public readonly String pWinStationName;
            public readonly WTS_CONNECTSTATE_CLASS State;
        }

        #endregion

        public static bool IsLoadedLibrary { get; private set; } = false;
        public static bool InitLibrary()
        {

            if (IsLoadedLibrary)
            {
                return IsLoadedLibrary;
            }
            if (Environment.UserInteractive)
            {
                Logger.Error("Cannot use WindowsServiceUtils, It is not a Windows Service!");
                return IsLoadedLibrary;
            }

            try
            {
                Advap32Module = LoadLibrary("advapi32.dll");
                UserenvModule = LoadLibrary("userenv.dll");
                Kernel32Module = LoadLibrary("kernel32.dll");
                Wtsapi32module = LoadLibrary("wtsapi32.dll");
                IntPtr ptrFunction = IntPtr.Zero;

                ptrFunction = GetProcAddress(Advap32Module, "DuplicateTokenEx");
                DuplicateTokenEx =
                    (_DuplicateTokenEx)Marshal.GetDelegateForFunctionPointer<_DuplicateTokenEx>(ptrFunction);

                ptrFunction = GetProcAddress(Advap32Module, "CreateProcessAsUserA");
                CreateProcessAsUser =
                    (_CreateProcessAsUser)Marshal.GetDelegateForFunctionPointer(ptrFunction, typeof(_CreateProcessAsUser));

                ptrFunction = GetProcAddress(Advap32Module, "OpenProcessToken");
                OpenProcessToken =
                    (_OpenProcessToken)Marshal.GetDelegateForFunctionPointer(ptrFunction, typeof(_OpenProcessToken));

                ptrFunction = GetProcAddress(UserenvModule, "CreateEnvironmentBlock");
                CreateEnvironmentBlock =
                    (_CreateEnvironmentBlock)Marshal.GetDelegateForFunctionPointer(ptrFunction,
                        typeof(_CreateEnvironmentBlock));
                ptrFunction = GetProcAddress(UserenvModule, "DestroyEnvironmentBlock");
                DestroyEnvironmentBlock =
                    (_DestroyEnvironmentBlock)Marshal.GetDelegateForFunctionPointer(ptrFunction,
                        typeof(_DestroyEnvironmentBlock));

                ptrFunction = GetProcAddress(Kernel32Module, "CloseHandle");
                CloseHandle = (_CloseHandle)Marshal.GetDelegateForFunctionPointer(ptrFunction, typeof(_CloseHandle));
                ptrFunction = GetProcAddress(Kernel32Module, "WTSGetActiveConsoleSessionId");
                WTSGetActiveConsoleSessionId =
                    (_WTSGetActiveConsoleSessionId)Marshal.GetDelegateForFunctionPointer(ptrFunction,
                        typeof(_WTSGetActiveConsoleSessionId));
                ptrFunction = GetProcAddress(Kernel32Module, "OpenProcess");
                OpenProcess =
                    (_OpenProcess)Marshal.GetDelegateForFunctionPointer(ptrFunction, typeof(_OpenProcess));

                ptrFunction = GetProcAddress(Wtsapi32module, "WTSQueryUserToken");
                WTSQueryUserToken =
                    (_WTSQueryUserToken) Marshal.GetDelegateForFunctionPointer(ptrFunction, typeof(_WTSQueryUserToken));
                ptrFunction = GetProcAddress(Wtsapi32module, "WTSEnumerateSessionsA");
                WTSEnumerateSessions =
                    (_WTSEnumerateSessions) Marshal.GetDelegateForFunctionPointer(ptrFunction,
                        typeof(_WTSEnumerateSessions));
                IsLoadedLibrary = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "InitLibrary:");
                IsLoadedLibrary = false;
            }
            return IsLoadedLibrary;
        }

        public static void FreeLibrary()
        {
            IsLoadedLibrary = false;
            try
            {
                FreeLibrary(Advap32Module);
                FreeLibrary(UserenvModule);
                FreeLibrary(Kernel32Module);
                FreeLibrary(Wtsapi32module);
            }
            catch
            {
                //Skip.
            }
        }

        public static bool IsWindowsLogon()
        {
            if (!InitLibrary())
            {
                return false;
            }
            try
            {
                uint activeConsoleSessionId = WTSGetActiveConsoleSessionId();
                Process[] processes = Process.GetProcessesByName("winlogon");
                foreach (Process p in processes)
                {
                    if ((uint)p.SessionId == activeConsoleSessionId)
                    {
                        return 0 != (uint)p.Id;
                    }
                }
                Logger.Info($"Cannot find active winlogon");
            }
            catch (Exception e)
            {
                Logger.Error(e, "CheckSessionUserToken:");
            }

            return false;
        }

        public static int CreateProcessAsCurrentUser(string appPath, string argument = null, string workDir = null, bool visible = true)
        {
            var startInfo = new STARTUPINFO();
            var procInfo = new PROCESS_INFORMATION();
            var pEnvironment = IntPtr.Zero;
            int errorCode;
            int winlogonPid = 0;
            var hUserTokenDuplicate = IntPtr.Zero;
            var hwinlogonProcess = IntPtr.Zero;
            var hPwinlogonToken = IntPtr.Zero;

            if (!InitLibrary())
            {
                return 0;
            }

            // obtain the currently active session id; every logged on user in the system has a unique session id
            
            
            try
            {
                uint activeConsoleSessionId = WTSGetActiveConsoleSessionId();
                uint creationFlags = CREATE_UNICODE_ENVIRONMENT |
                                       (uint)(visible ? CREATE_NEW_CONSOLE : CREATE_NO_WINDOW);
                startInfo.wShowWindow = (short) (visible ? SW.SW_SHOW : SW.SW_HIDE);
                startInfo.lpDesktop = "winsta0\\default";
                startInfo.cb = Marshal.SizeOf(typeof(STARTUPINFO));

                // obtain the process id of the winlogon process that is running within the currently active session
                Process[] processes = Process.GetProcessesByName("winlogon");
                foreach (Process p in processes)
                {
                    if ((uint)p.SessionId == activeConsoleSessionId)
                    {
                        winlogonPid = p.Id;
                    }
                }
                if (winlogonPid == 0)
                {
                    Logger.Error("Not find activeConsoleSessionId.");
                    return 0;
                }
                // obtain a handle to the winlogon process
                hwinlogonProcess = OpenProcess(MAXIMUM_ALLOWED, false, (uint)winlogonPid);
                // obtain a handle to the access token of the winlogon process
                if (!OpenProcessToken(hwinlogonProcess, TOKEN_DUPLICATE, ref hPwinlogonToken))
                {
                    CloseHandle(hwinlogonProcess);
                    return 0;
                }
                // copy the access token of the winlogon process; the newly created token will be a primary token
                if (!DuplicateTokenEx(hPwinlogonToken, MAXIMUM_ALLOWED, IntPtr.Zero, (int)SECURITY_IMPERSONATION_LEVEL.SecurityIdentification, (int)TOKEN_TYPE.TokenPrimary, ref hUserTokenDuplicate))
                {
                    CloseHandle(hwinlogonProcess);
                    CloseHandle(hPwinlogonToken);
                    return 0;
                }

                if (!CreateProcessAsUser(hUserTokenDuplicate,
                    string.IsNullOrWhiteSpace(argument) ? appPath : null, // Application Name
                    string.IsNullOrWhiteSpace(argument) ? null : $"{appPath} {argument}", // Command Line
                    IntPtr.Zero,
                    IntPtr.Zero,
                    false,
                    creationFlags,
                    pEnvironment,
                    workDir, // Working directory
                    ref startInfo,
                    out procInfo))
                {
                    errorCode = Marshal.GetLastWin32Error();
                    string exMessage =
                        $"StartProcessAsCurrentUser: _CreateProcessAsUser failed. Error Code: {errorCode}, appPath:{appPath}, argument:{argument}";
                    Logger.Error("[CLIP.eForm.AgentV2.HostSvc] Error " + exMessage);
                    exMessage = string.Format(exMessage, errorCode, appPath, argument);
                    throw new Exception(exMessage);
                }
                errorCode = Marshal.GetLastWin32Error();
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateProcessAsCurrentUser:");
            }
            finally
            {
                CloseHandle(hwinlogonProcess);
                CloseHandle(hPwinlogonToken);
                CloseHandle(hUserTokenDuplicate);
                if (pEnvironment != IntPtr.Zero)
                {
                    DestroyEnvironmentBlock(pEnvironment);
                }
                CloseHandle(procInfo.hThread);
                CloseHandle(procInfo.hProcess);
                FreeLibrary();
            }
            return (int)procInfo.dwProcessId;
        }
    }
}
