﻿using System;
using System.Windows.Forms;
using NLog;
using NodeView.Drawing;

namespace NodeView.Systems
{
    public class SystemScreenUtils
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public static IntRect GetActualScreenRect(AreaAlignment screenAlignment, IntPoint screenOffset, IntSize screenSize)
        {
            IntRect virtualScreenRect = GetVirtualScreenRect();
            if (screenSize.IsEmpty)
            {
                screenSize = new IntSize(virtualScreenRect.Width, virtualScreenRect.Height);
            }

            switch (screenAlignment)
            {
                case AreaAlignment.Manual:
                    break;
                case AreaAlignment.LeftTop:
                    screenOffset = new IntPoint(virtualScreenRect.X, virtualScreenRect.Y);
                    break;
                case AreaAlignment.RightTop:
                    screenOffset = new IntPoint(virtualScreenRect.Right - screenSize.Width, virtualScreenRect.Y);
                    break;
                case AreaAlignment.LeftBottom:
                    screenOffset = new IntPoint(virtualScreenRect.X, virtualScreenRect.Bottom - screenSize.Height);
                    break;
                case AreaAlignment.RightBottom:
                    screenOffset = new IntPoint(virtualScreenRect.Right - screenSize.Width, virtualScreenRect.Bottom - screenSize.Height);
                    break;
                default:
                    Logger.Error($"[{screenAlignment}] is not supported screenAlignment!");
                    break;
            }

            IntRect screenRect = new IntRect(screenOffset, screenSize);
            screenRect.Intersect(virtualScreenRect);
            return screenRect;
        }

        public static IntRect GetVirtualScreenRect()
        {
            int sx = 0;
            int sy = 0;
            int ex = 0;
            int ey = 0;
            bool isFistScreen = true;
            foreach (var screen in Screen.AllScreens)
            {
                if (isFistScreen)
                {
                    sx = screen.Bounds.X;
                    sy = screen.Bounds.Y;
                    ex = screen.Bounds.Right;
                    ey = screen.Bounds.Bottom;
                    isFistScreen = false;
                }
                else
                {
                    sx = Math.Min(sx, screen.Bounds.X);
                    sy = Math.Min(sy, screen.Bounds.Y);
                    ex = Math.Max(ex, screen.Bounds.Right);
                    ey = Math.Max(ey, screen.Bounds.Bottom);
                }
            }

            return new IntRect(sx, sy, ex - sx, ey - sy);
        }
    }
}
