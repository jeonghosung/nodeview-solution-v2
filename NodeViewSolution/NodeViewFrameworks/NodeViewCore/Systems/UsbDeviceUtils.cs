﻿using System.Collections.Generic;
using System.Management;

namespace NodeView.Systems
{
    public class UsbDeviceUtils
    {
        public static UsbDeviceInfo[] GetUsbDevices()
        {
            List<UsbDeviceInfo> usbDeviceInfos = new List<UsbDeviceInfo>();
            ManagementObjectCollection collection = null;
            try
            {
                //using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_USBHub"))
                using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_DiskDrive WHERE InterfaceType='USB'"))
                {
                    collection = searcher.Get();
                }

                foreach (var device in collection)
                {
                    usbDeviceInfos.Add(new UsbDeviceInfo()
                    {
                        Id = (string)device.GetPropertyValue("DeviceID"),
                        PnpId = (string)device.GetPropertyValue("PNPDeviceID"),
                        Description = (string)device.GetPropertyValue("Description"),
                    });
                }
            }
            finally
            {
                collection?.Dispose();
            }

            return usbDeviceInfos.ToArray();
        }
    }

    public class UsbDeviceInfo
    {
        public string Id { get; set; }
        public string PnpId { get; set; }
        public string Description { get; set; }
    }
}
