﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;
using NodeView.Utils;

namespace NodeView.Systems
{
    public class SystemInfo
    {
        private static string _machineKeyString = "";
        private static byte[] _machineKey = null;
        private static string _simpleMachineKey = "";

        public static string GetSimpleMachineKey()
        {
            if (string.IsNullOrWhiteSpace(_simpleMachineKey))
            {
                uint checkSum32 = 19740405;
                ushort checkSum16 = 0302;
                var hashBytes = GetUniqueMachineKey();
                for (int index = 0; index < 4; index++)
                {
                    checkSum32 += BitConverter.ToUInt32(hashBytes, index * 4);
                }

                for (int index = 0; index < 8; index++)
                {
                    checkSum16 += BitConverter.ToUInt16(hashBytes, index * 2);
                }

                List<byte> byteArray = new List<byte>();
                byteArray.AddRange(BitConverter.GetBytes(checkSum32));
                byteArray.AddRange(BitConverter.GetBytes(checkSum16));
                StringBuilder keyStringBuilder = new StringBuilder();
                keyStringBuilder.Append(Convert.ToBase64String(byteArray.ToArray()));
                keyStringBuilder.Replace("+", "-");
                keyStringBuilder.Replace("/", "_");
                _simpleMachineKey = keyStringBuilder.ToString();
            }
            return _simpleMachineKey;
        }

        public static string GetMachineKeyString()
        {
            if (string.IsNullOrWhiteSpace(_machineKeyString))
            {
                var hashBytes = GetUniqueMachineKey();
                StringBuilder keyStringBuilder = new StringBuilder();
                keyStringBuilder.Append(Convert.ToBase64String(hashBytes).Substring(0, 22));
                keyStringBuilder.Replace("+", "-");
                keyStringBuilder.Replace("/", "_");
                _machineKeyString =  keyStringBuilder.ToString();
            }

            return _machineKeyString;
        }

        public static byte[] GetUniqueMachineKey()
        {
            if (_machineKey == null)
            {
                ManagementClass mc = new ManagementClass("win32_processor");
                ManagementObjectCollection moc = mc.GetInstances();
                List<string> cpuIdList = new List<string>();
                foreach (var o in moc)
                {
                    if (o is ManagementObject mo)
                    {
                        cpuIdList.Add(mo.Properties["processorID"].Value.ToString());
                    }
                }
                string cupid = string.Join("", cpuIdList);

                string guid = "";
                var filename = "C:\\Windows\\smk.nv";
                if (File.Exists(filename))
                {
                    guid = File.ReadAllText(filename).Trim();
                }
                else
                {
                    guid = Guid.NewGuid().ToString();
                    File.WriteAllText(filename, guid);
                    DateTime dt = new DateTime(2019, 5, 20, 16, 5, 0);
                    File.SetCreationTime(filename, dt);
                    File.SetLastWriteTime(filename, dt);
                    File.SetLastAccessTime(filename, dt);
                }

                _machineKey = CryptUtils.CreateMd5(cupid + guid);
            }
            return _machineKey;
        }
    }
}
