﻿using System;
using System.Collections.Generic;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.Stations;
using NodeView.Utils;

namespace NodeView.DeviceControllers
{
    public abstract class WallControllerBase: DeviceControllerBase, IWallController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IntSize _screenSize = new IntSize(1920, 1080);
        private IntSize _matrix = new IntSize(1, 1);

        public IntSize ScreenSize
        {
            get => _screenSize;
            set
            {
                _screenSize = value;
                _attributes["size"] = $"{value}";
            }
        }
        public IntSize Matrix
        {
            get => _matrix;
            set
            {
                _matrix = value;
                _attributes["matrix"] = $"{value}";
            }
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("createWindow", "윈도우 생성", new[]
            {
                new FieldDescription("id", "", "윈도우 ID (없으면 자동부여)", "string"),
                new FieldDescription("contentId", "", "컨텐츠 ID", "string"),
                new FieldDescription("windowRect", "0,0,720,540", "윈도우 RECT", "IntRect"),
            }));

            commandDescriptions.Add(new CommandDescription("closeWindow", "윈도우 닫기", new[]
            {
                new FieldDescription("id", "", "윈도우 ID", "string"),
            }));

            commandDescriptions.Add(new CommandDescription("closeAllWindows", "모든 윈도우 닫기"));

            commandDescriptions.Add(new CommandDescription("createPopup", "팝업 윈도우 생성", new[]
            {
                new FieldDescription("id", "", "윈도우 ID (없으면 자동부여)", "string"),
                new FieldDescription("contentId", "", "컨텐츠 ID", "string"),
                new FieldDescription("windowRect", "0,0,720,540", "윈도우 RECT", "IntRect"),
            }));

            commandDescriptions.Add(new CommandDescription("closePopup", "팝업 윈도우 닫기", new[]
            {
                new FieldDescription("id", "", "윈도우 ID", "string"),
            }));

            commandDescriptions.Add(new CommandDescription("closeAllPopups", "모든 팝업 윈도우 닫기"));

            commandDescriptions.Add(new CommandDescription("applyPreset", "프리셋 적용", new[]
            {
                new FieldDescription("id", "", "프리셋 ID", "string"),
            }));

            return commandDescriptions.ToArray();
        }

        protected WallControllerBase()
        {
            DeviceType = "wall";
        }

        
        protected override bool SetAttribute(string key, string value)
        {
            if (!base.SetAttribute(key, value))
            {
                return false;
            }

            if (key == "size")
            {
                var size = StringUtils.GetValue(value, new IntSize(0, 0));
                if (!size.IsEmpty)
                {
                    ScreenSize = size;
                }
            }
            else if(key == "matrix")
            {
                var matrix = StringUtils.GetValue(value, new IntSize(0, 0));
                if (!matrix.IsEmpty)
                {
                    Matrix = matrix;
                }
            }

            return true;
        }
        
        public abstract IDataWallWindow CreateWindow(string id, string contentId, IntRect windowRect);

        public abstract void CloseWindow(string id);

        public abstract void CloseAllWindows();

        public abstract IDataWallWindow CreatePopup(string id, string contentId, IntRect windowRect);

        public abstract void MovePopup(string id, IntRect windowRect);

        public abstract void ClosePopup(string id);

        public abstract void CloseAllPopups();

        public abstract bool ApplyPreset(string id);
        
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "createWindow", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", Uuid.NewUuid);
                    string contentId = parameters.GetValue("contentId", "");
                    IntRect windowRect = parameters.GetValue("windowRect", IntRect.Empty);
                    if (string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
                    {
                        Logger.Warn("contentId or rect is empty.");
                        return false;
                    }
                    var window = CreateWindow(id, contentId, windowRect);
                    isHandled = (window != null);
                }
                else if (string.Compare(command, "closeWindow", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (string.IsNullOrWhiteSpace(id))
                    {
                        Logger.Warn("id is empty.");
                        return false;
                    }
                    CloseWindow(id);
                    isHandled = true;
                }
                else if (string.Compare(command, "closeAllWindows", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    CloseAllWindows();
                    isHandled = true;
                }
                else if (string.Compare(command, "createPopup", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", Uuid.NewUuid);
                    string contentId = parameters.GetValue("contentId", "");
                    IntRect windowRect = parameters.GetValue("windowRect", IntRect.Empty);
                    if (string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
                    {
                        Logger.Warn("contentId or rect is empty.");
                        return false;
                    }
                    var window = CreatePopup(id, contentId, windowRect);
                    isHandled = (window != null);
                }
                else if (string.Compare(command, "closePopup", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (string.IsNullOrWhiteSpace(id))
                    {
                        Logger.Warn("id is empty.");
                        return false;
                    }
                    ClosePopup(id);
                    isHandled = true;
                }
                else if (string.Compare(command, "closeAllPopups", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    CloseAllPopups();
                    isHandled = true;
                }
                else if (string.Compare(command, "applyPreset", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (string.IsNullOrWhiteSpace(id))
                    {
                        Logger.Warn("id is empty.");
                        return false;
                    }
                    ApplyPreset(id);
                    isHandled = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
    }
}
