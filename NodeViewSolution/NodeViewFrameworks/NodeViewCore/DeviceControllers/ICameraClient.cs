﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.Frameworks.Stations;
using NodeViewCore.DataModels;

namespace NodeViewCore.DeviceControllers
{
    public enum FocusType
    {
        NEAR,
        FAR,
        AUTO
    }
    public interface ICameraClient : IDisposable
    {
        void ControlPanTilt(int panValue, int tiltValue);
        void ControlZoom(int zoomValue);
        PtzPreset[] GetPresets();
        void ApplyPreset(int presetId);
        void SetPreset(int presetId, string presetName);
        void GoHome();
        void SetFocus(FocusType focusType);
    }
}
