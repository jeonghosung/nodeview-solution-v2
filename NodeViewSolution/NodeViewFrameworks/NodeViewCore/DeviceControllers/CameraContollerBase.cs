﻿using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeViewCore.DataModels;

namespace NodeViewCore.DeviceControllers
{
    public abstract class CameraContollerBase : DeviceControllerBase, ICameraClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected CameraContollerBase()
        {
            DeviceType = "camera";
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("pantilt", "Camera 좌,우 / 상,하 이동", new[]
            {
                new FieldDescription("pan", "0", "좌우 이동 값(-,+)", "int"),
                new FieldDescription("tilt", "0", "상하 이동 값(-,+)", "int")
            }));

            commandDescriptions.Add(new CommandDescription("zoom", "Zoom 설정", new[]
            {
                new FieldDescription("zoom", "0", "Zoom 값(-,+)", "int"),
            }));

            return commandDescriptions.ToArray();
        }

        protected override bool LoadConfig(Configuration config)
        {
            if (!base.LoadConfig(config))
            {
                return false;
            }

            return true;
        }

        public abstract void ControlPanTilt(int panValue, int tiltValue);

        public abstract void ControlZoom(int zoomValue);
        public PtzPreset[] GetPresets()
        {
            throw new NotImplementedException();
        }

        public abstract void ApplyPreset(int presetId);
        public void SetPreset(int presetId, string presetName)
        {
            throw new NotImplementedException();
        }

        public void GoHome()
        {
            throw new NotImplementedException();
        }

        public void SetFocus(FocusType focusType)
        {
            throw new NotImplementedException();
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "pantilt", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int panValue = parameters.GetValue("pan", 0);
                    int tiltVaut = parameters.GetValue("tilt", 0);
                    if (panValue != 0 || tiltVaut != 0)
                    {
                        ControlPanTilt(panValue, tiltVaut);
                        isHandled = true;
                    }
                }
                else if (string.Compare(command, "zoom", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int zoomValue = parameters.GetValue("zoom", 0);
                    if (zoomValue != 0)
                    {
                        ControlZoom(zoomValue);
                        isHandled = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
    }
}
