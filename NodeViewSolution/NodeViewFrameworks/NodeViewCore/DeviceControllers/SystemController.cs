﻿using System;
using System.Collections.Generic;
using System.Threading;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;

namespace NodeView.DeviceControllers
{
    public class SystemController: DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public SystemController(string id)
        {
            Id = id;
            DeviceType = "system";
            Name = "SystemUtils";
            Description = "NodeView System Utils";
            ConnectionString = "";
            IsTestMode = false;
            IsVerbose = false;
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("sleep", "sleep ", new[]
            {
                new FieldDescription("value", "1", "초", "int")
            }));
            return commandDescriptions.ToArray();
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "sleep", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int sleepSec = parameters.GetValue("value", 0);
                    if (sleepSec > 0)
                    {
                        Thread.Sleep(TimeSpan.FromSeconds(sleepSec));
                        isHandled = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
    }
}
