﻿using NodeView.DataModels;
using NodeView.Drawing;
using NodeView.Frameworks.Stations;

namespace NodeView.DeviceControllers
{
    public interface IMonitorController : IDeviceController
    {
        IntSize Matrix { get; }
        MonitorInputSource[] InputSources { get; }

        void ControlPower(int monitorNumber, bool isOn);
        void ControlPowerAll(bool isOn);
        void ControlInputSource(int monitorNumber, int inputSource);
        void ControlInputSourceAll(int inputSource);
        void ControlInputSourceName(int monitorNumber, string inputSourceName);
        void ControlInputSourceNameAll(string inputSourceName);
        void ControlBacklight(int monitorNumber, int level); //level: 1~5 (가장 밝음 1, 가장 어두움5)
    }
}
