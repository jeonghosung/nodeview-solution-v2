﻿using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.Stations;

namespace NodeView.DeviceControllers
{
    public interface IWallController : IDeviceController
    {
        IntSize ScreenSize { get; }
        IntSize Matrix { get; }

        IDataWallWindow CreateWindow(string id, string contentId, IntRect windowRect);
        void CloseWindow(string id);
        void CloseAllWindows();

        IDataWallWindow CreatePopup(string id, string contentId, IntRect windowRect);
        void MovePopup(string id, IntRect windowRect);
        void ClosePopup(string id);
        void CloseAllPopups();

        bool ApplyPreset(string id);
    }
}
