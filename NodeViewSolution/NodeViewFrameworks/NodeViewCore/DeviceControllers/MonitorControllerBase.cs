﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Drawing;
using NodeView.Frameworks.Stations;
using NodeView.Utils;

namespace NodeView.DeviceControllers
{
    public abstract class MonitorControllerBase : DeviceControllerBase, IMonitorController
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        protected MonitorInputSource[] _inputSources = new MonitorInputSource[0];
        private IntSize _matrix = new IntSize(1, 1);

        public IntSize Matrix
        {
            get => _matrix;
            set
            {
                _matrix = value;
                _attributes["matrix"] = $"{value}";
            }
        }

        public MonitorInputSource[] InputSources
        {
            get => _inputSources;
            set
            {
                _inputSources = value;
                if (value == null || value.Length == 0)
                {
                    _attributes.Remove("inputSources");
                }
                else
                {
                    _attributes["inputSources"] = string.Join(",", value.Select(x => x.Name));
                }
            }
        }

        protected MonitorControllerBase()
        {
            DeviceType = "monitor";
        }
        
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("power", "전원 On/Off", new[]
            {
                new FieldDescription("seq", "0", "모니터 번호(0은 전체)", "int"),
                new FieldDescription("value", "on", "on/off", "enum",  "on,off")
            }));

            var inputSources = InputSources.ToArray();
            if (inputSources.Length > 0)
            {
                commandDescriptions.Add(new CommandDescription("inputSourceName", "입력소스 변경", new[]
                {
                    new FieldDescription("seq", "0", "모니터 번호(0은 전체)", "int"),
                    new FieldDescription("value", inputSources[0].Name, "inputSource 이름", "enum",  string.Join(",", inputSources.Select(x=>x.Name)))
                }));
            }
            commandDescriptions.Add(new CommandDescription("inputSource", "입력소스 변경", new[]
            {
                new FieldDescription("seq", "0", "모니터 번호(0은 전체)", "int"),
                new FieldDescription("value", "144", "inputSource Code 번호", "int")
            }));
            commandDescriptions.Add(new CommandDescription("backlight", "램프 밝기 변경", new[]
            {
                new FieldDescription("seq", "0", "모니터 번호(0은 전체)", "int"),
                new FieldDescription("value", "1", "(1~5) 가장 밝은 것부터 1", "int")
            }));

            return commandDescriptions.ToArray();
        }

        protected override bool LoadConfig(Configuration config)
        {
            if (!base.LoadConfig(config))
            {
                return false;
            }

            IntSize matrix = config.GetValue("matrix", new IntSize(0,0));
            if (!matrix.IsEmpty)
            {
                Matrix = matrix;
            }

            if (config.TryGetNode("inputSources", out var inputsSection))
            {
                List<MonitorInputSource> inputSources = new List<MonitorInputSource>();
                foreach (var property in inputsSection.Properties)
                {
                    var source = MonitorInputSource.CreateFrom(property);
                    if (source != null)
                    {
                        inputSources.Add(source);
                    }
                }
                InputSources = inputSources.ToArray();
            }

            return true;
        }

        protected override bool SetAttribute(string key, string value)
        {
            if(!base.SetAttribute(key, value))
            {
                return false;
            }

            if (key == "matrix")
            {
                var matrix = StringUtils.GetValue(value, new IntSize(0, 0));
                if (!matrix.IsEmpty)
                {
                    Matrix = matrix;
                }
            }

            return true;
        }

        public abstract void ControlPower(int monitorNumber, bool isOn);

        public void ControlPowerAll(bool isOn)
        {
            ControlPower(0, isOn);
        }

        public abstract void ControlInputSource(int monitorNumber, int inputSource);

        public void ControlInputSourceAll(int inputSource)
        {
            ControlInputSource(0, inputSource);
        }

        public void ControlInputSourceName(int monitorNumber, string inputSourceName)
        {
            MonitorInputSource inputSource = FindInputSource(inputSourceName);
            if (inputSource != null)
            {
                ControlInputSource(monitorNumber, inputSource.Value);
            }
        }

        public void ControlInputSourceNameAll(string inputSourceName)
        {
            MonitorInputSource inputSource = FindInputSource(inputSourceName);
            if (inputSource != null)
            {
                ControlInputSource(0, inputSource.Value);
            }
        }

        private MonitorInputSource FindInputSource(string inputSourceName)
        {
            inputSourceName = inputSourceName.ToLower();
            foreach (var inputSource in InputSources.ToArray())
            {
                if (inputSourceName == inputSource.Name.ToLower())
                {
                    return inputSource;
                }
            }

            return null;
        }

        public abstract void ControlBacklight(int monitorNumber, int inputSource);

        public void ControlBacklightAll(int level)
        {
            ControlBacklight(0, level);
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "power", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int seq = parameters.GetValue("seq", -1);
                    bool isOn = parameters.GetValue("value", "on") == "on";
                    if (seq >= 0)
                    {
                        ControlPower(seq, isOn);
                        isHandled = true;
                    }
                }
                else if (string.Compare(command, "inputSourceName", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int seq = parameters.GetValue("seq", -1);
                    string value = parameters.GetValue("value", "");
                    MonitorInputSource inputSource = FindInputSource(value);
                    if (inputSource != null && seq >= 0 && seq <= (Matrix.Width * Matrix.Height))
                    {
                        ControlInputSource(seq, inputSource.Value);
                        isHandled = true;
                    }
                }
                else if (string.Compare(command, "inputSource", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int seq = parameters.GetValue("seq", -1);
                    int value = parameters.GetValue("value", 0);
                    if (value > 0 && seq >= 0 && seq <= (Matrix.Width * Matrix.Height))
                    {
                        ControlInputSource(seq, value);
                        isHandled = true;
                    }
                }
                else if (string.Compare(command, "backlight", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    int seq = parameters.GetValue("seq", -1);
                    int value = parameters.GetValue("value", 0);
                    if (value > 0 && seq >= 0 && seq <= (Matrix.Width * Matrix.Height))
                    {
                        ControlBacklight(seq, value);
                        isHandled = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
    }
}
