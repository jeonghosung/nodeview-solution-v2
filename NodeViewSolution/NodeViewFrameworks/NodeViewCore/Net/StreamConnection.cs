﻿using System;

namespace NodeView.Net
{
    public class StreamConnection: IDisposable
    {
        public IStreamClient StreamClient { get; }
        private readonly bool _haveToClose;

        public StreamConnection(IStreamClient streamClient)
        {
            StreamClient = streamClient;
            if (!StreamClient.IsOpend)
            {
                StreamClient.Open();
                _haveToClose = true;
            }
            else
            {
                _haveToClose = false;
            }
        }
        public void Dispose()
        {
            if (_haveToClose)
            {
                StreamClient.Close();
            }
        }
    }
}
