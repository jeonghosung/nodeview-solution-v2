﻿using NLog;
using System;
using System.IO.Ports;


namespace NodeView.Net
{
    public class SerialPortStreamClient: IStreamClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private long _length = 0;
        private SerialPort _serialPort;

        public bool IsOpend => (_serialPort != null && _serialPort.IsOpen);
        public bool CanRead => true;
        public bool CanWrite => true;
        public long Length => _length;

        public string SerialPortName { get; set; }
        public int BaudRate { get; set; }
        public int ReadTimeout { get; set; }
        public int WriteTimeout { get; set; }

        public SerialPortStreamClient(string serialPortName, int baudRate = 9600, int readTimeout = 1000, int writeTimeout = 1000)
        {
            SerialPortName = serialPortName;
            BaudRate = baudRate;
            ReadTimeout = readTimeout;
            WriteTimeout = writeTimeout;
            _length = 0;
            _serialPort = new SerialPort();
        }

        public void Flush()
        {
        }

        public void ClearReceivedData()
        {
            if (IsOpend && _serialPort.BytesToRead>0)
            {
                byte[] dummy = new byte[_serialPort.BytesToRead];
                Read(dummy, 0, dummy.Length);
            }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
			if (!IsOpend)
            {
                return -1;
            }

            int bytesCanRead = _serialPort.BytesToRead;

            if (bytesCanRead == 0)
	        {
				return 0;
	        }
			
	        int readCount = -1;
			try
            {
                count = Math.Min(count, bytesCanRead);

                readCount = _serialPort.Read(buffer, offset, count);
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot read from the serial port('{SerialPortName}').");
                readCount = -1;
                Close();
            }
            return readCount;
        }

        public bool Write(byte[] buffer, int offset, int count)
        {
            if (!IsOpend)
            {
                return false;
            }

	        bool res = false;
			try
            {
                _serialPort.Write(buffer, offset, count);
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot write to the serial port('{SerialPortName}').");
                res = false;
                Close();
            }
            return res;
        }

        public void OnUpdatedProperties()
        {
        }

        public bool Open()
        {
            if (!IsOpend)
            {
                try
                {
                    _serialPort.PortName = SerialPortName;
                    _serialPort.BaudRate = BaudRate;
                    _serialPort.DataBits = 8;
                    _serialPort.Parity = Parity.None;
                    _serialPort.StopBits = StopBits.One;
                    _serialPort.ReadTimeout = ReadTimeout;
                    _serialPort.WriteTimeout = WriteTimeout;
                    _serialPort.Open();
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Cannot open serial port('{SerialPortName}').");
                }
            }

            return IsOpend;
        }

        public void Close()
        {
            if (IsOpend)
            {
                Flush();
                _serialPort.Close();
            }
        }

        public void Dispose()
        {
            Close();
            _serialPort?.Dispose();
            _serialPort = null;
        }
    }
}
