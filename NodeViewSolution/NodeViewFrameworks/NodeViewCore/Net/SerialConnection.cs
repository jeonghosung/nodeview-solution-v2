﻿using System;
using System.IO.Ports;

namespace NodeView.Net
{
    public class SerialConnection:IDisposable
    {
        public SerialPort SerialPort { get; }
        private readonly bool _haveToClose;

        public SerialConnection(SerialPort serialPort)
        {
            SerialPort = serialPort;
            if (!SerialPort.IsOpen)
            {
                SerialPort.Open();
                _haveToClose = true;
            }
            else
            {
                _haveToClose = false;
            }
        }
        public void Dispose()
        {
            if (_haveToClose)
            {
                SerialPort.Close();
            }
        }
    }
}
