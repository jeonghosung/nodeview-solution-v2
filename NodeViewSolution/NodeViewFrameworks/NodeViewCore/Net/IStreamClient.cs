namespace NodeView.Net
{
    public interface IStreamClient
    {
        bool IsOpend { get; }
        bool CanRead { get; }
        bool CanWrite { get; }

        long Length { get; }
        void Flush();
        void ClearReceivedData();
        
        int Read(byte[] buffer, int offset, int count);
        bool Write(byte[] buffer, int offset, int count);

        bool Open();
        void Close();
    }
}