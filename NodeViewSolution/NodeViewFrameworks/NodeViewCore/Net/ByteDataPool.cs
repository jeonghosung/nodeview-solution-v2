﻿using System;
using System.Collections.Generic;

namespace NodeView.Net
{
    public class ByteDataPool
    {
        private readonly List<ByteDataPoolBuffer> _bytesList = new List<ByteDataPoolBuffer>();

        public int Count()
        {
            int count = 0;
            lock (_bytesList)
            {
                foreach (ByteDataPoolBuffer bytesBuffer in _bytesList)
                {
                    count += bytesBuffer.Count;
                }
            }

            return count;
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            int emptyBufferCount = 0;
            int resCount = 0;
            lock (_bytesList)
            {
                foreach (var dataPoolBuffer in _bytesList)
                {
                    int readCount = dataPoolBuffer.Read(buffer, offset, count);
                    offset += readCount;
                    count -= readCount;
                    resCount += readCount;
                    if (count <= 0)
                    {
                        break;
                    }

                    emptyBufferCount++;
                }

                for (int i = 0; i < emptyBufferCount; i++)
                {
                    _bytesList.RemoveAt(0);
                }
            }

            return resCount;
        }

        public byte ReadByte()
        {
            byte[] readBuffer = new byte[1];
            int readCount = Read(readBuffer, 0, 1);
            if (readCount != 1)
            {
                return 0;
            }

            return readBuffer[0];
        }

        public int Peek(byte[] buffer, int offset, int count)
        {
            int resCount = 0;
            lock (_bytesList)
            {
                foreach (var dataPoolBuffer in _bytesList)
                {
                    int readCount = dataPoolBuffer.Peek(buffer, offset, count);
                    offset += readCount;
                    count -= readCount;
                    resCount += readCount;
                    if (count <= 0)
                    {
                        break;
                    }
                }
            }

            return resCount;
        }

        public byte PeekByte()
        {
            byte[] readBuffer = new byte[1];
            int readCount = Peek(readBuffer, 0, 1);
            if (readCount != 1)
            {
                return 0;
            }

            return readBuffer[0];
        }

        public void Append(byte[] bytes, int startIndex = 0, int count = -1)
        {
            if (bytes == null || bytes.Length == 0)
            {
                return;
            }

            if (count < 0)
            {
                count = bytes.Length - startIndex;
            }
            byte[] targetDataBytes = new byte[count];

            int copyLength = (Math.Min((bytes.Length - startIndex), targetDataBytes.Length));
            Buffer.BlockCopy(bytes, startIndex, targetDataBytes, 0, copyLength);

            lock (_bytesList)
            {
                _bytesList.Add(new ByteDataPoolBuffer(targetDataBytes));
            }
        }

        public byte[] ReadAll()
        {
            byte[] buffer = new byte[Count()];
            if (buffer.Length == 0)
            {
                return buffer;
            }

            Read(buffer, 0, buffer.Length);
            return buffer;
        }
    }

    internal class ByteDataPoolBuffer
    {
        private readonly byte[] _bytes;
        private int _offset = 0;
        private int _length = 0;

        public int Count => _bytes.Length - _offset;
        public bool IsEmpty => Count == 0;

        public ByteDataPoolBuffer(byte[] bytes)
        {
            if (bytes == null)
            {
                _offset = 0;
                _length = 0;
                _bytes = new byte[0];
            }
            else
            {
                _offset = 0;
                _length = bytes.Length;
                _bytes = bytes;
            }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            int readCount = Math.Min(_length - _offset, count);
            Buffer.BlockCopy(_bytes, _offset, buffer, offset, readCount);
            _offset += readCount;
            return readCount;
        }

        public int Peek(byte[] buffer, int offset, int count)
        {
            int readCount = Math.Min(_length - _offset, count);
            Buffer.BlockCopy(_bytes, _offset, buffer, offset, readCount);
            return readCount;
        }
    }
}
