﻿using System;
using System.Diagnostics;
using System.Net.Sockets;
using NLog;

#pragma warning disable 1591

namespace NodeView.Net
{
    public class TcpStreamClient: IStreamClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private long _length = 0;
        private TcpClient _tcpClient = null;
        private NetworkStream _tcpStream = null;
        private bool _isErrorConnection = false;
        private Stopwatch _connectedTimeStopwatch = new Stopwatch();

        public bool IsOpend => (_tcpClient!=null && _tcpClient.Client != null && _tcpClient.Client.Connected && _tcpStream != null);

        public bool CanRead
        {
            get
            {
                return IsOpend;
            }
        }

        public bool CanWrite
        {
            get
            {
                return IsOpend;
            }
        }

        public long Length => _length;

        public string Hostname { get; set; } = "127.0.0.1";
        public int PortNumber { get; set; } = 9000;
        public int ReadTimeout { get; set; } = 1000;
        public int WriteTimeout { get; set; } = 1000;

        public TcpStreamClient(string hostname, int portNumber, int readTimeout = 1000, int writeTimeout = 1000)
        {
            Hostname = hostname;
            PortNumber = portNumber;
            ReadTimeout = readTimeout;
            WriteTimeout = writeTimeout;
            _length = 0;
            _isErrorConnection = false;
        }

        public void OnUpdatedProperties()
        {
        }

        public void Flush()
        {
            if (IsOpend)
            {
                _tcpStream.Flush();
            }
        }

        public void ClearReceivedData()
        {
            if (CanRead && _tcpClient.Available>0)
            {
                byte[] dummy = new byte[_tcpClient.Available];
                _tcpStream.Read(dummy, 0, dummy.Length);
            }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            if (!CanRead)
            {
                return -1;
            }

            int readCount = -1;
            int bytesCanRead = _tcpClient.Available;
            if (bytesCanRead == 0)
            {
                return CheckConnectionAlive() ? 0 : -1;
            }

            try
            {
                count = Math.Min(count, bytesCanRead);
                readCount = _tcpStream.Read(buffer, offset, count);
            }
            catch (Exception e)
            {
                SocketException socketException = e.InnerException as SocketException;
                if (socketException != null && socketException.SocketErrorCode == SocketError.TimedOut)
                {
                    readCount = 0;
                }
                else
                {
                    Logger.Error(socketException, $"Cannot read from the tcp stream ('{Hostname}:{PortNumber}').");
                    readCount = -1;
                    Close();
                }
            }
            return readCount;

        }

        public bool Write(byte[] buffer, int offset, int count)
        {
            bool res = false;
            if (!CanWrite)
            {
                return false;
            }
            try
            {
                _tcpStream.Write(buffer, offset, count);
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot write to the tcp stream ('{Hostname}:{PortNumber}').");
                res = false;
                Close();
            }
            return res;
        }

        public bool Open()
        {
            if (!IsOpend)
            {
                try
                {
                    _tcpClient = new TcpClient();
                    _tcpClient.ReceiveTimeout = ReadTimeout;
                    _tcpClient.SendTimeout = WriteTimeout;
                    _tcpClient.Connect(Hostname, PortNumber);
                    _tcpStream = _tcpClient.GetStream();
                    _isErrorConnection = false;
                    _connectedTimeStopwatch.Restart();
                }
                catch (Exception e)
                {
                    Close();
                    if (!_isErrorConnection)
                    {
                        Logger.Error(e, $"Cannot open tcp stream('{Hostname}:{PortNumber}').");
                        _isErrorConnection = true;
                    }
                }
            }

            return IsOpend;
        }

        public void Close()
        {
            Flush();
            _tcpStream?.Close();
            _tcpClient?.Close();
            _tcpStream = null;
            _tcpClient = null;
        }

        public void Dispose()
        {
            Close();
        }

        public bool CheckConnectionAlive()
        {
            if (IsOpend)
            {
                bool part1 = _tcpClient.Client.Poll(1000, SelectMode.SelectRead);
                bool part2 = (_tcpClient.Client.Available == 0);
                if (part1 && part2)
                {
                    Logger.Warn("CheckConnectionAlive is False!");
                    Close();
                    return false;
                }
                return true;
            }
            return false;
        }

    }
}
