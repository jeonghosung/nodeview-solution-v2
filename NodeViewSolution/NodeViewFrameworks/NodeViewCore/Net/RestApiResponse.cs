﻿using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using NLog;

namespace NodeView.Net
{
    public class RestApiResponse : ApiResponse
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly JToken _responseToken = null;
        public RestApiResponseType ResponseType { get; }

        public bool IsJsonResponse => (ResponseType == RestApiResponseType.JsonArray ||
                                       ResponseType == RestApiResponseType.JsonObject);
        public WebHeaderCollection Headers { get; }
        public CookieCollection Cookies { get; }
        public string Body { get; }
        public JToken ResponseJson => _responseToken ?? GetResponseJsonObject();
        public JObject ResponseJsonObject => GetResponseJsonObject();
        public JArray ResponseJsonArray => GetResponseJsonArray();


        public RestApiResponse(HttpStatusCode responseCode, string body = "", WebHeaderCollection headers = null, CookieCollection cookies = null) : base(responseCode)
        {
            Headers = headers ?? new WebHeaderCollection();
            Cookies = cookies ?? new CookieCollection();
            Body = string.IsNullOrWhiteSpace(body) ? "": body;
            _responseToken = null;
            ResponseType = RestApiResponseType.String;
            try
            {
                if (!string.IsNullOrWhiteSpace(body))
                {
                    _responseToken = JToken.Parse(Body);
                    if (_responseToken.Type == JTokenType.Array)
                    {
                        ResponseType = RestApiResponseType.JsonArray;
                    }
                    else if (_responseToken.Type == JTokenType.Object)
                    {
                        ResponseType = RestApiResponseType.JsonObject;
                    }
                    else
                    {
                        _responseToken = null;
                        ResponseType = RestApiResponseType.String;
                    }
                }
            }
            catch
            {
                _responseToken = null;
                ResponseType = RestApiResponseType.String;
            }
        }

        private JObject GetResponseJsonObject()
        {
            JObject resJsonObject = null;
            switch (ResponseType)
            {
                case RestApiResponseType.JsonObject:
                    resJsonObject = _responseToken.Value<JObject>();
                    break;
                case RestApiResponseType.JsonArray:
                    JArray array = _responseToken.Value<JArray>();
                    resJsonObject = new JObject();
                    resJsonObject["items"] = array;
                    break;
                default:
                    resJsonObject = new JObject();
                    resJsonObject["body"] = Body;
                    break;
            }

            return resJsonObject ?? new JObject();
        }


        private JArray GetResponseJsonArray()
        {
            JArray resJsonArray = null;
            switch (ResponseType)
            {
                case RestApiResponseType.JsonObject:
                    resJsonArray = new JArray();
                    resJsonArray.Add(_responseToken.Value<JObject>());
                    break;
                case RestApiResponseType.JsonArray:
                    resJsonArray = _responseToken.Value<JArray>();
                    break;
                default:
                    resJsonArray = new JArray();
                    var resJsonObject = new JObject();
                    resJsonObject["body"] = Body;
                    resJsonArray.Add(resJsonObject);
                    break;
            }

            return resJsonArray ?? new JArray();
        }

        public static RestApiResponse CreateFrom(HttpWebResponse response)
        {
            if (response == null)
            {
                return new RestApiResponse(HttpStatusCode.ExpectationFailed);
            }

            string characterSet = response.CharacterSet;
            Encoding encoding = string.IsNullOrWhiteSpace(characterSet)
                ? Encoding.UTF8
                : Encoding.GetEncoding(characterSet);
            HttpStatusCode statusCode = response.StatusCode;
            WebHeaderCollection headers = new WebHeaderCollection();
            foreach (string key in response.Headers)
            {
                headers[key] = response.Headers[key];
            }
            CookieCollection cookies = new CookieCollection();
            foreach (Cookie cookie in response.Cookies)
            {
                cookies.Add(cookie);
            }

            string body = "";
            try
            {
                using (Stream stReadData = response.GetResponseStream())
                {
                    if (stReadData != null)
                    {
                        using (StreamReader srReadData = new StreamReader(stReadData, encoding ?? Encoding.UTF8))
                        {
                            body = srReadData.ReadToEnd();
                        }
                    }
                }
            }
            catch
            {
                statusCode = HttpStatusCode.ExpectationFailed;
            }
            return new RestApiResponse(statusCode, body, headers, cookies);
        }
    }

    public enum RestApiResponseType
    {
        JsonObject,
        JsonArray,
        String
    }
}