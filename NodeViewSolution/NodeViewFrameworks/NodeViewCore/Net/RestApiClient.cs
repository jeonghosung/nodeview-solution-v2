﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Newtonsoft.Json.Linq;
using NLog;

namespace NodeView.Net
{
    public class RestApiClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string UserAgent { get; set; } = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.140 Safari/537.36 Edge/18.17763";
        public string Accept { get; set; } = "text/html, application/xhtml+xml, application/json, image/jxr, */*";
        public string Referer { get; set; } = "";
        //public string ContentType { get; set; } = "text/plain";
        public bool IsAddingCharsetOnContentType { get; set; } = true;
        public Encoding Encoding { get; set; } = Encoding.UTF8;
        public int Timeout { get; set; } = 3000;
        public string BaseUrl { get; set; }
        public ICredentials Credentials { get; set; } = null;
        public WebHeaderCollection Headers { get; set; } = new WebHeaderCollection();


        public RestApiClient(string baseUrl)
        {
            BaseUrl = baseUrl.Trim().TrimEnd('/');
        }

        public RestApiResponse Get(string path, string payload = "", string contentType= "text/plain")
        {
            string requestUri = GetRequestUrl(path);
            return HttpRequest(requestUri, "GET", payload, contentType, Encoding);
        }
        
        public RestApiResponse Get(string path, JToken jsonPayload)
        {
            string payload = jsonPayload?.ToString() ?? "{}";
            string requestUri = GetRequestUrl(path);
            return HttpRequest(requestUri, "GET", payload, "application/json", Encoding);
        }
        /*
        public RestApiResponse Get(string path, Dictionary<string, string> formDataPayload)
        {
            string payload = GetFormDataPayload(formDataPayload) ?? "";
            string requestUri = GetRequestUrl(path);
            return HttpRequest(requestUri, "GET", payload, "application/x-www-form-urlencoded", Encoding);
        }
        */
        public RestApiResponse Get(string path, Dictionary<string, object> queryStringDic, string payload = "", string contentType = "text/plain")
        {
            string requestUri = GetRequestUrl(path, queryStringDic);
            return HttpRequest(requestUri, "GET", payload, contentType, Encoding);
        }
        public RestApiResponse Get(string path, Dictionary<string, object> queryStringDic, JToken jsonPayload)
        {
            string payload = jsonPayload?.ToString() ?? "{}";
            string requestUri = GetRequestUrl(path, queryStringDic);
            return HttpRequest(requestUri, "GET", payload, "application/json", Encoding);
        }

        public RestApiResponse Get(string path, Dictionary<string, object> queryStringDic, Dictionary<string, string> formDataPayload)
        {
            string payload = GetFormDataPayload(formDataPayload) ?? "";
            string requestUri = GetRequestUrl(path, queryStringDic);
            return HttpRequest(requestUri, "GET", payload, "application/x-www-form-urlencoded", Encoding);
        }

        public RestApiResponse Post(string path, JToken jsonPayload)
        {
            string payload = jsonPayload?.ToString() ?? "{}";
            return Post(path, null, payload, "application/json");
        }

        public RestApiResponse Post(string path, Dictionary<string, string> formDataPayload)
        {
            string payload = GetFormDataPayload(formDataPayload) ?? "";
            return Post(path, null, payload, "application/x-www-form-urlencoded");
        }

        public RestApiResponse Post(string path, string payload, string contentType = "text/plain")
        {
            return Post(path, null, payload, contentType);
        }

        public RestApiResponse Post(string path, Dictionary<string, object> queryStringDic, string payload, string contentType = "text/plain")
        {
            string requestUri = GetRequestUrl(path, queryStringDic);
            return HttpRequest(requestUri, "POST", payload, contentType, Encoding);
        }
        public RestApiResponse Put(string path, JToken jsonPayload)
        {
            string payload = jsonPayload?.ToString() ?? "{}";
            return Put(path, null, payload, "application/json");
        }

        public RestApiResponse Put(string path, Dictionary<string, string> formDataPayload)
        {
            string payload = GetFormDataPayload(formDataPayload) ?? "";
            return Put(path, null, payload, "application/x-www-form-urlencoded");
        }

        public RestApiResponse Put(string path, string payload, string contentType = "text/plain")
        {
            return Put(path, null, payload, contentType);
        }
        public RestApiResponse Put(string path, Dictionary<string, object> queryStringDic, string payload, string contentType = "text/plain")
        {
            string requestUri = GetRequestUrl(path, queryStringDic);
            return HttpRequest(requestUri, "PUT", payload, contentType, Encoding);
        }

        public RestApiResponse Delete(string path)
        {
            return Delete(path, null, "");
        }

        public RestApiResponse Delete(string path, JToken jsonPayload)
        {
            string payload = jsonPayload?.ToString() ?? "{}";
            return Delete(path, null, payload, "application/json");
        }

        public RestApiResponse Delete(string path, Dictionary<string, string> formDataPayload)
        {
            string payload = GetFormDataPayload(formDataPayload) ?? "";
            return Delete(path, null, payload, "application/x-www-form-urlencoded");
        }

        public RestApiResponse Delete(string path, string payload, string contentType = "text/plain")
        {
            return Delete(path, null, payload, contentType);
        }

        public RestApiResponse Delete(string path, Dictionary<string, object> queryStringDic, string payload = "", string contentType = "text/plain")
        {
            string requestUri = GetRequestUrl(path, queryStringDic);
            return HttpRequest(requestUri, "DELETE", payload, contentType, Encoding);
        }

        private string GetRequestUrl(string path, Dictionary<string, object> queryStringDic = null)
        {
            path = path.Trim();

            
            string requestUrl = "";
            if (string.IsNullOrWhiteSpace(BaseUrl))
            {
                requestUrl = $"{path}";
            }
			else
			{
                if (string.IsNullOrWhiteSpace(path))
                {
                    requestUrl = $"{BaseUrl}";
                }
                else
                {
                    path = path.TrimStart('/');
                    requestUrl = $"{BaseUrl}/{path}";
                }
            }

            string queryString = "";
            if (queryStringDic != null && queryStringDic.Any())
            {
                List<string> queryList = new List<string>();
                foreach (var q in queryStringDic)
                {
                    string name = HttpUtility.UrlEncode($"{q.Key}");
                    string value = HttpUtility.UrlEncode($"{q.Value}");
                    queryList.Add($"{name}={value}");
                }

                queryString = string.Join("&", queryList);

                if (requestUrl.Contains("?"))
                {
                    if (!requestUrl.EndsWith("?"))
                    {
                        queryString = "&" + queryString;
                    }
                }
                else
                {
                    queryString = "?" + queryString;
                }
            }


            return requestUrl + queryString;
        }
        private string GetFormDataPayload(Dictionary<string,string> formDataDictionary)
        {
            if (formDataDictionary == null || formDataDictionary.Count == 0)
            {
                return "";
            }

            List<string> formDataList = new List<string>();
            foreach (var keyValuePair in formDataDictionary)
            {
                string name = HttpUtility.UrlEncode($"{keyValuePair.Key}");
                string value = HttpUtility.UrlEncode($"{keyValuePair.Value}");
                formDataList.Add($"{name}={value}");
            }
            
            return string.Join("&", formDataList);
        }

        private RestApiResponse HttpRequest(string requestUri, string method, string payload, string contentType, Encoding encoding, int timeout = 0)
        {
            RestApiResponse restApiResponse = null;

            HttpWebResponse response = null;
            try
            {
                method = method.ToUpper();
                if (encoding == null)
                {
                    encoding = Encoding.UTF8;
                }

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(requestUri);
                if (Headers != null && Headers.Count > 0)
                {
                    for (int i = 0; i < Headers.Count; i++)
                    {
                        String key = Headers.GetKey(i);
                        String[] values = Headers.GetValues(key);
                        if (values != null && values.Length > 0)
                        {
                            string value = string.Join(",", values);
                            request.Headers[key] = value;
                        }
                    }
                }

                request.UserAgent = UserAgent;
                
                if (timeout <= 0)
                {
                    timeout = Timeout;
                }
                request.Timeout = timeout;
                request.ReadWriteTimeout = timeout;

                request.Accept = Accept;
                if (!string.IsNullOrWhiteSpace(Referer))
                {
                    request.Referer = Referer;
                }
                if(Credentials != null)
                {
                    request.Credentials = Credentials;
                }
                
                if (method == "POST" || method == "PUT" || method == "DELETE")
                {
                    request.Method = method;

                    contentType = string.IsNullOrWhiteSpace(contentType) ? "application/x-www-form-urlencoded" : contentType;
                    if (IsAddingCharsetOnContentType)
                    {
                        request.ContentType = $"{contentType}; charset={encoding.WebName}";
                    }
                    else
                    {
                        request.ContentType = $"{contentType}";
                    }

                    if (string.IsNullOrWhiteSpace(payload))
                    {
                        request.ContentLength = 0;
                    }
                    else
                    {
                        byte[] byteArray = encoding.GetBytes(payload);
                        request.ContentLength = byteArray.Length;
                        Stream dataStream = request.GetRequestStream();
                        //dataStream.WriteTimeout = timeout;
                        dataStream.Write(byteArray, 0, byteArray.Length);
                        dataStream.Close();
                    }
                }
                else
                {
                    contentType = string.IsNullOrWhiteSpace(contentType) ? "text/plain" : contentType;
                    if (IsAddingCharsetOnContentType)
                    {
                        request.ContentType = $"{contentType}; charset={encoding.WebName}";
                    }
                    else
                    {
                        request.ContentType = $"{contentType}";
                    }
                }

                try
                {
                    response = request.GetResponse() as HttpWebResponse;
                }
                catch (WebException ex)
                {
                    response = ex.Response as HttpWebResponse;
                }

                if (response != null)
                {
                    restApiResponse = RestApiResponse.CreateFrom(response);
                }
                else
                {
                    restApiResponse = new RestApiResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot http request for '{requestUri}'.");
                restApiResponse = new RestApiResponse(HttpStatusCode.RequestTimeout);
            }
            finally
            {
                response?.Close();
            }

            return restApiResponse ?? new RestApiResponse(HttpStatusCode.PreconditionFailed);
        }
    }
}
