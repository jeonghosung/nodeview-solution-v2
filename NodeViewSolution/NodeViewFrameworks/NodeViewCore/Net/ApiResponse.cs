﻿using System.Net;
using Newtonsoft.Json.Linq;

namespace NodeView.Net
{
    public class ApiResponse
    {
        public static ApiResponse BadRequest => new ApiResponse(HttpStatusCode.BadRequest);
        public static ApiResponse Success => new ApiResponse(HttpStatusCode.OK);
        public HttpStatusCode ResponseCode { get; }
        public bool IsSuccess => ((int)ResponseCode / 100 == 2);
        public string Message { get; set; }

        public ApiResponse(HttpStatusCode responseCode, string message = "")
        {
            ResponseCode = responseCode;
            Message = message;
        }
    }
}
