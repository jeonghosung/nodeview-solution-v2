﻿using NLog;
using System;
using System.IO.Ports;
using System.Text;
using Newtonsoft.Json.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;


namespace NodeView.Net
{
    public class SerialJsonPort : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private SerialPort _serialPort = new SerialPort();
        private List<byte> _payloadBytes = new List<byte>();
        private volatile bool _isPayloadParsing = false;

        public bool IsOpen => (_serialPort?.IsOpen ?? false);

        public string PortName
        {
            get => _serialPort.PortName;
            set => _serialPort.PortName = value;
        }

        public int BaudRate
        {
            get => _serialPort.BaudRate;
            set => _serialPort.BaudRate = value;
        }
        public int ReadTimeout
        {
            get => _serialPort.ReadTimeout;
            set => _serialPort.ReadTimeout = value;
        }
        public int WriteTimeout
        {
            get => _serialPort.WriteTimeout;
            set => _serialPort.WriteTimeout = value;
        }

        public delegate void JsonReceivedEventHandler(object sender, JsonReceivedEventArgs args);

        public event JsonReceivedEventHandler? JsonReceived;
        public event SerialErrorReceivedEventHandler ErrorReceived;

        public SerialJsonPort()
        {
            _serialPort.DataReceived += SerialPortOnDataReceived;
            _serialPort.ErrorReceived += SerialPortOnErrorReceived;
        }

        public SerialJsonPort(string portName, int baudRate = 9600)
        {
            PortName = portName;
            BaudRate = baudRate;
        }

        public void ClearReceivedData()
        {
            if (IsOpen && _serialPort.BytesToRead>0)
            {
                byte[] dummy = new byte[_serialPort.BytesToRead];
                _serialPort.Read(dummy, 0, dummy.Length);
            }
        }

        public void Write(string json)
        {
            try
            {
                List<byte> byteList = new List<byte>();
                string str = json.Trim();
                byteList.Add(0x02);
                byte[] payload = Encoding.UTF8.GetBytes(str);
                byteList.AddRange(payload);
                byteList.Add(0x03);

                byte[] packet = byteList.ToArray();
                _serialPort.Write(packet, 0, packet.Length);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Write:");
            }
        }


        public void Write(JToken json)
        {
            Write(json.ToString());
        }

        public bool Open()
        {
            if (!IsOpen)
            {
                try
                {
                    _serialPort.Open();
                    _payloadBytes.Clear();
                    _isPayloadParsing = false;
                }
                catch (Exception e)
                {
                    Logger.Error(e, $"Cannot open serial port('{PortName}', {BaudRate}).");
                }
            }

            return IsOpen;
        }

        public void Close()
        {
            if (IsOpen)
            {
                _serialPort.Close();
                _payloadBytes.Clear();
                _isPayloadParsing = false;
            }
        }

        public void Dispose()
        {
            Close();
            if (_serialPort != null)
            {
                _serialPort.DataReceived -= SerialPortOnDataReceived;
                _serialPort.ErrorReceived -= SerialPortOnErrorReceived;
                _serialPort.Dispose();
                _serialPort = null;
            }
        }

        private void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                if (_serialPort.IsOpen && _serialPort.BytesToRead > 0)
                {
                    byte[] received = new byte[_serialPort.BytesToRead];
                    int readCount = _serialPort.Read(received, 0, received.Length);
                    if (readCount > 0)
                    {
                        for (int index = 0; index < readCount; index++)
                        {
                            byte data = received[index];
                            if (_isPayloadParsing)
                            {
                                if (data == 0x03)
                                {
                                    byte[] payload = _payloadBytes.ToArray();
                                    _isPayloadParsing = false;
                                    _payloadBytes.Clear();
                                    Task.Factory.StartNew(() => OnReceivedPayload(payload));
                                }
                                else if (data == 0x02) // 패킷이 깨어졌다 다시 시작 하자
                                {
                                    int skipBytesCount = _payloadBytes.Count;
                                    Logger.Error($"payload restarted(skipBytes={skipBytesCount}).");
                                    _payloadBytes.Clear();
                                }
                                else
                                {
                                    _payloadBytes.Add(data);
                                }
                            }
                            else
                            {
                                if (data == 0x02)
                                {
                                    _isPayloadParsing = true;
                                    _payloadBytes.Clear();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "SerialPortOnDataReceived:");
            }
        }

        private void OnReceivedPayload(byte[] payloadBytes)
        {
            try
            {
                string payload = Encoding.UTF8.GetString(payloadBytes).TrimEnd();
                JsonReceived?.Invoke(this, new JsonReceivedEventArgs(payload));
            }
            catch (Exception e)
            {
                Logger.Error(e, "OnReceivedPayload:");
            }
        }

        private void SerialPortOnErrorReceived(object sender, SerialErrorReceivedEventArgs e)
        {
            if (ErrorReceived != null)
            {
                ErrorReceived.Invoke(this, e);
                return;
            }
            SerialError err = e.EventType;
            string strErr = "";

            switch (err)
            {
                case SerialError.Frame:
                    strErr = "HardWare Framing Error";
                    break;
                case SerialError.Overrun:
                    strErr = "Charaters Buffer Over Run";
                    break;
                case SerialError.RXOver:
                    strErr = "Input Buffer OverFlow";
                    break;
                case SerialError.RXParity:
                    strErr = "Founded Parity Error";
                    break;
                case SerialError.TXFull:
                    strErr = "Write Buffer was Fulled";
                    break;
                default:
                    strErr = e.EventType.ToString();
                    break;
            }
            Logger.Error("SerialPortOnErrorReceived:" + strErr);
        }
    }

    public class JsonReceivedEventArgs
    {
        public JsonReceivedEventDataType DataType { get; }
        public JToken? JsonToken { get; }
        public string Payload { get; }

        internal JsonReceivedEventArgs(string payload)
        {
            DataType = JsonReceivedEventDataType.String;
            Payload = payload ?? "";
            if (string.IsNullOrEmpty(Payload))
            {
                JsonToken = null;
                return;
            }
            try
            {
                if (!string.IsNullOrWhiteSpace(payload))
                {
                    JsonToken = JToken.Parse(payload);
                    if (JsonToken.Type == JTokenType.Array)
                    {
                        DataType = JsonReceivedEventDataType.JsonArray;
                    }
                    else if (JsonToken.Type == JTokenType.Object)
                    {
                        DataType = JsonReceivedEventDataType.JsonObject;
                    }
                    else
                    {
                        JsonToken = null;
                        DataType = JsonReceivedEventDataType.String;
                    }
                }
            }
            catch
            {
                JsonToken = null;
                DataType = JsonReceivedEventDataType.String;
            }
        }

        private JObject GetResponseJsonObject()
        {
            JObject resJsonObject = null;
            switch (DataType)
            {
                case JsonReceivedEventDataType.JsonObject:
                    resJsonObject = JsonToken.Value<JObject>();
                    break;
                case JsonReceivedEventDataType.JsonArray:
                    JArray array = JsonToken.Value<JArray>();
                    resJsonObject = new JObject();
                    resJsonObject["items"] = array;
                    break;
                default:
                    resJsonObject = new JObject();
                    resJsonObject["payload"] = Payload;
                    break;
            }

            return resJsonObject ?? new JObject();
        }


        private JArray GetResponseJsonArray()
        {
            JArray resJsonArray = null;
            switch (DataType)
            {
                case JsonReceivedEventDataType.JsonObject:
                    resJsonArray = new JArray();
                    resJsonArray.Add(JsonToken.Value<JObject>());
                    break;
                case JsonReceivedEventDataType.JsonArray:
                    resJsonArray = JsonToken.Value<JArray>();
                    break;
                default:
                    resJsonArray = new JArray();
                    var resJsonObject = new JObject();
                    resJsonObject["payload"] = Payload;
                    resJsonArray.Add(resJsonObject);
                    break;
            }

            return resJsonArray ?? new JArray();
        }
    }

    public enum JsonReceivedEventDataType
    {
        JsonObject,
        JsonArray,
        String
    }
}
