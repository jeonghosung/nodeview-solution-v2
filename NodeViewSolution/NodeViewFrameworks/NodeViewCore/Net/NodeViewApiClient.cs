﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;

namespace NodeView.Net
{
    public class NodeViewApiClient : RestApiClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string UserToken { get; }
        public string AccessToken { get; private set; } = "";

        public bool HasAccessToken => !string.IsNullOrWhiteSpace(AccessToken);

        public NodeViewApiClient(string baseUrl, string userToken = "") : base(baseUrl)
        {
            //Todo: UserToken을 APP별로 할지 새션별로 할지 고민 해야함
            //UserToken = StringUtils.GetStringValue(userToken, AppUtils.AppId);
            UserToken = StringUtils.GetStringValue(userToken, Uuid.NewUniqueKey);
            Headers.Add("userToken", UserToken);
        }


        public bool Login(string password)
        {
            if (string.IsNullOrWhiteSpace(password)) return false;

            JObject requestJson = new JObject()
            {
                ["password"] = password
            };
            var response = Post("/auth/login", requestJson);
            if (response.IsSuccess)
            {
                KeyValueCollection responseHeaders = KeyValueCollection.CreateFrom(response.Headers);
                AccessToken = responseHeaders.GetStringValue("accessToken");
                if (string.IsNullOrWhiteSpace(AccessToken))
                {
                    Logger.Error("login was success, but accessToken is empty.");
                    return false;
                }
                Headers.Remove("accessToken");
                Headers.Add("accessToken", AccessToken);
                return true;
            }

            return false;
        }

        public void Logout()
        {
            var response = Get("/auth/logout");
            if (response.IsSuccess)
            {
                Headers.Remove("accessToken");
            }
        }

        public bool ResetPassword()
        {
            var response = Get("/auth/resetPassword");
            return response.IsSuccess;
        }

        public bool ChangePassword(string currentPassword, string newPassword)
        {
            JObject requestJson = new JObject()
            {
                ["currentpassword"] = currentPassword,
                ["newpassword"] = newPassword
            };
            var response = Post("/auth/changePassword", requestJson);
            return response.IsSuccess;

        }
    }
}
