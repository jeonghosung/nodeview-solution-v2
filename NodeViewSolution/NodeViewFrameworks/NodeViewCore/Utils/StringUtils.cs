﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace NodeView.Utils
{
    public static class StringUtils
    {
        public static T GetValue<T>(object value, T defaultValue)
        {
            if (value == null)
            {
                return defaultValue;
            }

            string stringValue = "";
            if (value is string sValue)
            {
                stringValue = sValue;
            }
            else
            {
                stringValue = $"{value}";
            }

            if (string.IsNullOrWhiteSpace(stringValue) && defaultValue != null)
            {
                return defaultValue;
            }

            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                return (T)converter.ConvertFromString(stringValue);
            }
            catch
            {
                //skip
            }
            return defaultValue;
        }
        public static string GetStringValue(object value, string defaultValue = "", bool useTrim = true)
        {
            return GetStringValue($"{value}", defaultValue, useTrim);
        }
        public static string GetStringValue(string value, string defaultValue = "", bool useTrim = true)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return defaultValue;
            }

            if (useTrim)
            {
                return value.Trim();
            }
            return value;
        }

        public static double GetDoubleValue(object value, double defaultValue = 0)
        {
            return GetValue(value, defaultValue);
        }

        public static int GetIntValue(object value, int defaultValue = 0)
        {
            return GetValue(value, defaultValue);
        }

        public static bool GetBoolValue(object value, bool defaultValue = false)
        {
            return GetValue(value, defaultValue);
        }

        public static byte[] GetStringToByteArray(string input, Encoding encoding = null)
        {
            if (input == null)
            {
                return null;
            }

            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }

            int nByteCount = encoding.GetByteCount(input);
            if (nByteCount < 1)
            {
                return null;
            }
            byte[] buf = new byte[nByteCount];
            byte[] tempBuf = encoding.GetBytes(input);
            Array.Copy(tempBuf, buf, nByteCount);
            return buf;
        }

        private static readonly Regex ReservedKeyRexEx = new Regex(@"^_[A-Za-z0-9_\-]*$");
        public static bool IsReservedKey(string input)
        {
            return string.IsNullOrWhiteSpace(input) || ReservedKeyRexEx.Match(input).Success;
        }

        private static readonly Regex IdRexEx = new Regex(@"^[A-Za-z][A-Za-z0-9_\-]*$");
        public static bool IsId(string input)
        {
            return string.IsNullOrWhiteSpace(input) || IdRexEx.Match(input).Success;
        }

        private static readonly Regex KeyRexEx = new Regex(@"^[A-Za-z0-9_\-]+$");
        public static bool IsKey(string input)
        {
            return string.IsNullOrWhiteSpace(input) || KeyRexEx.Match(input).Success;
        }

        private static readonly Regex NumberRexEx = new Regex(@"^(-)*[0-9]+([.][0-9]+)?$");
        public static bool IsNumber(string input)
        {
            return string.IsNullOrWhiteSpace(input) || NumberRexEx.Match(input).Success;
        }
        private static readonly Regex IntegerRexEx = new Regex(@"^(-)*[0-9]*$");
        public static bool IsInteger(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return true;

            return IntegerRexEx.Match(input).Success;
        }

        public static string GetIso8601String(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss zzz");
        }
        public static string GetIso8601UtcString(DateTime dateTime)
        {
            DateTime utc = dateTime.ToUniversalTime();
            return utc.ToString("yyyy-MM-ddTHH:mm:ssZ");
        }
        public static string GetIso8601ExString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-ddTHH:mm:ss.ttt zzz");
        }
        public static string GetIso8601ExUtcString(DateTime dateTime)
        {
            DateTime utc = dateTime.ToUniversalTime();
            return utc.ToString("yyyy-MM-ddTHH:mm:ss.tttZ");
        }

        public static string GetDateTimeMillisecondString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy.MM.dd HH:mm:ss.ff");
        }

        public static string GetDateTimeMillisecondString()
        {
            return GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        public static string GetDateTimeSecString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy.MM.dd HH:mm:ss");
        }
        public static string GetDateTimeMinString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy.MM.dd HH:mm");
        }

        public static string GetDateTimeSecString()
        {
            return GetDateTimeString(CachedDateTime.Now);
        }

        public static string GetDateTimeString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy.MM.dd HH:mm:ss");
        }

        public static string GetCurrentDateTimeString()
        {
            return GetDateTimeString(CachedDateTime.Now);
        }
        public static string GetCurrentTimeMillisecondString()
        {
            return GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        public static string GetDateString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy.MM.dd");
        }

        public static string GetCurrentDateString()
        {
            return GetDateTimeString(CachedDateTime.Now);
        }

        public static string ToFirstCharLower(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return "";
            }

            return source.Substring(0, 1).ToLower() + source.Substring(1);
        }

        public static string ToFirstCharLower(object obj)
        {
            string source = GetStringValue(obj);
            if (string.IsNullOrWhiteSpace(source))
            {
                return "";
            }
            return source.Substring(0, 1).ToLower() + source.Substring(1);
        }

        public static string ToFirstCharUpper(string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return "";
            }

            return source.Substring(0, 1).ToUpper() + source.Substring(1);
        }

        public static string ToFirstCharUpper(object obj)
        {
            string source = GetStringValue(obj);
            if (string.IsNullOrWhiteSpace(source))
            {
                return "";
            }

            return source.Substring(0, 1).ToUpper() + source.Substring(1);
        }

        public static bool ParseToBool(string boolString)
        {
            bool retBool = false;
            if (!string.IsNullOrWhiteSpace(boolString))
            {
                boolString = boolString.ToLower();

                switch (boolString)
                {
                    case "true":
                    case "t":
                    case "yes":
                    case "y":
                    case "on":
                    case "o":
                        retBool = true;
                        break;
                }
            }

            return retBool;
        }

        public static string GetTimeString(int time)
        {
            int h;
            int m;
            int s;

            if (time < 1000)
            {
                return $"{time}ms";
            }
            if (time < 60 * 1000)
            {
                return $"{time / 1000}s {time % 1000}ms";
            }
            time += 500;
            if (time < 60 * 60 * 1000)
            {
                m = time / (60 * 1000);
                s = (time - (m * 60 * 1000)) / 1000;
                return $"{m}m {s}s";
            }
            h = time / (60 * 60 * 1000);
            m = (time % (60 * 60 * 1000)) / (60 * 1000);
            s = (time % (60 * 1000)) / 1000;
            return $"{h}h {m}m {s}s";
        }

        public static string NormalizeToFileTitle(string data)
        {
            string temp = data.Trim();
            string[] split =
                temp.Split(new char[] { ' ', '\\', '/', ':', '*', '?', '\"', '<', '>', '|', '\r', '\n', '\t', '.' });
            return string.Join("_", split);
        }
        public static T ToEnum<T>(string value, T defaultValue) where T : struct
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return defaultValue;
            }

            return Enum.TryParse(value, true, out T result) ? result : defaultValue;
        }
        
        public static string[] Split(string input, char separator, int minArrayCount = 0)
        {
            List<string> stringList = new List<string>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                string[] splitStrings = input.Split(separator);
                foreach (string splitString in splitStrings)
                {
                    if (string.IsNullOrWhiteSpace(splitString))
                    {
                        stringList.Add("");
                    }
                    else
                    {
                        stringList.Add(splitString.Trim());
                    }
                }
            }

            for (int index = stringList.Count; index < minArrayCount; index++)
            {
                stringList.Add("");
            }
            return stringList.ToArray();
        }
        public static string[] DigestSplit(string input, char[] separators, int minArrayCount = 0)
        {
            List<string> stringList = new List<string>();
            if (!string.IsNullOrWhiteSpace(input))
            {
                string[] splitStrings = input.Split(separators);
                foreach (string splitString in splitStrings)
                {
                    if (!string.IsNullOrWhiteSpace(splitString))
                    {
                        stringList.Add(splitString.Trim());
                    }
                }
            }

            for (int index = stringList.Count; index < minArrayCount; index++)
            {
                stringList.Add("");
            }
            return stringList.ToArray();
        }

        public static (string, string) SplitFirst(string input, char delimiter)
        {
            string first = "";
            string other = "";

            var i = input.IndexOf(delimiter);

            if (i == -1)
            {
                first = input;
                other = "";
            }
            else
            {
                first = input.Substring(0, i);
                other = input.Substring(i + 1);
            }
            return (first, other);
        }
        public static (string, string) SplitFirst(string input, char[] separators)
        {
            string first = "";
            string other = "";

            var i = input.IndexOfAny(separators);

            if (i == -1)
            {
                first = input;
                other = "";
            }
            else
            {
                first = input.Substring(0, i);
                other = input.Substring(i + 1);
            }
            return (first, other);
        }
        public static (string, string) SplitLast(string input, char delimiter)
        {
            string other = "";
            string last = "";

            var i = input.LastIndexOf(delimiter);

            if (i == -1)
            {
                other = input;
                last = "";
            }
            else
            {
                other = input.Substring(0, i);
                last = input.Substring(i + 1);
            }
            return (other, last);
        }
        public static (string, string) SplitLast(string input, char[] separators)
        {
            string other = "";
            string last = "";

            var i = input.LastIndexOfAny(separators);

            if (i == -1)
            {
                other = input;
                last = "";
            }
            else
            {
                other = input.Substring(0, i);
                last = input.Substring(i + 1);
            }
            return (other, last);
        }

        public static byte[] GetBytesFromHexString(string hexStr)
        {
            var byteList = new List<byte>();

            for (int i = 0; i < hexStr.Length; i = i + 2)
            {
                byteList.Add(byte.Parse(
                    $"{hexStr[i]}{hexStr[i + 1]}",
                    NumberStyles.AllowHexSpecifier));
            }

            return byteList.ToArray();
        }


        public static object ConvertToDbTypeString(string data)
        {
            if (string.IsNullOrWhiteSpace(data))
            {
                return DBNull.Value;
            }

            return data;
        }

        public static string ToHexString(byte[] dataBytes, int startIndex = 0, int count = -1)
        {
            if (count < 0)
            {
                count = dataBytes.Length - startIndex;
            }
            byte[] targetDataBytes = new byte[count];

            int copyLength = (Math.Min((dataBytes.Length - startIndex), targetDataBytes.Length));
            Buffer.BlockCopy(dataBytes, startIndex, targetDataBytes, 0, copyLength);

            return BitConverter.ToString(targetDataBytes).Replace("-", string.Empty);
        }

        public static byte[] ToBytes(this string hexString)
        {
            int numberChars = hexString.Length;
            byte[] bytes = new byte[numberChars / 2];
            for (int i = 0; i < numberChars; i += 2)
            {
                bytes[i / 2] = Convert.ToByte(hexString.Substring(i, 2), 16);
            }

            return bytes;
        }

        public static (string, string) SplitPathToOthersAndLast(string path)
        {
            string others = "";
            string last = "";
            int charIndex = path.LastIndexOf('/');
            if (charIndex < 0)
            {
                last = path;
            }
            else
            {
                others = path.Substring(0, charIndex);
                last = path.Substring(charIndex + 1);
            }
            return (others, last);
        }

        public static (string, string) SplitPathToFirstAndOthers(string path)
        {
            string first = "";
            string others = "";
            int charIndex = path.IndexOf('/');
            if (charIndex < 0)
            {
                first = path;
            }
            else
            {
                first = path.Substring(0, charIndex);
                others = path.Substring(charIndex + 1);
            }
            return (first, others);
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
