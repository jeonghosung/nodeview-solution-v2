﻿using System;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace NodeView.Utils
{
    public static class XmlUtils
    {
        /*
        public static XmlWriterSettings GetXmlWriterSettings(Encoding encoding = null)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            
            settings.Encoding = encoding ?? Encoding.UTF8;
            return settings;
        }
        */
        public static XmlWriter CreateXmlWriter(string outputFileName, Encoding encoding = null, bool indent= true)
        {
            XmlWriter xmlWriter = null;
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings
                {
                    Encoding = encoding ?? Encoding.UTF8, Indent = indent
                };
                xmlWriter = XmlWriter.Create(outputFileName, settings);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return xmlWriter;
        }

        public static bool AttrContains(XmlNode node, string name)
        {
            return node?.Attributes?[name] != null;
        }
        public static T GetAttrValue<T>(XmlNode node, string name, T defaultValue)
        {
            string value;

            if (node?.Attributes == null)
            {
                value = "";
            }
            else
            {
                value = node.Attributes[name] == null ? "" : node.Attributes[name].Value;
            }

            return StringUtils.GetValue(value, defaultValue);
        }
        public static string GetStringAttrValue(XmlNode node, string name, string defaultValue = "", bool useTrim = true)
        {
            string stringValue = GetAttrValue(node, name, defaultValue);
            return useTrim ? stringValue.Trim() : stringValue;
        }

        public static int GetIntAttrValue(XmlNode node, string name, int defaultValue = 0)
        {
            return GetAttrValue(node, name, defaultValue);
        }

        public static double GetDoubleAttrValue(XmlNode node, string name, double defaultValue = 0)
        {
            return GetAttrValue(node, name, defaultValue);
        }

        public static bool GetBoolAttrValue(XmlNode node, string name, bool defaultValue = false)
        {
            return GetAttrValue(node, name, defaultValue);
        }

        public static T GetInnerText<T>(XmlNode node, string xpath, T defaultValue)
        {
            if (node == null)
            {
                return defaultValue;
            }
            string value = GetStringInnerText(node, xpath);

            return StringUtils.GetValue(value, defaultValue);
        }
        public static string GetStringInnerText(XmlNode node, string xpath, string defaultValue = "")
        {
            if (node == null)
            {
                return defaultValue??"";
            }
            string value = defaultValue;
            XmlNode selectedNode = node;
            if (!string.IsNullOrWhiteSpace(xpath))
            {
                selectedNode = node?.SelectSingleNode(xpath);
            }
            if (selectedNode != null)
            {
                value = selectedNode.InnerText.Trim();
            }
            return value;
        }

        public static int GetIntInnerText(XmlNode node, string xpath, int defaultValue = 0)
        {
            return GetInnerText(node, xpath, defaultValue);
        }

        public static double GetDoubleInnerText(XmlNode node, string xpath, double defaultValue = 0)
        {
            return GetInnerText(node, xpath, defaultValue);
        }

        public static bool GetBoolInnerText(XmlNode node, string xpath, bool defaultValue = false)
        {
            return GetInnerText(node, xpath, defaultValue);
        }
    }
}
