﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using NLog;
using NodeView.Drawing;
using static System.Resources.ResXFileRef;

namespace NodeView.Utils
{
    public static class JsonUtils
    {
        public static string SerializeObjectWithCamelCaseProperty<T>(T modelObject, bool indented=false)
        {
            var settings = new JsonSerializerSettings();
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            if (indented)
            {
                settings.Formatting = Formatting.Indented;
            }
            return JsonConvert.SerializeObject(modelObject, settings);
        }

        public static bool Contains(JObject jObject, string key)
        {
            if (jObject == null || string.IsNullOrWhiteSpace(key))
            {
                return false;
            }
            JToken lightValueJToken = jObject.SelectToken(@key);
            return lightValueJToken != null;
        }

        public static T ToEnum<T>(JObject jObject, string key, T defaultValue) where T : struct
        {
            string value = "";
            if (jObject == null || string.IsNullOrWhiteSpace(key))
            {
                return defaultValue;
            }

            JToken lightValueJToken = null;
            try
            {
                value = $"{jObject.SelectToken(@key)}";
            }
            catch
            {
                return defaultValue;
            }

            if (string.IsNullOrWhiteSpace(value))
            {
                return defaultValue;
            }

            return Enum.TryParse(value, true, out T result) ? result : defaultValue;
        }

        public static T GetValue<T>(JObject jObject, string key, T defaultValue)
        {
            T result = defaultValue;

            try
            {
                if (jObject == null || string.IsNullOrWhiteSpace(key))
                {
                    return defaultValue;
                }

                JToken lightValueJToken = null;
                try
                {
                    lightValueJToken = jObject.SelectToken(@key);
                }
                catch
                {
                    lightValueJToken = null;
                }
                if (lightValueJToken == null)
                {
                    return defaultValue;
                }

                try
                {
                    Type returnType = typeof(T);
                    if (lightValueJToken.Type == JTokenType.Array || returnType.IsArray)
                    {
                        T lightStatusValue = lightValueJToken.ToObject<T>();
                        return lightStatusValue;
                    }

                    string returnTypeNamespace = returnType.Namespace ?? "";
                    //string returnTypeName = returnType.Name;

                    switch (returnTypeNamespace)
                    {
                        case "System":
                        case "Newtonsoft.Json.Linq":
                        {
                            T lightStatusValue = lightValueJToken.Value<T>();
                            return lightStatusValue;
                        }
                        default:
                        {
                            var converter = TypeDescriptor.GetConverter(typeof(T));
                            return (T)converter.ConvertFromString($"{lightValueJToken}");
                        }
                    }
                }
                catch
                {
                    return StringUtils.GetValue($"{lightValueJToken}", defaultValue);
                }
            }
            catch
            {
                result = defaultValue;
            }

            return result;
        }

        public static string GetStringValue(JObject jObject, string key, string defaultValue = "", bool useTrim = true)
        {
            string stringValue = GetValue(jObject, key, defaultValue);
            return useTrim ? stringValue.Trim() : stringValue;
        }

        public static int GetIntValue(JObject jObject, string key, int defaultValue = 0)
        {
            return GetValue(jObject, key, defaultValue);
        }

        public static double GetDoubleValue(JObject jObject, string key, double defaultValue = 0)
        {
            return GetValue(jObject, key, defaultValue);
        }

        public static bool GetBoolValue(JObject jObject, string key, bool defaultValue = false)
        {
            return GetValue(jObject, key, defaultValue);
        }
    }
}
