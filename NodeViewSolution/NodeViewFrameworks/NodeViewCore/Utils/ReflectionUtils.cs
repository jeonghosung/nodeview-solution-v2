﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace NodeView.Utils
{
    public static class ReflectionUtils
    {
        public static IEnumerable<Type> GetLoadableTypes(this Assembly assembly)
        {
            if (assembly == null) throw new ArgumentNullException(nameof(assembly));
            try
            {
                return assembly.GetTypes();
            }
            catch (ReflectionTypeLoadException e)
            {
                return e.Types.Where(t => t != null);
            }
        }

        public static bool CanCreateInstance(this Type type, Type targetType, bool hasNullParameterConstructor = false)
        {
            if (!type.IsClass) return false;
            if (type.IsAbstract) return false;
            if (!targetType.IsAssignableFrom(type)) return false;
            return !hasNullParameterConstructor || type.GetConstructor(Type.EmptyTypes) != null;
        }

        public static bool CanCreateInstanceWithNullParameter(this Type type, Type targetType)
        {
            return CanCreateInstance(type, targetType, true);
        }
        public static bool HasNullParameterConstructor(this Type type)
        {
            return (type.GetConstructor(Type.EmptyTypes) != null);
        }

        public static bool CanInvoke(this MethodInfo method)
        {
            if (method.IsStatic) return true;
            if (method.IsAbstract) return false;

            var type = method.ReflectedType;
            if (type == null) return false;
            if (!type.IsClass) return false;
            if (type.IsAbstract) return false;

            return (true);
        }

        public static string GetPropertyStringValue(Type type, object instance, string name, string defaultValue = "")
        {
            string resValue = defaultValue;

            var propertyInfo = type.GetProperty(name);
            if (propertyInfo != null)
            {
                resValue = propertyInfo.GetValue(instance, null)?.ToString() ?? "";
            }
            return resValue;
        }
        public static int GetPropertyIntValue(Type type, object instance, string name, int defaultValue = 0)
        {
            int resValue = defaultValue;

            var propertyInfo = type.GetProperty(name);
            if (propertyInfo != null)
            {
                var value = propertyInfo.GetValue(instance, null);
                try
                {
                    resValue = Convert.ToInt32(value);
                }
                catch
                {
                    resValue = defaultValue;
                }
            }
            return resValue;
        }
        public static double GetPropertyDoubleValue(Type type, object instance, string name, double defaultValue = 0)
        {
            double resValue = defaultValue;

            var propertyInfo = type.GetProperty(name);
            if (propertyInfo != null)
            {
                var value = propertyInfo.GetValue(instance, null);
                try
                {
                    resValue = Convert.ToDouble(value);
                }
                catch
                {
                    resValue = defaultValue;
                }
            }
            return resValue;
        }

        public static bool IsInteger(object value)
        {
            return value is sbyte
                   || value is byte
                   || value is short
                   || value is ushort
                   || value is int
                   || value is uint
                   || value is long
                   || value is ulong;
        }
        public static bool IsNumber(object value)
        {
            return value is sbyte
                   || value is byte
                   || value is short
                   || value is ushort
                   || value is int
                   || value is uint
                   || value is long
                   || value is ulong
                   || value is float
                   || value is double
                   || value is decimal;
        }

    }
}
