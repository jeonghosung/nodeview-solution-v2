﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace NodeView.Utils
{
    public class CryptUtils
    {
        public static string CreateMd5HexString(string input)
        {
            return StringUtils.ToHexString(CreateMd5(input));
        }
        public static byte[] CreateMd5(string input)
        {
            using (MD5 md5 = MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(input);
                return md5.ComputeHash(inputBytes);
            }
        }

        public static string Decrypt(string key, byte[] iv, string input)
        {
            byte[] outputBuffer = null;
            using (RijndaelManaged aes = new RijndaelManaged())
            {
                try
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;
                    aes.Mode = CipherMode.CBC;
                    aes.Padding = PaddingMode.PKCS7;
                    aes.Key = CreateMd5(key);
                    aes.IV = iv;
                    var decrypt = aes.CreateDecryptor();
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
                        {
                            byte[] inputBytes = Convert.FromBase64String(input);
                            cs.Write(inputBytes, 0, inputBytes.Length);
                        }
                        outputBuffer = ms.ToArray();
                    }
                }
                catch
                {
                    outputBuffer = null;
                }
            }

            if (outputBuffer == null)
            {
                return "";
            }
            string output = Encoding.UTF8.GetString(outputBuffer);
            return output;
        }

        public static string Encrypt(string key, byte[] iv, string input)
        {
            byte[] outputBuffer = null;
            using (RijndaelManaged aes = new RijndaelManaged())
            {
                try
                {
                    aes.KeySize = 256;
                    aes.BlockSize = 128;
                    aes.Mode = CipherMode.CBC;
                    aes.Padding = PaddingMode.PKCS7;
                    aes.Key = CreateMd5(key);
                    aes.IV = iv;

                    var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);

                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
                        {
                            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
                            cs.Write(inputBytes, 0, inputBytes.Length);
                        }

                        outputBuffer = ms.ToArray();
                    }
                }
                catch
                {
                    outputBuffer = null;
                }
            }

            if (outputBuffer == null)
            {
                return "";
            }
            string output = Convert.ToBase64String(outputBuffer);
            return output;
        }
    }
}
