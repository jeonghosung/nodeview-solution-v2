﻿using System;
using System.Text;

namespace NodeView.Utils
{
    public class Uuid
    {
        public static string NewUuid
        {
            get
            {
                StringBuilder idStringBuilder = new StringBuilder();
                idStringBuilder.Append("u");
                idStringBuilder.Append(Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 22));
                idStringBuilder.Replace("+", "-");
                idStringBuilder.Replace("/", "_");
                return idStringBuilder.ToString();
            }
        }

        public static string NewUniqueKey
        {
            get
            {
                StringBuilder idStringBuilder = new StringBuilder();
                idStringBuilder.Append(Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 22));
                idStringBuilder.Replace("+", "-");
                idStringBuilder.Replace("/", "_");
                return idStringBuilder.ToString();
            }
        }
    }
}
