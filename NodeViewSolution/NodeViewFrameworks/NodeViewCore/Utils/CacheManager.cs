﻿using Newtonsoft.Json.Linq;
using NodeView.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeViewCore.Utils
{
    public class CacheManager
    {
        private Dictionary<string, string> _cacheDictionary = new Dictionary<string, string>();
        private string _filename;

        public CacheManager(string filename = "cache.json")
        {
            _filename = filename;
            Reload();
        }

        public void Set(string key, string value)
        {
            if (string.IsNullOrWhiteSpace(key)) return;
            if (value == null)
            {
                value = "";
            }

            if (_cacheDictionary.TryGetValue(key, out string cachedValue))
            {
                if (cachedValue == value)
                {
                    return;
                }
            }

            _cacheDictionary[key] = value;
            Save();
        }

        public T GetValue<T>(string key, T defaultValue)
        {
            if (string.IsNullOrWhiteSpace(key)) return defaultValue;

            T result = defaultValue;
            try
            {
                if (!string.IsNullOrWhiteSpace(key))
                {
                    if (_cacheDictionary.TryGetValue(key, out var value))
                    {
                        return StringUtils.GetValue(value, defaultValue);
                    }
                }
            }
            catch
            {
                result = defaultValue;
            }

            return result;
        }

        public void Reload()
        {
            if (File.Exists(_filename))
            {
                _cacheDictionary.Clear();
                string json = File.ReadAllText(_filename);
                JArray itemArray = JArray.Parse(json);
                foreach (var itemToken in itemArray)
                {
                    JObject item = itemToken.Value<JObject>();
                    if (item != null)
                    {
                        string name = item["name"]?.Value<string>() ?? "";
                        string value = item["value"]?.Value<string>() ?? "";
                        if (string.IsNullOrWhiteSpace(name) || string.IsNullOrWhiteSpace(value))
                        {
                            continue;
                        }
                        _cacheDictionary[name] = value;
                    }
                }
            }
        }

        public void Save()
        {
            JArray itemArray = new JArray();
            foreach (var itemKeyValue in _cacheDictionary)
            {
                JObject item = new JObject();
                item["name"] = itemKeyValue.Key;
                item["value"] = itemKeyValue.Value;
                itemArray.Add(item);
            }

            File.WriteAllText(_filename, itemArray.ToString());
        }
    }
}
