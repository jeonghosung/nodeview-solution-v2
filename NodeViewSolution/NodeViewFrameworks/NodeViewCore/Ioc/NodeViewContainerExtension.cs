﻿using Prism.Ioc;

namespace NodeView.Ioc
{
    public class NodeViewContainerExtension
    {
        private static IContainerExtension _instance = null;
        public static IContainerExtension CreateInstance(IContainerExtension containerExtension)
        {
            return _instance ?? (_instance = containerExtension);
        }

        public static IContainerRegistry GetContainerRegistry()
        {
            return _instance;
        }
        public static IContainerProvider GetContainerProvider()
        {
            return _instance;
        }
    }
}
