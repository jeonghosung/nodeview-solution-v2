﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.Net;
using Timer = System.Threading.Timer;

namespace NodeView.Protocols.NodeViews
{
    public class NodeViewCenterClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private RestApiClient _centerApiClient = null;
        private volatile bool _isStarted = false;
        private Timer _checkingLicenseTimer = null;
        private HttpStatusCode _lastLicenseStatusCode = HttpStatusCode.OK;
        private DateTime _lastLicenseSuccessTime = DateTime.Now;

        public bool IsShowErrorMessageBox { get; set; } = true;
        public string Ip { get; }
        public int Port { get; }
        public string AppIp { get; }
        public int AppPort { get; }
        public string AppName { get; }
        public string AppType { get; }
        public string AppApiBaseUri { get;}
        public string CenterApiBaseUri { get; }
        public int ProcessManagerPort { get; }

        public NodeViewCenterClient(string centerIp, int centerPort, string appIp, int appPort, int processManagerPort,string appName = "", string appType="")
        {
            Ip = centerIp;
            Port = centerPort;
            AppIp = appIp;
            AppPort = appPort;
            ProcessManagerPort = processManagerPort;
            AppName = string.IsNullOrWhiteSpace(appName) ? AppUtils.GetAppName() : appName;
            AppType = string.IsNullOrWhiteSpace(appType) ? AppUtils.GetAppType() : appType;
            AppApiBaseUri = $"http://{AppIp}:{AppPort}/api/v1";
            CenterApiBaseUri = $"http://{Ip}:{Port}/api/v1";
            _centerApiClient = new RestApiClient(CenterApiBaseUri);
            _checkingLicenseTimer = new Timer(OnCheckingLicenseTimer);
        }

        public bool Start()
        {
            if (!_isStarted)
            {
                if (_centerApiClient == null)
                {
                    return false;
                }

                _checkingLicenseTimer.Change(TimeSpan.FromSeconds(20), TimeSpan.FromMinutes(5));
                _isStarted = true;
            }
            return _isStarted;
        }

        public void Stop()
        {
            _checkingLicenseTimer.Change(Timeout.Infinite, Timeout.Infinite);
            _isStarted = true;
        }

        private void OnCheckingLicenseTimer(object state)
        {
            if (_isStarted)
            {
                _checkingLicenseTimer.Change(Timeout.Infinite, Timeout.Infinite);
                try
                {
                    var request = new JObject()
                    {
                        ["action"] = "keep-alive",
                        ["param"] = new JObject()
                        {
                            ["appKey"] = AppUtils.AppId,
                            ["appType"] = AppType,
                            ["name"] = AppName,
                            ["ip"] = AppIp,
                            ["port"] = $"{AppPort}",
                            ["uri"] = AppApiBaseUri,
                            ["processManagerPort"] = $"{ProcessManagerPort}"
                        }
                    };
                    var response = _centerApiClient.Put("/apps", request);

                    if (_lastLicenseStatusCode != response.ResponseCode)
                    {
                        if (_lastLicenseStatusCode == HttpStatusCode.OK)
                        {
                            Logger.Info($"LicenseStatus is changed [{_lastLicenseStatusCode}]->[{response.ResponseCode}]");
                        }
                        else
                        {
                            Logger.Error($"LicenseStatus is changed [{_lastLicenseStatusCode}]->[{response.ResponseCode}]");
                        }
                    }

                    if (response.IsSuccess)
                    {
                        _lastLicenseSuccessTime = DateTime.Now;
                    }
                    else
                    {
                        if(response.ResponseCode == HttpStatusCode.Forbidden)
                        {
                            string message = "노드뷰 접근이 거부되었습니다.\n관리자에게 문의 하세요.";
                            Logger.Error(message);
                            if (IsShowErrorMessageBox)
                            {
                                MessageBox.Show(message, "접근 거부", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                            AppUtils.Exit();
                        }
                        else
                        {
                            if (_lastLicenseStatusCode == HttpStatusCode.OK)
                            {
                                string message = "노드뷰 연결이 끊어졌습니다.\n관리자에게 문의 하세요.";
                                Logger.Error(message);
                                if (IsShowErrorMessageBox)
                                {
                                    MessageBox.Show(message, "연결 오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                if ((DateTime.Now - _lastLicenseSuccessTime).TotalMinutes > 60)
                                {
                                    string message = "노드뷰 연결 오류로 종료합니다.\n관리자에게 문의 하세요.";
                                    Logger.Error(message);
                                    if (IsShowErrorMessageBox)
                                    {
                                        MessageBox.Show(message, "연결 오류", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                    Application.Exit();
                                }
                            }
                        }
                    }
                    _lastLicenseStatusCode = response.ResponseCode;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "OnCheckingLicenseTimer:");
                }

                _checkingLicenseTimer.Change(TimeSpan.FromMinutes(5), TimeSpan.FromMinutes(5));
            }
        }

        public NodeViewAppNode[] GetApps()
        {
            List<NodeViewAppNode> appNodes = new List<NodeViewAppNode>();
            try
            {
                var response = _centerApiClient.Get("/apps");
                if (response.IsSuccess)
                {
                    foreach (var jsonToken in response.ResponseJsonArray)
                    {
                        var appJson = jsonToken.Value<JObject>();
                        var app = NodeViewAppNode.CreateFrom(appJson);
                        if (app != null)
                        {
                            if (app.AppType.Equals("center", StringComparison.OrdinalIgnoreCase))
                            {
                                app.Ip = Ip;
                                app.Port = Port;
                                app.Uri = $"http://{Ip}:{Port}/api/v1";
                            }
                            appNodes.Add(app);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetApps:");
            }

            return appNodes.ToArray();
        }
    }
}
