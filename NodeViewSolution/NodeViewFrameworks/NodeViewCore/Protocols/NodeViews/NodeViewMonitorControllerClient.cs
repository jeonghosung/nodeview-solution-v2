﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Net;

namespace NodeView.Protocols.NodeViews
{
    public class NodeViewMonitorControllerClient
    {
        public string DeviceBaseUri { get; }
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly RestApiClient _apiClient = null;

        public NodeViewMonitorControllerClient(string deviceBaseUri)
        {
            DeviceBaseUri = deviceBaseUri;
            _apiClient = new RestApiClient(deviceBaseUri);
        }

        public Device GetDevice()
        {
            try
            {
                var response = _apiClient.Get("");
                if (response.IsSuccess)
                {
                    return Device.CreateFrom(response.ResponseJsonObject);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetDevice:");
            }

            return null;
        }

        public bool ControlPower(int monitorNumber, bool isOn)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "power",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"{monitorNumber}",
                        ["value"] = isOn ? "on" : "off"
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlPower:");
            }

            return false;
        }

        public bool ControlPowerAll(bool isOn)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "power",
                    ["param"] = new JObject()
                    {
                        ["seq"] = "0",
                        ["value"] = isOn ? "on" : "off"
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlPowerAll:");
            }

            return false;
        }

        public bool ControlInputSource(int monitorNumber, int inputSource)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "inputSource",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"{monitorNumber}",
                        ["value"] = $"{inputSource}"
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSource:");
            }

            return false;
        }

        public bool ControlInputSourceAll(int inputSource)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "inputSource",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"0",
                        ["value"] = $"{inputSource}"
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSourceAll:");
            }

            return false;
        }

        public bool ControlInputSourceName(int monitorNumber, string inputSourceName)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "inputSourceName",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"{monitorNumber}",
                        ["value"] = inputSourceName
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSourceName:");
            }

            return false;
        }

        public bool ControlInputSourceNameAll(string inputSourceName)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "inputSourceName",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"0",
                        ["value"] = inputSourceName
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSourceNameAll:");
            }

            return false;
        }
        public bool ControlInputBackLight(int monitorNumber, string backLightValue)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "backlight",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"{monitorNumber}",
                        ["value"] = backLightValue
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSourceName:");
            }

            return false;
        }
        public bool ControlInputBackLightAll(string backLightValue)
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "backlight",
                    ["param"] = new JObject()
                    {
                        ["seq"] = $"{0}",
                        ["value"] = backLightValue
                    }
                };

                var response = _apiClient.Put("", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSourceName:");
            }

            return false;
        }
    }
}
