﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.Net;
using NodeView.Utils;
using Timer = System.Threading.Timer;

namespace NodeView.Protocols.NodeViews
{
    public class NodeViewProcessManagerClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly RestApiClient _processManagerApiClient = null;


        public NodeViewProcessManagerClient(string ip, int serverPort, string apiVersion = "v1")
        {
            _processManagerApiClient = new RestApiClient($"http://{ip}:{serverPort}/api/{apiVersion}");
        }

        public ProcessNode[] GetApps()
        {
            List<ProcessNode> processNodes = new List<ProcessNode>();
            try
            {
                var response = _processManagerApiClient.Get("/apps");
                if (response.IsSuccess)
                {
                    foreach (var jsonToken in response.ResponseJsonArray)
                    {
                        var appJson = jsonToken.Value<JObject>();
                        var app = ProcessNode.CreateFrom(appJson);
                        if (app != null)
                        {
                            processNodes.Add(app);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetApps:");
            }

            return processNodes.ToArray();
        }

        public ProcessNode GetApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;

            try
            {
                var response = _processManagerApiClient.Get($"/apps/{id}");
                if (response.IsSuccess)
                {
                    return ProcessNode.CreateFrom(response.ResponseJsonObject);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetApp:");
            }

            return null;
        }

        public ProcessNode RegisterApp(ProcessType processType, string name, string configFilename, string stopApiUrl = "", string argument = "")
        {
            try
            {
                var request = new JObject()
                {
                    ["action"] = "register",
                    ["param"] = new JObject()
                    {
                        ["id"] = AppUtils.AppId,
                        ["appType"] = AppUtils.GetAppType(),
                        ["name"] = name,
                        ["stopApiUrl"] = stopApiUrl,
                        ["processType"] = $"{processType}",
                        ["workingFolder"] = AppUtils.GetWorkingFolder(),
                        ["execution"] = processType == ProcessType.Service ? name: $"{AppUtils.GetAppFileName()}.exe",
                        ["argument"] = argument,
                        ["configFilename"] = configFilename
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    var app = ProcessNode.CreateFrom(response.ResponseJsonObject);
                    return app;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "RegisterApp:");
            }

            return null;
        }

        public ProcessNode StartApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;
            try
            {
                var request = new JObject()
                {
                    ["action"] = "start",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    var app = ProcessNode.CreateFrom(response.ResponseJsonObject);
                    return app;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StartApp:");
            }

            return null;
        }
        public ProcessNode StopApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;
            try
            {
                var request = new JObject()
                {
                    ["action"] = "stop",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    var app = ProcessNode.CreateFrom(response.ResponseJsonObject);
                    return app;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopApp:");
            }

            return null;
        }

        public ProcessNode RestartApp(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;
            try
            {
                var request = new JObject()
                {
                    ["action"] = "restart",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    var app = ProcessNode.CreateFrom(response.ResponseJsonObject);
                    return app;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "RestartApp:");
            }

            return null;
        }
        public Configuration GetConfig(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return null;
            try
            {
                var request = new JObject()
                {
                    ["action"] = "getConfig",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    var config = Configuration.CreateFrom(response.ResponseJsonObject);
                    return config;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetConfig:");
            }

            return null;
        }

        public bool SetConfig(string id, Configuration config)
        {
            if (string.IsNullOrWhiteSpace(id) || config == null) return false;

            try
            {
                var request = new JObject()
                {
                    ["action"] = "setConfig",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                        ["config"] = config.ToJson()
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                return response.IsSuccess;
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetConfig:");
            }

            return false;
        }

        public string[] GetLogFileNames(string id)
        {
            if (string.IsNullOrWhiteSpace(id)) return new string[0];

            try
            {
                var request = new JObject()
                {
                    ["action"] = "getLogFileNames",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    List<string> fileNameList = new List<string>();
                    foreach (var fileNameToken in response.ResponseJsonArray)
                    {
                        fileNameList.Add(fileNameToken.Value<string>());
                    }

                    return fileNameList.ToArray();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetLogFileNames:");
            }

            return new string[0];
        }

        public string GetLog(string id, string logFilename)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(logFilename)) return "";
            try
            {
                var request = new JObject()
                {
                    ["action"] = "getLog",
                    ["param"] = new JObject()
                    {
                        ["id"] = id,
                        ["fileName"] = logFilename
                    }
                };
                var response = _processManagerApiClient.Put("/apps", request);
                if (response.IsSuccess)
                {
                    var base64EncodedText = JsonUtils.GetStringValue(response.ResponseJsonObject, "log");
                    if (!string.IsNullOrWhiteSpace(base64EncodedText))
                    {
                        return StringUtils.Base64Decode(base64EncodedText);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetLogFile:");
            }

            return "";
        }
    }
}
