using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public class ModbusRtuReadInputRegistersFunction : ModbusRtuFunction, IModbusReadInputRegistersFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ushort StartAddress { get; }
        public ushort Length { get; }
        public override int ResponseHeaderByteSize { get; }

        public ModbusRtuReadInputRegistersFunction(byte deviceId, ushort startAddress, ushort length) : base(ModbusFunctionCodes.ReadInputRegisters, deviceId)
        {
            StartAddress = startAddress;
            Length = length;
            ResponseHeaderByteSize = 3;
        }

        public override byte[] BuildRequestPacket()
        {
            List<byte> packetData = new List<byte>();
            packetData.AddRange(ModbusUtility.GetBytes(StartAddress));
            packetData.AddRange(ModbusUtility.GetBytes(Length));
            return BuildRequestPacket(packetData.ToArray());
        }

        public bool TryParseResponse(byte[] packetBytes, out ushort[] datas)
        {
            bool res = true;
            int dataByteSize;
            int offset = ResponseHeaderByteSize;

            if (!TryParseResponseHeader(packetBytes, out dataByteSize))
            {
                Logger.Error("Packet Header is mismatched!.");
                res = false;
            }
            else
            {
                if (dataByteSize / 2 != Length)
                {
                    Logger.Error($"Request length ({Length}) != Respone Length({dataByteSize / 2}).");
                    res = false;
                }
            }

            if (res)
            {
                datas = new ushort[Length];
                for (int index = 0; index < Length; index++)
                {
                    datas[index] = ModbusUtility.GetUshort(packetBytes, offset);
                    offset += 2;
                }
            }
            else
            {
                datas = new ushort[0];
            }

            return res;
        }

        public override bool TryParseResponseHeader(byte[] headerBytes, out int dataByteSize)
        {
            dataByteSize = 0;
            if (headerBytes.Length < ResponseHeaderByteSize)
            {
                Logger.Warn($"readBytes length({headerBytes.Length}) < header size({ResponseHeaderByteSize}).");
                return false;
            }
            if (headerBytes[0] != DeviceId)
            {
                Logger.Warn($"DeviceId is mismatechecd [{headerBytes[0]} != {DeviceId}].");
                return false;
            }
            if (headerBytes[1] != (byte)FunctionCode)
            {
                Logger.Warn($"Function Code is mismatechecd [{headerBytes[1]} != {(byte)FunctionCode}].");
                return false;
            }
            dataByteSize = headerBytes[2];
            return true;
        }
    }
}