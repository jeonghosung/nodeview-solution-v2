using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public class ModbusTcpReadInputRegistersFunction : ModbusTcpFunction, IModbusReadInputRegistersFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ushort StartAddress { get; }
        public ushort Length { get; }

        public ModbusTcpReadInputRegistersFunction(byte deviceId, ushort startAddress, ushort length) : base(ModbusFunctionCodes.ReadInputRegisters, deviceId)
        {
            StartAddress = startAddress;
            Length = length;
        }

        public override byte[] BuildRequestPacket()
        {
            List<byte> packetData = new List<byte>();
            packetData.AddRange(ModbusUtility.GetBytes(StartAddress));
            packetData.AddRange(ModbusUtility.GetBytes(Length));
            return BuildRequestPacket(packetData.ToArray());
        }

        public bool TryParseResponse(byte[] packetBytes, out ushort[] datas)
        {
            bool res = true;
            int payloadLength;
            byte dataByteSize = 0;
            int offset = ResponseHeaderByteSize;
            datas = new ushort[0];

            if (!TryParseResponseHeader(packetBytes, out payloadLength))
            {
                Logger.Error("Packet Header is mismatched!.");
                return false;
            }

            if (packetBytes[offset] != DeviceId)
            {
                Logger.Warn($"DeviceId is mismatechecd [{packetBytes[offset]} != {DeviceId}].");
                return false;
            }
            offset++;
            if (packetBytes[offset] != (byte)FunctionCode)
            {
                Logger.Warn($"Function Code is mismatechecd [{packetBytes[offset]} != {(byte)FunctionCode}].");
                return false;
            }
            offset++;

            dataByteSize = packetBytes[offset];
            if (dataByteSize / 2 != Length)
            {
                Logger.Error($"Request length ({Length}) != Respone Length({payloadLength / 2}).");
                res = false;
            }
            offset++;

            if (res)
            {
                datas = new ushort[Length];
                for (int index = 0; index < Length; index++)
                {
                    datas[index] = ModbusUtility.GetUshort(packetBytes, offset);
                    offset += 2;
                }
            }
            else
            {
                datas = new ushort[0];
            }

            return res;
        }
    }
}