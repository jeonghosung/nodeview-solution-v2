﻿namespace NodeView.Protocols.Modbus
{
    public enum ModbusFunctionCodes :byte
    {
        ReadHoldingRegisters = 0x03,
        ReadInputRegisters = 0x04,
        WriteSingleRegister = 0x06,
        WriteMultipleRegisters = 0x10,
    }
}