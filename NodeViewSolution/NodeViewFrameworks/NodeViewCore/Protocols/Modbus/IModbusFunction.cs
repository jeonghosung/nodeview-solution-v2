﻿namespace NodeView.Protocols.Modbus
{
    public interface IModbusFunction
    {
        ModbusFunctionCodes FunctionCode { get; }
        byte DeviceId { get; }
        int ResponseHeaderByteSize { get; }

        byte[] BuildRequestPacket();

        bool TryParseResponseHeader(byte[] headerBytes, out int dataByteSize);
    }

    public interface IModbusReadHoldingRegistersFunction : IModbusFunction
    {
        bool TryParseResponse(byte[] packetBytes, out ushort[] datas);
    }

    public interface IModbusReadInputRegistersFunction : IModbusFunction
    {
        bool TryParseResponse(byte[] packetBytes, out ushort[] datas);
    }
    
    public interface IModbusWriteSingleRegisterFunction : IModbusFunction
    {
        bool TryParseResponse(byte[] packetBytes);
    }

    public interface IModbusWriteMultipleRegistersFunction : IModbusFunction
    {
        bool TryParseResponse(byte[] packetBytes);
    }
}