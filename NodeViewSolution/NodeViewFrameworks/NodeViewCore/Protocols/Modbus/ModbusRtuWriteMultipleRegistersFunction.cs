using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public class ModbusRtuWriteMultipleRegistersFunction : ModbusRtuFunction, IModbusWriteMultipleRegistersFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ushort StartAddress { get; }
        public ushort Length { get; }
        public override int ResponseHeaderByteSize { get; }
        public ushort[] WriteDatas { get; }

        public ModbusRtuWriteMultipleRegistersFunction(byte deviceId, ushort startAddress, ushort[] writeDatas) : base(ModbusFunctionCodes.WriteMultipleRegisters, deviceId)
        {
            StartAddress = startAddress;
            Length = (ushort)writeDatas.Length;
            WriteDatas = writeDatas;
            ResponseHeaderByteSize = 2;
        }


        public override byte[] BuildRequestPacket()
        {
            List<byte> packetData = new List<byte>();
            packetData.AddRange(ModbusUtility.GetBytes(StartAddress));
            packetData.AddRange(ModbusUtility.GetBytes(Length));
            packetData.Add((byte)(Length * 2));
            foreach (ushort data in WriteDatas)
            {
                packetData.AddRange(ModbusUtility.GetBytes(data));
            }

            return BuildRequestPacket(packetData.ToArray());
        }

        public bool TryParseResponse(byte[] packetBytes)
        {
            bool res = true;
            int dataByteSize;
            int offset = ResponseHeaderByteSize;

            if (!TryParseResponseHeader(packetBytes, out dataByteSize))
            {
                Logger.Error("Packet Header is mismatched!.");
                res = false;
            }
            else
            {
                if (dataByteSize != 4)
                {
                    Logger.Error($"Respone Length MUST BE 4, but ({dataByteSize}).");
                    res = false;
                }
            }

            if (res)
            {
                ushort startAddress = ModbusUtility.GetUshort(packetBytes, offset);
                if (startAddress != StartAddress)
                {
                    Logger.Error($"Response start address is Mismatched [{startAddress}/{StartAddress}]");
                    res = false;
                }
                offset += 2;
                ushort length = ModbusUtility.GetUshort(packetBytes, offset);
                if (length != Length)
                {
                    Logger.Error($"Response length is mismatched [{length}/{Length}]");
                    res = false;
                }
            }

            return res;
        }

        public override bool TryParseResponseHeader(byte[] headerBytes, out int dataByteSize)
        {
            dataByteSize = 0;
            if (headerBytes.Length < ResponseHeaderByteSize)
            {
                Logger.Warn($"readBytes length({headerBytes.Length}) < header size({ResponseHeaderByteSize}).");
                return false;
            }
            if (headerBytes[0] != DeviceId)
            {
                Logger.Warn($"DeviceId is mismatechecd [{headerBytes[0]} != {DeviceId}].");
                return false;
            }
            if (headerBytes[1] != (byte)FunctionCode)
            {
                Logger.Warn($"Function Code is mismatechecd [{headerBytes[1]} != {(byte)FunctionCode}].");
                return false;
            }
            dataByteSize = 4; //Starting address + Quantity of Registoers
            return true;
        }

    }
}