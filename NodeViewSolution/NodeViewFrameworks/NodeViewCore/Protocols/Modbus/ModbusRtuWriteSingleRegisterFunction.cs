using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public class ModbusRtuWriteSingleRegisterFunction : ModbusRtuFunction, IModbusWriteSingleRegisterFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public ushort Address { get; }
        public override int ResponseHeaderByteSize { get; }
        public ushort WriteData { get; }

        public ModbusRtuWriteSingleRegisterFunction(byte deviceId, ushort address, ushort writeData) : base(ModbusFunctionCodes.WriteSingleRegister, deviceId)
        {
            Address = address;
            WriteData = writeData;
            ResponseHeaderByteSize = 2;
        }


        public override byte[] BuildRequestPacket()
        {
            List<byte> packetData = new List<byte>();
            packetData.AddRange(ModbusUtility.GetBytes(Address));
            packetData.AddRange(ModbusUtility.GetBytes(WriteData));

            return BuildRequestPacket(packetData.ToArray());
        }

        public override bool TryParseResponseHeader(byte[] headerBytes, out int dataByteSize)
        {
            dataByteSize = 0;
            if (headerBytes.Length < ResponseHeaderByteSize)
            {
                Logger.Warn($"readBytes length({headerBytes.Length}) < header size({ResponseHeaderByteSize}).");
                return false;
            }
            if (headerBytes[0] != DeviceId)
            {
                Logger.Warn($"DeviceId is mismatechecd [{headerBytes[0]} != {DeviceId}].");
                return false;
            }
            if (headerBytes[1] != (byte)FunctionCode)
            {
                Logger.Warn($"Function Code is mismatechecd [{headerBytes[1]} != {(byte)FunctionCode}].");
                return false;
            }
            dataByteSize = 4; //address + data
            return true;
        }

        public bool TryParseResponse(byte[] packetBytes)
        {
            bool res = true;
            int dataByteSize;
            int offset = ResponseHeaderByteSize;

            if (!TryParseResponseHeader(packetBytes, out dataByteSize))
            {
                Logger.Error("Packet Header is mismatched!.");
                res = false;
            }
            else
            {
                if (dataByteSize != 4)
                {
                    Logger.Error($"Respone Length MUST BE 4, but ({dataByteSize}).");
                    res = false;
                }
            }

            if (res)
            {
                ushort address = ModbusUtility.GetUshort(packetBytes, offset);
                if (address != Address)
                {
                    Logger.Error($"Response start address is Mismatched [{address}/{Address}]");
                    res = false;
                }
                offset += 2;
                /*
                ushort length = ModbusUtility.GetUshort(packetBytes, offset);
                if (length != Length)
                {
                    Logger.Error($"Response length is mismatched [{length}/{Length}]");
                    res = false;
                }
                */
            }

            return res;
        }
    }
}