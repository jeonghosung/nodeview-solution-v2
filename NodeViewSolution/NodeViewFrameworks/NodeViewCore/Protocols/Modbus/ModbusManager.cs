﻿using NodeView.Net;

namespace NodeView.Protocols.Modbus
{
    public static class ModbusManager
    {
        public static ModbusMaster CreateRtuMaster(IStreamClient clientStream)
        {
            return new ModbusMaster(clientStream, new ModbusRtuMasterStrategy());
        }
        public static ModbusMaster CreateTcpMaster(IStreamClient clientStream)
        {
            return new ModbusMaster(clientStream, new ModbusTcpMasterStrategy());
        }
    }
}