namespace NodeView.Protocols.Modbus
{
    public abstract class ModbusFunction: IModbusFunction
    {
        public ModbusFunctionCodes FunctionCode { get; }
        public byte DeviceId { get; }
        public abstract int ResponseHeaderByteSize { get; }
        public abstract byte[] BuildRequestPacket();
        public abstract bool TryParseResponseHeader(byte[] headerBytes, out int dataByteSize);
        protected ModbusFunction(ModbusFunctionCodes function, byte deviceId)
        {
            FunctionCode = function;
            DeviceId = deviceId;
        }
    }
}