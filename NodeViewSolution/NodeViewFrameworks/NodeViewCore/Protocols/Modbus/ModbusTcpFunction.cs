using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public abstract class ModbusTcpFunction: ModbusFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static volatile ushort SeqTransactionId = 0;
        private const ushort ProtocolId = 0;

        private object _lockObject = new object();

        public ushort TransactionId { get; }
        public override int ResponseHeaderByteSize { get; }

        protected ModbusTcpFunction(ModbusFunctionCodes function, byte deviceId) : base(function, deviceId)
        {
            ResponseHeaderByteSize = 6;
            TransactionId = CreateTransactionId();
        }

        private ushort CreateTransactionId()
        {
            ushort transactionId = 1;
            lock (_lockObject)
            {
                if (SeqTransactionId == ushort.MaxValue)
                {
                    transactionId = 0;
                }
                else
                {
                    transactionId = ++SeqTransactionId;
                }

            }
            return transactionId;
        }

        protected byte[] BuildRequestPacket(byte[] dataBytes)
        {
            List<byte> packetBytes = new List<byte>();
            packetBytes.AddRange(ModbusUtility.GetBytes(TransactionId));
            packetBytes.AddRange(ModbusUtility.GetBytes(ProtocolId));
            ushort payloadLength = (ushort)(dataBytes.Length + 2);
            packetBytes.AddRange(ModbusUtility.GetBytes(payloadLength));
            packetBytes.Add(DeviceId);
            packetBytes.Add((byte)FunctionCode);
            packetBytes.AddRange(dataBytes);

            return packetBytes.ToArray();
        }


        public override bool TryParseResponseHeader(byte[] headerBytes, out int dataByteSize)
        {
            dataByteSize = 0;
            if (headerBytes.Length < ResponseHeaderByteSize)
            {
                Logger.Warn($"readBytes length({headerBytes.Length}) < header size({ResponseHeaderByteSize}).");
                return false;
            }
            int offset = 0;
            ushort transactionId = ModbusUtility.GetUshort(headerBytes, offset);
            offset += 2;
            ushort protocolId = ModbusUtility.GetUshort(headerBytes, offset);
            offset += 2;
            ushort payloadLength = ModbusUtility.GetUshort(headerBytes, offset);

            if (TransactionId != transactionId)
            {
                Logger.Warn($"TransactionId is mismatechecd [{TransactionId} != {transactionId}].");
                return false;
            }
            if (ProtocolId != protocolId)
            {
                Logger.Warn($"ProtocolId is mismatechecd [{ProtocolId} != {protocolId}].");
                return false;
            }

            dataByteSize = payloadLength;
            
            return true;
        }
    }
}