using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public class ModbusTcpWriteMultipleRegistersFunction : ModbusTcpFunction, IModbusWriteMultipleRegistersFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ushort StartAddress { get; }
        public ushort Length { get; }
        public override int ResponseHeaderByteSize { get; }
        public ushort[] WriteDatas { get; }

        public ModbusTcpWriteMultipleRegistersFunction(byte deviceId, ushort startAddress, ushort[] writeDatas) : base(ModbusFunctionCodes.WriteMultipleRegisters, deviceId)
        {
            StartAddress = startAddress;
            Length = (ushort)writeDatas.Length;
            WriteDatas = writeDatas;
            ResponseHeaderByteSize = 2;
        }


        public override byte[] BuildRequestPacket()
        {
            List<byte> packetData = new List<byte>();
            packetData.AddRange(ModbusUtility.GetBytes(StartAddress));
            packetData.AddRange(ModbusUtility.GetBytes(Length));
            packetData.Add((byte)(Length * 2));
            foreach (ushort data in WriteDatas)
            {
                packetData.AddRange(ModbusUtility.GetBytes(data));
            }
            return BuildRequestPacket(packetData.ToArray());
        }

        public bool TryParseResponse(byte[] packetBytes)
        {
            bool res = true;
            int payloadLength;
            int offset = ResponseHeaderByteSize;

            if (!TryParseResponseHeader(packetBytes, out payloadLength))
            {
                Logger.Error("Packet Header is mismatched!.");
                return false;
            }
            if (packetBytes[offset] != DeviceId)
            {
                Logger.Warn($"DeviceId is mismatechecd [{packetBytes[offset]} != {DeviceId}].");
                return false;
            }
            offset++;

            ushort startAddress = ModbusUtility.GetUshort(packetBytes, offset);
            if (startAddress != StartAddress)
            {
                Logger.Error($"Response start address is Mismatched [{startAddress}/{StartAddress}]");
                res = false;
            }
            offset += 2;
            ushort length = ModbusUtility.GetUshort(packetBytes, offset);
            if (length != Length)
            {
                Logger.Error($"Response length is mismatched [{length}/{Length}]");
                res = false;
            }

            return res;
        }
    }
}