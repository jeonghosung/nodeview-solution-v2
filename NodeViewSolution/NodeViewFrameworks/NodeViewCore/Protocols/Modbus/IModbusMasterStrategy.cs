﻿namespace NodeView.Protocols.Modbus
{
    public interface IModbusMasterStrategy
    {
        int CrcByteSize { get; }
        bool CheckCrc(byte[] readDataBytes, int offset);
        IModbusReadHoldingRegistersFunction CreateReadHoldingRegistersFunction(byte deviceId, ushort startAddress, ushort length);
        IModbusReadInputRegistersFunction CreateReadInputRegistersFunction(byte deviceId, ushort startAddress, ushort length);
        IModbusWriteSingleRegisterFunction CreateWriteSingleRegisterFunction(byte deviceId, ushort address, ushort writeData);
        IModbusWriteMultipleRegistersFunction CreateWriteMultipleRegistersFunction(byte deviceId, ushort startAddress, ushort[] writeDatas);
    }
}