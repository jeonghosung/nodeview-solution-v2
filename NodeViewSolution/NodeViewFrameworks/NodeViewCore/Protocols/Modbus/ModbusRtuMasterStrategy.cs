namespace NodeView.Protocols.Modbus
{
    public class ModbusRtuMasterStrategy : IModbusMasterStrategy
    {
        public int CrcByteSize { get; }

        public ModbusRtuMasterStrategy()
        {
            CrcByteSize = 2;
        }
        public bool CheckCrc(byte[] readDataBytes, int offset)
        {
            //ToDo: 정상적으로 체크하는 루틴을 구현해야 한다.
            return true;
        }
        public IModbusReadHoldingRegistersFunction CreateReadHoldingRegistersFunction(byte deviceId, ushort startAddress, ushort length)
        {
            return new ModbusRtuReadHoldingRegistersFunction(deviceId, startAddress, length);
        }

        public IModbusReadInputRegistersFunction CreateReadInputRegistersFunction(byte deviceId, ushort startAddress, ushort length)
        {
            return new ModbusRtuReadInputRegistersFunction(deviceId, startAddress, length);
        }

        public IModbusWriteSingleRegisterFunction CreateWriteSingleRegisterFunction(byte deviceId, ushort address, ushort writeData)
        {
            return new ModbusRtuWriteSingleRegisterFunction(deviceId, address, writeData);
        }

        public IModbusWriteMultipleRegistersFunction CreateWriteMultipleRegistersFunction(byte deviceId, ushort startAddress, ushort[] writeDatas)
        {
            return new ModbusRtuWriteMultipleRegistersFunction(deviceId, startAddress, writeDatas);
        }

    }
}