using System.Collections.Generic;
using NLog;

namespace NodeView.Protocols.Modbus
{
    public class ModbusTcpWriteSingleRegisterFunction : ModbusTcpFunction, IModbusWriteSingleRegisterFunction
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ushort Address { get; }
        public override int ResponseHeaderByteSize { get; }
        public ushort WriteData { get; }

        public ModbusTcpWriteSingleRegisterFunction(byte deviceId, ushort address, ushort writeData) : base(ModbusFunctionCodes.WriteSingleRegister, deviceId)
        {
            Address = Address;
            WriteData = writeData;
            ResponseHeaderByteSize = 2;
        }


        public override byte[] BuildRequestPacket()
        {
            List<byte> packetData = new List<byte>();
            packetData.AddRange(ModbusUtility.GetBytes(Address));
            packetData.AddRange(ModbusUtility.GetBytes(WriteData));
            return BuildRequestPacket(packetData.ToArray());
        }

        public bool TryParseResponse(byte[] packetBytes)
        {
            bool res = true;
            int payloadLength;
            int offset = ResponseHeaderByteSize;

            if (!TryParseResponseHeader(packetBytes, out payloadLength))
            {
                Logger.Error("Packet Header is mismatched!.");
                return false;
            }
            if (packetBytes[offset] != DeviceId)
            {
                Logger.Warn($"DeviceId is mismatechecd [{packetBytes[offset]} != {DeviceId}].");
                return false;
            }
            offset++;

            ushort address = ModbusUtility.GetUshort(packetBytes, offset);
            if (address != Address)
            {
                Logger.Error($"Response start address is Mismatched [{address}/{Address}]");
                res = false;
            }
            offset += 2;
            /*
            ushort length = ModbusUtility.GetUshort(packetBytes, offset);
            if (length != Length)
            {
                Logger.Error($"Response length is mismatched [{length}/{Length}]");
                res = false;
            }
            */
            return res;
        }
    }
}