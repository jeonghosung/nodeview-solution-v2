using System.Collections.Generic;

namespace NodeView.Protocols.Modbus
{
    public abstract class ModbusRtuFunction: ModbusFunction
    {
        protected ModbusRtuFunction(ModbusFunctionCodes function, byte deviceId) : base(function, deviceId)
        {
        }

        protected byte[] BuildRequestPacket(byte[] dataBytes)
        {
            List<byte> packetBytes = new List<byte>();
            packetBytes.Add(DeviceId);
            packetBytes.Add((byte)FunctionCode);
            packetBytes.AddRange(dataBytes);

            byte[] packetDataBody = packetBytes.ToArray();
            packetBytes.AddRange(ModbusUtility.CalculateCrc(packetDataBody));
            return packetBytes.ToArray();
        }
    }
}