﻿using System;
using System.Collections.Generic;
using System.Threading;
using NLog;
using NodeView.Net;

namespace NodeView.Protocols.Modbus
{
    public class ModbusMaster
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();


        private readonly IStreamClient _clientStream;
        private readonly IModbusMasterStrategy _modbusMasterStrategy;

        public ModbusMaster(IStreamClient clientStream, IModbusMasterStrategy modbusMasterStrategy)
        {
            _clientStream = clientStream;
            _modbusMasterStrategy = modbusMasterStrategy;
        }

        public bool IsOpend => _clientStream.IsOpend;


        public void ReOpen()
        {
            _clientStream?.Close();
            _clientStream?.Open();
        }

        public void ClearReceivedBuffer()
        {
            _clientStream?.ClearReceivedData();
        }

        public ushort[] ReadHoldingRegisters(byte deviceId, ushort startAddress, ushort length)
        {
            IModbusReadHoldingRegistersFunction function = _modbusMasterStrategy.CreateReadHoldingRegistersFunction(deviceId, startAddress, length);
            byte[] reponsePacket = CallFunction(function);
            if (reponsePacket != null && reponsePacket.Length > 0)
            {
                ushort[] datas;
                if (!function.TryParseResponse(reponsePacket, out datas))
                {
                    Logger.Error("Cannot parse response!.");
                }
                else
                {
                    return datas;
                }
            }
            return new ushort[0];
        }

        public ushort[] ReadInputRegisters(byte deviceId, ushort startAddress, ushort length)
        {
            IModbusReadInputRegistersFunction function = _modbusMasterStrategy.CreateReadInputRegistersFunction(deviceId, startAddress, length);
            byte[] reponsePacket = CallFunction(function);
            if (reponsePacket != null && reponsePacket.Length > 0)
            {
                ushort[] datas;
                if (!function.TryParseResponse(reponsePacket, out datas))
                {
                    Logger.Error("Cannot parse response!.");
                }
                else
                {
                    return datas;
                }
            }
            return new ushort[0];
        }

        public bool WriteSingleRegister(byte deviceId, ushort address, ushort writeData)
        {
            bool res = false;
            IModbusWriteSingleRegisterFunction function = _modbusMasterStrategy.CreateWriteSingleRegisterFunction(deviceId, address, writeData);
            byte[] reponsePacket = CallFunction(function);
            if (reponsePacket != null && reponsePacket.Length > 0)
            {
                if (function.TryParseResponse(reponsePacket))
                {
                    res = true;
                }
                else
                {
                    Logger.Error("Cannot parse response!.");
                }
            }

            return res;
        }

        public bool WriteMultipleRegisters(byte deviceId, ushort startAddress, ushort[] writeDatas)
        {
            bool res = false;
            IModbusWriteMultipleRegistersFunction function = _modbusMasterStrategy.CreateWriteMultipleRegistersFunction(deviceId, startAddress, writeDatas);
            byte[] reponsePacket = CallFunction(function);
            if (reponsePacket != null && reponsePacket.Length > 0)
            {
                if (function.TryParseResponse(reponsePacket))
                {
                    res = true;
                }
                else
                {
                    Logger.Error("Cannot parse response!.");
                }
            }

            return res;
        }

        private byte[] CallFunction(IModbusFunction function)
        {
            byte[] readPacket = new byte[0];
            if (!SendRequestPacket(function))
            {
                Logger.Error("Cannot send request packet");
            }
            else
            {
                Thread.Sleep(200);
                readPacket = ReadResponsePacket(function);
            }
            return readPacket;
        }

        private bool SendRequestPacket(IModbusFunction function)
        {
            bool res = false;
            try
            {
                byte[] packet = function.BuildRequestPacket();
                if (WriteBytes(packet))
                {
                    res = true;
                }
                else
                {
                    Logger.Error("Cannot write bytes.");
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot send packet.");
            }

            return res;
        }

        private byte[] ReadResponsePacket(IModbusFunction function)
        {
            bool res = true;
            int dataByteSize = 0;
            List<byte> readPacket = new List<byte>();
            try
            {
                //Read Packet Header
                {
                    byte[] readHeaderBytes = ReadBytes(function.ResponseHeaderByteSize);
                    if (!function.TryParseResponseHeader(readHeaderBytes, out dataByteSize))
                    {
                        Logger.Error("Packet Header is mismatched!.");
                        res = false;
                    }
                    else
                    {
                        readPacket.AddRange(readHeaderBytes);
                    }
                }
                //Read Packet Data
                if (res)
                {
                    int requestDataSize = dataByteSize + _modbusMasterStrategy.CrcByteSize;
                    byte[] readDataBytes = ReadBytes(requestDataSize);
                    if (readDataBytes.Length != requestDataSize)
                    {
                        Logger.Error("Packet data is mismatched!.");
                        res = false;
                    }
                    else
                    {
                        if (_modbusMasterStrategy.CheckCrc(readDataBytes,
                            readDataBytes.Length - _modbusMasterStrategy.CrcByteSize))
                        {
                            readPacket.AddRange(readDataBytes);
                        }
                        else
                        {
                            Logger.Error("Read packet CRC Error!.");
                            res = false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot read response packet!");
                res = false;
            }


            if (!res)
            {
                return new byte[0];
            }
            return readPacket.ToArray();
        }

        private bool WriteBytes(byte[] packet)
        {
            bool res = false;
            try
            {
                _clientStream.Write(packet, 0, packet.Length);
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "WriteBytes");
            }
            return res;
        }

        private byte[] ReadBytes(int requestReadSize)
        {
            int readSize = 0;
            int remaindSize = requestReadSize;
            byte[] readBytes = new byte[requestReadSize];

            try
            {
                for (int tryCount = 0; tryCount < 3; tryCount++)
                {
                    int ret = _clientStream.Read(readBytes, readSize, (requestReadSize - readSize));
                    readSize += ret;
                    if (readSize == requestReadSize)
                    {
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                readSize = -1;
                Logger.Error(e, "Cannot read bytes.");
            }

            //string readPacketString = BitConverter.ToString(readBytes);
            //Logger.Debug(readPacketString);

            if (readSize != requestReadSize)
            {
                Logger.Error($"Cannot read all bytes [{readSize}/{requestReadSize}].");
                readBytes = new byte[0];
            }
            return readBytes;
        }



    }
}