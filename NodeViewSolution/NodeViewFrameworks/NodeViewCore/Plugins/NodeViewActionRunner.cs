﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;

namespace NodeView.Plugins
{
    public class NodeViewActionRunner : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NodeViewActionRunner(string id)
        {
            Id = id;
            DeviceType = "actionRunner";
            Name = "ActionRunner";
            Description = "NodeView Action Runner";
            ConnectionString = "";
            IsTestMode = false;
            IsVerbose = false;
        }

        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("run", "액션 실행", new[]
            {
                new FieldDescription("id", "actionId", "액션 아이디"),
            }));

            return commandDescriptions.ToArray();
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "run", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (!string.IsNullOrWhiteSpace(id))
                    {

                        isHandled = ControlRunAction(id);
                    }
                }
                
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }

        private bool ControlRunAction(string id)
        {
            if (StationService == null)
            {
                Logger.Error("actionRunner is null.");
                return false;
            }

            return StationService.RunAction(id);
        }
    }
}
