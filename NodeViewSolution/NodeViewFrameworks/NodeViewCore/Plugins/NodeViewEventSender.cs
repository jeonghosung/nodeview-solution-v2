﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;

namespace NodeView.Plugins
{
    public class NodeViewEventSender : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string ConnectionStartString = "http";
        private RestApiClient _apiClient = null;

        public NodeViewEventSender(string id, string baseUri)
        {
            Id = id;
            DeviceType = "eventSender";
            Name = "EventSender";
            Description = "NodeView Event Sender";
            ConnectionString = baseUri;
            IsTestMode = false;
            IsVerbose = false;
        }

        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("eventOn", "이벤트 발생", new[]
            {
                new FieldDescription("id", "eventId", "이벤트 아이디"),
                //new FieldDescription("targetIds", "", "AppId list','로 구분, 없으면 Center"),
                new FieldDescription("message", "", "이벤트 메세지"),
                new FieldDescription("param", "", "이벤트 전달값")
            }));
            commandDescriptions.Add(new CommandDescription("eventOff", "이벤트 종료", new[]
            {
                new FieldDescription("id", "eventId", "이벤트 아이디")
            }));

            return commandDescriptions.ToArray();
        }
        public override bool Init(Configuration config, IStationService stationService)
        {
            if (!base.Init(config, stationService))
            {
                return false;
            }

            bool res = false;
            try
            {
                if (!ConnectionString.StartsWith(ConnectionStartString))
                {
                    Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                    return false;
                }

                _apiClient = new RestApiClient(ConnectionString);
                if (IsTestMode)
                {
                    res = true;
                }
                else
                {
                    var response = _apiClient.Get("");
                    if (!response.IsSuccess)
                    {
                        Logger.Error($"Error connect [{response.ResponseCode}].");
                        //일단 Init은 된 걸로!
                    }

                    res = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _apiClient = null;
            }
        }
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "eventOn", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (!string.IsNullOrWhiteSpace(id))
                    {

                        isHandled = ControlEventOn(id, parameters);
                    }
                }
                else if (string.Compare(command, "eventOff", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string id = parameters.GetValue("id", "");
                    if (!string.IsNullOrWhiteSpace(id))
                    {
                        isHandled = ControlEventOff(id);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
        //Todo:이건 구현 다시해야 한다.

        private bool ControlEventOn(string id, IEnumerable<IKeyValuePair> parameters = null)
        {
            if (_apiClient == null)
            {
                Logger.Error("apiClient is null.");
                return false;
            }

            JObject request = new JObject()
            {
                ["id"] = id,
                ["isOn"] = true,
            };
            if (parameters != null)
            {
                foreach (var keyValuePair in parameters)
                {
                    switch (keyValuePair.Key)
                    {
                        case "id":
                        case "isOn":
                            break;
                        default:
                            request[keyValuePair.Key] = $"{keyValuePair.Value}";
                            break;

                    }
                }
            }
            var response = _apiClient.Post("/events", request);
            return response.IsSuccess;
        }

        private bool ControlEventOff(string id)
        {
            if (_apiClient == null)
            {
                Logger.Error("apiClient is null.");
                return false;
            }

            JObject request = new JObject()
            {
                ["id"] = id,
                ["isOn"] = false,
            };
            var response = _apiClient.Post("/events", request);
            return response.IsSuccess;
        }
    }
}
