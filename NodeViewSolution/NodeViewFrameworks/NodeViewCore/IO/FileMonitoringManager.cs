﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.IO
{
    public class FileMonitoringManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private static Lazy<FileMonitoringManager> lazy = new Lazy<FileMonitoringManager>(() => new FileMonitoringManager());
        public static FileMonitoringManager Instance = lazy.Value;

        private readonly List<FolderWatcher> _watcherList =  new List<FolderWatcher>();
        public DateTime UpdatedTime { get; private set; } = CachedDateTime.Now;

        public string[] WatchingFolders
        {
            get
            {
                string[] watchingFolders = null;
                lock (_watcherList)
                {
                    watchingFolders = _watcherList.Select(x=>x.FolderPath).ToArray();
                }
                return watchingFolders??new string[0];
            }
        }

        public DateTime GetFolderUpdatedTime(string folderName)
        {
            string folderPath = folderName.ToLower();
            lock (_watcherList)
            {
                foreach (FolderWatcher watcher in _watcherList)
                {
                    if (watcher.IncludeSubFolder)
                    {
                        if (folderPath.StartsWith(watcher.FolderPath))
                        {
                            return watcher.UpdatedTime;
                        }
                    }
                    else
                    {
                        if (folderPath == watcher.FolderPath)
                        {
                            return watcher.UpdatedTime;
                        }
                    }
                }
            }
            return CachedDateTime.Now;
        }

        public bool StartWatchingFolder(string folderName, bool includeSubFolder=true, bool forceCreate = true)
        {
            try
            {
                string folderPath = Path.GetFullPath(folderName);
                if (!Directory.Exists(folderPath))
                {
                    if (forceCreate)
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    else
                    {
                        return false;
                    }
                }

                folderPath = folderPath.ToLower();

                lock (_watcherList)
                {
                    foreach (FolderWatcher watcher in _watcherList)
                    {
                        if (watcher.IncludeSubFolder)
                        {
                            if (folderPath.StartsWith(watcher.FolderPath))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            if (folderPath == watcher.FolderPath)
                            {
                                return true;
                            }
                        }
                    }
                }
                
                FolderWatcher folderWatcher = new FolderWatcher(folderPath, includeSubFolder);
                folderWatcher.Changed += WatcherOnChanged;
                folderWatcher.Deleted += WatcherOnDeleted;
                lock (_watcherList)
                {
                    _watcherList.Add(folderWatcher);
                    _watcherList.Sort((x,y)=>String.Compare(x.FolderPath, y.FolderPath, StringComparison.Ordinal));
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StartWatchingFolder:");
                return false;
            }
            UpdatedTime = CachedDateTime.Now;
            return true;
        }

        public void StopWatchingFolder(string folderName)
        {
            try
            {
                string folderPath = Path.GetFullPath(folderName).ToLower();
                lock (_watcherList)
                {
                    FolderWatcher toRemove = null;
                    foreach (FolderWatcher watcher in _watcherList)
                    {
                        if (folderPath == watcher.FolderPath)
                        {
                            toRemove = watcher;
                            break;
                        }
                    }

                    if (toRemove != null)
                    {
                        _watcherList.Remove(toRemove);
                        toRemove.Dispose();
                        UpdatedTime = CachedDateTime.Now;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "StopWatchingFolder:");
            }
        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs e)
        {
            Logger.Info($"WatcherOnChanged({e.Name}, {e.ChangeType}, {e.FullPath}");
            UpdatedTime = CachedDateTime.Now;
        }

        private void WatcherOnDeleted(object sender, FileSystemEventArgs e)
        {
            Logger.Info($"WatcherOnDeleted({e.Name}, {e.ChangeType}, {e.FullPath}");
            UpdatedTime = CachedDateTime.Now;
        }
    }
}
