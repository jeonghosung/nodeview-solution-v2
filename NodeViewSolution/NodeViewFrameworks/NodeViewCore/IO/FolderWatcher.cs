﻿using System;
using System.IO;
using NLog;
using NodeView.Utils;

namespace NodeView.IO
{
    public class FolderWatcher : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private FileSystemWatcher _watcher = new FileSystemWatcher();
        private FileSystemEventHandler _onChangedHandler;
        private FileSystemEventHandler _onDeletedHandler;
        
        public string FolderPath { get; }
        public bool IncludeSubFolder { get; }
        public DateTime UpdatedTime { get; private set; } = CachedDateTime.Now;
        public FolderWatcher(string folderPath, bool includeSubFolder)
        {
            FolderPath = folderPath;
            IncludeSubFolder = includeSubFolder;

            _watcher.Path = folderPath;
            _watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.DirectoryName | NotifyFilters.CreationTime | NotifyFilters.LastWrite;
            _watcher.IncludeSubdirectories = includeSubFolder;

            _watcher.Changed += WatcherOnChanged;
            _watcher.Deleted += WatcherOnDeleted;
            _watcher.Renamed += WatcherOnRenamed;

            _watcher.EnableRaisingEvents = true;
        }

        public event FileSystemEventHandler Changed
        {
            add => _onChangedHandler += value;
            remove => _onChangedHandler -= value;
        }

        public event FileSystemEventHandler Deleted
        {
            add => _onDeletedHandler += value;
            remove => _onDeletedHandler -= value;
        }

        private void WatcherOnChanged(object sender, FileSystemEventArgs e)
        {
            UpdatedTime = CachedDateTime.Now;
            _onChangedHandler?.Invoke(this, e);
        }
        private void WatcherOnDeleted(object sender, FileSystemEventArgs e)
        {
            UpdatedTime = CachedDateTime.Now;
            _onDeletedHandler?.Invoke(this, e);
        }
        private void WatcherOnRenamed(object sender, RenamedEventArgs e)
        {
            UpdatedTime = CachedDateTime.Now;
            if (Directory.Exists(e.FullPath))
            {
                Logger.Warn($"Renamed Folder [{e.FullPath}].");
                _onDeletedHandler?.Invoke(this, new FileSystemEventArgs(WatcherChangeTypes.Deleted, e.OldFullPath, ""));
                _onChangedHandler?.Invoke(this, new FileSystemEventArgs(WatcherChangeTypes.Changed, e.FullPath, ""));
            }
            else
            {
                Logger.Info($"WatcherOnRenamed({e.Name}, {e.ChangeType}, {e.FullPath}");
                WatcherOnDeleted(sender, new FileSystemEventArgs(WatcherChangeTypes.Deleted, Path.GetDirectoryName(e.OldFullPath), Path.GetFileName(e.OldFullPath)));
                WatcherOnChanged(sender, new FileSystemEventArgs(WatcherChangeTypes.Deleted, Path.GetDirectoryName(e.FullPath), Path.GetFileName(e.FullPath)));
            }
        }

        public void Dispose()
        {
            _watcher.EnableRaisingEvents = false;
            _watcher.Changed -= WatcherOnChanged;
            _watcher.Deleted -= WatcherOnDeleted;
            _watcher.Renamed -= WatcherOnRenamed;
            _watcher = null;
            _onChangedHandler = null;
            _onDeletedHandler = null;
        }
    }
}