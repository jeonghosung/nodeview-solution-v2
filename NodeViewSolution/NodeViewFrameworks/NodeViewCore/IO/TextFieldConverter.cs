﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using NLog;
using NodeView.Utils;

namespace NodeView.IO
{
    public class TextFieldConverter
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly Dictionary<string, ITextFieldMapping> _textFieldMappingDictionary = new Dictionary<string, ITextFieldMapping>();

        public bool Register(ITextFieldMapping mapping)
        {
            bool res = false;
            Type mappingType = mapping.ClassType;
            try
            {
                if (!mappingType.HasNullParameterConstructor())
                {
                    Logger.Warn($"'{mappingType.Name}' has not parameter less constructor.");
                    return false;
                }

                _textFieldMappingDictionary[mapping.ClassType.Name] = mapping;
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot CreateMap for '{mappingType.Name}'.");
                res = false;
            }
            return res;
        }

        public T CreateInstance<T>(string[] fields)
        {
            Type mappingType = typeof(T);
            try
            {
                if (_textFieldMappingDictionary.TryGetValue(mappingType.Name, out var mapping))
                {
                    var instance = mapping.CreateInstance(fields);
                    return (T)instance;

                }
                else
                {
                    Logger.Warn($"Cannot find mapping for '{mappingType.Name}'.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot CreateMap for '{mappingType.Name}'.");
            }
            return default(T);
        }

        public string[] ToFields<T>(T instance)
        {
            Type mappingType = typeof(T);
            try
            {
                if (_textFieldMappingDictionary.TryGetValue(mappingType.Name, out var mapping))
                {
                    var fields = mapping.ToFields(instance);
                    return fields ?? new string[0];

                }
                else
                {
                    Logger.Warn($"Cannot find mapping for '{mappingType.Name}'.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot CreateMap for '{mappingType.Name}'.");
            }
            return new string[0];
        }

    }

    public interface ITextFieldMapping
    {
        Type ClassType { get; }
        object CreateInstance(string[] sourceStrings);
        string[] ToFields(object instance);
    }

    public class TextFieldMapping<T> : ITextFieldMapping
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public Type ClassType { get; } = typeof(T);
        Dictionary<string, PropertyInfo> ClassPropertyInfos = new Dictionary<string, PropertyInfo>();
        public readonly Dictionary<string, int> Properties = new Dictionary<string, int>();

        private int _maxFieldIndex = -1;
        private Action<string[], T> _afterCreateInstanceFunction = null;
        private Action<T, string[]> _afterToFields = null;
        private Func<string[], T, bool> _checkValid = null;

        public int this[string propertyName]
        {
            get
            {
                propertyName = propertyName.ToLower();
                if (Properties.TryGetValue(propertyName, out int index))
                {
                    return index;
                }

                return -1;
            }
            set => SetProperty(propertyName, value);
        }

        public TextFieldMapping(string properties = "", int startIndex = 0)
        {
            foreach (var propertyInfo in ClassType.GetProperties())
            {
                if (propertyInfo.CanRead)
                {
                    string key = propertyInfo.Name.ToLower();
                    ClassPropertyInfos[key] = propertyInfo;
                }
            }

            if (!string.IsNullOrWhiteSpace(properties))
            {
                SetProperties(properties, startIndex);
            }
        }

        public TextFieldMapping<T> SetProperty(string propertyName, int fieldIndex)
        {
            propertyName = propertyName.ToLower();
            _maxFieldIndex = Math.Max(_maxFieldIndex, fieldIndex);
            Properties[propertyName] = fieldIndex;
            return this;
        }

        public TextFieldMapping<T> SetProperties(string properties, int startIndex = 0)
        {
            string[] splitProperties = StringUtils.Split(properties, ',');

            int index = startIndex;
            foreach (string property in splitProperties)
            {
                string[] splitProperty = StringUtils.Split(property, ':', 2);

                string propertyName = splitProperty[0];
                int propertyIndex = StringUtils.GetValue(splitProperty[1], -1);

                if (propertyIndex < 0)
                {
                    propertyIndex = index;
                }
                index = (propertyIndex + 1);

                if (!string.IsNullOrWhiteSpace(splitProperty[0]))
                {
                    SetProperty(propertyName, propertyIndex);
                }
                else
                {
                    //just skip a property index
                }

            }
            return this;
        }


        public TextFieldMapping<T> AfterCreateInstance(Action<string[], T> afterCreateInstanceFunction)
        {
            _afterCreateInstanceFunction = afterCreateInstanceFunction;
            return this;
        }

        public TextFieldMapping<T> AfterToFields(Action<T, string[]> afterToFields)
        {
            _afterToFields = afterToFields;
            return this;
        }

        public TextFieldMapping<T> CheckValid(Func<string[], T, bool> checkValid)
        {
            _checkValid = checkValid;
            return this;
        }

        public object CreateInstance(string[] fields)
        {
            try
            {
                if (fields == null || fields.Length == 0)
                {
                    return null;
                }

                int fieldCount = fields.Length;
                var instance = Activator.CreateInstance(ClassType);
                foreach (var propertyInfo in ClassPropertyInfos.Values)
                {
                    if (propertyInfo.CanWrite)
                    {
                        string key = propertyInfo.Name.ToLower();
                        if (Properties.TryGetValue(key, out int fieldIndex))
                        {
                            if (fieldIndex < fieldCount && fieldIndex >= 0)
                            {
                                var converter = TypeDescriptor.GetConverter(propertyInfo.PropertyType);
                                var value = converter.ConvertFromString(fields[fieldIndex]);
                                propertyInfo.SetValue(instance, value);
                            }
                        }
                    }
                }

                _afterCreateInstanceFunction?.Invoke(fields, (T)instance);

                if (_checkValid?.Invoke(fields, (T)instance) ?? true)
                {
                    return instance;
                }

                return null;

            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateInstance:");
            }

            return null;
        }

        public string[] ToFields(object instance)
        {
            try
            {
                if (_maxFieldIndex < 0)
                {
                    return new string[0];
                }
                string[] fields = new string[_maxFieldIndex + 1];

                
                foreach (var mappingItem in Properties)
                {
                    if(ClassPropertyInfos.TryGetValue(mappingItem.Key, out var propertyInfo))
                    {
                        fields[mappingItem.Value] = StringUtils.GetStringValue(propertyInfo.GetValue(instance, null));
                    }
                }
                for (int i = 0; i < fields.Length; i++)
                {
                    if (fields[i] == null)
                    {
                        fields[i] = "";
                    }
                }

                _afterToFields?.Invoke((T)instance, fields);

                return fields;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"ToFields for {typeof(T)}:");
            }
            return new string[0];
        }
    }
}
