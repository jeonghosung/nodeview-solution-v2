﻿using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataModels;
using NodeView.Frameworks.ContentControls;

namespace NodeView.DataWalls
{
    public interface IDataWallContent : IContentSource, INode
    {
        string Uri { get; set; }
        string UserId { get; set; }
        string UserPassword { get; set; }
    } 
}
