﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace NodeViewCore.DataWalls
{
    public interface IDataWallMemo
    {
        string Id { get; }
        string Memo { get; set; }
        bool LoadFrom(XmlNode xmlNode);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
    }
}
