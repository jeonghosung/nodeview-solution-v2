﻿using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Drawing;

namespace NodeView.DataWalls
{
    public interface IDataWallWindow
    {
        string Id { get; }
        string Name { get; set; }
        bool IsPopup { get; set; }
        IntRect WindowRect { get; set; }
        string ContentId { get; set; }
        IDataWallContent Content { get; }
        void SetContent(IDataWallContent content);
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
