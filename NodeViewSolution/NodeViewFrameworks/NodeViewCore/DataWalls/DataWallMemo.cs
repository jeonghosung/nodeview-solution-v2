﻿using NLog;
using NodeView.DataWalls;
using NodeView.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Xml.Linq;

namespace NodeViewCore.DataWalls
{
    public class DataWallMemo : IDataWallMemo
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string Id { get; }
        public string Memo { get; set; }

        public DataWallMemo(string id, string memo = null)
        {
            Id = string.IsNullOrWhiteSpace(id) ? "" : id;
            Memo = memo;
        }
        public static DataWallMemo CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetStringAttrValue(xmlNode, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var memo = new DataWallMemo(id);
                if (memo.LoadFrom(xmlNode))
                {
                    return memo;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Memo = xmlNode.InnerText;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Memo";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);

            if (!string.IsNullOrWhiteSpace(Memo))
            {
                xmlWriter.WriteCData(Memo);
            }

            xmlWriter.WriteEndElement();
        }
    }
}
