﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeView.DataWalls
{
    public class DataWallPreset : IDataWallPreset
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private List<IDataWallWindow> _windowList;

        public string Id { get; }
        public string Name { get; set; }

        public IEnumerable<IDataWallWindow> Windows => _windowList;

        public DataWallPreset(IDataWallPreset preset)
        {
            Id = preset.Id;
            Name = preset.Name;
            _windowList = new List<IDataWallWindow>();
            if (preset.Windows != null)
            {
                foreach (var window in preset.Windows)
                {
                    _windowList.Add(new DataWallWindow(window));
                }
            }
        }
        public DataWallPreset(string id, string name="", IEnumerable<IDataWallWindow> windows = null)
        {
            Id = string.IsNullOrWhiteSpace(id) ? "" : id;
            Name = string.IsNullOrWhiteSpace(name) ? Id : name;
            _windowList = new List<IDataWallWindow>();
            if (windows != null)
            {
                foreach (var window in windows)
                {
                    _windowList.Add(new DataWallWindow(window));
                }
            }
        }

        public static DataWallPreset CreateFrom(JObject json)
        {
            if (json == null)
            {
                return null;
            }
            try
            {
                string id = JsonUtils.GetStringValue(json, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var preset = new DataWallPreset(id);
                if (preset.LoadFrom(json))
                {
                    return preset;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                Name = JsonUtils.GetStringValue(json, "name", Name);

                List<IDataWallWindow> windowList = new List<IDataWallWindow>();
                JArray windowJArray = JsonUtils.GetValue<JArray>(json, "windows", null);
                if (windowJArray != null)
                {
                    foreach (JToken windowToken in windowJArray)
                    {
                        JObject windowJsonObject = windowToken.Value<JObject>();
                        DataWallWindow window = DataWallWindow.CreateFrom(windowJsonObject);
                        if (window != null)
                        {
                            windowList.Add(window);
                        }
                    }
                }

                _windowList = windowList;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public JToken ToJson()
        {
            var jObject = new JObject();
            jObject["id"] = Id;
            jObject["name"] = Name;
            JArray windowArray = new JArray();
            foreach (var window in Windows)
            {
                windowArray.Add(window.ToJson());
            }
            jObject["windows"] = windowArray;

            return jObject;
        }
        public static DataWallPreset CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetStringAttrValue(xmlNode, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var preset = new DataWallPreset(id);
                if (preset.LoadFrom(xmlNode))
                {
                    return preset;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);

                List<IDataWallWindow> windowList = new List<IDataWallWindow>();
                XmlNodeList windowNodeList = xmlNode.SelectNodes("Window");
                if (windowNodeList != null)
                {
                    foreach (XmlNode windowNode in windowNodeList)
                    {
                        var window = DataWallWindow.CreateFrom(windowNode);
                        if (window != null)
                        {
                            windowList.Add(window);
                        }
                    }
                }

                _windowList = windowList;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Preset";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);

            if (Windows != null)
            {
                foreach (var window in Windows)
                {
                    window.WriteXml(xmlWriter, "Window");
                }
            }

            xmlWriter.WriteEndElement();
        }
    }
}
