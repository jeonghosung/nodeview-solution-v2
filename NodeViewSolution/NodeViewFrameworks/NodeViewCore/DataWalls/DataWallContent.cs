﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Utils;

namespace NodeView.DataWalls
{
    public class DataWallContent : NodeBase, IDataWallContent
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string ContentType
        {
            get => _attributes.GetStringValue("contentType");
            set => _attributes.Set("contentType", value);
        }

        public string Name
        {
            get => _attributes.GetStringValue("name");
            set => _attributes.Set("name", value);
        }
        public string Uri
        {
            get => _attributes.GetStringValue("uri");
            set => _attributes.Set("uri", value);
        }

        public string UserId {
            get => _attributes.GetStringValue("userId");
            set => _attributes.Set("userId", value);
        }
        public string UserPassword {
            get => _attributes.GetStringValue("userPassword");
            set => _attributes.Set("userPassword", value);
        }

        public DataWallContent(IDataWallContent content = null)
        {
            NodeType = "DataWallContent";

            if (content != null)
            {
                Id = content.Id;
                ContentType = content.ContentType;
                Path = content.Path;
                Name = content.Name;
                Uri = content.Uri;
                UserId = content.UserId;
                UserPassword = content.UserPassword;
                _properties.Sets(content.Properties);
            }
        }

        public override INode CreateNode()
        {
            return new DataWallContent();
        }

        public static DataWallContent CreateFrom(IDataWallContent sourceContent)
        {
            var content = new DataWallContent();
            content.LoadFrom(sourceContent);
            return content;
        }

        public static DataWallContent CreateFrom(INode sourceNode)
        {
            var content = new DataWallContent();
            content.LoadFrom(sourceNode);
            return content;
        }

        public static DataWallContent CreateFrom(JObject json)
        {
            var content = new DataWallContent();
            content.LoadFrom(json);
            return content;
        }

        public static DataWallContent CreateFrom(XmlNode xmlNode)
        {
            var content = new DataWallContent();
            content.LoadFrom(xmlNode);
            return content;
        }

        public override bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                if (base.LoadFrom(xmlNode))
                {
                    Uri = XmlUtils.GetStringInnerText(xmlNode, "Uri", Uri);
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        protected override void WriteXmlAttribute(XmlWriter xmlWriter, string key, string value)
        {
            if (key == "uri")
            {
                //move on "WriteXmlAttributeAfter"
            }
            else
            {
                base.WriteXmlAttribute(xmlWriter, key, value);
            }
        }

        protected override void WriteXmlAttributeAfter(XmlWriter xmlWriter)
        {
            xmlWriter.WriteElementString("Uri", Uri);
        }
    }
}