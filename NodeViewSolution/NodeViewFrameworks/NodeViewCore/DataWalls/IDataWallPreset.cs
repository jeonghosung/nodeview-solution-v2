﻿using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json.Linq;

namespace NodeView.DataWalls
{
    public interface IDataWallPreset
    {
        string Id { get; }
        string Name { get; set; }
        IEnumerable<IDataWallWindow> Windows { get; }
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
