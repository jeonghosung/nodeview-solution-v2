﻿using System;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Drawing;
using NodeView.Net;
using NodeView.Utils;

namespace NodeView.Extension.Protocols
{
    public class NodeViewDataWallClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public string ApiBaseUrl { get; }
        public bool IsTestMode { get; set; } = false;
        private NodeViewApiClient _nodeViewApiClient = null;

        public NodeViewDataWallClient(string dataWallIp, int dataWallPort) : this($"http://{dataWallIp}:{dataWallPort}/api/v1")
        {

        }
        public NodeViewDataWallClient(string apiBaseUrl)
        {
            ApiBaseUrl = apiBaseUrl;
            _nodeViewApiClient = new NodeViewApiClient(apiBaseUrl);
        }

        public bool CreateWindow(IntRect windowRect, string contentId, string windowId = "")
        {
            if (string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
            {
                return false;
            }

            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(windowId))
                {
                    windowId = Uuid.NewUuid;
                }
                JObject requestJson = new JObject()
                {
                    ["id"] = windowId,
                    ["windowRect"] = $"{windowRect}",
                    ["contentId"] = contentId
                };

                if (IsTestMode)
                {
                    Logger.Info("CreateWindow:" + requestJson.ToString());
                }
                else
                {
                    var response = _nodeViewApiClient.Post("/windows", requestJson);
                    if (response.IsSuccess)
                    {
                        res = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateWindow:");
                res = false;
            }

            return res;
        }
        public void CloseWindow(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            try
            {
                JArray windowIds = new JArray();
                windowIds.Add(id);

                JObject requestJson = new JObject
                {
                    ["action"] = "close",
                    ["param"] = new JObject()
                    {
                        ["ids"] = windowIds
                    }
                };
                if (IsTestMode)
                {
                    Logger.Info("CloseWindow:" + requestJson.ToString());
                }
                else
                {
                    var response = _nodeViewApiClient.Put("/windows", requestJson);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CloseWindow:");
            }
        }

        public void CloseAllWindows()
        {
            try
            {
                JObject requestJson = new JObject
                {
                    ["action"] = "closeAll",
                };

                if (IsTestMode)
                {
                    Logger.Info("CloseAllWindows");
                }
                else
                {
                    var response = _nodeViewApiClient.Put("/windows", requestJson);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CloseAllWindows:");
            }
        }
    }
}
