﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.Utils;

namespace NodeView.Extension.Protocols
{
    public class MatrixGm2400Client
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const byte STX = 0x02;
        private const byte EXT = 0x03;
        private const byte DLE = 0x10;
        public bool IsTestMode { get; set; } = false;
        public string Ip { get; }
        public int Port { get; }
        public byte DeviceType { get; set; } = 0xA8;
        public byte DeviceNo { get; set; } = 0x01;

        public MatrixGm2400Client(string ip, int port)
        {
            Ip = ip;
            Port = port;
        }

        public bool ConnectVideo(byte input, byte output)
        {
            bool res = false;

            try
            {
                var payload = new List<byte>();
                payload.Add(DeviceType);
                payload.Add(DeviceNo);
                payload.Add(0x31); //command
                payload.Add(0x02); //video
                payload.Add(input);
                payload.Add(output);

                byte[] packet = BuildPacket(payload);
                if (IsTestMode)
                {
                    Logger.Info($"{StringUtils.ToHexString(packet)}");
                }
                else
                {
                    using (var tcpClient = new TcpClient(Ip, Port))
                    {
                        using (var stream = tcpClient.GetStream())
                        {
                            stream.Write(packet, 0, packet.Length);
                            Thread.Sleep(50);
                        }
                    }
                }

                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ConnectVideo:");
                res = false;
            }

            return res;
        }

        private byte[] BuildPacket(IEnumerable<byte> payload)
        {
            
            var packet = new List<byte>();
            packet.Add(STX);
            byte bcc = STX;

            foreach (byte data in payload)
            {
                if (data == STX || data == EXT || data == DLE)
                {
                    packet.Add(DLE);
                    bcc ^= DLE;
                    byte da = (byte)(data + 0x20);
                    packet.Add(da);
                    bcc ^= da;
                }
                else
                {
                    packet.Add(data);
                    bcc ^= data;
                }
            }
            packet.Add(EXT);
            bcc ^= EXT;
            packet.Add((bcc < 0x20) ? (byte)(bcc + 0x20): bcc);
            return packet.ToArray();
        }
    }
}
