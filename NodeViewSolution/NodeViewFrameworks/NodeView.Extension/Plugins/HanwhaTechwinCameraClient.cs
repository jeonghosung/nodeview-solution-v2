﻿using NLog;
using NodeView.Net;
using NodeViewCore.DeviceControllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Channels;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using NodeViewCore.DataModels;
using Newtonsoft.Json.Linq;

namespace NodeView.Extension.Plugins
{
    public class HanwhaTechwinCameraClient : ICameraClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly bool _isTestMode;
        private readonly bool _isVerbose;
        private RestApiClient _apiClient;

        public HanwhaTechwinCameraClient(string ip, string id, string password, bool isTestMode, bool isVerbose)
        {
            _isTestMode = isTestMode;
            _isVerbose = isVerbose;
            _apiClient = new RestApiClient($"http://{ip}//stw-cgi")
            {
                Credentials = new NetworkCredential(id, password)
            };
        }
        public void ControlPanTilt(int panValue, int tiltValue)
        {
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "relative");
            queryStringDictionary.Add("action", "control");
            queryStringDictionary.Add("Pan", panValue);
            queryStringDictionary.Add("Tilt", tiltValue);
            var response = _apiClient.Get("/ptzcontrol.cgi", queryStringDictionary);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="zoomValue">1~12</param>
        public void ControlZoom(int zoomValue)
        {
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "relative");
            queryStringDictionary.Add("action", "control");
            queryStringDictionary.Add("Zoom", zoomValue);
            var response = _apiClient.Get("/ptzcontrol.cgi", queryStringDictionary);
        }

        public void ApplyPreset(int presetId)
        {
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "preset");
            queryStringDictionary.Add("action", "control");
            queryStringDictionary.Add("Channel", 0);
            queryStringDictionary.Add("Preset", presetId);
            var response = _apiClient.Get("/ptzcontrol.cgi", queryStringDictionary);
        }

        public void SetPreset(int presetId, string presetName)
        {
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "preset");
            queryStringDictionary.Add("action", "add");
            queryStringDictionary.Add("Channel", 0);
            queryStringDictionary.Add("Preset", presetId);
            queryStringDictionary.Add("Name", presetName);
            var response = _apiClient.Get("/ptzconfig.cgi", queryStringDictionary);
        }

        public void GoHome()
        {
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "home");
            queryStringDictionary.Add("action", "control");
            queryStringDictionary.Add("Channel", 0);
            var response = _apiClient.Get("/ptzcontrol.cgi", queryStringDictionary);
        }

        public void SetFocus(FocusType focusType)
        {
            string type = "";
            switch (focusType)
            {
                case FocusType.NEAR:
                    type = "Near";
                    break;
                case FocusType.FAR:
                    type = "Far";
                    break;
            }

            if (focusType == FocusType.AUTO)
            {
                Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
                queryStringDictionary.Add("msubmenu", "focus");
                queryStringDictionary.Add("action", "control");
                queryStringDictionary.Add("Channel", 0);
                queryStringDictionary.Add("Mode", "AutoFocus");
                var response = _apiClient.Get("/image.cgi", queryStringDictionary);
            }
            else
            {
                Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
                queryStringDictionary.Add("msubmenu", "continuous");
                queryStringDictionary.Add("action", "control");
                queryStringDictionary.Add("Channel", 0);
                queryStringDictionary.Add("Focus", type);
                var response = _apiClient.Get("/ptzcontrol.cgi", queryStringDictionary);
            }
           
        }

        public PtzPreset[] GetPresets()
        {
            var ptzPresets = new List<PtzPreset>();

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("msubmenu", "preset");
            queryStringDictionary.Add("action", "view");
            queryStringDictionary.Add("Channel", 0);
            var response = _apiClient.Get("/ptzconfig.cgi", queryStringDictionary);
            if (response.IsSuccess)
            {
                if (response.ResponseType == RestApiResponseType.String)
                {
                    var ptzPresetsResponse = response.Body;
                    var ptzPresetRows =
                        ptzPresetsResponse.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
                    if (ptzPresetRows.Length > 0)
                    {
                        foreach (var ptzPresetRow in ptzPresetRows)
                        {
                            var ptzPresetInfos = ptzPresetRow.Split('.');
                            if (ptzPresetInfos.Length == 5)
                            {

                                if (int.TryParse(ptzPresetInfos[3], out int ptzPresetId))
                                {
                                    var presetNameInfos = ptzPresetInfos[4].Split('=');
                                    if (presetNameInfos.Length == 2)
                                    {
                                        var presetName = presetNameInfos[1];
                                        ptzPresets.Add(new PtzPreset(ptzPresetId, presetName));
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Logger.Error("reponse of contents is not an array.");
                }
            }

            return ptzPresets.ToArray();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
