﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeViewCore.DataModels;
using NodeViewCore.DeviceControllers;

namespace NodeView.Extension.Plugins
{

    public class TruenCameraControlClient :ICameraClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly string _ip;
        private readonly string _channel = "1";
        private readonly bool _isTestMode;
        private readonly bool _isVerbose;
        private readonly int _intervalMs;
        private RestApiClient _apiClient;

        public TruenCameraControlClient(string ip, string id, string password,  bool isTestMode = false, bool isVerbose = false, int intervalMs = 200)
        {
            _apiClient = new RestApiClient($"http://{ip}/httpapi")
            {
                Credentials = new NetworkCredential(id, password)
            };
            _ip = ip;
            _isTestMode = isTestMode;
            _isVerbose = true;
            if (!_isTestMode)
            {
                _isVerbose = isVerbose;
            }
            _intervalMs = intervalMs;
        }
        public void Move(int x, int y, int timeoutMs = 1000)
        {
            if (x == 0 && y == 0) return;

            string xDirection = (x < 0) ? "left" : "right";
            string yDirection = (y < 0) ? "up" : "down";

            int dx = Math.Min(10, Math.Abs(x));
            int dy = Math.Min(10, Math.Abs(y));

            if (dx == 0)
            {
                SendPtzMoveCommand($"{yDirection},{dy}", timeoutMs);
            }
            else if (dy == 0)
            {
                SendPtzMoveCommand($"{xDirection},{dx}", timeoutMs);
            }
            else
            {
                SendPtzMoveCommand($"{xDirection}{yDirection},{dx},{dy}", timeoutMs);
            }
        }

        public void Zoom(int valueZoomIn, int timeoutMs = 1000)
        {
            if (valueZoomIn == 0) return;
            string action = (valueZoomIn < 0) ? "zoomout" : "zoomin";
            int deltaValue = Math.Min(10, Math.Abs(valueZoomIn));


            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]{action}({deltaValue})");
            }
            if (_isTestMode)
            {
                return;
            }

            SendPtzMoveCommand($"{action},{deltaValue}", timeoutMs);
        }

        public void Focus(int valueFocusNear, int timeoutMs = 1000)
        {
            if (valueFocusNear == 0) return;

            string action = (valueFocusNear < 0) ? "focusfar" : "focusnear";
            int deltaValue = Math.Min(10, Math.Abs(valueFocusNear));

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]{action},{deltaValue}");
            }
            if (_isTestMode)
            {
                return;
            }

            SendPtzMoveCommand($"{action},{deltaValue}", timeoutMs);
        }

        public void SetPreset(int presetNo)
        {
            if (presetNo < 0) return;

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]PTZ_PRESETSET({presetNo})");
            }
            if (_isTestMode)
            {
                return;
            }

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("action", "sendptz");
            queryStringDictionary.Add("PTZ_CHANNEL", $"{_channel}");
            queryStringDictionary.Add("PTZ_PRESETSET", $"{presetNo}");
            var response = _apiClient.Get("/SendPTZ", queryStringDictionary);
            Thread.Sleep(_intervalMs);
        }

        public PtzPreset[] GetPresets()
        {
            return new PtzPreset[]{new PtzPreset(1,"Preset1")};
        }
        public void SetPreset(int presetId, string presetName)
        {
            throw new NotImplementedException();
        }

        public void GoHome()
        {
            throw new NotImplementedException();
        }

        public void SetFocus(FocusType focusType)
        {
            throw new NotImplementedException();
        }

        public void ApplyPreset(int presetNo)
        {
            if (presetNo < 0) return;

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]PTZ_PRESETGOTO({presetNo})");
            }
            if (_isTestMode)
            {
                return;
            }
            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("action", "sendptz");
            queryStringDictionary.Add("PTZ_CHANNEL", $"{_channel}");
            queryStringDictionary.Add("PTZ_PRESETGOTO", $"{presetNo}");
            var response = _apiClient.Get("/SendPTZ", queryStringDictionary);
            Thread.Sleep(_intervalMs);
        }

      

        public void Wiper(bool isOn)
        {
            int value = isOn ? 1 : 0;

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]PTZ_WIPER({value})");
            }
            if (_isTestMode)
            {
                return;
            }

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("action", "sendptz");
            queryStringDictionary.Add("PTZ_CHANNEL", $"{_channel}");
            queryStringDictionary.Add("PTZ_WIPER", $"{value}");
            var response = _apiClient.Get("/SendPTZ", queryStringDictionary);
            Thread.Sleep(_intervalMs);
        }

        private void SendPtzMoveCommand(string command, int timeoutMs = 1000)
        {

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]{command}");
            }
            if (_isTestMode)
            {
                return;
            }

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("action", "sendptz");
            queryStringDictionary.Add("PTZ_CHANNEL", $"{_channel}");
            queryStringDictionary.Add("PTZ_MOVE", command);
            queryStringDictionary.Add("PTZ_TIMEOUT", timeoutMs);
            var response = _apiClient.Get("/SendPTZ", queryStringDictionary);
            Thread.Sleep(_intervalMs);
        }
        public void SendPtzMoveContinue(int timeoutMs = 1000)
        {
            string command = "continue";

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]{command}");
            }
            if (_isTestMode)
            {
                return;
            }

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("action", "sendptz");
            queryStringDictionary.Add("PTZ_CHANNEL", $"{_channel}");
            queryStringDictionary.Add("PTZ_MOVE", command);
            queryStringDictionary.Add("PTZ_TIMEOUT", timeoutMs);
            var response = _apiClient.Get("/SendPTZ", queryStringDictionary);
            Thread.Sleep(_intervalMs);
        }
        public void SendPtzMoveStop()
        {
            string command = "stop";

            if (_isVerbose)
            {
                Logger.Info($"[{_ip}]{command}");
            }
            if (_isTestMode)
            {
                return;
            }

            Dictionary<string, object> queryStringDictionary = new Dictionary<string, object>();
            queryStringDictionary.Add("action", "sendptz");
            queryStringDictionary.Add("PTZ_CHANNEL", $"{_channel}");
            queryStringDictionary.Add("PTZ_MOVE", command);
            var response = _apiClient.Get("/SendPTZ", queryStringDictionary);
            Thread.Sleep(_intervalMs);
        }

        public void ControlPanTilt(int panValue, int tiltValue)
        {
            Move(panValue, tiltValue);
        }

        public void ControlZoom(int zoomValue)
        {
            Zoom(zoomValue * 5);
        }

        public void ApplyPreset(string presetId)
        {
            throw new NotImplementedException();
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
