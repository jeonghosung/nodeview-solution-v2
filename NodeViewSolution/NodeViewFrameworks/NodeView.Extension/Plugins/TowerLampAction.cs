﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class TowerLampAction
    {
        internal string Id { get; }
        internal int[] Lamps { get; set; } //on=1, blink=2, off = 3
        internal int Sound { get; set; } = 0; //off=0, on=1, 1~5

        internal TowerLampAction(string id, int lampCount)
        {
            Id = id;
            Lamps = new int[lampCount];
            for (int i = 0; i < lampCount; i++)
            {
                Lamps[i] = 3; // off as default.
            }

            Sound = 0; // off as default.
        }

        internal TowerLampAction(string id, string paramsString, int lampCount) : this(id, lampCount)
        {
            string[] splitParams = StringUtils.Split(paramsString, ',');
            foreach (var param in splitParams)
            {
                string[] nameValue = StringUtils.Split(param, '=', 2);
                string name = nameValue[0].ToLower();
                string value = nameValue[1].ToLower();

                if (name == "s")
                {
                    Sound = GetSoundValue(value);
                }
                else if (StringUtils.IsNumber(name))
                {
                    int number = StringUtils.GetIntValue(name);
                    if (number > 0 && number <= lampCount)
                    {
                        Lamps[number - 1] = GetLampValue(value);
                    }
                }
            }
        }

        private static int GetLampValue(string valueString)
        {
            if (StringUtils.IsNumber(valueString))
            {
                return StringUtils.GetIntValue(valueString);
            }

            int value = 3; //off as default.
            switch (valueString)
            {
                case "on":
                    value = 1;
                    break;
                case "blink":
                    value = 2;
                    break;
                case "off":
                    value = 3;
                    break;
            }

            return value;
        }

        private static int GetSoundValue(string valueString)
        {
            if (StringUtils.IsNumber(valueString))
            {
                return StringUtils.GetIntValue(valueString);
            }

            int value = 0; //off as default.
            switch (valueString)
            {
                case "on":
                    value = 1;
                    break;
                case "off":
                    value = 0;
                    break;
            }

            return value;
        }

        public void WriteXml(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("Actions");
            xmlWriter.WriteAttributeString("id", Id);
            var command = GetCommandString();
            xmlWriter.WriteAttributeString("command", command);
            xmlWriter.WriteEndElement();
        }

        public JObject ToJson()
        {
            JObject jsonObject = new JObject();
            jsonObject["id"] = Id;
            jsonObject["command"] = GetCommandString();
            return jsonObject;
        }

        private string GetCommandString()
        {
            List<string> commandList = new List<string>();
            for (int index = 0; index < Lamps.Length; index++)
            {
                commandList.Add($"{index + 1}={Lamps[index]}");
            }
            commandList.Add($"s={Sound}");

            return string.Join(",", commandList);
        }
    }
}
