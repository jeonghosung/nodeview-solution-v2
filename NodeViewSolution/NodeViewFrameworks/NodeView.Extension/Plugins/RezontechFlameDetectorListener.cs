﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class RezontechFlameDetectorListener : IDataListener
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private TcpClient _tcpClient;
        private Timer _checkConnectionTimer;
        private ByteDataPool _byteDataPool = new ByteDataPool();
        private bool _isFlameDetected = false;
        private bool _isAlarmOn = false;

        private const byte StartPacket = 0x3A;
        private const byte StatusCommandPacket = 0x12;
        private const byte Cr = 0x0D;
        private const byte Lf = 0x0a;
        protected IStationService StationService = null;
        public string Id { get; set; }
        public string Name { get; set; } = "";
        public bool IsTestMode { get; set; } = false;
        public bool IsVerbose { get; set; } = false;
        public string Description { get; set; } = "";
        public string Ip { get; set; } = "";
        public int Port { get; set; } = 101;
        public string AlarmPrefix { get; set; } = "";

        public bool IsStarted { get; protected set; }
        public bool Init(Configuration config, IStationService stationService)
        {
            Logger.Info("rezontec detector init.");
            StationService = stationService;
            if (LoadConfig(config))
            {

            }
            if (!RegisterOnStation())
            {
                return false;
            }
            _checkConnectionTimer = new Timer(DoCheckConnectionTimerCallback);
            return true;

        }

        protected virtual bool LoadConfig(Configuration config)
        {
            if (config == null)
            {
                if (string.IsNullOrWhiteSpace(Id)) return false;
                return true;
            }
            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    Id = StringUtils.GetStringValue(config.Id, Uuid.NewUuid);
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    Name = this.GetType().Name;
                }
                Description = config.GetValue("@description", "");
                Ip = config.GetValue("ip", "");
                Port = config.GetValue<int>("port", 101);
                IsTestMode = config.GetValue("isTestMode", false); ;
                IsVerbose = IsTestMode || config.GetValue("isVerbose", false);
                AlarmPrefix = config.GetValue("alarmIdPrefix", "");
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }
            return res;
        }
        private bool RegisterOnStation()
        {
            return true;
        }
        private void DoCheckConnectionTimerCallback(object? state)
        {
            using (TcpClient tcpClient = new TcpClient(Ip, Port))
            {
                try
                {
                    var stream = tcpClient.GetStream();
                    var getStatusPacket = new byte[7]
                    {
                    StartPacket, 0x01, StatusCommandPacket, 0x01, 0xEC, Cr, Lf
                    };
                    stream.Write(getStatusPacket, 0, getStatusPacket.Length);
                    if (stream.CanRead)
                    {
                        byte[] responseBytes = new byte[256];
                        var readBytes = stream.Read(responseBytes, 0, responseBytes.Length);
                        _byteDataPool.Append(responseBytes, 0, readBytes);
                        CheckPacket(_byteDataPool);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "DoCheckConnectionTimerCallback Error.");
                }
            }
        }
        public bool Start()
        {
            if (!IsStarted)
            {
                _checkConnectionTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
                IsStarted = true;
            }

            return IsStarted;

        }

        public void Stop()
        {
            _checkConnectionTimer.Change(Timeout.Infinite, Timeout.Infinite);
        }

        public void UpdatedNodes(UpdatedNodeArg[] updatedNodeItems)
        {
        }

        public void DeleteAllNodes(string path, bool recursive)
        {
        }

        public void CheckPacket(ByteDataPool byteDataPool)
        {
            List<byte> packetByteList = new List<byte>();
            bool isFindingStartCode = true;
            while (byteDataPool.Count() > 0)
            {
                if (isFindingStartCode)
                {
                    byte readByte = byteDataPool.ReadByte();
                    if (readByte == StartPacket)
                    {
                        isFindingStartCode = false;
                        packetByteList.Add(readByte);
                    }
                }
                else
                {
                    byte readByte = byteDataPool.ReadByte();
                    if (readByte == Lf)
                    {
                        packetByteList.Add(readByte);
                        
                        OnReceivePayload(packetByteList.ToArray());
                        packetByteList.Clear();
                        isFindingStartCode = true;
                    }
                    else if (readByte == StartPacket)
                    {
                        Logger.Error($"packet error!'{StringUtils.ToHexString(packetByteList.ToArray())}'");
                        packetByteList.Clear();
                        packetByteList.Add(readByte);
                    }
                    else
                    {
                        packetByteList.Add(readByte);
                    }
                }
            }

           
        }
        private void OnReceivePayload(byte[] payloadBytes)
        {
            var dataLength = payloadBytes[3];
            if (dataLength + 7 != payloadBytes.Length)
            {
                Logger.Error($"Packet Error. Packet Lenth is wrong.");
            }

            var flameDetectorStatus = payloadBytes[4];
            if (flameDetectorStatus == 0x07 && !_isAlarmOn)
            {
                _isFlameDetected = true;
                _isAlarmOn = true;
                StationService.OnEvent(new NodeViewEvent($"{AlarmPrefix}Fire", _isFlameDetected));
            }
            else if(flameDetectorStatus == 0x02 && _isFlameDetected)
            {
                _isFlameDetected = false;
                _isAlarmOn = false;
                StationService.OnEvent(new NodeViewEvent($"{AlarmPrefix}Fire", _isFlameDetected));
            }
        }
        public void Dispose()
        {
        }
    }
}
