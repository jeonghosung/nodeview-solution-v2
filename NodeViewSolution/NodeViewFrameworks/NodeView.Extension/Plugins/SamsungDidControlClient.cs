﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.Net;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class SamsungDidControlClient : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string ConnectionStartString = "serial://";
        private readonly string _connectionString;
        private readonly bool _isTestMode;
        private readonly bool _isVerbose;
        private SerialPort _monitorSerialPort = null;
        
        public SamsungDidControlClient(string connectionString, bool isTestMode, bool isVerbose)
        {
            _connectionString = connectionString;
            _isTestMode = isTestMode;
            _isVerbose = isVerbose;
        }

        public bool Init()
        {
            string partName = "COM3";
            int baudRate = 9600;
            if (!_connectionString.StartsWith(ConnectionStartString))
            {
                Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                return false;
            }

            try
            {
                string[] connectionParams =
                        StringUtils.Split(_connectionString.Substring(ConnectionStartString.Length), '/');
                partName = connectionParams[0];
                if (connectionParams.Length > 1)
                {
                    baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
                }

                Logger.Info($"Connect({partName}, {baudRate})");
                if (!_isTestMode)
                {
                    _monitorSerialPort = new SerialPort(partName) { BaudRate = baudRate };
                    _monitorSerialPort.Open();
                    _monitorSerialPort.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Init error.");
                return false;
            }
            return true;
        }

        public void ControlPower(int monitorNumber, bool isOn)
        {
            try
            {
                byte monitorId = (byte)monitorNumber;
                if (monitorNumber == 0)
                {
                    monitorId = 0xFE;
                }
                
                byte[] packet = BuildPacket(0X11, monitorId, (isOn) ? (byte)0x01 : (byte)0x00);
                string command = StringUtils.ToHexString(packet);
                
                if (_isTestMode || _isVerbose)
                {
                    Logger.Info($"ControlPower : {command}");
                    if (_isTestMode)
                    {
                        return;
                    }
                }

                WritePacket(packet);
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlPower:");
            }
        }

        
        public void ControlPowerAll(bool isOn)
        {
            ControlPower(0, isOn);
        }

        public void ControlInputSource(int monitorNumber, int inputSource)
        {
            try
            {
                byte monitorId = (byte)monitorNumber;
                if (monitorNumber == 0)
                {
                    monitorId = 0xFE;
                }

                byte[] packet = BuildPacket(0X14, monitorId, (byte)inputSource);
                string command = StringUtils.ToHexString(packet);

                if (_isTestMode || _isVerbose)
                {
                    Logger.Info($"ControlInputSource : {command}");
                    if (_isTestMode)
                    {
                        return;
                    }
                }

                WritePacket(packet);
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSource:");
            }
        }
        public void ControlInputSourceNameAll(int value)
        {
            ControlInputSource(0, value);
        }

        public void ControlBacklight(int monitorNumber, int level)
        {
            try
            {
                byte monitorId = (byte)monitorNumber;
                if (monitorNumber == 0)
                {
                    monitorId = 0xFE;
                }

                byte[] packet = BuildPacket(0X58, monitorId, (byte)level);
                string command = StringUtils.ToHexString(packet);

                if (_isTestMode || _isVerbose)
                {
                    Logger.Info($"ControlInputSource : {command}");
                    if (_isTestMode)
                    {
                        return;
                    }
                }

                WritePacket(packet);
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSource:");
            }
        }

        public void ControlBacklightAll(int level)
        {
            ControlBacklight(0, level);
        }

        public void ControlMute(int monitorNumber, bool isMute)
        {
            try
            { 
                byte volume = isMute ? (byte)0 : (byte)50;
                byte monitorId = (byte)monitorNumber;
                if (monitorNumber == 0)
                {
                    monitorId = 0xFE;
                }

                byte[] packet = BuildPacket(0X12, monitorId, volume);
                string command = StringUtils.ToHexString(packet);

                if (_isTestMode || _isVerbose)
                {
                    Logger.Info($"ControlMute : {command}");
                    if (_isTestMode)
                    {
                        return;
                    }
                }

                WritePacket(packet);

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlMute:");
            }
        }
        /*
        public void ControlTile(int monitorNumber, int width, int height, int tileNumber = 0)
        {
            try
            {
                int count = width * height;
                tileNumber = (count <= 1) ? 0 : tileNumber;
                if (tileNumber > count)
                {
                    Logger.Error($"tileNumber error ({tileNumber} > {width} X {height}).");
                    return;
                }
                string command = $"dd {monitorNumber:X2} {width:X1}{height:X1}";

                if (_isTestMode)
                {
                    Logger.Info(command);
                    if (tileNumber > 0)
                    {
                        command = $"di {monitorNumber:X2} {tileNumber:X2}";
                        Logger.Info(command);
                    }
                    return;
                }

                using (new SerialConnection(_monitorSerialPort))
                {
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    if (tileNumber > 0)
                    {
                        Thread.Sleep(200);
                        command = $"di {monitorNumber:X2} {tileNumber:X2}";
                        _monitorSerialPort.WriteLine(command);
                    }

                    Thread.Sleep(200);
                    _monitorSerialPort.ReadExisting();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlTile:");
            }
        }
        */
        public void ControlVolume(int monitorNumber, int value)
        {
            try
            {
                value = value < 0 ? 0 : (value > 100) ? 100: value;
                byte volume = (byte)value;
                byte monitorId = (byte)monitorNumber;
                if (monitorNumber == 0)
                {
                    monitorId = 0xFE;
                }

                byte[] packet = BuildPacket(0X12, monitorId, volume);
                string command = StringUtils.ToHexString(packet);

                if (_isTestMode || _isVerbose)
                {
                    Logger.Info($"ControlVolume : {command}");
                    if (_isTestMode)
                    {
                        return;
                    }
                }

                WritePacket(packet);

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlVolume:");
            }
        }

        private byte[] BuildPacket(byte command, byte monitorId, byte data)
        {
            return BuildPacket(command, monitorId, new byte[] { data });
        }
        private byte[] BuildPacket(byte command, byte monitorId, byte[] data)
        {
            List<byte> packetBytes = new List<byte>();
            byte checkSum = 0;
            packetBytes.Add(0xAA);
            packetBytes.Add(command);
            checkSum += command;
            packetBytes.Add(monitorId);
            checkSum += monitorId;
            byte dataLength = (byte)data.Length;
            packetBytes.Add(dataLength);
            checkSum += dataLength;

            foreach (byte dataByte in data)
            {
                packetBytes.Add(dataByte);
                checkSum += dataByte;
            }
            packetBytes.Add(checkSum);
            return packetBytes.ToArray();
        }

        private void WritePacket(byte[] packet)
        {
            if (packet == null || packet.Length == 0)
            {
                return;
            }

            try
            {
                using (new SerialConnection(_monitorSerialPort))
                {
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.Write(packet, 0, packet.Length);
                    Thread.Sleep(50);
                    _monitorSerialPort.ReadExisting();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "WritePacket:");
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _monitorSerialPort.Dispose();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
