﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;
using NLog;
using NodeView.DataModels;
using NodeView.DeviceControllers;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class LgDidController : MonitorControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string ConnectionStartString = "serial://";
        private LgDidControlClient _didControlClient = null;
        private SerialPort _monitorSerialPort = null;

        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("send", "모니터 명령어 전송", new[]
            {
                new FieldDescription("value", "", "전송 명령어")
            }));

            return commandDescriptions.ToArray();
        }

        public override bool Init(Configuration config, IStationService stationService)
        {
            if (!base.Init(config, stationService))
            {
                return false;
            }

            bool res;
            try
            {
                _didControlClient = new LgDidControlClient(ConnectionString, IsTestMode, IsVerbose);
                res = _didControlClient.Init();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _monitorSerialPort?.Dispose();
                _monitorSerialPort = null;
            }
        }

        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "send", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    string value = parameters.GetValue("value", "");
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        ControlSendCommand(value);
                        isHandled = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }

        public void ControlSendCommand(string command)
        {
            try
            {
                if (IsTestMode)
                {
                    return;
                }
                using (new SerialConnection(_monitorSerialPort))
                {
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    Thread.Sleep(50);
                    _monitorSerialPort.ReadExisting();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlSendCommand:");
            }
        }

        public override void ControlPower(int monitorNumber, bool isOn)
        {
            _didControlClient.ControlPower(monitorNumber,isOn);
        }

        public override void ControlInputSource(int monitorNumber, int inputSource)
        {
            _didControlClient.ControlInputSource(monitorNumber, inputSource);
        }
        public override void ControlBacklight(int monitorNumber, int level)
        {
            if (level < 1)
            {
                level = 1;
            }
            else if (level > 5)
            {
                level = 5;
            }

            int backlightLevel = (100 - ((level-1) * 25));
            _didControlClient.ControlBacklight(monitorNumber, backlightLevel);
        }
    }
}
