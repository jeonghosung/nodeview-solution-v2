﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class TowerLampController : DeviceControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string ConnectionStartString = "tcp://";
        private const byte WriteByte = 0x57;
        private const byte ReadByte = 0x52;
        private const byte AckByte = 0x41;
        private string _ip = "127.0.0.1";
        private int _port = 20000;

        private List<string> _lampList = new List<string>()
        {
            "red", "yellow", "green", "blue", "white"
        };
        private int _lampCount { get; set; } = 5;
        public int SountGroup { get; set; } = 0;
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());
            List<FieldDescription> fieldDescriptions = new List<FieldDescription>();
            foreach (var lamp in _lampList)
            {
                fieldDescriptions.Add(new FieldDescription(lamp, "On", "경광등 제어 : On, Blink, Off", "enum", "On, Blink, Off"));
            }
            fieldDescriptions.Add(new FieldDescription("sound", "0", "사운드 설정, 미사용 : Off, 사용 : 1~5 (사운드 종류)", "enum", "Off,1,2,3,4,5"));
            commandDescriptions.Add(new CommandDescription("Control", "경광등 제어", fieldDescriptions));
            return commandDescriptions.ToArray();
        }

        public override bool Init(Configuration config, IStationService stationService)
        {
            if (!base.Init(config, stationService))
            {
                return false;
            }
            ConnectionString = config.GetValue("connectionString", ConnectionString);
            if (!ConnectionString.StartsWith(ConnectionStartString))
            {
                Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");

                return false;
            }
            string[] connectionParams = StringUtils.Split(ConnectionString.Substring(ConnectionStartString.Length), ':', 2);
            _ip = StringUtils.GetValue(connectionParams[0], _ip);
            _port = StringUtils.GetIntValue(connectionParams[1], _port);
            SountGroup = GetSoundGroup(config.GetValue("soundGroup", "ws"));
            return true;
        }
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            try
            {
                if (IsTestMode)
                {
                    string parameterValues = "";
                    foreach (var parameter in parameters)
                    {
                        parameterValues += $"{parameter.Key} : {parameter.Value}\r\n";
                    }
                    Logger.Info($"RunControlCommand [{command}][{parameterValues}]");
                    return true;
                }

                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "Control", StringComparison.Ordinal) == 0)
                {
                    string paramsString = "";
                    for (int i = 0; i < _lampList.Count; i++)
                    {
                        string lampValue = parameters.GetValue($"{_lampList[i]}", "");
                        paramsString += $"{i + 1}={ConverToLampParameter(lampValue)},";
                    }
                    string soundValue = parameters.GetValue($"sound", "off");
                    string soundParam = soundValue.Equals("off", StringComparison.OrdinalIgnoreCase) ? "0" : soundValue;
                    paramsString += $"s={soundParam}";
                    var action = new TowerLampAction(Id, paramsString, _lampCount);
                    RunAction(action);
                    return true;
                }
            }

            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunControlCommand:");
            }
            return false;

            string ConverToLampParameter(string lampStatus)
            {
                switch (lampStatus.ToLower())
                {
                    case "on":
                        return "1";
                    case "off":
                        return "0";
                    case "blink":
                        return "2";
                    default: 
                        return "0";
                }
            }
        }

        private void RunAction(TowerLampAction action)
        {
            if (action == null)
            {
                return;
            }
            try
            {
                byte[] packet = new byte[10];
                packet[0] = WriteByte;
                packet[1] = Convert.ToByte(SountGroup);
                for (int lampIndex = 0; lampIndex < action.Lamps.Length; lampIndex++)
                {
                    packet[lampIndex + 2] = Convert.ToByte(action.Lamps[lampIndex]);
                }
                packet[7] = Convert.ToByte(action.Sound);
                packet[8] = 0x00;
                packet[9] = 0x00;
                string response = string.Empty;

                using (var tcpClient = new TcpClient())
                {
                    tcpClient.SendTimeout = 1000;
                    tcpClient.ReceiveTimeout = 3000;
                    tcpClient.Connect(_ip, _port);

                    var stream = tcpClient.GetStream();
                    stream.Write(packet, 0, packet.Length);
                    //stream.ReadTimeout = request.Timeout;

                    byte[] responseBytes = new byte[10];
                    if (stream.Read(responseBytes, 0, responseBytes.Length) == responseBytes.Length)
                    {
                        if (responseBytes[0] == AckByte)
                        {
                            if (IsVerbose) Logger.Info($"Ack Data. : {responseBytes}");
                        }
                        foreach (var b in responseBytes)
                        {
                            response += $"{b:X} ";
                        }
                    }

                    tcpClient.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunAction");
            }
        }

        private int GetSoundGroup(string soundGroup)
        {
            int result = 0;
            switch (soundGroup.ToLower())
            {
                case "ws":
                    break;
                case "wp":
                    result = 1;
                    break;
                case "wm":
                    result = 2;
                    break;
                case "wa":
                    result = 3;
                    break;
                case "wb":
                    result = 4;
                    break;
            }
            return result;
        }
    }
}
