﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using NLog;
using NodeView.Net;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class LgDidControlClient : IDisposable
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private const string ConnectionStartString = "serial://";
        private readonly string _connectionString;
        private readonly bool _isTestMode;
        private readonly bool _isVerbose;
        private SerialPort _monitorSerialPort = null;
        
        public LgDidControlClient(string connectionString, bool isTestMode, bool isVerbose)
        {
            _connectionString = connectionString;
            _isTestMode = isTestMode;
            _isVerbose = isVerbose;
        }

        public bool Init()
        {
            string partName = "COM3";
            int baudRate = 9600;
            if (!_connectionString.StartsWith(ConnectionStartString))
            {
                Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                return false;
            }

            try
            {
                string[] connectionParams =
                        StringUtils.Split(_connectionString.Substring(ConnectionStartString.Length), '/');
                partName = connectionParams[0];
                if (connectionParams.Length > 1)
                {
                    baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
                }

                Logger.Info($"Connect({partName}, {baudRate})");
                if (!_isTestMode)
                {
                    _monitorSerialPort = new SerialPort(partName) { BaudRate = baudRate };
                    _monitorSerialPort.Open();
                    _monitorSerialPort.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Init error.");
                return false;
            }
            return true;
        }
        public void ControlPower(int monitorNumber, bool isOn)
        {
            try
            {
                string onOff = (isOn) ? "01" : "00";
                string command = $"ka {monitorNumber:X2} {onOff}";
                if (_isTestMode)
                {
                    Logger.Info($"Power Command : {command}");
                    return;
                }
                using (new SerialConnection(_monitorSerialPort))
                {
                    Logger.Info($"Power Command : {command}");
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    Thread.Sleep(50);
                    _monitorSerialPort.ReadExisting();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlPower:");
            }
        }
        public void ControlPowerAll(bool isOn)
        {
            ControlPower(0, isOn);
        }
        public void ControlInputSource(int monitorNumber, int inputSource)
        {
            try
            {
                string command = $"xb {monitorNumber:X2} {inputSource}";
                if (_isTestMode)
                {
                    Logger.Info(command);
                    return;
                }

                using (new SerialConnection(_monitorSerialPort))
                {
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    Thread.Sleep(50);
                    _monitorSerialPort.ReadExisting();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlInputSource:");
            }
        }
        public void ControlInputSourceNameAll(int value)
        {
            ControlInputSource(0, value);
        }

        public void ControlBacklight(int monitorNumber, int level)
        {
            try
            {
                if (level < 0)
                {
                    level = 0;
                }
                else if (level > 100)
                {
                    level = 100;
                }

                string command = $"mg {monitorNumber:X2} {level:X2}";
                if (_isTestMode)
                {
                    Logger.Info(command);
                    return;
                }

                using (new SerialConnection(_monitorSerialPort))
                {
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    Thread.Sleep(50);
                    _monitorSerialPort.ReadExisting();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlBacklight:");
            }
        }

        public void ControlBacklightAll(int level)
        {
            ControlBacklight(0, level);
        }

        public void ControlMute(int monitorNumber, bool isMute)
        {
            try
            {
                var muteCommand = isMute ? "00" : "01";
                string command = $"ke {monitorNumber:X2} {muteCommand}";

                if (_isTestMode)
                {
                    Logger.Info(command);
                    return;
                }
                using (new SerialConnection(_monitorSerialPort))
                {
                    Logger.Info($"Volume Command : {command}");
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    Thread.Sleep(100);
                    _monitorSerialPort.ReadExisting();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlMute:");
            }
        }

        public void ControlTile(int monitorNumber, int width, int height, int tileNumber = 0)
        {
            try
            {
                int count = width * height;
                tileNumber = (count <= 1) ? 0 : tileNumber;
                if (tileNumber > count)
                {
                    Logger.Error($"tileNumber error ({tileNumber} > {width} X {height}).");
                    return;
                }
                string command = $"dd {monitorNumber:X2} {width:X1}{height:X1}";

                if (_isTestMode)
                {
                    Logger.Info(command);
                    if (tileNumber > 0)
                    {
                        command = $"di {monitorNumber:X2} {tileNumber:X2}";
                        Logger.Info(command);
                    }
                    return;
                }

                using (new SerialConnection(_monitorSerialPort))
                {
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    if (tileNumber > 0)
                    {
                        Thread.Sleep(200);
                        command = $"di {monitorNumber:X2} {tileNumber:X2}";
                        _monitorSerialPort.WriteLine(command);
                    }

                    Thread.Sleep(200);
                    _monitorSerialPort.ReadExisting();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlTile:");
            }
        }
        public void ControlVolume(int monitorNumber, int value)
        {
            try
            {
                string command = $"kf {monitorNumber:X2} {value:X2}";
                if (_isTestMode)
                {
                    Logger.Info($"Volume Command : {command}");
                    return;
                }
                using (new SerialConnection(_monitorSerialPort))
                {
                    if (value < 0)
                    {
                        value = 0;
                    }
                    else if (value > 100)
                    {
                        value = 100;
                    }
                    Logger.Info($"Volume Command : {command}");
                    _monitorSerialPort.ReadExisting();
                    _monitorSerialPort.WriteLine(command);
                    Thread.Sleep(100);
                    _monitorSerialPort.ReadExisting();
                }

            }
            catch (Exception e)
            {
                Logger.Error(e, "ControlVolume:");
            }
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _monitorSerialPort.Dispose();
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
