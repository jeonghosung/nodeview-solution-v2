﻿using System;
using System.IO.Ports;
using System.Text;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Plugins;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.Utils;

namespace NodeView.Extension.Plugins
{
    public class StationSerialCommandListener: INodeViewPlugin
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string ConnectionStartString = "serial://";

        private SerialPort _serialPort = null;
        private ByteDataPool _byteDataPool = new ByteDataPool();
        private byte[] _readBuffer = new byte[1024];
        protected IStationService StationService = null;
        public bool IsTestMode { get; set; } = false;
        public bool IsVerbose { get; set; } = false;
        public bool IsStarted { get; protected set; } = false;

        public string Id { get; set; }
        public string Name { get; set; } = "";
        public string Description { get; set; } = "";
        public string ConnectionString { get; set; } = "";

        public virtual bool Init(Configuration config, IStationService stationService)
        {
            StationService = stationService;
            if (!LoadConfig(config))
            {
                return false;
            }
            if (!RegisterOnStation())
            {
                return false;
            }

            return true;
        }

        public virtual bool Start()
        {
            if (!IsStarted)
            {
                try
                {
                    if (_serialPort != null)
                    {
                        _serialPort.DataReceived += SerialPortOnDataReceived;
                        _serialPort.Open();
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Start:");
                }
                IsStarted = _serialPort?.IsOpen ?? false;
            }
            return IsStarted;
        }

        public virtual void Stop()
        {
            if (_serialPort != null)
            {
                _serialPort.DataReceived -= SerialPortOnDataReceived;
                _serialPort.Close();
            }
            
            UnRegisterOnStation();
            StationService = null;
            IsStarted = false;
        }

        private bool RegisterOnStation()
        {
            return true;
        }

        protected virtual void UnRegisterOnStation()
        {
            
        }

        protected virtual bool LoadConfig(Configuration config)
        {
            if (config == null)
            {
                if (string.IsNullOrWhiteSpace(Id)) return false;
                return true;
            }

            bool res = false;
            try
            {
                if (string.IsNullOrWhiteSpace(Id))
                {
                    Id = StringUtils.GetStringValue(config.Id, Uuid.NewUuid);
                }

                if (string.IsNullOrWhiteSpace(Name))
                {
                    Name = this.GetType().Name;
                }
                Description = config.GetValue("@description", "");
                ConnectionString = config.GetValue("connectionString", "");
                IsTestMode = config.GetValue("isTestMode", false); ;
                IsVerbose = IsTestMode || config.GetValue("isVerbose", false);

                string partName = "COM3";
                int baudRate = 9600;
                if (!ConnectionString.StartsWith(ConnectionStartString))
                {
                    Logger.Error($"ConnectionString should be started with '{ConnectionStartString}'.");
                    return false;
                }

                string[] connectionParams = StringUtils.Split(ConnectionString.Substring(ConnectionStartString.Length), '/');
                partName = connectionParams[0];
                if (connectionParams.Length > 1)
                {
                    baudRate = StringUtils.GetIntValue(connectionParams[1], baudRate);
                }

                Logger.Info($"Connect({partName}, {baudRate})");
                if (!IsTestMode)
                {
                    _serialPort = new SerialPort(partName) { BaudRate = baudRate };
                    _serialPort.ReadTimeout = 200;
                }
                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Init:");
                res = false;
            }

            return res;
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _serialPort.Dispose();
                _serialPort = null;
            }
        }

        public void Dispose()
        {
            Stop();
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void SerialPortOnDataReceived(object sender, SerialDataReceivedEventArgs args)
        {
            try
            {
                int readCount = 0;
                int readBufferLength = _readBuffer.Length;
                try
                {
                    while ((readCount = _serialPort.Read(_readBuffer, 0, _readBuffer.Length)) > 0)
                    {
                        _byteDataPool.Append(_readBuffer, 0, readCount);
                    }
                }
                catch
                {
                    //skip
                }

                CheckPacket();
            }
            catch (Exception e)
            {
                Logger.Error(e, "SerialPortOnDataReceived:");
            }
        }

        private void CheckPacket()
        {
            byte[] readBuffer = _byteDataPool.ReadAll();
            byte[] packetBuffer = new byte[readBuffer.Length];
            int payloadCount = 0;

            int readBufferOffset = -1;
            bool isPayloadParsing = false;
            while (readBuffer.Length != 0 && readBufferOffset < readBuffer.Length)
            {
                readBufferOffset++;
                if (!isPayloadParsing)
                {
                    if (readBuffer[readBufferOffset] == 0x02)
                    {
                        isPayloadParsing = true;
                    }
                }
                else
                {
                    if (readBuffer[readBufferOffset] != 0x03)
                    {
                        packetBuffer[payloadCount++] = readBuffer[readBufferOffset];
                    }
                    else
                    {
                        string payload = Encoding.UTF8.GetString(packetBuffer, 0, payloadCount);
                        readBuffer = readBuffer.SubArray(readBufferOffset, readBuffer.Length - (readBufferOffset+1));
                        ReceivePayload(payload);
                        readBufferOffset = -1;
                    }
                }
            }

            if (readBuffer.Length > 0)
            {
                _byteDataPool.Append(readBuffer);
            }
        }

        private void ReceivePayload(string payload)
        {
            try
            {
                JObject requestJson = JObject.Parse(payload);
                string command = JsonUtils.GetStringValue(requestJson, "command");
                JObject bodyJson = JsonUtils.GetValue<JObject>(requestJson, "body", null);
                switch (command)
                {
                    case "riseEvent":
                        if (bodyJson != null)
                        {
                            var nodeViewEvent = NodeViewEvent.CreateFrom(bodyJson);
                            if (!string.IsNullOrWhiteSpace(nodeViewEvent?.Id))
                            {
                                bool isSuccess = StationService.OnEvent(nodeViewEvent);
                                string resMessage = isSuccess ? $"Success [{nodeViewEvent.Id}]." : $"cannot run the event action.";
                                Logger.Info(resMessage);
                            }
                        }
                        break;

                    default:
                        Logger.Warn($"[{command}] is not supported!");
                        break;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ReceivePayload:");
            }
        }
    }
}
