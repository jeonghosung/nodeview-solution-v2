﻿using Microsoft.Data.Sqlite;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Frameworks.Stations;
using NodeView.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace NodeView.Extension.Stations
{
    public class SqliteStationRepository : IStationRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        const int EventLogMaxSize = 1000;

        private readonly Dictionary<string, NodeViewAction> _actionDictionary = new Dictionary<string, NodeViewAction>();
        private readonly Dictionary<string, EventAction> _eventActionDictionary = new Dictionary<string, EventAction>();
        private readonly List<EventLog> _eventLogList = new List<EventLog>();
        private readonly string _dbNodesFilename;
        private readonly string _dbEventLogFilename;
        private string _connectionNodesString;
        private string _connectionEventLogsString;

        public string CachesFolder { get; protected set; } = "";
        public string ConfigsFolder { get; protected set; } = "";

        public SqliteStationRepository()
        {
            _dbNodesFilename = "nvNodes.bin";
            _dbEventLogFilename = "nvEventLogs.bin";
            CachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(CachesFolder);
            ConfigsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            Directory.CreateDirectory(ConfigsFolder);

            SetupDatabase();
            LoadActions();
            LoadEventActions();
            LoadEventLogs();
        }

        public IEnumerable<NodeViewAction> GetActions()
        {
            return _actionDictionary.Values.ToArray();
        }

        public int CreateActions(IEnumerable<NodeViewAction> actions)
        {
            if (actions == null) return 0;

            int updatedCount = 0;
            foreach (var action in actions)
            {
                if (string.IsNullOrWhiteSpace(action.Id))
                {
                    continue;
                }

                _actionDictionary[action.Id] = action;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveActions();
            }

            return updatedCount;
        }

        public int UpdateActions(IEnumerable<NodeViewAction> actions)
        {
            if (actions == null) return 0;

            int updatedCount = 0;
            foreach (var action in actions)
            {
                if (string.IsNullOrWhiteSpace(action.Id))
                {
                    continue;
                }

                _actionDictionary[action.Id] = action;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveActions();
            }

            return updatedCount;
        }

        public int DeleteActions(IEnumerable<string> uids)
        {
            if (uids == null) return 0;

            int updatedCount = 0;
            foreach (var uid in uids)
            {
                if (string.IsNullOrWhiteSpace(uid))
                {
                    continue;
                }

                if (_actionDictionary.ContainsKey(uid))
                {
                    _actionDictionary.Remove(uid);
                    updatedCount++;
                }
            }
            if (updatedCount > 0)
            {
                SaveActions();
            }
            return updatedCount;
        }

        public void DeleteAllActions()
        {
            if (_actionDictionary.Count > 0)
            {
                _actionDictionary.Clear();
                SaveActions();
            }
        }

        public IEnumerable<EventAction> GetEventActions()
        {
            return _eventActionDictionary.Values.ToArray();
        }

        public int CreateEventActions(IEnumerable<EventAction> eventActions)
        {
            if (eventActions == null) return 0;

            int updatedCount = 0;
            foreach (var eventAction in eventActions)
            {
                if (string.IsNullOrWhiteSpace(eventAction.Id))
                {
                    continue;
                }

                _eventActionDictionary[eventAction.Id] = eventAction;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveEventActions();
            }

            return updatedCount;
        }

        public int UpdateEventActions(IEnumerable<EventAction> eventActions)
        {
            if (eventActions == null) return 0;

            int updatedCount = 0;
            foreach (var eventAction in eventActions)
            {
                if (string.IsNullOrWhiteSpace(eventAction.Id))
                {
                    continue;
                }

                _eventActionDictionary[eventAction.Id] = eventAction;
                updatedCount++;
            }

            if (updatedCount > 0)
            {
                SaveActions();
            }

            return updatedCount;
        }

        public int DeleteEventActions(IEnumerable<string> uids)
        {
            if (uids == null) return 0;

            int updatedCount = 0;
            foreach (var uid in uids)
            {
                if (string.IsNullOrWhiteSpace(uid))
                {
                    continue;
                }

                if (_eventActionDictionary.ContainsKey(uid))
                {
                    _eventActionDictionary.Remove(uid);
                    updatedCount++;
                }
            }
            if (updatedCount > 0)
            {
                SaveEventActions();
            }
            return updatedCount;
        }

        public void DeleteAllEventActions()
        {
            if (_eventActionDictionary.Count > 0)
            {
                _eventActionDictionary.Clear();
                SaveEventActions();
            }
        }

        public IEnumerable<INode> GetNodes()
        {
            if (string.IsNullOrWhiteSpace(_connectionNodesString)) return new INode[0];

            List<INode> nodeList = new List<INode>();

            try
            {
                using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                {
                    dbConnection.Open();
                    String Sql = "select * from nodes";

                    SqliteCommand cmd = new SqliteCommand(Sql, dbConnection);
                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string json = (string)reader["json"];
                            if (!string.IsNullOrWhiteSpace(json))
                            {
                                JObject jObject = JObject.Parse(json);
                                if (jObject != null)
                                {
                                    Node node = Node.CreateFrom(jObject);
                                    if (node != null)
                                    {
                                        nodeList.Add(node);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetNodes:");
            }

            return nodeList.ToArray();
        }

        public int CreateNodes(IEnumerable<INode> nodes)
        {
            if (nodes == null) return 0;

            int updatedCount = 0;
            try
            {
                String Sql = "insert into nodes (id, path, updated, json) values ($id, $path, $updated, $json)";

                using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                {
                    dbConnection.Open();

                    foreach (var node in nodes)
                    {
                        if (string.IsNullOrWhiteSpace(node.PathId))
                        {
                            continue;
                        }

                        SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                        command.Parameters.AddWithValue("$id", node.PathId);
                        command.Parameters.AddWithValue("$path", node.Path);
                        command.Parameters.AddWithValue("$updated", $"{node.Updated}");
                        command.Parameters.AddWithValue("$json", node.ToJson().ToString());
                        updatedCount += command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateNodes:");
            }
            return updatedCount;
        }

        public int UpdateNodes(IEnumerable<INode> nodes)
        {
            if (nodes == null) return 0;

            int updatedCount = 0;
            try
            {
                String Sql = "update nodes set updated=$updated, json=$json where id=$id";

                using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                {
                    dbConnection.Open();

                    foreach (var node in nodes)
                    {
                        if (string.IsNullOrWhiteSpace(node.PathId))
                        {
                            continue;
                        }

                        SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                        command.Parameters.AddWithValue("$id", node.PathId);
                        command.Parameters.AddWithValue("$updated", $"{node.Updated}");
                        command.Parameters.AddWithValue("$json", node.ToJson());
                        updatedCount += command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "UpdateNodes:");
            }
            return updatedCount;
        }

        public int DeleteNodes(IEnumerable<string> uids)
        {
            if (uids == null) return 0;

            int updatedCount = 0;
            try
            {
                String Sql = "delete from nodes where id=$id";

                using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                {
                    dbConnection.Open();

                    foreach (var uid in uids)
                    {
                        if (string.IsNullOrWhiteSpace(uid))
                        {
                            continue;
                        }

                        SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                        command.Parameters.AddWithValue("$id", uid);
                        updatedCount += command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "DeleteNodes:");
            }
            return updatedCount;
        }

        public IEnumerable<IEventLog> GetLatestEventLogs()
        {
            return _eventLogList.ToArray();
        }

        public bool AppendEventLog(IEventLog eventLog)
        {
            bool res = false;
            if (eventLog == null) return false;

            try
            {
                EventLog newEventLog = new EventLog(eventLog);

                _eventLogList.Insert(0, newEventLog);
                if (_eventLogList.Count > EventLogMaxSize)
                {
                    _eventLogList.RemoveAt(_eventLogList.Count - 1);
                }

                String Sql = "insert into eventLogs (timestamp, eventLevel, senderName, senderIp, message) values ($timestamp, $eventLevel, $senderName, $senderIp, $message)";

                using (SqliteConnection dbConnection = new SqliteConnection(_connectionEventLogsString))
                {
                    dbConnection.Open();

                    SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                    command.Parameters.AddWithValue("$timestamp", newEventLog.Timestamp);
                    command.Parameters.AddWithValue("$eventLevel", $"{newEventLog.Level}");
                    command.Parameters.AddWithValue("$senderName", newEventLog.SenderName);
                    command.Parameters.AddWithValue("$senderIp", newEventLog.SenderIp);
                    command.Parameters.AddWithValue("$message", newEventLog.Message);
                    if (command.ExecuteNonQuery() > 0)
                    {
                        res = true;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "AppendEventLog:");
                res = false;
            }
            return res;
        }

        public IEnumerable<IEventLog> SearchEventLogs(string fromDate, string toDate, string searchKey, bool ascendingOrder = true, int maxLength = 1000)
        {
            if (string.IsNullOrWhiteSpace(fromDate) || string.IsNullOrWhiteSpace(toDate)) return new IEventLog[0];
            if (string.IsNullOrWhiteSpace(_connectionEventLogsString)) return new IEventLog[0];

            List<IEventLog> eventLogs = new List<IEventLog>();
            try
            {
                string startTime = $"{fromDate} 00:00:00";
                string endTime = $"{toDate} 99:99:99";
                string searchSql = "";
                if (!string.IsNullOrWhiteSpace(searchKey))
                {
                    searchSql = $" and message like '%{searchKey}%' ";
                }

                string orderBySql = "order by timestamp";
                if (!ascendingOrder)
                {
                    orderBySql += " desc";
                }
                using (SqliteConnection dbConnection = new SqliteConnection(_connectionEventLogsString))
                {
                    dbConnection.Open();
                    String Sql = $"select * from eventlogs where timestamp >= '{startTime}' and  timestamp <= '{endTime}' {searchSql} {orderBySql} limit {maxLength}";

                    SqliteCommand cmd = new SqliteCommand(Sql, dbConnection);
                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string timestamp = (string)reader["timestamp"];
                            AlarmEventLevel eventLevel = StringUtils.ToEnum((string)reader["eventLevel"], AlarmEventLevel.Normal);
                            string senderName = (string)reader["senderName"];
                            string senderIp = (string)reader["senderIp"];
                            string message = (string)reader["message"];
                            eventLogs.Add(new EventLog(timestamp, eventLevel, senderName, senderIp, message));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "SearchEventLogs:");
            }
            return eventLogs.ToArray();
        }

        public void DeleteAllNodes(string path, bool recursive = false)
        {
            if (string.IsNullOrWhiteSpace(path)) return;

            try
            {
                if (recursive)
                {
                    String Sql = "delete from nodes where path like $path";
                    using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                    {
                        SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                        command.Parameters.AddWithValue("$path", path);
                        command.ExecuteNonQuery();
                    }
                }
                else
                {
                    String Sql = "delete from nodes where path like $path";
                    using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                    {
                        SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                        command.Parameters.AddWithValue("$path", path + "%");
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "DeleteAllNodes:");
            }
        }

        private void LoadActions()
        {
            string eventsFile = Path.Combine(ConfigsFolder, "actions.xml");
            if (!File.Exists(eventsFile))
            {
                return;
            }

            try
            {
                _actionDictionary.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(eventsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Actions/Action");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        var action = NodeViewAction.CreateFrom(node);
                        if (action != null)
                        {
                            _actionDictionary[action.Id] = action;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadActions:");
            }
        }
        private void SaveActions()
        {
            string actionsFile = Path.Combine(ConfigsFolder, "actions.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(actionsFile))
                {
                    xmlWriter.WriteStartElement("Actions");
                    foreach (var action in _actionDictionary.Values.ToArray())
                    {
                        action.WriteXml(xmlWriter, "Action");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveActions:");
            }
        }

        private void LoadEventActions()
        {
            string eventActionsFile = Path.Combine(ConfigsFolder, "eventActions.xml");
            if (!File.Exists(eventActionsFile))
            {
                //Logger.Error($"Cannot find presets file[{presetsFile}].");
                return;
            }

            try
            {
                _eventActionDictionary.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(eventActionsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/EventActions/EventAction");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        var evnetAction = EventAction.CreateFrom(node);
                        if (evnetAction != null)
                        {
                            _eventActionDictionary[evnetAction.Id] = evnetAction;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadEventActions:");
            }
        }
        private void SaveEventActions()
        {
            string eventActionsFile = Path.Combine(ConfigsFolder, "eventActions.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(eventActionsFile))
                {
                    xmlWriter.WriteStartElement("EventActions");
                    foreach (var eventAction in _eventActionDictionary.Values.ToArray())
                    {
                        eventAction.WriteXml(xmlWriter, "EventAction");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveEventActions:");
            }
        }

        private void LoadEventLogs()
        {
            if (string.IsNullOrWhiteSpace(_connectionEventLogsString)) return;

            _eventLogList.Clear();
            try
            {
                using (SqliteConnection dbConnection = new SqliteConnection(_connectionEventLogsString))
                {
                    dbConnection.Open();
                    String Sql = $"select * from eventLogs order by timestamp desc limit {EventLogMaxSize}";

                    SqliteCommand cmd = new SqliteCommand(Sql, dbConnection);
                    using (SqliteDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string timestamp = (string)reader["timestamp"];
                            AlarmEventLevel eventLevel = StringUtils.ToEnum((string)reader["eventLevel"], AlarmEventLevel.Normal);
                            string senderName = (string)reader["senderName"];
                            string senderIp = (string)reader["senderIp"];
                            string message = (string)reader["message"];
                            _eventLogList.Add(new EventLog(timestamp, eventLevel, senderName, senderIp, message));
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadEventLogs:");
            }
        }

        private void SetupDatabase()
        {
            try
            {
                SetupNodesDatabase();
                SetupEventLogsDatabase();
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetupDatabase:");
                _connectionNodesString = "";
            }
        }

        private bool SetupNodesDatabase()
        {
            if (string.IsNullOrWhiteSpace(_dbNodesFilename))
            {
                Logger.Error($"db file for nodes name is empty.");
                return true;
            }

            string dbFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _dbNodesFilename);
            if (!File.Exists(dbFilename))
            {
                Logger.Info($"Create db file '{dbFilename}'.");
                _connectionNodesString = new SqliteConnectionStringBuilder($"Data Source={dbFilename}")
                {
                    Mode = SqliteOpenMode.ReadWriteCreate,
                    Password = "semsolcharles"
                }.ToString();
                //SqliteConnection.ClearAllPools(dbFilename);
                using (SqliteConnection dbConnection = new SqliteConnection(_connectionNodesString))
                {
                    dbConnection.Open();

                    string Sql =
                        "create table nodes (id VARCHAR (300) PRIMARY KEY UNIQUE NOT NULL, path VARCHAR (200)  DEFAULT ('') NOT NULL, updated VARCHAR (50)  DEFAULT ('') NOT NULL, json TEXT DEFAULT ('') NOT NULL);";
                    SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                    int result = command.ExecuteNonQuery();

                    Sql = "create index updatedIndex on nodes (updated);create index pathIndex on nodes (path);";
                    command = new SqliteCommand(Sql, dbConnection);
                    result = command.ExecuteNonQuery();
                }
            }

            _connectionNodesString = new SqliteConnectionStringBuilder($"Data Source={dbFilename}")
            {
                Mode = SqliteOpenMode.ReadWrite,
                Password = "semsolcharles"
            }.ToString();
                //$"Data Source={dbFilename};Password=semsolcharles;";
            return false;
        }

        private bool SetupEventLogsDatabase()
        {
            if (string.IsNullOrWhiteSpace(_dbEventLogFilename))
            {
                Logger.Error($"db file name for eventLogs is empty.");
                return true;
            }

            //_dbEventLogFilename
            string dbFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, _dbEventLogFilename);
            if (!File.Exists(dbFilename))
            {
                Logger.Info($"Create db file '{dbFilename}'.");

                //SqliteConnection.CreateFile(dbFilename);
                _connectionEventLogsString = new SqliteConnectionStringBuilder($"Data Source={dbFilename}")
                {
                    Mode = SqliteOpenMode.ReadWriteCreate,
                    Password = "semsolcharles"
                }.ToString();
                using (SqliteConnection dbConnection = new SqliteConnection(_connectionEventLogsString))
                {
                    dbConnection.Open();

                    //var changePasswordCommand = dbConnection.CreateCommand();
                    //changePasswordCommand.CommandText = "SELECT quote($newPassword);";
                    //changePasswordCommand.Parameters.AddWithValue("$newPassword", "semsolcharles");
                    //var quotedNewPassword = (string)changePasswordCommand.ExecuteScalar();

                    //changePasswordCommand.CommandText = "PRAGMA rekey = " + quotedNewPassword;
                    //changePasswordCommand.Parameters.Clear();
                    //changePasswordCommand.ExecuteNonQuery();

                    //dbConnection.ChangePassword("semsolcharles");


                    string Sql =
                        "create table eventlogs (timestamp VARCHAR (30) DEFAULT ('') NOT NULL, eventLevel VARCHAR (30) DEFAULT ('') NOT NULL, senderName VARCHAR (50) DEFAULT ('') NOT NULL, senderIp VARCHAR (50) DEFAULT ('') NOT NULL, message VARCHAR (500) DEFAULT ('') NOT NULL)";
                    SqliteCommand command = new SqliteCommand(Sql, dbConnection);
                    int result = command.ExecuteNonQuery();

                    Sql = "create index timestampIndex on eventlogs (timestamp);create index senderNameIndex on eventlogs (senderName);";
                    command = new SqliteCommand(Sql, dbConnection);
                    result = command.ExecuteNonQuery();
                }
            }

            _connectionEventLogsString = new SqliteConnectionStringBuilder($"Data Source={dbFilename}")
            {
                Mode = SqliteOpenMode.ReadWrite,
                Password = "semsolcharles"
            }.ToString();
            //_connectionEventLogsString = $"Data Source={dbFilename};Version=3;Password=semsolcharles;";
            return false;
        }
    }
}
