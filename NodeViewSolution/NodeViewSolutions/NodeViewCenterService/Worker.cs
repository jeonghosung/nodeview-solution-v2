using NLog;
using NodeView.Apps;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Extension.Stations;
using NodeView.Frameworks.Modularity;
using NodeView.Frameworks.Stations;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeViewCenter;
using Prism.DryIoc;
using Prism.Ioc;

namespace NodeViewCenterService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private static Logger Logger = LogManager.GetCurrentClassLogger();
        private static ModuleManager _nodeViewModuleManager = new ModuleManager();

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            CenterService service = CenterService.Instance;
            string configFilename = "NodeViewCenter.config.xml";
            
            var container = new DryIocContainerExtension();
            NodeViewContainerExtension.CreateInstance(container);
            container.RegisterInstance<IAuthentication>(new NodeViewAppAdminAuthentication());

            //StationService
            container.RegisterSingleton(typeof(IStationRepository), typeof(SqliteStationRepository));
            container.RegisterInstance<StationServiceManager>(service);
            container.RegisterInstance(typeof(ModuleManager), _nodeViewModuleManager);

            Logger.Info("Start ====================== ");

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var config = Configuration.Load(settingFile);
            if (config == null)
            {
                Logger.Error("Cannot load config!");
                Environment.Exit(1);
                return;
            }

            _nodeViewModuleManager.AddAssembly("NodeView.Extension.dll");
            if (config.TryGetNode("extensions", out var extensionsSection))
            {
                foreach (var extenstionProperty in extensionsSection.Properties)
                {
                    if (!_nodeViewModuleManager.AddAssembly(extenstionProperty.Value))
                    {
                        Logger.Error($"{extenstionProperty.Value}를 로드하는중에 오류가 발생했습니다.");
                        Environment.Exit(1);
                        return;
                    }
                }
            }

            RegisterTypes(container);
            OnCreatedShell(container, Array.Empty<string>());

            if (!service.Initialize(container, configFilename))
            {
                Logger.Error("service Initialize error!");
                Environment.Exit(1);
                return;
            }
            Thread.Sleep(100);
            OnInitialized(container);
            try
            {
                if (!service.Start())
                {
                    Logger.Error("service start error!");
                    Environment.Exit(1);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
                service.Stop();
                Environment.Exit(1);
            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                CenterService service = CenterService.Instance;
                service.Stop();
                Logger.Info("========================= End");
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
            }
            return base.StopAsync(cancellationToken);
        }
        private static void RegisterTypes(IContainerExtension containerExtension)
        {
            IContainerRegistry containerRegistry = containerExtension;

            _nodeViewModuleManager.RegisterTypes(containerRegistry);
        }

        private static void OnCreatedShell(IContainerProvider container, string[] args)
        {
            _nodeViewModuleManager.OnCreatedShell(container, args);
        }

        private static void OnInitialized(IContainerProvider container)
        {
            _nodeViewModuleManager.OnInitialized(container);
        }
    }
}