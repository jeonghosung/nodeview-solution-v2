﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeViewDataWall.Models;

namespace NodeViewDataWall
{
    public class NodeViewScreenProxyManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private Timer _actionQueueWorkerTimer = null;
        private Timer _screenWatcherTimer = null;

        private readonly ConcurrentQueue<Action> _actionQueue = new ConcurrentQueue<Action>();

        private volatile bool _isStarted = false;
        private NodeViewScreenProxy _screenPrimary = null;
        private NodeViewScreenProxy _screenSecondary = null;

        public bool IsTestMode { get; set; } = false;
        private void ActionQueueWorkerTimerCallback(object state)
        {
            _actionQueueWorkerTimer.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                while (_actionQueue.TryDequeue(out var action))
                {
                    action();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ActionQueueWorkerTimerCallback:");
            }
            _actionQueueWorkerTimer.Change(TimeSpan.FromMilliseconds(200), TimeSpan.FromMilliseconds(200));
        }
        private void ScreenWatcherTimerCallback(object state)
        {
            _screenWatcherTimer.Change(Timeout.Infinite, Timeout.Infinite);
            try
            {
                var screenPrimary = _screenPrimary;
                var screenSecondary = _screenSecondary;
                if (screenPrimary != null && screenPrimary.IsJoined && screenPrimary.CheckStatus() == NodeViewScreenStatus.Stopped)
                {
                    Logger.Error($"Primary Screen was stopped. try to restat.");
                    if (screenSecondary != null && screenSecondary.IsJoined && screenSecondary.CheckStatus() == NodeViewScreenStatus.Started)
                    {
                        _screenSecondary = null;
                        _screenPrimary = screenSecondary;
                    }
                    else
                    {
                        CloseAllScreens();
                        NodeViewScreenProxy nodeViewScreen = CreateNewScreen();
                        if (nodeViewScreen == null)
                        {
                            string errorMsg = $"Cannot create a screen application.";
                            Logger.Error(errorMsg);
                            _screenPrimary = null;
                        }
                        else
                        {
                            _screenPrimary = nodeViewScreen;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ScreenWatcherTimerCallback:");
            }
            _screenWatcherTimer.Change(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));
        }

        public bool Start()
        {
            if (_isStarted)
            {
                return true;
            }

            _isStarted = true;
            CloseAllScreens();

            if (IsTestMode) return _isStarted;

            try
            {
                NodeViewScreenProxy nodeViewScreen = CreateNewScreen();
                if (nodeViewScreen == null)
                {
                    string errorMsg = $"Cannot create a screen application.";
                    Logger.Error(errorMsg);
                    _screenPrimary = null;
                }
                else
                {
                    _screenPrimary = nodeViewScreen;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Start:");
                _screenPrimary = null;
            }
            _isStarted = _screenPrimary != null;
            if (_isStarted)
            {
                if (_actionQueueWorkerTimer == null)
                {
                    _actionQueueWorkerTimer = new Timer(ActionQueueWorkerTimerCallback);
                }
                _actionQueueWorkerTimer?.Change(TimeSpan.FromMilliseconds(200), TimeSpan.FromMilliseconds(200));

                if (_screenWatcherTimer == null)
                {
                    _screenWatcherTimer = new Timer(ScreenWatcherTimerCallback);
                }
                _screenWatcherTimer?.Change(TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(2));
            }
            return _isStarted;
        }

        public void Stop()
        {
            _actionQueueWorkerTimer?.Change(Timeout.Infinite, Timeout.Infinite);
            _screenWatcherTimer?.Change(Timeout.Infinite, Timeout.Infinite);
            CloseAllScreens();
            _isStarted = false;
        }

        public bool JoinScreen(string id, int port, int pid)
        {
            if (IsTestMode)
            {
                
                NodeViewScreenProxy nodeViewScreen = CreateNewScreen(id, port, pid);
                if (nodeViewScreen != null)
                {
                    CloseAllScreens(pid);
                    _screenPrimary = nodeViewScreen;
                    return _screenPrimary.IsJoined;
                }
                return false;
            }

            bool res = false;
            if (_screenPrimary.Id == id)
            {
                _screenPrimary.SetPid(pid);
                _screenPrimary.SetServicePort(port);
                res = _screenPrimary.IsJoined;
            }
            if (!res && _screenSecondary.Id == id)
            {
                _screenPrimary.SetPid(pid);
                _screenPrimary.SetServicePort(port);
                res = _screenSecondary.IsJoined;
            }

            return res;
        }

        private NodeViewScreenProxy CreateNewScreen(string id = "", int port = 0, int pid = 0)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                id = Uuid.NewUuid;
            }

            NodeViewScreenProxy nodeViewScreen = null;
            if (pid == 0)
            {
                nodeViewScreen = new NodeViewScreenProxy(id, port);
                nodeViewScreen.Start();
            }
            else
            {
                ScreenApplication screenApplication = new ScreenApplication(id, pid);
                nodeViewScreen = new NodeViewScreenProxy(screenApplication, port);
            }

            if (nodeViewScreen.CheckStatus() != NodeViewScreenStatus.Started)
            {
                nodeViewScreen.Stop();
                return null;
            }
            return nodeViewScreen;
        }

        public void CloseAllScreens(int exceptPid = 0)
        {
            try
            {
                /*
                _screenPrimary?.Stop();
                _screenSecondary?.Stop();
                */
                _screenPrimary = null;
                _screenSecondary = null;

                CloseAllNodeViewScreenProcesses(exceptPid);
            }
            catch
            {
                //skip
            }
        }
        public void CloseAllNodeViewScreenProcesses(int exceptPid = 0)
        {
            try
            {
                var processes = Process.GetProcessesByName("NodeViewScreen");
                foreach (var process in processes)
                {
                    try
                    {
                        if (exceptPid != 0 && process.Id == exceptPid)
                        {
                            continue;
                        }
                        process.Kill();
                    }
                    catch
                    {
                        //skip
                    }
                }
            }
            catch
            {
                //skip
            }
        }

        public void CreateWindows(DataWallWindow[] createWindows)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionCreateWindows(createWindows);
            });
        }
        public void ActionCreateWindows(DataWallWindow[] createWindows)
        {
            _screenPrimary?.CreateWindows(createWindows);
            _screenSecondary?.CreateWindows(createWindows);
        }

        public void CloseWindowsInRect(IntRect rect)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionCloseWindowsInRect(rect);
            });
        }
        public void ActionCloseWindowsInRect(IntRect rect)
        {
            _screenPrimary?.CloseWindowsInRect(rect);
            _screenSecondary?.CloseWindowsInRect(rect);
        }

        public void CloseAllWindows()
        {
            _actionQueue.Enqueue(() =>
            {
                ActionCloseAllWindows();
            });
        }
        public void ActionCloseAllWindows()
        {
            _screenPrimary?.CloseAllWindows();
            _screenSecondary?.CloseAllWindows();
        }

        public void CloseWindows(string[] closeWindowIds)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionCloseWindows(closeWindowIds);
            });
        }
        public void ActionCloseWindows(string[] closeWindowIds)
        {
            _screenPrimary?.CloseWindows(closeWindowIds);
            _screenSecondary?.CloseWindows(closeWindowIds);
        }

        public void CreatePopups(DataWallWindow[] createPopups)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionCreatePopups(createPopups);
            });
        }
        public void ActionCreatePopups(DataWallWindow[] createPopups)
        {
            _screenPrimary?.CreatePopups(createPopups);
            _screenSecondary?.CreatePopups(createPopups);
        }

        public void ClosePopupsInRect(IntRect rect)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionClosePopupsInRect(rect);
            });
        }
        public void ActionClosePopupsInRect(IntRect rect)
        {
            _screenPrimary?.ClosePopupsInRect(rect);
            _screenSecondary?.ClosePopupsInRect(rect);
        }

        public void CloseAllPopups()
        {
            _actionQueue.Enqueue(() =>
            {
                ActionCloseAllPopups();
            });
        }
        public void ActionCloseAllPopups()
        {
            _screenPrimary?.CloseAllPopups();
            _screenSecondary?.CloseAllPopups();
        }

        public void ClosePopups(string[] closePopupIds)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionClosePopups(closePopupIds);
            });
        }
        public void ActionClosePopups(string[] closePopupIds)
        {
            _screenPrimary?.ClosePopups(closePopupIds);
            _screenSecondary?.ClosePopups(closePopupIds);
        }

        public void MovePopup(string windowId, IntRect windowRect)
        {
            _actionQueue.Enqueue(() =>
            {
                ActionMovePopup(windowId, windowRect);
            });
        }

        private void ActionMovePopup(string windowId, IntRect windowRect)
        {
            _screenPrimary?.MovePopup(windowId, windowRect);
            _screenSecondary?.MovePopup(windowId, windowRect);
        }
    }
}