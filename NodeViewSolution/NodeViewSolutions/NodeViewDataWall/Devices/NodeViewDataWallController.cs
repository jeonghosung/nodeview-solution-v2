﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.DeviceControllers;
using NodeView.Drawing;

namespace NodeViewDataWall.Devices
{
    public class NodeViewDataWallController : WallControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NodeViewDataWallController(string id)
        {
            Id = id;
            DeviceType = "wall";
            Name = "DataWallController";
            Description = "NodeView DataWall";
            ConnectionString = "";
            DataWallService dataWallService = DataWallService.Instance;
            ScreenSize = new IntSize(dataWallService.ScreenSize);
            Matrix = new IntSize(dataWallService.ScreenMatrix);
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("refresh", "DataWall 화면 리플레시"));

            return commandDescriptions.ToArray();
        }
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "refresh", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    DataWallService.Instance.Refresh();
                    isHandled = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
        
        public override IDataWallWindow CreateWindow(string id, string contentId, IntRect windowRect)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
            {
                return null;
            }
            var window = new DataWallWindow(id, contentId, false, windowRect, contentId);
            var windows = DataWallService.Instance.CreateWindows(new []{window});
            if (windows != null && windows.Length > 0)
            {
                return windows[0];
            }

            return null;
        }

        public override void CloseWindow(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            DataWallService.Instance.CloseWindows(new[] { id });
        }

        public override void CloseAllWindows()
        {
            DataWallService.Instance.CloseAllWindows();
        }

        public override IDataWallWindow CreatePopup(string id, string contentId, IntRect windowRect)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
            {
                return null;
            }
            var window = new DataWallWindow(id, contentId, true, windowRect, contentId);
            var windows = DataWallService.Instance.CreatePopups(new[] { window });
            if (windows != null && windows.Length > 0)
            {
                return windows[0];
            }

            return null;
        }

        public override void MovePopup(string id, IntRect windowRect)
        {
            DataWallService.Instance.MovePopup(id, windowRect);
        }

        public override void ClosePopup(string id)
        {
            DataWallService.Instance.ClosePopups(new[] { id });
        }

        public override void CloseAllPopups()
        {
            DataWallService.Instance.CloseAllPopups();
        }

        public override bool ApplyPreset(string id)
        {
            return DataWallService.Instance.ApplyPreset(id);
        }
    }
}
