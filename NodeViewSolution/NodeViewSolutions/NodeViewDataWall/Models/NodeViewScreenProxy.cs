﻿using System;
using Newtonsoft.Json.Linq;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Net;
using NodeView.Utils;
using Prism.Mvvm;

namespace NodeViewDataWall.Models
{
    public class NodeViewScreenProxy: BindableBase
    {
        private ScreenApplication _application = null;
        private DateTime _updated = CachedDateTime.Now;
        private NodeViewScreenStatus _status = NodeViewScreenStatus.None;

        private int _servicePort = 0;
        private RestApiClient _screenApiClient = null;
        public string Id { get; } = "";
        public DateTime Updated
        {
            get => _updated;
            private set => SetProperty(ref _updated, value);
        }

        public NodeViewScreenStatus Status
        {
            get => _status;
            private set => SetProperty(ref _status, value);
        }

        public bool IsJoined => 0 != (_application?.Pid ?? 0);

        public NodeViewScreenProxy(string id, int port = 0, int pid = 0)
        {
            Id = id;
            SetPid(pid);
            SetServicePort(port);
        }
        public NodeViewScreenProxy(ScreenApplication screenApplication, int port = 0)
        {
            if (screenApplication != null)
            {
                Id = screenApplication.Id;
                _application = screenApplication;
            }
            SetServicePort(port);
        }

        public bool Start()
        {
            if (Status != NodeViewScreenStatus.None)
            {
                return false;
            }

            if (_application == null)
            {
                _application = new ScreenApplication(Id);
            }

            bool res = _application?.Start() ?? false;
            if (res)
            {
                Status = NodeViewScreenStatus.Started;
            }
            else
            {
                Status = NodeViewScreenStatus.HasError;
            }
            Updated = CachedDateTime.Now;
            return res;
        }

        public void Stop()
        {
            bool res = _application?.Stop() ?? false;
            _application = null;
            Status = NodeViewScreenStatus.Stopped;
            Updated = CachedDateTime.Now;
        }

        public void SetServicePort(int servicePort)
        {
            if (servicePort > 0)
            {
                _servicePort = servicePort;
                _screenApiClient = new RestApiClient($"http://127.0.0.1:{_servicePort}/api/v1");
            }
        }

        public void SetPid(int pid)
        {
            if (pid > 0)
            {
                if (_application != null && _application.Pid == pid)
                {
                    return;
                }
                _application = new ScreenApplication(Id, pid);
            }
        }
        public NodeViewScreenStatus CheckStatus()
        {
            ProcessRunningStatus processStatus = _application?.UpdateStatus() ?? ProcessRunningStatus.Stopped;
            switch (processStatus)
            {
                case ProcessRunningStatus.None:
                case ProcessRunningStatus.Unknown:
                case ProcessRunningStatus.Stop:
                case ProcessRunningStatus.Stopped:
                    Status = NodeViewScreenStatus.Stopped;
                    break;
                case ProcessRunningStatus.Start:
                case ProcessRunningStatus.Running:
                    Status = NodeViewScreenStatus.Started;
                    break;
                case ProcessRunningStatus.CannotLoad:
                    Status = NodeViewScreenStatus.HasError;
                    break;
                default:
                    Status = NodeViewScreenStatus.Stopped;
                    break;
            }

            return Status;
        }

        private void CheckResponse(RestApiResponse response)
        {
            if (response == null)
            {
                return;
            }
            if (!response.IsSuccess)
            {
                CheckStatus();
            }
        }

        public void CreateWindows(DataWallWindow[] createWindows)
        {
            if (_servicePort <= 0 || createWindows == null || createWindows.Length == 0)
            {
                return;
            }
            JArray windowArray = new JArray();
            foreach (var window in createWindows)
            {
                windowArray.Add(window.ToJson());
            }

            CheckResponse(_screenApiClient.Post("/windows", windowArray));
        }

        public void CloseWindowsInRect(IntRect screenRect)
        {
            if (screenRect == null || screenRect.IsEmpty)
            {
                return;
            }

            JObject requestJson = new JObject
            {
                ["action"] = "closeInRect", 
                ["param"] = new JObject()
                {
                    ["rect"] = $"{screenRect}"
                }
            };

            CheckResponse(_screenApiClient.Put("/windows", requestJson));
        }

        public void CloseAllWindows()
        {
            JObject requestJson = new JObject
            {
                ["action"] = "closeAll"
            };

            CheckResponse(_screenApiClient.Put("/windows", requestJson));
        }

        public void CloseWindows(string[] deleteWindowIds)
        {
            if (deleteWindowIds == null || deleteWindowIds.Length == 0)
            {
                return;
            }

            JArray windowIds = new JArray();
            foreach (string windowId in deleteWindowIds)
            {
                windowIds.Add(windowId);
            }
            JObject requestJson = new JObject
            {
                ["action"] = "close",
                ["param"] = new JObject()
                {
                    ["ids"] = windowIds
                }
            };

            CheckResponse(_screenApiClient.Put("/windows", requestJson));
        }

        public void CreatePopups(DataWallWindow[] createPopups)
        {
            if (_servicePort <= 0 || createPopups == null || createPopups.Length == 0)
            {
                return;
            }
            JArray windowArray = new JArray();
            foreach (var window in createPopups)
            {
                windowArray.Add(window.ToJson());
            }

            CheckResponse(_screenApiClient.Post("/popups", windowArray));
        }

        public void ClosePopupsInRect(IntRect screenRect)
        {
            if (screenRect == null || screenRect.IsEmpty)
            {
                return;
            }

            JObject requestJson = new JObject
            {
                ["action"] = "closeInRect",
                ["param"] = new JObject()
                {
                    ["rect"] = $"{screenRect}"
                }
            };

            CheckResponse(_screenApiClient.Put("/popups", requestJson));
        }

        public void CloseAllPopups()
        {
            JObject requestJson = new JObject
            {
                ["action"] = "closeAll"
            };

            CheckResponse(_screenApiClient.Put("/popups", requestJson));
        }

        public void ClosePopups(string[] deleteWindowIds)
        {
            if (deleteWindowIds == null || deleteWindowIds.Length == 0)
            {
                return;
            }

            JArray windowIds = new JArray();
            foreach (string windowId in deleteWindowIds)
            {
                windowIds.Add(windowId);
            }
            JObject requestJson = new JObject
            {
                ["action"] = "close",
                ["param"] = new JObject()
                {
                    ["ids"] = windowIds
                }
            };

            CheckResponse(_screenApiClient.Put("/popups", requestJson));
        }

        public void MovePopup(string windowId, IntRect windowRect)
        {
            if (string.IsNullOrWhiteSpace(windowId) || windowRect.IsEmpty)
            {
                return;
            }

            JObject requestJson = new JObject
            {
                ["action"] = "move",
                ["param"] = new JObject()
                {
                    ["id"] = windowId,
                    ["windowRect"] = $"{windowRect}"
                }
            };

            CheckResponse(_screenApiClient.Put("/popups", requestJson));
        }
    }

    public enum NodeViewScreenStatus
    {
        None,
        HasError,
        Stopped,
        Started
    }
}
