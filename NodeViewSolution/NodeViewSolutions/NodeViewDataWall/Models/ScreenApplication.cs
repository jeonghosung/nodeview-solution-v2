﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NLog;
using NodeView.Systems;

namespace NodeViewDataWall.Models
{
    public class ScreenApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private const string Execution = "NodeViewScreen.exe";
        //private const string Name = "NodeViewScreen";

        private Process _currentProcess = null;

        public string Id { get; }
        public int Pid => _currentProcess?.Id ?? 0;
        public ProcessRunningStatus Status { get; protected set; } = ProcessRunningStatus.None;

        public ScreenApplication(string id, int pid = 0)
        {
            Id = id;
            if (pid > 0)
            {
                JoinProcess(pid);
            }
        }

        private void JoinProcess(int pid)
        {
            if (_currentProcess != null || pid <= 0)
            {
                return;
            }

            try
            {
                var process = Process.GetProcessById(pid);
                _currentProcess = process;
                UpdateStatus();
            }
            catch
            {
                //Skip
            }
        }

        public bool Start()
        {
            bool res = false;
            try
            {
                var status = UpdateStatus();
                if (status == ProcessRunningStatus.Stopped)
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Execution);
                    if (!File.Exists(path))
                    {
                        SetStatus(ProcessRunningStatus.CannotLoad);
                        return false;
                    }

                    int processId = 0;
                    if (Environment.UserInteractive)
                    {
                        var process = new Process
                        {
                            StartInfo = new ProcessStartInfo
                            {
                                FileName = path,
                                Arguments = $"{Id}",
                                UseShellExecute = false,
                                CreateNoWindow = true,
                                WorkingDirectory = AppDomain.CurrentDomain.BaseDirectory,
                            }
                        };

                        if (process.Start())
                        {
                            processId = process.Id;
                        }
                    }
                    else
                    {
                        //processId = WindowsServiceUtils.CreateProcessAsCurrentUser(path, $"{Id}", AppDomain.CurrentDomain.BaseDirectory ?? "");
                    }

                    Logger.Info($"CurrentProcess Id : {processId}");
                    if (processId != 0)
                    {
                        _currentProcess = Process.GetProcessById(processId);
                        UpdateStatus();
                    }
                }

                res = true;
            }
            catch (Exception e)
            {
                Logger.Error(e, $"Cannot start process for '{Id}':");
                res = false;
            }

            return res;
        }
        public bool Stop()
        {
            bool res = false;
            try
            {
                var status = UpdateStatus();
                if (status == ProcessRunningStatus.Running)
                {
                    _currentProcess?.Kill();
                    _currentProcess?.Dispose();
                    _currentProcess = null;
                }

                UpdateStatus();
                res = true;
            }
            catch
            {
                res = false;
            }

            return res;
        }

        public ProcessRunningStatus UpdateStatus()
        {
            ProcessRunningStatus status = ProcessRunningStatus.Unknown;
            try
            {
                Process process = GetCurrentProcess();
                if (process == null)
                {
                    string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, Execution);
                    status = File.Exists(path) ? ProcessRunningStatus.Stopped : ProcessRunningStatus.CannotLoad;
                }
                else
                {
                    status = process.HasExited ? ProcessRunningStatus.Stopped : ProcessRunningStatus.Running;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"UpdateStatus for '{Id}':");
                status = ProcessRunningStatus.Unknown;
            }

            SetStatus(status);
            return Status;
        }

        private Process GetCurrentProcess()
        {
            if (_currentProcess != null)
            {
                if (!_currentProcess.HasExited)
                {
                    return _currentProcess;
                }

                _currentProcess.Dispose();
                _currentProcess = null;
            }
            return null;
        }

        protected void SetStatus(ProcessRunningStatus newState)
        {
            if (Status != newState)
            {
                if (Status == ProcessRunningStatus.None)
                {
                    if (newState == ProcessRunningStatus.CannotLoad)
                    {
                        Logger.Error($"Cannot load '{Id}({Execution})'");
                    }
                    else
                    {
                        Logger.Info($"Process('{Id}') status is '{newState}'");
                    }
                }
                else
                {
                    Logger.Info($"Process('{Id}') status was changed from '{Status}' to '{newState}'");
                }

                Status = newState;
            }
        }

    }

    public enum ProcessRunningStatus
    {
        None,
        CannotLoad,
        Start,
        Running,
        Stop,
        Stopped,
        Unknown
    }
}
