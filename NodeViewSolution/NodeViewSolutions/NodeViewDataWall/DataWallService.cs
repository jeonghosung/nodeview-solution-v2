﻿using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Net;
using NodeView.NodeViews;
using NodeView.Protocols.NodeViews;
using NodeView.Systems;
using NodeView.Tasks;
using NodeView.Utils;
using NodeViewCore.DataWalls;
using NodeViewDataWall.Devices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;

namespace NodeViewDataWall
{
    public class DataWallService: StationServiceManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region SingleTone
        private static readonly Lazy<DataWallService> Lazy = new Lazy<DataWallService>(() => new DataWallService());
        public static DataWallService Instance => Lazy.Value;
        #endregion

        private IntRect _logicalScreenRect = new IntRect(0, 0, 1920, 1080);
        private readonly NodeViewScreenProxyManager _screenManager = new NodeViewScreenProxyManager();
        private NodeViewProcessManagerClient _processManagerClient;

        private bool _keepScreenOnStart = false;
        private Timer _checkingTimer = null;
        private volatile bool _usingAutoRefresh = false;
        private int _autoRefreshTime = 0; //Hour*100 + Min
        private int _lastAutoRefreshCheckedTime = (DateTime.Now.Hour * 100 + DateTime.Now.Minute); //Hour*100 + Min

        private readonly Dictionary<string, DataWallContent> _contentDictionary = new Dictionary<string, DataWallContent>();
        private readonly List<DataWallWindow> _screenWindowList = new List<DataWallWindow>();
        private readonly List<DataWallWindow> _popupWindowList = new List<DataWallWindow>();
        private readonly List<DataWallPreset> _presetList = new List<DataWallPreset>();
        private readonly List<DataWallMemo> _memoList = new List<DataWallMemo>();

        public readonly List<string> InitErrorMessages = new List<string>();
        public bool HasInitError => InitErrorMessages.Count > 0;

        public IntSize ScreenSize => _logicalScreenRect.Size;
        public IntPoint ScreenMatrix { get; set; } = new IntPoint(1, 1);
        public bool IsSupportMonitorControl { get; set; } = false;
        public string ConfigsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string ScreenUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string ContentsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string PresetsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string DevicesUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string ActionsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string EventActionsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);

        protected override bool OnInitializing(Configuration config)
        {
            AreaAlignment screenAlignment = config.GetValue("screen.alignment", AreaAlignment.LeftTop);
            IntPoint screenOffset = config.GetValue("screen.offset", new IntPoint(0, 0));
            IntSize screenSize = config.GetValue("screen.size", new IntSize(0, 0));
            ScreenMatrix = config.GetValue("screen.matrix", new IntPoint(1, 1));

            IntRect actualScreenRect = SystemScreenUtils.GetActualScreenRect(screenAlignment, screenOffset, screenSize);
            _logicalScreenRect = new IntRect(0, 0, actualScreenRect.Width, actualScreenRect.Height);

            int processManagerPort = Config.GetValue("processManager.port", 20102);
            _processManagerClient = new NodeViewProcessManagerClient("127.0.0.1", processManagerPort);

            _keepScreenOnStart = Config.GetValue("keepScreenOnStart", _keepScreenOnStart);

            _usingAutoRefresh = Config.GetValue("usingAutoRefresh", _usingAutoRefresh);
            string[] splitAutoRefreshTimeString = StringUtils.Split(Config.GetValue("autoRefreshTime", ""), ':', 2);
            _autoRefreshTime = StringUtils.GetIntValue(splitAutoRefreshTimeString[0]) * 100 + StringUtils.GetIntValue(splitAutoRefreshTimeString[1]);
            return true;
        }

        protected override void OnInitialized()
        {
            base.OnInitialized();
            ConfigsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            DevicesUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);

            string stopApiUrl = $"http://127.0.0.1:{ServicePort}/api/v1/service/shutdown";
            Task.Factory.StartNew(() =>
            {
                _processManagerClient.RegisterApp(ProcessType.Application, AppUtils.GetAppName(), ConfigFilename, stopApiUrl);
            });

            var now = DateTime.Now;
            _lastAutoRefreshCheckedTime = (now.Hour * 100 + now.Minute); //Hour*100 + Min
            _checkingTimer = new Timer(CheckingTimerCallback);
        }

        private void CheckingTimerCallback(object state)
        {
            if (_usingAutoRefresh)
            {
                var now = DateTime.Now;
                int nowTime = (now.Hour * 100 + now.Minute); //Hour*100 + Min
                if (_lastAutoRefreshCheckedTime == nowTime)
                {
                    // Skip
                }
                else if (_lastAutoRefreshCheckedTime < nowTime)
                {
                    if (_autoRefreshTime > _lastAutoRefreshCheckedTime && _autoRefreshTime <= nowTime)
                    {
                        Logger.Info("Run Auto Refresh.");
                        ThreadAction.PostOnUiThread(Refresh);
                    }
                } 
                else  // 00시 기점
                {
                    if (_autoRefreshTime > nowTime && _autoRefreshTime <= _lastAutoRefreshCheckedTime)
                    {
                        // Skip
                    }
                    else
                    {
                        Logger.Info("Run Auto Refresh.");
                        ThreadAction.PostOnUiThread(Refresh);
                    }
                }

                _lastAutoRefreshCheckedTime = nowTime;
            }
        }

        protected override void InitDefaultPlugins()
        {
            base.InitDefaultPlugins();

            var dataWallController = new NodeViewDataWallController("dataWall");
            dataWallController.Init(null, this);
            dataWallController.IsTestMode = IsTestMode;
            dataWallController.IsVerbose = IsVerbose;
            _pluginManager.AddPlugin(dataWallController);
        }

        protected override bool InitPlugins(Configuration config)
        {
            if (!base.InitPlugins(config))
            {
                return false;
            }
            if (config.TryGetConfig("monitor", out var monitorControllerConfig))
            {
                if (monitorControllerConfig.GetValue("isEnabled", true))
                {
                    if (_pluginManager?.LoadPlugin(monitorControllerConfig) ?? false)
                    {
                        IsSupportMonitorControl = true;
                    }
                    else
                    {
                        Logger.Error($"Cannot load monitor controller.");
                    }
                }
            }
            if (config.TryGetConfig("cameraContol", out var cameraContolerConfig))
            {
                if (cameraContolerConfig.GetValue("isEnabled", true))
                {
                    if (_pluginManager?.LoadPlugin(cameraContolerConfig) ?? false)
                    {
                    }
                    else
                    {
                        Logger.Error($"Cannot load monitor controller.");
                    }
                }
            }

            return true;
        }

        
        //Todo: Get/SaveConfiguration은 프로세스메니져로 이전 해야 한다.
        public Configuration GetConfiguration()
        {
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewDataWall.config.xml");
            return Configuration.Load(settingFile);
        }
        public bool SaveConfiguration(Configuration configuration)
        {
            if (configuration == null) return false;

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewDataWall.config.xml");
            File.Copy(settingFile, settingFile+".backup", true);
            bool res = Configuration.Save(settingFile, configuration);
            ConfigsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            return res;
        }

        protected override bool OnStart()
        {
            if (!base.OnStart())
            {
                return false;
            }

            LoadContents();
            LoadPresets();
            LoadScreenCache();
            LoadMemos();

            _screenManager.IsTestMode = IsTestMode;
            if (!_screenManager.Start())
            {

                string errorMsg = $"Cannot create a screen application.";
                Logger.Error(errorMsg);
                InitErrorMessages.Add(errorMsg);
                return false;
            }

            OnScreenUpdated(false);
            _checkingTimer.Change(TimeSpan.FromSeconds(80), TimeSpan.FromSeconds(80));
            return true;
        }

        private void OnScreenUpdated(bool requestSave = true)
        {
            ScreenUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            if (requestSave)
            {
                SaveScreenCache();
            }
        }

        private void LoadScreenCache()
        {
            string filename = Path.Combine(CachesFolder, "screen.xml");
            if (!File.Exists(filename))
            {
                return;
            }
            if (!_keepScreenOnStart)
            {
                try
                {
                    File.Delete(filename);
                }
                catch
                {
                    //skip
                }
                return;
            }
            try
            {
                _screenWindowList.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(filename);
                XmlNodeList windowNodeList = xmlDocument.SelectNodes("/Screen/Window");
                if (windowNodeList != null)
                {
                    foreach (XmlNode contentNode in windowNodeList)
                    {
                        DataWallWindow window = DataWallWindow.CreateFrom(contentNode);
                        if (window != null)
                        {
                            _screenWindowList.Add(window);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadScreenCache:");
            }
        }
        private void SaveScreenCache()
        {
            string fileName = Path.Combine(CachesFolder, "screen.xml");
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(fileName))
                {
                    xmlWriter.WriteStartElement("Screen");
                    foreach (var window in _screenWindowList.ToArray())
                    {
                        window.WriteXml(xmlWriter);
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveScreenCache:");
            }
        }

        protected override void OnStop()
        {
            _checkingTimer.Change(Timeout.Infinite, Timeout.Infinite);
            base.OnStop();
            _screenManager?.Stop();
            _contentDictionary.Clear();
            _screenWindowList.Clear();
            _popupWindowList.Clear();
            _presetList.Clear();
            InitErrorMessages.Clear();

            OnScreenUpdated(false);
        } 
             
        private void LoadContents()
        {
            string contentsFile = Path.Combine(ConfigsFolder, "contents.xml");
            if (!File.Exists(contentsFile))
            {
                Logger.Error($"Cannot find contents file[{contentsFile}].");
                return;
            }

            try
            {
                _contentDictionary.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(contentsFile);
                XmlNodeList contentNodeList = xmlDocument.SelectNodes("/Contents/Content");
                if (contentNodeList != null)
                {
                    foreach (XmlNode contentNode in contentNodeList)
                    {
                        DataWallContent nodeViewContent = DataWallContent.CreateFrom(contentNode);
                        if (nodeViewContent != null)
                        {
                            _contentDictionary[nodeViewContent.Id] = nodeViewContent;
                        }
                    }
                }

                //Todo: 임시로 컨텐츠만  노드로
                DeleteAllNodes("datawall/content");
                List<INode> nodeList = new List<INode>();
                foreach (var content in _contentDictionary.Values)
                {
                    nodeList.Add(Node.CreateFrom(content));
                }
                CreateNodes("datawall/content", nodeList.ToArray(), true);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load contents file.");
            }
            ContentsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        private void SaveContents()
        {
            string contentsFile = Path.Combine(ConfigsFolder, "contents.xml");
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(contentsFile))
                {
                    xmlWriter.WriteStartElement("Contents");
                    foreach (var content in _contentDictionary.Values.ToArray())
                    {
                        content.WriteXml(xmlWriter, "Content");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveContents:");
            }
            ContentsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        private void SaveMemos()
        {
            string memosFile = Path.Combine(CachesFolder, "Memos.xml");
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(memosFile))
                {
                    xmlWriter.WriteStartElement("Memos");
                    foreach (var memo in _memoList)
                    {
                        memo.WriteXml(xmlWriter, "Memo");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveContents:");
            }
        }
        public void LoadMemos()
        {
            string memosFile = Path.Combine(CachesFolder, "Memos.xml");
            if (!File.Exists(memosFile))
            {
                return;
            }
            try
            {
                _memoList.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(memosFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Memos/Memo");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        DataWallMemo memo = DataWallMemo.CreateFrom(node);
                        if (memo != null)
                        {
                            _memoList.Add(memo);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load presets file.");
            }
        }
        private void LoadPresets()
        {
            string presetsFile = Path.Combine(ConfigsFolder, "presets.xml");
            if (!File.Exists(presetsFile))
            {
                //Logger.Error($"Cannot find presets file[{presetsFile}].");
                return;
            }

            try
            {
                _presetList.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(presetsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Presets/Preset");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        DataWallPreset preset = DataWallPreset.CreateFrom(node);
                        if (preset != null)
                        {
                            _presetList.Add(preset);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load presets file.");
            }
            PresetsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        private void SavePresets()
        {
            string presetsFile = Path.Combine(ConfigsFolder, "presets.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(presetsFile))
                {
                    xmlWriter.WriteStartElement("Presets");
                    foreach (var preset in GetPresets())
                    {
                        preset.WriteXml(xmlWriter, "Preset");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SavePresets:");
            }
            PresetsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }

        public DataWallMemo GetMemo(string id)
        {
            if (_memoList == null || _memoList.Count == 0)
            {
                return null;
            }

            if (_memoList.Any(memo => memo.Id.Equals(id, StringComparison.OrdinalIgnoreCase)))
            {
                return _memoList.Find(memo => memo.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
            }

            return null;
        }
        public DataWallContent[] GetContents()
        {
            return _contentDictionary.Values.ToArray();
        }

        public DataWallWindow[] GetWindows()
        {
            return _screenWindowList.ToArray();
        }

        public DataWallWindow[] RequestCreateWindows(string requester, DataWallWindow[] createWindows)
        {
            return CreateWindows(createWindows);
        }

        public DataWallWindow[] CreateWindows(DataWallWindow[] createWindows)
        {
            if (createWindows == null || createWindows.Length == 0)
            {
                return GetPopups();
            }

            string[] deleteIds = createWindows.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            List<DataWallWindow> createWindowList = new List<DataWallWindow>();
            foreach (var createWindow in createWindows)
            {
                createWindow.IsPopup = false;
                UpdateWindowContentByContentId(createWindow);
                if (createWindow.Content == null)
                {
                    continue;
                }

                createWindowList.Add(createWindow);
                CloseWindowsInRectInternal(createWindow.WindowRect);
                _screenWindowList.Add(createWindow);
            }

            _screenManager?.CreateWindows(createWindowList.ToArray());
            OnScreenUpdated();
            return GetWindows();
        }

        public void RequestCloseWindowsInRect(string requester, IntRect rect)
        {
            CloseWindowsInRect(rect);
        }

        public void CloseWindowsInRect(IntRect rect)
        {
            if (rect == null || rect.IsEmpty)
            {
                return;
            }
            CloseWindowsInRectInternal(rect);
            _screenManager?.CloseWindowsInRect(rect);
        }

        public int CloseWindowsInRectInternal(IntRect rect)
        {
            int deletedWindowCount = 0;
            if (rect == null || rect.IsEmpty)
            {
                return deletedWindowCount;
            }
            IntRect checkingRect = new IntRect(rect);
            var windows = _screenWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(checkingRect))
                {
                    deletedWindowCount++;
                    _screenWindowList.Remove(window);
                }
            }

            if (deletedWindowCount > 0)
            {
                OnScreenUpdated();
            }

            return deletedWindowCount;
        }

        public void CloseWindows(string[] closeWindowIds)
        {
            if (closeWindowIds == null || closeWindowIds.Length == 0)
            {
                return;
            }

            CloseWindowsInternal(closeWindowIds);

            _screenManager?.CloseWindows(closeWindowIds);
        }
        public int CloseWindowsInternal(string[] closeWindowIds)
        {
            int deletedWindowCount = 0;
            if (closeWindowIds == null || closeWindowIds.Length == 0)
            {
                return deletedWindowCount;
            }
            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string closeWindowId in closeWindowIds)
            {
                idDictionary[closeWindowId] = closeWindowId;
            }
            DataWallWindow[] windows = _screenWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    _screenWindowList.Remove(window);
                    deletedWindowCount++;
                }
            }
            if (deletedWindowCount > 0)
            {
                OnScreenUpdated();
            }
            return deletedWindowCount;
        }
        public void RequestCloseAllWindows(string requester)
        {
            CloseAllWindows();
        }

        public void CloseAllWindows()
        {
            if (_screenWindowList.Count == 0)
            {
                return;
            }
            _screenWindowList.Clear();

            _screenManager?.CloseAllWindows();
            OnScreenUpdated();
        }

        public void RequestCloseWindows(string requester, string[] closeWindowIds)
        {
            CloseWindows(closeWindowIds);
        }

        public DataWallWindow[] GetPopups()
        {
            return _popupWindowList.ToArray();
        }

        public DataWallWindow[] RequestCreatePopups(string requester, DataWallWindow[] createPopups)
        {
            return CreatePopups(createPopups);
        }

        public DataWallWindow[] CreatePopups(DataWallWindow[] createPopups)
        {
            if (createPopups == null || createPopups.Length == 0)
            {
                return GetPopups();
            }

            string[] deleteIds = createPopups.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            List<DataWallWindow> createWindowList = new List<DataWallWindow>();
            foreach (var createPopup in createPopups)
            {
                createPopup.IsPopup = true;
                UpdateWindowContentByContentId(createPopup);
                if (createPopup.Content == null)
                {
                    continue;
                }
                createWindowList.Add(createPopup);
                _popupWindowList.Add(createPopup);
            }

            _screenManager?.CreatePopups(createWindowList.ToArray());
            OnScreenUpdated();
            return GetPopups();
        }

        public void RequestClosePopupsInRect(string requester, IntRect rect)
        {
            ClosePopupsInRect(rect);
        }

        public void ClosePopupsInRect(IntRect rect)
        {
            if (rect == null || rect.IsEmpty)
            {
                return;
            }

            IntRect checkingRect = new IntRect(rect);
            int deletedWindowCount = 0;
            var windows = _popupWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(checkingRect))
                {
                    deletedWindowCount++;
                    _popupWindowList.Remove(window);
                }
            }

            if (deletedWindowCount > 0)
            {
                OnScreenUpdated();
            }
            _screenManager?.ClosePopupsInRect(rect);
        }
        public void RequestClosePopups(string requester, string[] deletePopupIds)
        {
            ClosePopups(deletePopupIds);
        }
        public void ClosePopups(string[] deletePopupIds)
        {
            if (deletePopupIds == null || deletePopupIds.Length == 0)
            {
                return;
            }

            ClosePopupsInternal(deletePopupIds);
            _screenManager?.ClosePopups(deletePopupIds);
        }

        public int ClosePopupsInternal(string[] deletePopupIds)
        {
            int deletedWindowCount = 0;
            if (deletePopupIds == null || deletePopupIds.Length == 0)
            {
                return deletedWindowCount;
            }
            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string deletePopupId in deletePopupIds)
            {
                idDictionary[deletePopupId] = deletePopupId;
            }
            DataWallWindow[] windows = _popupWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    deletedWindowCount++;
                    _popupWindowList.Remove(window);
                }
            }

            if (deletedWindowCount > 0)
            {
                OnScreenUpdated();
            }
            return deletedWindowCount;
        }

        public void RequestCloseAllPopups(string requester)
        {
            CloseAllPopups();
        }

        public void CloseAllPopups()
        {
            if (_popupWindowList.Count == 0)
            {
                return;
            }
            _popupWindowList.Clear();
            _screenManager?.CloseAllPopups();
            OnScreenUpdated();
        }

        public void RequestMovePopup(string requester, string windowId, IntRect windowRect)
        {
            MovePopup(windowId, windowRect);
        }

        public void MovePopup(string windowId, IntRect windowRect)
        {
            if (string.IsNullOrWhiteSpace(windowId) || windowRect.IsEmpty)
            {
                return;
            }

            DataWallWindow[] windows = _popupWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.Id == windowId)
                {
                    window.WindowRect = windowRect;
                    _screenManager?.MovePopup(windowId, windowRect);
                    OnScreenUpdated();
                    break;
                }
            }
        }

        private void UpdateWindowContentByContentId(DataWallWindow window)
        {
            if (!string.IsNullOrWhiteSpace(window.ContentId))
            {
                if (_contentDictionary.TryGetValue(window.ContentId, out var content))
                {
                    window.SetContent(content);
                }
            }
        }

        public DataWallPreset[] GetPresets()
        {
            return _presetList.ToArray();
        }

        public bool RequestCreatePreset(string requester, string id, string name)
        {
            return CreatePreset(id, name);
        }

        public bool CreatePreset(string id, string name)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var currentPresets = GetPresets();
                foreach (var currentPreset in currentPresets)
                {
                    if (currentPreset.Id == id)
                    {
                        Logger.Warn($"alreay exist preset id as '{id}'");
                        return false;
                    }
                }
            }
            DataWallPreset preset = new DataWallPreset(id, name, GetWindows());
            _presetList.Add(preset);

            SavePresets();

            return true;
        }

        public bool RequestRenamePreset(string requester, string id, string name)
        {
            return RenamePreset(id, name);
        }

        public bool RenamePreset(string id, string name)
        {
            bool res = false;
            var presets = GetPresets();
            foreach (var preset in presets)
            {
                if (preset.Id == id)
                {
                    preset.Name = name;
                    res = true;
                }
            }

            if (res)
            {
                SavePresets();
            }

            return res;
        }

        public void RequestDeleteAllPresets(string requester)
        {
            DeleteAllPresets();
        }

        public void DeleteAllPresets()
        {
            if (_presetList.Count > 0)
            {
                _presetList.Clear();
                SavePresets();
            }
        }

        public void RequestDeletePresets(string requester, string[] deleteIds)
        {
            DeletePresets(deleteIds);
        }

        public void DeletePresets(string[] deleteIds)
        {
            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string deleteId in deleteIds)
            {
                idDictionary[deleteId] = deleteId;
            }

            int deletedCount = 0;
            DataWallPreset[] presets = GetPresets();
            foreach (var preset in presets)
            {
                if (idDictionary.TryGetValue(preset.Id, out var outValue))
                {
                    _presetList.Remove(preset);
                    deletedCount++;
                }
            }

            if (deletedCount > 0)
            {
                SavePresets();
            }
        }

        public bool  RequestApplyPreset(string requester, string presetId)
        {
            return ApplyPreset(presetId);
        }

        public bool ApplyPreset(string presetId)
        {
            bool res = false;

            List<DataWallWindow> windowList = new List<DataWallWindow>();
            var presets = GetPresets();
            foreach (var preset in presets)
            {
                if (preset.Id == presetId)
                {
                    foreach (var presetWindow in preset.Windows)
                    {
                        windowList.Add(new DataWallWindow(presetWindow));
                    }
                    res = true;
                }
            }

            if (res)
            {
                CloseAllPopups();
                CloseAllWindows();
                CreateWindows(windowList.ToArray());
            }
            return res;
        }

        public bool JoinNodeViewScreen(string id, int port, int pid)
        {
            return _screenManager?.JoinScreen(id, port, pid) ?? false;
        }

        public void Refresh()
        {
            try
            {
                _screenManager.Stop();
                Thread.Sleep(500);
                _screenManager.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Refresh:");
            }
        }

        public void Shutdown()
        {
            Stop();
            /*
            var containerProvider = NodeViewContainerExtension.GetContainerProvider();
            if (containerProvider != null)
            {
                try
                {
                    var mainWindow = (Window)containerProvider.Resolve(typeof(Window), "mainWindow");
                    if (mainWindow != null)
                    {
                        mainWindow.Close();
                    }
                }
                catch
                {
                    //skip
                }
            }
            */
            Application.Current.Shutdown();
        }

        public ApiResponse RequestUpdateMemo(string requester, DataWallMemo dataWallMemo)
        {
            if (UpdateMemo(dataWallMemo))
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }

        private bool UpdateMemo(DataWallMemo dataWallMemo)
        {
            if (_memoList.Any(memo => memo.Id.Equals(dataWallMemo.Id, StringComparison.OrdinalIgnoreCase)))
            {
                var memo = _memoList.Find(memo => memo.Id.Equals(dataWallMemo.Id, StringComparison.OrdinalIgnoreCase));
                memo.Memo = dataWallMemo.Memo;
            }
            else
            {
                _memoList.Add(dataWallMemo);
            }

            SaveMemos();
            return true;
        }
        public ApiResponse RequestDeleteAllContents(string requester)
        {
            if (DeleteAllContents())
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }

        public ApiResponse RequestDeleteContents(string requester, string[] ids)
        {
            if (DeleteContents(ids))
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }

        public ApiResponse RequestUpdateContent(string requester, DataWallContent updateContent)
        {
            if (UpdateContent(updateContent))
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }
        public ApiResponse RequestCreateContents(string requester, DataWallContent[] contents)
        {
            if (CreateContents(contents))
            {
                return ApiResponse.Success;
            }
            return ApiResponse.BadRequest;
        }
        private bool DeleteAllContents()
        {
            _contentDictionary.Clear();
            SaveContents();
            return true;
        }

        private bool DeleteContents(string[] ids)
        {
            int updateCount = 0;
            foreach (var id in ids)
            {
                if (_contentDictionary.TryGetValue(id, out var content))
                {
                    updateCount++;
                    _contentDictionary.Remove(id);
                }
            }

            if (updateCount > 0)
            {
                SaveContents();
            }
            return true;
        }

        private bool UpdateContent(DataWallContent updateContent)
        {
            if (_contentDictionary.TryGetValue(updateContent.Id, out var content))
            {
                _contentDictionary[updateContent.Id] = updateContent;
                SaveContents();
                return true;
            }

            return false;
        }
        private bool CreateContents(DataWallContent[] contents)
        {
            int updateCount = 0;
            foreach (var content in contents)
            {
                if (string.IsNullOrWhiteSpace(content?.Id))
                {
                    continue;
                }

                _contentDictionary[content.Id] = content;
                updateCount++;
            }
            if (updateCount > 0)
            {
                SaveContents();
            }
            return true;
        }

        public void RequestShutdown(string requester)
        {
            ThreadAction.PostOnUiThread(Shutdown);
        }
    }
}
