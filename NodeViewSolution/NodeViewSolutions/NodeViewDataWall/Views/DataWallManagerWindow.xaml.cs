﻿using System;
using System.Drawing;
using System.Windows;
using System.Windows.Forms;

namespace NodeViewDataWall.Views
{
    /// <summary>
    /// DataWallManagerWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataWallManagerWindow : Window
    {
        private NotifyIcon _notify = new NotifyIcon();
        public DataWallManagerWindow()
        {
            InitializeComponent();

            DataWallService dataWallService = DataWallService.Instance;
            
            var menu = new ContextMenuStrip();
            ToolStripMenuItem item;
            ToolStripSeparator sep;

            item = new ToolStripMenuItem
            {
                Text = "Refresh",
            };
            item.Click += (_, _) => dataWallService.Refresh();
            menu.Items.Add(item);
            
            item = new ToolStripMenuItem
            {
                Text = "Exit"
            };
            item.Click += (_, _) =>
            {
                _notify.Visible = false;
                dataWallService.Shutdown();
            };
            menu.Items.Add(item);
            
            sep = new ToolStripSeparator();
            menu.Items.Add(sep);
            menu.Items.Add(item);

            _notify.ContextMenuStrip = menu;
            _notify.Icon = Properties.Resources.nv_wall_logo;
            _notify.Visible = true;
            _notify.Text = "NodeView DataWall";
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            _notify.Visible = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            WindowState = WindowState.Minimized;
            Hide();
        }
    }
}
