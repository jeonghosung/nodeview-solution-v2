﻿using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.Dialogs;
using NodeView.DialogWindows;
using NodeView.Extension.Stations;
using NodeView.Frameworks.Modularity;
using NodeView.Frameworks.Stations;
using NodeView.Ioc;
using NodeView.NodeViews;
using NodeView.Widgets;
using NodeViewManager.Dialogs;
using NodeViewManager.Views;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NodeView.DataWallContentControls;
using NodeView.Frameworks.Widgets;
using NodeViewScreenFramework.DataWallContentControls;
using Prism.DryIoc;
using NodeViewWindowCore.Dialogs;

namespace NodeViewManager
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : PrismApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private ManagerService _service;
        private readonly ModuleManager _nodeViewModuleManager = new ModuleManager();
        private readonly List<string> _criticalErrorMessages = new List<string>();
        private bool HasCriticalError => _criticalErrorMessages.Count > 0;
        private Window _mainWindow = null;

        protected override void OnStartup(StartupEventArgs e)
        {
            string appName = ResourceAssembly.GetName().Name;
            if (AppUtils.IsDuplicateExecution())
            {
                MessageBox.Show($"'{appName}' 프로그램이 이미 실행 중입니다.", "알림", MessageBoxButton.OK,
                    MessageBoxImage.Information);
                Current.Shutdown();

            }
            else
            {
                SetupUnhandledExceptionHandling();
                base.OnStartup(e);

            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            _service = ManagerService.Instance;
            string configFilename = "NodeViewManager.config.xml";

            //StationService
            if (containerRegistry is IContainerExtension containerExtension)
            {
                NodeViewContainerExtension.CreateInstance(containerExtension);
            }
            containerRegistry.RegisterSingleton(typeof(IStationRepository), typeof(SqliteStationRepository));
            containerRegistry.RegisterInstance<StationServiceManager>(_service);

            IEventAggregator eventAggregator = new EventAggregator();
            containerRegistry.RegisterInstance(typeof(IEventAggregator), eventAggregator);
            containerRegistry.RegisterInstance(typeof(ModuleManager), _nodeViewModuleManager);

            //ModuleManager
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var configuration = Configuration.Load(settingFile);
            containerRegistry.RegisterInstance(typeof(Configuration), configuration, "appConfig");

            _nodeViewModuleManager.AddAssembly("NodeView.Extension.dll");
            if (configuration.TryGetNode("extensions", out var extensionsSection))
            {
                foreach (var extenstionProperty in extensionsSection.Properties)
                {
                    if (!_nodeViewModuleManager.AddAssembly(extenstionProperty.Value))
                    {
                        Logger.Error($"{extenstionProperty.Value}를 로드하는중에 오류가 발생했습니다.");
                    }
                }
            }
            _nodeViewModuleManager.RegisterTypes(containerRegistry);

            //Dialogs NodeViewWidgets
            containerRegistry.Register(typeof(IWidgetControl), typeof(DummyWidget), "dummy");
            containerRegistry.Register(typeof(IWidgetControl), typeof(DataWallControlWidget), "dataWallControl");
            containerRegistry.Register(typeof(IWidgetControl), typeof(DataWallAdminWidget), "dataWallSettings");
            containerRegistry.Register(typeof(IWidgetControl), typeof(NodeViewCenterWidget), "centerSettings");
            containerRegistry.Register(typeof(IWidgetControl), typeof(IotStationAdminWidget), "iotStationSettings");
            containerRegistry.Register(typeof(IWidgetControl), typeof(EventLogWidget), "eventLog");
            //Todo: 실제 플러그인이 가능 하도록 수정 해야 한다. 임시 구현
            //containerRegistry.Register(typeof(IWidgetControl), typeof(PowerControlWidget), "powerControl");

            //Dialogs
            containerRegistry.RegisterDialogWindow<BasicDialogWindow>();
            containerRegistry.RegisterDialogWindow<RoundIconPopupDialogWindow>("RoundIconPopup");
            containerRegistry.RegisterDialog<ConfirmMessageDialog, ConfirmMessageDialogModel>("confirm");
            containerRegistry.RegisterDialog<WarningConfirmMessageDialog, WarningConfirmMessageDialogModel>("warningConfirm");
            containerRegistry.RegisterDialog<ErrorConfirmMessageDialog, ErrorConfirmMessageDialogModel>("errorConfirm");
            containerRegistry.RegisterDialog<MessageDialog, MessageDialogViewModel>();

            containerRegistry.RegisterDialog<CellSplitOptionDialog, CellSplitOptionDialogModel>();
            containerRegistry.RegisterDialog<WidgetSelectionDialog, WidgetSelectionDialogModel>();
            containerRegistry.RegisterDialog<DataWallAdminDialog, DataWallAdminDialogModel>();
            containerRegistry.RegisterDialog<LoginDialog, LoginDialogModel>();
            containerRegistry.RegisterDialog<LocalLoginDialog, LocalLoginDialogModel>();
            containerRegistry.RegisterDialog<AddPropertyDialog, AddPropertyDialogViewModel>();
			containerRegistry.RegisterDialog<NodeViewAppEditDialog, NodeViewAppEditDialogModel>();
            containerRegistry.RegisterDialog<DeviceCommandRunDialog, DeviceCommandRunDialogModel>();
            containerRegistry.RegisterDialog<AppSelectionDialog, AppSelectionDialogModel>();
            containerRegistry.RegisterDialog<EditCenterActionDialog, EditCenterActionDialogModel>();
            containerRegistry.RegisterDialog<EditCenterActionCommandDialog, EditCenterActionCommandDialogModel>();
            containerRegistry.RegisterDialog<EditEventActionDialog, EditEventActionDialogModel>();
            containerRegistry.RegisterDialog<EditAppActionDialog, EditAppActionDialogModel>();
            containerRegistry.RegisterDialog<EditAppActionCommandDialog, EditAppActionCommandDialogModel>();
            containerRegistry.RegisterDialog<MonitorControlDialog, MonitorControlDialogModel>();
            containerRegistry.RegisterDialog<ChangePasswordDialog, ChangePasswordDialogModel>();
            containerRegistry.RegisterDialog<LocalChangePasswordDialog, LocalChangePasswordDialogModel>();
            containerRegistry.RegisterDialog<AddPresetDialog, AddPresetDialogModel>();
            containerRegistry.RegisterDialog<GetPopupRectDialog, GetPopupRectDialogModel>();
            containerRegistry.RegisterDialog<EventLogSearchDialog, EventLogSearchDialogModel>();
            containerRegistry.RegisterDialog<AddPtzPresetDialog, AddPtzPresetDialogModel>();

            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "media");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "camera");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "ptz-camera");
            containerRegistry.RegisterDataWallContentControl(typeof(WebDataWallContentControl), "web");
            containerRegistry.RegisterDataWallContentControl(typeof(RemoteDataWallContentControl), "remote");
            containerRegistry.RegisterDataWallContentControl(typeof(RemoteDataWallContentViewerControl), "remoteviewer");
            containerRegistry.RegisterDataWallContentControl(typeof(ImageContentControl), "image");
            _service.Initialize(Container, configFilename);
        }

        protected override Window CreateShell()
        {
            if (HasCriticalError)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("설정에 오류가 발생하여 프로그램을 시작 할 수없습니다.");
                sbMessage.AppendLine("상세오류===============================");
                foreach (var errorMessage in _criticalErrorMessages)
                {
                    sbMessage.AppendLine(errorMessage);
                }

                MessageBox.Show(sbMessage.ToString(), "설정 오류", MessageBoxButton.OK, MessageBoxImage.Error);
                Current.Shutdown();
                return null;
            }

            _service.Start();
            if (_service.HasInitError)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("프로그램 시작중 아래의 경고가 발생했습니다.");
                sbMessage.AppendLine("상세경고===============================");
                foreach (var errorMessage in _service.InitErrorMessages)
                {
                    sbMessage.AppendLine(errorMessage);
                }

                MessageBox.Show(sbMessage.ToString(), "프로그램 시작", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            _mainWindow = Container.Resolve<ManagerWindow>();
            _mainWindow.Closed += delegate(object sender, EventArgs args) { AppUtils.Exit(); };
            //_mainWindow.DataContext = Container.Resolve<DataWallManagerWindowViewModel>();
            _nodeViewModuleManager.OnCreatedShell(Container, _mainWindow);
            return _mainWindow;
        }

        protected override void OnInitialized()
        {
            if (HasCriticalError)
            {
                return;
            }

            base.OnInitialized();
            _nodeViewModuleManager.OnInitialized(Container);

            IRegionManager regionManager = Container.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("WidgetSelector", typeof(WidgetSelectorOnTopControl));

            if (_mainWindow != null)
            {
                _mainWindow.Closed += (sender, args) => { Application.Current.Shutdown(); };
            }
        }
        protected override void OnExit(ExitEventArgs e)
        {
            _service?.Stop();
            base.OnExit(e);
        }

        private void SetupUnhandledExceptionHandling()
        {
            // Catch exceptions from all threads in the AppDomain.
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException",
                    false);

            // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
            TaskScheduler.UnobservedTaskException += (sender, args) =>
                ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", false);

            // Catch exceptions from a single specific UI dispatcher thread.
            Dispatcher.UnhandledException += (sender, args) =>
            {
                // If we are debugging, let Visual Studio handle the exception and take us to the code that threw it.
                if (!Debugger.IsAttached)
                {
                    args.Handled = true;
                    ShowUnhandledException(args.Exception, "Dispatcher.UnhandledException", true);
                }
            };
        }

        void ShowUnhandledException(Exception e, string unhandledExceptionType, bool promptUserForShutdown)
        {
            Logger.Error(e, $"ShowUnhandledException ['{unhandledExceptionType}',{promptUserForShutdown}]");
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{e}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
