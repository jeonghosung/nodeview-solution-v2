﻿using System.Windows.Input;
using NLog;
using NodeView.Dialogs;
using NodeViewManager.Auth;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeViewManager.Dialogs
{
    public class LocalLoginDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private NodeViewManagerAuthentication _nodeViewManagerAuthentication = null;
        private DelegateCommand<KeyEventArgs> _keyUpDelegateCommand;

        private string _password;
        private string _errorMessage;

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }
       
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public DelegateCommand<KeyEventArgs> KeyUpEventCommand => _keyUpDelegateCommand ?? (_keyUpDelegateCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyUpEvent));

        protected override void ExecuteApplyDialogCommand()
        {
            if (!LoginProcess())
            {
                return;
            }
            RaiseRequestClose(new DialogResult(ButtonResult.OK));
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            _nodeViewManagerAuthentication = new NodeViewManagerAuthentication();
            Title = "로그인";
        }
        private void ExecuteKeyUpEvent(KeyEventArgs args)
        {
            switch (args.Key)
            {
                case Key.Enter:
                    if (LoginProcess())
                    {
                        RaiseRequestClose(new DialogResult(ButtonResult.OK));
                    }
                    break;

            }
        }

        private bool LoginProcess()
        {
            if (string.IsNullOrWhiteSpace(Password))
            {
                ErrorMessage = "비밀번호를 입력해주세요.";
                return false;
            }
            if (!_nodeViewManagerAuthentication.Login(Password))
            {
                ErrorMessage = "로그인에 실패했습니다.";
                Password = string.Empty; 
                return false;
            }

            return true;
        }
    }
}
