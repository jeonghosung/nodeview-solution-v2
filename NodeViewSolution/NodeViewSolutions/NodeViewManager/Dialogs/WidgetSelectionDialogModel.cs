﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Dialogs;
using NodeView.Frameworks.Widgets;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeViewManager.Dialogs
{
    public class WidgetSelectionDialogModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public ObservableCollection<WidgetSource> WidgetSources { get; set; } = new ObservableCollection<WidgetSource>();
        public DelegateCommand<string> SelectWidgetSourceCommand { get; set; }
        public WidgetSelectionDialogModel()
        {
            SelectWidgetSourceCommand = new DelegateCommand<string>(ExecuteSelectWidgetSourceCommand);
        }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "NodeView App 선택";
            if (parameters.TryGetValue("widgetSources", out IEnumerable<WidgetSource> widgetSources))
            {
                WidgetSources.AddRange(widgetSources);
            }
        }
        private void ExecuteSelectWidgetSourceCommand(string id)
        {
            foreach (var widgetSource in WidgetSources.ToArray())
            {
                if (widgetSource.Id == id)
                {
                    var parameters = new DialogParameters();
                    parameters.Add("widgetSource", widgetSource);
                    RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
                }
            }
        }
    }
}
