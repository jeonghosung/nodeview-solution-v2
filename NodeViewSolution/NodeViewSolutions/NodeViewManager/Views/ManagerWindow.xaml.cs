﻿using System;
using System.Windows;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Frameworks.Widgets;
using NodeView.Ioc;
using Prism.Services.Dialogs;

namespace NodeViewManager.Views
{
    /// <summary>
    /// ManagerWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ManagerWindow : Window
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IDialogService _dialogService = null;

        public ManagerWindow()
        {
            InitializeComponent();
        }
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            try
            {
                _dialogService = (IDialogService)NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(IDialogService));
            }
            catch(Exception ex)
            {
                Logger.Info(ex, "OnInitialized error.");
            }

            if (_dialogService == null)
            {
                Logger.Warn("DialogService is null.");
                return;
            }
            _dialogService.ShowDialog("LocalLoginDialog", null, loginResultCallback =>
            {
                if (loginResultCallback.Result != ButtonResult.OK)
                {
                    Application.Current.Shutdown();
                }
            });
        }
    }
}
