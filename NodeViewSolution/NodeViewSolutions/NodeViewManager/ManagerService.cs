﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.Drawing;
using NodeView.NodeViews;
using NodeView.Protocols.NodeViews;
using NodeView.Systems;

namespace NodeViewManager
{
    public class ManagerService : StationServiceManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region SingleTone
        private static readonly Lazy<ManagerService> Lazy = new Lazy<ManagerService>(() => new ManagerService());
        public static ManagerService Instance => Lazy.Value;
        #endregion


        public readonly List<string> InitErrorMessages = new List<string>();
        private NodeViewAppNode _nodeViewCenterApp;
        private NodeViewAppNode _nodeViewEventLog;
        public bool HasInitError => InitErrorMessages.Count > 0;

        protected override bool OnInitializing(Configuration config)
        {
            if (!base.OnInitializing(config))
            {
                return false;
            }
            string centerIp = Config.GetValue("nodeViewCenter.ip", "127.0.0.1");
            int centerPort = Config.GetValue("nodeViewCenter.port", 20101);
            int processManagerPort = Config.GetValue("processManager.port", 20102);
            _nodeViewCenterApp = new NodeViewAppNode("center", "", "Center", "NodeViewCenter", centerIp, centerPort, processManagerPort:processManagerPort);
            _nodeViewEventLog = new NodeViewAppNode("eventLog", "", "EventLog", "EventLog", centerIp, centerPort, processManagerPort: processManagerPort);
            return true;
        }

        public NodeViewAppNode[] GetNodeViewApps()
        {
            List<NodeViewAppNode> appNodes = new List<NodeViewAppNode>();
            var apps = _centerClient.GetApps();
            if (apps == null || apps.Length == 0)
            {
                if (IsTestMode)
                {
                    apps = GetCachedApps();
                }
            }
            if (apps != null)
            {
                appNodes.AddRange(apps);
            }

            if (!appNodes.Any(node => node.AppType.Equals("center", StringComparison.OrdinalIgnoreCase)))
            {
                appNodes.Add(_nodeViewCenterApp);
            }
            appNodes.Add(_nodeViewEventLog);
            return appNodes.ToArray();
        }

        private NodeViewAppNode[] GetCachedApps()
        {
            List<NodeViewAppNode> appNodes = new List<NodeViewAppNode>();
            appNodes.Add(_nodeViewCenterApp);

            string cachedAppsFilename = Path.Combine(CachesFolder, "apps.json");
            if (File.Exists(cachedAppsFilename))
            {
                JArray appArray = JArray.Parse(File.ReadAllText(cachedAppsFilename, Encoding.UTF8));
                foreach (var apptoken in appArray)
                {
                    JObject json = apptoken.Value<JObject>();
                    var app = NodeViewAppNode.CreateFrom(json);
                    if (app != null)
                    {
                        appNodes.Add(app);
                    }
                }
            }

            return appNodes.ToArray();
        }
    }
}
