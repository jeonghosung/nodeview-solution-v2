﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Apps;
using NodeView.CommonWindows.ViewModels;
using NodeView.DataModels;
using NodeView.Frameworks.Widgets;
using NodeView.Net;
using NodeView.Protocols.NodeViews;
using NodeView.Utils;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Ioc;
using Prism.Services.Dialogs;

namespace NodeViewManager.ViewModels
{
    public class ManagerWindowViewModel:WindowChromeViewModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _cachesFolder;
        private IDialogService _dialogService = null;
        private WidgetItemViewModel _selectedItem = null;
        private List<WidgetSource> _dashboards = new List<WidgetSource>();
        private readonly ObservableCollection<WidgetItemViewModel> _widgetItems = new ObservableCollection<WidgetItemViewModel>();
        private NodeViewProcessManagerClient _processManagerClient;

        public DelegateCommand<string> SelectWidgetCommand { get; set; }
        public DelegateCommand AddWidgetCommand { get; set; }
        public DelegateCommand<string> RemoveWidgetCommand { get; set; }
        public DelegateCommand ChangePasswordCommand { get; set; }
        public ObservableCollection<WidgetItemViewModel> WidgetItems => _widgetItems;

        public ManagerWindowViewModel(IDialogService dialogService, IContainerProvider container)
        {
            _cachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(_cachesFolder);

            _dialogService = dialogService;
            AddWidgetCommand = new DelegateCommand(ExecuteAddWidgetCommand);
            RemoveWidgetCommand = new DelegateCommand<string>(ExecuteRemoveWidgetCommand);
            SelectWidgetCommand = new DelegateCommand<string>(ExecuteSelectWidgetCommand);
            ChangePasswordCommand = new DelegateCommand(ExecuteChangePasswordCommand);
            WindowState = WindowState.Maximized;
            var appConfig = container.Resolve<Configuration>("appConfig");
            int processManagerPort = appConfig.GetValue("processManager.port", 20102);
            _processManagerClient = new NodeViewProcessManagerClient("127.0.0.1", processManagerPort);
            Task.Factory.StartNew(() =>
            {
                _processManagerClient.RegisterApp(ProcessType.Application, AppUtils.GetAppName(), "", "");
            });
            LoadDashboards(appConfig);
            LoadWidgetItems();
        }

        private void LoadDashboards(Configuration appConfig)
        {
            Configuration dashboardConfigs = null;
            if (!appConfig.TryGetConfig("dashboards", out dashboardConfigs))
            {
                return;
            }

            foreach (var dashboardConfig in dashboardConfigs.ChildNodes)
            {
                var dashboard = WidgetSource.CreateFrom((IConfiguration) dashboardConfig);
                if (dashboard != null)
                {
                    _dashboards.Add(dashboard);
                }
            }
        }

        private void ExecuteChangePasswordCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "비밀번호 변경");
            _dialogService.ShowDialog("LocalChangePasswordDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                }
            });
        }

        private WidgetSource[] GetWidgetSources()
        {
            List<WidgetSource> widgetSources = new List<WidgetSource>();
            try
            {
                foreach (var app in ManagerService.Instance.GetNodeViewApps())
                {
                    if (string.Compare(app.AppType, "center", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        Configuration config = new Configuration(Uuid.NewUuid);
                        var appConfig = CreateConfiguration(app);
                        if (appConfig == null) continue;
                        config.SetChildNode(appConfig);

                        var widgetSource = new WidgetSource("", "centerSettings", $"NodeView 센터 설정", config);
                        widgetSources.Insert(0, widgetSource);
                    }
                    else if (string.Compare(app.AppType, "dataWall", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        Configuration config = new Configuration(Uuid.NewUuid);
                        var appConfig = CreateConfiguration(app);
                        if (appConfig == null) continue;
                        config.SetChildNode(appConfig);

                        var widgetSource = new WidgetSource("", "dataWallControl", $"{app.Name} 제어({app.Id})", config);
                        widgetSources.Insert(widgetSources.Count == 0 ? 0 : 1, widgetSource);

                        config = new Configuration(Uuid.NewUuid);
                        appConfig = CreateConfiguration(app);
                        if (appConfig == null) continue;
                        config.SetChildNode(appConfig);

                        widgetSource = new WidgetSource("", "dataWallSettings", $"{ app.Name } 설정({app.Id})", config);
                        widgetSources.Insert(widgetSources.Count == 0 ? 0 : 1, widgetSource);
                    }
                    else if (string.Compare(app.AppType, "iotStation", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        Configuration config = new Configuration(Uuid.NewUuid);
                        var appConfig = CreateConfiguration(app);
                        if (appConfig == null) continue;
                        config.SetChildNode(appConfig);

                        var widgetSource = new WidgetSource("", "iotStationSettings", $"{ app.Name } 설정({app.Id})", config);
                        widgetSources.Insert(widgetSources.Count == 0 ? 0 : 1, widgetSource);
                    }
                    else if (string.Compare(app.AppType, "eventLog", StringComparison.OrdinalIgnoreCase) == 0)
                    {
                        Configuration config = new Configuration(Uuid.NewUuid);
                        var appConfig = CreateConfiguration(app);
                        if (appConfig == null) continue;
                        config.SetChildNode(appConfig);

                        var widgetSource = new WidgetSource("", "eventLog", $"{ app.Name }", config);
                        widgetSources.Insert(widgetSources.Count == 0 ? 0 : 1, widgetSource);
                    }
                    //Todo: IotStation, Manager, 플러그인 데시보드 추가 해야 함
                }
                widgetSources.AddRange(_dashboards);
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetWidgetSources:");
            }

            return widgetSources.ToArray();
        }

        private Configuration CreateConfiguration(NodeViewAppNode app)
        {
            if (app == null) return null;

            Configuration config = new Configuration(app.Id);
            config.SetAttribute("appKey", app.AppKey);
            config.SetAttribute("appType", app.AppType);
            config.SetAttribute("name", app.Name);
            config.SetAttribute("ip", app.Ip);
            config.SetAttribute("port", $"{app.Port}");
            config.SetAttribute("uri", $"{app.Uri}");
            config.SetAttribute("isBlocked", app.IsBlocked);
            config.SetAttribute("processManagerPort", $"{app.ProcessManagerPort}");
            return config;
        }

        private void ExecuteAddWidgetCommand()
        {
            try
            {
                DialogParameters parameters = new DialogParameters();
                parameters.Add("widgetSources", GetWidgetSources());
                _dialogService.ShowDialog("WidgetSelectionDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        if (r.Parameters.TryGetValue("widgetSource", out WidgetSource widgetSource))
                        {
                            var widgetItem = CreateWidgetItem(widgetSource);
                            WidgetItems.Add(widgetItem);
                            SelectWidget(widgetItem.Id);
                            SaveWidgetItems();
                        }
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteAddWidgetCommand:");
            }
        }

        private void ExecuteRemoveWidgetCommand(string id)
        {
            RemoveWidget(id);
        }

        private void LoadWidgetItems()
        {
            string cachedWidgetsFilename = Path.Combine(_cachesFolder, "widgets.json");
            if (File.Exists(cachedWidgetsFilename))
            {
                /*
                Configuration widgetsConfig = Configuration.Load(cachedWidgetsFilename);
                foreach (var widgetConfig in widgetsConfig.ChildNodes)
                {
                    
                }
                */
                JArray widgetArray = JArray.Parse(File.ReadAllText(cachedWidgetsFilename, Encoding.UTF8));
                foreach (var widgetToken in widgetArray)
                {
                    JObject json = widgetToken.Value<JObject>();
                    var widgetSource = WidgetSource.CreateFrom(json);
                    if (widgetSource != null)
                    {
                        var widgetItem = CreateWidgetItem(widgetSource);
                        WidgetItems.Add(widgetItem);
                    }
                }
                if (WidgetItems.Count > 0)
                {
                    SelectWidget(WidgetItems[0].Id);
                }
            }
        }

        private void SaveWidgetItems()
        {
            try
            {
                JArray widgetItemArray = new JArray();
                foreach (var widgetItem in WidgetItems.ToArray())
                {
                    widgetItemArray.Add(widgetItem.Content.ToJson());
                }
                string cachedWidgetsFilename = Path.Combine(_cachesFolder, "widgets.json");
                File.WriteAllText(cachedWidgetsFilename, widgetItemArray.ToString(), Encoding.UTF8);
            }
            catch (Exception e)
            {
                Logger.Error(e, "SaveWidgetItems:");
            }
        }

        private static WidgetItemViewModel CreateWidgetItem(WidgetSource widgetSource)
        {
            var widgetItem = new WidgetItemViewModel(widgetSource);
            return widgetItem;
        }

        private void RemoveWidget(string id)
        {
            var widget = FindWidget(id);
            if (widget == null) return;

            _widgetItems.Remove(widget);
            if (_selectedItem?.Id == id)
            {
                _selectedItem = null;
                if (_widgetItems.Count > 0)
                {
                    SelectWidget(_widgetItems[0].Id);
                }
            }
            SaveWidgetItems();
        }

        private void ExecuteSelectWidgetCommand(string id)
        {
            SelectWidget(id);
        }

        private void SelectWidget(string id)
        {
            if (_selectedItem != null && _selectedItem.Id == id)
            {
                return;
            }

            var foundWidget = FindWidget(id);
            if (foundWidget == null)
            {
                return;
            }

            foundWidget.IsSelected = true;
            if (_selectedItem != null)
            {
                _selectedItem.IsSelected = false;
            }

            _selectedItem = foundWidget;
        }

        private WidgetItemViewModel FindWidget(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            foreach (var item in WidgetItems.ToArray())
            {
                if (item.Id == id)
                {
                    return item;
                }
            }

            return null;
        }
    }
}
