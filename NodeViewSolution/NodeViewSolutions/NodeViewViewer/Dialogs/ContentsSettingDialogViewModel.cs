﻿using NLog;
using NodeView.DataWalls;
using NodeView.Dialogs;
using NodeView.Tasks;
using NodeView.ViewModels;
using Prism.Commands;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using NodeViewViewer.ViewModels;
using NodeViewWindowCore.ViewModels;

namespace NodeViewViewer.Dialogs
{
    public class ContentsSettingDialogViewModel : DialogModelBase
    {
        private ContentSettingViewModel _contentSettingViewModel;

        public ContentSettingViewModel ContentSettingViewModel
        {
            get => _contentSettingViewModel;
            set => SetProperty(ref _contentSettingViewModel, value);

        }
       private IDialogService _dialogService = null;
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            if (parameters.TryGetValue("title", out string title))
            {
                if (!string.IsNullOrWhiteSpace(title))
                {
                    Title = title;
                }
            }
            if (parameters.TryGetValue("dialogservice", out IDialogService dialogService))
            {
                _dialogService = dialogService;
            }
            ContentSettingViewModel = new ContentSettingViewModel(_dialogService);
        }
        public override void RaiseRequestClose(IDialogResult dialogResult)
        {
            var parameters = GetResultParameters();
            parameters.Add("isContentModified", ContentSettingViewModel.IsContentModified);
            base.RaiseRequestClose(new DialogResult(dialogResult.Result, parameters));
        }
    }
}
