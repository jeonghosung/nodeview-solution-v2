﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Dialogs;
using NodeView.Net;
using Prism.Services.Dialogs;

namespace NodeViewViewer.Dialogs
{
    public class AddViewerPresetDialogViewModel : DialogModelBase
    {
        private string _errorMessage;
        private string _presetName;
        private string _presetId;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IEnumerable<IDataWallPreset> _currentDataWallPresets = null;
        private IEnumerable<IDataWallWindow> _currentDataWallWindows = null;

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public string PresetName
        {
            get => _presetName;
            set => SetProperty(ref _presetName, value);
        }
        public string PresetId
        {
            get => _presetId;
            set => SetProperty(ref _presetId, value);
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "프리셋 추가";
            if (parameters.TryGetValue("windows", out IEnumerable<IDataWallWindow> datawallWindows))
            {
                if (datawallWindows != null && datawallWindows.Any())
                {
                    _currentDataWallWindows = datawallWindows;
                }
            }
            if (parameters.TryGetValue("wallPresets", out IEnumerable<IDataWallPreset> dataWallPresets))
            {
                if (dataWallPresets != null && dataWallPresets.Any())
                {
                    _currentDataWallPresets = dataWallPresets;
                }
            }
        }
        protected override void ExecuteApplyDialogCommand()
        {
            if (_currentDataWallPresets != null && _currentDataWallPresets.Any(preset => preset.Id.Equals(PresetId, StringComparison.OrdinalIgnoreCase)))
            {
                ErrorMessage = "동일한 ID가 존재합니다.";
                PresetId = "";
                return;
            }

            if (string.IsNullOrWhiteSpace(PresetId) || string.IsNullOrWhiteSpace(PresetName))
            {
                ErrorMessage = "ID 또는 이름은 비워놓을 수 없습니다.";
                return;
            }
            ViewerService.Instance.AddPreset(new DataWallPreset(PresetId, PresetName, _currentDataWallWindows));
            base.ExecuteApplyDialogCommand();
        }
    }
}
