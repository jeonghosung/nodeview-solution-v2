﻿using NodeView.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DryIoc;
using NLog;
using NodeView.Drawing;
using NodeView.Tasks;
using NodeView.Utils;
using NodeView.ViewModels;
using NodeViewViewer.Models;
using NodeViewViewer.ViewModels;
using Prism.Commands;
using Prism.Services.Dialogs;

namespace NodeViewViewer.Dialogs
{
    public class LayoutSettingDialogViewModel : DialogModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IntSize _screenSize = new IntSize(1920, 1080);
        private List<ViewerLayoutCellBindable> _selectedLayoutCells = new List<ViewerLayoutCellBindable>();
        private ObservableCollection<ViewerLayoutBindable> _layouts = new ObservableCollection<ViewerLayoutBindable>();
        private bool _isLayoutCellSelected;
        private bool _isMultiLayoutCellSelected;
        private ViewerLayoutBindable _selectedLayout;
        private IDialogService _dialogService = null;
        private IDialogParameters _lastCellSplitOptionDialogParameters = null;
        private bool _isLayoutsModified = false;
        private bool _canDelete = false;

        public string ConfigsFolder { get; protected set; } = "";
        public IntSize ScreenSize
        {
            get => _screenSize;
            set => SetProperty(ref _screenSize, value);
        }
        public bool IsLayoutCellSelected
        {
            get => _isLayoutCellSelected;
            set => SetProperty(ref _isLayoutCellSelected, value);
        }
        public bool IsMultiLayoutCellSelected
        {
            get => _isMultiLayoutCellSelected;
            set => SetProperty(ref _isMultiLayoutCellSelected, value);
        }

        public ObservableCollection<ViewerLayoutBindable> Layouts
        {
            get => _layouts;
            set => SetProperty(ref _layouts, value);
        }

        public bool CanDelete
        {
            get => _canDelete;
            set => SetProperty(ref _canDelete, value);
        }
        public ObservableCollection<ViewerLayoutCellBindable> LayoutCells { get; set; } = new();

        public DelegateCommand SplitCellCommand { get; set; }
        public DelegateCommand MergeCellCommand { get; set; }
        public DelegateCommand<string> SelectCellCommand { get; set; }
        public DelegateCommand OpenAddLayoutDialogCommand { get; set; }
        public DelegateCommand DeleteLayoutCommand { get; set; }
        public DelegateCommand<string> SelectLayoutCommand { get; set; }

        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Init();
            if (parameters.TryGetValue("title", out string title))
            {
                if (!string.IsNullOrWhiteSpace(title))
                {
                    Title = title;
                }
            }

            if (parameters.TryGetValue("dialogservice", out IDialogService dialogService))
            {
                _dialogService = dialogService;
            }
        }
        public override void RaiseRequestClose(IDialogResult dialogResult)
        {
            dialogResult.Parameters.Add("islayoutsmodified", _isLayoutsModified);
            base.RaiseRequestClose(dialogResult);
        }
        private void Init()
        {
            var screenCells = GetDividedCells(new IntRect(0, 0, ScreenSize.Width, ScreenSize.Height), 2,2);
            LayoutCells.AddRange(screenCells);
            
            SelectCellCommand = new DelegateCommand<string>(ExecuteSelectCellCommand);
            SplitCellCommand = new DelegateCommand(ExecuteSplitCellCommand);
            MergeCellCommand = new DelegateCommand(ExecuteMergeCellCommand);
            OpenAddLayoutDialogCommand = new DelegateCommand(ExecuteOpenAddLayoutDialogCommand);
            SelectLayoutCommand = new DelegateCommand<string>(ExecuteSelectLayoutCommand);
            DeleteLayoutCommand = new DelegateCommand(ExecuteDeleteLayoutCommand);

            ConfigsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            Directory.CreateDirectory(ConfigsFolder);
            LoadLayouts();
        }
        private void LoadLayouts()
        {
            ThreadAction.PostOnUiThread(() =>
            {
                Layouts.Clear();
                foreach (var viewerLayout in ViewerService.Instance.GetLayouts())
                {
                    Layouts.Add(new ViewerLayoutBindable(viewerLayout));
                }
            });
        }
        public ViewerLayoutCellBindable[] GetDividedCells(IntRect targetRect, int divideColumns, int divideRows)
        {
            double dx = (double)targetRect.Width / (double)divideColumns;
            double dy = (double)targetRect.Height / (double)divideRows;

            List<ViewerLayoutCellBindable> celList = new List<ViewerLayoutCellBindable>();
            for (int y = 0; y < divideRows; y++)
            {
                int y1 = (int)(y * dy);
                int y2 = (int)((y + 1) * dy);
                for (int x = 0; x < divideColumns; x++)
                {
                    int x1 = (int)(x * dx);
                    int x2 = (int)((x + 1) * dx);
                    celList.Add(new ViewerLayoutCellBindable(new IntRect(targetRect.X + x1, targetRect.Y + y1, (x2 - x1), (y2 - y1))));
                }
            }

            return celList.ToArray();
        }
        private void SelectCell(string id)
        {
            var selectedScreenCells = _selectedLayoutCells.ToArray();
            
            foreach (var screenCell in selectedScreenCells)
            {
                if (screenCell.Id == id)
                {
                    ClearSelectedScreenCells();
                    UpdateSelectedCellProperties();
                    return;
                }
            }

            ViewerLayoutCellBindable foundScreenCell = FindScreenCell(id);
            if (foundScreenCell == null)
            {
                return;
            }
            if (selectedScreenCells.Length == 0)
            {
                foundScreenCell.IsSelected = true;
                _selectedLayoutCells.Add(foundScreenCell);
            }
            else
            {
                bool isHandled = false;
                IntRect selectedRect = new IntRect(foundScreenCell.Rect);
                foreach (var selectedScreenCell in selectedScreenCells)
                {
                    selectedRect.Union(selectedScreenCell.Rect);
                }

                IntRect unionRect = new IntRect(selectedRect);
                List<ViewerLayoutCellBindable> newSelectedScreenCellList = new List<ViewerLayoutCellBindable>();
                if (!isHandled)
                {
                    foreach (var screenCell in LayoutCells.ToArray())
                    {
                        if (selectedRect.IsIntersectsAreaWith(screenCell.Rect))
                        {
                            newSelectedScreenCellList.Add(screenCell);
                            unionRect.Union(screenCell.Rect);
                        }
                    }

                    if (!isHandled)
                    {
                        if (selectedRect != unionRect)
                        {
                            ClearSelectedScreenCells();
                            foundScreenCell.IsSelected = true;
                            _selectedLayoutCells.Add(foundScreenCell);
                        }
                        else
                        {
                            ClearSelectedScreenCells();
                            foreach (ViewerLayoutCellBindable screenCell in newSelectedScreenCellList.ToArray())
                            {
                                screenCell.IsSelected = true;
                                _selectedLayoutCells.Add(screenCell);
                            }
                        }
                    }
                }
            }
            UpdateSelectedCellProperties();
        }
        private void UpdateSelectedCellProperties()
        {
            bool isLayoutCellSelected = false;
            bool isMultiLayoutCellSelected = false;
            var selectedScreenCells = _selectedLayoutCells.ToArray();
            if (selectedScreenCells.Length == 1)
            {
                isLayoutCellSelected = true;
                isMultiLayoutCellSelected = false;
            }
            else if (selectedScreenCells.Length > 1)
            {
                isLayoutCellSelected = true;
                isMultiLayoutCellSelected = true;
            }
            IsLayoutCellSelected = isLayoutCellSelected;
            IsMultiLayoutCellSelected = isMultiLayoutCellSelected;
        }
        private ViewerLayoutCellBindable FindScreenCell(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return null;
            }

            ViewerLayoutCellBindable foundScreenCell = null;
            foreach (var cell in LayoutCells.ToArray())
            {
                if (cell.Id == id)
                {
                    foundScreenCell = cell;
                    break;
                }
            }

            return foundScreenCell;
        }
        private void ClearSelectedScreenCells()
        {
            foreach (var screenCell in _selectedLayoutCells)
            {
                screenCell.IsSelected = false;
            }
            _selectedLayoutCells.Clear();
            UpdateSelectedCellProperties();
        }
        private ViewerLayoutCellBindable MergeSelectedCells()
        {
            if (_selectedLayoutCells.Count <= 1)
            {
                return null;
            }
            var selectedScreenCells = _selectedLayoutCells.ToArray();
            ClearSelectedScreenCells();

            IntRect newScreenRect = IntRect.Empty;
            foreach (var screenCell in selectedScreenCells)
            {
                if (newScreenRect.IsEmpty)
                {
                    newScreenRect = screenCell.Rect;
                }
                else
                {
                    newScreenRect.Union(screenCell.Rect);
                }
                LayoutCells.Remove(screenCell);
            }

            ViewerLayoutCellBindable mergeScreenCell = new ViewerLayoutCellBindable(newScreenRect);
            LayoutCells.Insert(0, mergeScreenCell);
            return mergeScreenCell;
        }
        private void SplitSelectedCell(int columns, int rows)
        {
            if (columns <= 0 || rows <= 0)
            {
                return;
            }
            if (_selectedLayoutCells.Count == 0)
            {
                return;
            }
            ViewerLayoutCellBindable targetScreenCell = _selectedLayoutCells[0];
            if (_selectedLayoutCells.Count > 1)
            {
                targetScreenCell = MergeSelectedCells();
            }

            if (targetScreenCell == null)
            {
                return;
            }
            ClearSelectedScreenCells();

            ViewerLayoutCellBindable[] newScreenCells = GetDividedCells(targetScreenCell.Rect, columns, rows);
            LayoutCells.Remove(targetScreenCell);
            foreach (ViewerLayoutCellBindable newScreenCell in newScreenCells)
            {
                LayoutCells.Insert(0, newScreenCell);
            }
        }
        private void ExecuteSelectCellCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            SelectCell(id);
        }
        private void ExecuteMergeCellCommand()
        {
            MergeSelectedCells();
        }
        private void ExecuteSplitCellCommand()
        {
            try
            {
                _dialogService.ShowDialog("CellSplitOptionDialog", _lastCellSplitOptionDialogParameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        _lastCellSplitOptionDialogParameters = r.Parameters;
                        SplitSelectedCell(r.Parameters.GetValue<int>("columns"), r.Parameters.GetValue<int>("rows"));
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Error(e, "ExecuteSplitCellCommand:");
            }
        }
        private void ExecuteOpenAddLayoutDialogCommand()
        {
            var parameters = new DialogParameters
            {
                {"layoutcells", LayoutCells.ToArray()}
            };
            _dialogService.ShowDialog("AddLayoutDialog", parameters, addPresetResult =>
            {
                if (addPresetResult.Result == ButtonResult.OK)
                {
                    var isLayoutModified = addPresetResult.Parameters.GetValue<bool>("islayoutsmodified");
                    if (isLayoutModified)
                    {
                        _isLayoutsModified = isLayoutModified;
                        LoadLayouts();
                    }
                }
            });
        }
        private void ExecuteDeleteLayoutCommand()
        {
            if (_selectedLayout == null)
            {
                return;
            }
            var parameters = new DialogParameters
            {
                {"title", $"Layout 삭제"}, {"message", $"{_selectedLayout.Id} / {_selectedLayout.Name}을 삭제하시겠습니까?"}
            };
            _dialogService.ShowDialog("confirm", parameters, addPresetResult =>
            {
                if (addPresetResult.Result == ButtonResult.OK)
                {
                    ViewerService.Instance.DeleteLayouts(new []{_selectedLayout.Id});
                    LoadLayouts();
                    _isLayoutsModified = true;
                }
            });
        }
        private void ExecuteSelectLayoutCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            if (_selectedLayout != null)
            {
                string selectedLayoutId = _selectedLayout.Id;
                _selectedLayout.IsSelected = false;
                _selectedLayout = null;
                if (selectedLayoutId == id)
                {
                    return;
                }
            }

            ClearAllSelectedLayout();

            foreach (var layout in Layouts.ToArray())
            {
                if (layout.Id == id)
                {
                    _selectedLayout = layout;
                    _selectedLayout.IsSelected = true;
                }
            }

            if (_selectedLayout != null)
            {
                CanDelete = !_selectedLayout.IsDefault;
                LayoutCells.Clear();
                foreach (var selectedLayoutCell in _selectedLayout.Cells)
                {
                    LayoutCells.Add(new ViewerLayoutCellBindable(selectedLayoutCell));
                }
            }
            
        }
        private void ClearAllSelectedLayout()
        {
            if (_selectedLayout != null)
            {
                _selectedLayout.IsSelected = false;
                _selectedLayout = null;
            }
        }
    }
}
