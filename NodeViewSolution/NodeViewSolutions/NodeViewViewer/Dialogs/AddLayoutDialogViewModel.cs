﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Dialogs;
using NodeView.Net;
using NodeViewViewer.Models;
using Prism.Services.Dialogs;

namespace NodeViewViewer.Dialogs
{
    public class AddLayoutDialogViewModel : DialogModelBase
    {
        private string _errorMessage;
        private string _layoutName;
        private string _layoutId;
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IViewerLayout[] _currentLayouts = null;
        private IEnumerable<IViewerLayoutCell> _layoutCells;
        private bool _isLayoutsModified = false;

        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        public string LayoutName
        {
            get => _layoutName;
            set => SetProperty(ref _layoutName, value);
        }
        public string LayoutId
        {
            get => _layoutId;
            set => SetProperty(ref _layoutId, value);
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            Title = "Layout 추가";
            _currentLayouts = ViewerService.Instance.GetLayouts();
            if (parameters.TryGetValue("layoutcells", out IViewerLayoutCell[] layoutCells))
            {
                if (layoutCells != null && layoutCells.Length > 0)
                {
                    _layoutCells = layoutCells;
                }
            }
        }
        protected override void ExecuteApplyDialogCommand()
        {
            if (_currentLayouts != null && _currentLayouts.Any(preset => preset.Id.Equals(LayoutId, StringComparison.OrdinalIgnoreCase)))
            {
                ErrorMessage = "동일한 ID가 존재합니다.";
                LayoutId = "";
                return;
            }

            if (string.IsNullOrWhiteSpace(LayoutId) || string.IsNullOrWhiteSpace(LayoutName))
            {
                ErrorMessage = "ID 또는 이름은 비워놓을 수 없습니다.";
                return;
            }
            ViewerService.Instance.AddLayouts(new ViewerLayout(LayoutId, LayoutName, false, _layoutCells));
            _isLayoutsModified = true;
            var parameters = GetResultParameters();
            parameters.Add("islayoutsmodified", _isLayoutsModified);
            RaiseRequestClose(new DialogResult(ButtonResult.OK, parameters));
        }
    }
}
