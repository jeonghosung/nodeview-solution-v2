﻿using NodeView.Dialogs;
using Prism.Services.Dialogs;
using System;
using NodeViewViewer.Auth;

namespace NodeViewViewer.Dialogs
{
    public class LocalChangePasswordDialogModel : DialogModelBase
    {
        private NodeViewViewerAuthentication _NodeViewViewerAuthentication = null;

        private string _currentPassword;
        private string _newPassword;
        private string _newPasswordConfirm;
        private string _newPasswordText;
        private string _newPasswordConfirmText;
        private string _errorMessage;
        public string CurrentPassword
        {
            get => _currentPassword;
            set => SetProperty(ref _currentPassword, value);
        }
        public string NewPassword
        {
            get => _newPassword;
            set => SetProperty(ref _newPassword, value);
        }
        public string NewPasswordConfirm
        {
            get => _newPasswordConfirm;
            set => SetProperty(ref _newPasswordConfirm, value);
        }

        /// 마스킹 처리된 패스워드 텍스트 todo : 다른 처리방안 생각중
        public string NewPasswordText
        {
            get => _newPasswordText;
            set => SetProperty(ref _newPasswordText, value);
        }
        public string NewPasswordConfirmText
        {
            get => _newPasswordConfirmText;
            set => SetProperty(ref _newPasswordConfirmText, value);
        }
        public string ErrorMessage
        {
            get => _errorMessage;
            set => SetProperty(ref _errorMessage, value);
        }
        protected override void ExecuteCancelDialogCommand()
        {
            RaiseRequestClose(new DialogResult(ButtonResult.Cancel));
        }
        public override void OnDialogOpened(IDialogParameters parameters)
        {
            base.OnDialogOpened(parameters);
            _NodeViewViewerAuthentication = new NodeViewViewerAuthentication();
            if (parameters.TryGetValue("title", out string title))
            {
                if (!string.IsNullOrWhiteSpace(title))
                {
                    Title = title;
                }
            }
        }
        protected override void ExecuteApplyDialogCommand()
        {
            if (string.IsNullOrWhiteSpace(NewPassword) || string.IsNullOrWhiteSpace(NewPasswordConfirm))
            {
                return;
            }
            if (!NewPassword.Equals(NewPasswordConfirm, StringComparison.OrdinalIgnoreCase))
            {
                ErrorMessage = "변경할 비밀번호가 일치하지 않습니다.";
                NewPassword = NewPasswordConfirm = NewPasswordText = NewPasswordConfirmText = string.Empty;
                return;
            }

            if (!_NodeViewViewerAuthentication.ChangePassword(CurrentPassword, NewPassword))
            {
                ErrorMessage = "현재 비밀번호를 확인해주세요.";
                return;
            }
            RaiseRequestClose(new DialogResult(ButtonResult.OK));
        }
    }
}
