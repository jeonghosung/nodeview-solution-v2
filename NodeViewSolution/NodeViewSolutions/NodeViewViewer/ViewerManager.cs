﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.ViewModels;

namespace NodeViewViewer
{
    public class ViewerManager
    {
        #region SingleTone
        private static readonly Lazy<ViewerManager> Lazy = new Lazy<ViewerManager>(() => new ViewerManager());
        public static ViewerManager Instance => Lazy.Value;
        public bool IsTestMode { get; set; }

        #endregion

        public bool Start()
        {
            return true;
        }

        public void CreateWindows(DataWallWindow[] toArray)
        {
            throw new NotImplementedException();
        }

        public void CloseWindowsInRect(IntRect rect)
        {
            throw new NotImplementedException();
        }

        public void CloseWindows(string[] closeWindowIds)
        {
            throw new NotImplementedException();
        }

        public void CloseAllWindows()
        {
            throw new NotImplementedException();
        }

        public DataWallContent[] GetContents()
        {
            return ViewerService.Instance.GetContents();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }
    }
}
