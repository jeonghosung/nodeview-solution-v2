﻿using System;
using System.Collections.Generic;
using System.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.DeviceControllers;
using NodeView.Drawing;

namespace NodeViewViewer.Devices
{
    public class NodeViewViewerController : WallControllerBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public NodeViewViewerController(string id)
        {
            Id = id;
            DeviceType = "wall";
            Name = "ViewerController";
            Description = "NodeViewViewer";
            ConnectionString = "";
            ViewerService dataWallService = ViewerService.Instance;
            ScreenSize = new IntSize(dataWallService.ScreenSize);
            Matrix = new IntSize(dataWallService.ScreenMatrix);
        }
        protected override CommandDescription[] GetCommandDescriptions()
        {
            List<CommandDescription> commandDescriptions = new List<CommandDescription>();
            commandDescriptions.AddRange(base.GetCommandDescriptions());

            commandDescriptions.Add(new CommandDescription("refresh", "Viewer 화면 리플레시"));

            return commandDescriptions.ToArray();
        }
        public override bool RunCommand(string command, KeyValueCollection parameters)
        {
            bool isHandled = false;
            try
            {
                if (base.RunCommand(command, parameters))
                {
                    return true;
                }

                if (string.Compare(command, "refresh", StringComparison.OrdinalIgnoreCase) == 0)
                {
                    ViewerService.Instance.Refresh();
                    isHandled = true;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"[{Id}]RunCommand:");
            }
            return isHandled;
        }
        
        public override IDataWallWindow CreateWindow(string id, string contentId, IntRect windowRect)
        {
            if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(contentId) || windowRect.IsEmpty)
            {
                return null;
            }
            var window = new DataWallWindow(id, contentId, false, windowRect, contentId);
            //var windows = ViewerService.Instance.CreateWindows(new []{window});
            //if (windows != null && windows.Length > 0)
            //{
            //    return windows[0];
            //}

            return null;
        }

        public override void CloseWindow(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            //ViewerService.Instance.CloseWindows(new[] { id });
        }

        public override void CloseAllWindows()
        {
            //ViewerService.Instance.CloseAllWindows();
        }

        public override IDataWallWindow CreatePopup(string id, string contentId, IntRect windowRect)
        {
            throw new NotImplementedException();
        }

        public override void MovePopup(string id, IntRect windowRect)
        {
            throw new NotImplementedException();
        }

        public override void ClosePopup(string id)
        {
            throw new NotImplementedException();
        }

        public override void CloseAllPopups()
        {
            throw new NotImplementedException();
        }


        public override bool ApplyPreset(string id)
        {
            return true;
            //return ViewerService.Instance.ApplyPreset(id);
        }
    }
}
