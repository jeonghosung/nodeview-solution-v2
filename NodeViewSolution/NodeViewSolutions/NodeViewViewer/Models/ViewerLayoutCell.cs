﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;

namespace NodeViewViewer.Models
{
    public class ViewerLayoutCell : IViewerLayoutCell
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public string Id { get; }
        public string Name { get; set; }
        public IntRect Rect { get; set; }
        public IDataWallWindow DataWallWindow { get; set; }

        public ViewerLayoutCell(IViewerLayoutCell viewerLayoutCell)
        {
            Id = viewerLayoutCell.Id;
            Name = viewerLayoutCell.Name;
            Rect = new IntRect(viewerLayoutCell.Rect);
            DataWallWindow = viewerLayoutCell.DataWallWindow == null ? new DataWallWindow("", screenRect:viewerLayoutCell.Rect) : viewerLayoutCell.DataWallWindow;
        }
        public ViewerLayoutCell(string id, string name = "", IntRect rect = null)
        {
            Id = id;
            Name = name;
            Rect = rect ?? IntRect.Empty;
        }

        public JToken ToJson()
        {
            var jObject = new JObject
            {
                ["id"] = Id,
                ["name"] = Name,
                ["rect"] = Rect.ToString()
            };
            return jObject;
        }
        public static ViewerLayoutCell CreateFrom(JObject json)
        {
            try
            {
                string id = JsonUtils.GetStringValue(json, "id", Uuid.NewUuid);
                if (json == null || string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }

                var layoutCell = new ViewerLayoutCell(id);
                if (layoutCell.LoadFrom(json))
                {
                    return layoutCell;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public static ViewerLayoutCell CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetStringAttrValue(xmlNode, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var preset = new ViewerLayoutCell(id);
                if (preset.LoadFrom(xmlNode))
                {
                    return preset;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                Name = JsonUtils.GetValue(json, "name", Name);
                Rect = JsonUtils.GetValue(json, "rect", Rect);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Layout";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("rect", $"{Rect}");
            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
                Rect = XmlUtils.GetAttrValue(xmlNode, "rect", new IntRect(Rect));
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
    }
}
