﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.Drawing;

namespace NodeViewViewer.Models
{
    public interface IViewerLayout
    {
        string Id { get; }
        string Name { get; set; }
        bool IsDefault { get; }
        IEnumerable<IViewerLayoutCell> Cells { get; }
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
