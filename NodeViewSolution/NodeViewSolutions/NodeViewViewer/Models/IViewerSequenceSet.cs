﻿using Newtonsoft.Json.Linq;
using NodeView.DataWalls;
using System.Xml;

namespace NodeViewViewer.Models
{
    public interface IViewerSequenceSet
    {
        public string Id { get; }
        public string Name { get; }
        public int Interval { get; set; }
        public IDataWallPreset Preset { get; set; }
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
