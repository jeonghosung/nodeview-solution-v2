﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Utils;

namespace NodeViewViewer.Models
{
    public class ViewerSequenceSet : IViewerSequenceSet
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        public string Id { get; }
        public string Name { get; }
        public int Interval { get; set; } = 30;
        public IDataWallPreset Preset { get; set; }

        public ViewerSequenceSet(IViewerSequenceSet sequenceSet)
        {
            Preset = sequenceSet.Preset;
            Id = sequenceSet.Id;
            Name = sequenceSet.Name;
            Interval = sequenceSet.Interval;
        }
        public ViewerSequenceSet(IDataWallPreset preset, int interval = 30)
        {
            Preset = preset;
            Id = preset.Id;
            Name = preset.Name;
            Interval = interval;
        }
        public JToken ToJson()
        {
            throw new NotImplementedException();
        }

        public static ViewerSequenceSet CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                int interval = XmlUtils.GetIntAttrValue(xmlNode, "interval");
                XmlNode presetNode = xmlNode["Preset"];
                if (presetNode != null)
                {
                    DataWallPreset preset = DataWallPreset.CreateFrom(presetNode);
                    if (preset != null)
                    {
                        var sequenceSet = new ViewerSequenceSet(preset, interval);
                        if (sequenceSet.LoadFrom(xmlNode))
                        {
                            return sequenceSet;
                        }
                    }
                    
                }
                
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(JObject json)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "SequenceSet";
            }
            xmlWriter.WriteStartElement(tagName);
            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("interval", Interval.ToString());

            if (Preset != null)
            {
                Preset.WriteXml(xmlWriter, "Preset");
            }

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                IDataWallPreset preset = null;
                XmlNode presetNode = xmlNode["Preset"];
                if (presetNode != null)
                {
                    preset = DataWallPreset.CreateFrom(presetNode);
                }

                Preset = preset;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
    }
}
