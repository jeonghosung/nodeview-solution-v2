﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;

namespace NodeViewViewer.Models
{
    public class ViewerSequnce : IViewerSequnce
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private List<IViewerSequenceSet> _sequenceSets;
        public string Id { get; }
        public string Name { get; set; }
        public IEnumerable<IViewerSequenceSet> SequenceSets => _sequenceSets;
        public ViewerSequnce(string id, string name = "", IEnumerable<IViewerSequenceSet> seqeunceSets = null)
        {
            Id = id;
            Name = string.IsNullOrWhiteSpace(name) ? name : id;
            if (seqeunceSets != null)
            {
                _sequenceSets = seqeunceSets.ToList();
            }
        }

        public void UpdateSequence(IViewerSequenceSet newSequenceSet)
        {
            if (_sequenceSets.Any(sequenceSet => sequenceSet.Id.Equals(newSequenceSet.Id)))
            {
                var currentSequeneSet = _sequenceSets.First(sequenceSet => sequenceSet.Id.Equals(newSequenceSet.Id));
                _sequenceSets.Insert(_sequenceSets.IndexOf(currentSequeneSet), newSequenceSet);
                _sequenceSets.Remove(currentSequeneSet);
            }
            else
            {
                _sequenceSets.ToList().Add(newSequenceSet);
            }
        }
        public JToken ToJson()
        {
            throw new NotImplementedException();
        }

        public bool LoadFrom(JObject json)
        {
            throw new NotImplementedException();
        }

        public static ViewerSequnce CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetStringAttrValue(xmlNode, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                var sequnce = new ViewerSequnce(id);
                if (sequnce.LoadFrom(xmlNode))
                {
                    return sequnce;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Sequence";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);

            if (SequenceSets != null)
            {
                foreach (var viewerSequenceSet in SequenceSets)
                {
                    viewerSequenceSet.WriteXml(xmlWriter, "SequenceSet");
                }
            }

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);

                ViewerSequenceSet seqeunceSets = null;
                List<IViewerSequenceSet> sequenceSets = new List<IViewerSequenceSet>();
                XmlNodeList sequenceSetList = xmlNode.SelectNodes("SequenceSet");
                if (sequenceSetList != null)
                {
                    foreach (XmlNode sequenceSet in sequenceSetList)
                    {
                        var window = ViewerSequenceSet.CreateFrom(sequenceSet);
                        if (window != null)
                        {
                            sequenceSets.Add(window);
                        }
                    }
                }

                _sequenceSets = sequenceSets;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
    }
}
