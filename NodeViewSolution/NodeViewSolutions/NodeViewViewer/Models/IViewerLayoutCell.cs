﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataWalls;
using NodeView.Drawing;

namespace NodeViewViewer.Models
{
    public interface IViewerLayoutCell
    {
        string Id { get; }
        string Name { get; set; }
        IntRect Rect { get; set; }
        IDataWallWindow DataWallWindow { get; set; }
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
