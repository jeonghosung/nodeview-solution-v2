﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataWalls;

namespace NodeViewViewer.Models
{
    public interface IViewerSequnce 
    {
        public string Id { get; }
        public string Name { get; set; }
        public IEnumerable<IViewerSequenceSet> SequenceSets { get; }
        JToken ToJson();
        bool LoadFrom(JObject json);
        void WriteXml(XmlWriter xmlWriter, string tagName = "");
        bool LoadFrom(XmlNode xmlNode);
    }
}
