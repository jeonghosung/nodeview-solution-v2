﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.Frameworks.Widgets;
using NodeView.Ioc;
using NodeView.ViewModels;
using Prism.Services.Dialogs;

namespace NodeViewViewer.Views
{
    /// <summary>
    /// ManagerWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ViewerWindow : Window
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private Point _startPoint;
        private Point _enterPoint;

        private IDialogService _dialogService = null;

        public ViewerWindow()
        {
            InitializeComponent();
        }
        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            try
            {
                _dialogService = (IDialogService)NodeViewContainerExtension.GetContainerProvider().Resolve(typeof(IDialogService));
            }
            catch(Exception ex)
            {
                Logger.Info(ex, "OnInitialized error.");
            }

            if (_dialogService == null)
            {
                Logger.Warn("DialogService is null.");
                return;
            }
            _dialogService.ShowDialog("LocalLoginDialog", null, loginResultCallback =>
            {
                if (loginResultCallback.Result != ButtonResult.OK)
                {
                    Application.Current.Shutdown();
                }
            });
        }
        private void ScrollViewer_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            ScrollViewer scrollViewer = (ScrollViewer)sender;
            if (e.Delta < 0)
            {
                scrollViewer.LineRight();
            }
            else
            {
                scrollViewer.LineLeft();
            }
            e.Handled = true;
        }
        private T FindAnchestor<T>(DependencyObject current) where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);

            return null;
        }

        private void treeViewContents_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _startPoint = e.GetPosition(null);
        }

        private void treeViewContents_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            Point mousPos = e.GetPosition(null);
            Vector diff = _startPoint - mousPos;

            if ((e.LeftButton == MouseButtonState.Pressed) && (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance) && (Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                Button button = sender as Button;
                if (button == null)
                {
                    return;

                }
                try
                {
                    DragDrop.DoDragDrop(button, data: button.DataContext, DragDropEffects.Move);
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "DoDragDrop Exception.");
                }
            }
        }
    }
}
