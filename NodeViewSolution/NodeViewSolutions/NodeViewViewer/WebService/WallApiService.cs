﻿using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Authentications;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Ioc;
using NodeView.Net;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using RestApiResponse = NodeView.WebServers.RestApiResponse;

namespace NodeViewViewer.WebService
{
    [RestResource(BasePath = "/api/v1")]
    public class WallApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IAuthentication _authentication = null;
        public WallApiService()
        {
            try
            {
                _authentication = (IAuthentication)NodeViewContainerExtension.GetContainerProvider()?.Resolve(typeof(IAuthentication));
            }
            catch
            {
                //Logger.Info("Not supported IAuthentication.");
                _authentication = null;
            }
        }
        private bool ValidateAccessToken(IHttpContext context)
        {
            return _authentication?.ValidateAccessToken(context) ?? true;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = "")]
        public IHttpContext Echo(IHttpContext context)
        {
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok, "echo!");
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = "/")]
        public IHttpContext Index(IHttpContext context)
        {
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok,
                    TemplateManager.Render("PageLinks", GetServicePageList()), Encoding.UTF8);
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }
        private object GetServicePageList()
        {
            var linkInfos = new List<PageLinkInfo>();
            linkInfos.Add(new PageLinkInfo("Infos", "/api/v1/infos", ""));
            linkInfos.Add(new PageLinkInfo("Screen", "/api/v1/screen", ""));
            linkInfos.Add(new PageLinkInfo("Contents", "/api/v1/contents", ""));
            linkInfos.Add(new PageLinkInfo("Windows", "/api/v1/windows", ""));
            linkInfos.Add(new PageLinkInfo("PopupWindows", "/api/v1/popups", ""));
            linkInfos.Add(new PageLinkInfo("Preset", "/api/v1/presets", ""));
            return linkInfos;
        }
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/service/shutdown")]
        public IHttpContext ServiceShutdown(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                string requester = $"{context.Request.RemoteEndPoint.Address}";
                service.RequestShutdown(requester);

                RestApiResponse.SendResponseSuccess(context, "");
            }
            catch (Exception e)
            {
                Logger.Error(e, "ServiceShutdown!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/infos")]
        public IHttpContext GetInfos(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                JObject infoJson = new JObject()
                {
                    ["screen"] = new JObject()
                    {
                        ["size"] = $"{service.ScreenSize}",
                        ["matrix"] = $"{service.ScreenMatrix}",
                        ["updated"] = $"{service.ScreenUpdated}"
                    }
                    ["configs"] = new JObject()
                    {
                        ["updated"] = $"{service.ConfigsUpdated}"
                    },
                    ["contents"] = new JObject()
                    {
                        ["updated"] = $"{service.ContentsUpdated}"
                    },
                    ["presets"] = new JObject()
                    {
                        ["updated"] = $"{service.PresetsUpdated}"
                    },
                    ["devices"] = new JObject()
                    {
                        ["updated"] = $"{service.DevicesUpdated}"
                    }
                };

                RestApiResponse.SendResponseSuccess(context, infoJson);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetContents!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        
        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/contents")]
        public IHttpContext GetContents(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }

                var contents = service.GetContents();
                if (contents != null)
                {
                    JArray resArray = new JArray();
                    foreach (var content in contents)
                    {
                        resArray.Add(content.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get contents.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetContents!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/contents")]
        public IHttpContext CreateContents(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                if (!ValidateAccessToken(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                    return context;
                }

                string requester = $"{context.Request.RemoteEndPoint.Address}";

                ApiResponse result = ApiResponse.BadRequest;
                JObject responseJson = new JObject();

                List<DataWallContent> contents = new List<DataWallContent>();

                var jsonToken = JToken.Parse(context.Request.Payload);
                if (jsonToken.Type == JTokenType.Object)
                {
                    var content = DataWallContent.CreateFrom(jsonToken.Value<JObject>());
                    if (string.IsNullOrWhiteSpace(content?.Id))
                    {
                        RestApiResponse.SendResponseBadRequest(context, "payload error");
                        return context;
                    }
                    contents.Add(content);
                }
                else if (jsonToken.Type == JTokenType.Array)
                {
                    JArray array = jsonToken.Value<JArray>();
                    if (array != null)
                    {
                        foreach (var token in array)
                        {
                            var content = DataWallContent.CreateFrom(token.Value<JObject>());
                            if (string.IsNullOrWhiteSpace(content?.Id))
                            {
                                RestApiResponse.SendResponseBadRequest(context, "payload error");
                                return context;
                            }
                            contents.Add(content);
                        }
                    }
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload is not (a) content(s) json.");
                    return context;
                }

                //result = service.RequestCreateContents(requester, contents.ToArray());
                if (result.IsSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, responseJson);
                }
                else
                {
                    RestApiResponse.SendResponse(context, result);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateContents!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/contents")]
        public IHttpContext PutActionContents(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                ApiResponse result = ApiResponse.BadRequest;
                JObject responseJson = new JObject();

                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "deleteAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    //result = service.RequestDeleteAllContents(requester);
                }
                else if (string.Compare(putAction, "delete", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                    if (idArray != null && idArray.Count > 0)
                    {
                        List<string> idList = new List<string>();
                        foreach (JToken token in idArray)
                        {
                            string id = token.Value<string>();
                            if (!string.IsNullOrWhiteSpace(id))
                            {
                                idList.Add(id);
                            }
                        }

                        if (idList.Count > 0)
                        {
                            //result = service.RequestDeleteContents(requester, idList.ToArray());
                        }
                    }
                }
                else if (string.Compare(putAction, "update", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    JObject contentJson = JsonUtils.GetValue<JObject>(json, "param", null);
                    if (contentJson != null)
                    {
                        var content = DataWallContent.CreateFrom(contentJson);
                        if (!string.IsNullOrWhiteSpace(content?.Id))
                        {
                            //result = service.RequestUpdateContent(requester, content);
                        }
                    }
                }

                if (result.IsSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, responseJson);
                }
                else
                {
                    RestApiResponse.SendResponse(context, result);
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Widnows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/windows")]
        public IHttpContext GetWindows(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                var windows = service.GetWindows();
                if (windows != null)
                {
                    JArray resArray = new JArray();
                    foreach (var window in windows)
                    {
                        resArray.Add(window.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get windows.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateWindows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/windows")]
        public IHttpContext CreateWindows(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                List<DataWallWindow> windowList = new List<DataWallWindow>();

                var jsonToken = JToken.Parse(context.Request.Payload);
                if (jsonToken.Type == JTokenType.Object)
                {
                    var window = DataWallWindow.CreateFrom(jsonToken.Value<JObject>());
                    if (window == null)
                    {
                        RestApiResponse.SendResponseBadRequest(context, "payload error");
                        return context;
                    }
                    window.IsPopup = false;
                    windowList.Add(window);
                }
                else if (jsonToken.Type == JTokenType.Array)
                {
                    JArray array = jsonToken.Value<JArray>();
                    foreach (var token in array)
                    {
                        var window = DataWallWindow.CreateFrom(token.Value<JObject>());
                        if (window == null)
                        {
                            RestApiResponse.SendResponseBadRequest(context, "payload error");
                            return context;
                        }
                        window.IsPopup = false;
                        windowList.Add(window);
                    }
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload is not (a) content(s) json.");
                    return context;
                }

                //var resWindows = service.RequestCreateWindows(requester, windowList.ToArray());
                //if (resWindows != null)
                //{
                //    JArray resArray = new JArray();
                //    foreach (var windowContent in resWindows)
                //    {
                //        resArray.Add(windowContent.ToJson());
                //    }
                //    RestApiResponse.SendResponseSuccess(context, resArray);
                //}
                //else
                //{
                //    RestApiResponse.SendResponseBadRequest(context, "Cannot create windows.");
                //}
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateWindows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/windows")]
        public IHttpContext PutActionWindows(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                bool isSuccess = false;
                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "applyPreset", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string presetId = JsonUtils.GetStringValue(json, "param.id");
                    //isSuccess = service.RequestApplyPreset(requester, presetId);
                }
                else if (string.Compare(putAction, "closeInRect", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    IntRect rect = JsonUtils.GetValue<IntRect>(json, "param.rect", null);
                    if (rect != null && !rect.IsEmpty)
                    {
                        //service.RequestCloseWindowsInRect(requester, rect);
                        isSuccess = true;
                    }
                }
                else if (string.Compare(putAction, "closeAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    //service.RequestCloseAllWindows(requester);
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "close", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                    if (idArray != null && idArray.Count > 0)
                    {
                        List<string> idList = new List<string>();
                        foreach (JToken token in idArray)
                        {
                            string id = token.Value<string>();
                            if (!string.IsNullOrWhiteSpace(id))
                            {
                                idList.Add(id);
                            }
                        }

                        if (idList.Count > 0)
                        {
                            //service.RequestCloseWindows(requester, idList.ToArray());
                        }
                        isSuccess = true;
                    }
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, null);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Widnows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
        
        [RestRoute(HttpMethod = HttpMethod.DELETE, PathInfo = @"/windows/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext CloseWindow(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/windows/(?<id>[a-zA-Z0-9_\-]+)$");
                if (!match.Success)
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot parse id.");
                    return context;
                }

                id = match.Groups["id"].Value;

                //service.RequestCloseWindows(requester, new[] { id });

                RestApiResponse.SendResponseSuccess(context, null);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CloseWindow!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/presets")]
        public IHttpContext GetPresets(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                DataWallPreset[] presets = service.GetPresets();
                if (presets != null)
                {
                    JArray resArray = new JArray();
                    foreach (var preset in presets)
                    {
                        resArray.Add(preset.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get presets.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot GetPresets!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/presets")]
        public IHttpContext CreatePresets(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                if (!ValidateAccessToken(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                    return context;
                }

                string requester = $"{context.Request.RemoteEndPoint.Address}";

                var jsonObject = JObject.Parse(context.Request.Payload);

                string id = JsonUtils.GetStringValue(jsonObject, "id");
                string name = JsonUtils.GetStringValue(jsonObject, "name");

                if (string.IsNullOrWhiteSpace(id))
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload error");
                    return context;
                }

                //if (service.RequestCreatePreset(requester, id, name))
                //{ 
                //    RestApiResponse.SendResponseSuccess(context, null);
                //}
                //else
                //{
                //    RestApiResponse.SendResponseBadRequest(context, "Cannot create preset.");
                //}
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateWindows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/presets")]
        public IHttpContext PutActionPresets(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                string requester = $"{context.Request.RemoteEndPoint.Address}";

                bool isSuccess = false;
                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "rename", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    string id = JsonUtils.GetStringValue(json, "param.id");
                    string name = JsonUtils.GetStringValue(json, "param.name");
                    if (string.IsNullOrWhiteSpace(id) || string.IsNullOrWhiteSpace(name))
                    {
                        isSuccess = false;
                    }
                    else
                    {
                        //isSuccess = service.RequestRenamePreset(requester, id, name);
                    }
                }
                else if (string.Compare(putAction, "deleteAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    //service.RequestDeleteAllPresets(requester);
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "delete", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    if (!ValidateAccessToken(context))
                    {
                        RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                        return context;
                    }

                    JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                    if (idArray != null && idArray.Count > 0)
                    {
                        List<string> idList = new List<string>();
                        foreach (JToken token in idArray)
                        {
                            string id = token.Value<string>();
                            if (!string.IsNullOrWhiteSpace(id))
                            {
                                idList.Add(id);
                            }
                        }

                        if (idList.Count > 0)
                        {
                            //service.RequestDeletePresets(requester, idList.ToArray());
                        }
                        isSuccess = true;
                    }
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, null);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Presets!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
        
        [RestRoute(HttpMethod = HttpMethod.DELETE, PathInfo = @"/presets/(?<id>[a-zA-Z][a-zA-Z0-9]*)$")]
        public IHttpContext DeletePreset(IHttpContext context)
        {
            try
            {
                ViewerService service = ViewerService.Instance;
                if (service.IsVerbose)
                {
                    Logger.Info($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
                }
                if (!ValidateAccessToken(context))
                {
                    RestApiResponse.SendResponse(context, HttpStatusCode.Unauthorized, null);
                    return context;
                }

                string requester = $"{context.Request.RemoteEndPoint.Address}";

                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/presets/(?<id>[a-zA-Z][a-zA-Z0-9]*)$");
                if (!match.Success)
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot parse id.");
                    return context;
                }

                id = match.Groups["id"].Value;

                //service.RequestCloseWindows(requester, new[] { id });

                RestApiResponse.SendResponseSuccess(context, null);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot DeletePreset!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
    }
}
