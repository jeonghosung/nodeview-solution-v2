﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;

namespace NodeViewViewer.WebService
{
    [RestResource(BasePath = "/proxy/v1")]
    public class ProxyService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [RestRoute(HttpMethod = Semsol.Grapevine.Shared.HttpMethod.GET, PathInfo = "/")]
        public IHttpContext Index(IHttpContext context)
        {
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok,
                    TemplateManager.Render("PageLinks", GetServicePageList()), Encoding.UTF8);
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }
        private object GetServicePageList()
        {
            var linkInfos = new List<PageLinkInfo>();
            linkInfos.Add(new PageLinkInfo("Join", "/proxy/v1/screens", ""));
            return linkInfos;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/screens")]
        public IHttpContext PutActionScreens(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ViewerService dataWallService = ViewerService.Instance;
                bool isSuccess = false;
                JObject responseJson = null;
                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if(string.Compare(putAction, "join", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string id = JsonUtils.GetStringValue(json, "param.id");
                    int port = JsonUtils.GetIntValue(json, "param.port", 0);
                    int pid = JsonUtils.GetIntValue(json, "param.pid", 0);

                   
                    JArray windowsArray = new JArray();
                    foreach (var window in dataWallService.GetWindows())
                    {
                        windowsArray.Add(window.ToJson());
                    }
                    JArray popupsArray = new JArray();

                    JObject currentScreenInfoJson = new JObject();
                    currentScreenInfoJson["windows"] = windowsArray;
                    currentScreenInfoJson["popups"] = popupsArray;

                    responseJson = currentScreenInfoJson;
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "applyPreset", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string presetId = JsonUtils.GetStringValue(json, "param.presetId");
                    //isSuccess = dataWallService.RequestApplyPreset(requester, presetId);
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, responseJson);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Screens!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
    }
}
