﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Utils;
using Semsol.Grapevine.Interfaces.Server;

namespace NodeViewViewer.Auth
{
    public class NodeViewViewerAuthentication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _cachedPassword = "";
        private readonly string _cachedPasswordFileName;

        public NodeViewViewerAuthentication()
        {
            string cachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(cachesFolder);
            _cachedPasswordFileName = Path.Combine(cachesFolder, "cachedPassword.pw");
            if (File.Exists(_cachedPasswordFileName))
            {
                _cachedPassword = File.ReadAllText(_cachedPasswordFileName).Trim();
            }
        }
        public bool Login(string password)
        {
            try
            {

                if (string.IsNullOrWhiteSpace(password))
                {
                    return false;
                }

                if (string.IsNullOrWhiteSpace(_cachedPassword))
                {
                    if (password != "1234")
                    {
                        return false;
                    }
                }
                else
                {
                    if (_cachedPassword != CryptUtils.CreateMd5HexString(password))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Login:");
            }

            return false;
        }
        public bool ChangePassword(string currentPassword, string newPassword)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(newPassword) || string.IsNullOrWhiteSpace(currentPassword))
                {
                    return false;
                }

                string encryptNewPassword = CryptUtils.CreateMd5HexString(newPassword);
                string encryptCurrentPassword = CryptUtils.CreateMd5HexString(currentPassword);
                if (string.IsNullOrWhiteSpace(_cachedPassword) && !currentPassword.Equals("1234"))
                {
                    return false;
                }
                if (!string.IsNullOrWhiteSpace(_cachedPassword) && !encryptCurrentPassword.Equals(_cachedPassword, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }
                if (_cachedPassword == encryptNewPassword)
                {
                    return true;
                }

                _cachedPassword = encryptNewPassword;
                File.WriteAllText(_cachedPasswordFileName, _cachedPassword);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ChangePassword:");
            }

            return false;
        }
    }
}
