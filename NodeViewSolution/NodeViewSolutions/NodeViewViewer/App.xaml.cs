﻿using CefSharp;
using CefSharp.Wpf;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.DataWallContentControls;
using NodeView.Dialogs;
using NodeView.DialogWindows;
using NodeView.Extension.Stations;
using NodeView.Frameworks.Modularity;
using NodeView.Frameworks.Stations;
using NodeView.Ioc;
using NodeViewScreenFramework;
using NodeViewScreenFramework.DataWallContentControls;
using NodeViewViewer.Dialogs;
using NodeViewViewer.Views;
using NodeViewWindowCore.Dialogs;
using Prism.DryIoc;
using Prism.Events;
using Prism.Ioc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace NodeViewViewer
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : PrismApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private ViewerService _service;
        private readonly ModuleManager _nodeViewModuleManager = new ModuleManager();
        private readonly List<string> _criticalErrorMessages = new List<string>();
        private bool HasCriticalError => _criticalErrorMessages.Count > 0;
        private Window _mainWindow = null;

        public App()
        {
            var settings = new CefSettings()
            {
                //By default CefSharp will use an in-memory cache, you need to specify a Cache Folder to persist data
                //CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "CefSharp\\Cache")
            };
            try
            {
                //NOTE: WebRTC Device Id's aren't persisted as they are in Chrome see https://bitbucket.org/chromiumembedded/cef/issues/2064/persist-webrtc-deviceids-across-restart
                settings.CefCommandLineArgs.Add("enable-media-stream");
                //https://peter.sh/experiments/chromium-command-line-switches/#use-fake-ui-for-media-stream
                settings.CefCommandLineArgs.Add("use-fake-ui-for-media-stream");
                //For screen sharing add (see https://bitbucket.org/chromiumembedded/cef/issues/2582/allow-run-time-handling-of-media-access#comment-58677180)
                settings.CefCommandLineArgs.Add("enable-usermedia-screen-capturing");
                settings.CefCommandLineArgs["autoplay-policy"] = "no-user-gesture-required";
                settings.IgnoreCertificateErrors = true;
                if (Cef.IsInitialized is null or false)
                {
                    //Perform dependency check to make sure all relevant resources are in our output directory.
                    Cef.Initialize(settings);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Cef init error.");
            }
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            string appName = ResourceAssembly.GetName().Name;
            if (AppUtils.IsDuplicateExecution())
            {
                MessageBox.Show($"'{appName}' 프로그램이 이미 실행 중입니다.", "알림", MessageBoxButton.OK,
                    MessageBoxImage.Information);
                Current.Shutdown();

            }
            else
            {
                SetupUnhandledExceptionHandling();
                base.OnStartup(e);


            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            _service = ViewerService.Instance;
            string configFilename = "NodeViewViewer.config.xml";

            //StationService
            if (containerRegistry is IContainerExtension containerExtension)
            {
                NodeViewContainerExtension.CreateInstance(containerExtension);
            }
            containerRegistry.RegisterSingleton(typeof(IStationRepository), typeof(SqliteStationRepository));
            //containerRegistry.RegisterInstance<StationServiceManager>(_service);

            IEventAggregator eventAggregator = new EventAggregator();
            containerRegistry.RegisterInstance(typeof(IEventAggregator), eventAggregator);
            containerRegistry.RegisterInstance(typeof(ModuleManager), _nodeViewModuleManager);

            //ModuleManager
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var configuration = Configuration.Load(settingFile);
            containerRegistry.RegisterInstance(typeof(Configuration), configuration, "appConfig");

            _nodeViewModuleManager.AddAssembly("NodeView.Extension.dll");
            if (configuration.TryGetNode("extensions", out var extensionsSection))
            {
                foreach (var extenstionProperty in extensionsSection.Properties)
                {
                    if (!_nodeViewModuleManager.AddAssembly(extenstionProperty.Value))
                    {
                        Logger.Error($"{extenstionProperty.Value}를 로드하는중에 오류가 발생했습니다.");
                    }
                }
            }
            _nodeViewModuleManager.RegisterTypes(containerRegistry);


            //Dialogs
            containerRegistry.RegisterDialogWindow<BasicDialogWindow>();
            containerRegistry.RegisterDialog<MessageDialog, MessageDialogViewModel>();
            containerRegistry.RegisterDialog<ConfirmMessageDialog, ConfirmMessageDialogModel>("confirm");
            containerRegistry.RegisterDialog<WarningConfirmMessageDialog, WarningConfirmMessageDialogModel>("warningConfirm");
            containerRegistry.RegisterDialog<CellSplitOptionDialog, CellSplitOptionDialogModel>();
            containerRegistry.RegisterDialog<AddLayoutDialog, AddLayoutDialogViewModel>();
            containerRegistry.RegisterDialog<AddViewerPresetDialog, AddViewerPresetDialogViewModel>();
            containerRegistry.RegisterDialog<LocalLoginDialog, LocalLoginDialogModel>();
            containerRegistry.RegisterDialog<LayoutSettingDialog, LayoutSettingDialogViewModel>();
            containerRegistry.RegisterDialog<ContentsSettingDialog, ContentsSettingDialogViewModel>();
            containerRegistry.RegisterDialog<LocalChangePasswordDialog, LocalChangePasswordDialogModel>();
            containerRegistry.RegisterDialog<AddPropertyDialog, AddPropertyDialogViewModel>();
            containerRegistry.RegisterDialog<AddPtzPresetDialog, AddPtzPresetDialogModel>();

            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "media");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "camera");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "ptz-camera");
            containerRegistry.RegisterDataWallContentControl(typeof(WebDataWallContentControl), "web");
            containerRegistry.RegisterDataWallContentControl(typeof(RemoteDataWallContentControl), "remote");
            containerRegistry.RegisterDataWallContentControl(typeof(RemoteDataWallContentViewerControl), "remoteviewer");
            containerRegistry.RegisterDataWallContentControl(typeof(ImageContentControl), "image");

            string[] options = Array.Empty<string>();
            var useRtspTcp = configuration.GetValue("useRtspTcp", false);
            if (useRtspTcp)
            {
                options = new[]{ "--rtsp-tcp" };
            }

            VlcLlibraryManager.Instance.Init(options);
            _service.Initialize(Container, configFilename);
        }

        protected override Window CreateShell()
        {
            if (HasCriticalError)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("설정에 오류가 발생하여 프로그램을 시작 할 수없습니다.");
                sbMessage.AppendLine("상세오류===============================");
                foreach (var errorMessage in _criticalErrorMessages)
                {
                    sbMessage.AppendLine(errorMessage);
                }

                MessageBox.Show(sbMessage.ToString(), "설정 오류", MessageBoxButton.OK, MessageBoxImage.Error);
                Current.Shutdown();
                return null;
            }

            _service.Start();
            if (_service.HasInitError)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("프로그램 시작중 아래의 경고가 발생했습니다.");
                sbMessage.AppendLine("상세경고===============================");
                foreach (var errorMessage in _service.InitErrorMessages)
                {
                    sbMessage.AppendLine(errorMessage);
                }

                MessageBox.Show(sbMessage.ToString(), "프로그램 시작", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            _mainWindow = Container.Resolve<ViewerWindow>();
            _mainWindow.Closed += (sender, args) => AppUtils.Exit();
            _nodeViewModuleManager.OnCreatedShell(Container, _mainWindow);
            return _mainWindow;
        }

        protected override void OnInitialized()
        {
            if (HasCriticalError)
            {
                return;
            }

            base.OnInitialized();
            _nodeViewModuleManager.OnInitialized(Container);

            if (_mainWindow != null)
            {
                _mainWindow.Closed += (sender, args) => { Application.Current.Shutdown(); };
            }
        }
        protected override void OnExit(ExitEventArgs e)
        {
            Logger.Info($"OnExit. ExitCode : {e.ApplicationExitCode}");
            _service?.Stop();
            base.OnExit(e);
        }

        private void SetupUnhandledExceptionHandling()
        {
            // Catch exceptions from all threads in the AppDomain.
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException",
                    false);

            // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
            TaskScheduler.UnobservedTaskException += (sender, args) =>
                ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", false);

            // Catch exceptions from a single specific UI dispatcher thread.
            Dispatcher.UnhandledException += (sender, args) =>
            {
                // If we are debugging, let Visual Studio handle the exception and take us to the code that threw it.
                if (!Debugger.IsAttached)
                {
                    args.Handled = true;
                    ShowUnhandledException(args.Exception, "Dispatcher.UnhandledException", true);
                }
            };
        }

        private List<string> _exceptUnhandledExceptionMessage = new List<string>()
            { "Waiting on the Task or accessing its Exception property" };
        void ShowUnhandledException(Exception e, string unhandledExceptionType, bool promptUserForShutdown)
        {
            Logger.Error(e, $"ShowUnhandledException ['{unhandledExceptionType}',{promptUserForShutdown}]");
            if (_exceptUnhandledExceptionMessage.Any(message => e.Message.IndexOf(message, StringComparison.Ordinal) >= 0))
            {
                return;
            }
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{e}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
