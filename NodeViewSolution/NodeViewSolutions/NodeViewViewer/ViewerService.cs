﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Frameworks.Plugins;
using NodeView.Frameworks.Stations;
using NodeView.Net;
using NodeView.NodeViews;
using NodeView.Protocols.NodeViews;
using NodeView.Systems;
using NodeView.Tasks;
using NodeView.Utils;
using NodeView.ViewModels;
using NodeView.WebServers;
using NodeViewViewer.Devices;
using NodeViewViewer.Models;
using NodeViewViewer.ViewModels;
using Prism.Ioc;

namespace NodeViewViewer
{
    public class ViewerService 
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region SingleTone
        private static readonly Lazy<ViewerService> Lazy = new Lazy<ViewerService>(() => new ViewerService());
        public static ViewerService Instance => Lazy.Value;
        #endregion

        private IntRect _screenSize = new IntRect(0, 0, 1920, 1080);
        private NodeViewProcessManagerClient _processManagerClient;

        private readonly Dictionary<string, DataWallContent> _contentDictionary = new Dictionary<string, DataWallContent>();
        private readonly List<DataWallWindow> _screenWindowList = new List<DataWallWindow>();
        private readonly List<DataWallPreset> _presetList = new List<DataWallPreset>();
        private readonly List<ViewerLayout> _viewerLayoutList = new List<ViewerLayout>();
        private ViewerSequnce _viewerSequnce = null;
        protected WebServer _webServer = null;
        protected volatile bool _isStarted = false;

        public readonly List<string> InitErrorMessages = new List<string>();
        public bool HasInitError => InitErrorMessages.Count > 0;

        public IContainerProvider ContainerProvider { get; protected set; }
        public string ConfigFilename { get; set; }
        public Configuration Config { get; protected set; }
        public bool IsNodeVewApp { get; protected set; } = true;
        public bool IsTestMode { get; protected set; } = false;
        public bool IsVerbose { get; protected set; } = false;
        public string ServiceIp { get; protected set; }
        public int ServicePort { get; protected set; }
        public int ProcessManagerPort { get; protected set; }
        public string ServiceName { get; protected set; }
        public string ServiceBaseUrl { get; protected set; }
        public string CachesFolder { get; protected set; } = "";
        public string ConfigsFolder { get; protected set; } = "";
        public bool IsStarted
        {
            get => _isStarted;
            protected set => _isStarted = value;
        }

        public IntSize ScreenSize => _screenSize.Size;
        public IntPoint ScreenMatrix { get; set; } = new IntPoint(1, 1);
        public string ConfigsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string ScreenUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string ContentsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string PresetsUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public string DevicesUpdated { get; set; } = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        public bool Initialize(IContainerProvider containerProvider, string configFilename = "")
        {
            ConfigsFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewConfigs");
            Directory.CreateDirectory(ConfigsFolder);
            CachesFolder = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches");
            Directory.CreateDirectory(CachesFolder);

            ContainerProvider = containerProvider;
            //_pluginManager = new PluginManager(containerProvider, this);
            if (string.IsNullOrWhiteSpace(configFilename))
            {
                configFilename = AppUtils.GetAppName() + ".config.xml";
            }

            if (!File.Exists(configFilename))
            {
                configFilename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
                if (!File.Exists(configFilename))
                {
                    Logger.Error("Cannot find config file.");
                    return false;
                }
            }

            ConfigFilename = configFilename;
            Config = Configuration.Load(configFilename);
            if (Config == null)
            {
                Logger.Error("Cannot load config file.");
                return false;
            }

            IsTestMode = Config.GetValue("isTestMode", IsTestMode);
            IsVerbose = IsTestMode || Config.GetValue("isVerbose", IsVerbose);
            ServiceIp = Config.GetValue("service.ip", "127.0.0.1");
            ServicePort = Config.GetValue("service.port", 20201);
            ServiceName = Config.GetValue("service.name", AppUtils.GetAppName());
            ProcessManagerPort = Config.GetValue("processManager.port", 20102);
            ServiceBaseUrl = $"http://{ServiceIp}:{ServicePort}";
            _webServer = new WebServer(ServicePort);
            //if (IsNodeVewApp)
            //{
            //    string centerIp = Config.GetValue("nodeViewCenter.ip", "127.0.0.1");
            //    int centerPort = Config.GetValue("nodeViewCenter.port", 20101);
            //    _centerClient = new NodeViewCenterClient(centerIp, centerPort, ServiceIp, ServicePort, ProcessManagerPort, ServiceName);
            //}

            //_repository = ContainerProvider.Resolve<IStationRepository>();

            if (!OnInitializing(Config))
            {
                Logger.Error("Cannot initializing.");
                return false;
            }

            //LoadDataFromRepository();

            //InitDefaultPlugins();
            //if (!InitPlugins(Config))
            //{
            //    Logger.Error("Cannot init plugins.");
            //    return false;
            //}

            OnInitialized();
            return true;
        }

        protected bool OnInitializing(Configuration config)
        {
            IntSize screenSize = config.GetValue("screen.size", new IntSize(0, 0));
            _screenSize = new IntRect(0, 0, screenSize.Width, screenSize.Height);
            ScreenMatrix = config.GetValue("screen.matrix", new IntPoint(1, 1));
            
            int processManagerPort = Config.GetValue("processManager.port", 20102);
            _processManagerClient = new NodeViewProcessManagerClient("127.0.0.1", processManagerPort);
            return true;
        }
        protected void OnInitialized()
        {
            ConfigsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            DevicesUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);

            string stopApiUrl = $"http://127.0.0.1:{ServicePort}/api/v1/service/shutdown";
            Task.Factory.StartNew(() =>
            {
                _processManagerClient.RegisterApp(ProcessType.Application, AppUtils.GetAppName(), ConfigFilename, stopApiUrl);
            });
        }
        //protected void InitDefaultPlugins()
        //{
        //    base.InitDefaultPlugins();

        //    var dataWallController = new NodeViewViewerController("viewer");
        //    dataWallController.Init(null, this);
        //    dataWallController.IsTestMode = IsTestMode;
        //    dataWallController.IsVerbose = IsVerbose;
        //    _pluginManager.AddPlugin(dataWallController);
        //}
        //protected bool InitPlugins(Configuration config)
        //{
        //    if (!base.InitPlugins(config))
        //    {
        //        return false;
        //    }

        //    return true;
        //}

        //Todo: Get/SaveConfiguration은 프로세스메니져로 이전 해야 한다.
        public Configuration GetConfiguration()
        {
            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewViewer.config.xml");
            return Configuration.Load(settingFile);
        }
        public bool SaveConfiguration(Configuration configuration)
        {
            if (configuration == null) return false;

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewViewer.config.xml");
            File.Copy(settingFile, settingFile + ".backup", true);
            bool res = Configuration.Save(settingFile, configuration);
            ConfigsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            return res;
        }
        public bool Start()
        {
            if (!IsStarted)

            {
                try
                {
                    if (!OnStart()) return false;
                    

                    //if (IsNodeVewApp)
                    //{
                    //    if (!_centerClient?.Start() ?? true)
                    //    {
                    //        Logger.Error("Cannot start nodeview center client.");
                    //        return false;
                    //    }
                    //}
                    if (!_webServer?.Start() ?? false)
                    {
                        Logger.Error("Cannot start web server.");
                        return false;
                    }

                    //startAction
                   
                    //OnStarted();
                    IsStarted = true;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "Start:");
                }
            }
            return IsStarted;
        }
        protected bool OnStart()
        {
            LoadContents();
            LoadPresets();
            LoadLayouts();
            LoadSequence();

            ViewerManager.Instance.IsTestMode = IsTestMode;
            if (!ViewerManager.Instance.Start())
            {

                string errorMsg = $"Cannot create a screen application.";
                Logger.Error(errorMsg);
                InitErrorMessages.Add(errorMsg);
                return false;
            }
            ScreenUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
            return true;
        }
        protected void OnStop()
        {
            _contentDictionary.Clear();
            _screenWindowList.Clear();
            _viewerLayoutList.Clear();
            _presetList.Clear();
            InitErrorMessages.Clear();

            ScreenUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }
        private void LoadContents()
        {
            string contentsFile = Path.Combine(ConfigsFolder, "contents.xml");
            if (!File.Exists(contentsFile))
            {
                Logger.Error($"Cannot find contents file[{contentsFile}].");
                return;
            }

            try
            {
                _contentDictionary.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(contentsFile);
                XmlNodeList contentNodeList = xmlDocument.SelectNodes("/Contents/Content");
                if (contentNodeList != null)
                {
                    foreach (XmlNode contentNode in contentNodeList)
                    {
                        DataWallContent nodeViewContent = DataWallContent.CreateFrom(contentNode);
                        ContentItem contentItem = ContentItem.CreateFrom(contentNode);
                        if (nodeViewContent != null)
                        {
                            _contentDictionary[nodeViewContent.Id] = nodeViewContent;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load contents file.");
            }
            ContentsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }
        private void SaveContents()
        {
            string contentsFile = Path.Combine(ConfigsFolder, "contents.xml");
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(contentsFile))
                {
                    xmlWriter.WriteStartElement("Contents");
                    foreach (var content in _contentDictionary.Values.ToArray())
                    {
                        content.WriteXml(xmlWriter, "Content");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SaveContents:");
            }
            ContentsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }
        private void LoadPresets()
        {
            string presetsFile = Path.Combine(ConfigsFolder, "presets.xml");
            if (!File.Exists(presetsFile))
            {
                //Logger.Error($"Cannot find presets file[{presetsFile}].");
                return;
            }

            try
            {
                _presetList.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(presetsFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Presets/Preset");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        DataWallPreset preset = DataWallPreset.CreateFrom(node);
                        if (preset != null)
                        {
                            _presetList.Add(preset);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load presets file.");
            }
            PresetsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }
        private void SavePresets()
        {
            string presetsFile = Path.Combine(ConfigsFolder, "presets.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(presetsFile))
                {
                    xmlWriter.WriteStartElement("Presets");
                    foreach (var preset in GetPresets())
                    {
                        preset.WriteXml(xmlWriter, "Preset");
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SavePresets:");
            }
            PresetsUpdated = StringUtils.GetDateTimeMillisecondString(CachedDateTime.Now);
        }
        private void LoadLayouts()
        {
            string layoutFile = Path.Combine(ConfigsFolder, "layouts.xml");
            if (!File.Exists(layoutFile))
            {
                //Logger.Error($"Cannot find presets file[{presetsFile}].");
                return;
            }

            try
            {
                _viewerLayoutList.Clear();
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(layoutFile);
                XmlNodeList nodeList = xmlDocument.SelectNodes("/Layouts/Layout");
                if (nodeList != null)
                {
                    foreach (XmlNode node in nodeList)
                    {
                        ViewerLayout layout = ViewerLayout.CreateFrom(node);
                        if (layout != null)
                        {
                            _viewerLayoutList.Add(layout);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load layouts file.");
            }
        }
        private void LoadSequence()
        {
            string viewerSequnceFile = Path.Combine(ConfigsFolder, "sequence.xml");
            if (!File.Exists(viewerSequnceFile))
            {
                return;
            }

            try
            {
                _viewerSequnce = null;
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(viewerSequnceFile);
                XmlNode node = xmlDocument.SelectSingleNode("Sequence");
                if (node != null)
                {
                    ViewerSequnce sequence = ViewerSequnce.CreateFrom(node);
                    if (sequence != null)
                    {
                        _viewerSequnce = sequence;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Load layouts file.");
            }
        }
        private void SaveSequence()
        {
            string sequenceFile = Path.Combine(ConfigsFolder, "sequence.xml");
            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(sequenceFile))
                {
                    _viewerSequnce.WriteXml(xmlWriter);
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SavePresets:");
            }
        }

        public void UpdateSequence(IViewerSequenceSet sequnceSet)
        {
            _viewerSequnce.UpdateSequence(sequnceSet);
            SaveSequence();
        }
        public void DeleteLayouts(string[] deleteIds)
        {
            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string deleteId in deleteIds)
            {
                idDictionary[deleteId] = deleteId;
            }

            int deletedCount = 0;
            ViewerLayout[] layouts = GetLayouts();
            foreach (var layout in layouts)
            {
                if (idDictionary.TryGetValue(layout.Id, out var outValue))
                {
                    _viewerLayoutList.Remove(layout);
                    deletedCount++;
                }
            }

            if (deletedCount > 0)
            {
                SaveLayouts();
            }
        }
        private void SaveLayouts()
        {
            string presetsFile = Path.Combine(ConfigsFolder, "layouts.xml");

            try
            {
                using (var xmlWriter = XmlUtils.CreateXmlWriter(presetsFile))
                {
                    xmlWriter.WriteStartElement("Layouts");
                    foreach (var viewerLayout in GetLayouts())
                    {
                        var layout = new ViewerLayout(viewerLayout.Id, viewerLayout.Name, viewerLayout.IsDefault, viewerLayout.Cells);
                        layout.WriteXml(xmlWriter);
                    }
                    xmlWriter.WriteEndElement();
                    xmlWriter.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, $"SavePresets:");
            }
        }
        public DataWallContent[] GetContents()
        {
            return _contentDictionary.Values.ToArray();
        }
        public ViewerLayout[] GetLayouts()
        {
            return _viewerLayoutList.OrderBy(layout => layout.Id).ToArray();
        }
        public DataWallWindow[] GetWindows()
        {
            return _screenWindowList.ToArray();
        }
        public ViewerSequnce GetSequnce()
        {
            return _viewerSequnce;
        }
        public ViewerLayout[] AddLayouts(ViewerLayout viewerLayout)
        {
            if (viewerLayout == null)
            {
                return _viewerLayoutList.ToArray();
            }

            if (!_viewerLayoutList.Any(layout => layout.Id.Equals(viewerLayout.Id)))
            {
                _viewerLayoutList.Add(viewerLayout);
                SaveLayouts();
            }

            return _viewerLayoutList.ToArray();
        }
        public void AddPreset(DataWallPreset preset)
        {
            _presetList.Add(preset);
            SavePresets();
        }
        public DataWallPreset[] GetPresets()
        {
            return _presetList.ToArray();
        }
        public void DeletePresets(string[] deleteIds)
        {
            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string deleteId in deleteIds)
            {
                idDictionary[deleteId] = deleteId;
            }

            int deletedCount = 0;
            DataWallPreset[] presets = GetPresets();
            foreach (var preset in presets)
            {
                if (idDictionary.TryGetValue(preset.Id, out var outValue))
                {
                    _presetList.Remove(preset);
                    deletedCount++;
                }
            }

            if (deletedCount > 0)
            {
                SavePresets();
            }
        }
        public void SaveSequence(IEnumerable<IViewerSequenceSet> sequenceSets)
        {
            if (sequenceSets == null)
            {
                DeleteSequence();
                return;
            }
            if (_viewerSequnce != null)
            {
                var id = _viewerSequnce.Id;
                _viewerSequnce = null;
                _viewerSequnce = new ViewerSequnce(id, seqeunceSets: sequenceSets);
            }
            else
            {
                _viewerSequnce = new ViewerSequnce(Uuid.NewUuid, seqeunceSets: sequenceSets);
            }
            SaveSequence();
        }
        private void DeleteSequence()
        {
            string sequenceFile = Path.Combine(ConfigsFolder, "sequence.xml");
            try
            {
                File.Delete(sequenceFile);
            }
            catch (Exception e)
            {
                Logger.Error(e, "DeleteSequence Error.");
            }
        }
        public void Refresh()
        {
            try
            {
                ViewerManager.Instance.Stop();
                Thread.Sleep(500);
                ViewerManager.Instance.Start();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Refresh:");
            }
        }
        public void Shutdown()
        {
            Stop();
            Application.Current.Shutdown();
        }
        public bool DeleteContents(string[] ids)
        {
            int updateCount = 0;
            foreach (var id in ids)
            {
                if (_contentDictionary.TryGetValue(id, out var content))
                {
                    updateCount++;
                    _contentDictionary.Remove(id);
                }
            }

            if (updateCount > 0)
            {
                SaveContents();
            }
            return true;
        }
        public bool UpdateContent(DataWallContent updateContent)
        {
            if (_contentDictionary.TryGetValue(updateContent.Id, out var content))
            {
                _contentDictionary[updateContent.Id] = updateContent;
                SaveContents();
                return true;
            }

            return false;
        }
        public bool CreateContents(DataWallContent[] contents)
        {
            int updateCount = 0;
            foreach (var content in contents)
            {
                if (string.IsNullOrWhiteSpace(content?.Id))
                {
                    continue;
                }

                _contentDictionary[content.Id] = content;
                updateCount++;
            }
            if (updateCount > 0)
            {
                SaveContents();
            }
            return true;
        }
        public void RequestShutdown(string requester)
        {
            ThreadAction.PostOnUiThread(Shutdown);
        }
        public void Stop()
        {
            try
            {
                _webServer?.Stop();
                OnStop();
            }
            catch (Exception e)
            {
                Logger.Error(e, "Stop:");
            }
            IsStarted = false;
        }
    }
}
