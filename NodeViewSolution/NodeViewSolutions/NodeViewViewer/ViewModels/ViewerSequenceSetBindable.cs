﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NodeView.DataWalls;
using NodeView.ViewModels;
using NodeViewViewer.Models;

namespace NodeViewViewer.ViewModels
{
    public class ViewerSequenceSetBindable : SelectableViewModel, IViewerSequenceSet
    {
        private int _interval = 10;
        private IDataWallPreset _preset;
        public string Id { get; }
        public string Name { get; }
        public List<int> IntervalList { get; set; } = new() { 10, 20, 30, 40, 50, 60 };

        public int Interval
        {
            get => _interval;
            set
            {
                if (_interval != value)
                {
                    SetProperty(ref _interval, value);
                    //todo : 더좋은방법있는지 고민중
                    ViewerService.Instance.UpdateSequence(new ViewerSequenceSet(this));
                }
                
            }
        }

        public IDataWallPreset Preset
        {
            get => _preset;
            set => SetProperty(ref _preset, value);

        }
        public ViewerSequenceSetBindable(IDataWallPreset preset, int interval = 30)
        {
            _preset = preset;
            Id = preset.Id;
            Name = preset.Name;
            _interval = interval;
        }
        public JToken ToJson()
        {
            throw new NotImplementedException();
        }

        public bool LoadFrom(JObject json)
        {
            throw new NotImplementedException();
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            throw new NotImplementedException();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            throw new NotImplementedException();
        }
    }
}
