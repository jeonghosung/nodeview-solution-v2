﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Utils;
using NodeView.ViewModels;
using NodeViewViewer.Models;

namespace NodeViewViewer.ViewModels
{
    public class ViewerSequnceBindable : SelectableViewModel, IViewerSequnce
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        
        private string _name;
        private List<IViewerSequenceSet> _seqeunceSets;
        public string Id { get; }
        public IEnumerable<IViewerSequenceSet> SequenceSets => _seqeunceSets;
        public string Name
        {
            get=>_name;
            set => SetProperty(ref _name, value);

        }

        public ViewerSequnceBindable(string id, string name = "", IEnumerable<IViewerSequenceSet> seqeunceSets = null)
        {
            Id = id;
            _name = string.IsNullOrWhiteSpace(name) ? name : id;
            if (seqeunceSets != null)
            {
                foreach (var viewerSequenceSet in seqeunceSets)
                {
                    _seqeunceSets.Add(new ViewerSequenceSetBindable(viewerSequenceSet.Preset, viewerSequenceSet.Interval));
                }
            }
        }
        public ViewerSequnceBindable(IViewerSequnce sequnce)
        {
            Id = sequnce.Id;
            _name = sequnce.Name;
            if (sequnce.SequenceSets != null)
            {
                foreach (var sequnceSequenceSet in sequnce.SequenceSets)
                {
                    _seqeunceSets.Add(new ViewerSequenceSetBindable(sequnceSequenceSet.Preset, sequnceSequenceSet.Interval));
                }
            }
        }


        public JToken ToJson()
        {
            var jObject = new JObject();
            jObject["id"] = Id;
            jObject["name"] = Name;
            foreach (var preset in SequenceSets)
            {
                preset.ToJson();
            }
            return jObject;
        }

        public bool LoadFrom(JObject json)
        {
            
            return false;
        }

        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Sequence";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);

            if (SequenceSets != null)
            {
                foreach (var viewerSequenceSet in SequenceSets)
                {
                    viewerSequenceSet.WriteXml(xmlWriter, "SequenceSet");
                }
            }

            xmlWriter.WriteEndElement();
        }

        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);

                ViewerSequenceSet seqeunceSets = null;
                List<IViewerSequenceSet> sequenceSets = new List<IViewerSequenceSet>();
                XmlNodeList sequenceSetList = xmlNode.SelectNodes("SequenceSet");
                if (sequenceSetList != null)
                {
                    foreach (XmlNode sequenceSet in sequenceSetList)
                    {
                        var window = ViewerSequenceSet.CreateFrom(sequenceSet);
                        if (window != null)
                        {
                            sequenceSets.Add(window);
                        }
                    }
                }
                _seqeunceSets = sequenceSets;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
    }
}
