﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeView.ViewModels;
using NodeViewViewer.Models;
using Prism.Commands;
using Prism.Mvvm;

namespace NodeViewViewer.ViewModels
{
    public class ViewerLayoutCellBindable : SelectableViewModel, IViewerLayoutCell
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private string _id = Uuid.NewUuid;
        private string _name = "";
        private IntRect _rect;
        private IDataWallWindow _dataWallWindow = null;

        public string Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public string Name
        {
            get=> _name;
            set => SetProperty(ref _name, value);
        }

        public IntRect Rect
        {
            get => _rect;
            set => SetProperty(ref _rect, value);
        }
        public DelegateCommand<DragEventArgs> ContentDropCommad { get; set; }
        public IDataWallWindow DataWallWindow
        {
            get=> _dataWallWindow;
            set=> SetProperty(ref _dataWallWindow, value);
        }

        public ViewerLayoutCellBindable(IViewerLayoutCell layoutCell)
        {
            _id = layoutCell.Id;
            _name = layoutCell.Name;
            _rect = layoutCell.Rect;
            _dataWallWindow = layoutCell.DataWallWindow == null ? new DataWallWindow("", screenRect: layoutCell.Rect) : layoutCell.DataWallWindow;
            ContentDropCommad = new DelegateCommand<DragEventArgs>(ExecuteContentDropCommad);
        }
        public ViewerLayoutCellBindable(IntRect rect)
        {
            _rect = rect;
        }
        public JToken ToJson()
        {
            var jObject = new JObject
            {
                ["id"] = Id,
                ["name"] = Name,
                ["rect"] = Rect.ToString()
            };
            return jObject;
        }
        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                Name = JsonUtils.GetValue(json, "name", Name);
                Rect = JsonUtils.GetValue(json, "rect", Rect);
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Layout";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("rect", $"{Rect}");
            xmlWriter.WriteEndElement();
        }
        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);
                Rect = XmlUtils.GetAttrValue(xmlNode, "rect", new IntRect(Rect));
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        
        private void ExecuteContentDropCommad(DragEventArgs args)
        {
            if (args == null)
            {
                return;
            }
            ContentItem content = args.Data.GetData(typeof(ContentItem)) as ContentItem;
            var datawallWindow = new DataWallWindow(content.Id,
                    screenRect: Rect,
                    content: DataWallContent.CreateFrom(content));
            DataWallWindow = datawallWindow;
        }
    }
}
