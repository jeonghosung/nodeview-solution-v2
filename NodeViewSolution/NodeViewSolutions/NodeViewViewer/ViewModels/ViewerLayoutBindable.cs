﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.Drawing;
using NodeView.Utils;
using NodeView.ViewModels;
using NodeViewViewer.Models;

namespace NodeViewViewer.ViewModels
{
    public class ViewerLayoutBindable : SelectableViewModel, IViewerLayout
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private bool _isDefault = false;
        private IEnumerable<IViewerLayoutCell> _cells;
        public string Id { get; }
        public string Name { get; set; }
        public IntRect ScreenSize { get; set; } = new IntRect(0, 0, 1920, 1080);
        public bool IsDefault => _isDefault;
        public IEnumerable<IViewerLayoutCell> Cells => _cells;

        public ViewerLayoutBindable(IViewerLayout viewerLayout)
        {
            Id = viewerLayout.Id;
            Name = viewerLayout.Name;
            _isDefault = viewerLayout.IsDefault;
            _cells = viewerLayout.Cells;
        }

        public ViewerLayoutBindable(string id, string name = "", bool isDefault = false, IEnumerable<IViewerLayoutCell> cells = null)
        {
            Id = id;
            Name = name;
            _isDefault = isDefault;
            _cells = cells;
        }

        public JToken ToJson()
        {
            var jObject = new JObject();
            jObject["id"] = Id;
            jObject["name"] = Name;
            jObject["isDefalut"] = IsDefault;
            JArray layoutCellArray = new JArray();
            foreach (var cell in Cells)
            {
                layoutCellArray.Add(cell.ToJson());
            }
            jObject["Layout"] = layoutCellArray;

            return jObject;
        }
        public static ViewerLayout CreateFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return null;
            }
            try
            {
                string id = XmlUtils.GetStringAttrValue(xmlNode, "id");
                if (string.IsNullOrWhiteSpace(id))
                {
                    return null;
                }
                string name = XmlUtils.GetStringAttrValue(xmlNode, "name", id);
                bool isDefault = XmlUtils.GetBoolAttrValue(xmlNode, "isDefault", false);
                var viewerLayout = new ViewerLayout(id, name, isDefault);
                if (viewerLayout.LoadFrom(xmlNode))
                {
                    return viewerLayout;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateFrom:");
            }

            return null;
        }
        public bool LoadFrom(JObject json)
        {
            if (json == null)
            {
                return false;
            }
            try
            {
                Name = JsonUtils.GetStringValue(json, "name", Name);

                List<IViewerLayoutCell> layoutCellList = new List<IViewerLayoutCell>();
                JArray layoutCellJArray = JsonUtils.GetValue<JArray>(json, "LayoutCell", null);
                if (layoutCellJArray != null)
                {
                    foreach (JToken layoutCellToken in layoutCellJArray)
                    {
                        JObject layoutJsonObject = layoutCellToken.Value<JObject>();
                        ViewerLayoutCell window = ViewerLayoutCell.CreateFrom(layoutJsonObject);
                        if (window != null)
                        {
                            layoutCellList.Add(window);
                        }
                    }
                }

                _cells = layoutCellList;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
        public void WriteXml(XmlWriter xmlWriter, string tagName = "")
        {
            if (string.IsNullOrWhiteSpace(tagName))
            {
                tagName = "Layout";
            }
            xmlWriter.WriteStartElement(tagName);

            xmlWriter.WriteAttributeString("id", Id);
            xmlWriter.WriteAttributeString("name", Name);
            xmlWriter.WriteAttributeString("isDefault", _isDefault.ToString());

            if (Cells != null)
            {
                foreach (var cell in Cells)
                {
                    cell.WriteXml(xmlWriter, "LayoutCell");
                }
            }

            xmlWriter.WriteEndElement();
        }
        public bool LoadFrom(XmlNode xmlNode)
        {
            if (xmlNode == null)
            {
                return false;
            }
            try
            {
                Name = XmlUtils.GetAttrValue(xmlNode, "name", Name);

                List<IViewerLayoutCell> layoutCellList = new List<IViewerLayoutCell>();
                XmlNodeList layoutCellNodeList = xmlNode.SelectNodes("LayoutCell");
                if (layoutCellNodeList != null)
                {
                    foreach (XmlNode layoutCellNode in layoutCellNodeList)
                    {
                        var layoutCell = ViewerLayoutCell.CreateFrom(layoutCellNode);
                        if (layoutCell != null)
                        {
                            layoutCellList.Add(layoutCell);
                        }
                    }
                }

                _cells = layoutCellList;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "LoadFrom:");
            }

            return false;
        }
    }
}
