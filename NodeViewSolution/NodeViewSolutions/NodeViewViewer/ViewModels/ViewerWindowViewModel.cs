﻿using System;
using NLog;
using NodeView.Apps;
using NodeView.CommonWindows.ViewModels;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Protocols.NodeViews;
using NodeView.Tasks;
using NodeView.ViewModels;
using NodeViewViewer.Models;
using Prism.Commands;
using Prism.Ioc;
using Prism.Services.Dialogs;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Microsoft.VisualBasic.Logging;
using NodeViewWindowCore.Views;
using NodeView.Models;
using System.Configuration;
using System.IO;
using NodeViewViewer.Dialogs;
using Configuration = NodeView.DataModels.Configuration;

namespace NodeViewViewer.ViewModels
{
    public class ViewerWindowViewModel : WindowChromeViewModelBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private bool _isTestMode = false;
        private bool _isVerbose = false;
        private IDialogService _dialogService = null;
        private string _layoutId = string.Empty;
        private NodeViewProcessManagerClient _processManagerClient;
        private ContentTreeViewFolder _wallContentFolder = new("");
        private ObservableCollection<ViewerLayoutBindable> _layouts = new();

        private ObservableCollection<ViewerLayoutBindable> _defaultLayouts = new();
        private IntSize _screenSize = new(1920, 1080);
        private int _layoutColumn = 6;
        private int _layoutControlWidth = 600;
        public OsdConfig DefaultOsdConfig { get; private set; } = new OsdConfig();
        private ContentItem _selectedContent = null;
        private ViewerLayoutCellBindable _selectedViewerLayoutBindable = null;
        private ObservableCollection<DataWallWindow> _expensionContent = new();
        private DataWallPresetBindable _selectedDataWallPreset = null;
        private ViewerSequenceSetBindable _selectedSequenceSet = null;
        private Timer _playSequenceTimer = null;
        private int _currentSequenceIndex = 0;
        private int _screenRow = 1;
        private int _screenColumnSpan = 16;
        private int _screenRowSpan = 16;
        private bool _isFullScreen = false;
        private bool _isContentExpensionMode = false;
        private bool _isSequenceMode = false;
        private CacheManager _cacheManager;

        public ContentTreeViewFolder WallContentFolder
        {
            get => _wallContentFolder;
            set => SetProperty(ref _wallContentFolder, value);
        }
        public ObservableCollection<ViewerLayoutBindable> Layouts
        {
            get => _layouts;
            set => SetProperty(ref _layouts, value);
        }

        public ObservableCollection<ViewerLayoutBindable> DefaultLayouts
        {
            get => _defaultLayouts;
            set => SetProperty(ref _defaultLayouts, value);
        }
        public IntSize ScreenSize
        {
            get => _screenSize;
            set => SetProperty(ref _screenSize, value);
        }
        public int LayoutColumn
        {
            get => _layoutColumn;
            set => SetProperty(ref _layoutColumn, value);
        }
        public int LayoutControlWidth
        {
            get => _layoutControlWidth;
            set => SetProperty(ref _layoutControlWidth, value);
        }
        public int ScreenRow
        {
            get => _screenRow;
            set => SetProperty(ref _screenRow, value);
        }
        public int ScreenColumnSpan
        {
            get => _screenColumnSpan;
            set => SetProperty(ref _screenColumnSpan, value);
        }
        public int ScreenRowSpan
        {
            get => _screenRowSpan;
            set => SetProperty(ref _screenRowSpan, value);
        }
        public bool IsFullScreen
        {
            get => _isFullScreen;
            set => SetProperty(ref _isFullScreen, value);
        }
        public bool IsContentExpensionMode
        {
            get => _isContentExpensionMode;
            set => SetProperty(ref _isContentExpensionMode, value);
        }

        public bool IsSequenceMode
        {
            get => _isSequenceMode;
            set => SetProperty(ref _isSequenceMode, value);
        }
        public ObservableCollection<DataWallWindow> ExpensionContent
        {
            get => _expensionContent;
            set => SetProperty(ref _expensionContent, value);
        }
        public ObservableCollection<ContentItem> WallContents { get; } = new();
        public ObservableCollection<DataWallPresetBindable> WallPresets { get; } = new();
        public ObservableCollection<ViewerLayoutCellBindable> LayoutCells { get; set; } = new();
        public ObservableCollection<ViewerSequenceSetBindable> SequenceSets { get; set; } = new();

        public DelegateCommand ChangePasswordCommand { get; set; }
        public DelegateCommand OpenLayoutSettingDialogCommand { get; set; }
        public DelegateCommand OpenContentsSettingDialogCommand { get; set; }
        public DelegateCommand<string> ToggleWallContentSelectionCommand { get; set; }
        public DelegateCommand<string> SelectLayoutCommand { get; set; }
        public DelegateCommand<ViewerLayoutCellBindable> SelectCellCommand { get; set; }
        public DelegateCommand<ViewerLayoutCellBindable> MouseDoubleClickCellCommand { get; set; }
        public DelegateCommand<object> MouseDoubleClickExpensionContentCommand { get; set; }
        public DelegateCommand FullScreenCommand { get; set; }
        public DelegateCommand ClearScreenCommand { get; set; }
        public DelegateCommand<KeyEventArgs> KeyUpEventCommand { get; set; }
        public DelegateCommand WindowClosingCommand { get; set; }
        public DelegateCommand AddPresetCommand { get; set; }
        public DelegateCommand DeletePresetCommand { get; set; }
        public DelegateCommand<string> RunWallPresetCommand { get; set; }
        public DelegateCommand<string> TogglePresetSelectionCommand { get; set; }
        public DelegateCommand<object> AddSequnceSetCommand { get; set; }
        public DelegateCommand RemoveSequnceSetCommand { get; set; }
        public DelegateCommand SequnceIndexUpCommand { get; set; }
        public DelegateCommand SequnceIndexDownCommand { get; set; }
        public DelegateCommand<string> ToggleSequenceSelectionCommand { get; set; }
        public DelegateCommand StartSequenceCommand { get; set; }
        public DelegateCommand StopSequenceCommand { get; set; }
        public DelegateCommand<ContentItem> OpenPreviewWindowCommand { get; set; }

        public ViewerWindowViewModel(IDialogService dialogService, IContainerProvider container)
        {
            _dialogService = dialogService;
            WindowState = WindowState.Maximized;

            var appConfig = container.Resolve<Configuration>("appConfig");
            DefaultOsdConfig.IsShow = appConfig.GetValue("osd.isShow", DefaultOsdConfig.IsShow);
            DefaultOsdConfig.Alignment = appConfig.GetValue("osd.alignment", DefaultOsdConfig.Alignment);
            DefaultOsdConfig.Offset = appConfig.GetValue("osd.offset", DefaultOsdConfig.Offset);
            DefaultOsdConfig.Opacity = appConfig.GetValue("osd.opacity", DefaultOsdConfig.Opacity);
            DefaultOsdConfig.FontSize = appConfig.GetValue("osd.fontSize", DefaultOsdConfig.FontSize);
            DefaultOsdConfig.Color = appConfig.GetValue("osd.color", DefaultOsdConfig.Color);

            int processManagerPort = appConfig.GetValue("processManager.port", 20102);
            _processManagerClient = new NodeViewProcessManagerClient("127.0.0.1", processManagerPort);

            _isTestMode = appConfig.GetValue("isTestMode", false);
            _isVerbose = appConfig.GetValue("isVerbose", false);

            Task.Factory.StartNew(() =>
            {
                _processManagerClient.RegisterApp(ProcessType.Application, AppUtils.GetAppName(), "", "");
            });
            _playSequenceTimer = new Timer(OnPlaySequenceTimer);

           
            LoadCacheData();
            InitCommand();
            LoadContents();
            LoadPresets();
            LoadLayouts();
            LoadSequence();
        }
        private void LoadCacheData()
        {
            _cacheManager = new CacheManager(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewCaches", "cache.json"));
            _layoutId = _cacheManager.GetValue("lastLayout", "1X1");
        }
        private void InitCommand()
        {
            ChangePasswordCommand = new DelegateCommand(ExecuteChangePasswordCommand);
            OpenLayoutSettingDialogCommand = new DelegateCommand(ExecuteOpenLayoutSettingDialogCommand);
            OpenContentsSettingDialogCommand = new DelegateCommand(ExecuteOpenContentsDialogCommand);
            ToggleWallContentSelectionCommand = new DelegateCommand<string>(ExecuteToggleWallContentSelectionCommand);
            SelectLayoutCommand = new DelegateCommand<string>(ExecuteSelectLayoutCommand);
            SelectCellCommand = new DelegateCommand<ViewerLayoutCellBindable>(ExecuteSelectCellCommand);
          
            MouseDoubleClickCellCommand = new DelegateCommand<ViewerLayoutCellBindable>(ExecuteMouseDoubleClickCellCommand);
            MouseDoubleClickExpensionContentCommand =
                new DelegateCommand<object>(ExecuteMouseDoubleClickExpensionContentCommand);
            FullScreenCommand = new DelegateCommand(ExecuteFullScreenCommand);
            ClearScreenCommand = new DelegateCommand(ExecuteClearScreenCommand);
            KeyUpEventCommand = new DelegateCommand<KeyEventArgs>(ExecuteKeyUpCommand);
            WindowClosingCommand = new DelegateCommand(ExecuteWindowClosingCommand);
            AddPresetCommand = new DelegateCommand(ExecuteAddPresetCommand);
            DeletePresetCommand = new DelegateCommand(ExecuteDeletePresetCommand);
            RunWallPresetCommand = new DelegateCommand<string>(ExecuteRunWallPresetCommand);
            TogglePresetSelectionCommand = new DelegateCommand<string>(ExecuteTogglePresetSelectionCommand);
            AddSequnceSetCommand = new DelegateCommand<object>(ExecuteAddSequnceSetCommand);
            RemoveSequnceSetCommand = new DelegateCommand(ExecuteRemoveSequnceSetCommand);
            SequnceIndexUpCommand = new DelegateCommand(ExecuteSequnceIndexUpCommand);
            SequnceIndexDownCommand = new DelegateCommand(ExecuteSequnceIndexDownCommand);
            ToggleSequenceSelectionCommand = new DelegateCommand<string>(ExecuteToggleSequenceSelectionCommand);
            StartSequenceCommand = new DelegateCommand(ExecuteStartSequenceCommand);
            StopSequenceCommand = new DelegateCommand(ExecuteStopSequenceCommand);
            OpenPreviewWindowCommand = new DelegateCommand<ContentItem>(ExecuteOpenPreviewWindowCommand);
        }
        
        private bool CheckSequenceMode()
        {
            if (_isSequenceMode)
            {
                var parameters = new DialogParameters();
                parameters.Add("title", "Sequence 모드 확인");
                parameters.Add("message", "Sequence 모드 종료 후 시도하세요.\r\nSequence 모드를 종료하시겠습니까?");
                _dialogService.ShowDialog("confirm", parameters, r =>
                {
                    if (r.Result == ButtonResult.OK)
                    {
                        IsSequenceMode = false;
                        ExecuteStopSequenceCommand();
                    }
                });
                return true;
            }

            return false;
        }
        private void RemoveExpensionContent()
        {
            ExpensionContent.Clear();
            IsContentExpensionMode = false;
        }
        private void SetExpensionContent(DataWallWindow dataWallContent)
        {
            if (ExpensionContent.Count > 0)
            {
                ExpensionContent.Clear();
            }
            ExpensionContent.Add(dataWallContent);
            IsContentExpensionMode = true;
        }
        private void LoadLayouts()
        {
            ThreadAction.PostOnUiThread(() =>
            {
                Layouts.Clear();
                DefaultLayouts.Clear();
                foreach (var viewerLayout in ViewerService.Instance.GetLayouts())
                {
                    Layouts.Add(new ViewerLayoutBindable(viewerLayout));
                    if (viewerLayout.IsDefault)
                    {
                        DefaultLayouts.Add(new ViewerLayoutBindable(viewerLayout));
                    }
                }

                LayoutColumn = Layouts.Count < 6 ? Layouts.Count % 6 : 6;
                LayoutControlWidth = LayoutColumn * 100;
                if (!string.IsNullOrWhiteSpace(_layoutId))
                {
                    var lastLayout = Layouts.FirstOrDefault(layout =>
                        layout.Id.Equals(_layoutId, StringComparison.OrdinalIgnoreCase));
                    if (lastLayout != null)
                    {
                        foreach (var selectedLayoutCell in lastLayout.Cells)
                        {
                            LayoutCells.Add(new ViewerLayoutCellBindable(selectedLayoutCell));
                        }
                    }
                }
                
            });
        }
        private void LoadPresets()
        {
            ThreadAction.PostOnUiThread(() =>
            {
                WallPresets.Clear();
                foreach (var dataWallPreset in ViewerService.Instance.GetPresets())
                {
                    WallPresets.Add(new DataWallPresetBindable(dataWallPreset));
                }
            });
        }
        private void LoadContents()
        {
            ThreadAction.PostOnUiThread(() =>
            {
                WallContents.Clear();
                foreach (var dataWallContent in ViewerService.Instance.GetContents())
                {
                    WallContents.Add(ContentItem.CreateFrom(dataWallContent));
                }
                WallContentFolder = ContentTreeViewFolder.CreateFrom(WallContents);
            });
        }
        private void LoadSequence()
        {
            ThreadAction.PostOnUiThread(() =>
            {
                ViewerSequnce sequnce = ViewerService.Instance.GetSequnce();
                if (sequnce != null)
                {
                    SequenceSets?.Clear();
                    foreach (var sequnceSequenceSet in sequnce.SequenceSets)
                    {
                        SequenceSets.Add(new ViewerSequenceSetBindable(sequnceSequenceSet.Preset,
                            sequnceSequenceSet.Interval));
                    }
                }
            });
        }
        private void SaveSequence()
        {
            if (SequenceSets.Count > 0)
            {
                List<IViewerSequenceSet> sequenceSets = new List<IViewerSequenceSet>();
                foreach (var viewerSequenceSet in SequenceSets)
                {
                    sequenceSets.Add(new ViewerSequenceSet(viewerSequenceSet));
                }
                ViewerService.Instance.SaveSequence(sequenceSets);
            }
            else
            {
                ViewerService.Instance.SaveSequence(null);
            }
        }
        private void OnPlaySequenceTimer(object state)
        {
            _playSequenceTimer.Change(Timeout.Infinite, Timeout.Infinite);
            var sequence = SequenceSets[_currentSequenceIndex];

            ThreadAction.PostOnUiThread(() =>
            {
                RunDatawallPreset(sequence.Preset);
                if (_currentSequenceIndex == SequenceSets.Count - 1)
                {
                    _currentSequenceIndex = 0;
                }
                else
                {
                    _currentSequenceIndex++;
                }
            });

            _playSequenceTimer.Change(TimeSpan.FromSeconds(sequence.Interval), TimeSpan.FromSeconds(10));
        }
        private void ClearAllSelectedViewerLayout()
        {
            if (_selectedViewerLayoutBindable != null)
            {
                _selectedViewerLayoutBindable.IsSelected = false;
                _selectedViewerLayoutBindable = null;
            }
        }
        private void ClearAllSelectedContent()
        {
            if (_selectedContent != null)
            {
                _selectedContent.IsSelected = false;
                _selectedContent = null;
            }
        }
        private void ClearAllSelectedPreset()
        {
            if (_selectedDataWallPreset != null)
            {
                _selectedDataWallPreset.IsSelected = false;
                _selectedDataWallPreset = null;
            }
        }
        private void ClearAllSelectedSequenceSet()
        {
            if (_selectedSequenceSet != null)
            {
                _selectedSequenceSet.IsSelected = false;
                _selectedSequenceSet = null;
            }
        }
        private void RestoreScreen()
        {
            ScreenRow = 1;
            ScreenColumnSpan = 16;
            ScreenRowSpan = 16;
            IsFullScreen = false;
        }
        private void RunDatawallPreset(IDataWallPreset preset)
        {
            try
            {
                LayoutCells?.Clear();
               
                    foreach (var dataWallWindow in preset.Windows)
                    {
                        LayoutCells.Add(new ViewerLayoutCellBindable(dataWallWindow.WindowRect) { Id = dataWallWindow.Id, DataWallWindow = dataWallWindow });
                    }
                

            }
            catch (Exception ex)
            {
                Logger.Error(ex, "RunDatawallPreset error.");
            }
        }
        private void ExecuteChangePasswordCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "비밀번호 변경");
            _dialogService.ShowDialog("LocalChangePasswordDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.OK)
                {
                }
            });
        }
        private void ExecuteFullScreenCommand()
        {
            ScreenRow = 0;
            ScreenColumnSpan = 20;
            ScreenRowSpan = 20;
            IsFullScreen = true;
        }
        private void ExecuteOpenLayoutSettingDialogCommand()
        {
            var parameters = new DialogParameters();
            parameters.Add("title", "레이아웃 세팅");
            parameters.Add("dialogservice", _dialogService);
            _dialogService.ShowDialog("LayoutSettingDialog", parameters, r =>
            {
                if (r.Result == ButtonResult.Cancel)
                {
                    var isLayoutModified = r.Parameters.GetValue<bool>("islayoutsmodified");
                    if (isLayoutModified)
                    {
                        LoadLayouts();
                    }
                }
            });
        }
        private void ExecuteOpenContentsDialogCommand()
        {
            try
            {
                var parameters = new DialogParameters();
                parameters.Add("title", "컨텐츠 세팅");
                parameters.Add("dialogservice", _dialogService);
                _dialogService.ShowDialog("ContentsSettingDialog", parameters, r =>
                {
                    if (r.Result == ButtonResult.Cancel)
                    {
                        var resultParameter = r.Parameters;
                        resultParameter.TryGetValue("isContentModified", out bool isContentModified);
                        if (isContentModified)
                        {
                            LoadContents();
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteOpenContentsDialogCommand Error.");
            }
        }
        private void ExecuteToggleWallContentSelectionCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            if (_selectedContent != null)
            {
                string selectedContentId = _selectedContent.Id;
                _selectedContent.IsSelected = false;
                _selectedContent = null;
                if (selectedContentId == id)
                {
                    return;
                }
            }

            ClearAllSelectedContent();

            foreach (var content in WallContents.ToArray())
            {
                if (content.Id == id)
                {
                    _selectedContent = content;
                    _selectedContent.IsSelected = true;
                }
            }
        }
        private void ExecuteSelectLayoutCommand(string id)
        {
            ViewerLayoutBindable selectedLayout = null;
            foreach (var layout in Layouts.ToArray())
            {
                if (layout.Id == id)
                {
                    selectedLayout = layout;
                }
            }
            if (selectedLayout != null)
            {
                LayoutCells.Clear();
                ExecuteClearScreenCommand();
                _layoutId = selectedLayout.Id;
                foreach (var selectedLayoutCell in selectedLayout.Cells)
                {
                    LayoutCells.Add(new ViewerLayoutCellBindable(selectedLayoutCell));
                }
            }
        }
        private void ExecuteClearScreenCommand()
        {
            foreach (var viewerLayoutCellBindable in LayoutCells)
            {
                viewerLayoutCellBindable.DataWallWindow = null;
            }
            ClearAllSelectedViewerLayout();
        }
        private void ExecuteKeyUpCommand(KeyEventArgs keyEventArgs)
        {
            switch (keyEventArgs.Key)
            {
                case Key.Escape:
                    if (IsFullScreen)
                    {
                        RestoreScreen();
                    }

                    if (IsContentExpensionMode)
                    {
                        RemoveExpensionContent();
                    }
                    break;
                case Key.F3:
                    if (!IsFullScreen)
                    {
                        ExecuteFullScreenCommand();
                    }
                    break;
                case Key.Delete:
                    if (Keyboard.IsKeyDown(Key.LeftCtrl))
                    {
                        ExecuteClearScreenCommand();
                        break;
                    }

                    if (_selectedViewerLayoutBindable != null)
                    {
                        IDataWallWindow window = _selectedViewerLayoutBindable.DataWallWindow;
                        _selectedViewerLayoutBindable.DataWallWindow = new DataWallWindow(window.Id,screenRect: window.WindowRect, contentId:window.ContentId);
                    }
                    break;
            }
        }
        private void ExecuteAddPresetCommand()
        {
            var parameters = new DialogParameters
            {
                {"wallPresets", WallPresets}, {"windows", LayoutCells.Select(cell=> cell.DataWallWindow)}
            };
            _dialogService.ShowDialog("AddViewerPresetDialog", parameters, addPresetResult =>
            {
                Task.Run(LoadPresets);
            });
        }
        private void ExecuteDeletePresetCommand()
        {
            if (_selectedDataWallPreset == null)
            {
                return;
            }
            var parameters = new DialogParameters
            {
                {"title", $"프리셋 삭제"}, {"message", $"{_selectedDataWallPreset.Id} / {_selectedDataWallPreset.Name}을 삭제하시겠습니까?"}
            };
            _dialogService.ShowDialog("confirm", parameters, addPresetResult =>
            {
                if (addPresetResult.Result == ButtonResult.OK)
                {
                    ViewerService.Instance.DeletePresets(new[] { _selectedDataWallPreset.Id });
                    LoadPresets();
                }
            });

            
        }
        private void ExecuteTogglePresetSelectionCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }
            if (_selectedDataWallPreset != null)
            {
                string selectedPresetId = _selectedDataWallPreset.Id;
                _selectedDataWallPreset.IsSelected = false;
                _selectedDataWallPreset = null;
                if (selectedPresetId == id)
                {
                    return;
                }
            }

            ClearAllSelectedPreset();

            foreach (var content in WallPresets.ToArray())
            {
                if (content.Id == id)
                {
                    _selectedDataWallPreset = content;
                    _selectedDataWallPreset.IsSelected = true;
                }
            }
        }
        private void ExecuteRunWallPresetCommand(string id)
        {
            if (CheckSequenceMode())
            {
                return;
            }
            if (WallPresets.Any(preset => preset.Id.Equals(id)))
            {
                var preset = WallPresets.First(preset => preset.Id.Equals(id));
                var parameters = new DialogParameters
                {
                    {"title", $"프리셋 실행"}, {"message", $"{preset.Id} / {preset.Name}을 실행하시겠습니까?"}
                };
                _dialogService.ShowDialog("confirm", parameters, addPresetResult =>
                {
                    if (addPresetResult.Result == ButtonResult.OK)
                    {
                        RunDatawallPreset(preset);
                    }
                });
            }
            
        }
        private void ExecuteAddSequnceSetCommand(object preset)
        {
            if (preset == null)
            {
                return;
            }
            try
            {
                if (preset is DataWallPresetBindable selectedPreset)
                {
                    if (SequenceSets.Any(sequence => sequence.Id.Equals(selectedPreset.Id)))
                    {
                        MessageBox.Show($"동일한 프리셋은 추가 할 수 없습니다.");
                    }
                    else
                    {
                        var sequence = new ViewerSequenceSetBindable(selectedPreset);
                        SequenceSets.Add(sequence);
                        ToggleSequenceSelection(sequence.Id);
                    }
                    SaveSequence();
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteAddSequnceSetCommand Error.");
            }
        }
        private void ExecuteRemoveSequnceSetCommand()
        {
            if (_selectedSequenceSet == null)
            {
                return;
            }

            var index = SequenceSets.IndexOf(_selectedSequenceSet);
            if (index < 0)
            {
                return;
            }
            SequenceSets.Remove(_selectedSequenceSet);
            if (SequenceSets.Count > 0)
            {
                if (index > 0)
                {
                    index--;
                }

                var selectedSequenceSet = SequenceSets[index];
                selectedSequenceSet.IsSelected = true;
                _selectedSequenceSet = selectedSequenceSet;
            }
            else
            {
                _selectedSequenceSet = null;
            }
            SaveSequence();
        }
        private void ExecuteSequnceIndexDownCommand()
        {
            if (_selectedSequenceSet == null || SequenceSets.Count < 2)
            {
                return;
            }
            var index = SequenceSets.IndexOf(_selectedSequenceSet);
            if (index < 0)
            {
                return;
            }

            if (index == SequenceSets.Count - 1)
            {
                return;
            }
            SequenceSets.Remove(_selectedSequenceSet);
            SequenceSets.Insert(index + 1, _selectedSequenceSet);
            _selectedSequenceSet.IsSelected = true;
            SaveSequence();
        }
        private void ExecuteSequnceIndexUpCommand()
        {
            if (_selectedSequenceSet == null || SequenceSets.Count < 2)
            {
                return;
            }
            var index = SequenceSets.IndexOf(_selectedSequenceSet);
            if (index  <= 0)
            {
                return;
            }

            SequenceSets.Remove(_selectedSequenceSet);
            SequenceSets.Insert(index - 1, _selectedSequenceSet);
            _selectedSequenceSet.IsSelected = true;
            SaveSequence();
        }
        private void ExecuteToggleSequenceSelectionCommand(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return;
            }

            ToggleSequenceSelection(id);
        }
        private void ToggleSequenceSelection(string id)
        {
            if (_selectedSequenceSet != null)
            {
                string selectedSequnceSetId = _selectedSequenceSet.Id;
                _selectedSequenceSet.IsSelected = false;
                _selectedSequenceSet = null;
                if (selectedSequnceSetId == id)
                {
                    return;
                }
            }

            ClearAllSelectedSequenceSet();

            foreach (var sequenceSet in SequenceSets.ToArray())
            {
                if (sequenceSet.Id == id)
                {
                    _selectedSequenceSet = sequenceSet;
                    _selectedSequenceSet.IsSelected = true;
                }
            }
        }
        private void ExecuteSelectCellCommand(ViewerLayoutCellBindable selectedViewerLayoutCellBindable)
        {
            if (selectedViewerLayoutCellBindable is ViewerLayoutCellBindable viewerLayoutCell)
            {
                if (viewerLayoutCell.IsSelected)
                {
                    viewerLayoutCell.IsSelected = false;
                }
                else
                {
                    ClearAllSelectedViewerLayout();
                    viewerLayoutCell.IsSelected = true;
                    _selectedViewerLayoutBindable = viewerLayoutCell;
                }

                if (_selectedContent != null)
                {
                    if (selectedViewerLayoutCellBindable.DataWallWindow != null)
                    {
                        selectedViewerLayoutCellBindable.DataWallWindow = null;
                    }

                    var content = DataWallContent.CreateFrom(_selectedContent);
                    var osdConfig = DefaultOsdConfig.Clone();
                    osdConfig.IsShow = content.GetValue("osdIsShow", osdConfig.IsShow);
                    osdConfig.Alignment = content.GetValue("osdAlignment", osdConfig.Alignment);
                    osdConfig.Offset = content.GetValue("osdOffset", osdConfig.Offset);
                    osdConfig.Opacity = content.GetValue("osdOpacity", osdConfig.Opacity);
                    osdConfig.FontSize = content.GetValue("osdFontSize", osdConfig.FontSize);
                    osdConfig.Color = content.GetValue("osdColor", osdConfig.Color);

                    content.SetProperty("osdIsShow", $"{osdConfig.IsShow}");
                    content.SetProperty("osdAlignment", $"{osdConfig.Alignment}");
                    content.SetProperty("osdOffset", $"{osdConfig.Offset}");
                    content.SetProperty("osdOpacity", $"{osdConfig.Opacity}");
                    content.SetProperty("osdFontSize", $"{osdConfig.FontSize}");
                    content.SetProperty("osdColor", $"{osdConfig.Color}");
                    selectedViewerLayoutCellBindable.DataWallWindow = new DataWallWindow(viewerLayoutCell.Id,
                        screenRect: selectedViewerLayoutCellBindable.Rect,
                        content: content);
                    selectedViewerLayoutCellBindable.IsSelected = false;
                    ClearAllSelectedContent();
                }

            }

        }
        private void ExecuteStartSequenceCommand()
        {
            if (SequenceSets.Count == 0)
            {
                var parameters = new DialogParameters();
                parameters.Add("title", "Sequence 확인");
                parameters.Add("message", "설정된 Sequence가 없습니다.");
                _dialogService.ShowDialog("confirm", parameters, r => { });
                return;
            }

            IsSequenceMode = true;
            _playSequenceTimer.Change(TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10));
        }
        private void ExecuteStopSequenceCommand()
        {
            _playSequenceTimer.Change(Timeout.Infinite, Timeout.Infinite);
            _currentSequenceIndex = 0;
            IsSequenceMode = false;
        }
        private void ExecuteOpenPreviewWindowCommand(ContentItem contentItem)
        {
            if (contentItem == null)
            {
                return;
            }
            ContentItem item = contentItem;
            switch (contentItem.ContentType.ToLower())
            {
                case "remote":
                    item.SetProperty("isPreview", "true");
                    break;
                case "media":
                case "camera":
                case "web":
                case "image":
                //case "ptz-camera":
                    break;
                case "ptz-camera":
                    PtzCameraControl ptzCameraControl = new PtzCameraControl(item, _isTestMode, _isVerbose, _dialogService);
                    ptzCameraControl.Show();
                    return;
            }
            try
            {
                PreviewWindow remotePreview = new PreviewWindow(item);
                remotePreview.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "ExecuteOpenPreviewWindowCommand error.");
            }
        }
        private void ExecuteMouseDoubleClickExpensionContentCommand(object obj)
        {
            RemoveExpensionContent();
        }
        private void ExecuteMouseDoubleClickCellCommand(ViewerLayoutCellBindable datawallContent)
        {
            if (CheckSequenceMode())
            {
                return;
            }
            var datawallWindow = LayoutCells.FirstOrDefault(window => window.Id.Equals(datawallContent.Id));
            if (datawallWindow.DataWallWindow == null)
            {
                return;
            }
            DataWallWindow expensionDataWallWindow = new DataWallWindow(datawallWindow.DataWallWindow);
            expensionDataWallWindow.WindowRect = new IntRect(0, 0, _screenSize.Width, _screenSize.Height);
            SetExpensionContent(expensionDataWallWindow);
        }
        private void ExecuteWindowClosingCommand()
        {
            _cacheManager.Set("lastLayout", _layoutId);
        }
    }
}
