﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.DataWalls;
using NodeView.ViewModels;
using NodeViewWindowCore.ViewModels;
using Prism.Services.Dialogs;

namespace NodeViewViewer.ViewModels
{
    public class ContentSettingViewModel : ContentSettingBaseViewModel
    {
        private bool _isContentModified = false;
        public bool IsContentModified => _isContentModified;

        public ContentSettingViewModel(IDialogService dialogService) : base(dialogService)
        {
            LoadContents();
        }

        protected override void LoadContents()
        {
            WallContents.Clear();
            foreach (var dataWallContent in ViewerService.Instance.GetContents())
            {
                WallContents.Add(ContentItem.CreateFrom(dataWallContent));
            }
            WallContentFolder = ContentTreeViewFolder.CreateFrom(WallContents);
        }

        protected override bool UpdateContent(DataWallContent content)
        {
            _isContentModified = true;
            return ViewerService.Instance.UpdateContent(content);
        }

        protected override bool CreateContent(DataWallContent[] content)
        {
            _isContentModified = true;
            return ViewerService.Instance.CreateContents(content);
        }

        protected override bool DeleteContent(string[] ids)
        {
            _isContentModified = true;
            return ViewerService.Instance.DeleteContents(ids);
        }

        
    }
}
