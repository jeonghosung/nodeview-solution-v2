using NLog;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeViewProcessManager;
using System.ComponentModel;
using Prism.DryIoc;

namespace NodeViewProcessManagerApp
{
    public partial class NodeViewProcessManagerForm : Form
    {
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        private bool _isLoaded = false;
        private NotifyIcon _notify = new NotifyIcon();
        private DateTime _lastReloadTime = DateTime.MinValue;

        public NodeViewProcessManagerForm()
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Minimized;
            this.Hide();
        }

        private void NodeViewProcessManagerForm_Load(object sender, EventArgs e)
        {
            InitNotify();
            ProcessManagerService service = ProcessManagerService.Instance;
            string configFilename = "NodeViewProcessManager.config.xml";
            var container = new DryIocContainerExtension();
            NodeViewContainerExtension.CreateInstance(container);

            try
            {
                string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
                var config = Configuration.Load(settingFile);
                if (config == null)
                {
                    Logger.Error("Cannot load config!");
                    this.Close();
                    return;
                }

                if (!service.Initialize(container, configFilename))
                {
                    Logger.Error("service Initialize error!");
                    Close();
                    return;
                }

                try
                {
                    if (!service.Start())
                    {
                        Logger.Error("service start error!");
                        Close();
                        return;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error(ex, "service.Start:");
                    service.Stop();
                }

                Thread.Sleep(100);
            }
            catch (Exception exception)
            {
                Logger.Error(exception, "Load:");
            }
            _isLoaded = true;
        }

        private void InitNotify()
        {
            var menu = new ContextMenuStrip();
            ToolStripMenuItem menuItem = new ToolStripMenuItem
            {
                Text = "Open"
            };
            menuItem.Click += delegate
            {
                this.WindowState = FormWindowState.Normal;
                this.Show();
            };
            menu.Items.Add(menuItem);

            menuItem = new ToolStripMenuItem
            {
                Text = "Exit"
            };
            menuItem.Click += delegate
            {
                _notify.Visible = false;
                Close();
            };
            menu.Items.Add(menuItem);
            string applicationLocation = System.Reflection.Assembly.GetEntryAssembly().Location;
            string applicationDirectory = Path.GetDirectoryName(applicationLocation);
            //_notify.Icon = new System.Drawing.Icon(new Uri("path"));
            _notify.Icon = new Icon(Path.Combine(applicationDirectory, "process.ico"));
            _notify.Visible = true;
            _notify.DoubleClick += delegate
            {
                //DoubleMethod();
            };
            _notify.ContextMenuStrip = menu;
            _notify.Text = "NodeView Process Manager";
        }

        private void NodeViewProcessManagerForm_Activated(object sender, EventArgs e)
        {
            if (_isLoaded)
            {
                if ((DateTime.Now - _lastReloadTime).Seconds > 5)
                {
                    ReloadProcessList();
                }
            }
        }

        private void ReloadProcessList()
        {
            listBoxProcess.Items.Clear();
            try
            {
                var processNodes = ProcessManagerService.Instance.GetApps();
                foreach (var processNode in processNodes)
                {
                    listBoxProcess.Items.Add(
                        $"[{processNode.ProcessType}][{processNode.AppType}]{processNode.Name}({processNode.Status})({processNode.Id})");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ReloadProcessList:");
            }
        }

        private void NodeViewProcessManagerForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                Hide();
            }
        }

        private void NodeViewProcessManagerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _notify.Visible = false;
            ProcessManagerService.Instance.Stop();
        }

        private void buttonReload_Click(object sender, EventArgs e)
        {
            ReloadProcessList();
        }
    }
}