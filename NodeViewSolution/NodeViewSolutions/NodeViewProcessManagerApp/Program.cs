using NLog;

namespace NodeViewProcessManagerApp
{
    internal static class Program
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += Application_ThreadException;
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.Automatic);
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            // throw new Exception("UI 생성되기 전에 오류를 throw 했습니다.");    
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ApplicationConfiguration.Initialize();
            Application.Run(new NodeViewProcessManagerForm());
        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            Logger.Error("Application_ThreadException " + e.Exception.Message);
            //System.Diagnostics.Trace.WriteLine("Application_ThreadException " + e.Exception.Message);
            MessageBox.Show("Application_ThreadException " + e.Exception.Message);
            Application.Exit(new System.ComponentModel.CancelEventArgs(false));
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Logger.Error("CurrentDomain_UnhandledException " + ((Exception)e.ExceptionObject).Message);
            //System.Diagnostics.Trace.WriteLine("CurrentDomain_UnhandledException " + ((Exception)e.ExceptionObject).Message);
            MessageBox.Show("CurrentDomain_UnhandledException " + ((Exception)e.ExceptionObject).Message + " Is Terminating: " + e.IsTerminating.ToString());

        }
    }
}