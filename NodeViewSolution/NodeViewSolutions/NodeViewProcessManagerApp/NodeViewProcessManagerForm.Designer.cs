﻿namespace NodeViewProcessManagerApp
{
    partial class NodeViewProcessManagerForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NodeViewProcessManagerForm));
            this.listBoxLogs = new System.Windows.Forms.ListBox();
            this.listBoxProcess = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonReload = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBoxLogs
            // 
            this.listBoxLogs.FormattingEnabled = true;
            this.listBoxLogs.ItemHeight = 15;
            this.listBoxLogs.Location = new System.Drawing.Point(20, 282);
            this.listBoxLogs.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listBoxLogs.Name = "listBoxLogs";
            this.listBoxLogs.Size = new System.Drawing.Size(556, 424);
            this.listBoxLogs.TabIndex = 7;
            // 
            // listBoxProcess
            // 
            this.listBoxProcess.FormattingEnabled = true;
            this.listBoxProcess.ItemHeight = 15;
            this.listBoxProcess.Location = new System.Drawing.Point(20, 34);
            this.listBoxProcess.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.listBoxProcess.Name = "listBoxProcess";
            this.listBoxProcess.Size = new System.Drawing.Size(556, 199);
            this.listBoxProcess.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 15);
            this.label2.TabIndex = 5;
            this.label2.Text = "Logs:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 15);
            this.label1.TabIndex = 4;
            this.label1.Text = "process List:";
            // 
            // buttonReload
            // 
            this.buttonReload.Location = new System.Drawing.Point(501, 9);
            this.buttonReload.Name = "buttonReload";
            this.buttonReload.Size = new System.Drawing.Size(75, 23);
            this.buttonReload.TabIndex = 8;
            this.buttonReload.Text = "Reload";
            this.buttonReload.UseVisualStyleBackColor = true;
            this.buttonReload.Click += new System.EventHandler(this.buttonReload_Click);
            // 
            // NodeViewProcessManagerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(596, 714);
            this.Controls.Add(this.buttonReload);
            this.Controls.Add(this.listBoxLogs);
            this.Controls.Add(this.listBoxProcess);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "NodeViewProcessManagerForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NodeView Process Manager";
            this.Activated += new System.EventHandler(this.NodeViewProcessManagerForm_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.NodeViewProcessManagerForm_FormClosing);
            this.Load += new System.EventHandler(this.NodeViewProcessManagerForm_Load);
            this.SizeChanged += new System.EventHandler(this.NodeViewProcessManagerForm_SizeChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ListBox listBoxLogs;
        private ListBox listBoxProcess;
        private Label label2;
        private Label label1;
        private Button buttonReload;
    }
}