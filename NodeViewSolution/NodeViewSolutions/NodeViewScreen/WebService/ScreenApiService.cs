﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeView.WebServers;
using Semsol.Grapevine.Interfaces.Server;
using Semsol.Grapevine.Server;
using Semsol.Grapevine.Server.Attributes;
using Semsol.Grapevine.Shared;

namespace NodeViewScreen.WebService
{
    [RestResource(BasePath = "/api/v1")]
    public class ScreenApiService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        [RestRoute(HttpMethod = Semsol.Grapevine.Shared.HttpMethod.GET, PathInfo = "/")]
        public IHttpContext Index(IHttpContext context)
        {
            try
            {
                context.Response.SendResponse(HttpStatusCode.Ok,
                    TemplateManager.Render("PageLinks", GetServicePageList()), Encoding.UTF8);
            }
            catch (Exception)
            {
                context.Response.SendResponse(HttpStatusCode.InternalServerError);
            }

            return context;
        }
        private object GetServicePageList()
        {
            var linkInfos = new List<PageLinkInfo>();
            linkInfos.Add(new PageLinkInfo("Windows", "/api/v1/windows", ""));
            linkInfos.Add(new PageLinkInfo("PopupWindows", "/api/v1/popups", ""));
            return linkInfos;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/windows")]
        public IHttpContext GetWindows(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ScreenManager screenManager = ScreenManager.Instance;

                var windows = screenManager.GetWindows();
                if (windows != null)
                {
                    JArray resArray = new JArray();
                    foreach (var window in windows)
                    {
                        resArray.Add(window.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get windows.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateWindows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/windows")]
        public IHttpContext CreateWindows(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ScreenManager screenManager = ScreenManager.Instance;


                List<DataWallWindow> windowList = new List<DataWallWindow>();

                var jsonToken = JToken.Parse(context.Request.Payload);
                if (jsonToken.Type == JTokenType.Object)
                {
                    var window = DataWallWindow.CreateFrom(jsonToken.Value<JObject>());
                    if (window == null)
                    {
                        RestApiResponse.SendResponseBadRequest(context, "payload error");
                        return context;
                    }
                    window.IsPopup = false;
                    windowList.Add(window);
                }
                else if (jsonToken.Type == JTokenType.Array)
                {
                    JArray array = jsonToken.Value<JArray>();
                    foreach (var token in array)
                    {
                        var window = DataWallWindow.CreateFrom(token.Value<JObject>());
                        if (window == null)
                        {
                            RestApiResponse.SendResponseBadRequest(context, "payload error");
                            return context;
                        }
                        window.IsPopup = false;
                        windowList.Add(window);
                    }
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload is not (a) content(s) json.");
                    return context;
                }

                var resWindows = screenManager.RequestCreateWindows(requester, windowList.ToArray());
                if (resWindows != null)
                {
                    JArray resArray = new JArray();
                    foreach (var windowContent in resWindows)
                    {
                        resArray.Add(windowContent.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot create windows.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreateWindows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/windows")]
        public IHttpContext PutActionWindows(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ScreenManager screenManager = ScreenManager.Instance;
                bool isSuccess = false;
                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "closeInRect", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    IntRect rect = JsonUtils.GetValue< IntRect>(json, "param.rect", null);
                    if (rect != null && !rect.IsEmpty)
                    {
                        screenManager.RequestCloseWindowsInRect(requester, rect);
                        isSuccess = true;
                    }
                }
                else if (string.Compare(putAction, "closeAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    screenManager.RequestCloseAllWindows(requester);
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "close", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                    if (idArray != null && idArray.Count > 0)
                    {
                        List<string> idList = new List<string>();
                        foreach (JToken token in idArray)
                        {
                            string id = token.Value<string>();
                            if (!string.IsNullOrWhiteSpace(id))
                            {
                                idList.Add(id);
                            }
                        }

                        if (idList.Count > 0)
                        {
                            screenManager.RequestCloseWindows(requester, idList.ToArray());
                        }
                        isSuccess = true;
                    }
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, null);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Widnows!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }
        
        [RestRoute(HttpMethod = HttpMethod.DELETE, PathInfo = @"/windows/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext CloseWindow(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/windows/(?<id>[a-zA-Z0-9_\-]+)$");
                if (!match.Success)
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot parse id.");
                    return context;
                }

                id = match.Groups["id"].Value;

                ScreenManager screenManager = ScreenManager.Instance;

                screenManager.RequestCloseWindows(requester, new[] { id });

                RestApiResponse.SendResponseSuccess(context, null);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CloseWindow!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.GET, PathInfo = @"/popups")]
        public IHttpContext GetPopups(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ScreenManager screenManager = ScreenManager.Instance;

                var popups = screenManager.GetPopups();
                if (popups != null)
                {
                    JArray resArray = new JArray();
                    foreach (var popup in popups)
                    {
                        resArray.Add(popup.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot get popups.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreatePopups!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.POST, PathInfo = @"/popups")]
        public IHttpContext CreatePopups(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ScreenManager screenManager = ScreenManager.Instance;


                List<DataWallWindow> windowList = new List<DataWallWindow>();

                var jsonToken = JToken.Parse(context.Request.Payload);
                if (jsonToken.Type == JTokenType.Object)
                {
                    var window = DataWallWindow.CreateFrom(jsonToken.Value<JObject>());
                    if (window == null)
                    {
                        RestApiResponse.SendResponseBadRequest(context, "payload error");
                        return context;
                    }

                    window.IsPopup = true;
                    windowList.Add(window);
                }
                else if (jsonToken.Type == JTokenType.Array)
                {
                    JArray array = jsonToken.Value<JArray>();
                    foreach (var token in array)
                    {
                        var window = DataWallWindow.CreateFrom(token.Value<JObject>());
                        if (window == null)
                        {
                            RestApiResponse.SendResponseBadRequest(context, "payload error");
                            return context;
                        }
                        window.IsPopup = true;
                        windowList.Add(window);
                    }
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "payload is not (a) content(s) json.");
                    return context;
                }

                var resPopups = screenManager.RequestCreatePopups(requester, windowList.ToArray());
                if (resPopups != null)
                {
                    JArray resArray = new JArray();
                    foreach (var popupContent in resPopups)
                    {
                        resArray.Add(popupContent.ToJson());
                    }
                    RestApiResponse.SendResponseSuccess(context, resArray);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot create popups.");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot CreatePopups!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        [RestRoute(HttpMethod = HttpMethod.PUT, PathInfo = @"/popups")]
        public IHttpContext PutActionPopups(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                ScreenManager screenManager = ScreenManager.Instance;
                bool isSuccess = false;
                var json = JObject.Parse(context.Request.Payload);
                string putAction = JsonUtils.GetValue(json, "action", "").ToLower();

                if (string.Compare(putAction, "closeInRect", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    IntRect rect = JsonUtils.GetValue<IntRect>(json, "param.rect", null);
                    if (rect != null && !rect.IsEmpty)
                    {
                        screenManager.RequestClosePopupsInRect(requester, rect);
                        isSuccess = true;
                    }
                }
                else if (string.Compare(putAction, "closeAll", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    screenManager.RequestCloseAllPopups(requester);
                    isSuccess = true;
                }
                else if (string.Compare(putAction, "close", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    JArray idArray = JsonUtils.GetValue<JArray>(json, "param.ids", null);
                    if (idArray != null && idArray.Count > 0)
                    {
                        List<string> idList = new List<string>();
                        foreach (JToken token in idArray)
                        {
                            string id = token.Value<string>();
                            if (!string.IsNullOrWhiteSpace(id))
                            {
                                idList.Add(id);
                            }
                        }

                        if (idList.Count > 0)
                        {
                            screenManager.RequestClosePopups(requester, idList.ToArray());
                        }
                        isSuccess = true;
                    }
                }
                else if (string.Compare(putAction, "move", StringComparison.CurrentCultureIgnoreCase) == 0)
                {
                    string windowId = JsonUtils.GetValue<string>(json, "param.id", null);
                    IntRect windowRect = JsonUtils.GetValue<IntRect>(json, "param.windowRect", IntRect.Empty);
                    if (!string.IsNullOrWhiteSpace(windowId) && !windowRect.IsEmpty)
                    {
                        screenManager.RequestMovePopup(requester, windowId, windowRect);
                        isSuccess = true;
                    }
                }

                if (isSuccess)
                {
                    RestApiResponse.SendResponseSuccess(context, null);
                }
                else
                {
                    RestApiResponse.SendResponseBadRequest(context, "");
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot do PutAction for Popups!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

        
        [RestRoute(HttpMethod = HttpMethod.DELETE, PathInfo = @"/popups/(?<id>[a-zA-Z0-9_\-]+)$")]
        public IHttpContext ClosePopup(IHttpContext context)
        {
            Logger.Trace($"[{context.Request.HttpMethod}][{context.Request.RemoteEndPoint.Address}][{context.Request.PathInfo}]");
            string requester = $"{context.Request.RemoteEndPoint.Address}";
            try
            {
                string id = "";
                var match = Regex.Match(context.Request.PathInfo, @"/popups/(?<id>[a-zA-Z0-9_\-]+)$");
                if (!match.Success)
                {
                    RestApiResponse.SendResponseBadRequest(context, "Cannot parse id.");
                    return context;
                }

                id = match.Groups["id"].Value;

                ScreenManager screenManager = ScreenManager.Instance;

                screenManager.RequestClosePopups(requester, new[] { id });

                RestApiResponse.SendResponseSuccess(context, null);
            }
            catch (Exception e)
            {
                Logger.Error(e, "Cannot ClosePopup!");
                RestApiResponse.SendResponseInternalServerError(context);
            }
            return context;
        }

    }
}
