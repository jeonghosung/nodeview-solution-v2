﻿using CefSharp;
using CefSharp.Wpf;
using NLog;
using NodeView.DataModels;
using NodeView.DataWallContentControls;
using NodeView.Ioc;
using NodeView.Tasks;
using NodeView.Utils;
using NodeViewScreen.Services.ViewWindows;
using NodeViewScreenFramework;
using NodeViewScreenFramework.DataWallContentControls;
using Prism.DryIoc;
using Prism.Events;
using Prism.Ioc;
using Prism.Regions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Configuration = NodeView.DataModels.Configuration;

namespace NodeViewScreen
{
    /// <summary>
    /// App.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class App : PrismApplication
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IConfiguration _configuration;
        private ScreenWebServer _webServer;
        private ScreenManager _screenManager;
        private readonly List<string> _criticalErrorMessages = new List<string>();
        private Window _mainWindow;

        private string _id = Uuid.NewUuid;

        public bool HasCriticalError => _criticalErrorMessages.Count > 0;

        public App() : base()
        {
            SetupUnhandledExceptionHandling();
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                _id = args[1];
            }
            else
            {
                _id = Uuid.NewUuid;
            }
            var settings = new CefSettings()
            {
                //by default cefsharp will use an in-memory cache, you need to specify a cache folder to persist data
                //CachePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "cefsharp\\cache")
            };
            try
            {
                //NOTE: WebRTC Device Id's aren't persisted as they are in Chrome see https://bitbucket.org/chromiumembedded/cef/issues/2064/persist-webrtc-deviceids-across-restart
                settings.CefCommandLineArgs.Add("enable-media-stream");
                //https://peter.sh/experiments/chromium-command-line-switches/#use-fake-ui-for-media-stream
                settings.CefCommandLineArgs.Add("use-fake-ui-for-media-stream");
                //For screen sharing add (see https://bitbucket.org/chromiumembedded/cef/issues/2582/allow-run-time-handling-of-media-access#comment-58677180)
                settings.CefCommandLineArgs.Add("enable-usermedia-screen-capturing");
                settings.CefCommandLineArgs["autoplay-policy"] = "no-user-gesture-required";
                settings.IgnoreCertificateErrors = true;
                if (Cef.IsInitialized is null or false)
                {
                    //Perform dependency check to make sure all relevant resources are in our output directory.
                    Cef.Initialize(settings);
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Cef init error.");
            }
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            if (containerRegistry is IContainerExtension containerExtension)
            {
                NodeViewContainerExtension.CreateInstance(containerExtension);
                var viewWindowService = new ViewWindowService(containerExtension);
                containerRegistry.RegisterInstance(typeof(IViewWindowService), viewWindowService);
            }

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "NodeViewDataWall.config.xml");
            if (!File.Exists(settingFile))
            {
                _criticalErrorMessages.Add("설정파일을 찾을 수 없습니다.");
                return;
            }
            _configuration = Configuration.Load(settingFile);

            //containerRegistry.RegisterInstance(typeof(SemsolConfiguration), _configuration);
            var datawallPort = _configuration.GetValue("service.port", 20111);
            int servicePort = GetServicePort(datawallPort);

            if (servicePort <= 0)
            {
                _criticalErrorMessages.Add("Cannot find service port.");
                servicePort = 20111;
            }
            _webServer = new ScreenWebServer(_id, servicePort);

            IEventAggregator eventAggregator = new EventAggregator();
            containerRegistry.RegisterInstance(typeof(IEventAggregator), eventAggregator);
            containerRegistry.RegisterInstance(typeof(Configuration), _configuration, "appConfig");
            var isEnableMixin = _configuration.GetValue("mixinWall.isEnabled", false);
            if (isEnableMixin)
            {
                containerRegistry.Register(typeof(IViewWindowManager), typeof(ViewWindowManager4Mixin));
            }
            else
            {
                containerRegistry.Register(typeof(IViewWindowManager), typeof(ViewWindowManager));
            }
            //containerRegistry.Register(typeof(IViewWindowManager), typeof(ViewWindowManager4Mixin));
            //IViewWindow
            containerRegistry.Register(typeof(IViewWindow), typeof(ViewWindow));
            //DataWallContentControls
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "media");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "camera");
            containerRegistry.RegisterDataWallContentControl(typeof(VlcDataWallContentControl), "ptz-camera");
            containerRegistry.RegisterDataWallContentControl(typeof(WebDataWallContentControl), "web");
            containerRegistry.RegisterDataWallContentControl(typeof(RemoteDataWallContentControl), "remote");
            containerRegistry.RegisterDataWallContentControl(typeof(RemoteDataWallContentViewerControl), "remoteviewer");
            containerRegistry.RegisterDataWallContentControl(typeof(ImageContentControl), "image");

            string[] options = Array.Empty<string>();
            var useRtspTcp = _configuration.GetValue("useRtspTcp", false);
            if (useRtspTcp)
            {
                options = new[] { "--rtsp-tcp" };
            }

            VlcLlibraryManager.Instance.Init(options);
            _screenManager = ScreenManager.Instance;
            _screenManager.Initialize(_id, servicePort, _configuration, eventAggregator);
            containerRegistry.RegisterInstance(typeof(ScreenManager), _screenManager);

        }

        private int GetServicePort(int serviceStartPort)
        {
            for (int portIndex = 0; portIndex < 200; portIndex++)
            {
                using (TcpClient tcpClient = new TcpClient())
                {
                    try
                    {
                        tcpClient.ReceiveTimeout = 500;
                        tcpClient.SendTimeout = 500;
                        tcpClient.Connect("127.0.0.1", serviceStartPort + portIndex);
                    }
                    catch
                    {
                        return serviceStartPort + portIndex;
                    }
                }
            }

            return -1;
        }

        protected override Window CreateShell()
        {
            if (HasCriticalError)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("설정에 오류가 발생하여 프로그램을 시작 할 수없습니다.");
                sbMessage.AppendLine("상세오류===============================");
                foreach (var errorMessage in _criticalErrorMessages)
                {
                    sbMessage.AppendLine(errorMessage);
                }

                MessageBox.Show(sbMessage.ToString(), "설정 오류", MessageBoxButton.OK, MessageBoxImage.Error);
                Current.Shutdown();
                return null;
            }

            _screenManager.Start();
            if (_screenManager.HasInitError)
            {
                StringBuilder sbMessage = new StringBuilder();
                sbMessage.AppendLine("프로그램 시작중 아래의 경고가 발생했습니다.");
                sbMessage.AppendLine("상세경고===============================");
                foreach (var errorMessage in _screenManager.InitErrorMessages)
                {
                    sbMessage.AppendLine(errorMessage);
                }

                MessageBox.Show(sbMessage.ToString(), "프로그램 시작", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (!_webServer.Start())
            {
                MessageBox.Show($"'{_webServer.Port}'번 포트에서 서버를 시작 할 수없습니다.", "서버 시작", MessageBoxButton.OK,
                    MessageBoxImage.Warning);
            }

            IRegionManager regionManager = Container.Resolve<IRegionManager>();

            _mainWindow = Container.Resolve<ScreenManagerWindow>();
            _mainWindow.Loaded += MainWindowOnLoaded;
            return _mainWindow;
        }

        private void MainWindowOnLoaded(object sender, RoutedEventArgs e)
        {
            _mainWindow.Loaded -= MainWindowOnLoaded;
            ThreadAction.PostOnUiThread(() =>
            {
                if (!_screenManager.JoinToDataWall())
                {
                    Application.Current.Shutdown();
                }
            });
        }

        protected override void OnInitialized()
        {
            if (HasCriticalError)
            {
                return;
            }

            base.OnInitialized();
            if (_mainWindow != null)
            {
                _mainWindow.Closed += (sender, args) => { Application.Current.Shutdown(); };
            }
        }

        protected override void OnExit(ExitEventArgs e)
        {
            VlcLlibraryManager.Instance.Dispose();
            _webServer?.Stop();
            _screenManager?.Stop();
            base.OnExit(e);
        }

        private void SetupUnhandledExceptionHandling()
        {
            // Catch exceptions from all threads in the AppDomain.
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException",
                    false);

            // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
            TaskScheduler.UnobservedTaskException += (sender, args) =>
                ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", false);

            // Catch exceptions from a single specific UI dispatcher thread.
            Dispatcher.UnhandledException += (sender, args) =>
            {
                // If we are debugging, let Visual Studio handle the exception and take us to the code that threw it.
                if (!Debugger.IsAttached)
                {
                    args.Handled = true;
                    ShowUnhandledException(args.Exception, "Dispatcher.UnhandledException", true);
                }
            };
        }
        private List<string> _exceptUnhandledExceptionMessage = new List<string>()
            { "Waiting on the Task or accessing its Exception property" };
        void ShowUnhandledException(Exception e, string unhandledExceptionType, bool promptUserForShutdown)
        {
            Logger.Error(e, $"ShowUnhandledException ['{unhandledExceptionType}',{promptUserForShutdown}]");
            if (_exceptUnhandledExceptionMessage.Any(message => e.Message.IndexOf(message, StringComparison.Ordinal) >= 0))
            {
                return;
            }
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{e}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}
