﻿using System.Collections.Generic;
using NodeView.DataWalls;
using NodeView.Drawing;

namespace NodeViewScreen
{
    public interface IViewWindowManager
    {
        void Start();
        void Stop();
        DataWallWindow[] GetWindows();
        void CreateWindows(DataWallWindow[] createWindows);
        void CloseWindowsInRect(IntRect rect);
        void CloseWindows(string[] deleteWindowIds);
        void CloseAllWindows();
        DataWallWindow[] GetPopups();
        void CreatePopups(DataWallWindow[] createPopups);
        void ClosePopupsInRect(IntRect rect);
        void ClosePopups(string[] deletePopupIds);
        void CloseAllPopups();
        void MovePopup(string windowId, IntRect windowRect);
    }
}