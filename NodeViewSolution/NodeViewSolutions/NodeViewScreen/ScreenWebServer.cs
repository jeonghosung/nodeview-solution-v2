﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Semsol.Grapevine.Server;

namespace NodeViewScreen
{
    public class ScreenWebServer
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly RestServer _restServer;

        public string Id { get; }
        public int Port { get; }
        public bool IsListening => _restServer.IsListening;

        public ScreenWebServer(string id, int port = 8900)
        {
            Id = id;
            Port = port;
            _restServer = new RestServer
            {
                Host = "*",
                Port = port.ToString()
            };
            _restServer.PublicFolders.Add(new PublicFolder("WebRoot", ""));
            _restServer.PublicFolders.Add(new PublicFolder("logs", "logs"));
        }

        public bool Start()
        {
            bool res = false;

            try
            {
                _restServer.Start();
                res = true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Cannot start!");
            }

            Logger.Info($"ScreenServer[{Id}] on {Port}");
            return res;
        }

        public void Stop()
        {
            _restServer.Stop();
        }
    }
}
