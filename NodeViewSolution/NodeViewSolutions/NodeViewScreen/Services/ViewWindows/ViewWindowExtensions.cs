﻿namespace NodeViewScreen.Services.ViewWindows
{
    public static class ViewWindowExtensions
    {
        public static IViewWindowModel GetViewWindowModel(this IViewWindow viewWindow)
        {
            return (IViewWindowModel)viewWindow.DataContext;
        }
    }
}
