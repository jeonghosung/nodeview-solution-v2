﻿using System;
using System.ComponentModel;
using System.Windows;
using NodeView.Drawing;

namespace NodeViewScreen.Services.ViewWindows
{
    /// <summary>
    /// Interface for a dialog hosting window.
    /// </summary>
    public interface IViewWindow
    {
        event RoutedEventHandler Loaded;
        event EventHandler Closed;
        event CancelEventHandler Closing;

        string Id { get; set; }
        IntRect WindowRect { get; set; }
        bool Topmost { get; set; }
        object Content { get; set; }
        void Show();
        object DataContext { get; set; }
        void Close();
    }
}
