﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using NLog;
using Prism.Common;
using Prism.Ioc;

namespace NodeViewScreen.Services.ViewWindows
{
    public class ViewWindowService : IViewWindowService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly IContainerExtension _containerExtension;

        public ViewWindowService(IContainerExtension containerExtension)
        {
            _containerExtension = containerExtension;
        }

        public IViewWindow Show(IViewWindowModel viewWindowModel, string windowName = null)
        {
            try
            {
                IViewWindow viewWindow = CreateWallWindow(windowName);
                if (viewWindow == null)
                {
                    return null;
                }
                viewWindow.Id = viewWindowModel.Id;
                viewWindow.DataContext = viewWindowModel;

                if (!ConfigureWallWindowEvents(viewWindow))
                {
                    viewWindow.Close();
                    return null;
                }
                if (viewWindowModel.WindowRect.IsEmpty)
                {
                    Logger.Error("WindowRect is empty.");
                    viewWindow.Close();
                    return null;

                }
                else
                {
                    viewWindow.Topmost = viewWindowModel.IsPopup;
                    viewWindow.WindowRect = viewWindowModel.WindowRect;
                }
                viewWindow.Show();
                return viewWindow;
            }
            catch (Exception e)
            {
                Logger.Error(e, "Show");
            }

            return null;
        }

        private bool ConfigureWallWindowEvents(IViewWindow wallWindow)
        {
            try
            {
                Action requestCloseHandler = null;
                requestCloseHandler = () =>
                {
                    wallWindow.Close();
                };

                RoutedEventHandler loadedHandler = null;
                loadedHandler = (o, e) =>
                {
                    wallWindow.Loaded -= loadedHandler;
                    wallWindow.GetViewWindowModel().RequestClose += requestCloseHandler;
                };
                wallWindow.Loaded += loadedHandler;

                CancelEventHandler closingHandler = null;
                closingHandler = (o, e) =>
                {
                    if (!wallWindow.GetViewWindowModel().CanCloseWindow())
                        e.Cancel = true;
                };
                wallWindow.Closing += closingHandler;

                EventHandler closedHandler = null;
                closedHandler = (o, e) =>
                {
                    wallWindow.Closed -= closedHandler;
                    wallWindow.Closing -= closingHandler;
                    try
                    {
                        wallWindow.GetViewWindowModel().RequestClose -= requestCloseHandler;
                        wallWindow.GetViewWindowModel().OnWindowClosed();
                    }
                    catch
                    {
                        //skip
                    }
                    wallWindow.DataContext = null;
                    wallWindow.Content = null;
                };
                wallWindow.Closed += closedHandler;
                return true;
            }
            catch (Exception e)
            {
                Logger.Error(e, "ConfigureWallWindowEvents:");
            }

            return false;
        }

        
        private IViewWindow CreateWallWindow(string name)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(name))
                {
                    return _containerExtension.Resolve<IViewWindow>();
                }
                return _containerExtension.Resolve<IViewWindow>(name);
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateWallWindow:");
            }
            return null;
        }
    }
}
