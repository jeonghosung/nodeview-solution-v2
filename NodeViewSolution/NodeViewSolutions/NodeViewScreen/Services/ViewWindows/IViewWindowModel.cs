﻿using System;
using NodeView.DataWalls;
using NodeView.Drawing;

namespace NodeViewScreen.Services.ViewWindows
{
    /// <summary>
    /// Interface that provides dialog functions and events to ViewModels.
    /// </summary>
    public interface IViewWindowModel : IDataWallWindow
    {
        event Action RequestClose;

        bool CanCloseWindow();

        void OnWindowClosed();

        void OnWindowOpened();

    }
}
