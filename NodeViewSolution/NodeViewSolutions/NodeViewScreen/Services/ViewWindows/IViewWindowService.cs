﻿namespace NodeViewScreen.Services.ViewWindows
{
    public interface IViewWindowService
    {
        IViewWindow Show(IViewWindowModel viewWindowModel, string windowName = null);
    }
}
