﻿using System.Windows;
using NLog;
using NodeView.Drawing;
using NodeView.Utils;

namespace NodeViewScreen.Services.ViewWindows
{
    /// <summary>
    /// ViewWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ViewWindow : Window, IViewWindow
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private IntRect _windowRect;
        public string Id { get; set; } = Uuid.NewUuid;

        public IntRect WindowRect
        {
            get => _windowRect;
            set
            {
                _windowRect = value;
                Top = _windowRect.Top;
                Left = _windowRect.Left;
                Width = _windowRect.Width;
                Height = _windowRect.Height;
            }
        }

        public ViewWindow()
        {
            InitializeComponent();
            
        }

    }
}
