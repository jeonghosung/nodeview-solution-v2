﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using NLog;
using NodeView.Drawing;
using NodeViewScreen.Models;

namespace NodeViewScreen.MixinWalls
{
    public class LeadtechWallClient
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        public bool IsTestMode { get; set; } = true;
        public bool IsVerbose { get; set; } = false;
        public string WallManagerIp { get; set; } = "127.0.0.1";
        public int WallManagerPort { get; set; } = 8882;
        public bool IsOpened { get; protected set; } = false;
        public const long NullWindowId = 0;
        public int LastResponseCode { get; private set; } = 0;


        public LeadtechWallClient(string ip = "127.0.0.1", int port = 8882)
        {
            WallManagerIp = ip;
            WallManagerPort = port;
            if (IsVerbose) Logger.Info($"LeadtechWallClient ('{WallManagerIp}',{WallManagerPort})");
        }

        public LeadtechWindow[] GetWindowList()
        {
            List<LeadtechWindow> windowList = new List<LeadtechWindow>();
            try
            {
                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0205);
                var response = RequestApi(request);
                if (response.IsSucceeded)
                {
                    int payloadLength = response.Payload.Length;
                    int offset = 0;
                    int itemPayloadLength = LeadtechWindow.PacketSize;
                    while (payloadLength  >= (offset + itemPayloadLength))
                    {
                        LeadtechWindow window = LeadtechWindow.CreateFrom(response.Payload, offset);
                        offset += itemPayloadLength;
                        if (window != null)
                        {
                            windowList.Add(window);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetWindowList:");
            }

            if (IsVerbose)
            {
                Logger.Info($"GetWindowList=====================");
                foreach (var window in windowList)
                {
                    Logger.Info($"[{window.WindowId}, ({window.WindowRect}), {window.ChannelName}");
                }
                Logger.Info($"==================================");
            }

            return windowList.ToArray();
        }

        public long CreateWindow(int channelType, string channelName, IntRect rect)
        {
            long windowId = NullWindowId;
            try
            {
                if (IsVerbose) Logger.Info($"LeadtechWallClient ('{channelName}',[{rect}])");

                List<byte> payloadByteList = new List<byte>();
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Left)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Top)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Right)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Bottom)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(channelType)));
                payloadByteList.AddRange(GetStringToBytes(channelName, 256));

                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0215, payloadByteList.ToArray());
                var response = RequestApi(request);
                if (response.IsSucceeded)
                {
                    if (response.Payload.Length >= 8)
                    {
                        windowId = BitConverter.ToInt64(response.Payload, 0);
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "CreateWindow:");
                windowId = NullWindowId;
            }

            return windowId;
        }

        public void CloseWindow(long windowId)
        {
            try
            {
                if (IsVerbose) Logger.Info($"CloseWindow ('{windowId}')");

                List<byte> payloadByteList = new List<byte>();
                payloadByteList.AddRange(BitConverter.GetBytes(windowId));

                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0217, payloadByteList.ToArray());
                var response = RequestApi(request);
            }
            catch (Exception e)
            {
                Logger.Error(e, "CloseWindow:");
            }
        }

        public bool MoveWindow(long windowId, IntRect rect)
        {
            bool res = false;
            try
            {
                if (IsVerbose) Logger.Info($"MoveWindow ('{windowId}',[{rect}])");

                List<byte> payloadByteList = new List<byte>();
                payloadByteList.AddRange(BitConverter.GetBytes(windowId));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Left)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Top)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Right)));
                payloadByteList.AddRange(BitConverter.GetBytes((int)(rect.Bottom)));

                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0207, payloadByteList.ToArray());
                var response = RequestApi(request);
                res = response.IsSucceeded;
            }
            catch (Exception e)
            {
                Logger.Error(e, "MoveWindow:");
                res = false;
            }

            return res;
        }

        public void CloseAllWindow()
        {
            try
            {
                if (IsVerbose) Logger.Info($"CloseAllWindow()");
                List<byte> payloadByteList = new List<byte>();
                payloadByteList.AddRange(GetStringToBytes("", 64));

                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0230, payloadByteList.ToArray());
                var response = RequestApi(request);
            }
            catch (Exception e)
            {
                Logger.Error(e, "CloseAllWindow:");
            }
        }

        public LeadtechChannel[] GetChannelList()
        {
            List<LeadtechChannel> channelList = new List<LeadtechChannel>();
            try
            {
                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0201);
                var response = RequestApi(request);
                if (response.IsSucceeded)
                {
                    int payloadLength = response.Payload.Length;
                    int offset = 0;
                    int itemPayloadLength = LeadtechChannel.PacketSize;
                    while (payloadLength >= (offset + itemPayloadLength))
                    {
                        LeadtechChannel channel = LeadtechChannel.CreateFrom(response.Payload, offset);
                        offset += itemPayloadLength;
                        if (channel != null)
                        {
                            channelList.Add(channel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetChannelList:");
            }

            return channelList.ToArray();
        }
        public string[] GetPresetList()
        {
            List<string> presetList = new List<string>();
            try
            {
                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0203);
                var response = RequestApi(request);
                if (response.IsSucceeded)
                {
                    int payloadLength = response.Payload.Length;
                    int offset = 0;
                    int itemPayloadLength = 256;
                    while (payloadLength >= (offset + itemPayloadLength))
                    {
                        string preset = Encoding.UTF8.GetString(response.Payload, offset, 256).TrimEnd('\0').Trim();
                        offset += itemPayloadLength;
                        if (!string.IsNullOrWhiteSpace(preset))
                        {
                            presetList.Add(preset);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "GetPresetList:");
            }

            return presetList.ToArray();
        }

        public bool RunPreset(string presetName)
        {
            bool res = false;
            try
            {
                List<byte> payloadByteList = new List<byte>();
                payloadByteList.AddRange(GetStringToBytes(presetName, 256));

                LeadtechManagerRequest request = new LeadtechManagerRequest(0x0211, payloadByteList.ToArray());
                var response = RequestApi(request);
                res = response.IsSucceeded;
            }
            catch (Exception e)
            {
                Logger.Error(e, "SetPreset:");
                res = false;
            }

            return res;
        }

        private LeadtechManagerResponse RequestApi(LeadtechManagerRequest request, bool hasResult = true)
        {
            var response = RequestApiCore(request, hasResult);
            int tryCount = 0;
            while (response.Result == 0x30c && tryCount < 4)
            {
                Thread.Sleep(500);
                response = RequestApiCore(request, hasResult);
                tryCount++;
            }

            LogLeadtechErrorResponse(request.Command, response.Result);
            LastResponseCode = response.Result;
            return response;
        }

        private void LogLeadtechErrorResponse(int command, int responseCode)
        {
            if (responseCode == 0) //Success
            {
                return;
            }
            
            if (responseCode == 0x1101)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 연결오류");
            }
            else if (responseCode == 0x301)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 요청 처리 실패");
            }
            else if (responseCode == 0x302)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 해당 Window ID를 가진 창이 없음");
            }
            else if (responseCode == 0x303)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 서버의 영역을 벗어남");
            }
            else if (responseCode == 0x304)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 잘못된 영역입니다.");
            }
            else if (responseCode == 0x305)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 해당 Preset이 존재하지 않음");
            }
            else if (responseCode == 0x306)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 해당 Schedule이 존재하지 않음");
            }
            else if (responseCode == 0x307)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] Schedule이 실행 중");
            }
            else if (responseCode == 0x308)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] Schedule 목록이 없음");
            }
            else if (responseCode == 0x309)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] Packet 오류");
            }
            else if (responseCode == 0x30a)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 잘못된 서버 이름");
            }
            else if (responseCode == 0x30b)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 접속된 서버 없음");
            }
            else if (responseCode == 0x30c)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 해당 Packet 처리 중");
            }
            else if (responseCode == 0x30d)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] Hold 상태로 인한 Packet 처리 실패");
            }
            else if (responseCode == 0x30e)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 해당 Channel이 존재하지 않음");
            }
            else if (responseCode == 0x30f)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] Channel type 오류");
            }
            else if (responseCode == 0x310)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 서버 요청 타임아웃");
            }
            else if (responseCode == 0x311)
            {
                Logger.Warn($"[0x{command:X} / 0x{responseCode:X}] 서버 접속 해제");
            }
        }

        private LeadtechManagerResponse RequestApiCore(LeadtechManagerRequest request, bool hasResult = true)
        {
            int result = 0;
            byte[] dataBytes = null;

            try
            {
                using (var tcpClient = new TcpClient())
                {
                    tcpClient.SendTimeout = 1000;
                    tcpClient.ReceiveTimeout = 3000;
                    tcpClient.Connect(WallManagerIp, WallManagerPort);

                    byte[] packet = request.Packet;
                    var stream = tcpClient.GetStream();
                    stream.Write(packet, 0, packet.Length);
                    //stream.ReadTimeout = request.Timeout;

                    int dataSize = 0;
                    byte[] headerBytes = new byte[12];
                    if (stream.Read(headerBytes, 0, headerBytes.Length) == headerBytes.Length)
                    {
                        int offset = 0;
                        int command = BitConverter.ToInt32(headerBytes, offset);
                        offset += 4;
                        if (command != (request.Command + 1))
                        {
                            Logger.Error($"Response command is not matched[{(request.Command + 1)}!={command}]!");
                            result = 0x10103; //Response command is not matched.
                        }
                        else
                        {
                            dataSize = BitConverter.ToInt32(headerBytes, offset);
                            offset += 4;
                        }

                        if (hasResult)
                        {
                            if (dataSize >= 4)
                            {
                                result = BitConverter.ToInt32(headerBytes, offset);
                                offset += 4;
                                if (result != 0)
                                {
                                    Logger.Error($"Response result [{result} != 0]");
                                }

                                dataSize -= 4;
                            }
                            else
                            {
                                result = 0x10102; // There is no result.
                            }
                        }
                    }
                    else
                    {
                        Logger.Error("Response header is not matched!");
                    }

                    if (dataSize > 0)
                    {
                        dataBytes = new byte[dataSize];
                        if (stream.Read(dataBytes, 0, dataBytes.Length) != dataBytes.Length)
                        {
                            Logger.Error("Response data is not matched!");
                            result = 0x10104; //Response data size is not matched.
                            dataBytes = new byte[0];
                        }
                    }
                    tcpClient.Close();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
                result = 0x1101; // internal error.
                dataBytes = new byte[0];
            }

            return new LeadtechManagerResponse(request.Command, result, dataBytes);
        }

        private static byte[] GetStringToBytes(string message, int bytesLength = -1, byte fillValue = 0x00, bool appendNullString = false)
        {
            //제대로 수정해야 함
            byte[] messageBytes = Encoding.UTF8.GetBytes(message);
            if (bytesLength > 0)
            {
                byte[] buffer = Enumerable.Repeat((byte)fillValue, bytesLength).ToArray();
                Array.Copy(messageBytes, buffer, messageBytes.Length);
                return buffer;
            }
            else
            {
                return messageBytes;
            }
        }
    }

    public class LeadtechChannel
    {
        public const int PacketSize = 260;
        public string ChannelName { get; }
        public int ChannelType { get; }

        public LeadtechChannel(string channelName, int channelType)
        {
            ChannelName = channelName;
            ChannelType = channelType;
        }

        public static LeadtechChannel CreateFrom(byte[] dataBytes, int offset = 0)
        {
            int channelType = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            string channelName = Encoding.UTF8.GetString(dataBytes, offset, 256).TrimEnd('\0');
            return new LeadtechChannel(channelName, channelType);
        }
    }

    public class LeadtechWindow
    {
        public const int PacketSize = 288;
        public long WindowId { get; }
        public string ChannelName { get; }
        public int ChannelType { get; }
        public IntRect WindowRect { get; }
        public int ZOrder { get; }

        public LeadtechWindow(long windowId, string channelName, int channelType, IntRect windowRect, int zOrder)
        {
            WindowId = windowId;
            ChannelName = channelName;
            ChannelType = channelType;
            WindowRect = windowRect;
            ZOrder = zOrder;
        }

        public static LeadtechWindow CreateFrom(byte[] dataBytes, int offset = 0)
        {
            long windowId = BitConverter.ToInt64(dataBytes, offset);
            offset += 8;
            int winPosLeft = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            int winPosTop = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            int winPosRight = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            int winPosBottom = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            int channelType = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            int zorder = BitConverter.ToInt32(dataBytes, offset);
            offset += 4;
            string channelName = Encoding.UTF8.GetString(dataBytes, offset, 256).TrimEnd('\0');
            return new LeadtechWindow(windowId, channelName, channelType, new IntRect(winPosLeft, winPosTop, winPosRight-winPosLeft, winPosBottom-winPosTop), zorder);
        }
    }

    public class LeadtechManagerRequest
    {
        public int Command { get; }
        public byte[] Payload { get; }
        public int Timeout { get; set; } = 1000;
        public byte[] Packet => GetPacket();

        public LeadtechManagerRequest(int command, byte[] payload = null)
        {
            Command = command;
            Payload = payload ?? new byte[0];
        }

        private byte[] GetPacket()
        {
            List<byte> packetByteList = new List<byte>();
            packetByteList.AddRange(BitConverter.GetBytes((int)(Command)));
            packetByteList.AddRange(BitConverter.GetBytes((int)(Payload.Length)));
            if (Payload.Length > 0)
            {
                packetByteList.AddRange(Payload);
            }
            return packetByteList.ToArray();
        }

    }

    public class LeadtechManagerResponse
    {
        public int RequestCommand { get; }
        public int Result { get; }
        public byte[] Payload { get; }
        public bool IsSucceeded => Result == 0;

        public LeadtechManagerResponse(int requestCommand, int result, byte[] payload = null)
        {
            RequestCommand = requestCommand;
            Result = result;
            Payload = payload ?? new byte[0];
        }
    }
}
