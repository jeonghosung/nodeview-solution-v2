﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeViewScreen.Models;

namespace NodeViewScreen.MixinWalls
{
    public class LeadtechMixinWall : IMixinWall
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private LeadtechWallClient _leadtechWallClient = null;

        public bool IsTestMode { get; set; } = true;
        public bool IsVerbose { get; set; } = false;
        public bool IsPopupSupported { get; } = false;
        public IntPoint ScreenOffset { get; set; } = new IntPoint(0, 0);
        public string WallManagerIp { get; set; } = "127.0.0.1";
        public int WallManagerPort { get; set; } = 8882;
        public bool IsConnected { get; protected set; } = true;
        public const long NullWindowId = 0;
        public int LastResponseCode { get; private set; } = 0;


        public LeadtechMixinWall(string ip = "127.0.0.1", int port = 8882)
        {
            WallManagerIp = ip;
            WallManagerPort = port;
            if (IsVerbose) Logger.Info($"Start LeadtechMixinWall ('{WallManagerIp}',{WallManagerPort})");

            _leadtechWallClient = new LeadtechWallClient(WallManagerIp, WallManagerPort);
        }

        public void Clear()
        {
            if (IsVerbose)
            {
                Logger.Info("Clear");
            }
            if(!IsTestMode)
            {
                CloseAllWindows();
            }
        }

        public IScreenWindow OpenWindow(DataWallWindow dataWallWindow)
        {
            if (dataWallWindow == null) return null;

            LeadtechScreenWindow screenWindow = null;
            if (IsVerbose)
            {
                Logger.Info("OpenWindow");
            }
            // ChannelType : 1(RGBX), 2(NETPC), 3(IPCAM), 4(TICKER)
            // 현재 Content 설정에서 Agent(NetPC)를 remote로 설정함
            var contentType = 3;
            if (dataWallWindow.Content.ContentType.Equals("remote", StringComparison.OrdinalIgnoreCase))
            {
                contentType = 2;
            }
            if (IsTestMode)
            {
                IntRect realRect = new IntRect(dataWallWindow.WindowRect);
                realRect.Offset(ScreenOffset.X, ScreenOffset.Y);
                long windowId = 0;
                LeadtechWindow leadtechWindow = new LeadtechWindow(windowId, dataWallWindow.ContentId, contentType, realRect, 1);
                screenWindow = new LeadtechScreenWindow(leadtechWindow, dataWallWindow);
            }
            else
            {
                //        public long CreateWindow(int channelType, string channelName, IntRect rect)
                IntRect realRect = new IntRect(dataWallWindow.WindowRect);
                realRect.Offset(ScreenOffset.X, ScreenOffset.Y);
                long windowId = _leadtechWallClient.CreateWindow(contentType, dataWallWindow.ContentId, realRect);
                if (windowId != NullWindowId)
                {
                    LeadtechWindow leadtechWindow = new LeadtechWindow(windowId, dataWallWindow.ContentId, contentType, realRect, 1);
                    screenWindow = new LeadtechScreenWindow(leadtechWindow, dataWallWindow);
                }
            }

            return screenWindow;
        }

        public void CloseWindow(IScreenWindow screenWindow)
        {
            if (screenWindow == null) return;

            if (IsVerbose)
            {
                Logger.Info("CloseWindow");
            }

            if (!IsTestMode)
            {
                if (screenWindow is LeadtechScreenWindow leadtechScreenWindow)
                {
                    _leadtechWallClient.CloseWindow(leadtechScreenWindow.LeadtechWindow.WindowId);
                }
                else
                {
                    Logger.Error($"screenWinodw({screenWindow.Id}) is not LeadtechScreenWindow.");
                }
            }
        }

        public void CloseAllWindows()
        {
            if (IsVerbose)
            {
                Logger.Info("CloseAllWindows");
            }
            if (!IsTestMode)
            {
                _leadtechWallClient.CloseAllWindow();
            }
        }

        public void CloseAllPopups()
        {
            if (IsVerbose)
            {
                Logger.Warn("'CloseAllPopups' is not supported.");
            }
        }

        public void MoveWindow(IScreenWindow screenWindow, IntRect windowRect)
        {
            if(screenWindow == null) return;
            if (IsVerbose)
            {
                Logger.Info("MoveWindow");
            }

            if (!IsTestMode)
            {
                if (screenWindow is LeadtechScreenWindow leadtechScreenWindow)
                {
                    _leadtechWallClient.CloseWindow(leadtechScreenWindow.LeadtechWindow.WindowId);
                }
                else
                {
                    Logger.Error($"screenWinodw({screenWindow.Id}) is not LeadtechScreenWindow.");
                }
            }
        }
    }
}
