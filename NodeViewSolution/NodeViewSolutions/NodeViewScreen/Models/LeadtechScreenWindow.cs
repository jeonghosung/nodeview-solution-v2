﻿using System;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeViewScreen.MixinWalls;

namespace NodeViewScreen.Models
{
    public class LeadtechScreenWindow : IScreenWindow
    {
        public string Id => ScreenWindow?.Id ?? "";
        public IntRect WindowRect => ScreenWindow?.WindowRect ?? new IntRect(0, 0, 0, 0);

        public LeadtechWindow LeadtechWindow { get; }
        public DataWallWindow ScreenWindow { get; }
        public LeadtechScreenWindow(LeadtechWindow leadtechWindow, DataWallWindow window)
        {
            LeadtechWindow = leadtechWindow;
            ScreenWindow = window;
        }
        public DataWallWindow GetDataWallWindow()
        {
            return ScreenWindow;
        }
    }
}