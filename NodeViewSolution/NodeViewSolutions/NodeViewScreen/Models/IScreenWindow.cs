﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeViewScreen.Services.ViewWindows;

namespace NodeViewScreen.Models
{
    public interface IScreenWindow
    {
        string Id { get; }
        IntRect WindowRect { get; }
        DataWallWindow GetDataWallWindow();
    }
}
