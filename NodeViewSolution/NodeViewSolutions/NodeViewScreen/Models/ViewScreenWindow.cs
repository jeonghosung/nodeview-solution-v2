﻿using NodeView.DataWalls;
using NodeView.Drawing;
using NodeViewScreen.Services.ViewWindows;

namespace NodeViewScreen.Models
{
    internal class ViewScreenWindow : IScreenWindow
    {
        public string Id => ScreenWindow?.Id ?? "";

        public IntRect WindowRect => ScreenWindow?.WindowRect ?? new IntRect(0, 0, 0, 0);

        public IViewWindow ViewWindow { get; }
        public DataWallWindow ScreenWindow { get; }

        public ViewScreenWindow(IViewWindow viewWindow, DataWallWindow window)
        {
            ViewWindow = viewWindow;
            ScreenWindow = window;
        }
        public IViewWindow GetViewWindow()
        {
            return ViewWindow;
        }

        public DataWallWindow GetDataWallWindow()
        {
            return ScreenWindow;
        }
    }
}