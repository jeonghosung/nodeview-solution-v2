﻿using NodeView.DataWalls;
using NodeView.Drawing;

namespace NodeViewScreen.Models
{
    public interface IMixinWall
    {
        bool IsTestMode { get; set; }
        bool IsVerbose { get; set; }
        bool IsPopupSupported { get; }
        IntPoint ScreenOffset { get; set; }
        void Clear();
        IScreenWindow OpenWindow(DataWallWindow dataWallWindow);
        void CloseWindow(IScreenWindow screenWindow);
        void CloseAllWindows();
        void CloseAllPopups();
        void MoveWindow(IScreenWindow screenWindow, IntRect windowRect);
    }
}
