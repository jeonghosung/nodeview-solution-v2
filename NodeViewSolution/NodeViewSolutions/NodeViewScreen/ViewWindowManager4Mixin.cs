﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeViewScreen.EventModels;
using NodeViewScreen.MixinWalls;
using NodeViewScreen.Models;
using NodeViewScreen.Services.ViewWindows;
using NodeViewScreen.ViewModels;
using Prism.Events;

namespace NodeViewScreen
{
    public class ViewWindowManager4Mixin : IViewWindowManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IEventAggregator _eventAggregator = null;
        private IViewWindowService _viewWindowService = null;

        private IntRect _screenRect = new IntRect(0, 0, 1920, 1080);
        private readonly IConfiguration _configuration;
        private IMixinWall _mixinWall = null;
        private string[] _mixinWallContentTypes = new string[0];
        private readonly List<IScreenWindow> _screenWindowList = new List<IScreenWindow>();
        private readonly List<IScreenWindow> _popupViewWindowList = new List<IScreenWindow>();



        //public ViewWindowManager(IntRect screenRect, IViewWindowService viewWindowService, IEventAggregator eventAggregator)
        public ViewWindowManager4Mixin(IntRect screenRect, IConfiguration configuration,
            IViewWindowService viewWindowService, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _viewWindowService = viewWindowService;
            _screenRect = screenRect;
            _configuration = configuration;
            SetupMixinWall(configuration);
        }

        private void SetupMixinWall(IConfiguration configuration)
        {
            if (configuration == null)
            {
                return;
            }

            bool usingMixinWall = configuration.GetValue("mixinWall.isEnabled", false);
            if (!usingMixinWall)
            {
                return;
            }

            string mixinWallType = configuration.GetValue("mixinWall.type", "");
            if (string.IsNullOrWhiteSpace(mixinWallType))
            {
                Logger.Error("mixinWall.type isEmpty.");
            }
            else if (mixinWallType == "leadtech")
            {
                string ip = configuration.GetValue("mixinWall.leadtech.ip", "127.0.0.1");
                int port = configuration.GetValue("mixinWall.leadtech.port", 8882);
                _mixinWall = new LeadtechMixinWall(ip, port);

            }
            else
            {
                Logger.Error($"[{mixinWallType}] is not supported.");
            }

            if (_mixinWall != null)
            {
                bool isTestMode = configuration.GetValue("mixinWall.isTestMode", true);
                bool isVerbose = configuration.GetValue("mixinWall.isVerbose", isTestMode);
                _mixinWallContentTypes = StringUtils.Split(configuration.GetValue("mixinWall.contentTypes", ""), ',');
                _mixinWall.IsTestMode = isTestMode;
                _mixinWall.IsVerbose = isVerbose;
                _mixinWall.ScreenOffset = _screenRect.LeftTop;
            }
        }

        public void Start()
        {
            _mixinWall?.Clear();
        }

        public void Stop()
        {
            _mixinWall?.Clear();
        }

        public DataWallWindow[] GetWindows()
        {
            var screenWindows = _screenWindowList.ToArray();
            List<DataWallWindow> nodeViewWindowList = new List<DataWallWindow>();
            foreach (var screenWindow in screenWindows)
            {
                DataWallWindow dataWallWindow = screenWindow.GetDataWallWindow();
                if (dataWallWindow != null)
                {
                    nodeViewWindowList.Add(dataWallWindow);
                }
            }

            return nodeViewWindowList.ToArray();
        }

        public void CreateWindows(DataWallWindow[] createWindows)
        {
            if (createWindows == null || createWindows.Length == 0)
            {
                return;
            }

            string[] deleteIds = createWindows.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            foreach (var createWindow in createWindows)
            {
                try
                {
                    CloseWindowsInRectInternal(createWindow.WindowRect);
                    var screenWindow = OpenWindow(createWindow);
                    if (screenWindow != null)
                    {
                        _screenWindowList.Add(screenWindow);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, "CreateWindows:");
                }
            }

            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                .Publish(new ScreenWindowListUpdatedEventArg(true, false));
        }

        public void CloseWindowsInRect(IntRect rect)
        {
            if (rect.IsEmpty)
            {
                return;
            }

            int closeWindowCount = CloseWindowsInRectInternal(rect);

            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                    .Publish(new ScreenWindowListUpdatedEventArg(true, false));
            }
        }

        private int CloseWindowsInRectInternal(IntRect rect)
        {
            if (rect.IsEmpty)
            {
                return 0;
            }

            IntRect actualRect = new IntRect(rect);
            actualRect.Offset(_screenRect.X, _screenRect.Y);

            int closeWindowCount = 0;
            var windows = _screenWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(actualRect))
                {
                    closeWindowCount++;
                    _screenWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            return closeWindowCount;
        }

        public void CloseWindows(string[] deleteWindowIds)
        {
            if (deleteWindowIds == null || deleteWindowIds.Length == 0)
            {
                return;
            }

            int closeWindowCount = CloseWindowsInternal(deleteWindowIds);

            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                    .Publish(new ScreenWindowListUpdatedEventArg(true, false));
            }
        }

        public int CloseWindowsInternal(string[] closeIds)
        {
            int updatedCount = 0;
            if (closeIds == null || closeIds.Length == 0)
            {
                return updatedCount;
            }

            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string id in closeIds)
            {
                idDictionary[id] = id;
            }

            var screenWindows = _screenWindowList.ToArray();
            foreach (var screenWindow in screenWindows)
            {
                if (idDictionary.TryGetValue(screenWindow.Id, out var outValue))
                {
                    updatedCount++;
                    _screenWindowList.Remove(screenWindow);
                    CloseWindow(screenWindow);
                }
            }

            return updatedCount;
        }

        public void CloseAllWindows()
        {
            var screenWindows = _screenWindowList.ToArray();
            _screenWindowList.Clear();
            foreach (var screenWindow in screenWindows)
            {
                if (screenWindow is ViewScreenWindow viewScreenWindow) //IViewWindow
                {
                    CloseWindow(screenWindow);
                }
                else // 일단 mixinWall도 개별 클로즈를 시도해보자.
                {
                    CloseWindow(screenWindow);
                }
            }
            _mixinWall?.CloseAllWindows(); //그러나 CloseAllWindows()도 호출한다. 외부에서 오픈한 창까지 지우기 위해!

            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                .Publish(new ScreenWindowListUpdatedEventArg(true, false));
        }

        public DataWallWindow[] GetPopups()
        {
            var screenWindows = _popupViewWindowList.ToArray();
            List<DataWallWindow> nodeViewWindowList = new List<DataWallWindow>();
            foreach (var screenWindow in screenWindows)
            {
                var dataWallWindow = screenWindow.GetDataWallWindow();
                if (dataWallWindow != null)
                {
                    nodeViewWindowList.Add(dataWallWindow);
                }
            }

            return nodeViewWindowList.ToArray();
        }

        public void CreatePopups(DataWallWindow[] createPopups)
        {
            if (createPopups == null || createPopups.Length == 0)
            {
                return;
            }

            string[] deleteIds = createPopups.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            foreach (var createPopup in createPopups)
            {
                try
                {
                    var viewWindow = OpenWindow(createPopup);
                    if (viewWindow != null)
                    {
                        _popupViewWindowList.Add(viewWindow);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, "CreatePopups:");
                }
            }

            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                .Publish(new ScreenWindowListUpdatedEventArg(false, true));

        }

        public void ClosePopupsInRect(IntRect rect)
        {
            if (rect.IsEmpty)
            {
                return;
            }

            IntRect actualRect = new IntRect(rect);
            actualRect.Offset(_screenRect.X, _screenRect.Y);

            int closeWindowCount = 0;
            var windows = _popupViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(actualRect))
                {
                    closeWindowCount++;
                    _popupViewWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                    .Publish(new ScreenWindowListUpdatedEventArg(false, true));
            }
        }

        public void ClosePopups(string[] closeIds)
        {
            if (closeIds == null || closeIds.Length == 0)
            {
                return;
            }

            int closeWindowCount = ClosePopupsInternal(closeIds);
            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                    .Publish(new ScreenWindowListUpdatedEventArg(true, false));
            }
        }

        public int ClosePopupsInternal(string[] closeIds)
        {
            int updatedCount = 0;
            if (closeIds == null || closeIds.Length == 0)
            {
                return updatedCount;
            }

            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string id in closeIds)
            {
                idDictionary[id] = id;
            }

            var windows = _popupViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    updatedCount++;
                    _popupViewWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            return updatedCount;
        }

        public void CloseAllPopups()
        {
            var screenWindows = _popupViewWindowList.ToArray();
            _popupViewWindowList.Clear();
            foreach (var screenWindow in screenWindows)
            {
                if (screenWindow is ViewScreenWindow viewScreenWindow) //IViewWindow
                {
                    CloseWindow(screenWindow);
                }
                else // 일단 mixinWall도 개별 클로즈를 시도해보자.
                {
                    CloseWindow(screenWindow);
                }
            }

            if (_mixinWall?.IsPopupSupported ?? false)
            {
                _mixinWall?.CloseAllPopups(); //그러나 CloseAllWindows()도 호출한다. 외부에서 오픈한 창까지 지우기 위해!
            }

            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                .Publish(new ScreenWindowListUpdatedEventArg(false, true));
        }


        public void MovePopup(string windowId, IntRect windowRect)
        {
            var screenWindows = _popupViewWindowList.ToArray();
            foreach (var screenWindow in screenWindows)
            {
                if (screenWindow.Id == windowId)
                {
                    if (screenWindow is ViewScreenWindow viewScreenWindow) //IViewWindow
                    {
                        var realRect = new IntRect(windowRect);
                        realRect.Offset(_screenRect.X, _screenRect.Y);
                        viewScreenWindow.ViewWindow.WindowRect = realRect;
                        viewScreenWindow.ScreenWindow.WindowRect = windowRect.Clone();
                    }
                    else // MixinWindow
                    {
                        _mixinWall?.MoveWindow(screenWindow, windowRect);
                    }
                    
                    _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>()
                        .Publish(new ScreenWindowListUpdatedEventArg(false, true));
                    break;
                }
            }
        }

        private IScreenWindow OpenWindow(DataWallWindow dataWallWindow)
        {
            IScreenWindow screenWindow = null;
            if (dataWallWindow == null || dataWallWindow.Content == null)
            {
                return null;
            }
            

            if (_mixinWall != null && _mixinWallContentTypes.Contains(dataWallWindow.Content.ContentType))
            {
                if (dataWallWindow.IsPopup)
                {
                    if (_mixinWall.IsPopupSupported)
                    {
                        screenWindow = _mixinWall.OpenWindow(dataWallWindow);
                    }
                }
                else
                {
                    screenWindow = _mixinWall.OpenWindow(dataWallWindow);
                }
            }

            if(screenWindow == null)
            {
                var viewWindowModel = new ViewWindowViewModel(dataWallWindow);
                viewWindowModel.WindowRect.Offset(_screenRect.X, _screenRect.Y);
                var viewWindow = _viewWindowService.Show(viewWindowModel);
                if (viewWindow != null)
                {
                    screenWindow = new ViewScreenWindow(viewWindow, dataWallWindow);
                }
            }
            return screenWindow;
        }

        private void CloseWindow(IScreenWindow screenWindow)
        {
            if (screenWindow == null)
            {
                return;
            }
            try
            {
                if (screenWindow is ViewScreenWindow viewScreenWindow) //IViewWindow
                {
                    viewScreenWindow.ViewWindow?.Close();
                }
                else // MixinWindow
                {
                    _mixinWall?.CloseWindow(screenWindow);
                }
            }
            catch
            {
                //skip
            }
        }
    }
}
