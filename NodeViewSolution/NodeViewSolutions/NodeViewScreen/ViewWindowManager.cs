﻿
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeViewScreen.EventModels;
using NodeViewScreen.Services.ViewWindows;
using NodeViewScreen.ViewModels;
using Prism.Events;

namespace NodeViewScreen
{
    public class ViewWindowManager : IViewWindowManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private IEventAggregator _eventAggregator = null;
        private IViewWindowService _viewWindowService = null;

        private IntRect _screenRect = new IntRect(0, 0, 1920, 1080);
        private readonly List<IViewWindow> _screenViewWindowList = new List<IViewWindow>();
        private readonly List<IViewWindow> _popupViewWindowList = new List<IViewWindow>();



        //public ViewWindowManager(IntRect screenRect, IViewWindowService viewWindowService, IEventAggregator eventAggregator)
        public ViewWindowManager(IntRect screenRect, IViewWindowService viewWindowService, IEventAggregator eventAggregator)
        {
            _eventAggregator = eventAggregator;
            _viewWindowService = viewWindowService;
            _screenRect = screenRect;
        }

        public DataWallWindow[] GetWindows()
        {
            var windows = _screenViewWindowList.ToArray();
            List<DataWallWindow> nodeViewWindowList = new List<DataWallWindow>();
            foreach (var window in windows)
            {
                var viewWindowModel = window.GetViewWindowModel();
                var nodeViewWindow = new DataWallWindow(viewWindowModel);
                IntRect rect = nodeViewWindow.WindowRect;
                rect.Offset(-_screenRect.X, -_screenRect.Y);
                nodeViewWindow.WindowRect = rect;
                nodeViewWindowList.Add(nodeViewWindow);
            }
            return nodeViewWindowList.ToArray();
        }

        public void CreateWindows(DataWallWindow[] createWindows)
        {
            if (createWindows == null || createWindows.Length == 0)
            {
                return;
            }

            string[] deleteIds = createWindows.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            foreach (var createWindow in createWindows)
            {
                try
                {
                    CloseWindowsInRectInternal(createWindow.WindowRect);
                    var viewWindow = OpenWindow(createWindow);
                    if (viewWindow != null)
                    {
                        _screenViewWindowList.Add(viewWindow);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, "CreateWindows:");
                }
            }
            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(true, false));
        }

        public void CloseWindowsInRect(IntRect rect)
        {
            if (rect.IsEmpty)
            {
                return;
            }
            int closeWindowCount = CloseWindowsInRectInternal(rect);

            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(true, false));
            }
        }
        private int CloseWindowsInRectInternal(IntRect rect)
        {
            if (rect.IsEmpty)
            {
                return 0;
            }
            IntRect actualRect = new IntRect(rect);
            actualRect.Offset(_screenRect.X, _screenRect.Y);

            int closeWindowCount = 0;
            var windows = _screenViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(actualRect))
                {
                    closeWindowCount++;
                    _screenViewWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            return closeWindowCount;
        }

        public void CloseWindows(string[] deleteWindowIds)
        {
            if (deleteWindowIds == null || deleteWindowIds.Length == 0)
            {
                return;
            }

            int closeWindowCount = CloseWindowsInternal(deleteWindowIds);
            
            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(true, false));
            }
        }
        public int CloseWindowsInternal(string[] closeIds)
        {
            int updatedCount = 0;
            if (closeIds == null || closeIds.Length == 0)
            {
                return updatedCount;
            }

            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string id in closeIds)
            {
                idDictionary[id] = id;
            }

            var windows = _screenViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    updatedCount++;
                    _screenViewWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            return updatedCount;
        }

        public void CloseAllWindows()
        {
            var windows = _screenViewWindowList.ToArray();
            _screenViewWindowList.Clear();
            foreach (var window in windows)
            {
                CloseWindow(window);
            }
            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(true, false));
        }

        public DataWallWindow[] GetPopups()
        {
            var windows = _popupViewWindowList.ToArray();
            List<DataWallWindow> nodeViewWindowList = new List<DataWallWindow>();
            foreach (var window in windows)
            {
                var viewWindowModel = window.GetViewWindowModel();
                var nodeViewWindow = new DataWallWindow(viewWindowModel);
                nodeViewWindow.WindowRect.Offset(-_screenRect.X, -_screenRect.Y);
                nodeViewWindowList.Add(nodeViewWindow);
            }
            return nodeViewWindowList.ToArray();
        }

        public void CreatePopups(DataWallWindow[] createPopups)
        {
            if (createPopups == null || createPopups.Length == 0)
            {
                return;
            }

            string[] deleteIds = createPopups.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            foreach (var createPopup in createPopups)
            {
                try
                {
                    var viewWindow = OpenWindow(createPopup);
                    if (viewWindow != null)
                    {
                        _popupViewWindowList.Add(viewWindow);
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e, "CreatePopups:");
                }
            }
            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(false, true));

        }

        public void ClosePopupsInRect(IntRect rect)
        {
            if (rect.IsEmpty)
            {
                return;
            }

            IntRect actualRect = new IntRect(rect);
            actualRect.Offset(_screenRect.X, _screenRect.Y);

            int closeWindowCount = 0;
            var windows = _popupViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(actualRect))
                {
                    closeWindowCount++;
                    _popupViewWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(false, true));
            }
        }
        public void ClosePopups(string[] closeIds)
        {
            if (closeIds == null || closeIds.Length == 0)
            {
                return;
            }

            int closeWindowCount = ClosePopupsInternal(closeIds);
            if (closeWindowCount > 0)
            {
                _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(true, false));
            }
        }

        public int ClosePopupsInternal(string[] closeIds)
        {
            int updatedCount = 0;
            if (closeIds == null || closeIds.Length == 0)
            {
                return updatedCount;
            }

            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string id in closeIds)
            {
                idDictionary[id] = id;
            }

            var windows = _popupViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    updatedCount++;
                    _popupViewWindowList.Remove(window);
                    CloseWindow(window);
                }
            }

            return updatedCount;
        }

        public void CloseAllPopups()
        {
            var windows = _popupViewWindowList.ToArray();
            _popupViewWindowList.Clear();
            foreach (var window in windows)
            {
                CloseWindow(window);
            }
            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(false, true));
        }


        public void MovePopup(string windowId, IntRect windowRect)
        {
            var windows = _popupViewWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.Id == windowId)
                {
                    var realRect = new IntRect(windowRect);
                    realRect.Offset(_screenRect.X, _screenRect.Y);
                    window.WindowRect = realRect;
                    _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Publish(new ScreenWindowListUpdatedEventArg(false, true));
                    break;
                }
            }
        }

        public void Start()
        {
            
        }

        public void Stop()
        {
            
        }

        private IViewWindow OpenWindow(DataWallWindow window)
        {
            if (window == null)
            {
                return null;
            }
            var viewWindowModel = new ViewWindowViewModel(window);
            viewWindowModel.WindowRect.Offset(_screenRect.X, _screenRect.Y);
            var viewWindow = _viewWindowService.Show(viewWindowModel);
            return viewWindow;
        }

        private void CloseWindow(IViewWindow window)
        {
            try
            {
                window.Close();
            }
            catch
            {
                //skip
            }
        }
    }
}
