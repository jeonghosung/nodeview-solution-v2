﻿using Prism.Events;

namespace NodeViewScreen.EventModels
{
    public class ScreenWindowListUpdatedEvent : PubSubEvent<ScreenWindowListUpdatedEventArg>
    {
    }

    public class ScreenWindowListUpdatedEventArg
    {
        public bool IsWindowListUpdated { get; set; } = false;
        public bool IsPopupListUpdated { get; set; } = false;

        public ScreenWindowListUpdatedEventArg(bool isWindowListUpdated, bool isPopupListUpdated)
        {
            IsWindowListUpdated = isWindowListUpdated;
            IsPopupListUpdated = isPopupListUpdated;
        }
    }
}
