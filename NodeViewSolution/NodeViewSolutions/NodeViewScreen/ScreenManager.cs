﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Threading;
using Newtonsoft.Json.Linq;
using NLog;
using NodeView.DataModels;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Ioc;
using NodeView.Models;
using NodeView.Net;
using NodeView.Systems;
using NodeView.Tasks;
using NodeView.Utils;
using NodeViewScreen.EventModels;
using NodeViewScreen.Services.ViewWindows;
using Prism.Events;
using Prism.Ioc;

namespace NodeViewScreen
{
    public class ScreenManager
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #region SingleTone
        private static readonly Lazy<ScreenManager> Lazy = new Lazy<ScreenManager>(() => new ScreenManager());
        public static ScreenManager Instance => Lazy.Value;
        #endregion

        private bool _isTestMode = false;
        private int _wallServerPort = 20105;
        private string _id;
        private int _port;
        private RestApiClient _dataWallProxyApiClient;

        private DispatcherTimer _actionQueueWorkerTimer = new DispatcherTimer();
        private readonly ConcurrentQueue<Action> _actionQueue = new ConcurrentQueue<Action>();

        private IEventAggregator _eventAggregator = null;
        private IViewWindowManager _viewWindowManager = null;

        public IConfiguration Configuration { get; set; } = null;
        public OsdConfig DefaultOsdConfig { get; private set; } = new OsdConfig();
        public readonly List<string> InitErrorMessages = new List<string>();
        public bool HasInitError => InitErrorMessages.Count > 0;

        private IntRect _actualScreenRect = new IntRect(0, 0, 1920, 1080);
        private List<DataWallWindow> _screenWindowList = new List<DataWallWindow>();
        private List<DataWallWindow> _popupWindowList = new List<DataWallWindow>();

        public void Initialize(string id, int port, IConfiguration configuration, IEventAggregator eventAggregator)
        {
            _id = id;
            _port = port;
            Configuration = configuration;
            _eventAggregator = eventAggregator;

            _isTestMode = configuration.GetValue("isTestMode", _isTestMode);
            _wallServerPort = configuration.GetValue("service.port", _wallServerPort);
            _dataWallProxyApiClient = new RestApiClient($"http://127.0.0.1:{_wallServerPort}/proxy/v1");

            AreaAlignment screenAlignment = configuration.GetValue("screen.alignment", AreaAlignment.LeftTop);
            IntPoint screenOffset = configuration.GetValue("screen.offset", new IntPoint(0, 0));
            IntSize screenSize = configuration.GetValue("screen.size", new IntSize(0, 0));

            //DefaultOsdConfig
            {
                DefaultOsdConfig.IsShow = configuration.GetValue("osd.isShow", DefaultOsdConfig.IsShow);
                DefaultOsdConfig.Alignment = configuration.GetValue("osd.alignment", DefaultOsdConfig.Alignment);
                DefaultOsdConfig.Offset = configuration.GetValue("osd.offset", DefaultOsdConfig.Offset);
                DefaultOsdConfig.Opacity = configuration.GetValue("osd.opacity", DefaultOsdConfig.Opacity);
                DefaultOsdConfig.UseDropShadowEffect = configuration.GetValue("osd.useDropShadowEffect", DefaultOsdConfig.UseDropShadowEffect);
                DefaultOsdConfig.FontSize = configuration.GetValue("osd.fontSize", DefaultOsdConfig.FontSize);
                DefaultOsdConfig.Color = configuration.GetValue("osd.color", DefaultOsdConfig.Color);
            }

            _actualScreenRect = SystemScreenUtils.GetActualScreenRect(screenAlignment, screenOffset, screenSize);

            IContainerProvider containerProvider = NodeViewContainerExtension.GetContainerProvider();
            List<(Type Type, object Instance)> paramList = new List<(Type Type, object Instance)>();
            paramList.Add((typeof(IntRect), _actualScreenRect));
            paramList.Add((typeof(IConfiguration), configuration));
            _viewWindowManager = containerProvider.Resolve<IViewWindowManager>(paramList.ToArray());

            _eventAggregator.GetEvent<ScreenWindowListUpdatedEvent>().Subscribe(OnViewWindowListUpdatedEvent);
        }

        public void Start()
        {
            //bool isJoined = JoinToDataWall();
            _actionQueueWorkerTimer.Tick += ActionQueueWorkerTimerOnTick;
            _actionQueueWorkerTimer.Interval = TimeSpan.FromSeconds(0.5);
            _actionQueueWorkerTimer.Start();
            _viewWindowManager.Start();
        }

        public bool JoinToDataWall()
        {
            bool res = false;
            JObject requestJson = new JObject()
            {
                ["action"] = "join",
                ["param"] = new JObject()
                {
                    ["id"] = _id,
                    ["port"] = _port,
                    ["pid"] = Process.GetCurrentProcess().Id
                }
            };
            var response = _dataWallProxyApiClient.Put("/screens", requestJson);
            if (response.IsSuccess)
            {
                try
                {
                    List<DataWallWindow> windowList = new List<DataWallWindow>();
                    List<DataWallWindow> popupList = new List<DataWallWindow>();

                    JObject jsonResponse = response.ResponseJsonObject;
                    JArray windowArray = JsonUtils.GetValue<JArray>(jsonResponse, "windows", null);
                    if (windowArray != null)
                    {
                        foreach (JToken token in windowArray)
                        {
                            JObject windowObject = token.Value<JObject>();
                            if (windowObject != null)
                            {
                                var window = DataWallWindow.CreateFrom(windowObject);
                                if (window != null)
                                {
                                    windowList.Add(window);
                                }
                            }
                        }
                    }
                    JArray popupArray = JsonUtils.GetValue<JArray>(jsonResponse, "popups", null);
                    if (windowArray != null)
                    {
                        foreach (JToken token in popupArray)
                        {
                            JObject windowObject = token.Value<JObject>();
                            if (windowObject != null)
                            {
                                var window = DataWallWindow.CreateFrom(windowObject);
                                if (window != null)
                                {
                                    popupList.Add(window);
                                }
                            }
                        }
                    }

                    CreateWindows(windowList.ToArray());
                    CreatePopups(popupList.ToArray());
                    res = true;
                }
                catch (Exception e)
                {
                    Logger.Error(e, "JoinToDataWall:");
                    res = false;
                }
            }

            if (!res)
            {
                Logger.Error("Cannot join to datawall.");
                MessageBox.Show("DataWall 접속할 수 없습니다.");
                Application.Current.Shutdown();
            }

            return res;
        }

        public void Stop()
        {
            _actionQueueWorkerTimer.Stop();
            _viewWindowManager.Stop();
            //CloseAllPopups();
            //CloseAllWindows();
        }
        private void OnViewWindowListUpdatedEvent(ScreenWindowListUpdatedEventArg arg)
        {
            if (arg.IsWindowListUpdated)
            {
                _screenWindowList = new List<DataWallWindow>(_viewWindowManager.GetWindows());
            }

            if (arg.IsPopupListUpdated)
            {
                _popupWindowList = new List<DataWallWindow>(_viewWindowManager.GetPopups());
            }

            //Todo: 마스터 서버로 보고해야 함
        }
        public DataWallWindow[] GetWindows()
        {
            return _screenWindowList.ToArray();
        }

        public DataWallWindow[] RequestCreateWindows(string requester, DataWallWindow[] createWindows)
        {
            return CreateWindows(createWindows);
        }

        private DataWallWindow[] CreateWindows(DataWallWindow[] createWindows)
        {
            if (createWindows == null || createWindows.Length == 0)
            {
                return new DataWallWindow[0];
            }
            string[] closeIds = createWindows.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(closeIds);
            ClosePopupsInternal(closeIds);

            List<DataWallWindow> createdWindowList = new List<DataWallWindow>();
            foreach (var createWindow in createWindows)
            {
                if (createWindow.Content is DataWallContent content)
                {
                    var osdConfig = DefaultOsdConfig.Clone();
                    osdConfig.IsShow = content.GetValue("osdIsShow", osdConfig.IsShow);
                    osdConfig.Alignment = content.GetValue("osdAlignment", osdConfig.Alignment);
                    osdConfig.Offset = content.GetValue("osdOffset", osdConfig.Offset);
                    osdConfig.Opacity = content.GetValue("osdOpacity", osdConfig.Opacity);
                    osdConfig.UseDropShadowEffect = content.GetValue("useDropShadowEffect", osdConfig.UseDropShadowEffect);
                    osdConfig.FontSize = content.GetValue("osdFontSize", osdConfig.FontSize);
                    osdConfig.Color = content.GetValue("osdColor", osdConfig.Color);

                    content.SetProperty("osdIsShow", $"{osdConfig.IsShow}");
                    content.SetProperty("osdAlignment", $"{osdConfig.Alignment}");
                    content.SetProperty("osdOffset", $"{osdConfig.Offset}");
                    content.SetProperty("osdOpacity", $"{osdConfig.Opacity}");
                    content.SetProperty("osdUseDropShadowEffect", $"{osdConfig.UseDropShadowEffect}");
                    content.SetProperty("osdFontSize", $"{osdConfig.FontSize}");
                    content.SetProperty("osdColor", $"{osdConfig.Color}");
                }

                createWindow.IsPopup = false;
                CloseWindowsInRectInternal(createWindow.WindowRect);
                _screenWindowList.Add(createWindow);
                createdWindowList.Add(createWindow);
            }

            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.CreateWindows(createdWindowList.ToArray());
            });
            return createdWindowList.ToArray();
        }
        public void RequestCloseWindowsInRect(string requester, IntRect rect)
        {
            CloseWindowsInRect(rect);
        }

        public void CloseWindowsInRect(IntRect rect)
        {
            if (rect == null || rect.IsEmpty)
            {
                return;
            }
            CloseWindowsInRectInternal(rect);
            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.CloseWindowsInRect(rect);
            });
        }
        public void CloseWindowsInRectInternal(IntRect rect)
        {
            if (rect == null || rect.IsEmpty)
            {
                return;
            }
            var windows = _screenWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(rect))
                {
                    _screenWindowList.Remove(window);
                }
            }
        }
        public void RequestCloseWindows(string requester, string[] closeIds)
        {
            CloseWindows(closeIds);
        }

        public void CloseWindows(string[] closeIds)
        {
            if (closeIds == null || closeIds.Length == 0)
            {
                return;
            }

            CloseWindowsInternal(closeIds);
            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.CloseWindows(closeIds);
            });
        }

        public int CloseWindowsInternal(string[] closeIds)
        {
            int updatedCount = 0;
            if (closeIds == null || closeIds.Length == 0)
            {
                return updatedCount;
            }

            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string id in closeIds)
            {
                idDictionary[id] = id;
            }
            DataWallWindow[] windows = _screenWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    _screenWindowList.Remove(window);
                    updatedCount++;
                }
            }

            return updatedCount;
        }

        public void RequestCloseAllWindows(string requester)
        {
            CloseAllWindows();
        }

        private void CloseAllWindows()
        {
            _screenWindowList.Clear();
            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.CloseAllWindows();
            });
        }

        public DataWallWindow[] GetPopups()
        {
            return _popupWindowList.ToArray();
        }

        public DataWallWindow[] RequestCreatePopups(string requester, DataWallWindow[] createPopups)
        {
            return CreatePopups(createPopups);
        }

        private DataWallWindow[] CreatePopups(DataWallWindow[] createPopups)
        {
            if (createPopups == null || createPopups.Length == 0)
            {
                return new DataWallWindow[0];
            }
            string[] deleteIds = createPopups.Select((x) => x.Id).ToArray();
            CloseWindowsInternal(deleteIds);
            ClosePopupsInternal(deleteIds);

            List<DataWallWindow> createdWindowList = new List<DataWallWindow>();
            foreach (var createPopup in createPopups)
            {
                if (createPopup.Content is DataWallContent content)
                {
                    var osdConfig = DefaultOsdConfig.Clone();
                    osdConfig.IsShow = content.GetValue("osdIsShow", osdConfig.IsShow);
                    osdConfig.Alignment = content.GetValue("osdAlignment", osdConfig.Alignment);
                    osdConfig.Offset = content.GetValue("osdOffset", osdConfig.Offset);
                    osdConfig.Opacity = content.GetValue("osdOpacity", osdConfig.Opacity);
                    osdConfig.UseDropShadowEffect = content.GetValue("osdUseDropShadowEffect", osdConfig.UseDropShadowEffect);
                    osdConfig.FontSize = content.GetValue("osdFontSize", osdConfig.FontSize);
                    osdConfig.Color = content.GetValue("osdColor", osdConfig.Color);

                    content.SetProperty("osdIsShow", $"{osdConfig.IsShow}");
                    content.SetProperty("osdAlignment", $"{osdConfig.Alignment}");
                    content.SetProperty("osdOffset", $"{osdConfig.Offset}");
                    content.SetProperty("osdOpacity", $"{osdConfig.Opacity}");
                    content.SetProperty("osdUseDropShadowEffect", $"{osdConfig.UseDropShadowEffect}");
                    content.SetProperty("osdFontSize", $"{osdConfig.FontSize}");
                    content.SetProperty("osdColor", $"{osdConfig.Color}");
                }

                createPopup.IsPopup = true;
                _popupWindowList.Add(createPopup);
                createdWindowList.Add(createPopup);
            }
            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.CreatePopups(createdWindowList.ToArray());
            });
            return createdWindowList.ToArray();
        }

        public void RequestClosePopupsInRect(string requester, IntRect rect)
        {
            ClosePopupsInRect(rect);
        }

        public void ClosePopupsInRect(IntRect rect)
        {
            if (rect == null || rect.IsEmpty)
            {
                return;
            }
            IntRect checkingRect = new IntRect(rect);
            var windows = _popupWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.WindowRect.IsIntersectsAreaWith(checkingRect))
                {
                    _popupWindowList.Remove(window);
                }
            }

            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.ClosePopupsInRect(rect);
            });
        }

        public void RequestClosePopups(string requester, string[] deletePopupIds)
        {
            ClosePopups(deletePopupIds);
        }
        public void ClosePopups(string[] deletePopupIds)
        {
            if (deletePopupIds == null || deletePopupIds.Length == 0)
            {
                return;
            }

            ClosePopupsInternal(deletePopupIds);

            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.ClosePopups(deletePopupIds);
            });
        }

        public int ClosePopupsInternal(string[] closeIds)
        {
            int updatedCount = 0;
            if (closeIds == null || closeIds.Length == 0)
            {
                return updatedCount;
            }
            Dictionary<string, string> idDictionary = new Dictionary<string, string>();
            foreach (string id in closeIds)
            {
                idDictionary[id] = id;
            }
            DataWallWindow[] windows = _popupWindowList.ToArray();
            foreach (var window in windows)
            {
                if (idDictionary.TryGetValue(window.Id, out var outValue))
                {
                    _popupWindowList.Remove(window);
                    updatedCount++;
                }
            }

            return updatedCount;
        }

        public void RequestCloseAllPopups(string requester)
        {
            CloseAllPopups();
        }

        private void CloseAllPopups()
        {
            _popupWindowList.Clear();
            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.CloseAllPopups();
            });
        }

        public void RequestMovePopup(string requester, string windowId, IntRect windowRect)
        {
            MovePopup(windowId, windowRect);
        }
        public void MovePopup(string windowId, IntRect windowRect)
        {
            if (string.IsNullOrWhiteSpace(windowId) || windowRect.IsEmpty)
            {
                return;
            }

            DataWallWindow[] windows = _popupWindowList.ToArray();
            foreach (var window in windows)
            {
                if (window.Id == windowId)
                {
                    window.WindowRect = windowRect;
                    break;
                }
            }
            _actionQueue.Enqueue(() =>
            {
                _viewWindowManager.MovePopup(windowId, windowRect);
            });
        }

        private void ActionQueueWorkerTimerOnTick(object sender, EventArgs arg)
        {
            _actionQueueWorkerTimer.Stop();
            try
            {
                while (_actionQueue.TryDequeue(out var action))
                {
                    action();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "ActionQueueWorkerTimerOnTick:");
            }
            _actionQueueWorkerTimer.Start();
        }
    }
}
