﻿using System;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeViewScreen.Services.ViewWindows;
using Prism.Commands;
using Prism.Mvvm;

namespace NodeViewScreen.ViewModels
{
    public class ViewWindowViewModel : DataWallWindowBindableBase, IViewWindowModel
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private DelegateCommand _closeCommand = null;

        public event Action RequestClose;
        public DelegateCommand CloseCommand => _closeCommand ?? (_closeCommand = new DelegateCommand(DoRequestClose));

        public ViewWindowViewModel(IDataWallWindow window) : base(window)
        {
        }

        public ViewWindowViewModel(string id, string name = "", bool isPopup = false, IntRect screenRect = null, string contentId = "", DataWallContent content = null) : base(id, name, isPopup, screenRect, contentId, content)
        {
        }
        private void DoRequestClose()
        {
            RequestClose?.Invoke();
        }

        public bool CanCloseWindow()
        {
            return true;
        }

        public void OnWindowClosed()
        {
            //Logger.Info($"OnWindowClosed('{Uid}')");
        }

        public void OnWindowOpened()
        {
            //Logger.Info($"OnWindowOpened('{Uid}')");
        }
    }
}
