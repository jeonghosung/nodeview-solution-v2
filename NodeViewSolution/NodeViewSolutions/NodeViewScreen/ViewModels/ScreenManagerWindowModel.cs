﻿using System;
using System.Collections.Generic;
using System.Threading;
using NLog;
using NodeView.DataWalls;
using NodeView.Drawing;
using NodeView.Utils;
using NodeViewScreen.Services.ViewWindows;
using Prism.Commands;
using Prism.Mvvm;

namespace NodeViewScreen.ViewModels
{
    public class ScreenManagerWindowModel : BindableBase
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
    }
}
