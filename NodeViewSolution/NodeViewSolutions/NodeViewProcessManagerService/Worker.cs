using System.Windows;
using NLog;
using NodeView.Apps;
using NodeView.DataModels;
using NodeView.Ioc;
using NodeViewProcessManager;
using Prism.DryIoc;
using Prism.Ioc;

namespace NodeViewProcessManagerService
{
    public class Worker : BackgroundService
    {
        private readonly ILogger<Worker> _logger;
        private static Logger Logger = LogManager.GetCurrentClassLogger();

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            ProcessManagerService service = ProcessManagerService.Instance;
            string configFilename = "NodeViewProcessManager.config.xml";

            OnStartup();
            var container = new DryIocContainerExtension();
            NodeViewContainerExtension.CreateInstance(container);

            Logger.Info("Start ====================== ");

            string settingFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, configFilename);
            var config = Configuration.Load(settingFile);
            if (config == null)
            {
                Logger.Error("Cannot load config!");
                await StopAsync(stoppingToken);
                return;
            }

            RegisterTypes(container);
            OnCreatedShell(container, Array.Empty<string>());

            if (!service.Initialize(container, configFilename))
            {
                Logger.Error("service Initialize error!");
                await StopAsync(stoppingToken);
                return;
            }
            Thread.Sleep(100);
            OnInitialized(container);
            try
            {
                if (!service.Start())
                {
                    Logger.Error("service start error!");
                    await StopAsync(stoppingToken);
                    return;
                }
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
                service.Stop();
            }
        }
        static void OnStartup()
        {
            string appName = AppUtils.GetAppFileName();
            if (AppUtils.IsDuplicateExecution())
            {
                Logger.Error($"'{appName}' 프로그램이 이미 실행 중입니다.");
                AppUtils.Exit();
            }
            else
            {
                SetupUnhandledExceptionHandling();

            }
        }

        public override Task StopAsync(CancellationToken cancellationToken)
        {
            ProcessManagerService service = ProcessManagerService.Instance;
            try
            {
                service.Stop();
                Logger.Info("========================= End");
            }
            catch (Exception e)
            {
                Logger.Error(e, "Main:");
            }
            return base.StopAsync(cancellationToken);
        }

        private static void RegisterTypes(IContainerRegistry containerRegistry)
        {
        }

        private static void OnCreatedShell(IContainerProvider container, string[] args)
        {
        }

        private static void OnInitialized(IContainerProvider container)
        {
        }
        static void SetupUnhandledExceptionHandling()
        {
            // Catch exceptions from all threads in the AppDomain.
            AppDomain.CurrentDomain.UnhandledException += (sender, args) =>
                ShowUnhandledException(args.ExceptionObject as Exception, "AppDomain.CurrentDomain.UnhandledException",
                    false);

            // Catch exceptions from each AppDomain that uses a task scheduler for async operations.
            TaskScheduler.UnobservedTaskException += (sender, args) =>
                ShowUnhandledException(args.Exception, "TaskScheduler.UnobservedTaskException", false);
        }

        static void ShowUnhandledException(Exception e, string unhandledExceptionType, bool promptUserForShutdown)
        {
            Logger.Error(e, $"ShowUnhandledException ['{unhandledExceptionType}',{promptUserForShutdown}]");
            var messageBoxTitle = $"Unexpected Error Occurred: {unhandledExceptionType}";
            var messageBoxMessage = $"The following exception occurred:\n\n{e}";
            var messageBoxButtons = MessageBoxButton.OK;

            if (promptUserForShutdown)
            {
                messageBoxMessage += "\n\nNormally the app would die now. Should we let it die?";
                messageBoxButtons = MessageBoxButton.YesNo;
            }

            // Let the user decide if the app should die or not (if applicable).
            if (MessageBox.Show(messageBoxMessage, messageBoxTitle, messageBoxButtons) == MessageBoxResult.Yes)
            {
                Application.Current.Shutdown();
            }
        }
    }
}